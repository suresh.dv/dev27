<apex:component allowDML="true" controller="FM_AddLoadRequestCmpCtrl" >
	<apex:attribute type="String" name="srcType" description="Facility or Location" required="true" assignTo="{!sourceType}"/>
	<apex:attribute type="String" name="srcId" description="Id of the facility or location" required="true" assignTo="{!sourceId}"/>
	<apex:attribute type="String" name="rteId" description="Id of the route" required="false" assignTo="{!routeId}"/>
	<apex:attribute type="Boolean" name="allowEdit" description="Flag to be able to add/cancel Load Requests" default="false" required="false"/>
	<style>
		.reasonPicklist {
			width: 80%;
			max-width: 200px;
		}
		.actionTable {
			border: 0px !important;
			width: 100%;
			max-width: 250px;
		}
		.actionColumn {
			max-width: 300px;
			/*min-width: 200px;*/
			min-width: 25px;
		}
		.tankPickList {
			width: 80px;
		}
		.actionTableColumn {
			border: 0px !important;
		}
		.reasonColumn {
			max-width: 200px;
			min-width: 50px;
		}
		.tankInfoColumn {
			max-width: 15px;
			min-width: 10px;
		}
		.actionIcon {
			cursor: pointer;
		}
		.icon-container {
			display: block;
			width: 20px;
			height: 20px;
			overflow: hidden;
		}
		.refresh {
			clip: rect(44px, 20px, 66px, 0px);
			margin-top: -44px;
		}
		/*Popup*/
        .custPopup
        {
            background-color: white;
            border-radius: 5px;
            border-style: solid;
            border-width: 1px;
            left: 50%;
            margin-left: -250px;
            padding:10px;
            position: fixed;
            top: 30%;
            width: 450px;
            z-index: 99;
        }
        .custPopup .apexp .bPageBlock .pbHeader table {
            padding: 0px;
        }
        .custPopup .apexp .bPageBlock .pbBody,
        .custPopup .apexp .bPageBlock .pbBottomButtons {
            margin:  0px !important;
            white-space: normal;
        }
        .custPopup .popupmessage span{
            height: 100%;
            overflow: auto;
        }
        .popupBackground{
            background-color:black;
            filter: alpha(opacity = 20);
            height: 100%;
            left: 0;
            opacity: 0.20;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 98;
        }
	</style>
	<apex:outputPanel id="containerPanel" rendered="{!NOT(ISBLANK(sourceId))}">
		<apex:form id="lsform">

			<apex:pageMessages id="messages" />

			<apex:actionFunction name="onLoadFn" action="{!getData}" reRender="lsform"/>

			<apex:pageBlock mode="none" rendered="{!location != null || facility != null}">
				<apex:outputPanel layout="none" id="loadRequestPanel" >

					<!-- Buttons area -->
					<apex:outputPanel id="buttonsArea">
						<!-- <apex:commandButton value="Release" rendered="{!IsLoadRequestsToRelease}"
											onClick="return confirm('Are you sure you want to release all new Load Requests?');"
											action="{!releaseLoadRequests}"/> -->
						<apex:commandButton value="Add" action="{!AddLoadRequest}" rendered="{!allowEdit}"
											reRender="lsform" status="ajaxStatus"/>
						<apex:commandButton rendered="{!IsLoadRequestsToSave}" value="Save" action="{!saveNewLoadRequests}"
											reRender="loadRequestPanel, msgs, lsform" status="ajaxStatus"/>
					</apex:outputPanel>
					<apex:variable var="rowIndex" value="{!0}"/>
					<apex:pageMessage detail="No load requests for this location yet!" severity="warning" rendered="{!loadRequestWrappers == null || loadRequestWrappers.size == 0}"/>
					<apex:pageBlockTable value="{!loadRequestWrappers}" var="lrw" id="loadRequestTable" rendered="{!loadRequestWrappers != null && loadRequestWrappers.size > 0}">

						<!-- Actions like cancel, remove -->
						<apex:column styleClass="actionColumn" rendered="{!isAction}">
							<!-- <apex:image value="/img/func_icons/remove12_on.gif" alt="Cancel Load Request" rendered="{!IF((lrw.loadRequest.Status__c == loadRequestHauledStatus || lrw.loadRequest.Status__c == loadRequestCancelledStatus) || lrw.isNew, false, isDispatcher)}" styleClass="actionIcon">
								<apex:actionSupport action="{!cancelLoadRequestPopupOpen}" event="onclick" reRender="lsform" status="ajaxStatus">
									<apex:param name="index" value="{!rowIndex}"/>
								</apex:actionSupport>
							</apex:image> -->
							<apex:image value="/img/func_icons/remove12_on.gif" alt="Remove Load Request" rendered="{!lrw.isNew}" styleClass="actionIcon">
								<apex:actionSupport action="{!removeLoadRequestByIndex}" event="onclick" reRender="lsform" status="ajaxStatus">
									<apex:param name="index" value="{!rowIndex}"/>
								</apex:actionSupport>
							</apex:image>
							<apex:outputPanel styleClass="icon-container">
								<apex:image value="/img/alohaSkin/sync.png" alt="Recreate Load Request" rendered="{!lrw.recreateAllowed}" styleClass="actionIcon refresh">
									<apex:actionSupport action="{!lrw.recreate}" event="onclick"/>
								</apex:image>
							</apex:outputPanel>
						</apex:column>

						<!-- Load request info -->
						<apex:column >
							<apex:facet name="header">Load Request</apex:facet>
							<apex:outputLink rendered="{!!lrw.isNew}" value="/{!lrw.loadRequest.Id}" target="_blank">
								{!lrw.loadRequest.Name}
							</apex:outputLink>
						</apex:column>

						<!-- Tank information -->
						<apex:column rendered="{!IsLoadRequestsToSave}">
							<apex:facet name="header">Tank</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew}" value="{!lrw.loadRequest.Tank_Label__c}" />
							<apex:selectList rendered="{!lrw.isNew}" size="1" value="{!lrw.tank}" styleClass="tankPickList">
								<apex:selectOption itemValue="" itemLabel="--Standard Tanks--" itemDisabled="true"/>
								<apex:selectOptions value="{!tanks}" rendered="{!tanks != null && tanks.size > 0}"/>
								<apex:selectOption itemValue="" itemLabel="--Extra Tanks--" itemDisabled="true"/>
								<apex:selectOptions value="{!extraTanks}"/>
								<apex:actionSupport action="{!updateLoadRequestTankInfo}" event="onchange" reRender="loadRequestPanel" status="ajaxStatus">
									<apex:param name="index" value="{!rowIndex}"/>
								</apex:actionSupport>
							</apex:selectList>
						</apex:column>

						<!-- Flow Rate info -->
						<apex:column >
							<apex:facet name="header">{!$ObjectType.FM_Load_Request__c.fields.Act_Flow_Rate__c.Label}</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew}" value="{!lrw.loadRequest.Act_Flow_Rate__c}"/>
							<apex:outputPanel styleClass="requiredInput" layout="block" >
							<apex:outputPanel styleClass="requiredBlock" layout="block"/>
								<apex:inputField rendered="{!lrw.isNew}" value="{!lrw.loadRequest.Act_Flow_Rate__c}" />
							</apex:outputPanel>
						</apex:column>

						<!-- Act tank Level info -->
						<apex:column >
							<apex:facet name="header">{!$ObjectType.FM_Load_Request__c.fields.Act_Tank_Level__c.Label}</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew || !isTankInTankLabels}" value="{!lrw.loadRequest.Act_Tank_Level__c}"/>
							<apex:outputPanel styleClass="requiredInput" layout="block" >
							<apex:outputPanel styleClass="requiredBlock" layout="block"/>
								<apex:inputField rendered="{!lrw.isNew && (isTankInTankLabels || ISBLANK(lrw.loadRequest.Act_Tank_Level__c))}" value="{!lrw.loadRequest.Act_Tank_Level__c}" />
							</apex:outputPanel>
						</apex:column>

						<!-- Tank low level info -->
						<apex:column >
							<apex:facet name="header">{!$ObjectType.FM_Load_Request__c.fields.Tank_Low_Level__c.Label}</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew || !isTankInTankLabels}" value="{!lrw.loadRequest.Tank_Low_Level__c}"/>
							<apex:outputPanel styleClass="requiredInput" layout="block" >
							<apex:outputPanel styleClass="requiredBlock" layout="block"/>
								<apex:inputField rendered="{!lrw.isNew && isTankInTankLabels}" value="{!lrw.loadRequest.Tank_Low_Level__c}"/>
							</apex:outputPanel>
						</apex:column>

						<!-- Tank size info -->
						<apex:column >
							<apex:facet name="header">{!$ObjectType.FM_Load_Request__c.fields.Tank_Size__c.Label}</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew || !isTankInTankLabels}" value="{!lrw.loadRequest.Tank_Size__c}"/>
							<apex:outputPanel styleClass="requiredInput" layout="block" >
							<apex:outputPanel styleClass="requiredBlock" layout="block"/>
								<apex:inputField rendered="{!lrw.isNew && isTankInTankLabels}" value="{!lrw.loadRequest.Tank_Size__c}"/>
							</apex:outputPanel>
						</apex:column>

						<!-- Load type information -->
						<apex:column >
							<apex:facet name="header">Load Type</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew}" value="{!lrw.loadRequest.Load_Type__c}"/>
							<apex:inputField rendered="{!lrw.isNew && isDispatcher && sourceType == 'location'}" value="{!lrw.loadRequest.Load_Type__c}" required="true"/>
							<apex:selectList rendered="{!lrw.isNew && isDispatcher && sourceType == 'facility'}" value="{!lrw.loadRequest.Load_Type__c}" required="true" size="1">
								<apex:selectOptions value="{!facilityLoadTypes}" />
							</apex:selectList>
						</apex:column>

						<!-- Product information -->
						<apex:column >
							<apex:facet name="header">Product</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew}" value="{!lrw.loadRequest.Product__c}" />
							<apex:inputField rendered="{!lrw.isNew}" value="{!lrw.loadRequest.Product__c}" required="true"/>
						</apex:column>

						<!-- Shift information -->
						<apex:column >
							<apex:facet name="header">Shift</apex:facet>
							<apex:outputText rendered="{!AND(!lrw.isNew, allowEdit)}" value="{!lrw.shift}"/>
							<apex:outputField rendered="{!AND(!lrw.isNew, !allowEdit)}" value="{!lrw.loadRequest.Shift__c}"/>
							<apex:selectList rendered="{!lrw.isNew}" value="{!lrw.shift}" required="true" size="1">
								<apex:selectOptions value="{!shiftOptions}"/>
							</apex:selectList>
						</apex:column>

						<!-- Load Weight information (when adding new LR) -->
						<apex:column rendered="{!IsLoadRequestsToSave}">
							<apex:facet name="header">Load Weight</apex:facet>
							<apex:inputField rendered="{!lrw.isNew}" value="{!lrw.loadRequest.Load_Weight__c}" required="true"/>
							<apex:outputText rendered="{!!lrw.isNew}" value="{!lrw.loadRequest.Load_Weight__c}"/>
						</apex:column>

						<!-- Comments -->
						<apex:column >
							<apex:facet name="header">Comments</apex:facet>
							<apex:outputField rendered="{!!lrw.isNew}" value="{!lrw.loadRequest.Standing_Comments__c}" />
							<apex:inputField rendered="{!lrw.isNew}" value="{!lrw.loadRequest.Standing_Comments__c}" />
						</apex:column>

						<!-- Create reason (displaying only for new) -->
						<apex:column styleClass="reasonColumn">
							<apex:facet name="header">Create Reason</apex:facet>
							<apex:selectList size="1" value="{!lrw.loadRequest.Create_Reason__c}" styleClass="reasonPicklist" rendered="{!lrw.isNew}">
								<apex:selectOptions value="{!createReasonOptions}"/>
								<apex:actionSupport event="onchange" action="{!lrw.handleReasonChange}" reRender="loadRequestPanel"/>
							</apex:selectList>
							<apex:outputField value="{!lrw.loadRequest.Create_Reason__c}" styleClass="reasonPicklist" rendered="{!AND(!lrw.isNew, lrw.loadRequest.Create_Reason__c != 'Other')}"/>
							<apex:outputField value="{!lrw.loadRequest.Create_Comments__c}" rendered="{!AND(!lrw.isNew, lrw.loadRequest.Create_Reason__c == 'Other')}"/>
							<apex:inputField value="{!lrw.loadRequest.Create_Comments__c}" rendered="{!AND(lrw.isNew, lrw.loadRequest.Create_Reason__c == 'Other')}"/>
						</apex:column>

						<!-- Status of LR -->
						<apex:column >
							<apex:facet name="header">Status</apex:facet>
							<apex:outputField value="{!lrw.loadRequest.Status__c}" />
							<apex:outputText value=" - " rendered="{!IF(lrw.loadRequest.Status__c == loadRequestCancelledStatus, true, false)}"/>
							<apex:outputField value="{!lrw.loadRequest.Cancel_Reason__c}"
											rendered="{!IF(lrw.loadRequest.Status__c == loadRequestCancelledStatus, true, false)}"/>
							<apex:variable var="rowIndex" value="{!rowIndex + 1}"/>
						</apex:column>
						<apex:column rendered="{!IsHaluedLoadRequest}">
							<apex:facet name="header">Ticket Number</apex:facet>
							<apex:outputField value="{!lrw.loadRequest.Ticket_Number__c}" rendered="{!lrw.loadRequest.Ticket_Number__c != null}"/>
						</apex:column>
					</apex:pageBlockTable>
				</apex:outputPanel>
			</apex:pageBlock>

			<!------ Rebook Reason Prompt ------>
	        <apex:outputPanel id="cancelPopup" styleClass="popup-wrapper">
	            <apex:outputPanel layout="block" rendered="{!showCancelPopup}">
	                <apex:outputPanel styleClass="popupBackground" layout="block" />
	                <apex:outputPanel styleClass="custPopup" layout="block">
	                    <apex:pageBlock mode="edit" title="Cancel">
	                        <apex:pageBlockSection collapsible="false" columns="1" showHeader="false" >
	                            <apex:inputField value="{!cancelLoadRequest.Cancel_Reason__c}" required="true"/>
	                            <apex:outputText value="Note: Cancellation of the truck trip will result in cancellation of the load and the load cannot be rebooked. You will have to create a new Load Request to rebook the load." />
	                        </apex:pageBlockSection>
	                        <apex:pageBlockButtons location="bottom">
	                            <apex:commandButton value="Confirm" action="{!cancelLoadRequestPopupConfirm}" rerender="lsform" status="ajaxStatus" />
	                            <apex:commandButton value="Close" action="{!cancelLoadRequestPopupCancel}" rerender="lsform" status="ajaxStatus" />
	                        </apex:pageBlockButtons>
	                </apex:pageBlock>
	                </apex:outputPanel>
	            </apex:outputPanel>
	        </apex:outputPanel>
		</apex:form>
		<script>
			onLoadFn();
		</script>
	</apex:outputPanel>
</apex:component>