/**
 * Created by MarcelBrimus on 02/09/2019.
 */

import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class vtt_calendar_wo_item extends NavigationMixin(LightningElement) {
    @api item;
    @api key;

    navigateToRecord(evt){
        evt.preventDefault();
        evt.stopPropagation();
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": this.item.WorkOrderId,
                "objectApiName": "HOG_Maintenance_Servicing_Form__c",
                "actionName": "view"
            },
        });
    }
}