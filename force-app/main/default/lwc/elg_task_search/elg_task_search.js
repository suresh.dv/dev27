/**
 * Created by MarosGrajcar on 10/28/2019.
 */

import {LightningElement, track} from 'lwc';
import getUserDefaultAMU from '@salesforce/apex/ELG_ListView_Endpoint.getUserDefaultAMU';
import getAMUsPosts from '@salesforce/apex/ELG_TaskSearch_Endpoint.getAMUsPosts';
import getSearchResults from '@salesforce/apex/ELG_TaskSearch_Endpoint.getSearchResults';
import getPicklistValues from '@salesforce/apex/HOG_PicklistServiceLtng.getPicklist';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

//Global variables
var sizeOfOffset = 0;
const numberOfRecords = 50;

export default class HogListView extends LightningElement {

    /* VARIABLES */

    //TASKS
    @track tasks;
    @track showSpinner = true;

    //DATATABLE COLUMN SELECTOR
    @track defaultView = true;
    @track completedView = false;
    @track cancelledView = false;

    //ERROR
    @track showError = false;
    @track errorMessage;

    //USER SORTING
    @track userSort = false;
    @track checked = false;
    //default sorting
    @track sortByField = 'CreatedDate';
    @track sortingDirection = 'DESC';

    //PAGINATION
    @track totalRecords;
    @track currentPage;
    @track pageSize = numberOfRecords;
    @track totalPages;
    @track singlePage = false;

    //PICKLIST OPTIONS VARIABLES
    @track thermalAMUs;
    @track thermalPosts;
    @track taskStatuses;

    //CLEAR SPECIFIC SELECTION FROM FILTER (little x icon)
    @track postIsSelected = false;
    @track statusIsSelected = false;
    @track startDateIsSelected = false;
    @track endDateIsSelected = false;

    //VALUE HOLDERS VARIABLES
    @track selectedAMU;
    @track selectedPost;
    @track selectedTaskStatus;
    @track startDate;
    @track endDate;

    //SHOW FILTER SECTION
    //when AMU is selected will show filter options
    @track showFilter = false;

    //Basic datatable
    @track taskColumns = [
        {
            label: 'Task Name',
            fieldName: 'nameUrl',
            type: 'url',
            typeAttributes: {label: {fieldName: 'taskName'}, target: '_blank', tooltip: 'Open Task'},
        },
        {label: 'Short Description', fieldName: 'shortDescription', type: 'text'},
        {label: 'Status', fieldName: 'taskStatus', type: 'text'},
        {label: 'Operators', fieldName: 'operators', type: 'text'},
        {label: 'Dates', fieldName: 'dates', type: 'text'},
    ];

    //when completed
    @track completedTasksColumns = [
            {
                label: 'Task Name',
                fieldName: 'nameUrl',
                type: 'url',
                typeAttributes: {label: {fieldName: 'taskName'}, target: '_blank', tooltip: 'Open Task'},
            },
            {label: 'Short Description', fieldName: 'shortDescription', type: 'text'},
            {label: 'Status', fieldName: 'taskStatus', type: 'text'},
            {label: 'Comment', fieldName: 'taskComment' , type: 'text'},
            {label: 'Operators', fieldName: 'operators', type: 'text'},
            {label: 'Dates', fieldName: 'dates', type: 'text'},
        ];

        //when cancelled
        @track cancelledTasksColumns = [
                {
                    label: 'Task Name',
                    fieldName: 'nameUrl',
                    type: 'url',
                    typeAttributes: {label: {fieldName: 'taskName'}, target: '_blank', tooltip: 'Open Task'},
                },
                {label: 'Short Description', fieldName: 'shortDescription', type: 'text'},
                {label: 'Status', fieldName: 'taskStatus', type: 'text'},
                {label: 'Cancel Reason', fieldName: 'taskCancelReason' , type: 'text'},
                {label: 'Operators', fieldName: 'operators', type: 'text'},
                {label: 'Dates', fieldName: 'dates', type: 'text'},
            ];


    /* CONSTRUCTOR */
    constructor() {
        super();
        this.getDefaultAMU();
    }

    /* GETTERS */
    get sortByFieldOptions() {
        return [
            { label: 'Created Date', value: 'CreatedDate'},
            { label: 'Name', value: 'Name'},
            { label: 'Task Status', value: 'Status__c'},
            { label: 'Post', value: 'Post_Name__c'},
        ];
    }

    get sortByDirection() {
        return [
            { label: 'Ascending', value: 'ASC'},
            { label: 'Descending', value: 'DESC'},
        ];
    }

    /* FUNCTIONS */

    //Get list of AMUs
    getDefaultAMU() {
        getUserDefaultAMU({}).then(result => {
            if (result != null) {
                this.selectedAMU = result;
                this.showFilterOptions(this.selectedAMU.Id);
            }
            this.showSpinner = false;
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    //Get Posts, populate Shift Cycle options, populate Handover status options
    showFilterOptions(thermalAMU) {
        getAMUsPosts({currentAMU: thermalAMU}).then(result => {
            this.thermalPosts = result;
            let context = this;
            this.pickListValues('ELG_Task__c', 'Status__c', function (value) {
                context.taskStatuses = value;
            });
        }).catch(error => {
                this.showSpinner = false;
                if (Array.isArray(error.body)) {
                    error.body.map(e => e.message).join(', ');
                } else if (typeof error.body.message === 'string') {
                    this.showToast(error.body.message);
                }
        });
        this.showFilter = true;
    }

    //Generic method to get picklist values from Object
    pickListValues(nameOfObject, nameOfField, callbackFunction) {
        getPicklistValues({objectName: nameOfObject, fieldName: nameOfField}).then(result => {
            callbackFunction(result);
        });
    }

    //Search button with params
    //Will pass javascript Object to Apex
    availableTasks() {
        let searchParams = {
            amuId: this.selectedAMU.Id,
            postId: this.selectedPost,
            taskStatus: this.selectedTaskStatus,
            startDate: this.startDate,
            endDate: this.endDate,
        };
        this.tasks = null;
        this.showSpinner = true;
        sizeOfOffset = 0;
        getSearchResults({filterSelection: searchParams,
                        offsetSize: sizeOfOffset,
                        fieldSort : this.sortByField,
                        sortDirection: this.sortingDirection,
                        defaultView: this.defaultView,
                        completedView: this.completedView,
                        cancelledView: this.cancelledView}).then(result => {
            if (result[0].hasError) {
                this.showError = true;
                this.errorMessage = result[0].errorMsg;
                this.showSpinner = false;
            } else {
                this.showError = false;
                this.tasks = result;
                this.showSpinner = false;
                this.totalRecords = result[0].numberOfRecords;
                this.totalPages = Math.ceil(this.totalRecords / 50);
               if (this.totalPages == 1) {
                    this.singlePage = true;
               } else {
                    this.singlePage = false;
                    this.currentPage = (sizeOfOffset / 50) +1;
               }
            }
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    //Pagination buttons
    //the same as availableTasks function except this one do not reset offset value
    loadMoreTasks() {
        let searchParams = {
            amuId: this.selectedAMU.Id,
            postId: this.selectedPost,
            shiftCycle: this.selectedShiftCycle,
            taskStatus: this.selectedTaskStatus,
            startDate: this.startDate,
            endDate: this.endDate,
        };
        this.tasks = null;
        this.showSpinner = true;
        getSearchResults({filterSelection: searchParams,
                        offsetSize: sizeOfOffset,
                        fieldSort : this.sortByField,
                        sortDirection: this.sortingDirection,
                        defaultView: this.defaultView,
                        completedView: this.completedView,
                        cancelledView: this.cancelledView}).then(result => {
            this.showError = false;
            this.tasks = result;
            this.showSpinner = false;
            this.totalRecords = result[0].numberOfRecords;
            this.totalPages = Math.ceil(this.totalRecords / 50);
            this.currentPage = (sizeOfOffset / 50) +1;
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    //Clear data except Shift Engineer check and AMU
    clearData() {
        this.tasks = null;

        this.selectedPost = null;
        this.selectedTaskStatus = null;
        this.startDate = null;
        this.endDate = null;

        this.checked = false;
        this.userSort = false;
        this.sortByField = 'CreatedDate';
        this.sortingDirection = 'DESC';

        this.postIsSelected = false;
        this.statusIsSelected = false;
        this.startDateIsSelected = false;
        this.endDateIsSelected = false;

        this.defaultView = true;
        this.completedView = false;
        this.cancelledView = false;

        const clearLookup = this.template.querySelectorAll('c-elg_amu_lookup');
        if (clearLookup) {
            clearLookup.forEach(function (value) {
                value.clearSelection();
            });
        }
    }

    //CLEAR SELECTION FUNCTIONS
    clearPostSelection() {
        this.selectedPost = null;
        this.postIsSelected = false;
    }

    clearStatusSelection() {
        this.selectedTaskStatus = null;
        this.statusIsSelected = false;
        this.defaultView = true;
        this.completedView = false;
        this.cancelledView = false;
    }

    clearStartDate() {
        this.startDate = null;
        this.startDateIsSelected = false;
    }

    clearEndDate() {
        this.endDate = null;
        this.endDateIsSelected = false;
    }

    /* PAGINATION SECTION */

    get showFirstButton() {
        if (this.currentPage === 1) {
            return true;
        }
        return false;
    }

    get showLastButton() {
        if (Math.ceil(this.totalRecords / this.pageSize) === this.currentPage) {
            return true;
        }
        return false;
    }

    handlePrevious() {
        sizeOfOffset -= numberOfRecords;
        this.loadMoreTasks();
    }

    handleNext() {
        sizeOfOffset += numberOfRecords;
        this.loadMoreTasks();
    }

    handleFirst() {
        sizeOfOffset = 0;
        this.loadMoreTasks();
    }

    handleLast() {
        sizeOfOffset = (this.totalPages - 1) * numberOfRecords;
        this.loadMoreTasks();
    }

   /* HANDLERS */
    handleAMU(event) {
        var operatingField = event.detail;
        if (operatingField) {
            this.selectedAMU = operatingField;
            this.showFilterOptions(operatingField.Id);
        } else {
            this.selectedAMU = null;
        }
    }

    clearAMU(event) {
        this.selectedAMU = null;
        this.showFilter = false;
        this.clearData();
    }

    handleAMUChange(event) {
        this.selectedAMU = event.detail.value;
        this.showFilterOptions(this.selectedAMU);
        this.clearData();
    }

    handleSelectedPost(event) {
        this.selectedPost = event.detail.value;
        this.postIsSelected = true;
    }

    handleSelectedTaskStatus(event) {
        this.selectedTaskStatus = event.detail.value;
        if (this.selectedTaskStatus == 'Completed') {
            this.defaultView = false;
            this.completedView = true;
            this.cancelledView = false;
        } else if (this.selectedTaskStatus == 'Cancelled') {
            this.defaultView = false;
            this.completedView = false;
            this.cancelledView = true;
        } else {
            this.defaultView = true;
            this.completedView = false;
            this.cancelledView = false;
        }
        this.statusIsSelected = true;
    }

    handleSelectedStartDate(event) {
        this.startDate = event.detail.value;
        this.startDateIsSelected = true;
    }

    handleSelectedEndDate(event) {
        this.endDate = event.detail.value;
        this.endDateIsSelected = true;
    }

    //START OF USER SORTING HANDLERS
    handleCheckboxChange(event) {
        this.checked = !this.checked;
        this.userSort = this.checked;

        if (!this.checked) {
            this.sortByField = 'CreatedDate';
            this.sortingDirection = 'DESC';
        }
    }

    handleSortByField(event) {
        this.sortByField = event.detail.value;
    }

    handleSortingDirection(event) {
        this.sortingDirection = event.detail.value;
    }
    // END OF USER SORTING HANDLERS

    /* TOAST */
    showToast(errorMessage) {
        const event = new ShowToastEvent({
            title: 'Please, contact HOGLloydSF@huskyenergy.com with screenshot attached and description what action triggered this error.',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky',
        });
        this.dispatchEvent(event);
    }

    /* SELECTED TASKS SECTION */

    openSelectedTasks() {
        var selectedDatatableTasks = this.template.querySelector('lightning-datatable').getSelectedRows();

        if (selectedDatatableTasks.length > 1) {
           const selectedHandovers = new CustomEvent("passSelectedTasks", { detail: {selectedDatatableTasks} });
           this.dispatchEvent(selectedHandovers);
        } else {
           const event = new ShowToastEvent({
                       message: 'Please, select at least two Tasks to proceed.',
                       variant: 'warning',
                   });
           this.dispatchEvent(event);
        }

    }

}