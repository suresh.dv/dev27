/**
 * Created by MarcelBrimus on 29/11/2019.
 */

import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class VttLightningCommunityManagementDisabledContact extends NavigationMixin(LightningElement) {
    @api contact;

    navigateToRecord(){
       this[NavigationMixin.Navigate]({
           type: 'standard__recordPage',
           attributes: {
               recordId: this.contact.Id,
               objectApiName: 'Contact',
               actionName: 'view'
           }
       });
    }
}