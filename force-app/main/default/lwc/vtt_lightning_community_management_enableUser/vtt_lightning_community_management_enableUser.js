/**
 * Created by MarcelBrimus on 29/11/2019.
 */

import { LightningElement, api, track } from 'lwc';
import enableLightningUser from '@salesforce/apex/VTT_LTNG_PortalManagementEndpoint.enableLightningUser';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class VttLightningCommunityManagementEnableUser extends LightningElement {
    @api contact;
    @track showModal = false;
    @track migratingUser = false;

    enableContactTradesman(){
        this.enableLightningUser('HOG_Vendor_Community_VTT_User_Lightning')
    }

    enableContactSupervisor(){
        this.enableLightningUser('HOG_Vendor_Community_VTT_Vendor_Supervisor_Lightning')
    }

    openmodal() {
        this.showModal = true
    }

    closeModal() {
        this.showModal = false
    }

    enableLightningUser(userType){
        this.migratingUser = true;
        enableLightningUser({
            contact: JSON.stringify(this.contact),
            userType: userType
        }).then(result => {
            if(result.success){
                this.showNotification('Successfully created lightning community user', 'Please click refresh button to see changes after a while', 'success');
            } else {
                var errorMessage = '';
                result.errors.forEach(function (item, index) {
                    errorMessage = errorMessage + item + '.'
                });
                this.showNotification('Error Creating User ', errorMessage, 'error');
            }
            this.migratingUser = false;
            this.closeModal();
        }).catch(error => {
            this.migratingUser = false;
            this.closeModal();
            console.log('Migrating err ' + error)
        });
    }

    // UTILITIES
    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }
}