/**
 * Created by MarosGrajcar on 1/15/2020.
 */

import { LightningElement, api, track } from 'lwc';
import equipmentFormsForWorkOrder from '@salesforce/apex/Equipment_Forms_Endpoint.equipmentFormsForWorkOrder';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

export default class EqtAddTransferForms extends NavigationMixin(LightningElement) {

    @track addForms;
    @track addFormsEmpty = true;
    @track transferTOForms;
    @track transferTOFormsEmpty = true;
    @track transferFROMForms;
    @track transferFROMFormsEmpty = true;

    @track loading = true;

    @api
    get recordId() {
       return this._recordId;
    }

    set recordId(value) {
       this._recordId = value;
       this.loadForms();
       //this.loadTransferToForm();
       //this.loadTransferFROMForm();
    }

    @api
    loadForms() {
        this.loading = true;
        equipmentFormsForWorkOrder({flocId : this.recordId}).then(result => {
            if (result.addForms.length > 0 ) {
                this.addForms = result.addForms;
                this.addFormsEmpty = false;
            }

            if (result.transferToForms.length > 0 ) {
                this.transferTOForms = result.transferToForms;
                this.transferTOFormsEmpty = false;
            }

            if (result.transferFromForms.length > 0 ) {
                this.transferFROMForms = result.transferFromForms;
                this.transferFROMFormsEmpty = false;
            }
        }).catch(error => {
            console.log('Error on load forms' + JSON.stringify(error));
            this.dispatchEvent(
                new ShowToastEvent({
                title: '',
                message: error.message,
                variant: 'error',
                }),
            );
        }).finally(() => {
            this.loading = false;
        });
    }

    @track openAddFormModal = false;
    @track openTransferFormModal = false;
    @track formId;
    @track showButtons;
    openAddEquipmentModal(event) {
        this.formId = event.currentTarget.dataset.id;
        this.openAddFormModal = !this.openAddFormModal;
        this.showButtons = true;
    }

    closeAddFormModal() {
        this.openAddFormModal = !this.openAddFormModal;
        this.loadForms();
    }

    saveAddForm() {
        var saveMethod = this.template.querySelector('c-eqt_add_cru');
        if(saveMethod) {
            this.showButtons = false;
            saveMethod.getUserInputs();
        }
    }

    enableButtons() {
        this.showButtons = true;
    }

    openTransferModal(event) {
        this.formId = event.currentTarget.dataset.id;
        this.openTransferFormModal = !this.openTransferFormModal;
        this.showButtons = true;
    }

    closeTransferFormModal() {
        this.openTransferFormModal = !this.openTransferFormModal;
        this.loadForms();
    }

    saveTransferForm() {
        var saveMethod = this.template.querySelector('c-eqt_transfer_cru');
        if(saveMethod) {
            this.showButtons = false;
            saveMethod.getUserInputs();
        }
    }


    navigateToAddFormRecord(event) {
        var formId = event.currentTarget.dataset.id;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'Equipment_Missing_Form__c',
                actionName: 'view',
                recordId: formId
            },
        });
    }

    navigateToTransferFormRecord(event) {
        var formId = event.currentTarget.dataset.id;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'Equipment_Transfer_Form__c',
                actionName: 'view',
                recordId: formId
            },
        });
    }

    navigateToUser(event) {
        var userId = event.currentTarget.dataset.userid;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'User',
                actionName: 'view',
                recordId: userId
            },
        });
    }
}