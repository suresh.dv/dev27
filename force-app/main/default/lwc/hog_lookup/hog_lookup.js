/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-08-20 - Created.
*************************************************************************************************/
import { LightningElement, track, api } from 'lwc';

export default class HOG_Lookup extends LightningElement {

//required attributes
    @api label; //String
    @api objectAPIName; //String - target SObject API Name
//optional attributes
    @api fieldAPIName = 'Name'; //String - field API Name that result would be displayed
    @api additionalFields = ''; //String - field API Names (comma delimiter) that would query additional values
    @api placeholder = ''; //String - placeholder on empty input
    @api isRequired = false;
    @api isReadOnly = false;
    @api isDisabled = false;
    @api iconName = 'standard:people'; //String - icon name (formatted in lightning design system way)
    @api renderIcon = false; //Boolean - enables/disables icon on input/result
    @api withSharing; //Boolean - with/without sharing flag. Defaulted to true

    constructor() {
        super();
        this.withSharing = true;
    }

    @track selectedRecord = undefined; //SObject

//APIs
    @api
    getSelectedRecord() {
        return this.selectedRecord;
    }

    @api
    getSelectedRecordId() {
        if(this.isRecordSelected()) {
            return this.selectedRecord.Id;
        }

        return undefined;
    }

    @api
    selectRecord(record) {
        this.selectedRecord = record;
    }

    @api
    get preSelectedRecord() {
        return this.selectedRecord;
    }

    set preSelectedRecord(value) {
        this.selectedRecord = value;
    }

    @api
    clearSelection() {
        this.resetComponent();
    }

    @api
    isValid() {
        return !this.isRequired()
                || this.isRecordSelected();
    }

//MAIN LOGIC
    resetComponent() {
        this.selectedRecord = undefined;
        const evt = new CustomEvent('clearselection');
        this.dispatchEvent(evt);
    }

    handleRecordSelection(evt) {
        this.selectedRecord = evt.detail;
        this.dispatchEvent(
            new CustomEvent(
                'selection',
                {detail: this.selectedRecord}
            )
        );
    }

//HELPERS
    isRecordSelected() {
        return this.selectedRecord !== undefined
                && this.selectedRecord !== null;
    }

    isReadOnlyMode() {

        return this.isReadOnly
                || this.isDisabled;
    }

    isSelectedMode() {
        return this.isRecordSelected()
                && !this.isReadOnlyMode();
    }

//GETTERS
    get selectedText() {
        if(this.selectedRecord) {
            return this.selectedRecord[this.fieldAPIName];
        }
        return '';
    }

    get readOnlyMode() {
        return this.isReadOnlyMode();
    }

    get inputMode() {
        return !this.isSelectedMode()
                && !this.isReadOnlyMode();
    }

    get selectedMode() {
        return this.isSelectedMode()
                && !this.isReadOnlyMode();
    }

}