/**
 * Created by MarosGrajcar on 8/21/2019.
 */

import { LightningElement, api, track} from 'lwc';
import userId from '@salesforce/user/Id';

const PADDING_ON = 'slds-m-around_small slds-p-around_large';
const PADDING_OFF = '';
const HIDDEN = 'slds-hide';
const SHOWN = '';

export default class HogGenericAccordion extends LightningElement {

    @api header = '';
    @api backgroundColor = '#F5F5F5';
    @api headerTextColor = 'black';
    @api bodyTextColor = 'black';
    @api disablePadding = false;
    @api useCookiesState = false;
    @api recordIdentifier = '';
    @api uniqueAccordionIdentifier = '';

    @track _loadOpenAccordion = false;
    @track displayControlledStyling = HIDDEN;

    connectedCallback() {
        this.getCookieState();
    }

    getCookieState() {
        if (this.useCookiesState) {
            var cookieStatus = this.getCookie(this.recordIdentifier);

            if(cookieStatus != undefined && cookieStatus != null) {
                this.loadOpenAccordion = cookieStatus == true || cookieStatus == 'true';
            }
        }
    }

    setCookieState() {
        if (this.useCookiesState) {
            var lalala = this.createCookie(this.recordIdentifier, this._loadOpenAccordion);
        }
    }

    getCookie(name) {
        let cookieString = "; " + document.cookie;
        let parts = cookieString.split("; " + this.uniqueAccordionIdentifier + userId + name + "=");
        if (parts.length === 2) {
            return unescape(parts.pop().split(";").shift());
        }
        return null;
    }

    createCookie(name, value, days) {
        let expires;
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = this.uniqueAccordionIdentifier + userId + name + "=" + escape(value) + expires + "; path=/";
    }

    @api
    get loadOpenAccordion() {
        return this._loadOpenAccordion;
    }

    set loadOpenAccordion(value) {
        this._loadOpenAccordion = value;
        this.setControlledStyling();
    }

    get sectionBackgroundColor() {
        var bckClr = 'background-color: ' +this.backgroundColor;
        return bckClr;
    }

    get headerColor() {
        var headerClr = 'color: ' +this.headerTextColor;
        return headerClr;
    }

     get bodyColor() {
        var bodyClr = 'color: ' +this.bodyTextColor;
        return bodyClr;
    }

    toggleExpandCollapse() {
        this.loadOpenAccordion = !this.loadOpenAccordion;
        const toggleEvent = new CustomEvent('toggle', {detail: this._loadOpenAccordion});
        this.dispatchEvent(toggleEvent);
        this.setCookieState();
    }

    setControlledStyling() {
        if(this._loadOpenAccordion) {
            this.displayControlledStyling = SHOWN;
        } else {
            this.displayControlledStyling = HIDDEN;
        }
    }

    get paddingClasses() {
        if(this.disablePadding) {
            return PADDING_OFF;
        }
        return PADDING_ON;
    }

}