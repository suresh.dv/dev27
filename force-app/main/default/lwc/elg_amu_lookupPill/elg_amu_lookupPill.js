/**
 * Created by MarosGrajcar on 3/4/2020.
 */

import { LightningElement, api } from 'lwc';

export default class ElgAmuLookupPill extends LightningElement {

    @api isRequired;
    @api label;
    @api selectedText;

    @api iconName = 'standard:service_territory_location';
    @api renderIcon = false;

    clearSelection() {
        const evt = new CustomEvent('clearselection');
        this.dispatchEvent(evt);
    }
}