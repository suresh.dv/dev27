/**
 * Created by MarosGrajcar on 2/12/2020.
 */

import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class EqtSystemInformationSection extends NavigationMixin(LightningElement) {
    @api equipmentForm;

    /* NAVIGATION */
    navigateToUser(event) {
    var userId = event.currentTarget.dataset.userid;

    this[NavigationMixin.Navigate]({
        type: 'standard__recordPage',
        attributes: {
            objectApiName: 'User',
            actionName: 'view',
            recordId: userId
        },
        });
    }
}