/**
 * Created by MarosGrajcar on 12/12/2019.
 */

import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import somethingMore from '@salesforce/apex/Equipment_Forms_Endpoint.somethingMore';


export default class EqtTableMastersView extends NavigationMixin(LightningElement) {

    @api equipment;
    @api recordId;

    @track loading = false;
    @track isExposed = false;
    @track childEquipments = [];

    openChildEquipments(event) {
        var eqId = event.target.dataset.equip;
        this.loading = true;

        somethingMore({equipmentId : eqId, flockId : this.recordId}).then(result => {
            this.childEquipments = result;
            this.isExposed = true;
            //this.prepareMapForHTML();
        }).catch(error => {
        }).finally(() => {
            this.loading = false;
        });
    }

    closeChildEquipments() {
        this.isExposed = false;
    }

    handleOpenEquipmentView(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'Equipment__c',
                recordId: event.target.dataset.id,
                actionName: 'view',
            },
        });
    }
}