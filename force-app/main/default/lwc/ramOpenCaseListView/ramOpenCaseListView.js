import { LightningElement, wire, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import ACCOUNT_NAME from '@salesforce/schema/User.Contact.Account.Name';
import ACCOUNT_ID from '@salesforce/schema/User.Contact.AccountId';
import USER_ID from '@salesforce/user/Id';

// import getRaMOpenCases from '@salesforce/apex/RaMPortalController.getRaMOpenCases';
import myRaMOpenCases from '@salesforce/apex/RaMPortalController.getRaMOpenCases';
import caseAssign from '@salesforce/apex/RaMPortalController.assignCaseToTechnician';
import assignmentUsersByCase from '@salesforce/apex/RaMPortalController.getAssignmentUsersByCase';

const actions = [
  {label: 'Show Details', name: 'show_details'},
  {label: 'Assign', name: 'assign_case'}
];

const columns = [
  { label: 'Ticket #', fieldName: 'CaseDetailView', sortable: "true", type: 'url',
      typeAttributes: { label:{fieldName:'Ticket_Number_R_M__c'}, target: '_self' } },  
  { label: 'Location #', fieldName: 'LocationName', sortable: "true" },
  { label: 'Priority', fieldName: 'Priority', sortable: "true" },
  { label: 'Summary', fieldName: 'Subject', sortable: "true" },
  { label: 'Equipment Type', fieldName: 'Equipment_Type__c', sortable: "true" },
  { label: 'Equipment Class', fieldName: 'Equipment_Class__c', sortable: "true" },
  { label: 'Status', fieldName: 'Status', sortable: "true" },
  { label: 'Opened Date/Time', fieldName: 'CreatedDate', sortable: "true", type: 'date',
      typeAttributes:{ year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" } },
  {
    type: 'action',
    typeAttributes: { rowActions: actions },
  },       
];

export default class RamOpenCaseListView extends NavigationMixin(LightningElement) {
    @track records;
    @track columns = columns;
    @track sortBy;
    @track sortDirection;
    @track selectedRow ={};
    @track options = [];
    @track selectedUsers = [];
    @track showAssignmentModal = false;
    @track selectedTechnicians = [];
    @track assignButtonLabel = 'Assign';
    @track isUpdateDisabled = false;

    MIN_SELECTION = 1;

    @wire(getRecord, { recordId: USER_ID, fields: [ACCOUNT_NAME, ACCOUNT_ID] }) 
    wiredUser;  
    
    get accountName() {
      return getFieldValue(this.wiredUser.data, ACCOUNT_NAME);
    } 

    get accountId() {
      return getFieldValue(this.wiredUser.data, ACCOUNT_ID);
    }   

    connectedCallback() {
      this.showMyCases();
    }

    showMyCases() {
      myRaMOpenCases().then(data => {
        const rows = [];
        
        data.forEach(function(record) {
          const caseObj = Object.assign({}, record, {
            CaseDetailView: '/ram-case-detail?caseNumber=' + record.CaseNumber,
            LocationName: record.LocationNumber__r.Name,
          });
          rows.push(caseObj);
        });

        this.records = rows;
        this.error = undefined;
      }).
      catch(error => {
        console.log(error);
        this.error = error;
        this.records = undefined;
      })      
    }
    
    handleRowAction(event) {
      const actionName = event.detail.action.name;
      this.selectedRow = event.detail.row;

      switch(actionName) {
        case 'show_details':
          this.navigateDetailPage();
          break;
        case 'assign_case':
          this.selectedTechnicians = [];          
          this.openAssignmentModal();
          break;
        default:
      }
    }  
    
    navigateDetailPage() {
      this[NavigationMixin.Navigate]({
        type: 'comm__namedPage',
        attributes: {
          pageName: 'ram-case-detail'
          
        },
        state: {caseNumber: this.selectedRow.CaseNumber}
      });
    } 

    async handleAssignment() {
      if(this.selectedTechnicians.length == 0) {
        return;
      }

      this.disbleAssignButton();

      let assignmentInfo = {
        recordId: this.selectedRow.Id,
        technicianIds: this.selectedTechnicians,
        subject: this.selectedRow.Subject,
        ticketNumber: this.selectedRow.Ticket_Number_R_M__c
      };
      
      try{
        await caseAssign(assignmentInfo);
        this.closeAssignmentModal();
        this.showSuccessToast('Ticket ' + this.selectedRow.Ticket_Number_R_M__c + ' assigned successfully');

      } catch(error) {
        this.showErrorToast('Error assigning ticket ' + this.selectedRow.Ticket_Number_R_M__c);

      } finally {
        this.enableAssignButton();
      }
    }

    handleAssignmentChange(event) {
      this.selectedTechnicians = event.detail.value;
    }
    
    get min() {
      return this.MIN_SELECTION;
    }

    openAssignmentModal() {
      this.options = [];
      this.selectedUsers = [];

      assignmentUsersByCase({
        caseRecordId: this.selectedRow.Id
      }).then(data => {
        const assigned = [];
        const unassigned = [];

        data.forEach(function(record) {
          assigned.push({
            label: record.userName,
            value: record.userId              
          });

          if(record.hasAssignment) {
            unassigned.push(record.userId);
          } 
        });

        this.error = undefined;
        this.options.push(...assigned);   
        this.selectedUsers.push(...unassigned);    

      }).
      catch(error => {
        this.showErrorToast('Error while loading RaM Case assignment data');
      })

      this.showAssignmentModal = true;
    }
  
    closeAssignmentModal() {
      this.showAssignmentModal = false;
    } 
    
    disbleAssignButton() {
      this.isUpdateDisabled = true;
      this.assignButtonLabel = 'Generating Assignments';
    }

    enableAssignButton() {
      this.isUpdateDisabled = false;
      this.assignButtonLabel = 'Assign';
    }    

    handleSortdata(event) {
      // field name
      this.sortBy = event.detail.fieldName;

      // sort direction
      this.sortDirection = event.detail.sortDirection;

      // calling sortdata function to sort the data based on direction and selected field
      this.sortData(event.detail.fieldName, event.detail.sortDirection);
  }

  sortData(fieldname, direction) {
      // serialize the data before calling sort function
      let parseData = JSON.parse(JSON.stringify(this.records));

      // Return the value stored in the field
      let keyValue = (a) => {
          return a[fieldname];
      };

      // cheking reverse direction 
      let isReverse = direction === 'asc' ? 1: -1;

      // sorting data 
      parseData.sort((x, y) => {
          x = keyValue(x) ? keyValue(x) : ''; // handling null values
          y = keyValue(y) ? keyValue(y) : '';

          // sorting values based on direction
          return isReverse * ((x > y) - (y > x));
      });

      // set the sorted data to data table data
      this.records = parseData;

  }

  showSuccessToast(successMessage) {
    const event = new ShowToastEvent({
      variant: 'success',
      message: successMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }

  showErrorToast(errorMessage) {
    const event = new ShowToastEvent({
      variant: 'error',
      message: errorMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }    

}