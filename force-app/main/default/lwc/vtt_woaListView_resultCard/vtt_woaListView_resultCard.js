/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 28/08/2019 - Created.
*************************************************************************************************/
const HIDE_SEARCH_BUTTON_NAME = 'Hide search';
const SHOW_SEARCH_BUTTON_NAME = 'Show search';
const TRADESMAN_STARTED_WORK_STATUS = 'Started Work';
const PAGE_SIZE = 20;
const ORDER_OPTIONS = [
        {value: 'Requested_Start_Time__c ASC', label: 'Basic Start Date ASC'},
        {value: 'Requested_Start_Time__c DESC', label: 'Basic Start Date DESC'},
        {value: 'Work_Order_Number__c ASC', label: 'Work Order Number ASC'},
        {value: 'Work_Order_Number__c DESC', label: 'Work Order Number DESC'},
        {value: 'Order_Description__c ASC', label: 'Order Description ASC'},
        {value: 'Order_Description__c DESC', label: 'Order Description DESC'},
        {value: 'Main_Work_Centre__c ASC', label: 'Main Work Center ASC'},
        {value: 'Main_Work_Centre__c DESC', label: 'Main Work Center DESC'},
        {value: 'Order_Type__c ASC', label: 'Order Type ASC'},
        {value: 'Order_Type__c DESC', label: 'Order Type DESC'},
        {value: 'User_Status_Code__c ASC', label: 'User Status Code ASC'},
        {value: 'User_Status_Code__c DESC', label: 'User Status Code DESC'},
        {value: 'Plant_Section__c ASC', label: 'Plant Section ASC'},
        {value: 'Plant_Section__c DESC', label: 'Plant Section DESC'}
];
const DEFAULT_ORDER = 'Requested_Start_Time__c ASC';

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getRecords from '@salesforce/apex/VTT_WOA_ListView_Endpoint.getRecordsByFilter';
import autoAssign from '@salesforce/apex/VTT_WOA_ListView_Endpoint.runAutoAssignmentOnWOAs';
import getUserFlags from '@salesforce/apex/VTT_Utility_Endpoint.getTradesmanInfo';

export default class VTT_WOAListViewResultCard extends NavigationMixin(LightningElement) {
//VARS
    @track loading = false;
    @track additionalLoading = false;
    @track processing = false;
    @track records = []; //HOG_Maintenance_Servicing_Form__c[]
    @track errorMessage = undefined;
    filter; //VTT_WOA_SearchCriteria
    nextOffset = 0;
    @track infiniteLoad = true;
    @track tradesmanInfo; //VTT_TradesmanFlags

    @track showManageAssignments = false;
    _selectedRecords = []; //Work_Order_Activity__c[]
    @track selectedIds = []; //Id[]
    @track showStartWorkPopup = false;

    @track order = DEFAULT_ORDER;
    @track orderOptions = ORDER_OPTIONS;

    @track reloadAvailable = false;

    @track searchToggleButtonName = HIDE_SEARCH_BUTTON_NAME;

//CONSTRUCTOR
    constructor() {
        super();
        this.loading = true;
        getUserFlags()
            .then(result => {
                this.tradesmanInfo = result;
                this.loading = false;
            })
            .catch(error => {
                this.loading = false;
            });
    }

//APIs
    @api searchToggled() {
        this.toggleToggleButtonName();
    }

    @api runSearch(filter) {
        this.nextOffset = 0;
        this.loading = true;
        this.infiniteLoad = true;
        this.filter = filter;
        this.doSearch(false, false);
    }

//HANDLERS/ACTIONS
    reload() {
        this.loading = true;
        this.infiniteLoad = true;
        this.doSearch(false, true);
    }

    handleOrderChange(evt) {
        this.order = evt.detail.value;
        this.reload();
    }

    loadMore() {
        this.additionalLoading = true;
        this.doSearch(true, false);
    }

    toggleSearch() {
        const toggleSearchEvent = new CustomEvent('togglesearch');
        this.dispatchEvent(toggleSearchEvent);
        this.toggleToggleButtonName();
    }

    toggleToggleButtonName() {
        if(this.searchToggleButtonName === HIDE_SEARCH_BUTTON_NAME) {
            this.searchToggleButtonName = SHOW_SEARCH_BUTTON_NAME;
        } else {
            this.searchToggleButtonName = HIDE_SEARCH_BUTTON_NAME;
        }
    }

    handleExpand() {
        this.toggleAccordions(true);
    }

    handleCollapse() {
        this.toggleAccordions(false);
    }

    handleStartWork() {
        this.showStartWorkPopup = true;
    }

    handleStartWorkNo() {
        this.showStartWorkPopup = false;
    }

    handleStartWorkYes(evt) {
        this.showStartWorkPopup = false;
        this.tradesmanInfo.tradesman = evt.detail;
    }

    handleJobInProgress() {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.tradesmanInfo.currentWorkOrderActivityId,
                objectApiName: 'Work_Order_Activity__c',
                actionName: 'view'
            }
        });
    }

    handleManageAssignments() {
        this.gatherSelection();
        this.showManageAssignments = true;
    }

    handleManageAssignmentsClose() {
        this.clearSelection();
        this.showManageAssignments = false;
    }

    handleManageAssignmentsSuccess() {
        this.reload();
    }

    runAutoAssignments() {
        this.loading = true;
        let selectedRecords = this.collectSelectedActivities();
        autoAssign({activities: selectedRecords})
                .then(result => {
                    this.reload();
                })
                .catch(error => {
                    const event = new ShowToastEvent({
                        title: 'Error',
                        type: 'error',
                        message: error.body.message
                    });
                    this.dispatchEvent(event);
                    this.loading = false;
                });
    }

//HELPERS
    doSearch(append, doReload) {
        let params = this.getSearchParams(doReload);
        this.reloadAvailable = true;
        getRecords({filterString : JSON.stringify(this.filter), sorting: this.order, offset: params.offset, queryLimit: params.pageLimit})
                .then(result => {
                    this.handleSuccessSearch(result, append);
                })
                .catch(error => {
                    this.handleErrorSearch(error);
                });
    }

    getSearchParams(doReload) {
        let offset = this.nextOffset;
        let pageLimit = PAGE_SIZE;

        if(doReload) {
            offset = 0;
            pageLimit = this.nextOffset;
        }

        return {
            offset: offset,
            pageLimit : pageLimit
        };
    }

    handleSuccessSearch(result, append) {
        let context = this;
        new Promise(function(resolve, reject) {
            if(append) {
                context.records.push.apply(context.records, result);
            } else {
                context.records = result;
            }
            context.errorMessage = undefined;
            context.loading = false;
            if(result === undefined || result.length == 0 || result.length < PAGE_SIZE) {
                context.infiniteLoad = false;
            }
            context.additionalLoading = false;
            resolve();
        }).then(() => context.finishLoading(context));
    }

    finishLoading(context) {
        context.nextOffset = context.nextOffset + PAGE_SIZE;
    }

    handleErrorSearch(error) {
        this.records = [];
        this.errorMessage = error.body.message;
        this.loading = false;
        this.additionalLoading = false;
    }

    recordsHasElements() {
        return this.records !== undefined
                && this.records.length > 0;
    }

    toggleAccordions(toggleFlag) {
        this.processing = true;
        var context = this;
        new Promise(function(resolve, reject) {
            setTimeout(() => {
                context.toggleActivityLists(toggleFlag);
                resolve(context);
            }, 250);
        }).then(function(context) {
            context.processing = false;
        });
    }

    toggleActivityLists(expand) {
        const resultItems = this.template.querySelectorAll('c-vtt_woa-list-view_result-item');
        if(resultItems) {
            for(let i = 0; i < resultItems.length; i++) {
                if(expand){
                    resultItems[i].doExpand();
                } else {
                    resultItems[i].doCollapse();
                }
            }
        }
    }

    collectSelectedActivities() {
        let selectedActivities = [];
        const resultItems = this.template.querySelectorAll('c-vtt_woa-list-view_result-item');

        if(resultItems) {
            for(let i = 0; i < resultItems.length; i++) {
                selectedActivities.push.apply(selectedActivities, resultItems[i].getSelectedRows());
            }
        }

        return selectedActivities;
    }

    getSelectedIds() {
        let selectedIds = [];
        if(this._selectedRecords && this._selectedRecords.length > 0) {
            for(let i = 0; i < this._selectedRecords.length; i++) {
                selectedIds.push(this._selectedRecords[i].Id);
            }
        }
        return selectedIds;
    }

    gatherSelection() {
        this._selectedRecords = this.collectSelectedActivities();
        this.selectedIds = this.getSelectedIds();
    }

    clearSelection() {
        this._selectedRecords = [];
        this.selectedIds = [];
        const resultItems = this.template.querySelectorAll('c-vtt_woa-list-view_result-item');

        if(resultItems) {
            for(let i = 0; i < resultItems.length; i++) {
                resultItems[i].clearSelection();
            }
        }

    }

//GETTERS
    get isAvailableStartWork() {
        return this.recordsHasElements()
                &&  this.tradesmanInfo.tradesman.Tradesman_Start_Work_Enabled__c;
    }

    get cannotStartWork() {
        return !(this.tradesmanInfo.tradesman.Current_Work_Order_Activity__c == null
               && this.tradesmanInfo.tradesman.Tradesman_Status__c !== TRADESMAN_STARTED_WORK_STATUS
               && this.tradesmanInfo.tradesman.Tradesman_Still_Working__c == false);
    }

    get recordCount() {
        return this.records.length;
    }

    get showTable() {
        return this.records !== undefined && this.records.length > 0;
    }

    get allRecordsLoaded() {
        return this.records.length % PAGE_SIZE > 0 && !this.infiniteLoad;
    }

    get isAvailableJobInProgress() {
        return this.recordsHasElements()
                && this.tradesmanInfo.currentWorkOrderActivityId !== undefined;
    }

    get isAvailableManageAssignments() {
        return this.recordsHasElements()
                && (
                    this.tradesmanInfo.isAdmin
                    || this.tradesmanInfo.isVendorSupervisor
                );
    }

    get isAvailableRunAutoAssignments() {
        return this.recordsHasElements()
                && this.tradesmanInfo.isAdmin;
    }

    get possibleAdditionalLoading() {
        return !this.additionalLoading && this.infiniteLoad;
    }

}