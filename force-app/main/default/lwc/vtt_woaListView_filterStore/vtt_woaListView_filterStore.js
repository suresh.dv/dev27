/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
const FILTER_COOKIE_NAME = 'VTT_WOAListViewFilter_CookieFilter';

import { LightningElement, track, api } from 'lwc';
import getStoredFilters from '@salesforce/apex/VTT_WOA_SearchCriteria_Endpoint.getStoredFiltersForCurrentUser';
import deleteSelectedFilter from '@salesforce/apex/VTT_WOA_SearchCriteria_Endpoint.deleteFilter';
import setLastSelectedFilter from '@salesforce/apex/VTT_WOA_SearchCriteria_Endpoint.setLastSelectedFilter';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import userId from '@salesforce/user/Id';

export default class VTT_WOAListViewFilterStore extends LightningElement {
//VARS
    @track loading = false;
    @track filterOptions; //{
                          //    value: storedFilter.Id,
                          //    label: storedFilter.Assigned_Name__c
                          //};
    storedFilters; //Work_Order_Activity_Search_Criteria__c[]
    @track errorMessage = undefined;

    @track selectedFilter; //Id

    cookieFilter = null;

//CONSTRUCTOR
    constructor() {
        super();
        this.cookieFilter = this.getCookie(FILTER_COOKIE_NAME);
        this.loadStoredFilters();
    }

//    APIs
    @api selectFilter(filterId) {
        this.cookieFilter = undefined;
        this.selectedFilter = filterId;
        return this.getSelectedFilter();
    }

    @api getSelectedFilter() {
        for(let i = 0; this.storedFilters.length > i; i++) {
            if(this.storedFilters[i].Id === this.selectedFilter) {
                return this.storedFilters[i];
            }
        }
        return undefined;
    }

    @api reload() {
        this.loading = true;
        getStoredFilters()
                .then(result => {
                    this.handleSuccess(result);
                })
                .catch(error => {
                    this.handleError(error);
                });
    }

//HANDLERS/ACTIONS
    handleSuccess(result) {
        this.errorMessage = undefined;
        this.storedFilters = result;
        this.buildFilterOptions();
        if(!this.cookieFilter) {
            this.fireSelectionEvent();
        }
        this.loading = false;
   }

    handleError(error, showOnlyToast) {
        if(showOnlyToast) {
            this.errorMessage = error;
        }
        this.loading = false;
        this.showToast();
    }

    handleSelection(evt) {
        this.loading = true;
        this.selectedFilter = evt.detail.value;
        this.saveLastSelection();
        this.fireSelectionEvent();
    }

    saveLastSelection() {
        setLastSelectedFilter({filterId : this.selectedFilter})
                .then(result => {
                    this.handleSuccess(result);
                })
                .catch(error => {
                    this.handleError(error);
                });
    }

    fireSelectionEvent() {
        const selectionEvent = new CustomEvent('selection', {detail: this.selectedFilter});
        this.dispatchEvent(selectionEvent);
    }

    handleDelete() {
        if(this.selectedFilter) {
            this.loading = true;
            deleteSelectedFilter({filterId : this.selectedFilter})
                    .then(result => {
                        this.showSuccessToast('Filter was successfully deleted.')
                        this.loadStoredFilters();
                        this.selectedFilter = undefined;
                    })
                    .catch(error => {
                        this.handleError(error, true);
                    });
        }
    }

//HELPERS
    loadStoredFilters() {
        this.loading = true;
        getStoredFilters()
                .then(result => {
                    this.handleSuccess(result);
                })
                .catch(error => {
                    this.handleError(error);
                });
    }

    buildFilterOptions() {
        this.filterOptions = [];
        if(this.storedFilters !== undefined && this.storedFilters.length > 0) {
            for(let i = 0; this.storedFilters.length > i; i++) {
                this.appendFilterAndGetLastSelectedFilter(this.storedFilters[i]);
            }
        }
    }

    appendFilterAndGetLastSelectedFilter(filterItem) {
        let filter = {
            value: filterItem.Id,
            label: filterItem.Assigned_Name__c
        };
        this.filterOptions.push(filter);
        if((!this.cookieFilter || this.isFilterSame(this.cookieFilter, filterItem.Filter_String__c)) && filterItem.Last_Search_Criteria_Selected__c) {
            this.sendLastSelectedFilter(filterItem);
            this.selectedFilter = filterItem.Id;
        }
    }

    isFilterSame(cookieFilter, sfFilter) {
        let ckFilterObj = JSON.parse(cookieFilter);
        let sfFilterObj = JSON.parse(sfFilter);

        return ckFilterObj.scheduledFromFilter === sfFilterObj.scheduledFromFilter
               && ckFilterObj.scheduledToFilter === sfFilterObj.scheduledToFilter
               && ckFilterObj.routeFilter === sfFilterObj.routeFilter
               && ckFilterObj.filterName === sfFilterObj.filterName
               && ckFilterObj.hideCompletedFilter === sfFilterObj.hideCompletedFilter
               && ckFilterObj.amuFilter === sfFilterObj.amuFilter
               && ckFilterObj.vendorFilter === sfFilterObj.vendorFilter
               && ckFilterObj.tradesmanFilter === sfFilterObj.tradesmanFilter
               && ckFilterObj.plannerGroupFilter === sfFilterObj.plannerGroupFilter
               && ckFilterObj.activityMultiStatusFilter === sfFilterObj.activityMultiStatusFilter;
    }

    showToast() {
        const event = new ShowToastEvent({
            title: 'Error',
            type: 'error',
            message: this.errorMessage
        });
        this.dispatchEvent(event);
    }

    showSuccessToast(msg) {
        const event = new ShowToastEvent({
            title: 'Success',
            type: 'success',
            message: msg
        });
        this.dispatchEvent(event);
    }

    sendLastSelectedFilter(filter) {
        const lastFilterLoadEvt = new CustomEvent('lastfilterload', {detail: filter});
        this.dispatchEvent(lastFilterLoadEvt);
    }

    getCookie(name) {
        let cookieString = "; " + document.cookie;
        let parts = cookieString.split("; " + userId + name + "=");
        if (parts.length === 2) {
            return unescape(parts.pop().split(";").shift());
        }
        return null;
    }

//GETTERS
    get hasError() {
        return this.errorMessage !== undefined;
    }

}