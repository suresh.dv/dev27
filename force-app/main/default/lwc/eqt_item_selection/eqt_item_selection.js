/**
 * Created by MarosGrajcar on 11/25/2019.
 */

import { LightningElement, track, api } from 'lwc';
import loadEquipments from '@salesforce/apex/Equipment_Forms_Endpoint.loadEquipments';
import loadSelectedEquipments from '@salesforce/apex/Equipment_Forms_Endpoint.loadSelectedEquipments';
import getEquipmentForFromForm from '@salesforce/apex/Equipment_Forms_Endpoint.getEquipmentForFromForm';


export default class EqtItemSelection extends LightningElement {

    @track equipmentList = [];
    @track equipmentSelectedList = [];
    @track userSearch;
    @track selectedEquipment = [];
    @track loading;

    @api isFromForm;
    @api flocId;
    @api selectedTransferItems = [];

    equipmentWasRemoved = false;
    //@track selectionMade = false;


    /* CONSTRUCTOR */
    /*constructor() {
        super();
        if (isFromForm) {
            this.getAvailableEquipment();
        }
    }*/

    connectedCallback() {
        if (this.isFromForm) {
            this.loading = true;
            getEquipmentForFromForm({flocId : this.flocId, selectedTransferItems : this.selectedTransferItems}).then(result => {
                this.equipmentList = result;
                this.loading = false;
            }).catch(error => {
                console.log('Error on equipment item selection: ' +error);
                this.loading = false;
            });
        }
    }

     //Get list of AMUs
    getAvailableEquipment() {
        this.loading = true;
        loadEquipments({userInput : this.userSearch,
                        isFromForm : this.isFromForm,
                        flocId : this.flocId,
                        getInitialEquipment : this.equipmentWasRemoved,
                        selectedTransferItems : this.selectedTransferItems}).then(result => {
            this.equipmentWasRemoved = false;
            if (result.length > 0) {
                let context = this;
                var temporaryList = [];
                result.forEach(function (equipment) {
                    if (context.selectedEquipment.includes(equipment.Id)) {
                        //do nothing
                    } else {
                        temporaryList.push(equipment);
                        context.equipmentList = temporaryList;
                    }
                });
            }
            this.loading = false;
        }).catch(error => {
            console.log('Error on equipment item selection: ' +error);
            this.equipmentWasRemoved = false;
            this.loading = false;
        });
    }

    handleEquipmentSearch(event) {
        this.equipmentList = [];
        this.userSearch = event.target.value;
        if (this.userSearch == '' || this.userSearch == null) {
            this.equipmentList = [];
        } else {
            this.getAvailableEquipment();
        }
    }

    addSelectedEquipment(event) {
        var eqId = event.currentTarget.dataset.id;

        if (!this.equipmentList.includes(eqId)) {
            this.selectedEquipment.push(eqId);
        }

        this.preventDuplicates(eqId);
        this.getSelectedEquipment();
    }

    preventDuplicates(eqId) {
        var breakException = {};
        var equipmentObject;

         try {
            this.equipmentList.forEach(function (equipment) {
                if (equipment.Id == eqId) {
                    equipmentObject = equipment;
                    throw breakException;
                }
            });
        } catch (ex) {
            this.removeFromArray(this.equipmentList, equipmentObject);
        }

    }

      //Get list of AMUs
    getSelectedEquipment() {
        loadSelectedEquipments({selectedEquipments : this.selectedEquipment}).then(result => {
            this.equipmentSelectedList = result;
        }).catch(error => {
            console.log('Error on equipment item selection: ' +error);
        });
    }

    /*Create new objects for Transfer selected Equipment*/
    @api
    saveSelectedEquipment(event) {

        const equipmentEvent = new CustomEvent("selectedequipmentevent", {
          detail: this.equipmentSelectedList
        });
        this.dispatchEvent(equipmentEvent);

        // Dispatches the event.
    }

    removeSelectedEquipment(event) {
        var eqId = event.currentTarget.dataset.id;
        var breakException = {};

        var equipmentObject;

        if (this.equipmentSelectedList.length > 1) {
            try {
                this.equipmentSelectedList.forEach(function (equipment) {
                    if (equipment.Id == eqId) {
                        equipmentObject = equipment;
                        throw breakException;
                    }
                });
            } catch (ex) {
                this.removeFromArray(this.selectedEquipment, equipmentObject.Id);
                this.removeFromArray(this.equipmentSelectedList, equipmentObject);
            }
        } else {
            this.equipmentSelectedList = [];
            this.selectedEquipment = [];
        }

        this.equipmentWasRemoved = true;
        this.getAvailableEquipment();

    }


    removeFromArray(arrayToCheck) {
        var toRemove;
        var array = arguments;
        var arraySize = array.length;
        var indexOfTheValue;

        while (arraySize > 1 && arrayToCheck.length) {
            toRemove = array[--arraySize];
            while ((indexOfTheValue = arrayToCheck.indexOf(toRemove)) !== -1) {
                arrayToCheck.splice(indexOfTheValue, 1);
            }
        }

        return arrayToCheck;
    }

}