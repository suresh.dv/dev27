/**
 * Created by MarosGrajcar on 2/14/2020.
 */

import { LightningElement, api, track } from 'lwc';

export default class EqtCommentsSection extends LightningElement {

    @api form;
    @api isUserAdmin;
    @api isView;
    @api isNewForm = false;


    //todo lanch event to get change; pass form

    @track commentsObject = {
        'Comments__c' : '',
        'Reason__c' : '',
    };

    _originalComments;
    @api
    get originalComments() {
        return this._originalComments;
    }

    set originalComments(value) {
        this._originalComments = value;
        this.commentsObject.Comments__c = this.originalComments.Comments__c;
        this.commentsObject.Reason__c = this.originalComments.Reason__c;
    }

    handleComment(event) {
        this.commentsObject.Comments__c = event.target.value;
    }

    handleReason(event) {
        this.commentsObject.Reason__c = event.target.value;
    }

    @api
    getComments() {
        return this.commentsObject;
    }

}