import { LightningElement, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';

import USER_ID from '@salesforce/user/Id';
import ACCOUNT_NAME from '@salesforce/schema/User.Contact.Account.Name';

import getActivityDetail from '@salesforce/apex/RaMPortalController.getActivityDetails';
import setActivityStatus from '@salesforce/apex/RaMPortalController.updateActivityStatus';


export default class RamOpenTaskDetailView extends LightningElement {
  activityId;
  @track activityData;
  @track error;
  @track inProgressLbl = 'Work In Progress';
  @track pendingPartsLbl = 'Pending Parts';
  @track pendingApprovalLbl = 'Pending Approval';
  @track closedLbl = 'Resolved';
  @track inProgressDisabled = true;
  @track pendingPartsDisabled = true;
  @track pendingApprovalDisabled = true;
  @track closedDisabled = true;
  @track showCommentsModal = false;
  @track commentsModalLbl;
  @track isCommentsModalLblDisabled = false;
  @track comments = '';
  @track currentStep;

  @wire(getRecord, { recordId: USER_ID, fields: [ACCOUNT_NAME] }) 
  wiredAccount; 
  
  get accountName() {
    return getFieldValue(this.wiredAccount.data, ACCOUNT_NAME);
  } 
  
  connectedCallback() {
    this.activityId = this.getUrlParamValue(window.location.href, 'id');
    this.getActivityData();
  }

  getActivityData() {
    getActivityDetail({
      activityId: this.activityId,
      userId: USER_ID
    }).then(data => {
      this.activityData = data;
      this.error = undefined;

      this.setCurrentStep();
    }).
    catch(error => {
      this.activityData = undefined;
      this.error = error;  
    }) 
  }

  getUrlParamValue(url, key) {
    return new URL(url).searchParams.get(key);
  }   

  submitComments() {
    var getPosition = function (options) {
      return new Promise(function (resolve, reject) {
        navigator.geolocation.getCurrentPosition(resolve, reject, options);
      });
    }

    this.disableCommentsModalLbl();

    getPosition()
      .then((position) => {
        let currentPosition = {
          recordId: this.activityId,
          lat: position.coords.latitude,
          lon: position.coords.longitude,
          comments: this.comments,
          currentStatus: this.activityData.tkt.status,
          nextStatus: this.commentsModalLbl,
          errorCode: '',
          caseRecordId: this.activityData.tkt.caseId
        };

        this.updateActivity(currentPosition);
      })
      .catch((err) => {
        let currentPosition = {
          recordId: this.activityId,
          lat: undefined,
          lon: undefined,
          comments: this.comments,
          currentStatus: this.activityData.tkt.status,
          nextStatus: this.commentsModalLbl,
          errorCode: err.message,
          caseRecordId: this.activityData.tkt.caseId
        };

        this.updateActivity(currentPosition);
      })
  }

  async updateActivity(currentPosition) {

    try{
      await setActivityStatus(currentPosition);
      this.enableCommentsModalLbl();
      this.showSuccessToast('Activity updated successfully');
      this.getActivityData();

    }catch(error) {
      this.enableCommentsModalLbl();
      this.showErrorToast('Error updating activity');
      
    } finally {
      this.closeCommentsModal();
    }

  }

  handleCommentsChange(event) {
    this.comments = event.target.value;
  }

  handleUploadFinished(event) {
    alert('File upload');
    let strFileNames = '';
    const uploadedFiles = event.detail.files;

    for(let i = 0; i < uploadedFiles.length; i++) {
      strFileNames += uploadedFiles[i].name + ', ';
    }

    console.log(strFileNames);
  }

  setCurrentStep() {
    if(this.activityData.tkt.status === 'Assigned') {
      this.currentStep = 's0';
      this.enableWorkButtons();
    } else if(this.activityData.tkt.status === 'Work in Progress') {
      this.currentStep = 's1';
      this.enableWorkButtons();
    } else if(this.activityData.tkt.status === 'Pending Parts') {
      this.currentStep = 's2';
      this.enableWorkButtons();
    } else if(this.activityData.tkt.status === 'Pending Approval') {
      this.currentStep = 's3';
      this.enableWorkButtons();
    } else if(this.activityData.tkt.status === 'Resolved') {
      this.currentStep = 's4';
      this.inProgressDisabled = true;
      this.pendingPartsDisabled = true;
      this.pendingApprovalDisabled = true;
      this.closedDisabled = true;       
    } 
  }

  enableWorkButtons() {
    this.inProgressDisabled = false;
    this.pendingPartsDisabled = false;
    this.pendingApprovalDisabled = false;
    this.closedDisabled = false;    
  }

  startWork() {
    this.commentsModalLbl = this.inProgressLbl;
    this.openCommentsModal();
  }

  pendingParts() {
    this.commentsModalLbl = this.pendingPartsLbl;
    this.openCommentsModal();
  }

  pendingApproval() {
    this.commentsModalLbl = this.pendingApprovalLbl;
    this.openCommentsModal();
  }

  workCompleted() {
    this.commentsModalLbl = this.closedLbl;
    this.openCommentsModal();
  }

  openCommentsModal() {
    this.comments = '';
    this.showCommentsModal = true;
  }

  closeCommentsModal() {
    this.commentsModalLbl = '';
    this.showCommentsModal = false;
  } 

  enableCommentsModalLbl() {
    this.isCommentsModalLblDisabled = false;
  }

  disableCommentsModalLbl() {
    this.isCommentsModalLblDisabled = true;
  }  

  get acceptedFormats() {
    return ['.pdf', '.png','.jpg','.jpeg', '.gif', '.avi', '.mp4', '.mov', '.txt', '.doc', '.docx', '.xlsx', 'xls'];
  }  
  
  showSuccessToast(successMessage) {
    const event = new ShowToastEvent({
      variant: 'success',
      message: successMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }

  showErrorToast(errorMessage) {
    const event = new ShowToastEvent({
      variant: 'error',
      message: errorMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }     

}