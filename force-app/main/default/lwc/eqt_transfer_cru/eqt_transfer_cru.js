/**
 * Created by MarosGrajcar on 11/29/2019.
 */

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getPicklistValues from '@salesforce/apex/HOG_PicklistServiceLtng.getPicklist';
import getUserPermission from '@salesforce/apex/Equipment_Forms_Endpoint.getUserPermission';
import getExpirationDate from '@salesforce/apex/Equipment_Forms_Endpoint.getExpirationDate';
import loadTransferForm from '@salesforce/apex/Equipment_Forms_Endpoint.loadTransferForm';
import loadEquipmentTransferItems from '@salesforce/apex/Equipment_Forms_Endpoint.loadEquipmentTransferItems';
import insertTransferForm from '@salesforce/apex/Equipment_Forms_Endpoint.insertTransferFormEquipmentForm';
import updateTransferEquipmentForm from '@salesforce/apex/Equipment_Forms_Endpoint.updateTransferEquipmentForm';
import getFloc from '@salesforce/apex/Equipment_Forms_Endpoint.getFloc';

export default class EqtTransferCru extends NavigationMixin(LightningElement) {

    /* VARIABLES */

    @api isOnCommunity = false;
    @track _editModal = false;
    @api formId;

    @api
    get editModal() {
       return this._editModal;
    }

    set editModal(value) {
       this._editModal = value;
       if (this._editModal) {
           this.recordId = this.formId;
           /*this.checkUserPermission();
           this.loadForm();*/
       }
    }

    // FORM VARIABLES
    @track transferForm; // Equipment_Transfer_Form__c SObject
    @track transferReason;
    @track dateOfTransfer;
    @track authorizedBy;
    @track safetyCriticalEq;
    @track regulatoryEq;
    @track expirationDate;
    @track formStatus;
    @track workOrder;
    @track shippedVia;
    @track waybillNo;
    @track chargeNo;
    @track comment;
    @track reason;
    @track reasonForTransferPicklist; // get values from Equipment_Transfer_Form__c.Reason_for_Transfer_Additional_Details__c field Picklist
    @track tagColour;
    @track tagColourOptions;


     @track endpointForm = {
                "Id" : '',
                "Reason_for_Transfer_Additional_Details__c": '',
                "Date_of_Physical_Transfer__c": '',
                "Authorized_By__c": '',
                "Maintenance_Work_Order__c": '',
                "Shipped_Via__c": '',
                "Waybill_No__c": '',
                "Charge_No__c": '',
                "Regulatory_Equipment__c": '',
                "Safety_Critical_Equipment__c": '',
                "Expiration_Date__c": '',
                "Comments__c": '',
                "Reason__c": ''}

    // FLOC VARIABLES
    @track flocName;

    // STATE VARIABLES
    @track isView = false;
    @track isNewForm = true;
    @track isUserAdmin = false;
    @track loading = true;
    @track openModal = false;
    @api isFromForm = false; //set from Button click that FROM form has been selected

    // SELECTED EQUIPMENTs VARIABLES
    @track equipmentList = []; //list from child
    @track selectedEquipment = []; //need to iterate from child list to populate this list. for some reason it is not working without iteration
    equipmentForEndpoint = []; // pass equipments to endpoint to set up Equipment_Transfer_Items

    // COMBOBOX VARIABLES
    @track selectedDestination;
    @track transferDestination;
    @track isDestinationSelected = true;


    /* FILE VARIABLES */



    filesList = [];


    @track itemFile;
    @track itemFileName;
    itemFileContents;
    itemFileReader;
    itemContent;

    itemFilesToInsert = [];

    handleFileUploadOnEdit(event) {
        var equipmentId =  event.currentTarget.dataset.id;
        var index = event.currentTarget.dataset.index;

        this.itemFile = event.target.files;
        this.itemFileName = event.target.files[0].name;;
        this.itemFileReader = new FileReader();
        this.itemFileReader.readAsDataURL(this.itemFile[0]);
        this.itemFileReader.onloadend = (() => {
            this.itemFileContents = this.itemFileReader.result;
            let base64 = 'base64,';
            this.itemContent = this.itemFileContents.indexOf(base64) + base64.length;
            this.itemFileContents = this.itemFileContents.substring(this.itemContent);
        });

        this.selectedEquipment[index].fileName = this.itemFileName;

        var tmp = this.objectBuilder(this.selectedEquipment[index].item, null, null, null, null);
        tmp.itemFile.fileName = this.itemFileName;
        tmp.itemFile.fileContent = encodeURIComponent(this.itemFileContents);

        if (this.itemFilesToInsert.length > 0) {
            var equipmentObject;
            let context = this;
            this.itemFilesToInsert.forEach(function(value) {
                if (value.equipment.Id == equipmentId) {
                    equipmentObject = value;
                    removeFromArray(context.itemFilesToInsert, value);
                    context.itemFilesToInsert.push(tmp);
                } else {
                    context.itemFilesToInsert.push(tmp);
                }
            });
        } else {
            this.itemFilesToInsert.push(tmp);
        }
    }


    handleFileUpload(event) {
        var equipmentId =  event.currentTarget.dataset.id;
        var index = event.currentTarget.dataset.index;

        this.itemFile = event.target.files;
        this.itemFileName = event.target.files[0].name;;
        this.itemFileReader = new FileReader();
        this.itemFileReader.readAsDataURL(this.itemFile[0]);
        this.itemFileReader.onloadend = (() => {
            this.itemFileContents = this.itemFileReader.result;
            let base64 = 'base64,';
            this.itemContent = this.itemFileContents.indexOf(base64) + base64.length;
            this.itemFileContents = this.itemFileContents.substring(this.itemContent);
        });
        this.equipmentForTransfer[index].itemFile.fileName = this.itemFileName;
        this.equipmentForTransfer[index].itemFile.fileContent = encodeURIComponent(this.itemFileContents);
    }

    removeSelectedEquipment(event) {

        var eqId = event.currentTarget.dataset.id;
        var breakException = {};

        var equipmentObject;

        if (this.equipmentForTransfer.length > 1) {
            try {
                this.equipmentForTransfer.forEach(function (equipment) {
                    if (equipment.equipment.Id == eqId) {
                        equipmentObject = equipment;
                        throw breakException;
                    }
                });
            } catch (ex) {
                this.removeFromArray(this.equipmentForTransfer, equipmentObject);
            }
        } else {
            this.equipmentForTransfer = [];
        }

        this.removeFromArray(this.temporaryEquipmentIds, eqId);

    }


    removeFromArray(arrayToCheck) {
        var toRemove;
        var array = arguments;
        var arraySize = array.length;
        var indexOfTheValue;

        while (arraySize > 1 && arrayToCheck.length) {
            toRemove = array[--arraySize];
            while ((indexOfTheValue = arrayToCheck.indexOf(toRemove)) !== -1) {
                arrayToCheck.splice(indexOfTheValue, 1);
            }
        }

        return arrayToCheck;
    }

    get transferDestinationOptions() {
        return [
            { label: 'Location', value: 'Location__c' },
            { label: 'Facility', value: 'Facility__c' },
            { label: 'Well Event', value: 'Well_Event__c' },
            { label: 'System', value: 'System__c' },
            { label: 'Sub-System', value: 'Sub_System__c' },
            { label: 'Functional Equipment Level', value: 'Functional_Equipment_Level__c' },
            { label: 'Yard', value: 'Yard__c' },
        ];
    }

    /* GETTERS SETTERS */

    // gets id of the created form
    @track _recordId;
    @api
    get recordId() {
        return this._recordId;
    }

    set recordId(value) {
        this._recordId = value;
        if (this._recordId) {
            this.checkUserPermission();
            this.loadForm();
            let context = this;
            this.pickListValues('Equipment_Transfer_Form__c', 'Reason_for_Transfer_Additional_Details__c', function (value) {
                context.reasonForTransferPicklist = value;
            });
            this.pickListValues('Equipment_Transfer_Item__c', 'Tag_Colour__c', function (value) {
                context.tagColourOptions = value;
            });
        } else {
            this.loadEquipment();
            this.setExpirationDate();
        }
    }

    // passed value from FLOC
    _flockId
    @api
    get flockId() {
      return this._flockId;
    }

    set flockId(value) {
        this._flockId = value;
        let context = this;
                this.pickListValues('Equipment_Transfer_Form__c', 'Reason_for_Transfer_Additional_Details__c', function (value) {
                    context.reasonForTransferPicklist = value;
                });
                this.pickListValues('Equipment_Transfer_Item__c', 'Tag_Colour__c', function (value) {
                    context.tagColourOptions = value;
                });
        this.setExpirationDate();
        this.getFlocName();
    }

    /* ENDPOINTS (CALLBACKS) */

    // gets existing form data

    @track toAPIName;
    @track toFlocId;
    @track toFloc;
    @track fromAPIName;
    @track fromFlocId;
    @track formClosed = false;
    loadForm() {
         this.itemsToRemove = [];
         this.selectedEquipment = [];
         this.filesIdsToRemove = [];
         this.attachmentIdsToRemove = [];
         this.equipmentForTransfer = [];
         this.itemFilesToInsert = [];
        loadTransferForm({formId : this.recordId}).then(result => {
            //this.returnedMessage = result;
            this.transferForm = result.transferForm;
             if (this.formIsClosed()) {
                this.formClosed = true;
            }
            this.isFromForm = result.flocInfo.isFromForm;
            this.selectedDestination = result.flocInfo.toAPIName;
            this.toAPIName = result.flocInfo.toAPIName;
            this.toFlocId = result.flocInfo.toFlocId;
            this.toFloc = result.flocInfo.toFloc;
            this.transferDestination = result.flocInfo.toFloc.Id;

            if (this.isFromForm) {
                this.fromAPIName = result.flocInfo.fromAPIName;
                this.fromFlocId = result.flocInfo.fromFlocId;
                this.flockId = this.fromFlocId;
            }

            this.loadItems();
            //this.setupForm();
        }).catch(error => {
           console.log('Error on load transfer form' + error);
            this.loading = false;
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

     formIsClosed() {
        if (this.transferForm.Status__c == 'Auto Closed'
        || this.transferForm.Status__c == 'Rejected Closed'
        || this.transferForm.Status__c == 'Processed') {
            return true;
        } else {
            return false;
        }
    }


    // gets Equipment_Transfer_Items for existing form
     loadItems() {
        loadEquipmentTransferItems({formId : this.recordId}).then(result => {
            //this.returnedMessage = result;
            this.selectedEquipment = result;
            //this.setupForm();
            this.isView = true;
            this.isNewForm = false;
            this.loading = false;
            if (this.editModal) {
                 this.editView();
            }
        }).catch(error => {
           console.log('Error on load transfer form' + error);
            this.loading = false;
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

    // call from parent cmp due to modal window
    @api
    insertForm() {
            if (this.equipmentForTransfer.length > 0 && this.validateFormBeforeSave()) {
                this.loading = true;
                insertTransferForm({fromSource : this.flockId,
                                    toDestination : this.transferDestination,
                                    transferForm : this.endpointForm,
                                    itemTransfer : this.equipmentForTransfer}).then(result => {
                    this.showToast('', 'Equipment Transfer Form has been successfully created.', 'success');
                    //eval("$A.get('e.force:refreshView').fire();");
                    this.dispatchEvent(new CustomEvent('closemodal'));
                }).catch(error => {
                    var message;
                    if (error.body) {
                        message = error.body.message;
                    } else {
                        message = error.message;
                    }
                    this.dispatchEvent(
                        new ShowToastEvent({
                        title: '',
                        message: message,
                        variant: 'error',
                        }),
                    );
                    this.dispatchEvent(new CustomEvent('enablebuttons'));
                    this.loading = false;
                });
            } else {
                if (this.equipmentForTransfer.length  < 1) {
                    this.showToast('', 'Please, select at least one equipment for transfer.', 'warning');
                } else {
                    this.showToast('', 'Please, fill the required fields before saving the form.', 'warning');
                }
                this.dispatchEvent(new CustomEvent('enablebuttons'));
            }

    }

    // params:
    // Set Ids co treba zmazat za items, Set Ids co treba zmazat za files || attachments
    // Object co treba insertnut, poslat cele srant na to a updanut cely object

    @api
    updateForm() {
            if ((this.selectedEquipment.length > 0 || this.equipmentForTransfer.length > 0) && this.validateFormBeforeSave()) {
               this.loading = true;

                updateTransferEquipmentForm({transferForm : this.endpointForm,
                                            isFromForm : this.isFromForm,
                                            destination : this.transferDestination,
                                            itemsToRemove : this.itemsToRemove,
                                            equipmentItems : this.selectedEquipment,
                                            filesToRemove : this.filesIdsToRemove,
                                            attachmentsToRemove : this.attachmentIdsToRemove,
                                            itemTransfer : this.equipmentForTransfer,
                                            itemFilesToInsert : this.itemFilesToInsert}).then(result => {
                    this.showToast('', 'Equipment Transfer Form has been successfully updated.', 'success');
                    this.isView = true;
                    this.loadForm();
                    if (this.editModal) {
                        this.dispatchEvent(new CustomEvent('closemodal'));
                    }
                }).catch(error => {
                    let errorMsg;
                    if(error.body) {
                        errorMsg = error.body.message;
                    } else {
                        errorMsg = error.message;
                    }
                    this.showToast('', errorMsg, 'error');
                    this.loading = false;
                    this.dispatchEvent(new CustomEvent('enablebuttons'));
                });
            } else {
               if (this.selectedEquipment.length  < 1 && this.equipmentForTransfer < 1) {
                   this.showToast('', 'Please, select at least one equipment for transfer.', 'warning');
               } else {
                   this.showToast('', 'Please, fill the required fields before saving the form.', 'warning');
               }
                this.dispatchEvent(new CustomEvent('enablebuttons'));
            }

    }

     /* FUNCTIONS */
    showToast(title, message, toastType) {
        const toastEvent = new ShowToastEvent({title : title, message : message, variant : toastType});
        this.dispatchEvent(toastEvent);
    }

    validateFormBeforeSave() {
    var destinationFilled = true;

    if (this.isFromForm) {
        if (this.transferDestination == null) {
            destinationFilled = false;
        } else {
            destinationFilled = true;
        }
    }

    return this.endpointForm.Reason_for_Transfer_Additional_Details__c != ''
            && this.endpointForm.Date_of_Physical_Transfer__c != ''
            && this.endpointForm.Authorized_By__c != ''
            && destinationFilled
            && this.validateTagColour();
    }

    validateTagColour() {
        var checkTagColour = true;

        if (this.equipmentForTransfer != null) {
            this.equipmentForTransfer.forEach(function (equipment) {
                if (equipment.tagColour == null) {
                   checkTagColour = false;
                }
            });
        }
        return checkTagColour;
    }

    // set up FLOC name for the new form
    //@track flocAPIName;
    getFlocName() {
        getFloc({flockId : this.flockId}).then(result => {
            this.flocAPIName = result.flocAPI;
            this.flocName = result.floc.Name;
            this.fromFlocId = result.floc.Id;
        }).catch(error => {
            console.log('Error on load add form' + error);
            this.dispatchEvent(
                new ShowToastEvent({
                title: '',
                message: error.message,
                variant: 'error',
                }),
            );
        }).finally(() => {
            this.loading = false;
        });
    }

    // determine user permission and role
    checkUserPermission() {
        getUserPermission().then(result => {
            this.isUserAdmin = result;
        }).catch(error => {
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

    // prepopulate expiration date on new form
    setExpirationDate() {
        getExpirationDate().then(result => {
            this.expirationDate = result;
        }).catch(error => {
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

    // gets picklist values (generic function)
    pickListValues(nameOfObject, nameOfField, callbackFunction) {
        getPicklistValues({objectName: nameOfObject, fieldName: nameOfField}).then(result => {
            callbackFunction(result);
        });
    }

    /* FUNCTIONS */

    temporaryEquipmentList = [];
    @track temporaryEquipmentIds = [];
    @track workOrderName;
    setupForm() {
        this.endpointForm.Id = this.transferForm.Id;
        this.endpointForm.Reason_for_Transfer_Additional_Details__c = this.transferForm.Reason_for_Transfer_Additional_Details__c;
        this.endpointForm.Date_of_Physical_Transfer__c = this.transferForm.Date_of_Physical_Transfer__c;
        this.endpointForm.Authorized_By__c = this.transferForm.Authorized_By__c;
        this.endpointForm.Safety_Critical_Equipment__c = this.transferForm.Safety_Critical_Equipment__c;
        this.endpointForm.Regulatory_Equipment__c = this.transferForm.Regulatory_Equipment__c;
        this.endpointForm.Expiration_Date__c = this.transferForm.Expiration_Date__c;
        this.endpointForm.Maintenance_Work_Order__c = this.transferForm.Maintenance_Work_Order__c;
        this.endpointForm.Shipped_Via__c = this.transferForm.Shipped_Via__c;
        this.endpointForm.Waybill_No__c = this.transferForm.Waybill_No__c;
        this.endpointForm.Charge_No__c = this.transferForm.Charge_No__c;
        this.endpointForm.Comments__c = this.transferForm.Comments__c;
        this.endpointForm.Reason__c = this.transferForm.Reason__c;
        this.temporaryEquipmentList = this.selectedEquipment;
        if (this.selectedEquipment.length > 0 ) {
            let context = this;
            this.selectedEquipment.forEach(function(equipment) {
                context.temporaryEquipmentIds.push(equipment.item.Equipment__c);
            });
        }
        this.equipmentForTransfer = [];

    }

    setupTransferReasonPicklist() {
        var changeView = this.template.querySelector('c-eqt_transfer_information-section');
        if (changeView) {
            changeView.setReasonPicklist();
        }
    }

    cancelEditState(event) {
        this.selectedEquipment = this.temporaryEquipmentList;
        this.temporaryEquipmentIds = [];
        this.equipmentForTransfer = [];
        this.filesIdsToRemove = [];
        this.attachmentIdsToRemove = [];
        this.itemFilesToInsert = [];
        this.itemsToRemove = [];

        this.isView = !this.isView;
        this.loadForm();

    }

    itemsToRemove = [];
    removeEquipmentOnEdit(event) {
        var itemId = event.currentTarget.dataset.id;
        var equipmentId = event.currentTarget.dataset.equipmentid;
        var index = event.currentTarget.dataset.index;

        let temporaryList = [];
        this.itemsToRemove.push(itemId);

        this.selectedEquipment.forEach(function(value){
           if (value.item.Id != itemId) {
                temporaryList.push(value);
           }
        });

        this.selectedEquipment = temporaryList;
        this.removeFromArray(this.temporaryEquipmentIds, equipmentId);
    }

    @track filesIdsToRemove = [];
    @track attachmentIdsToRemove = [];
    removeFile(event) {
        var fileId = event.currentTarget.dataset.id;
        var objectType = event.currentTarget.dataset.objecttype;
        var index = event.currentTarget.dataset.index;


        if (this.selectedEquipment[index].hasAttachment) {
            this.selectedEquipment[index].hasAttachment = false;
            if (objectType == 'File') {
                this.selectedEquipment[index].files = [];
                this.filesIdsToRemove.push(fileId);
            } else {
                this.selectedEquipment[index].attachments = [];
                this.attachmentIdsToRemove.push(fileId);
            }

        }

    }

    // call from modal on child function
    saveEquipmentSelection() {
        var saveMethod = this.template.querySelector('c-eqt_item_selection');
        if(saveMethod) {
            saveMethod.saveSelectedEquipment();
        }
    }

    saveEquipmentSelectionOnEdit() {
        var saveMethod = this.template.querySelector('c-eqt_item_selection');
        if(saveMethod) {
            saveMethod.saveSelectedEquipment();
        }
    }

    @track test = [];
    @track equipmentForTransfer = [];
    // onselectedequipmentevent event from child to populate list view of selected equipment
    handleSelectedEquipment(event) {
        this.equipmentList = event.detail;
        let context = this;


        this.equipmentList.forEach(function (equipment) {
            if (!context.temporaryEquipmentIds.includes(equipment.Id)) {

                var tmp = context.objectBuilder(equipment, null, null, null, null);
                context.equipmentForTransfer.push(tmp);
                context.temporaryEquipmentIds.push(equipment.Id);
            }
        });

        this.openModal = false;
    }


    objectBuilder(equipment, tagColour, itemFile, fileName, fileContent) {
        var test = {"equipment" : equipment, "tagColour" : tagColour, "itemFile" : {"fileName" : fileName, "fileContent" : fileContent}};
        return test;
    }

    /* USER ACTIONS */

    //open modal to pick equipment for transfer
    openEquipmentList() {
        if (this.isNewForm) {
            this.temporaryEquipmentIds = [];
            let context = this;
            this.equipmentForTransfer.forEach(function(value){
                context.temporaryEquipmentIds.push(value.equipment.Id);
            });

        }
        this.openModal = true;
    }

    closeEquipmentListModal() {
        this.openModal = false;
    }

    // change state from view to edit and vice versa
    editView() {
        this.setupForm();
        this.isView = !this.isView;
    }


    @api
    getUserInputs() {
        var sectionComments = this.template.querySelector('c-eqt_comments-section');
            if (sectionComments) {
                var userComments = sectionComments.getComments();
                this.comment = userComments.Comments__c;
                this.reason = userComments.Reason__c;
        }
        var saveMethod = this.template.querySelector('c-eqt_transfer_information-section');
        if (saveMethod) {
            this.endpointForm = saveMethod.userInputs();
        }

        if (this.isFromForm) {
            var getDestination = this.template.querySelector('c-eqt_transfer_information-section');
            if (getDestination) {
                this.transferDestination = getDestination.userSelectedDestination();
            }
        }


        if (this.recordId) {
            this.endpointForm.Comments__c = this.comment;
            this.endpointForm.Reason__c = this.reason;
            this.updateForm()
        } else {
            this.endpointForm.Id = null;
            this.endpointForm.Comments__c = this.comment;
            this.endpointForm.Reason__c = this.reason;
            this.insertForm();
        }
    }

    /* HANDLERS */
    handleSelectedDestination(event) {
        const clearDestination = this.template.querySelector('c-hog_lookup.blabla');
        if (clearDestination) {
            clearDestination.clearSelection();
            this.clearDestination();
        }
        this.selectedDestination = event.target.value;

    }

    handleTransferReason(event) {
        this.transferReason = event.target.value;
    }

    handleTagColour(event) {
        var tagColour = event.target.value;
        var equipmentId = event.currentTarget.dataset.id;
        this.tagColour = tagColour;
        var index = event.currentTarget.dataset.index;

        this.equipmentForTransfer[index].tagColour = tagColour;

    }

    handleTagColourOnItem(event) {
        var tagColour = event.target.value;
        var index = event.currentTarget.dataset.index;
        this.selectedEquipment[index].item.Tag_Colour__c = tagColour;
    }

    handleDateOfTransfer(event) {
        this.dateOfTransfer = event.target.value;
    }

    handleSafetyCritical(event) {
        this.safetyCriticalEq = !this.safetyCriticalEq;
    }

    handleRegulatoryEq(event) {
        this.regulatoryEq = !this.regulatoryEq;
    }

    handleWorkOrder(event) {
        var workOrder = event.detail;
        if (workOrder) {
            this.workOrder = workOrder.Id;
        } else {
            this.workOrder = null;
        }
    }

    clearWorkOrder(event) {
        this.workOrder = null;
    }

    handleShippedVia(event) {
        this.shippedVia = event.target.value;
    }

    handleWaybillNo(event) {
        this.waybillNo = event.target.value;
    }

    handleChargeNo(event) {
        this.chargeNo = event.target.value;
    }

    handleComment(event) {
        this.comment = event.target.value;
    }

    handleReason(event) {
        this.reason = event.target.value;
    }

    handleExpirationDate(event) {
        this.expirationDate = event.target.value;
    }

    handleAuthorizedBy(event) {
        var userObject = event.detail;
        if (userObject) {
            this.authorizedBy = userObject.Id;
        } else {
            this.authorizedBy = null;
        }
    }

    clearAuthorizedBy(event) {
        this.authorizedBy = null;
    }

    handleDestinationSelection(event) {
        var selectedObject = event.detail;
        if (selectedObject) {
            this.transferDestination = selectedObject.Id;
        } else {
            this.transferDestination = null;
        }
    }

    clearDestination(event) {
        this.transferDestination = null;
    }

    /* NAVIGATION */
    navigateToUser(event) {
        var userId = event.currentTarget.dataset.userid;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'User',
                actionName: 'view',
                recordId: userId
            },
        });
    }

    redirectToRecordPage(event) {
        var recordId = event.currentTarget.dataset.id;
        var apiName = event.currentTarget.dataset.apiname;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: apiName,
                actionName: 'view',
                recordId: recordId
            },
        });
    }

    openFilePreview(event) {
        if(this.isOnCommunity) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                objectApiName: "ContentDocument",
                actionName: 'view',
                recordId: event.currentTarget.dataset.id
                },
                });
        } else {
            this[NavigationMixin.Navigate]({
                type: 'standard__namedPage',
                attributes: {
                pageName: 'filePreview'
                },
                state : {
                selectedRecordId:event.currentTarget.dataset.id
                }
                });
            }
    }
}