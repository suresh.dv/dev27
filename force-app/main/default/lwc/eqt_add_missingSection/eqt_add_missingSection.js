/**
 * Created by MarosGrajcar on 2/12/2020.
 */

import { LightningElement, api, track } from 'lwc';

export default class EqtAddMissingSection extends LightningElement {

    @api addForm;
    @api isView;

    @track description;
    @track manufacturer;
    @track manufacturerSerialNo;
    @track safetyCriticalEq;
    @track partOfPackage;
    @track modelNumber;
    @track tagNumber;
    @track pId;
    @track regulatoryEq;

    addFormObject = {
        "Id" : '',
        "Description__c" : '',
        "Manufacturer__c" : '',
        "Manufacturer_Serial_No__c" : '',
        "Model_Number__c" : '',
        "Tag_Number__c" : '',
        "P_ID__c" : '',
        "Part_of_Package__c" : '',
        "Safety_Critical_Equipment__c" : '',
        "Regulatory_Equipment__c" : ''};

    _originalForm;
    @api
    get originalForm() {
        return this._originalForm;
    }

    set originalForm(value) {
        this._originalForm = value;
        this.addFormObject = JSON.parse(JSON.stringify(this.originalForm));
    }

    /*FUNCTIONS*/
    @api
    userInputs() {
        return this.addFormObject;
    }


    /* HANDLERS */

    /* checkboxes */
    handleSafetyCritical(event) {
        this.safetyCriticalEq = !this.safetyCriticalEq;
        this.addFormObject.Safety_Critical_Equipment__c = this.safetyCriticalEq;
    }

    handlePartOfPackage(event) {
        this.partOfPackage = !this.partOfPackage;
        this.addFormObject.Part_of_Package__c = this.partOfPackage;
    }

    handleRegulatory(event) {
        this.regulatoryEq = !this.regulatoryEq;
        this.addFormObject.Regulatory_Equipment__c = this.regulatoryEq;
    }

    /* input fields*/
    handleDescriptionChange(event) {
        this.description = event.target.value;
        this.addFormObject.Description__c = this.description;
    }

    handleManufacturerChange(event) {
        this.manufacturer = event.target.value;
        this.addFormObject.Manufacturer__c = this.manufacturer;
    }

    handleManufacturerSerialNoChange(event) {
        this.manufacturerSerialNo = event.target.value;
        this.addFormObject.Manufacturer_Serial_No__c = this.manufacturerSerialNo;
    }

    handleModelNumberChange(event) {
        this.modelNumber = event.target.value;
        this.addFormObject.Model_Number__c = this.modelNumber;
    }

    handleTagNumberChange(event) {
        this.tagNumber = event.target.value;
        this.addFormObject.Tag_Number__c = this.tagNumber;
    }

    handlePIdChange(event) {
        this.pId = event.target.value;
        this.addFormObject.P_ID__c = this.pId;
    }

}