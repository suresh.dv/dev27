/**
 * Created by MarcelBrimus on 26/08/2019.
 */

import { LightningElement, track, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import fetchEvents from '@salesforce/apex/VTT_LTNG_Calendar_Endpoint.loadEventsForTradesman';
import fetchOnHoldActivities from '@salesforce/apex/VTT_LTNG_Calendar_Endpoint.loadOnHoldActivities';
import fetchAccounts from '@salesforce/apex/VTT_LTNG_Calendar_Endpoint.loadVendorAccounts';
import fetchTradesman from '@salesforce/apex/VTT_LTNG_Calendar_Endpoint.loadTradesman';
import fetchDetails from '@salesforce/apex/VTT_LTNG_Calendar_Endpoint.loadEventDataForDate';
import fetUserDetails from '@salesforce/apex/VTT_LTNG_Calendar_Endpoint.loadCurrentUserData';

export default class VttTradesmenCalendar extends NavigationMixin(LightningElement) {
    // Flag for loading
    @track eventsLoading = false;
    @track accountsLoading = false;
    @track tradesmanLoading = false;
    @track detailsLoading = false;
    @track activitiesOnHoldLoading = false;
    @track isUserAdmin = false;
    @track isAdminOrSupervisor = false;

    // Selected account on picklist
    @track selectedAccount;
    @track selectedTradesman;

    // List of activities
    @track activityList;
    @api selectedDate;

    _accounts = [];
    _tradesman = [];
    _activitiesOnHold = [];

    @track logTime = {
        TotalHoursOffEquipmentText : '',
        TotalHoursOnEquipmentText: '',
        TotalHoursText: '',
    };
    @track summary;
    @track woalist;

    _calendarLabel;

    _visibleStartDate;
    _visibleEndDate;
    _loggedInUserData;

    // GETTERS SETTERS
    get accounts(){ return this._accounts; }

    get tradesman(){ return this._tradesman; }

    get activitiesOnHold(){ return this._activitiesOnHold; }

//    get logTime(){ return this.logTime; }

//    get summary(){ return this.summary; }
//
//    get woalist(){ return this.woalist; }

    get calendarLabel(){ return this._calendarLabel; }

    set calendarLabel(value){ this._calendarLabel = value; }

    get activitiesOnHoldSize(){ return this._activitiesOnHold.length; }

    get isAvailableJobInProgress() {
        return this._loggedInUserData !== undefined && this._loggedInUserData.Current_Work_Order_Activity__c !== undefined;
    }

    get loggedInUserData(){ return this._loggedInUserData; }

    // HANDLERS
    handleDateClick(eventData){
        this.initDataForDate(eventData.detail);
    }

    handleEventClick(event){
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": event.detail.extendedProps.activityId,
                "objectApiName": "Work_Order_Activity__c",
                "actionName": "view"
            },
        });
    }

    handleDatesChange(data){
        this._visibleStartDate = data.detail.activeStart;
        this._visibleEndDate = data.detail.activeEnd;
        this.initCalendarEvents(this.selectedTradesman,
                                this._visibleStartDate,
                                this._visibleEndDate);
    }

    accountchange(event){
        this.initTradesmanPickList(event.detail.value);
    }

    // Fired when user changes tradesman
    tradesmanchange(event){
        this.populateSelectedTradesman(event.detail.value);
        this.selectedTradesman = event.detail.value;
        this.initCalendarEvents(event.detail.value, this._visibleStartDate, this._visibleEndDate);
        this.initOnHoldData(event.detail.value);
    }

    // INITIALIZERS
    initializeComponentData(){
        console.log('Initializing Data')
        this.initAccountsPicklist();
        this.initUserData();
    }

    initUserData(){
        fetUserDetails().then(result => {
            if(result.success){
               if(result.resultObjects && result.resultObjects.length > 0){
                   this.isUserAdmin = result.resultObjects[0]['isAdminUser'];
                   this.isAdminOrSupervisor = result.resultObjects[0]['isSupervisor'] || this.isUserAdmin;
                   if(result.resultObjects[0]['userData'] !== null){
                        // Initialize picklist values so they can be preselected
                        this.initTradesmanPickList(result.resultObjects[0]['userData'].Account.Id);
                        this.initCalendarEvents(
                            result.resultObjects[0]['userData'].Id,
                            this._visibleStartDate,
                            this._visibleEndDate)

                        // Preselect values on picklists
                        this.selectedTradesman = result.resultObjects[0]['userData'].Id;
                        this._loggedInUserData = result.resultObjects[0]['userData'];
                        this.selectedAccount = result.resultObjects[0]['userData'].Account.Id;
                        this.calendarLabel = 'Activity Calendar for: ' + result.resultObjects[0]['userData'].Name;

                        // And init onhold data
                        this.initOnHoldData(this.selectedTradesman);

                        console.log('Calling initDataForDate')
                        // Init details for preselected date on calendar load
                        this.initDataForDate(new Date());
                   }
               }
            } else {
                var errorMessage = '';
                result.errors.forEach(function (item, index) {
                    errorMessage = errorMessage + item + '.'
                });
                this.showNotification('Error getting user data ', errorMessage, 'error');
            }

            this.detailsLoading = false;
        }).catch(error => {
            this.eventsLoading = false;
            this.detailsLoading = false;
            console.log('handleDateClick err ' + error)
        });
    }

    initAccountsPicklist(){
        this.accountsLoading = true;
        var tempAccounts = [];

        fetchAccounts().then(result => {
            if(result.success){
                if(result.resultObjects && result.resultObjects.length > 0){
                    result.resultObjects[0].forEach((item) => {
                        var acc = {
                            label : item.Name,
                            value : item.Id
                        }
                        tempAccounts.push(acc);
                    });
                }
                this._accounts = tempAccounts;
            } else {
                var errorMessage = '';
                result.errors.forEach(function (item, index) {
                    errorMessage = errorMessage + item + '.'
                });
                this.showNotification('Error getting accounts data ', errorMessage, 'error');
            }
            this.accountsLoading = false;
        }).catch(error => {
            this.eventsLoading = false;
            console.log('initAccountsPicklist err' + error)
        });
    }

    initCalendarEvents(tradesmanId, startDate, endDate) {
        this.eventsLoading = true;
        if(tradesmanId){
            fetchEvents({
                tradesmanID: tradesmanId,
                startDate: startDate,
                endDate: endDate
            }).then(result => {
                if(result.success){
                    var calendarEvents = [];
                    if(result.resultObjects && result.resultObjects.length > 0){
                        result.resultObjects[0].forEach((item) => {
                            calendarEvents.push({
                                title: item.title,
                                start: item.startString,
                                end: item.endString,
                                className: item.className,
                                // For popup
                                extendedProps: {
                                    param1Label: 'Work Order Number:',
                                    param1Value: item.woWONumber,
                                    param2Label: 'AMU:',
                                    param2Value: item.amuName,
                                    param3Label: item.woFlocType,
                                    param3Value: item.woFlocName,
                                    param4Label: 'Equipment Tag#:',
                                    param4Value: item.equipmentTagNumber,
                                    activityId: item.activityId
                                }
                            });
                        });
                    }
                    this.activityList = calendarEvents;
                } else {
                    var errorMessage = '';
                    result.errors.forEach(function (item, index) {
                        errorMessage = errorMessage + item + '.'
                    });
                    this.showNotification('Error getting events', errorMessage, 'error');
                }
                this.eventsLoading = false;
            }).catch(error => {
                this.eventsLoading = false;
                console.log('initCalendarEvents err ' + error)
            });
        }
    }

    initOnHoldData(tradesmanId) {
        this.activitiesOnHoldLoading = true;

        fetchOnHoldActivities(
            { tradesmanID: tradesmanId }
        ).then(result => {

            if(result.success){
                if(result.resultObjects !== undefined && result.resultObjects.length > 0){
                   this._activitiesOnHold = result.resultObjects[0];
                }
            } else {
                var errorMessage = '';
                result.errors.forEach(function (item, index) {
                    errorMessage = errorMessage + item + '.'
                });
                this.showNotification('Error getting activities on hold ', errorMessage, 'error');
            }
            this.activitiesOnHoldLoading = false;
        }).catch(error => {
            this.activitiesOnHoldLoading = false;
            console.log('initOnHoldData err ' + error)
        });
    }

    initTradesmanPickList(accId){
        this.tradesmanLoading = true;
        fetchTradesman({accountId:accId}).then(result => {

            if(result.success){
                var tradesmanTemp = [];
                if(result.resultObjects && result.resultObjects.length > 0){
                    result.resultObjects[0].forEach((item) => {
                        var trade = {
                            label : item.Name,
                            value : item.Id
                        }
                       tradesmanTemp.push(trade);
                    });
                }
                this._tradesman = tradesmanTemp;
            } else {
                var errorMessage = '';
                result.errors.forEach(function (item, index) {
                    errorMessage = errorMessage + item + '.'
                });
                this.showNotification('Error getting tradesman data', errorMessage, 'error');
            }
            this.tradesmanLoading = false;
        }).catch(error => {
            this.tradesmanLoading = false;
            console.log('initTradesmanPickList err ' + error)
        });
    }

    initDataForDate(dateString){
        console.log('In initDataForDate '+ dateString)
        this.detailsLoading = true;
            this.selectedDate = dateString;
            fetchDetails({
                selectedDate : dateString,
                tradesmanId : this.selectedTradesman
            }).then(result => {
                if(result.success){
                   if(result.resultObjects && result.resultObjects.length > 0){
                       console.log('Setting results ')
                       console.log('Setting results ' + result.resultObjects[0]['summary'])
                       console.log('Setting results ' + result.resultObjects[0]['woalist'])
                       this.logTime = result.resultObjects[0]['totals'];
                       this.summary = result.resultObjects[0]['summary'];
                       this.woalist = result.resultObjects[0]['woalist'];
                   }
                } else {
                    var errorMessage = '';
                    result.errors.forEach(function (item, index) {
                        errorMessage = errorMessage + item + '.'
                    });
                    this.showNotification('Error getting event data for this date ', errorMessage, 'error');
                }
                this.detailsLoading = false;
            }).catch(error => {
                this.detailsLoading = false;
                console.log('handleDateClick err ' + error)
            });
    }

    // UTILITIES
    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    populateSelectedTradesman(tradesmanId){
        var context = this;
        this._tradesman.find(function(element){
            if (element.value === tradesmanId){
               context.calendarLabel = 'Activity Calendar for: ' + element.label;
            };
        });
    }

    handleJobInProgress() {
        console.log('this.selectedTradesmanData.Current_Work_Order_Activity__c ' + this._loggedInUserData.Current_Work_Order_Activity__c)
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this._loggedInUserData.Current_Work_Order_Activity__c,
                objectApiName: 'Work_Order_Activity__c',
                actionName: 'view'
            }
        });
    }
}