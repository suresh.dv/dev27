/**
 * Created by MarcelBrimus on 29/11/2019.
 */

import { LightningElement, api, track } from 'lwc';
import disableUser from '@salesforce/apex/VTT_LTNG_PortalManagementEndpoint.disableUser';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class VttLightningCommunityManagementDisableUser extends LightningElement {
    @api isCommunityUser;
    @api contact;
    @track showModal = false;
    @track disablingInProgress = false;

    disableAsInternalUser(){
        this.disableUser(false);
    }

    disableAsCommunityUser(){
        this.disableUser(true);
    }

    disableUser(isCommunityUser){
        this.disablingInProgress = true;
        disableUser({
            isCommunityUser: isCommunityUser,
            contact: JSON.stringify(this.contact)
        }).then(result => {
            if(result.success){
                if(isCommunityUser){
                    this.showNotification('Successfully disabled user', 'Administrator has been notified and this user will be disabled', 'success');
                } else {
                    this.showNotification('Successfully disabled user', 'You have successfully disabled user please refresh this page after a while in order to changes to appear', 'success');
                }
            } else {
                 var errorMessage = '';
                 result.errors.forEach(function (item, index) {
                     errorMessage = errorMessage + item + '.'
                 });
                 this.showNotification('Error getting contacts ', errorMessage, 'error');
            }
            this.disablingInProgress = false;
            this.closeModal();
        }).catch(error => {
            this.disablingInProgress = false;
            this.closeModal();
            console.log('onAccountChange err ' + error)
        });
    }

    openmodal() {
        this.showModal = true
    }

    closeModal() {
        this.showModal = false
    }

    // UTILITIES
    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }
}