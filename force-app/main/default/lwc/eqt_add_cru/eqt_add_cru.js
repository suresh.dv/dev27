/**
 * Created by MarosGrajcar on 11/25/2019.
 */

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getUserPermission from '@salesforce/apex/Equipment_Forms_Endpoint.getUserPermission';
import getExpirationDate from '@salesforce/apex/Equipment_Forms_Endpoint.getExpirationDate';
import loadAddForm from '@salesforce/apex/Equipment_Forms_Endpoint.loadAddEquipmentForm';
import insertAddForm from '@salesforce/apex/Equipment_Forms_Endpoint.insertAddEquipmentForm';
import updateAddEquipmentForm from '@salesforce/apex/Equipment_Forms_Endpoint.updateAddEquipmentForm';
import rejectAddEquipmentForm from '@salesforce/apex/Equipment_Forms_Endpoint.rejectAddEquipmentForm';
import processAddEquipmentForm from '@salesforce/apex/Equipment_Forms_Endpoint.processAddEquipmentForm';
import loadFLOC from '@salesforce/apex/Equipment_Forms_Endpoint.loadFLOC';

export default class EqtAddCru extends NavigationMixin(LightningElement) {

    /* VARIABLES */

    //public variables
    // metadata property that is set on component to show active form or not
    @api isOnCommunity = false;
    @api formId;

    //local variables
    /* FORM VARIABLES */
    @track addForm; // Equipment_Missing_Form__c SObject
    @track description;
    @track manufacturer;
    @track manufacturerSerialNo;
    @track safetyCriticalEq;
    @track partOfPackage;
    @track modelNumber;
    @track tagNumber;
    @track pId;
    @track regulatoryEq;
    @track comment;
    @track reason;
    @track expirationDate; // pre-fill expiration date value based on custom settings value (see Equipment Forms documentation)

    /* COMPONENT STATE VARIABLES */
    @track loading = true;
    @track isView = false; // when true Read mode is active, when false Edit or Create is mode is active
    @track isNewForm = true;
    @track isUserAdmin = false;
    @track formClosed = false;

    /* TABLE VIEW VARIABLES */
    @track activeForms; // return active forms for table view (eqt_forms_holder cmp)
    @track eqtFormsHolderPresent = false; // when false the component is standalone, when true the component is called from eqt_forms_holder

    /* File Variables */
    MAX_FILE_SIZE = 14999999;
    @track containsSerialPlate = false;
    @track containsAttachment = false;

    //Serial Plate
    // variables for upload
    @track serialPlateFile;
    @track serialPlateFileName;
    serialPlateFileContents;
    serialPlateFileReader;
    serialContent;
    // variables to view file
    @track uploadedSerialPlateFile;
    @track uploadedSerialPlateAttachment;

    //Attachment
    // variables for upload
    @track attachmentFile;
    @track attachmentFileName;
    attachmentFileContents;
    attachmentFileReader;
    attachmentContent;
    // variables to view file
    @track uploadedAttachmentFile;
    @track uploadedAttachment;


    @track endpointForm = {
        "Id" : '',
        "Description__c": '',
        "Manufacturer__c": '',
        "Manufacturer_Serial_No__c": '',
        "Safety_Critical_Equipment__c": '',
        "Part_of_Package__c": '',
        "Model_Number__c": '',
        "Tag_Number__c": '',
        "P_ID__c": '',
        "Regulatory_Equipment__c": '',
        "Expiration_Date__c": '',
        "Comments__c": '',
        "Reason__c" : ''};

   // check for record Id. If true, load form, when false set it to new form





    /*GETTERS AND SETTERS*/
    _flockId;
    @api
    get flockId() {
      return this._flockId;
    }

    set flockId(value) {
        this._flockId = value;
        this.loadFLOCInformation();
        this.setExpirationDate();
    }

    _recordId;
    @api
    get recordId() {
        return this._recordId;
    }

    set recordId(value) {
        this._recordId = value;
        if (this._recordId) {
            this.checkUserPermission();
            this.loadForm();
        } else {
            this.loadFLOCInformation();
            this.setExpirationDate();
       }
    }

     _editModal = false;
    @api
    get editModal() {
        return this._editModal;
    }

    set editModal(value) {
        this._editModal = value;
        if (this._editModal) {
            this.recordId = this.formId;
            this.checkUserPermission();
            this.loadForm();
        }
    }

    /* ENDPOINTS (CALLBACKS) */
    loadForm() {
        loadAddForm({formId : this.recordId}).then(result => {
            this.addForm = result.addForm;
            if (this.formIsClosed()) {
                this.formClosed = true;
            }
            this.uploadedSerialPlateFile = null;
            this.uploadedAttachmentFile = null;
            this.uploadedSerialPlateAttachment = null;
            this.uploadedAttachment = null;
            if (result.addFormFiles) {
                let context = this;
                result.addFormFiles.forEach(function (value) {
                    if (context.addForm.Serial_plate_of_asset__c == value.ContentDocumentId) {
                        context.containsSerialPlate = true;
                        context.uploadedSerialPlateFile = value;
                        context.serialPlateFileName = value.ContentDocument.Title;
                    } else if (context.addForm.Attachment__c == value.ContentDocumentId) {
                        context.containsAttachment = true;
                        context.uploadedAttachmentFile = value;
                    }
                });
            }

            if (result.addFormAttachments) {
            let context = this;
            result.addFormAttachments.forEach(function (value) {
                if (context.addForm.Serial_plate_of_asset__c == value.Id) {
                    context.containsSerialPlate = true;
                    context.uploadedSerialPlateAttachment = value;
                    context.serialPlateFileName = value.Name;
                } else if (context.addForm.Attachment__c == value.Id) {
                    context.containsAttachment = true;
                    context.uploadedAttachment = value;
                }
            });
            }

            this.insertSerialPlateFile = false;
            this.insertAttachmentFile = false;
            this.isView = true;
            this.isNewForm = false;
            this.loading = false;

            if (this.editModal) {
                 this.editView();
            }

        }).catch(error => {
            this.showToast('', this.handleErrorMsg(error), 'error');
        });
    }

    handleErrorMsg(errorMsg) {
        var message = '';
        if(errorMsg.body.message) {
            message = errorMsg.body.message;
        } else {
            message = errorMsg;
        }
        return message;
    }

    @track flockInformation;
    loadFLOCInformation() {
        loadFLOC({flockId : this.flockId}).then(result => {
            this.flockInformation = result;
            this.loading = false;
        }).catch(error => {
            console.log('Error on load add form' + JSON.stringify(error));
            this.dispatchEvent(
                new ShowToastEvent({
                title: '',
                message: error.message,
                variant: 'error',
                }),
            );
        });
    }

    @api
    insertForm() {
         if (this.validateFormBeforeSave()) {
            this.loading = true;
            insertAddForm({flocId : this.flockId,
                            addForm : this.endpointForm,
                            serialPlateBase64Data : encodeURIComponent(this.serialPlateFileContents),
                            serialPlateName : this.serialPlateFileName,
                            attachmentBase64Data : encodeURIComponent(this.attachmentFileContents),
                            attachmentFileName : this.attachmentFileName}).then(result => {
                this.showToast('', 'Add Equipment Form has been successfully created.', 'success');
                //eval("$A.get('e.force:refreshView').fire();");
                this.dispatchEvent(new CustomEvent('closemodal'));
            }).catch(error => {
                window.console.log(error);
                this.dispatchEvent(
                    new ShowToastEvent({
                    title: '',
                    message: error.message,
                    variant: 'error',
                    }),
                );
                this.dispatchEvent(new CustomEvent('closemodal'));
            });
         } else {
             this.showToast('', 'Please, fill the required fields before saving the form.', 'warning');
             this.dispatchEvent(new CustomEvent('enablebuttons'));
         }
    }

    @api
    updateForm() {
        if (this.validateFormBeforeSave()) {
            let handleUploadsObject = {"originalSerialPlateFileId" : this.originalSerialPlateFileId,
                                        "serialPlateFileHasChanged" : this.serialPlateFileHasChanged,
                                        "serialPlateBase64Data" : encodeURIComponent(this.serialPlateFileContents),
                                        "serialPlateName" : this.serialPlateFileName,
                                        "originalSerialPlateAttachmentId" : this.originalSerialPlateAttachmentId,
                                        "serialPlateAttachmentHasChanged" : this.serialPlateAttachmentHasChanged,
                                        "originalAttachmentFileId" : this.originalAttachmentFileId,
                                        "attachmentFileHasChanged" : this.attachmentFileHasChanged,
                                        "attachmentBase64Data" : encodeURIComponent(this.attachmentFileContents),
                                        "attachmentFileName" : this.attachmentFileName,
                                        "originalAttachmentId" : this.originalAttachmentId,
                                        "insertSerialPlate" : this.insertSerialPlateFile,
                                        "insertAttachment" : this.insertAttachmentFile,
                                        "attachmentHasChanged" : this.attachmentHasChanged,};

            this.loading = true;
            updateAddEquipmentForm({addForm : this.endpointForm,
                                    handleUploads : handleUploadsObject}).then(result => {
                this.isView = true;
                this.loadForm();
                this.showToast('', 'Add Missing Equipment Form has been successfully updated ', 'success');
                if (this.editModal) {
                    //eval("$A.get('e.force:refreshView').fire();");
                    this.dispatchEvent(new CustomEvent('closemodal'));
                }
            }).catch(error => {
                let errorMsg;
                if(error.body) {
                    errorMsg = error.body.message;
                } else {
                    errorMsg = error;
                }
                console.log('Error ' +errorMsg);
                this.showToast('', errorMsg, 'error');
                this.dispatchEvent(new CustomEvent('closemodal'));
                this.loading = false;
            });
        } else {
            this.showToast('', 'Please, fill the required fields before Saving the form', 'warning');
            this.dispatchEvent(new CustomEvent('enablebuttons'));
        }
    }

    checkUserPermission() {
        getUserPermission().then(result => {
            this.isUserAdmin = result;
        }).catch(error => {
           this.showToast('', error, 'error');
        });
    }

    setExpirationDate() {
        getExpirationDate().then(result => {
            this.expirationDate = result;
            this.loading = false;
        }).catch(error => {
           this.showToast('', error, 'error');
        });
    }

    formIsClosed() {
        if (this.addForm.Status__c == 'Auto Closed'
        || this.addForm.Status__c == 'Rejected Closed'
        || this.addForm.Status__c == 'Processed') {
            return true;
        } else {
            return false;
        }
    }

    /* FUNCTIONS */
    showToast(title, message, toastType) {
        const toastEvent = new ShowToastEvent({title : title, message : message, variant : toastType});
        this.dispatchEvent(toastEvent);
    }

    handleError(error, showOnlyToast) {
        if(showOnlyToast) {
            this.errorMessage = error;
        }
        this.loading = false;
        this.showToast();
    }

    setupEditView() {
        this.endpointForm.Id = this.addForm.Id;
        this.endpointForm.Description__c = this.addForm.Description__c;
        this.endpointForm.Manufacturer__c = this.addForm.Manufacturer__c;
        this.endpointForm.Manufacturer_Serial_No__c = this.addForm.Manufacturer_Serial_No__c;
        this.endpointForm.Safety_Critical_Equipment__c = this.addForm.Safety_Critical_Equipment__c;
        this.endpointForm.Part_of_Package__c = this.addForm.Part_of_Package__c;
        this.endpointForm.Model_Number__c = this.addForm.Model_Number__c;
        this.endpointForm.Tag_Number__c = this.addForm.Tag_Number__c;
        this.endpointForm.P_ID__c = this.addForm.P_ID__c;
        this.endpointForm.Regulatory_Equipment__c = this.addForm.Regulatory_Equipment__c;
        this.endpointForm.Comments__c = this.addForm.Comments__c;
        this.endpointForm.Reason__c = this.addForm.Reason__c;
        this.endpointForm.Expiration_Date__c = this.addForm.Expiration_Date__c;
    }

    validateFormBeforeSave() {
        return this.endpointForm.Description__c != null
            && this.endpointForm.Description__c != ''
            && this.endpointForm.Manufacturer__c != null
            && this.endpointForm.Manufacturer__c != ''
            && this.endpointForm.Manufacturer_Serial_No__c != null
            && this.endpointForm.Manufacturer_Serial_No__c != ''
            && this.endpointForm.Model_Number__c != null
            && this.endpointForm.Model_Number__c != ''
            && this.serialPlateFileName != null;
    }

    /* USER ACTIONS */
    editView() {
        this.setupEditView();
        this.isView = !this.isView;
    }

    cancelEditView() {
        if (this.uploadedSerialPlateFile && this.serialPlateFileHasChanged) {
            this.serialPlateFileName = this.uploadedSerialPlateFile.ContentDocument.Title;
            this.containsSerialPlate = !this.containsSerialPlate;
            this.serialPlateFileHasChanged = !this.serialPlateFileHasChanged;
        }

        if (this.uploadedAttachmentFile && this.attachmentFileHasChanged) {
            this.attachmentFileName = this.uploadedAttachmentFile.ContentDocument.Title;
            this.containsAttachment = !this.containsAttachment;
            this.attachmentFileHasChanged = !this.attachmentFileHasChanged;
        }

        if (this.serialPlateAttachmentHasChanged) {
            this.containsSerialPlate = !this.containsSerialPlate;
            this.serialPlateAttachmentHasChanged = !this.serialPlateAttachmentHasChanged;
        }

        if (this.attachmentHasChanged) {
            this.containsAttachment = !this.containsAttachment;
            this.attachmentHasChanged = !this.attachmentHasChanged;
        }

        this.isView = !this.isView;
    }

    @api
    getUserInputs() {
        var sectionComments = this.template.querySelector('c-eqt_comments-section');
        if (sectionComments) {
            var userComments = sectionComments.getComments();
            this.comment = userComments.Comments__c;
            this.reason = userComments.Reason__c;
        }

        var saveMethod = this.template.querySelector('c-eqt_add_missing-section');
        if (saveMethod) {
           this.endpointForm = saveMethod.userInputs();
        }

        if (this.recordId) {
            this.endpointForm.Expiration_Date__c = this.expirationDate;
            this.endpointForm.Comments__c = this.comment;
            this.endpointForm.Reason__c = this.reason;
            this.updateForm()
        } else {
            this.endpointForm.Id = null;
            this.endpointForm.Expiration_Date__c = this.expirationDate;
            this.endpointForm.Comments__c = this.comment;
            this.endpointForm.Reason__c = this.reason;
            this.insertForm();
        }
    }


    /* HANDLERS */

    /* checkboxes */
    handleSafetyCritical(event) {
        this.safetyCriticalEq = !this.safetyCriticalEq;
    }

    handlePartOfPackage(event) {
        this.partOfPackage = !this.partOfPackage;
    }

    handleRegulatory(event) {
        this.regulatoryEq = !this.regulatoryEq;
    }

    /* input fields*/
    handleDescriptionChange(event) {
        this.description = event.target.value;
    }

    handleManufacturerChange(event) {
        this.manufacturer = event.target.value;
    }

    handleManufacturerSerialNoChange(event) {
        this.manufacturerSerialNo = event.target.value;
    }

    handleModelNumberChange(event) {
        this.modelNumber = event.target.value;
    }


    handleTagNumberChange(event) {
        this.tagNumber = event.target.value;
    }


    handlePIdChange(event) {
        this.pId = event.target.value;
    }

    handleExpirationDate(event) {
        this.expirationDate = event.target.value;
    }

   /* handleComment(event) {
        this.comment = event.target.value;
    }

    handleReason(event) {
        this.reason = event.target.value;
    }*/

    handleSerialPlateUpload(event) {
        this.serialPlateFile = event.target.files;
        this.serialPlateFileName = event.target.files[0].name;
        this.serialPlateFileReader = new FileReader();
        this.serialPlateFileReader.readAsDataURL(this.serialPlateFile[0]);
        this.serialPlateFileReader.onloadend = (() => {
            this.serialPlateFileContents = this.serialPlateFileReader.result;
            let base64 = 'base64,';
            this.serialContent = this.serialPlateFileContents.indexOf(base64) + base64.length;
            this.serialPlateFileContents = this.serialPlateFileContents.substring(this.serialContent);
        });

        this.insertSerialPlateFile = true;
    }


    handleAttachmentUpload(event) {
        this.attachmentFile = event.target.files;
        this.attachmentFileName = event.target.files[0].name;
        this.attachmentFileReader = new FileReader();
        this.attachmentFileReader.readAsDataURL(this.attachmentFile[0]);
        this.attachmentFileReader.onloadend = (() => {
            this.attachmentFileContents = this.attachmentFileReader.result;
            let base64 = 'base64,';
            this.attachmentContent = this.attachmentFileContents.indexOf(base64) + base64.length;
            this.attachmentFileContents = this.attachmentFileContents.substring(this.attachmentContent);
        });
        this.insertAttachmentFile = true;
    }

    @track openModal = false;
    formRejectModal() {
        this.openModal = !this.openModal;
    }

    rejectForm() {
        this.loading = true;
        rejectAddEquipmentForm({formId : this.recordId, rejectionReason : this.reason}).then(result => {
                this.showToast('', 'Add Missing Equipment Form has been successfully rejected ', 'success');
                this.isView = true;
                this.openModal = false;
                this.loadForm();
            }).catch(error => {
                let errorMsg;
                if(error.body) {
                    errorMsg = error.body.message;
                } else {
                    errorMsg = error;
                }
                console.log('Error ' +errorMsg);
                this.showToast('', errorMsg, 'error');
                this.loading = false;
            });
    }

    formProcessed() {
        this.loading = true;
        processAddEquipmentForm({formId : this.recordId}).then(result => {
                this.showToast('', 'Add Missing Equipment Form has been successfully proceed', 'success');
                this.isView = true;
                this.loadForm();
            }).catch(error => {
                let errorMsg;
                if(error.body) {
                    errorMsg = error.body.message;
                } else {
                    errorMsg = error;
                }
                console.log('Error ' +errorMsg);
                this.showToast('', errorMsg, 'error');
                this.loading = false;
            });
    }

    /* NAVIGATION */
    navigateToUser(event) {
        var userId = event.currentTarget.dataset.userid;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'User',
                actionName: 'view',
                recordId: userId
            },
        });
    }

    redirectToRecordPage(event) {
        var recordId = event.currentTarget.dataset.id;
        var apiName = event.currentTarget.dataset.apiname;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: apiName,
                actionName: 'view',
                recordId: recordId
            },
        });
    }


    @track serialPlateURL;

    openFilePreview(event) {
        if(this.isOnCommunity) {
            this[NavigationMixin.Navigate]({
                type: 'standard__recordPage',
                attributes: {
                    objectApiName: "ContentDocument",
                    actionName: 'view',
                    recordId: event.currentTarget.dataset.id
                },
            });
        } else {
            this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'filePreview'
            },
            state : {
                selectedRecordId:event.currentTarget.dataset.id
            }
            });
        }
    }

    originalSerialPlateFileId;
    serialPlateFileHasChanged = false;
    originalAttachmentFileId;
    attachmentFileHasChanged = false;
    originalSerialPlateAttachmentId;
    serialPlateAttachmentHasChanged = false;
    originalAttachmentId;
    attachmentHasChanged = false;

    insertSerialPlateFile = false;
    insertAttachmentFile = false;

    removeFile(event) {
        var recordId = event.currentTarget.dataset.id;
        var objectType = event.currentTarget.dataset.objecttype;
        var fieldType = event.currentTarget.dataset.fieldtype

        if (fieldType == 'serial') {
            this.containsSerialPlate = false;
            this.serialPlateFileName = null;

            if (objectType == 'file') {
                this.originalSerialPlateFileId = recordId;
                this.serialPlateFileHasChanged = true;

            } else if (objectType == 'attachment') {
                this.originalSerialPlateAttachmentId = recordId;
                this.attachmentFileHasChanged = true;
            }
        } else if (fieldType == 'attachment') {
            this.containsAttachment = false;
            this.attachmentFileName = null;

            if (objectType == 'file') {
                this.originalAttachmentFileId = recordId;
                this.attachmentFileHasChanged = true;

            } else if (objectType == 'attachment') {
                this.originalAttachmentId = recordId;
                this.attachmentHasChanged = true;
            }
        }

    }

}