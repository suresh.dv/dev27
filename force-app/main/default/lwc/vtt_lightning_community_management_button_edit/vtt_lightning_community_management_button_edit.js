/**
 * Created by MarcelBrimus on 29/11/2019.
 */

import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class VttLightningCommunityManagementButtonEdit extends NavigationMixin(LightningElement) {
   @api contact;
   @api isEnabledContact;
   @api key;

   editContact(){
       this[NavigationMixin.Navigate]({
           type: 'standard__recordPage',
           attributes: {
               recordId: this.contact.Id,
               objectApiName: 'Contact',
               actionName: 'edit'
           }
       });
   }
}