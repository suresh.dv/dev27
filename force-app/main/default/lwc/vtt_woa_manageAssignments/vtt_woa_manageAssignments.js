/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 04/09/2019 - Created.
*************************************************************************************************/
const WOA_COLUMNS_SETTING = [
                                {label: 'Work Order', fieldName: 'woUrl', type: 'url',
                                    typeAttributes: {
                                        label: { fieldName: 'woName'},
                                        tooltip: { fieldName: 'woName'}
                                    }
                                },
                                {label: 'Activity', fieldName: 'Name', type: 'text'},
                                {label: 'Order Type', fieldName: 'woType', type: 'text'},
                                {label: 'Work Order Status', fieldName: 'woStatus', type: 'text'},
                                {label: 'Work Center', fieldName: 'Work_Center__c', type: 'text'},
                                {label: 'Route', fieldName: 'woRoute', type: 'text'},
                                {label: 'Location', fieldName: 'Location_Name__c', type: 'text'},
                                {label: 'Scheduled', fieldName: 'Scheduled_Start_Date__c', type: 'date',
                                    typeAttributes: {
                                        year: 'numeric',
                                        month: '2-digit',
                                        day: '2-digit',
                                        hour: '2-digit',
                                        minute: '2-digit'
                                    }
                                },
                                {label: 'Vendor', fieldName: 'vendorUrl', type: 'url',
                                    typeAttributes: {
                                        label: { fieldName: 'vendorName'},
                                        tooltip: { fieldName: 'vendorName'}
                                    }
                                },
                                {label: 'Assigned', fieldName: 'Assigned_Text__c', type: 'text'},
                                {label: 'Status', fieldName: 'woaStatus', type: 'text'}
                            ];
const MODE_STANDALONE = 'STANDALONE';
const MODE_DEPENDENT = 'DEPENDENT';

import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getUserFlags from '@salesforce/apex/VTT_Utility_Endpoint.getTradesmanInfo';
import assign from '@salesforce/apex/VTT_WOA_Assignment_Endpoint.assign';
import unassign from '@salesforce/apex/VTT_WOA_Assignment_Endpoint.unassign';
import getRecords from '@salesforce/apex/VTT_WorkOrderActivity_Endpoint.getRecords';

export default class VTT_WOAManageAssignments extends NavigationMixin(LightningElement) {

//VARS
    @track _woas = []; // Work_Order_Activity__c[] enriched
    @api tradesmanInfo = undefined; // VTT_TradesmanFlags
    @track actionInProgress = true;
    @api mode = MODE_STANDALONE;

    @track woasColumns = WOA_COLUMNS_SETTING;
    @track selectedWOAs = []; // Work_Order_Activity__c[] enriched
    @track tradesmanInfoLoaded = false;

    _selectedVendor = undefined; // Id
    _selectedTradesmen = []; // Id[]
    _woaIds = []; // Id[]
    _initialLoading = true;
    _clearInProgress = false;

    _indexCounter = 0;
    _indexCounterLimit = 0;
    @track hideTable = true;

    /*
    Loads User Flags like isAdmin, isVendor or similar (check VTT_TradesmanFlags class
    */
    constructor() {
        super();
        getUserFlags()
            .then(result => {
                this.tradesmanInfo = result;
                this.tradesmanInfoLoaded = true;
            })
            .catch(error => {
                this.handleError(error);
            });
    }

//SETTERS
    /*
    Setter for Work Order Activity list.
    Handles also Enrichment of the data for table.
    */
    set woas(value){
        this.hideTable = true;
        this.enrichData(value);
        this._initialLoading = false;
    }

    /*
    Setter for Work Order Activity Id list.
    Handles load of Work Order Activity records.
    */
    set woaIds(value) {
        this._woaIds = value;
        this.loadRecords();
    }

//APIs
    @api
    get woas() {
        return this._woas;
    }

    @api
    get woaIds() {
        return this._woaIds;
    }

    /*
    API method that runs data refresh.
    Works in 2 modes.
    Dependent mode expects List of Work Order Activity records as parameter.
    Independent mode doesn't expect any parameter.
    */
    @api
    doRefreshData(data) {
        this.hideTable = true;
        if(this.mode === MODE_DEPENDENT) {
            this.actionInProgress = true;
            this.enrichData(data);
        } else {
            this.loadRecords();
        }
    }

//HANDLERS/ACTIONS
    /*
    Fires close event.
    */
    handleCancel() {
        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }

    /*
    Clears Tradesmen List selections.
    */
    handleClear() {
        const tradesmanList = this.template.querySelector('c-vtt_woa_manage-assignments_tradesman-list');
        if(tradesmanList) {
            tradesmanList.clearData();
            this._clearInProgress = true;
        } else {
        }
    }

    /*
    retrieves selected Vendor and Tradesmen and call Apex to run assignment job.
    After response is returned handling is performed on it.
    */
    handleAssign() {
        this.actionInProgress = true;
        let selectedTradesmen = this.getSelectedTradesmen();
        let selectedVendorId = this.getSelectedVendorId();

        assign({
            activities: this.selectedWOAs,
            tradesmenIds: selectedTradesmen,
            vendorAccountId: selectedVendorId
        })
        .then(result => {
            this.handleResult(result, 'Assignment was successful.');
        })
        .catch(error => {
            this.handleError(error);
        });
    }

    /*
    Retrieves selected Vendor and Tradesmen and call Apex to run un-assignment job.
    After response is returned handling is performed on it.
    */
    handleUnassign() {
        this.actionInProgress = true;
        let selectedTradesmen = this.getSelectedTradesmen();
        let selectedVendorId = this.getSelectedVendorId();

        unassign({
            activities: this.selectedWOAs,
            tradesmenIds: selectedTradesmen,
            vendorAccountId: selectedVendorId
        })
        .then(result => {
            this.handleResult(result, 'Unassignment was successful.');
        })
        .catch(error => {
            this.handleError(error);
        });
    }

    /*
    Success result handling function.
    Works also in two modes.
    First it fire Success Toast message.
    Then it fires successassignment event
    For Dependent mode and single WOA it even fires close event
    For standalone mode it reloads data.
    */
    handleResult(result, message) {
        const event = new ShowToastEvent({
            title: 'Success',
            type: 'success',
            message: message
        });
        this.dispatchEvent(event);
        const assignmentEvt = new CustomEvent('successassignment');

        if(this.isDependentMode() && this._woas && this._woas.length === 1) {
            const closeEvent = new CustomEvent('close');
            this.dispatchEvent(closeEvent);
        }

        this.dispatchEvent(assignmentEvt);
        if(this.isStandaloneMode()) {
            this.loadRecords();
        }
    }

    /*
    Error result handling function.
    Fires Error Toast message
    then stops action. (during action => spinner spinning and disabled buttons)
    */
    handleError(error) {
        let errorMsg = error;
        if(error.body) {
            errorMsg = error.body.message;
        }
        const event = new ShowToastEvent({
            title: 'Error',
            type: 'error',
            message: errorMsg
        });
        this.dispatchEvent(event);
        this.actionInProgress = false;
    }

    handleVendorSelection(evt) {
        this._selectedVendor = evt.detail;
    }

    handleTradesmenSelection(evt) {
        this._selectedTradesmen = evt.detail;
    }

    handleRefresh() {
        this.loadRecords();
    }

//HELPERS
    /*
    Shows spinner, disable buttons (actionInProgress)
    Loads Work Order Activities.
    */
    loadRecords() {
        this.actionInProgress = true;
        getRecords({woaIds: this._woaIds})
            .then(result => {
                this.woas = result;
            })
            .catch(error => {
                this.handleError(error);
            });
    }

    /*
    Enriching Work Order Activity records.
    First it clones the record to overcome record lock.
    Then it builds aggregated fields for purpose of displaying it on Lighting-datatable.
    Last thing it does is when only single record is provided(loaded) it will pre-select this record.
    */
    enrichData(value) {
        this._woas = [];
        this._indexCounter = 0;
        let promiseRunning = false;
        if(value) {
            this.resetIndexCounterLimit(value);
            for(let i = 0; i < value.length; i++) {
                this._woas.push(JSON.parse(JSON.stringify(value[i])));//To overcome locked records
                promiseRunning = true;
                this.generateUrl(this._woas[i].Id, i, 'woaUrl');

                if(this._woas[i].Assigned_Vendor__r !== undefined) {
                    this.generateUrl(this._woas[i].Assigned_Vendor__c, i, 'vendorUrl');
                    this._woas[i].vendorName = this._woas[i].Assigned_Vendor__r.Name;
                } else {
                    this._woas[i].vendorName = '';
                    this._woas[i].vendorUrl = '';
                }

                this._woas[i].woType = this._woas[i].Maintenance_Work_Order__r.Order_Type__c;
                this._woas[i].woPriority = this._woas[i].Maintenance_Work_Order__r.Work_Order_Priority_Number__c;
                this._woas[i].woStatus = this._woas[i].Maintenance_Work_Order__r.User_Status_Code__c;
                this._woas[i].woRoute = this._woas[i].Maintenance_Work_Order__r.Plant_Section__c;
                this._woas[i].woaStatus = this._woas[i].Status__c;
                if(this._woas[i].Status_Reason__c) {
                    this._woas[i].woaStatus += ' ' + this._woas[i].Status_Reason__c;
                }

                this.generateUrl(this._woas[i].Maintenance_Work_Order__c, i, 'woUrl');
                this._woas[i].woName = this._woas[i].Maintenance_Work_Order__r.Name;

                if(value.length === 1) {
                    this.selectedWOAs = [];
                    this.selectedWOAs.push(this._woas[i]);
                }
            }

        }
        if(!promiseRunning) {
            this.hideTable = false;
            this.actionInProgress = false;
        }
    }

    resetIndexCounterLimit(data){
        this._indexCounterLimit = 0;
        for(let i = 0; i < data.length; i++) {
            this._indexCounterLimit += 2;
            if(data[i].Assigned_Vendor__r !== undefined) {
                this._indexCounterLimit++;
            }

        }
    }

    generateUrl(recordId, index, propertyName) {
        var navigationLinkRef = {
            type: 'standard__recordPage',
            attributes: {
                recordId: recordId,
                actionName: 'view'
            }
        };

        // Set the link's HREF value so the user can click "open in new tab" or copy the link...
        this[NavigationMixin.GenerateUrl](navigationLinkRef)
            .then((url) => {
                this._woas[index][propertyName] = url;
                this._indexCounter++;
                if(this._indexCounter == this._indexCounterLimit) {
                    this.hideTable = false;
                    this.actionInProgress = false;
                }
            }
        );
    }

    /*
    Gets selected Tradesmen from Tradesman list component
    */
    getSelectedTradesmen() {
        if(this._selectedTradesmen) {
            return this._selectedTradesmen;
        }

        const tradesmanList = this.template.querySelector('c-vtt_woa_manage-assignments_tradesman-list');

        if(tradesmanList) {
            return tradesmanList.getSelectedTradesmen();
        }

        return undefined;
    }

    /*
    Gets selected Vendor Id from Tradesman list component
    */
    getSelectedVendorId() {
        if(this._selectedVendor) {
            return this._selectedVendor;
        }

        const tradesmanList = this.template.querySelector('c-vtt_woa_manage-assignments_tradesman-list');

        if(tradesmanList) {
            return tradesmanList.getSelectedVendorId();
        }

        return undefined;
    }

    handleRowSelection(evt) {
        this.selectedWOAs = evt.detail.selectedRows;
    }

    handleActionStart() {
        this.actionInProgress = true;
    }

    handleActionStop() {
        this.actionInProgress = false;
        this._clearInProgress = false;
    }

    hasWOARecords() {
        return this._woas != undefined
            && this._woas.length > 0;
    }

    hasOneWOARecord() {
        return this.hasWOARecords()
            && this._woas.length === 1;
    }

    showActions() {
        return this.hasWOARecords()
               && !this.actionInProgress;
    }

    isSelectedVendor() {
        let selectedVendorId = this.getSelectedVendorId();
        return selectedVendorId != undefined
                && selectedVendorId != '';
    }

    isSelectedWOA() {
        return this.selectedWOAs != undefined
                && this.selectedWOAs.length > 0;
    }

    isSupportedMode() {
        return this.isStandaloneMode()
            || this.isDependentMode();
    }

    isStandaloneMode() {
        return this.mode === MODE_STANDALONE;
    }

    isDependentMode() {
        return this.mode === MODE_DEPENDENT;
    }

//GETTERS
    get showBasicActions() {
        return this.showActions();
    }

    get showAdvancedActions() {
        return this.showActions()
                && this.isSelectedVendor()
                && this.isSelectedWOA()
                && this.isSupportedMode();
    }

    get hasMissingRecordsError() {
        return !this.hasWOARecords()
            && this.isSupportedMode()
            && !this.actionInProgress;
    }

    get hasOneRecord() {
        return this.hasOneWOARecord();
    }

    get hasStandaloneMode() {
        return this.mode === MODE_STANDALONE;
    }

    get hasSupportedMode() {
        return this.isSupportedMode();
    }

    get withoutError() {
        return this.hasWOARecords()
            && this.isSupportedMode()
            && !this._initialLoading;
    }

    get isRefreshAvailable() {
        return this.isStandaloneMode()
            && this.showActions();
    }

    get clearInProgress() {
        return this._clearInProgress;
    }

}