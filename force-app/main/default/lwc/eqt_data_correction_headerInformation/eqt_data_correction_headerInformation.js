/**
 * Created by MarosGrajcar on 2/13/2020.
 */

import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getPicklistValues from '@salesforce/apex/HOG_PicklistServiceLtng.getPicklist';

export default class EqtDataCorrectionHeaderInformation extends NavigationMixin(LightningElement) {

    @api equipmentForm;
    @api isView;
    @api expirationDate;
    @track equipmentStatusOptions;
    //todo pass by event the value from picklist
    @track equipmentStatus;

    _equipment;
    @api
    get equipment() {
        return this._equipment;
    }

    set equipment(value) {
        this._equipment = value;
        let context = this;
        this.pickListValues('Equipment_Correction_Form__c', 'Equipment_Status__c', function (value) {
            context.equipmentStatusOptions = value;
        });
    }

    @api
    changeHeaderState() {
        if (this.isView) {
            let context = this;
            this.pickListValues('Equipment_Correction_Form__c', 'Equipment_Status__c', function (value) {
                context.equipmentStatusOptions = value;
            });
        }
        this.isView = !this.isView;
    }

    pickListValues(nameOfObject, nameOfField, callbackFunction) {
        getPicklistValues({objectName: nameOfObject, fieldName: nameOfField}).then(result => {
            callbackFunction(result);
        });
    }

    handleEquipmentStatus(event) {
        this.equipmentStatus = event.target.value;
        const evt = new CustomEvent('equipmentstatus', {detail: this.equipmentStatus});
        this.dispatchEvent(evt);
    }

    redirectToRecordPage(event) {
        var recordId = event.currentTarget.dataset.id;
        var apiName = event.currentTarget.dataset.apiname;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: apiName,
                actionName: 'view',
                recordId: recordId
            },
        });
    }
}