/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 28/08/2019 - Created.
*************************************************************************************************/
import { LightningElement, track, api } from 'lwc';
import saveFilter from '@salesforce/apex/VTT_WOA_SearchCriteria_Endpoint.saveFilter';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

const SAVE_BUTTON_ENABLED_LABEL = 'Save';
const SAVE_BUTTON_DISABLED_LABEL = 'Saving...';

export default class VTT_WOAListViewSavePopup extends LightningElement {
//VARS
    filterName = '';
    @track errorMessage = '';
    @track loading = false;
    @track saveButtonLabel = SAVE_BUTTON_ENABLED_LABEL;
    @track buttonsDisabled = false;

    @api filter; //VTT_WOA_SearchCriteria

//HANDLERS/ACTIONS
    handleOnChange(evt) {
        this.filterName = evt.detail.value;
    }

    handleCancel() {
        this.loading = true;
        this.reInit();
        this.sendCloseEvent(false);
    }

    handleSave() {
        if(this.filterName) {
            this.buttonsDisabled = true;
            this.saveButtonLabel = SAVE_BUTTON_DISABLED_LABEL;
            this.loading = true;
            saveFilter({filterString: JSON.stringify(this.filter), filterName: this.filterName})
                    .then(result => {
                        this.handleSaveSuccess(result);
                    })
                    .catch(error => {
                        this.errorMessage = error;
                        this.showErrorToast();
                        this.loading = false;
                        this.buttonsDisabled = false;
                        this.saveButtonLabel = SAVE_BUTTON_ENABLED_LABEL;
                    });
        } else {
            this.errorMessage = 'Filter Name is required.';
        }
    }

    handleSaveSuccess(result) {
        this.loading = false;
        this.buttonsDisabled = false;
        this.saveButtonLabel = SAVE_BUTTON_ENABLED_LABEL;
        this.sendCloseEvent(true);
    }

//HELPERS
    reInit() {
        this.errorMessage = '';
        this.filterName = '';
    }

    sendCloseEvent(doReload) {
        const saveEvent = new CustomEvent('close', {detail: doReload});
        this.dispatchEvent(saveEvent);
    }

    showErrorToast() {
        const event = new ShowToastEvent({
            title: 'Error',
            type: 'error',
            message: this.errorMessage
        });
        this.dispatchEvent(event);
    }

    showSuccessToast() {
        const event = new ShowToastEvent({
            title: 'Success',
            type: 'success',
            message: 'Your filter successfully was saved.'
        });
        this.dispatchEvent(event);
    }

}