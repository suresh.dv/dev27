/**
 * Created by MarosGrajcar on 12/9/2019.
 */

import { LightningElement, track, api } from 'lwc';
import getFLOCKEquipment from '@salesforce/apex/Equipment_Forms_Endpoint.getFLOCKEquipment';
import somethingMore from '@salesforce/apex/Equipment_Forms_Endpoint.somethingMore';

export default class EqtTableView extends LightningElement {

    @track superiorEquipments = [];
    @track childEquipments;

    @track mapOfValues = [];
    @track openModal = false;

    @track isExpanded = false;
    @track loading = true;


    _recordId;
    @api
    get recordId() {
        return this._recordId;
    }

    set recordId(value) {
        this._recordId = value;
        this.flockEquipment();
    }

     //Get list of AMUs
    flockEquipment() {
        getFLOCKEquipment({flockId : this.recordId}).then(result => {
            this.superiorEquipments = result[0];
            this.childEquipments = result[1];
        }).catch(error => {
            console.log('error ' +error);
            console.log('Error ' +error.body.message);
        }).finally(() => {
            this.loading = false;
        });
    }

    handleOpenChildEquipment(event) {
        var event = event.target.dataset.index;
    }
}