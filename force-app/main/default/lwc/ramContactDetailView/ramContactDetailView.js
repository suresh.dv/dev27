import { LightningElement, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';

import updateRaMContact from '@salesforce/apex/RaMPortalController.updateContact';
import getAssignments from '@salesforce/apex/RaMPortalController.getAssignmentsByContact';

import USER_ID from '@salesforce/user/Id';
import ACCOUNT_NAME from '@salesforce/schema/User.Contact.Account.Name';

const assignmentColumns = [
  {label: 'Ticket#', fieldName: 'CaseDetailView', type: 'url',
      typeAttributes: { label:{fieldName:'ticketNumber'}, target: '_self' }},
  {label: 'Summary', fieldName: 'subject'},
  {label: 'Status', fieldName: 'status'},
  {label: 'Work Start Date', fieldName: 'workStart', type: 'date'},
];

export default class RamContactDetailView extends LightningElement {
  @track contactId;
  @track showContactModal = false;
  @track saveButtonLabel = 'Save';
  @track isSaveDisabled = false;  
  @track assignmentRecords;
  @track error;
  @track columns = assignmentColumns;

  @wire(getRecord, { recordId: USER_ID, fields: [ACCOUNT_NAME] }) 
  wiredAccount; 

  connectedCallback() {
    this.contactId = this.getUrlParamValue(window.location.href, 'id');

    getAssignments({
      contactId: this.contactId
    }).then(data => {
      const rows = [];

      data.forEach(function(record) {
        const rec = Object.assign({}, record, {
          CaseDetailView: '/ram-case-detail?caseNumber=' + record.caseNumber,
        });
        rows.push(rec);
      });

      this.assignmentRecords = rows;
      this.error = undefined;
    }).
    catch(error => {
      this.assignmentRecords = undefined;
      this.error = error;      
    })    
  } 

  getUrlParamValue(url, key) {
    return new URL(url).searchParams.get(key);
  }  
  
  get accountName() {
    return getFieldValue(this.wiredAccount.data, ACCOUNT_NAME);
  }  

  async handleContactUpdate(event) {
    event.preventDefault();

    let userContact = {
      contactId: this.contactId,
      firstName: event.detail.fields.FirstName,
      lastName: event.detail.fields.LastName,
      emailAddress: event.detail.fields.Email,
      mobilePhone: event.detail.fields.MobilePhone,
      businessPhone: event.detail.fields.Phone,
      homePhone: event.detail.fields.HomePhone,
      otherPhone: event.detail.fields.OtherPhone,
      title: event.detail.fields.Title,
      description: event.detail.fields.Description,
      mailingStreet: event.detail.fields.MailingStreet,
      mailingCity: event.detail.fields.MailingCity,
      mailingState: event.detail.fields.MailingState,
      mailingPostalCode: event.detail.fields.MailingPostalCode,
      mailingCountry: event.detail.fields.MailingCountry 
    };

    this.disbleSaveButton();

    try{
      await updateRaMContact(userContact);
      this.showSuccessToast('Contact updated successfully');
      this.enableSaveButton();
      this.closeContactModal();

    } catch(error) {
      this.showErrorToast('Error updating contact');
    }

  }

  openContactModal() {
    this.showContactModal = true;
  }

  closeContactModal() {
    this.showContactModal = false;
  }  

  disbleSaveButton() {
    this.isSaveDisabled = true;
    this.saveButtonLabel = 'Saving';
  }

  enableSaveButton() {
    this.isSaveDisabled = false;
    this.saveButtonLabel = 'Save';
  }  

  showSuccessToast(successMessage) {
    const event = new ShowToastEvent({
      variant: 'success',
      message: successMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }

  showErrorToast(errorMessage) {
    const event = new ShowToastEvent({
      variant: 'error',
      message: errorMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }   

}