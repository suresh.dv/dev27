/**
 * Created by MarosGrajcar on 2/12/2020.
 */

import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';


export default class EqtAddHeaderInformation extends NavigationMixin(LightningElement) {

    @api expirationDate;
    @api flocAPI;
    @api addForm;
    @track flockInformation;

    _flocInfo;
    @api
    get flocInfo() {
      return this._flocInfo;
    }

    set flocInfo(value) {
        this._flocInfo = value;
        if (this.flocInfo) {
            this.flockInformation = this.flocInfo.floc;
            this.prepareInformationHeader(this.flocInfo.flocAPI);
        }
    }



    @track location = false;
    @track facility = false;
    @track wellEvent = false;
    @track fel = false;
    @track yard = false;
    @track system = false;
    @track subSystem = false;
    prepareInformationHeader(apiName) {

        switch(apiName) {
            case 'Location__c':
                this.location = true;
                break;
            case 'Facility__c':
                this.facility = true;
                break;
            case 'Well_Event__c':
                this.wellEvent = true;
                break;
            case 'Functional_Equipment_Level__c':
                this.fel = true;
                break;
            case 'Yard__c':
                this.yard = true;
                break;
            case 'System__c':
                this.system = true;
                break;
            case 'Sub_System__c':
                this.subSystem = true;
                break;
        }
    }

    redirectToRecordPage(event) {
        var recordId = event.currentTarget.dataset.id;
        var apiName = event.currentTarget.dataset.apiname;
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: apiName,
                actionName: 'view',
                recordId: recordId
            },
        });
    }

}