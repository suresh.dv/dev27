/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
import { LightningElement, track } from 'lwc';

const FILTER_HIDDEN = 'slds-size_12-of-12 slds-small-size_12-of-12 slds-medium-size_6-of-12 slds-large-size_4-of-12 custom_hide';
const FILTER_VISIBLE = 'slds-size_12-of-12 slds-small-size_12-of-12 slds-medium-size_6-of-12 slds-large-size_4-of-12';
const CONTENT_CLASS_FILTER_HIDDEN = 'slds-size_12-of-12 slds-small-size_12-of-12 slds-medium-size_12-of-12 slds-large-size_12-of-12';
const CONTENT_CLASS_FILTER_VISIBLE = 'slds-size_12-of-12 slds-small-size_12-of-12 slds-medium-size_6-of-12 slds-large-size_8-of-12';

export default class VT_WOAListView extends LightningElement {
//VARS
    showFilter = true;
    @track filterColumnClass = FILTER_VISIBLE;
    @track contentColumnClass = CONTENT_CLASS_FILTER_VISIBLE;
    @track filter; //VTT_WOA_SearchCriteria

//HANDLERS/ACTIONS
    handleSearch(evt) {
        this.handleToggleSearch();
        this.filter = JSON.parse(JSON.stringify(evt.detail));
        const resultPane = this.template.querySelector('c-vtt_woa-list-view_result-card');
        if(resultPane) {
            resultPane.searchToggled();
            resultPane.runSearch(this.filter);
        }
    }

    handleToggleSearch() {
        this.showFilter = !this.showFilter;
        if(this.showFilter) {
            this.filterColumnClass = FILTER_VISIBLE;
            this.contentColumnClass = CONTENT_CLASS_FILTER_VISIBLE;
        } else {
            this.filterColumnClass = FILTER_HIDDEN;
            this.contentColumnClass = CONTENT_CLASS_FILTER_HIDDEN;
        }
    }

}