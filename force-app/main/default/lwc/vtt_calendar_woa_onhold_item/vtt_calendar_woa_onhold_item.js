/**
 * Created by MarcelBrimus on 04/09/2019.
 */

import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class vtt_calendar_woa_onhold_item extends NavigationMixin(LightningElement) {
    @api item;
    @api key;

    get logDate() {
        var date = '';
        if(this.item.Work_Order_Activity_Logs__r){
          date = this.item.Work_Order_Activity_Logs__r.length > 0 ? this.item.Work_Order_Activity_Logs__r[0].Date__c : '';
        }
        return date;
    }

    navigateToRecord(evt){
        evt.preventDefault();
        evt.stopPropagation();
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": this.item.Id,
                "objectApiName": "Work_Order_Activity__c",
                "actionName": "view"
            },
        });
    }
}