import { LightningElement, wire, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

import USER_ID from '@salesforce/user/Id';
import ACCOUNT_NAME from '@salesforce/schema/User.Contact.Account.Name';

import getActivities from '@salesforce/apex/RaMPortalController.getOpenActivityListByTechnician';

const columns = [
  { label: 'Ticket #', fieldName: 'recordDetailView', type: "url",
      typeAttributes: {label:{fieldName:'ticketNumber'}, target: '_self'} },  
  { label: 'Location #', fieldName: 'locationNumber' },
  { label: 'Summary', fieldName: 'subject' },
  { label: 'Assigned Date/Time', fieldName: 'assignedDate', type: 'date',
      typeAttributes:{ year: "numeric", month: "long", day: "2-digit", hour: "2-digit", minute: "2-digit" } },
  { label: 'Is Primary ?', fieldName: 'isPrimary', type: 'boolean' },    
];

export default class RamOpenTaskListView extends NavigationMixin(LightningElement) {
  @track openActivities;
  @track inProgressActivities;
  @track pendingActivities;
  @track error;
  @track columns = columns;

  @wire(getRecord, { recordId: USER_ID, fields: [ACCOUNT_NAME] }) 
  wiredAccount;  
  
  get accountName() {
    return getFieldValue(this.wiredAccount.data, ACCOUNT_NAME);
  }
  
  connectedCallback() {
    this.showMyActivities();
  }

  showMyActivities() {

    getActivities({
      userId: USER_ID
    }).then(data => {
      const openActRows = [];
      const inProgRows = [];
      const pendingRows = [];
        
        data.forEach(function(record) {
          let row = {};
          
          if(record.tkt.status == 'Assigned') {
            row.recordDetailView = '/ram-activity-detail?id=' + record.recordId
            row.ticketNumber = record.tkt.ticketNumber;
            row.locationNumber = record.loc.locationNumber;
            row.subject = record.tkt.subject;
            row.assignedDate = record.tkt.assignedDate;
            row.isPrimary = record.tkt.isPrimaryTechnician;

            openActRows.push(row);
          } else if(record.tkt.status == 'Work in Progress') {
            row.recordDetailView = '/ram-activity-detail?id=' + record.recordId
            row.ticketNumber = record.tkt.ticketNumber;
            row.locationNumber = record.loc.locationNumber;
            row.subject = record.tkt.subject;
            row.assignedDate = record.tkt.assignedDate; 
            row.isPrimary = record.tkt.isPrimaryTechnician;

            inProgRows.push(row);
          } else if(record.tkt.status == 'Pending Parts') {
            row.recordDetailView = '/ram-activity-detail?id=' + record.recordId
            row.ticketNumber = record.tkt.ticketNumber;
            row.locationNumber = record.loc.locationNumber;
            row.subject = record.tkt.subject;
            row.assignedDate = record.tkt.assignedDate;
            row.isPrimary = record.tkt.isPrimaryTechnician;

            pendingRows.push(row);
          } else if(record.tkt.status == 'Pending Approval') {
            row.recordDetailView = '/ram-activity-detail?id=' + record.recordId
            row.ticketNumber = record.tkt.ticketNumber;
            row.locationNumber = record.loc.locationNumber;
            row.subject = record.tkt.subject;
            row.assignedDate = record.tkt.assignedDate;
            row.isPrimary = record.tkt.isPrimaryTechnician;

            pendingRows.push(row);
          }
        }); 
        
        this.openActivities = openActRows;
        this.inProgressActivities = inProgRows;
        this.pendingActivities = pendingRows;      
        this.error = undefined;        

    }).
    catch(error => {
      this.showErrorToast('Error loading data')
    })
  }

  showSuccessToast(successMessage) {
    const event = new ShowToastEvent({
      variant: 'success',
      message: successMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }

  showErrorToast(errorMessage) {
    const event = new ShowToastEvent({
      variant: 'error',
      message: errorMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }    

}