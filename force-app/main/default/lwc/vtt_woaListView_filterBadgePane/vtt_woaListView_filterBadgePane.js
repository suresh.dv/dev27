/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 30/08/2019 - Created.
*************************************************************************************************/
import { LightningElement, api } from 'lwc';

export default class VTT_WOAListViewFilterBadgePane extends LightningElement {
//VARs
    @api filter; //VTT_WOA_SearchCriteria

//GETTERS
    get vendor() {
        return 'Vendor: ' + this.filter.vendorName;
    }

    get isSelectedVendor() {
        return this.filter !== undefined
                && this.filter.vendorFilter;
    }

    get tradesman() {
        return 'Tradesman: ' + this.filter.tradesmanName;
    }

    get isSelectedTradesman() {
        return this.filter !== undefined
                && this.filter.tradesmanFilter;
    }

    get scheduledFrom() {
        return 'Scheduled From: ' + this.filter.scheduledFromFilter;
    }

    get isSelectedScheduledFrom() {
        return this.filter !== undefined
                && this.filter.scheduledFromFilter;
    }

    get scheduledTo() {
        return 'Scheduled To: ' + this.filter.scheduledToFilter;
    }

    get isSelectedScheduledTo() {
        return this.filter !== undefined
                && this.filter.scheduledToFilter;
    }

    get AMU() {
        return 'AMU: ' + this.filter.AMUName;
    }

    get isSelectedAMU() {
        return this.filter !== undefined
                && this.filter.amuFilter;
    }

    get operatingRoute() {
        return 'Operating Route: ' + this.filter.routeFilter;
    }

    get isSelectedOperatingRoute() {
        return this.filter !== undefined
                && this.filter.routeFilter;
    }

    get plannerGroup() {
        return 'Planner Group: ' + this.filter.plannerGroupFilter;
    }

    get isSelectedPlannerGroup() {
        return this.filter !== undefined
                && this.filter.plannerGroupFilter;
    }

    get activityStatus() {
        return 'Activity Status: ' + this.listToString(this.filter.activityMultiStatusFilter);
    }

    get isSelectedActivityStatus() {
        return this.filter !== undefined
                && this.filter.activityMultiStatusFilter
                && this.filter.activityMultiStatusFilter.length > 0;
    }

    get hideCompleted() {
        return 'Hide Completed: ' + (this.filter.hideCompletedFilter ? 'yes' : 'no');
    }

    get isSelectedHideCompleted() {
        return this.filter !== undefined;
    }

    get anyText() {
        return 'Any Text: ' + this.filter.filterName;
    }

    get isSelectedAnyText() {
        return this.filter !== undefined
                && this.filter.filterName;
    }

//HELPERS
    listToString(listToConvert) {
        let tmpString = '';

        for(let i = 0; i < listToConvert.length; i++) {
            if(tmpString !== '') {
                tmpString = tmpString + ', ';
            }
            tmpString = tmpString + listToConvert[i];
        }

        return tmpString;
    }

}