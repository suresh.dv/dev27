/**
 * Created by MarosGrajcar on 11/29/2019.
 */

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getPicklistValues from '@salesforce/apex/HOG_PicklistServiceLtng.getPicklist';
import getUserPermission from '@salesforce/apex/Equipment_Forms_Endpoint.getUserPermission';
import getExpirationDate from '@salesforce/apex/Equipment_Forms_Endpoint.getExpirationDate';
import loadEquipmentForDataUpdateForm from '@salesforce/apex/Equipment_Forms_Endpoint.loadEquipmentForNewDataUpdateForm';
import loadCorrectionForm from '@salesforce/apex/Equipment_Forms_Endpoint.loadCorrectionForm';
import updateDataUpdateForm from '@salesforce/apex/Equipment_Forms_Endpoint.updateDataUpdateForm';
import insertNewDataUpdateForm from '@salesforce/apex/Equipment_Forms_Endpoint.insertNewDataUpdateForm';
import rejectCorrectionEquipmentForm from '@salesforce/apex/Equipment_Forms_Endpoint.rejectCorrectionEquipmentForm';
import processCorrectionEquipmentForm from '@salesforce/apex/Equipment_Forms_Endpoint.processCorrectionEquipmentForm';

export default class EqtDataUpdateCru extends NavigationMixin(LightningElement) {

    /* VARIABLES */

    @api isOnCommunity = false;

    /* EQUIPMENT VARIABLES */
    _equipmentId;
    @track equipment; // Equipment__c SObject
    @track equipmentForm; // Equipment_Correction_Form__c SObject
    @track description;
    @track manufacturer;
    @track modelNo;
    @track manufacturerSerialNo;
    @track tagNumber;
    @track safetyCriticalEq;
    @track expirationDate;
    @track regulatoryEq;
    @track comment;
    @track reason;
    @track equipmentStatus;
    @track equipmentStatusOptions;
    @track openModal;

    @track endpointForm = {
            "Id" : '',
            "Description_old__c" : '',
            "Description_new__c" : '',
            "Manufacturer_old__c" : '',
            "Manufacturer_new__c" : '',
            "Model_Number_old__c" : '',
            "Model_Number_new__c" : '',
            "Manufacturer_Serial_No_old__c" : '',
            "Manufacturer_Serial_No_new__c" : '',
            "Tag_Number_old__c" : '',
            "Tag_Number_new__c" : '',
            "Safety_Critical_Equipment__c" : '',
            "Regulatory_Equipment__c" : '',
            "Expiration_Date__c" : '',
            "Comments__c" : '',
            "Reason__c" : '',
            "Equipment_Status__c" : ''};

    formRejectModal() {
        this.openModal = !this.openModal;
    }
    rejectForm() {
        this.loading = true;
        rejectCorrectionEquipmentForm({formId : this.recordId, rejectionReason : this.reason}).then(result => {
                this.showToast('', 'Add Missing Equipment Form has been successfully rejected ', 'success');
                this.isView = true;
                this.openModal = false;
                this.loadForm();
            }).catch(error => {
                let errorMsg;
                if(error.body) {
                    errorMsg = error.body.message;
                } else {
                    errorMsg = error;
                }
                console.log('Error ' +errorMsg);
                this.showToast('', errorMsg, 'error');
                this.loading = false;
            });
    }

     formProcessed() {
        this.loading = true;
        processCorrectionEquipmentForm({formId : this.recordId}).then(result => {
                this.showToast('', 'Add Missing Equipment Form has been successfully proceed', 'success');
                this.isView = true;
                this.loadForm();
            }).catch(error => {
                let errorMsg;
                if(error.body) {
                    errorMsg = error.body.message;
                } else {
                    errorMsg = error;
                }
                console.log('Error ' +errorMsg);
                this.showToast('', errorMsg, 'error');
                this.loading = false;
            });
    }

    handleEquipmentStatus(event) {
        this.equipmentStatus = event.target.value;
    }


    pickListValues(nameOfObject, nameOfField, callbackFunction) {
        getPicklistValues({objectName: nameOfObject, fieldName: nameOfField}).then(result => {
            callbackFunction(result);
        });
    }

    /* STATE VARIABLES */
    @track isView = false;
    @track isNewForm = true;
    @track loading = true;
    @track isUserAdmin = false; // track user permission. If true, user can Reject Form and Edit Reason and Expiration date, when false user cannot Reject neither Edit
    @track formClosed = false;

    /* ATTACHMENT FILE VARIABLES */
    @track attachmentFile;
    @track attachmentFileName;
    attachmentFileContents;
    attachmentFileReader;
    attachmentContent;
    @track insertAttachmentFile = false;
    @track uploadedAttachmentFile;
    @track uploadedAttachment;

    /* COMPONENT INIT */

    // check for record Id. If true, load form, when false set it to new form
    @track _recordId;

    @api
    get recordId() {
        return this._recordId;
    }

    set recordId(value) {
        this._recordId = value;
        if (this._recordId) {
            this.checkUserPermission();
            this.loadForm();
        } else {
            this.loadEquipment();
            this.checkUserPermission();
            this.setExpirationDate();
        }
    }

    _equipmentId;
        @api
        get equipmentId() {
          return this._equipmentId;
        }

        set equipmentId(value) {
            this._equipmentId = value;
            let context = this;
            this.pickListValues('Equipment_Correction_Form__c', 'Equipment_Status__c', function (value) {
                context.equipmentStatusOptions = value;
            });
            this.setExpirationDate();
            this.checkUserPermission();
            this.loadEquipment();
    }



    /* ENDPOINTS (CALLBACKS) */
    loadEquipment() {
        loadEquipmentForDataUpdateForm({equipmentId : this.equipmentId}).then(result => {
            this.equipment = result;
            this.loading = false;
        }).catch(error => {
        console.log('loading eq error');
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

    loadForm() {
        loadCorrectionForm({formId : this.recordId}).then(result => {
            this.equipmentForm = result.correctionForm;
            this.uploadedAttachmentFile = null;
            this.uploadedAttachment = null;

            if (this.formIsClosed()) {
                this.formClosed = true;
            }
            if (result.correctionFormFiles) {
                this.uploadedAttachmentFile = result.correctionFormFiles[0];
                this.containsAttachment = true;
            }

            if (result.correctionFormAttachments) {
                this.uploadedAttachment = result.correctionFormAttachments[0];
                this.containsAttachment = true;
            }

            this.insertAttachmentFile = false;

            this.isView = true;
            this.isNewForm = false;
            this.loading = false;
        }).catch(error => {
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

    formIsClosed() {
        if (this.equipmentForm.Status__c == 'Auto Closed'
        || this.equipmentForm.Status__c == 'Rejected Closed'
        || this.equipmentForm.Status__c == 'Processed') {
            return true;
        } else {
            return false;
        }
    }

    insertForm() {
        if (this.validateForm()) {
            this.loading = true;
            this.endpointForm.Description_old__c = this.equipment.Description_of_Equipment__c;
            this.endpointForm.Manufacturer_old__c = this.equipment.Manufacturer__c;
            this.endpointForm.Model_Number_old__c = this.equipment.Model_Number__c;
            this.endpointForm.Manufacturer_Serial_No_old__c = this.equipment.Manufacturer_Serial_No__c;
            this.endpointForm.Tag_Number_old__c = this.equipment.Tag_Number__c;

            insertNewDataUpdateForm({equipmentId: this.equipmentId,
                                correctionForm : this.endpointForm,
                                attachmentBase64Data : encodeURIComponent(this.attachmentFileContents),
                                attachmentFileName : this.attachmentFileName}).then(result => {
                this.showToast('', result, 'success');
                this.dispatchEvent(new CustomEvent('successfullinsert'));
            }).catch(error => {
                let errorMsg;
                if(error.body) {
                    errorMsg = error.body.message;
                } else {
                    errorMsg = error;
                }
                console.log('Error ' +errorMsg);
                this.showToast('', errorMsg, 'error');
                this.loading = false;
            });
        } else {
            this.showToast('', 'Please, fill at least one field before saving the form.', 'warning');
        }
    }

    closeForm() {
        this.dispatchEvent(new CustomEvent('closeOpenedForm'));
    }

    updateForm() {
        if (this.validateFormBeforeUpdate()) {
            this.loading = true;

             let handleUploadsObject = {
                            "insertAttachment" : this.insertAttachmentFile,
                            "originalAttachmentFileId" : this.originalAttachmentFileId,
                            "attachmentFileName" : this.attachmentFileName,
                            "attachmentBase64Data" : encodeURIComponent(this.attachmentFileContents),
                            "originalAttachmentId" : this.originalAttachmentId};

            updateDataUpdateForm({correctionForm : this.endpointForm,
                                handleUploads : handleUploadsObject}).then(result => {
                this.showToast('', 'Data Update Form has been successfully updated ', 'success');
                this.isView = true;
                this.loadForm();
            }).catch(error => {
                let errorMsg;
                if(error.body) {
                    errorMsg = error.body.message;
                } else {
                    errorMsg = error;
                }
                console.log('Error ' +errorMsg);
                this.showToast('', errorMsg, 'error');
                this.loading = false;
            });
        } else {
            this.showToast('', 'Please, make a change to at least one field before saving the form.', 'warning');
        }
    }

    checkUserPermission() {
        getUserPermission().then(result => {
            this.isUserAdmin = result;
        }).catch(error => {
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

    setExpirationDate() {
        getExpirationDate().then(result => {
            this.expirationDate = result;
        }).catch(error => {
           this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: error.message,
               variant: 'error',
               }),
           );
        });
    }

    /* FUNCTIONS */
    showToast(title, message, toastType) {
        const toastEvent = new ShowToastEvent({title : title, message : message, variant : toastType});
        this.dispatchEvent(toastEvent);
    }

    setupInitialForm() {
        this.endpointForm.Id = this.equipmentForm.Id;
        this.endpointForm.Description_old__c = this.equipmentForm.Description_old__c;
        this.endpointForm.Description_new__c = this.equipmentForm.Description_new__c;
        this.endpointForm.Manufacturer_old__c = this.equipmentForm.Manufacturer_old__c;
        this.endpointForm.Manufacturer_new__c = this.equipmentForm.Manufacturer_new__c;
        this.endpointForm.Manufacturer_Serial_No_old__c = this.equipmentForm.Manufacturer_Serial_No_old__c;
        this.endpointForm.Manufacturer_Serial_No_new__c = this.equipmentForm.Manufacturer_Serial_No_new__c;
        this.endpointForm.Model_Number_old__c = this.equipmentForm.Model_Number_old__c;
        this.endpointForm.Model_Number_new__c = this.equipmentForm.Model_Number_new__c;
        this.endpointForm.Tag_Number_old__c = this.equipmentForm.Tag_Number_old__c;
        this.endpointForm.Tag_Number_new__c = this.equipmentForm.Tag_Number_new__c;
        this.endpointForm.Comments__c = this.equipmentForm.Comments__c;
        this.endpointForm.Reason__c = this.equipmentForm.Reason__c;
        this.endpointForm.Safety_Critical_Equipment__c = this.equipmentForm.Safety_Critical_Equipment__c;
        this.endpointForm.Regulatory_Equipment__c = this.equipmentForm.Regulatory_Equipment__c;
        this.endpointForm.Expiration_Date__c = this.equipmentForm.Expiration_Date__c;

        this.safetyCriticalEq = this.equipmentForm.Safety_Critical_Equipment__c;
        this.regulatoryEq = this.equipmentForm.Regulatory_Equipment__c;
        this.expirationDate = this.equipmentForm.Expiration_Date__c;



        this.changeHeaderInformationState();

        this.equipmentStatus = this.equipmentForm.Equipment_Status__c;

        if (this.uploadedAttachmentFile || this.uploadedAttachment) {
            this.containsAttachment = true;
        } else {
            this.containsAttachment = false;
        }
    }

    setupForm() {
        this.endpointForm.Id = this.correctionForm.Id;
        this.endpointForm.Description__c = this.correctionForm.Description__c;
        this.endpointForm.Manufacturer__c = this.correctionForm.Manufacturer__c;
        this.endpointForm.Manufacturer_Serial_No__c = this.correctionForm.Manufacturer_Serial_No__c;
        this.endpointForm.Safety_Critical_Equipment__c = this.correctionForm.Safety_Critical_Equipment__c;
        this.endpointForm.Part_of_Package__c = this.correctionForm.Part_of_Package__c;
        this.endpointForm.Model_Number__c = this.correctionForm.Model_Number__c;
        this.endpointForm.Tag_Number__c = this.correctionForm.Tag_Number__c;
        this.endpointForm.P_ID__c = this.correctionForm.P_ID__c;
        this.endpointForm.Regulatory_Equipment__c = this.correctionForm.Regulatory_Equipment__c;
        this.endpointForm.Comments__c = this.correctionForm.Comments__c;
        this.endpointForm.Reason__c = this.correctionForm.Reason__c;
        this.endpointForm.Expiration_Date__c = this.correctionForm.Expiration_Date__c;
    }

    validateForm() {
        return this.endpointForm.Description_new__c != ''
                || this.endpointForm.Manufacturer_new__c != ''
                || this.endpointForm.Model_Number_new__c != ''
                || this.endpointForm.Manufacturer_Serial_No_new__c != ''
                || this.endpointForm.Tag_Number_new__c != ''
                || this.endpointForm.Comments__c != ''
                || this.endpointForm.Equipment_Status__c != null
                || this.attachmentFileName != null;
    }

    validateFormBeforeUpdate() {
        return this.endpointForm.Description_new__c != this.equipmentForm.Description_new__c
                || this.endpointForm.Equipment_Status__c != this.equipmentForm.Equipment_Status__c
                || this.endpointForm.Manufacturer_new__c != this.equipmentForm.Manufacturer_new__c
                || this.endpointForm.Model_Number_new__c != this.equipmentForm.Model_Number_new__c
                || this.endpointForm.Manufacturer_Serial_No_new__c != this.equipmentForm.Manufacturer_Serial_No_new__c
                || this.endpointForm.Tag_Number_new__c != this.equipmentForm.Tag_Number_new__c
                || this.endpointForm.Safety_Critical_Equipment__c != this.equipmentForm.Safety_Critical_Equipment__c
                || this.endpointForm.Regulatory_Equipment__c != this.equipmentForm.Regulatory_Equipment__c
                || this.endpointForm.Comments__c != this.equipmentForm.Comments__c
                || this.endpointForm.Reason__c != this.equipmentForm.Reason__c
                || this.attachmentFileHasChanged
                || this.attachmentHasChanged
                || this.attachmentFileName != null;
    }

    /* USER ACTIONS */
    editViewState() {
        this.setupInitialForm();
        this.isView = false;
    }

    cancelEditView() {
        if (this.uploadedAttachmentFile && this.attachmentFileHasChanged) {
            this.attachmentFileName = this.uploadedAttachmentFile.ContentDocument.Title;
            this.insertAttachmentFile = false;
            this.attachmentFileHasChanged = !this.attachmentFileHasChanged;
        }

         if (this.attachmentHasChanged) {
            this.attachmentHasChanged = !this.attachmentHasChanged;
        }

        this.insertAttachmentFile = false;
        this.originalAttachmentFileId = null;
        this.originalAttachmentId = null;
        this.insertAttachmentFile = false;

        this.changeHeaderInformationState();

        this.isView = true;

    }

    changeHeaderInformationState() {
        var changeView = this.template.querySelector('c-eqt_data_correction_header-information');
        if (changeView) {
            changeView.changeHeaderState();
        }
    }

    @api
    getUserInputs() {
        var sectionComments = this.template.querySelector('c-eqt_comments-section');
        	if (sectionComments) {
                var userComments = sectionComments.getComments();
                this.comment = userComments.Comments__c;
                this.reason = userComments.Reason__c;
        }
        var saveMethod = this.template.querySelector('c-eqt_data_correction-section');
        if (saveMethod) {
            this.endpointForm = saveMethod.userInputs();
        }

        if (this.recordId) {
            this.endpointForm.Equipment_Status__c = this.equipmentStatus;
            this.endpointForm.Expiration_Date__c = this.expirationDate;
            this.endpointForm.Comments__c = this.comment;
            this.endpointForm.Reason__c = this.reason;
            this.endpointForm.Safety_Critical_Equipment__c = this.safetyCriticalEq;
            this.endpointForm.Regulatory_Equipment__c = this.regulatoryEq;
            this.updateForm()
        } else {
            this.endpointForm.Id = null;
            this.endpointForm.Equipment_Status__c = this.equipmentStatus;
            this.endpointForm.Expiration_Date__c = this.expirationDate;
            this.endpointForm.Comments__c = this.comment;
            this.endpointForm.Reason__c = this.reason;
            this.endpointForm.Safety_Critical_Equipment__c = this.safetyCriticalEq;
            this.endpointForm.Regulatory_Equipment__c = this.regulatoryEq;
            this.insertForm();
        }
    }

    originalAttachmentFileId;
    originalAttachmentId;
    attachmentFileHasChanged = false;
    attachmentHasChanged = false;
    @track containsAttachment = true;

    removeFile(event) {
        var recordId = event.currentTarget.dataset.id;
        var objectType = event.currentTarget.dataset.objecttype;
        this.attachmentFileName = null;
        this.containsAttachment = false;

        if (objectType == 'file') {
            this.originalAttachmentFileId = recordId;
            this.attachmentFileHasChanged = true;

        } else if (objectType == 'attachment') {
            this.originalAttachmentId = recordId;
            this.attachmentHasChanged = true;
        }

    }

    /* HANDLERS */

    setEquipmentStatus(event) {
        this.equipmentStatus = event.detail;
    }

    handleDescription(event) {
        this.description = event.target.value;
    }

    handleManufacturer(event) {
        this.manufacturer = event.target.value;
    }

    handleModelNo(event) {
        this.modelNo = event.target.value;
    }

    handleManufacturerSerialNo(event) {
        this.manufacturerSerialNo = event.target.value;
    }

    handleTagNumber(event) {
        this.tagNumber = event.target.value;
    }
    handleSafetyCriticalEq(event) {
        this.safetyCriticalEq = !this.safetyCriticalEq;
    }

    handleRegulatoryEq(event) {
        this.regulatoryEq = !this.regulatoryEq;
    }

    handleComment(event) {
        this.comment = event.target.value;
    }

    handleReason(event) {
        this.reason = event.target.value;
    }

    handleExpirationDate(event) {
        this.expirationDate = event.target.value;
    }

    handleAttachmentFileUpload(event) {
        this.attachmentFile = event.target.files;
        this.attachmentFileName = event.target.files[0].name;
        this.attachmentFileReader = new FileReader();
        this.attachmentFileReader.readAsDataURL(this.attachmentFile[0]);
        this.attachmentFileReader.onloadend = (() => {
            this.attachmentFileContents = this.attachmentFileReader.result;
            let base64 = 'base64,';
            this.attachmentContent = this.attachmentFileContents.indexOf(base64) + base64.length;
            this.attachmentFileContents = this.attachmentFileContents.substring(this.attachmentContent);
        });
        this.insertAttachmentFile = true;
    }


    /* NAVIGATION */
    navigateToUser(event) {
        var userId = event.currentTarget.dataset.userid;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'User',
                actionName: 'view',
                recordId: userId
            },
        });
    }

    redirectToRecordPage(event) {
        var recordId = event.currentTarget.dataset.id;
        var apiName = event.currentTarget.dataset.apiname;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: apiName,
                actionName: 'view',
                recordId: recordId
            },
        });
    }

    openFilePreview(event) {
       if(this.isOnCommunity) {
           this[NavigationMixin.Navigate]({
               type: 'standard__recordPage',
               attributes: {
                   objectApiName: "ContentDocument",
                   actionName: 'view',
                   recordId: event.currentTarget.dataset.id
               },
           });
       } else {
           this[NavigationMixin.Navigate]({
           type: 'standard__namedPage',
           attributes: {
               pageName: 'filePreview'
           },
           state : {
               selectedRecordId:event.currentTarget.dataset.id
           }
           });
       }
    }

}