/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 21/01/2020 - Created.
*************************************************************************************************/
const MODE_STANDALONE = 'STANDALINE';
const MODE_LOCATION = 'LOCATION';
const ERROR_INVALID_USER = 'Only Production Engineer or Operation Engineer can create Vent Gas Alert';
const MAX_FILE_SIZE = 34078720; //something around 25MB

import { LightningElement, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import { NavigationMixin } from 'lightning/navigation';

import getProductionEngineer from '@salesforce/apex/VG_AlertEndpoint.getProductionEngineer';
import isValidLocation from '@salesforce/apex/VG_AlertEndpoint.isValidLocation';
import isValidUser from '@salesforce/apex/VG_AlertEndpoint.isValidUser';
import saveFiles from '@salesforce/apex/VG_AlertEndpoint.saveFiles';

export default class VG_AlertNew extends NavigationMixin(LightningElement) {
//VARS
    @api mode = MODE_STANDALONE;

    @track validUser = true;
    @track alert = {};
    @track formInvalid = true;
    @track loading;
    @track error = undefined;

    @track files = [];
    @track hasFiles = false;

    @track enableEngineerField = false;
    @track displayFilesInput = false;

    _validWell = false;

    _initialValidationRunning = true;
    _loading = false;
    _formLoading = true;

    connectedCallback() {
        this._formLoading = true;
        this._initialValidationRunning = true;
        this.resetLoadingFlag();
        this.validateUser();
    }

    disconnectedCallback() {
        this.files = [];
        this.alert = {};
        this.validUser = true;
        this.formInvalid = true;
        this.hasFiles = false;
        this.enableEngineerField = false;
        this._validWell = false;
        this._initialValidationRunning = true;
        this._loading = false;
        this._formLoading = true;
    }

//APIs
    @api
    get locationId() {
        return this.alert.Well_Location__c;
    }

    set locationId(value) {
        this.alert.Well_Location__c = value;
        this.validateLocation();
    }

//HANDLERS(Actions)
    handleWellLocationChange(evt) {
        this.alert.Well_Location__c = evt.detail.value[0];
        if(this.alert.Well_Location__c) {
            this._loading = true;
            this.resetLoadingFlag();
            this.validateLocation();
        } else {
            this._validWell = false;
            this.validateForm();
            this.getProductionEngineer();
        }
    }

    handleFormError(evt) {
        this.error = 'Error: ' + evt.detail.detail;
        this._loading = false;
        this.resetLoadingFlag();
    }

    handleFormLoad(evt) {
        this._formLoading = false;
        this.resetLoadingFlag();
        this.displayFilesInput = true;
    }

    handleSubmit(evt) {
        this._loading = true;
        this.resetLoadingFlag();
    }

    handleSuccess(evt) {
        let recordId = evt.detail.id;
        if(this.hasFiles) {
            this.storeFiles(recordId);
        } else {
            this.handleRedirect(recordId);
        }
    }

    handleRemoveFile(evt) {
        let newFileList = [];

        for(let i = 0; i < this.files.length; i++) {
            if(i != evt.currentTarget.dataset.index) {
                newFileList.push(this.files[i]);
            }
        }

        this.files = newFileList;
        this.resetHasFilesFlag();
    }

    handleFileSelection(evt) {
        if(evt.target.files.length > 0) {
            for(let i = 0; i < evt.target.files.length; i++) {
                if(evt.target.files[i].size > MAX_FILE_SIZE) {
                    const errorToast = new ShowToastEvent({
                        title: 'File size error',
                        variant: 'error',
                        message: 'File is too large (limit 25MB): ' + evt.target.files[i].name,
                    });
                    this.dispatchEvent(errorToast);
                } else {
                    this.handleFileLoad(evt.target.files[i]);
                }
            }
        }
    }

//HELPERS
    storeFiles(recordId) {
        const infoToast = new ShowToastEvent({
            title: 'File upload',
            message: 'Starting to upload your files. This may take a while.'
        });
        this.dispatchEvent(infoToast);
        saveFiles({
            files: JSON.stringify(this.files),
            alertId: recordId
        }).then(result => {
            this.handleRedirect(recordId);
        }).catch(error => {
            let errorMsg = '';
            if(error.body) {
                errorMsg = error.body.message;
            } else {
                errorMsg = error;
            }
            const errorToast = new ShowToastEvent({
                title: 'File upload error',
                variant: 'error',
                mode: 'sticky',
                message: errorMsg
            });
            this.dispatchEvent(errorToast);
            this.handleRedirect(recordId);
        }).finally(() => {
            this._loading = false;
            this.resetLoadingFlag();
        });
    }

    handleRedirect(recordId) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: 'HOG_Vent_Gas_Alert__c',
                actionName: 'view',
                recordId: recordId
            },
        });
    }

    handleFileLoad(fileToLoad) {
        let fileName = fileToLoad.name;

        let fileReader = new FileReader();

        fileReader.onloadend = (() => {
            let file = {};
            file.name = fileName;
            file.fileContents = fileReader.result;
            let base64 = 'base64,';
            file.content = file.fileContents.indexOf(base64) + base64.length;
            file.fileContents = encodeURIComponent(file.fileContents.substring(file.content));

            this.files.push(file);
            this.resetHasFilesFlag();
        });

        fileReader.readAsDataURL(fileToLoad);
    }

    resetHasFilesFlag() {
        this.hasFiles = this.files && this.files.length > 0;
    }

    validateUser() {
        isValidUser().then(result => {
            this.validUser = result;
            if(!this.validUser) {
                this.error = ERROR_INVALID_USER;
                this._loading = false;
                this._formLoading = false;
            }
        }).catch(error => {
            this.handleError(error);
        }).finally(() => {
            this._initialValidationRunning = false;
            this.resetLoadingFlag();
        });
    }

    validateForm() {
        this.formInvalid = !this._validWell;
    }

    validateLocation() {
        isValidLocation({
            wellId : this.alert.Well_Location__c
        }).then(result => {
            this._validWell = result;
            this.error = undefined;
            this.getProductionEngineer();
        }).catch(error => {
            this.handleError(error);
            this._validWell = false;
        }).finally(() => {
            this._loading = false;
            this.resetLoadingFlag();
            this.validateForm();
        });
    }

    getProductionEngineer() {
        if(this.alert && this.alert.Well_Location__c) {
            this.enableEngineerField = false;
            this._loading = true;
            this.resetLoadingFlag();
            getProductionEngineer({
                wellId : this.alert.Well_Location__c
            }).then(result => {
                this.alert.Production_Engineer__c = result;
                this.error = undefined;
            }).catch(error => {
                this.handleError(error);
            }).finally(() => {
                this._loading = false;
                this.resetDisableFlagForEngineerField();
                this.resetLoadingFlag();
                this.validateForm();
            });
        } else {
            this.alert.Production_Engineer__c = null;
            this.enableEngineerField = false;
        }
    }

    resetDisableFlagForEngineerField() {
        if(!this.alert || !this.alert.Production_Engineer__c) {
            this.enableEngineerField = true;
        }
    }

    handleError(error) {
        if(error.body) {
            this.error = error.body.message;
        } else {
            this.error = error;
        }
    }

    resetLoadingFlag() {
        this.loading = this._loading || this._initialValidationRunning || this._formLoading;
    }

//GETTERS
    get isLocationMode() {
        return this.mode === MODE_LOCATION;
    }

    get isStandaloneMode() {
        return this.mode === MODE_STANDALONE;
    }

}