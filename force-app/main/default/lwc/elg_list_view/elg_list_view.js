/**
 * Created by MarosGrajcar on 8/27/2019.
 */

import {LightningElement, track} from 'lwc';
import getUserDefaultAMU from '@salesforce/apex/ELG_ListView_Endpoint.getUserDefaultAMU';
import getAMUsPosts from '@salesforce/apex/ELG_ListView_Endpoint.getAMUsPosts';
import getSearchResults from '@salesforce/apex/ELG_ListView_Endpoint.getSearchResults';
import getShiftSummarySearchResults from '@salesforce/apex/ELG_ListView_Endpoint.getShiftSummarySearchResults';
import getPicklistValues from '@salesforce/apex/HOG_PicklistServiceLtng.getPicklist';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

//Global variables
var sizeOfOffset = 0;
const numberOfRecords = 50;

export default class HogListView extends LightningElement {

    /* VARIABLES */
    @track showSpinner = true;

    //HANDOVER FORM
    @track handovers;

    //SHIFT SUMMARY
    @track shiftSummaries;
    @track shiftLeadPostId;
    @track showShiftSummaries = false;
    @track showDeciderCheckbox = false;
    @track searchHandovers = false;

    //ERROR
    @track showError = false;
    @track errorMessage;

    //USER SORTING
    @track userSort = false;
    @track checked = false;
    //default sorting
    @track sortByField = 'CreatedDate';
    @track sortingDirection = 'DESC';

    //NOT REVIEWED BY SHIFT ENGINEER
    @track showReviewSelection = false;
    @track changeFilterCriteria = false;
    @track shiftEngineerReview = false;
    @track steamChiefReview = false;
    @track shiftLeadReview = false;
    @track supervisorReview = false;

    //PAGINATION
    @track hasRecords = false;
    @track totalRecords;
    @track currentPage;
    @track pageSize = numberOfRecords;
    @track totalPages;
    @track singlePage = false;

    //PICKLIST OPTIONS VARIABLES
    @track operationFields;
    @track postAMUs = {};
    @track amuPosts;
    @track shiftDayCycles;
    @track handoverStatuses;
    @track shiftSummaryStatuses;

    //CLEAR SPECIFIC SELECTION FROM FILTER (little x icon)
    @track postIsSelected = false;
    @track cycleIsSelected = false;
    @track statusIsSelected = false;
    @track startDateIsSelected = false;
    @track endDateIsSelected = false;

    //VALUE HOLDERS VARIABLES
    @track selectedAMU;
    @track selectedPost;
    @track selectedShiftCycle;
    @track selectedHandoverStatus;
    @track selectedShiftSummaryStatus;
    @track selectedHandoverStatusReviews;
    @track primaryId;
    @track shiftLeadId;
    @track shiftEngineerId;
    @track startDate;
    @track endDate;

    //SHOW FILTER SECTION
    //when AMU is selected will show filter options
    @track showFilter = false;

    //DATATABLE VARIABLE DEFAULT
    @track handoverColumns = [
        {label: 'AMU', fieldName: 'amuName', type: 'text'},
        {
            label: 'Handover Name',
            fieldName: 'nameUrl',
            type: 'url',
            typeAttributes: {label: {fieldName: 'handoverName'}, target: '_blank', tooltip: 'Open Handover Form'},
        },
        {label: 'Outgoing Operator', fieldName: 'outgoingOperator', type: 'text'},
        {label: 'Shift Engineer', fieldName: 'shiftEngineer', type: 'text'},
        {label: 'Handover Status', fieldName: 'handoverStatus', type: 'text'},
    ];

    //DATATABLE VARIABLE FOR SHIFT ENGINEER (when checkbox is checked)
    @track handoverColumnsForSE = [
            {label: 'AMU', fieldName: 'amuName', type: 'text'},
            {
                label: 'Handover Name',
                fieldName: 'nameUrl',
                type: 'url',
                typeAttributes: {label: {fieldName: 'handoverName'}, target: '_blank', tooltip: 'Open Handover Form'},
            },
            {label: 'Outgoing Operator', fieldName: 'outgoingOperator', type: 'text'},
            {label: 'Shift Engineer', fieldName: 'shiftEngineer', type: 'text'},
            {label: 'Awaiting Review by', fieldName: 'awaitingReviewText' , type: 'text'},
            {label: 'Handover Status', fieldName: 'handoverStatus', type: 'text'},
        ];

    //DATATABLE VARIABLE FOR SHIFT SUMMARIES (WHEN SHIFT LEAD POST IS SELECTED)
    @track shiftSummaryColumns = [
         {label: 'AMU', fieldName: 'amuName', type: 'text'},
         {
             label: 'Shift Summary Name',
             fieldName: 'nameUrl',
             type: 'url',
             typeAttributes: {label: {fieldName: 'summaryName'}, target: '_blank', tooltip: 'Open Shift Summary'},
         },
         {label: 'Outgoing Shift Lead', fieldName: 'outgoingShiftLead', type: 'text'},
         {label: 'Shift Summary Status', fieldName: 'summaryStatus', type: 'text'},
     ];

    @track handoverStatusesForReviewers = [
            {label: 'Accepted', value: 'Accepted'},
            {label: 'In Progress', value: 'In Progress'},
            ];

    /* CONSTRUCTOR */
    constructor() {
        super();
        this.getDefaultAMU();
    }

    /* GETTERS */
    get sortByFieldOptions() {
        return [
            { label: 'Created Date', value: 'CreatedDate'},
            { label: 'Name', value: 'Handover_Name__c'},
            { label: 'Outgoing Operator', value: 'Outgoing_Operator__c'},
            { label: 'Shift Engineer', value: 'Shift_Assignement__r.Shift_Engineer__r.Name'},
            { label: 'Handover Status', value: 'Status__c'},
            { label: 'Post', value: 'Post__c'},
            { label: 'Shift Cycle', value: 'Shift_Cycle__c'},
        ];
    }

    get sortByDirection() {
        return [
            { label: 'Ascending', value: 'ASC'},
            { label: 'Descending', value: 'DESC'},
        ];
    }

    /* FUNCTIONS */

    //Get list of AMUs
    getDefaultAMU() {
        getUserDefaultAMU({}).then(result => {
            if (result != null) {
                this.selectedAMU = result;
                this.showFilterOptions(this.selectedAMU.Id);
            }
            this.showSpinner = false;
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    //Get Posts, populate Shift Cycle options, populate Handover status options
    showFilterOptions(operationField) {
        getAMUsPosts({currentAMU: operationField}).then(result => {
            this.amuPosts = result;

            let context = this;
            this.pickListValues('ELG_Shift_Assignement__c', 'Shift_Cycle__c', function (value) {
                context.shiftDayCycles = value;
            });

            this.amuPosts.forEach(function(post){
                if (post.shiftLeadPost) {
                    context.shiftLeadPostId = post.value;
                }
            });

            this.pickListValues('ELG_Shift_Handover__c', 'Status__c', function (value) {
                context.handoverStatuses = value;
            });

            this.pickListValues('ELG_Shift_Summary__c', 'Status__c', function (value) {
                context.shiftSummaryStatuses = value;
            });

        }).catch(error => {
                this.showSpinner = false;
                console.log('Error: ' +error);
        });
        this.showFilter = true;
    }

    //Generic method to get picklist values from Object
    pickListValues(nameOfObject, nameOfField, callbackFunction) {
        getPicklistValues({objectName: nameOfObject, fieldName: nameOfField}).then(result => {
            callbackFunction(result);
        });
    }

    //Search button with params
    //Will pass javascript Object to Apex
    availableHandovers() {
        let searchParams = {
            amuId: this.selectedAMU.Id,
            postId: this.selectedPost,
            shiftCycle: this.selectedShiftCycle,
            handoverStatus: this.selectedHandoverStatus,
            reviewerHandoverState: this.selectedHandoverStatusReviews,
            primaryId: this.primaryId,
            shiftEngineerId: this.shiftEngineerId,
            startDate: this.startDate,
            endDate: this.endDate,
        };
        this.handovers = null;
        this.showSpinner = true;
        sizeOfOffset = 0;
        getSearchResults({filterSelection: searchParams,
                        offsetSize: sizeOfOffset,
                        fieldSort : this.sortByField,
                        sortDirection: this.sortingDirection,
                        notReviewedBySE: this.shiftEngineerReview,
                        notReviewedBySteamChief: this.steamChiefReview,
                        notReviewedByShiftLead: this.shiftLeadReview,
                        notReviewedBySupervisor: this.supervisorReview}).then(result => {
            if (result[0].hasError) {
                this.showError = true;
                this.errorMessage = result[0].errorMsg;
                this.showSpinner = false;
            } else {
                this.showError = false;
                this.handovers = result;
                this.hasRecords = true;
                this.showSpinner = false;
                this.totalRecords = result[0].numberOfRecords;
                this.totalPages = Math.ceil(this.totalRecords / 50);
               if (this.totalPages == 1) {
                    this.singlePage = true;
               } else {
                    this.singlePage = false;
                    this.currentPage = (sizeOfOffset / 50) +1;
               }
            }
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    availableShiftSummaries() {
        let searchParams = {
            amuId: this.selectedAMU.Id,
            postId: this.selectedPost,
            shiftCycle: this.selectedShiftCycle,
            shiftSummaryStatus: this.selectedShiftSummaryStatus,
            shiftLeadId: this.shiftLeadId,
            startDate: this.startDate,
            endDate: this.endDate
        };
        this.shiftSummaries = null;
        this.showSpinner = true;
        sizeOfOffset = 0;
        getShiftSummarySearchResults({filterSelection: searchParams,
                        offsetSize: sizeOfOffset,
                        fieldSort : this.sortByField,
                        sortDirection: this.sortingDirection}).then(result => {
                console.log('RESULT: ' +JSON.stringify(result));
            if (result[0].hasError) {
                this.showError = true;
                this.errorMessage = result[0].errorMsg;
                this.showSpinner = false;
            } else {
                this.showError = false;
                this.shiftSummaries = result;
                this.hasRecords = true;
                this.showSpinner = false;
                this.totalRecords = result[0].numberOfRecords;
                console.log('this.totalRecords: ' +this.totalRecords)
                this.totalPages = Math.ceil(this.totalRecords / 50);
               if (this.totalPages == 1) {
                    this.singlePage = true;
               } else {
                    this.singlePage = false;
                    this.currentPage = (sizeOfOffset / 50) +1;
               }
            }
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    //Pagination buttons
    //the same as availableHandovers function except this one do not reset offset value
    loadMoreHandovers() {
        let searchParams = {
            amuId: this.selectedAMU.Id,
            postId: this.selectedPost,
            shiftCycle: this.selectedShiftCycle,
            handoverStatus: this.selectedHandoverStatus,
            reviewerHandoverState: this.selectedHandoverStatusReviews,
            primaryId: this.primaryId,
            shiftEngineerId: this.shiftEngineerId,
            shiftEngineerReview: this.shiftEngineerReview,
            startDate: this.startDate,
            endDate: this.endDate,
        };
        this.handovers = null;
        this.showSpinner = true;
        getSearchResults({filterSelection: searchParams,
                        offsetSize: sizeOfOffset,
                        fieldSort : this.sortByField,
                        sortDirection: this.sortingDirection,
                        notReviewedBySE: this.shiftEngineerReview,
                        notReviewedBySteamChief: this.steamChiefReview,
                        notReviewedByShiftLead: this.shiftLeadReview,
                        notReviewedBySupervisor: this.supervisorReview}).then(result => {
            this.showError = false;
            this.handovers = result;
            this.showSpinner = false;
            this.totalRecords = result[0].numberOfRecords;
            this.totalPages = Math.ceil(this.totalRecords / 50);
            this.currentPage = (sizeOfOffset / 50) +1;
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    loadMoreShiftSummaries() {
        let searchParams = {
            amuId: this.selectedAMU.Id,
            postId: this.selectedPost,
            shiftCycle: this.selectedShiftCycle,
            shiftSummaryStatus: this.selectedShiftSummaryStatus,
            shiftLeadId: this.shiftLeadId,
            startDate: this.startDate,
            endDate: this.endDate
        };
        this.shiftSummaries = null;
        this.showSpinner = true;
        getShiftSummarySearchResults({filterSelection: searchParams,
                        offsetSize: sizeOfOffset,
                        fieldSort : this.sortByField,
                        sortDirection: this.sortingDirection}).then(result => {
            this.showError = false;
            this.shiftSummaries = result;
            this.showSpinner = false;
            this.totalRecords = result[0].numberOfRecords;
            this.totalPages = Math.ceil(this.totalRecords / 50);
            this.currentPage = (sizeOfOffset / 50) +1;
        }).catch(error => {
            this.showSpinner = false;
            if (Array.isArray(error.body)) {
                error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                this.showToast(error.body.message);
            }
        });
    }

    //Clear data except Shift Engineer check and AMU
    clearData() {
        this.selectedPost = null;
        this.selectedShiftCycle = null;
        this.selectedHandoverStatus = null;
        this.selectedShiftSummaryStatus = null;
        this.selectedHandoverStatusReviews = null;

        this.hasRecords = false;
        this.primaryId = null;
        this.shiftLeadId = null;
        this.shiftEngineerId = null;
        this.startDate = null;
        this.endDate = null;
        this.handovers = null;
        this.checked = false;
        this.userSort = false;
        this.sortByField = 'CreatedDate';
        this.sortingDirection = 'DESC';
        this.postIsSelected = false;
        this.cycleIsSelected = false;
        this.statusIsSelected = false;
        this.startDateIsSelected = false;
        this.endDateIsSelected = false;
    }

    //CLEAR SELECTION FUNCTIONS
    clearPostSelection() {
        this.selectedPost = null;
        this.postIsSelected = false;
        this.showShiftSummaries = false;
        this.handovers = null;
        this.shiftSummaries = null;
        this.showShiftSummaries = false;
        this.showDeciderCheckbox = false;
        this.searchHandovers = false;
        this.hasRecords = false;
    }

    clearCycleSelection() {
        this.selectedShiftCycle = null;
        this.cycleIsSelected = false;
    }

    clearStatusSelection() {
        this.selectedHandoverStatus = null;
        this.selectedHandoverStatusReviews = null;
        this.statusIsSelected = false;
    }

    clearStartDate() {
        this.startDate = null;
        this.startDateIsSelected = false;
    }

    clearEndDate() {
        this.endDate = null;
        this.endDateIsSelected = false;
    }

    /* PAGINATION SECTION */

    get showFirstButton() {
        if (this.currentPage === 1) {
            return true;
        }
        return false;
    }

    get showLastButton() {
        if (Math.ceil(this.totalRecords / this.pageSize) === this.currentPage) {
            return true;
        }
        return false;
    }

    handlePrevious() {
        sizeOfOffset -= numberOfRecords;
        if (this.showShiftSummaries) {
            this.loadMoreShiftSummaries();
        } else {
            this.loadMoreHandovers();
        }
    }

    handleNext() {
        sizeOfOffset += numberOfRecords;
        if (this.showShiftSummaries) {
            this.loadMoreShiftSummaries();
        } else {
            this.loadMoreHandovers();
        }
    }

    handleFirst() {
        sizeOfOffset = 0;
        if (this.showShiftSummaries) {
            this.loadMoreShiftSummaries();
        } else {
            this.loadMoreHandovers();
        }
    }

    handleLast() {
        sizeOfOffset = (this.totalPages - 1) * numberOfRecords;
        if (this.showShiftSummaries) {
            this.loadMoreShiftSummaries();
        } else {
            this.loadMoreHandovers();
        }
    }

   /* HANDLERS */
    handleAMU(event) {
        var operatingField = event.detail;
        if (operatingField) {
            this.selectedAMU = operatingField;
            this.showFilterOptions(operatingField.Id);
        } else {
            this.selectedAMU = null;
        }
    }

    clearAMU(event) {
        this.selectedAMU = null;
        this.showFilter = false;
        this.clearData();
    }

    handleAMUChange(event) {
        this.selectedAMU = event.detail.value;
        this.showFilterOptions(this.selectedAMU);
        this.clearData();
    }

    handleSelectedPost(event) {
        this.selectedPost = event.detail.value;
        if (this.selectedPost == this.shiftLeadPostId) {
            this.showShiftSummaries = true;
            this.showDeciderCheckbox = true;
        } else {
            this.showShiftSummaries = false;
            this.showDeciderCheckbox = false;
        }
        this.handovers = null;
        this.shiftSummaries = null;
        this.hasRecords = false;
        this.postIsSelected = true;
    }

    handleSelectedShiftCycle(event) {
        this.selectedShiftCycle = event.detail.value;
        this.cycleIsSelected = true;
    }

    handleSelectedHandoverStatus(event) {
        this.selectedHandoverStatus = event.detail.value;
        this.statusIsSelected = true;
    }

    handleSelectedShiftSummaryStatus(event) {
        this.selectedShiftSummaryStatus = event.detail.value;
        this.statusIsSelected = true;
    }

     handleSelectedHandoverStatusReviews(event) {
        this.selectedHandoverStatusReviews = event.detail.value;
        this.statusIsSelected = true;
    }

    handleSelectionPrimary(event) {
        var userObject = event.detail;
        if (userObject) {
            this.primaryId = userObject.Id;
        } else {
            this.primaryId = null;
        }
    }

    clearPrimary(event) {
        this.primaryId = null;
    }

    handleSelectionPrimary(event) {
        var userObject = event.detail;
        if (userObject) {
            this.shiftLeadId = userObject.Id;
        } else {
            this.shiftLeadId = null;
        }
    }

    clearShiftLead(event) {
        this.shiftLeadId = null;
    }

    handleSelectionShiftEngineer(event) {
        var userObject = event.detail;
        if (userObject) {
            this.shiftEngineerId = userObject.Id;
        } else {
            this.shiftEngineerId = null;
        }
    }

    clearShiftEngineer(event) {
        this.shiftEngineerId = null;
    }

    handleSelectedStartDate(event) {
        this.startDate = event.detail.value;
        this.startDateIsSelected = true;
    }

    handleSelectedEndDate(event) {
        this.endDate = event.detail.value;
        this.endDateIsSelected = true;
    }

    //START OF USER SORTING HANDLERS
    handleCheckboxChange(event) {
        this.checked = !this.checked;
        this.userSort = this.checked;

        if (!this.checked) {
            this.sortByField = 'CreatedDate';
            this.sortingDirection = 'DESC';
        }
    }

    handleSortByField(event) {
        this.sortByField = event.detail.value;
    }

    handleSortingDirection(event) {
        this.sortingDirection = event.detail.value;
    }
    // END OF USER SORTING HANDLERS

    /* TOAST */
    showToast(errorMessage) {
        const event = new ShowToastEvent({
            title: 'Please, contact HOGLloydSF@huskyenergy.com with screenshot attached and description what action triggered this error.',
            message: errorMessage,
            variant: 'error',
            mode: 'sticky',
        });
        this.dispatchEvent(event);
    }

    /* Review */
    handleReviewSelection(event) {
        this.showReviewSelection = !this.showReviewSelection;
        if (this.showReviewSelection) {
            this.selectedPost = null;
            this.handoverStatus = null;
            this.statusIsSelected = false;
            this.postIsSelected = false;
            this.selectedHandoverStatus = null;
            this.selectedHandoverStatusReviews = null;
            this.changeFilterCriteria = true;
        } else {
            this.selectedHandoverStatus = null;
            this.selectedHandoverStatusReviews = null;
            this.statusIsSelected = false;
            this.shiftEngineerReview = false;
            this.steamChiefReview = false;
            this.shiftLeadReview = false;
            this.supervisorReview = false;
            this.changeFilterCriteria = false;
        }
    }

    handleDeciderCheckbox(event) {
        this.searchHandovers = !this.searchHandovers;
        this.hasRecords = false;

        if (this.searchHandovers) {
            this.showShiftSummaries = false;
        } else {
            this.showShiftSummaries = true;
        }
    }

     handleShiftEngineerReview(event) {
        this.shiftEngineerReview = !this.shiftEngineerReview;
    }

    handleSteamChiefReview(event) {
        this.steamChiefReview = !this.steamChiefReview;
    }

    handleShiftLeadReview(event) {
        this.shiftLeadReview = !this.shiftLeadReview;
    }

    handleSupervisorReview(event) {
        this.supervisorReview = !this.supervisorReview;
    }

    /* SELECTED HANDOVERS SECTIONS */

    openSelectedHandovers() {
       var selectedDatatableHandovers = this.template.querySelector('lightning-datatable').getSelectedRows();

       if (selectedDatatableHandovers.length > 1) {
           const selectedHandovers = new CustomEvent("passSelectedHandovers", { detail: {selectedDatatableHandovers} });
           this.dispatchEvent(selectedHandovers);
       } else {
           const event = new ShowToastEvent({
                       message: 'Please, select at least two Handovers to proceed.',
                       variant: 'warning',
                   });
           this.dispatchEvent(event);
       }

    }

    /* SELECTED SUMMARIES SECTION */
    openSelectedSummaries() {
        var selectedDatatableSummaries = this.template.querySelector('lightning-datatable.blabla').getSelectedRows();
        console.log('selectedDatatableSummaries: ' +JSON.stringify(selectedDatatableSummaries));

       if (selectedDatatableSummaries.length > 1) {
           const selectedSummaries = new CustomEvent("passSelectedSummaries", { detail: {selectedDatatableSummaries} });
           this.dispatchEvent(selectedSummaries);
       } else {
           const event = new ShowToastEvent({
                       message: 'Please, select at least two Handovers to proceed.',
                       variant: 'warning',
                   });
           this.dispatchEvent(event);
       }
    }
}