/**
 * Created by MarosGrajcar on 2/13/2020.
 */

import { LightningElement, api, track } from 'lwc';

export default class EqtDataCorrectionSection extends LightningElement {

    @api isView;
    @api equipmentForm;

    @track description;
    @track manufacturer;
    @track modelNo;
    @track manufacturerSerialNo;
    @track tagNumber;
    @track safetyCriticalEq;
    @track expirationDate;
    @track regulatoryEq;

    correctionFormObject = {
        "Id" : '',
        "Description_new__c" : '',
        "Manufacturer_new__c" : '',
        "Model_Number_new__c" : '',
        "Manufacturer_Serial_No_new__c" : '',
        "Tag_Number_new__c" : ''}

    _originalForm;
    @api
    get originalForm() {
        return this._originalForm;
    }

    set originalForm(value) {
        this._originalForm = value;
        this.correctionFormObject = JSON.parse(JSON.stringify(this.originalForm));
    }

    _equipment;
    @api
    get equipment() {
        return this._equipment;
    }

    set equipment(value) {
        this._equipment = value;
    }

    @api
    userInputs() {
        return this.correctionFormObject;
    }

    /* HANDLERS */
    handleDescription(event) {
        this.description = event.target.value;
        this.correctionFormObject.Description_new__c = this.description;
    }

    handleManufacturer(event) {
        this.manufacturer = event.target.value;
        this.correctionFormObject.Manufacturer_new__c = this.manufacturer;
    }

    handleModelNo(event) {
        this.modelNo = event.target.value;
        this.correctionFormObject.Model_Number_new__c = this.modelNo;
    }

    handleManufacturerSerialNo(event) {
        this.manufacturerSerialNo = event.target.value;
        this.correctionFormObject.Manufacturer_Serial_No_new__c = this.manufacturerSerialNo;
    }

    handleTagNumber(event) {
        this.tagNumber = event.target.value;
        this.correctionFormObject.Tag_Number_new__c = this.tagNumber;
    }
}