/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 30/08/2019 - Created.
*************************************************************************************************/
const WOA_COLUMNS_SETTING = [
                                {label: 'Work Order Activity', fieldName: 'woaUrl', type: 'url',
                                    typeAttributes: {
                                        label: { fieldName: 'Name'},
                                        tooltip: { fieldName: 'Name'}
                                    }
                                },
                                {label: 'Location', fieldName: 'Functional_Location_Description_SAP__c', type: 'text'},
                                {label: 'Scheduled', fieldName: 'Scheduled_Start_Date__c', type: 'date',
                                    typeAttributes: {
                                        year: 'numeric',
                                        month: 'short',
                                        day: '2-digit',
                                        hour: '2-digit',
                                        minute: '2-digit',
                                    }
                                },
                                {label: 'Vendor', fieldName: 'vendorUrl', type: 'url',
                                    typeAttributes: {
                                        label: { fieldName: 'vendorName'},
                                        tooltip: { fieldName: 'vendorName'}
                                    }
                                },
                                {label: 'Assigned', fieldName: 'Assigned_Text__c', type: 'text'},
                                {label: 'Status', fieldName: 'Status__c', type: 'text'}
                            ];

import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class VTT_WOAListViewResultItem extends NavigationMixin(LightningElement) {
//VARS
    @api record;
    @track woasColumns = WOA_COLUMNS_SETTING;
    @api tradesmanInfo; //VTT_TradesmanFlags

    @track expanded = false;

//APIs
    @api
    clearSelection() {
        const woaTable = this.template.querySelector('c-vtt_woa-list-view_woa-table');
        if(woaTable) {
            woaTable.clearSelection();
        }
    }

    @api doExpand() {
        this.expanded = true;
    }
    @api doCollapse() {
        this.expanded = false;
    }

    @api getSelectedRows() {
        const woaTable = this.template.querySelector('c-vtt_woa-list-view_woa-table');
        if(woaTable) {
            return woaTable.getSelectedRecords();
        }
        return [];
    }

//HANDLERS/ACTIONS
    handleToggle(evt) {
        this.expanded = evt.detail;
    }

    handleRowSelection(evt) {
        this.selectedWOAs = evt.detail.selectedRows;
    }

//GETTERS
    get location() {
        if(this.record.Well_Event__r) return this.record.Well_Event__r.Name;
        if(this.record.Yard__r) return this.record.Yard__r.Name;
        if(this.record.Functional_Equipment_Level__r) return this.record.Functional_Equipment_Level__r.Name;
        if(this.record.Sub_System__r) return this.record.Sub_System__r.Name;
        if(this.record.System__r) return this.record.System__r.Name;
        if(this.record.Location__r) return this.record.Location__r.Name;
        if(this.record.Facility__r) return this.record.Facility__r.Name;
        if(this.record.Operating_Field_AMU_Lookup__r) return this.record.Operating_Field_AMU_Lookup__r.Name;
        return '';
    }

    get hasActivities() {
        return this.record.Work_Order_Activities__r !== undefined && this.record.Work_Order_Activities__r.length > 0;
    }

}