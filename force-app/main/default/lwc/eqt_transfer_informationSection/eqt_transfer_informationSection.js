/**
 * Created by MarosGrajcar on 2/17/2020.
 */

import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getPicklistValues from '@salesforce/apex/HOG_PicklistServiceLtng.getPicklist';

export default class EqtTransferInformationSection extends NavigationMixin(LightningElement) {

    @api isNewForm;
    //@api isView;
    @api isUserAdmin;
    @api isFromForm;

    @api flocName;
    @api toFlocId;
    @api toAPIName;

    @api fromFlocId;
    @api fromAPIName;

    @api transferForm;

    @track transferReason;
    @track dateOfTransfer;
    @track authorizedBy;
    @track safetyCriticalEq;
    @track regulatoryEq;
    @track formStatus;
    @track workOrder;
    @track shippedVia;
    @track waybillNo;
    @track chargeNo;

    @track reasonForTransferPicklist;

    @track selectedDestination;
    @api transferDestination;
    @track isDestinationSelected = true;

    transferFormObject = {
            "Id" : '',
            "Reason_for_Transfer_Additional_Details__c" : '',
            "Date_of_Physical_Transfer__c" : '',
            "Authorized_By__c" : '',
            "Maintenance_Work_Order__c" : '',
            "Maintenance_Work_Order__c" : '',
            "Shipped_Via__c" : '',
            "Waybill_No__c" : '',
            "Charge_No__c" : '',
            "Safety_Critical_Equipment__c" : '',
            "Regulatory_Equipment__c" : '',
            "Expiration_Date__c" : '',
            "Tag_Number_new__c" : ''}


    get transferDestinationOptions() {
            return [
                { label: 'Location', value: 'Location__c' },
                { label: 'Facility', value: 'Facility__c' },
                { label: 'Well Event', value: 'Well_Event__c' },
                { label: 'System', value: 'System__c' },
                { label: 'Sub-System', value: 'Sub_System__c' },
                { label: 'Functional Equipment Level', value: 'Functional_Equipment_Level__c' },
                { label: 'Yard', value: 'Yard__c' },
            ];
    }

    _flocId;
    @api
    get flocId() {
        return this._flocId;
    }

    set flocId(value) {
        this._flocId = value;
        let context = this;
        this.pickListValues('Equipment_Transfer_Form__c', 'Reason_for_Transfer_Additional_Details__c', function (value) {
            context.reasonForTransferPicklist = value;
        });
    }

    _expirationDate;
    @api
    get expirationDate() {
        return this._expirationDate;
    }

    set expirationDate(value) {
        this._expirationDate = value;
        this.transferFormObject.Expiration_Date__c = this.expirationDate;
    }

    _originalForm;
    @api
    get originalForm() {
        return this._originalForm;
    }

    set originalForm(value) {
        this._originalForm = value;
        this.transferFormObject = JSON.parse(JSON.stringify(this.originalForm));
    }

    _isView;
    @api
    get isView() {
        return this._isView;
    }

    set isView(value) {
        this._isView = value;
        if (!this.isView) {
            let context = this;
            this.pickListValues('Equipment_Transfer_Form__c', 'Reason_for_Transfer_Additional_Details__c', function (value) {
                context.reasonForTransferPicklist = value;
            });
        }
    }

    @api
    userInputs() {
        return this.transferFormObject;
    }

    @api
    userSelectedDestination() {
        return this.transferDestination;
    }


    pickListValues(nameOfObject, nameOfField, callbackFunction) {
        getPicklistValues({objectName: nameOfObject, fieldName: nameOfField}).then(result => {
            callbackFunction(result);
        });
    }


    handleTransferReason(event) {
        this.transferReason = event.target.value;
        this.transferFormObject.Reason_for_Transfer_Additional_Details__c = this.transferReason;
    }

    handleDateOfTransfer(event) {
        this.dateOfTransfer = event.target.value;
        this.transferFormObject.Date_of_Physical_Transfer__c = this.dateOfTransfer;
    }

    handleSafetyCritical(event) {
        this.safetyCriticalEq = !this.safetyCriticalEq;
        this.transferFormObject.Safety_Critical_Equipment__c = this.safetyCriticalEq;
    }

    handleRegulatoryEq(event) {
        this.regulatoryEq = !this.regulatoryEq;
        this.transferFormObject.Regulatory_Equipment__c = this.regulatoryEq;
    }

    handleWorkOrder(event) {
        var workOrder = event.detail;
        if (workOrder) {
            this.workOrder = workOrder.Id;
        } else {
            this.workOrder = null;
        }
        this.transferFormObject.Maintenance_Work_Order__c = this.workOrder;
    }

    clearWorkOrder(event) {
        this.workOrder = null;
        this.transferFormObject.Maintenance_Work_Order__c = '';
    }

    handleShippedVia(event) {
        this.shippedVia = event.target.value;
        this.transferFormObject.Shipped_Via__c = this.shippedVia;
    }

    handleWaybillNo(event) {
        this.waybillNo = event.target.value;
        this.transferFormObject.Waybill_No__c = this.waybillNo;
    }

    handleChargeNo(event) {
        this.chargeNo = event.target.value;
        this.transferFormObject.Charge_No__c = this.chargeNo;
    }

    handleExpirationDate(event) {
        this.expirationDate = event.target.value;
        this.transferFormObject.Expiration_Date__c = this.expirationDate;
    }

    handleAuthorizedBy(event) {
        var userObject = event.detail;
        if (userObject) {
            this.authorizedBy = userObject.Id;
        } else {
            this.authorizedBy = null;
        }
        this.transferFormObject.Authorized_By__c = this.authorizedBy;
    }

    clearAuthorizedBy(event) {
        this.authorizedBy = null;
        this.transferFormObject.Authorized_By__c = '';
    }

    handleDestinationSelection(event) {
        var selectedObject = event.detail;
        if (selectedObject) {
            this.transferDestination = selectedObject.Id;
        } else {
            this.transferDestination = null;
        }
        if (this.transferDestination == this.fromFlocId) {
            this.dispatchEvent(
               new ShowToastEvent({
               title: '',
               message: 'You cannot select the same destination for Transfer',
               variant: 'warning',
               }),
           );

            this.clearLookup('transferDestinationOnInsert');
            this.clearLookup('transferDestinationOnInsertMobile');
            this.clearLookup('transferDestination');
            this.clearLookup('transferDestinationMobile');
        }
    }

    clearDestination(event) {
        this.transferDestination = null;
    }

    clearLookup(templateName) {
        const clearDestination = this.template.querySelector('c-hog_lookup.' + templateName);
        if (clearDestination) {
           clearDestination.clearSelection();
           this.clearDestination();
        }
    }

    handleSelectedDestination(event) {
        this.clearLookup('transferDestinationOnInsert');
        this.clearLookup('transferDestinationOnInsertMobile');
        this.clearLookup('transferDestination');
        this.clearLookup('transferDestinationMobile');
        this.selectedDestination = event.target.value;
    }

    redirectToRecordPage(event) {
        var recordId = event.currentTarget.dataset.id;
        var apiName = event.currentTarget.dataset.apiname;

        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                objectApiName: apiName,
                actionName: 'view',
                recordId: recordId
            },
        });
    }
}