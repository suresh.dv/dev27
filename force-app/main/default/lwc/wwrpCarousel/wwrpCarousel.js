import { LightningElement } from 'lwc';

import WWRP_CAROUSEL_IMAGES from '@salesforce/resourceUrl/WWRP';

export default class WwrpCarousel extends LightningElement {

    rotationImage1 = WWRP_CAROUSEL_IMAGES + '/images/Home-Rotation-1.jpg';
    rotationImage2 = WWRP_CAROUSEL_IMAGES + '/images/Home-Rotation-2.jpg';
    rotationImage3 = WWRP_CAROUSEL_IMAGES + '/images/Home-Rotation-3.jpg';
    rotationImage4 = WWRP_CAROUSEL_IMAGES + '/images/Home-Rotation-4.jpg'; 
    huskyLogo = WWRP_CAROUSEL_IMAGES + '/images/Husky-Energy.png';

    userAgent = navigator.userAgent;
    appName = navigator.appName;

    toggleMenu() {
      const menuOptions = this.template.querySelector('.overlayMenu');
      
      if(menuOptions.style.display === 'none') {
        menuOptions.style.display = 'block';
      } else {
        menuOptions.style.display = 'none';
      }
    }
 
}