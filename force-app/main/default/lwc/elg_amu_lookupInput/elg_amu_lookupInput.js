/**
 * Created by MarosGrajcar on 3/4/2020.
 */

import { LightningElement, api, track } from 'lwc';
import fetchPostAMUs from '@salesforce/apex/ELG_AMU_LookupEndpoint.fetchPostAMUs';

const MINIMAL_SEARCH_TERM_LENGTH = 2;
const SEARCH_DELAY = 300;
const HIDE_RESULT_DELAY = 300;
const NO_RESULTS_MESSAGE = 'No results found';

export default class ElgAmuLookupInput extends LightningElement {
//required attributes
    @api label;
    @api objectAPIName;
    @api amuIds = [];
//optional attributes
    @api fieldAPIName = 'Name';
    @api additionalFields = '';
    @api placeholder = '';
    @api isRequired = false;

    @track searchTerm = '';
    @track hasFocus = false;
    @track searching = false;

    @track displayResults = false;
    @track searchResults = [];
    @track searchMessage = '';
    @api selectedRecord = undefined;
    searchTimeOut;

//MAIN LOGIC
    handleSelection(evt) {
        this.dispatchEvent(
            new CustomEvent(
                'selection',
                {
                    detail: evt.detail
                }
            )
        );
        this.displayResults = false;
    }

    handleBlur() {
        setTimeout(() => {
                this.displayResults = false
            },
            HIDE_RESULT_DELAY
        );
    }

    handleFocus() {
        if(this.searchTerm.length < MINIMAL_SEARCH_TERM_LENGTH) {
            this.displayResults = false;
            return;
        }

        this.displayResults = true;
    }

    handleInput(evt) {
        this.searchTerm = evt.target.value;

        this.killPreviousSearchTimeOut();

        if(this.searchTerm.length < MINIMAL_SEARCH_TERM_LENGTH) {
            this.displayResults = false;
            return;
        }

        this.runSearch();
    }

    killPreviousSearchTimeOut() {
        if(this.searchTimeOut) {
            clearTimeout(this.searchTimeOut);
            this.searchTimeOut = undefined;
        }
    }

    runSearch() {
        this.searching = true;
        this.displayResults = true;
        console.log('Fetching: ' +this.amuIds);
        this.searchTimeOut = setTimeout(() => {
                fetchPostAMUs({
                        stringToSearch: this.searchTerm,
                        postAMUs : this.amuIds
                    })
                    .then(result => {
                        console.log('result: ' +JSON.stringify(result));
                        this.handleResult(result);
                    })
                    .catch(error => {
                        console.log('error: ' +error);
                        this.searchMessage = error;
                        this.searching = false;
                    });
            },
            SEARCH_DELAY
        );
    }

    handleResult(result) {
        this.searchMessage = '';
        this.searchResults = result;
        if(result.length === 0) {
            this.searchMessage = NO_RESULTS_MESSAGE;
        }
        this.searching = false;
    }

//GETTERS
    get searchMessageEmpty() {
        return this.searchMessage === ''
                || this.searchMessage === null
                || this.searchMessage === undefined;
    }
}