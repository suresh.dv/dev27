/**
 * Created by MarcelBrimus on 22/08/2019.
 */

import { LightningElement, api, wire, track } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import FullCalendarJS from '@salesforce/resourceUrl/fullcalendar_4';

export default class vtt_calendar extends LightningElement {

    // This is used to show and hide hover
    @track hoverStyle = 'display:none;';

    // Flag to display spinner
    @api loading = false;

    /**********
    * SETTINGS
    ***********/
    // Title of calendar
    @api calendarTitle;

    // Hide/Show color legend
    @api showLegend = false;

    // Hide/Show color hover
    @api hoverEffect = false;

    hoverElement = {
        title : '',
        param1Label : '',
        param1Value : '',
        param2Label : '',
        param2Value : '',
        param3Label : '',
        param3Value : '',
        param4Label : '',
        param4Value : ''
    };

    // Main calendar reference
    _calendar;

    // Passed from other components
    _eventsData = [];
    @api get eventsData(){
        return this._eventsData;
    }

    // This clears current events set and adds new set of events into calendar
    set eventsData(values){
        this.clearAllEvents();
        this._eventsData = values;
        if(values && this._calendar){
            this.addNewEvents();
        }
    }

    // Init
    fullCalendarJsInitialised = false;
    connectedCallback() {
        // Performs this operation only on first render
        if (this.fullCalendarJsInitialised) {
            return;
        }
        this.fullCalendarJsInitialised = true;
        this.loadLibraries();
    }

    // Load External Libraries
    loadLibraries(){
        this.loading = true;

        // Executes all loadScript and loadStyle promises
        // and only resolves them once all promises are done
        // Have to be splited into two promises because of FullCalendar V4 dependencies
        Promise.all([
            loadScript(this, FullCalendarJS + '/packages/core/main.js'),
            loadStyle(this, FullCalendarJS + '/packages/core/main.css'),
            loadStyle(this, FullCalendarJS + '/packages/daygrid/main.css'),
            loadStyle(this, FullCalendarJS + '/packages/timegrid/main.css'),
            loadStyle(this, FullCalendarJS + '/packages/bootstrap/main.css'),
            loadStyle(this, FullCalendarJS + '/packages/customsfdc/custom.css'),
        ]).then(() => {
            Promise.all([
                loadScript(this, FullCalendarJS + '/packages/daygrid/main.js'),
                loadScript(this, FullCalendarJS + '/packages/interaction/main.js'),
                loadScript(this, FullCalendarJS + '/packages/bootstrap/main.js'),
                loadScript(this, FullCalendarJS + '/packages/timegrid/main.js'),
            ]).then(() => {
                // Send out an event that all libraries are loaded and fullcalendar can be used
                this.librariesLoadedAndCalendarRendered();
                this.loading = false;
            }).catch(error => {
                console.log('Error initializing calendar ' + error)
                throw error;
            });
        }).catch(error => {
            console.log('Error initializing calendar ' + error)
            throw error;
        })
    }

    // MAIN render method
    renderCalendar(){
        // Clear calendar and render again
        this.template.querySelector('div.calendar').innerHTML = "";

        const calendarElement = this.template.querySelector('div.calendar');
        var context = this;
        context._calendar = new FullCalendar.Calendar(calendarElement, {
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },
            timeZone: false,
            nextDayThreshold: '09:00:00',
            eventLimit: true,
            plugins: ['dayGrid', 'timeGrid', 'interaction' ],
            themeSystem: 'bootstrap',
            events: [],
            rerenderDelay: 1,
            eventClick: function( eventClickInfo ){
                eventClickInfo.jsEvent.preventDefault();
                context.eventClicked(eventClickInfo.event);
            },
            dateClick: function(info) {
                context.dateClicked(info.date);
            },
            datesRender: function(info){
                context.datesChanged(info.view);
                context.clearAllEvents();
            },
            eventMouseEnter: function(info){
                // Show hover effect if enabled
                if(context.hoverEffect){
                    // Hack to position this hover properly on community
                    context.hoverStyle = 'display:block; position:absolute; left: '+ info.jsEvent.pageX +
                                            'px; top:' + (info.jsEvent.pageY - 300) + 'px';
                    context.hoverElement.title = info.event.title;
                    context.hoverElement.param1Label = info.event.extendedProps.param1Label;
                    context.hoverElement.param1Value = info.event.extendedProps.param1Value;

                    context.hoverElement.param2Label = info.event.extendedProps.param2Label;
                    context.hoverElement.param2Value = info.event.extendedProps.param2Value;

                    context.hoverElement.param3Label = info.event.extendedProps.param3Label;
                    context.hoverElement.param3Value = info.event.extendedProps.param3Value;

                    context.hoverElement.param4Label = info.event.extendedProps.param4Label;
                    context.hoverElement.param4Value = info.event.extendedProps.param4Value;
                }
            },
            eventMouseLeave: function(info){
                // Hides hover effect
                context.hoverStyle = 'display:none;';
            }
        });
        context._calendar.render();
    }

    /********************
    *       HELPERS
    ********************/
    // Clears all events in calendar
    clearAllEvents(){
        if(this._calendar){
            var eventSources = this._calendar.getEvents();
            var len = eventSources.length;
            for (var i = 0; i < len; i++) {
                eventSources[i].remove();
            }
        }
    }

    // Add all events
    addNewEvents(){
        var len = this._eventsData.length;
        for (var i = 0; i < len; i++) {
            console.log(JSON.stringify(this._eventsData[i]))
            this._calendar.addEvent(this._eventsData[i]);
        }
    }

    /********************
    *       EVENTS
    ********************/
    // Dispatches event that date on calendar was clicked
    dateClicked(date){
        const eData = new CustomEvent('dateclick',{ detail: date });
        this.dispatchEvent(eData);
    }

    // Fired when event on calendar is clicked and sends out event data
    eventClicked(event){
        const eventData = new CustomEvent('eventclick', { detail: event});
        this.dispatchEvent(eventData);
    }

    // Fired when user changes month, day or week
    datesChanged(info){
        const datesChanged = new CustomEvent('dateschanged',{ detail: info });
        this.dispatchEvent(datesChanged);
    }

    // Fired when libraries are loaded and full-calendar can be user
    // Calendar is rendered first so that date event is fired
    // Then sends out an event that libs are loaded and calendar can be used
    librariesLoadedAndCalendarRendered(){
        // We can now safely render calendar
        this.renderCalendar();
        // Send event that notifies parent component that he can use calendar
        const libLoaded = new CustomEvent('librariesloaded',{ detail: true });
        this.dispatchEvent(libLoaded);
    }
}