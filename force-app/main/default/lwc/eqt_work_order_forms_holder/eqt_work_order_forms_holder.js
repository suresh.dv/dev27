/**
 * Created by MarosGrajcar on 1/27/2020.
 */

import { LightningElement, api, track } from 'lwc';
import getRecord from '@salesforce/apex/Equipment_Forms_Endpoint.getRecord';

export default class EqtFormsHolderCommunity extends LightningElement {

    @track flocId;
    @track initialLoading = true;
    @track hasFloc = true;
    record;

    _woId;
   @api
   get recordId() {
       return this._woId;
   }

   set recordId(value) {
       this._woId = value;
       this.loadRecord();
   }


    loadRecord() {
        this.initialLoading = true;
        getRecord({recordId: this._woId})
            .then(result => {
                this.record = result;
                if (!this.extractFLOCId()) {
                    this.hasFloc = false;
                }
            }).catch(error => {
                this.error = error.message;
            }).finally(() => {
                this.initialLoading = false;
            });
    }

    extractFLOCId() {
        let hasFloc = false;
        if(this.record) {
            if(this.record.Well_Event__c != null) {
                this.flocId = this.record.Well_Event__c;
                hasFloc = true;
            } else if(this.record.Yard__c != null) {
                this.flocId = this.record.Yard__c;
                hasFloc = true;
            } else if(this.record.Functional_Equipment_Level__c != null) {
                this.flocId = this.record.Functional_Equipment_Level__c;
                hasFloc = true;
            } else if(this.record.Sub_System__c != null) {
                this.flocId = this.record.Sub_System__c;
                hasFloc = true;
            } else if(this.record.System__c != null) {
                this.flocId = this.record.System__c;
                hasFloc = true;
            } else if(this.record.Location__c != null) {
                this.flocId = this.record.Location__c;
                hasFloc = true;
            } else if(this.record.Facility__c != null) {
                this.flocId = this.record.Facility__c;
                hasFloc = true;
            }
        } else {
            this.flocId = undefined;
        }

        return hasFloc;
    }
}