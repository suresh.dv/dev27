import { LightningElement, track, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';

import getUserListView from '@salesforce/apex/RaMPortalController.getPortalUsers';
import activateOrCreateUser from '@salesforce/apex/RaMPortalController.activateOrCreateUser';
import deactivatePortalUser from '@salesforce/apex/RaMPortalController.deactivateUser';
import updateRaMContact from '@salesforce/apex/RaMPortalController.updateContact';
import createRaMContact from '@salesforce/apex/RaMPortalController.createContact';

import USER_ID from '@salesforce/user/Id';
import ACCOUNT_ID from '@salesforce/schema/User.Contact.AccountId';
import ACCOUNT_OWNER from '@salesforce/schema/User.Contact.Account.OwnerId';
import ACCOUNT_NAME from '@salesforce/schema/User.Contact.Account.Name';

const activeUserActions = [
  { label: 'Details', name: 'show_details' },
  { label: 'Edit Contact', name: 'edit_contact' },
  { label: 'Deactivate', name: 'deactivate_user'},
];

const activeColumns = [
  {  label: 'Name', fieldName: 'ContactDetailView', sortable: "true", type: "url",
      typeAttributes: {label:{fieldName:'name'}, target: '_self'} },
  { label: 'Mobile Phone', fieldName: 'mobilePhone', type: 'phone', sortable: "true" }, 
  { label: 'Business Phone', fieldName: 'phone', type: 'phone', sortable: "true" },   
  { label: 'Login Id', fieldName: 'userName', sortable: "true" },    
  { label: 'Last Login', fieldName: 'lastLogin', type: 'date', 
      typeAttributes:{ year: "numeric", month: "long", day: "2-digit", hour: "2-digit", minute: "2-digit" },    
    sortable: "true" }, 
  {
    type: 'action',
    typeAttributes: { rowActions: activeUserActions },
  },    
];

const inactiveUserActions = [
  { label: 'Details', name: 'show_details' },
  { label: 'Edit Contact', name: 'edit_contact' },  
  { label: 'Activate', name: 'activate_user'},
];

const inactiveColumns = [
  {  label: 'Name', fieldName: 'ContactDetailView', sortable: "true", type: "url",
      typeAttributes: {label:{fieldName:'name'}, target: '_self'} },
  { label: 'Mobile Phone', fieldName: 'mobilePhone', type: 'phone', sortable: "true" }, 
  { label: 'Business Phone', fieldName: 'phone', type: 'phone', sortable: "true" },   
  { label: 'Login Id', fieldName: 'userName', sortable: "true" },    
  { label: 'Last Login', fieldName: 'lastLogin', type: 'date', 
      typeAttributes:{ year: "numeric", month: "long", day: "2-digit", hour: "2-digit", minute: "2-digit" },    
    sortable: "true" }, 
  {
    type: 'action',
    typeAttributes: { rowActions: inactiveUserActions },
  },    
];

export default class RamUserManagementListView extends NavigationMixin(LightningElement) {
  @track activeRamUsers;
  @track inactiveRamUsers;
  @track error;
  @track activeUserColumns = activeColumns;
  @track inactiveUserColumns = inactiveColumns;
  @track rowOffset = 0;
  @track showContactModal = false;
  @track showUserSelectionModal = false;
  @track showDeactivateUserModal = false;
  @track showContactEditModal = false;
  @track selectedRow ={};
  @track contactId;
  @track saveButtonLabel = 'Save';
  @track isSaveBtnDisabled = false;

  _data;    
  
  @wire(getUserListView)
  wiredUserList(result) {    
    this._data = result;
    if (result.data) {
      const activeUserrows = [];
      const inactiveUserrows = [];

      result.data.forEach(function(record) {
        const contactObj = Object.assign({}, record, {
          ContactDetailView: '/ram-contact-detail?id=' + record.contactId,
        });

        if(record.isActive) {
          activeUserrows.push(contactObj);
        } else {
          inactiveUserrows.push(contactObj);
        }
        
      });

      this.activeRamUsers = activeUserrows;
      this.inactiveRamUsers = inactiveUserrows;
      this.error = undefined;
    } else if (result.error) {
      this.activeRamUsers = undefined;
      this.error = result.error;      
    }
  } 
  
  @wire(getRecord, { recordId: USER_ID, fields: [ACCOUNT_ID, ACCOUNT_OWNER, ACCOUNT_NAME] }) 
  wiredUser;

  get accountId() {
    return getFieldValue(this.wiredUser.data, ACCOUNT_ID);
  }

  get accountOwner() {
    return getFieldValue(this.wiredUser.data, ACCOUNT_OWNER);
  } 

  get accountName() {
    return getFieldValue(this.wiredUser.data, ACCOUNT_NAME);
  }    

  overrideOwner(event) {
    OwnerId = accountOwner();
  }

  handleRowAction(event) {
    const actionName = event.detail.action.name;
    const row = event.detail.row;
    this.selectedRow = row;

    switch(actionName) {
      case 'show_details':
        this.navigateDetailPage();
        break;
      case 'deactivate_user':
        this.openDeactivateUserModal();
        break;
      case 'edit_contact':
        this.contactId = this.selectedRow.contactId;
        this.openUserEditModal();
        break;
      case 'activate_user':
        this.openUserSelectionModal();
        break;
      default:
    }
  }

  async deactivateUser() {
    this.isSaveBtnDisabled = true;

    try{
      await deactivatePortalUser({userId: this.selectedRow.userId});
      this.closeDeactivateUserModal();
      this.isSaveBtnDisabled = false;
      this.showSuccessToast('User ' + this.selectedRow.name + ' successfully de activated');
      return refreshApex(this._data);   

    } catch(error) {
      this.showErrorToast('Error deactivating User ' + this.selectedRow.name);
    }
  }

  async activateUser() {
    let portalUser = {
      contactId: this.selectedRow.contactId,
      isConverted: this.selectedRow.hasUserAccount,
      userId: this.selectedRow.userId,
      needPasswordReset: false,
      userType: ''
    };

    this.isSaveBtnDisabled = true;

    try{
      await activateOrCreateUser(portalUser);
      this.closeUserSelectionModal();
      this.isSaveBtnDisabled = false;
      this.showSuccessToast('User ' + this.selectedRow.name + ' successfully activated');
      return refreshApex(this._data);

    } catch(error) {
      this.showErrorToast('Error activating User ' + this.selectedRow.name);
    }
    
  }  

  async activateAndResetPassword() {
    let portalUser = {
      contactId: this.selectedRow.contactId,
      isConverted: this.selectedRow.hasUserAccount,
      userId: this.selectedRow.userId,
      needPasswordReset: true,
      userType: ''
    };

    this.isSaveBtnDisabled = true;

    try{
      await activateOrCreateUser(portalUser);
      this.closeUserSelectionModal();
      this.isSaveBtnDisabled = false;
      this.showSuccessToast('User ' + this.selectedRow.name + ' successfully activated password reset');
      return refreshApex(this._data);

    } catch(error) {
      console.log(error);
    }
  }

  async createDispatcherUser() {
    let portalUser = {
      contactId: this.selectedRow.contactId,
      isConverted: this.selectedRow.hasUserAccount,
      userId: this.selectedRow.userId,
      needPasswordReset: false,
      userType: 'RaM Dispatcher'
    };

    this.isSaveBtnDisabled = true;

    try{
      await activateOrCreateUser(portalUser);
      this.closeUserSelectionModal();
      this.isSaveBtnDisabled = false;
      this.showSuccessToast('New dispatch user ' + this.selectedRow.name + ' created');
      return refreshApex(this._data);

    } catch(error) {
      console.log(error);
    }
  }

  async createTechnicianUser() {
    let portalUser = {
      contactId: this.selectedRow.contactId,
      isConverted: this.selectedRow.hasUserAccount,
      userId: this.selectedRow.userId,
      needPasswordReset: false,
      userType: 'RaM Technician'
    };

    this.isSaveBtnDisabled = true;

    try{
      await activateOrCreateUser(portalUser);
      this.closeUserSelectionModal();
      this.isSaveBtnDisabled = false;
      this.showSuccessToast('New technician user ' + this.selectedRow.name + ' created');
      return refreshApex(this._data);

    } catch(error) {
      console.log(error);
    }
  } 
  
  async handleContactUpdate(event) {
    event.preventDefault();

    let userContact = {
      contactId: this.contactId,
      firstName: event.detail.fields.FirstName,
      lastName: event.detail.fields.LastName,
      emailAddress: event.detail.fields.Email,
      mobilePhone: event.detail.fields.MobilePhone,
      businessPhone: event.detail.fields.Phone,
      homePhone: event.detail.fields.HomePhone,
      otherPhone: event.detail.fields.OtherPhone,
      title: event.detail.fields.Title,
      description: event.detail.fields.Description,
      mailingStreet: event.detail.fields.MailingStreet,
      mailingCity: event.detail.fields.MailingCity,
      mailingState: event.detail.fields.MailingState,
      mailingPostalCode: event.detail.fields.MailingPostalCode,
      mailingCountry: event.detail.fields.MailingCountry 
    };

    this.disbleSaveButton();

    try{
      await updateRaMContact(userContact);
      this.showSuccessToast('Contact updated successfully');
      this.enableSaveButton();
      this.closeUserEditModal();

    } catch(error) {
      this.showErrorToast('Error updating contact');
    } finally {
      return refreshApex(this._data);
    }

  }  

  async handleContactCreate(event) {
    event.preventDefault();

    let userContact = {
      accountId: this.accountId,
      firstName: event.detail.fields.FirstName,
      lastName: event.detail.fields.LastName,
      emailAddress: event.detail.fields.Email,
      mobilePhone: event.detail.fields.MobilePhone,
      businessPhone: event.detail.fields.Phone,
      homePhone: event.detail.fields.HomePhone,
      otherPhone: event.detail.fields.OtherPhone,
      title: event.detail.fields.Title,
      description: event.detail.fields.Description,
      mailingStreet: event.detail.fields.MailingStreet,
      mailingCity: event.detail.fields.MailingCity,
      mailingState: event.detail.fields.MailingState,
      mailingPostalCode: event.detail.fields.MailingPostalCode,
      mailingCountry: event.detail.fields.MailingCountry 
    };

    this.disbleSaveButton();

    try{
      await createRaMContact(userContact);
      this.showSuccessToast('Contact created successfully');
      this.enableSaveButton();
      this.closeContactModal();

    } catch(error) {
      this.showErrorToast('Error creating contact');
    } finally {
      return refreshApex(this._data);
    }

  }    

  navigateDetailPage() {
    this[NavigationMixin.Navigate]({
      type: 'comm__namedPage',
      attributes: {
        recordId: this.selectedRow.contactId,
        pageName: 'ram-contact-detail'
        
      },
      state: {id: this.selectedRow.contactId}
    });
  }

  openContactModal() {
    this.showContactModal = true;
  }

  closeContactModal() {
    this.showContactModal = false;
  }

  openUserSelectionModal() {
    this.showUserSelectionModal = true;
  }

  closeUserSelectionModal() {
    this.showUserSelectionModal = false;
  } 
  
  openUserEditModal() {
    this.showContactEditModal = true;
  }

  closeUserEditModal() {
    this.showContactEditModal = false;
  }   
  
  openDeactivateUserModal() {
    this.showDeactivateUserModal = true;
  }

  closeDeactivateUserModal() {
    this.showDeactivateUserModal = false;
  }  

  disbleSaveButton() {
    this.isSaveDisabled = true;
    this.saveButtonLabel = 'Saving';
  }

  enableSaveButton() {
    this.isSaveDisabled = false;
    this.saveButtonLabel = 'Save';
  }   

  // handleSuccess() {
  //   this.closeContactModal();
  //   this.showSuccessToast('New Contact created');
  //   return refreshApex(this._data);
  // }     

  showSuccessToast(successMessage) {
    const event = new ShowToastEvent({
      variant: 'success',
      message: successMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }

  showErrorToast(errorMessage) {
    const event = new ShowToastEvent({
      variant: 'error',
      message: errorMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }  

}