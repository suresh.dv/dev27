/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-08-21 - Created.
*************************************************************************************************/
import { LightningElement, api } from 'lwc';

export default class HOG_LookupPill extends LightningElement {

    @api isRequired;
    @api label; //String label of the field
    @api selectedText; //String - resulted text based on attributes

    @api iconName = 'standard:people';
    @api renderIcon = false;

    /**
        Fires event to notify parent component, that selection was cleared.
    */
    clearSelection() {
        const evt = new CustomEvent('clearselection');
        this.dispatchEvent(evt);
    }

}