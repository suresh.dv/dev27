/**
 * Created by MarosGrajcar on 3/10/2020.
 */

import { LightningElement, api, track } from 'lwc';
import getShiftSummaries from '@salesforce/apex/ELG_ShiftSummary_Endpoint.getShiftSummaries';
import saveShiftSummary from '@salesforce/apex/ELG_ShiftSummary_Endpoint.saveShiftSummary';

export default class ElgShiftSummary extends LightningElement {

    @api shiftId;
    @track shiftSummary;
    temporaryShiftSummary;
    @track isView = true;
    @track loading = true;

    connectedCallback() {
        console.log('Shift id: ' +this.shiftId);
        this.loading = true;
        this.getSummaries();
    }

    @api
    getSummaries() {
        getShiftSummaries({shiftId : this.shiftId}).then(result => {
            console.log('result: ' +JSON.stringify(result));
            this.shiftSummary = result;
        }).catch(error => {
            console.log('Error' + error);
        }).finally(() => {
            this.loading = false;
        });
    }

    @api
    editMode() {
        this.temporaryShiftSummary = JSON.parse(JSON.stringify(this.shiftSummary));
        this.isView = false;
    }

    saveSummary() {
        this.loading = true;
        saveShiftSummary({shiftSummary : this.shiftSummary}).then(result => {
            console.log('saving: ');
            //this.shiftSummary = result;
            this.isView = true;
            this.getSummaries();
        }).catch(error => {
            console.log('Error' + error);
        });
        this.dispatchCancelEditMode();
    }

    cancelEditMode() {
        this.shiftSummary = this.temporaryShiftSummary;
        this.isView = true;
        this.dispatchCancelEditMode();
    }

    dispatchCancelEditMode() {
        console.log('dispatching event');
        this.dispatchEvent(new CustomEvent('canceleditmode'));
    }

    handleHSESection(event) {
        this.shiftSummary.HSE_Incidents__c = event.target.value
    }

    handleDailySection(event) {
        this.shiftSummary.Daily_Variance_300_Barrels_or_more__c = event.target.value;
    }

    handleForecastSection(event) {
        this.shiftSummary.Forecasted_Variance_300_Barrels_or_more__c = event.target.value;
    }

    handleShutdownSection(event) {
        this.shiftSummary.Facility_Shutdowns_Trips__c = event.target.value;
    }

    handleOperationalSection(event) {
        this.shiftSummary.Current_or_Upcoming_Operational_Issues__c = event.target.value;
    }
}