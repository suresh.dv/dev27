/**
 * Created by MarcelBrimus on 29/11/2019.
 */

import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import migrateToCommunity from '@salesforce/apex/VTT_LTNG_PortalManagementEndpoint.migrateToCommunity';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class VttLightningCommunityManagementEnabledContact extends NavigationMixin(LightningElement) {
    @api contact;
    // Who is logged in
    @api isCommunityUser;
    @track showMigrateModal = false;
    @track showRevertModal = false;
    @track migratingUser = false;

    get isClassicCommunityUser() {
        if(this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_User'
        || this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_Vendor_Supervisor'){
            return true
        } else {
            return false
        }
    }

    get canMigrate() {
        var communityUser = this.isCommunityUser;
        if((this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_User'
        || this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_Vendor_Supervisor')
        && communityUser !== null && communityUser !== undefined && communityUser !== true){
            return true
        } else {
            return false
        }
    }

    get canRevert() {
        var communityUser = this.isCommunityUser;
        if((this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_User_Lightning'
        || this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_Vendor_Supervisor_Lightning')
        && communityUser !== null && communityUser !== undefined && communityUser !== true){
            return true
        } else {
            return false
        }
    }

    get isLightningCommunityUser() {
        if(this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_User_Lightning'
        || this.contact.User__r.Vendor_Portal_User_Type__c == 'HOG_Vendor_Community_VTT_Vendor_Supervisor_Lightning'){
            return true
        } else {
            return false
        }
    }

    navigateToRecord(){
       this[NavigationMixin.Navigate]({
           type: 'standard__recordPage',
           attributes: {
               recordId: this.contact.Id,
               objectApiName: 'Contact',
               actionName: 'view'
           }
       });
    }
    // Based on Vendor_Portal_User_Type__c of Contact either migrates or reverts from / to Lightning and classic community
    migrateToCommunity(){
        this.migratingUser = true;
        migrateToCommunity({
            contact: JSON.stringify(this.contact)
        }).then(result => {
            if(result.success){
                this.showNotification('Successfully migrated user', 'Please click refresh button to see changes after a while', 'success');
            } else {
                var errorMessage = '';
                result.errors.forEach(function (item, index) {
                    errorMessage = errorMessage + item + '.'
                });
                this.showNotification('Error Migrating User ', errorMessage, 'error');
            }
            this.migratingUser = false;
            this.closeModal();
        }).catch(error => {
            this.migratingUser = false;
            this.closeModal();
            console.log('Migrating err ' + error)
        });
    }

    openMigrateModal() {
        this.showMigrateModal = true
    }

    openRevertModal() {
        this.showRevertModal = true
    }

    closeModal() {
        this.showMigrateModal = false
        this.showRevertModal = false
    }

     // UTILITIES
    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

}