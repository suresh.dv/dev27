/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 20/09/2019 - Created.
*************************************************************************************************/
import { LightningElement, api, track } from 'lwc';
import getVendors from '@salesforce/apex/VTT_WOA_Assignment_Endpoint.getVendors';
import getTradesmen from '@salesforce/apex/VTT_WOA_Assignment_Endpoint.getTradesmen';
import getSelectedTradesmen from '@salesforce/apex/VTT_WOA_Assignment_Endpoint.getAssignedTradesmenForActivity';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class VTT_WOA_ManageAssignments_tradesmanList extends LightningElement {
//VARS
    @api woas = []; //Work_Order_Activity__c[] enriched
    @api tradesmanInfo = undefined; //VTT_TradesmanFlags
    @track loading = false;

    @track allVendors = []; //HOG_PicklistItem_Simple[]
    @track selectedVendor = ''; //Id

    @track allTradesmen = []; //HOG_PicklistItem_Simple[]
    @track selectedTradesmen = []; //Id[]

//CONSTRUCTOR
    /*
    On component load, vendors are loaded.
    */
    constructor() {
        super();
        this.loadVendors();
    }

//APIs
    /*
    API function
    Clear tradesmen and selected tradesmen data.
    if User is Admin, selected vendor is cleared.
    If Single record is Managed, Assigned Vendor is selected based on that Activity.
    otherwise isVendorSupervisor flag is checked.
    if true, AccountId from Tradesman record is used to populate selectedVendor.
    for both cases when Selected Vendor is populated, populateTradesmanOptions is called to fetch Tradesmen options
    for Account
    */
    @api clearData() {
        this.allTradesmen = [];
        this.selectedTradesmen = [];

        if(this.tradesmanInfo) {
            if(this.tradesmanInfo.isAdmin) {
                this.selectedVendor = '';
            }
            if(this.hasOneRecord()) {
                this.selectedVendor = this.woas[0].Assigned_Vendor__c;
                this.populateTradesmanOptions();
            } else if(this.tradesmanInfo.isVendorSupervisor) {
                this.selectedVendor = this.tradesmanInfo.tradesman.AccountId;
                this.populateTradesmanOptions();
            }
        }
    }

    @api getSelectedTradesmen() {
        return this.selectedTradesmen;
    }

    @api getSelectedVendorId() {
        return this.selectedVendor;
    }

//HANDLERS/ACTIONS
    handleVendorSelection(evt) {
        this.selectedVendor = evt.detail.value;
        this.dispatchEvent(new CustomEvent('vendorselection', { detail:this.selectedVendor }));
        this.populateTradesmanOptions();
    }

    handleTradesmenSelection(evt) {
        this.selectedTradesmen = evt.detail.value;
        this.dispatchEvent(new CustomEvent('tradesmenselection', { detail:this.selectedTradesmen }));
    }

//HELPERS
    loadVendors() {
        this.startAction();
        getVendors()
                .then(result => {
                    this.allVendors = result;
                    this.finishAction();
                    this.clearData();
                })
                .catch(error => {
                    const event = new ShowToastEvent({
                        title: 'Error',
                        type: 'error',
                        message: error.body.message
                    });
                    this.dispatchEvent(event);
                    this.finishAction();
                });
    }

    startAction() {
        this.loading = true;
        this.dispatchEvent(new CustomEvent('actionstart'));
    }

    finishAction() {
        this.loading = false;
        this.dispatchEvent(new CustomEvent('actionstop'));
    }

    populateTradesmanOptions() {
        this.startAction();
        getTradesmen({accountId : this.selectedVendor})
                .then(result => {
                    this.allTradesmen = result;
                    if(this.hasOneRecord()) {
                        this.preselectTradesmen();
                    } else {
                        this.finishAction();
                    }
                })
                .catch(error =>{
                    this.finishAction();
                    const event = new ShowToastEvent({
                        title: 'Error',
                        type: 'error',
                        message: error.body.message
                    });
                    this.dispatchEvent(event);
                });
    }

    preselectTradesmen() {
        getSelectedTradesmen({activityId: this.woas[0].Id})
                .then(result => {
                    this.selectedTradesmen = result;
                    this.dispatchEvent(new CustomEvent('tradesmenselection', { detail:this.selectedTradesmen }));
                    this.finishAction();
                })
                .catch(error => {
                    this.finishAction();
                    const event = new ShowToastEvent({
                        title: 'Error',
                        type: 'error',
                        message: error.body.message
                    });
                    this.dispatchEvent(event);
                });
    }

    hasOneRecord() {
        return this.woas
            && this.woas.length === 1;
    }

//GETTERS
    get notAdmin() {
        return !this.tradesmanInfo || !this.tradesmanInfo.isAdmin;
    }

}