/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 18/09/2019 - Created.
*************************************************************************************************/
import { LightningElement, track, api } from 'lwc';
import startWork from '@salesforce/apex/VTT_WOA_ListView_Endpoint.startWork';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class VTT_WOA_StartWork_Popup extends LightningElement {
//VARS
    @api tradesman; //Contact
    @track loading = false;

//HANDLERS/ACTIONS
    close() {
        const closeEvent = new CustomEvent('close');
        this.dispatchEvent(closeEvent);
    }

    save() {
        this.loading = true;
        startWork({tradesman : this.tradesman})
                .then(result => {
                    this.loading = false;
                    const successEvent = new CustomEvent('success', {detail: result});
                    this.dispatchEvent(successEvent);
                })
                .catch(error => {
                    this.loading = false;
                    console.log('Error: ' + error.body.message);
                    const event = new ShowToastEvent({
                        title: 'Error',
                        type: 'error',
                        message: error.body.message
                    });
                    this.dispatchEvent(event);
                });
    }

}