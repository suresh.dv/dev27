/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
import { LightningElement, api } from 'lwc';

export default class HOG_ErrorMsg extends LightningElement {

    @api errorMessage = '';

    get hasError() {
        return this.errorMessage !== undefined && this.errorMessage !== '';
    }

}