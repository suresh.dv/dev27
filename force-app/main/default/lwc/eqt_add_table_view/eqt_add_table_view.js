/**
 * Created by MarosGrajcar on 11/21/2019.
 */

import { LightningElement, track, api } from 'lwc';

export default class EqtAddTableView extends LightningElement {

    @track activeForms;
    @track openModal = false;


    @api _recordId;
    @api
    get recordId() {
        return this._recordId;
    }

    set recordId(value) {
        this._recordId = value;
        this.getActiveAddForms();
    }

    /* OPEN MODAL */
    openCreateForm() {
        this.openModal = !this.openModal;
    }
}