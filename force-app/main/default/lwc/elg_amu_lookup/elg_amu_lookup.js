/**
 * Created by MarosGrajcar on 3/4/2020.
 */

import { LightningElement, api, track } from 'lwc';

export default class ElgAmuLookup extends LightningElement {
//required attributes
    @api label;
    @api objectAPIName;
    @api amuIds = [];
//optional attributes
    @api fieldAPIName = 'Name';
    @api additionalFields = '';
    @api placeholder = '';
    @api isRequired = false;
    @api isReadOnly = false;
    @api isDisabled = false;
    @api iconName = 'standard:service_territory_location';
    @api renderIcon = false;

    @track selectedRecord = undefined;

//APIs
    @api
    getSelectedRecord() {
        console.log('get selected record: '+this.selectedRecord);
        return this.selectedRecord;
    }

    @api
    getSelectedRecordId() {
        console.log('get selected record id: '+this.isRecordSelected());
        if(this.isRecordSelected()) {
            return this.selectedRecord.Id;
        }

        return undefined;
    }

    @api
    selectRecord(record) {
        console.log('get selected record 2: ' +this.selectedRecord);
        this.selectedRecord = record;
    }

    @api
    get preSelectedRecord() {
        console.log('this.selectedRecord: : ' +this.selectedRecord)
        return this.selectedRecord;
    }

    set preSelectedRecord(value) {
        this.selectedRecord = value;
    }

    @api
    clearSelection() {
        console.log('clear');
        this.resetComponent();
    }

    @api
    isValid() {
        console.log('isValid: ' +!this.isRequired() + ' this.isRecordSelected(): ' +this.isRecordSelected());
        return !this.isRequired()
                || this.isRecordSelected();
    }

//MAIN LOGIC
    resetComponent() {
        console.log('resetComponent');
        this.selectedRecord = undefined;
        const evt = new CustomEvent('clearselection');
        this.dispatchEvent(evt);
    }

    handleRecordSelection(evt) {
        console.log('handleREcordSelection');
        this.selectedRecord = evt.detail;
        this.dispatchEvent(
            new CustomEvent(
                'selection',
                {
                    detail: this.selectedRecord
                }
            )
        );
    }

//HELPERS
    isRecordSelected() {
        console.log('isRecordSelected: ' +this.selectedRecord);
        return this.selectedRecord !== undefined
                && this.selectedRecord !== null;
    }

    isReadOnlyMode() {

        console.log('isReadOnlyMode: ' +this.isReadOnly+ ' this.isDisabled: '+this.isDisabled);
        return this.isReadOnly
                || this.isDisabled;
    }

    isSelectedMode() {
        console.log('isSelectedMode: ' +this.isRecordSelected()+ ' !this.isReadOnlyMode() ' +!this.isReadOnlyMode());
        return this.isRecordSelected()
                && !this.isReadOnlyMode();
    }

//GETTERS
    get selectedText() {
        console.log('this.selectedRecord[this.fieldAPIName]: ' + this.selectedRecord[this.fieldAPIName]);
        if(this.selectedRecord) {
            return this.selectedRecord[this.fieldAPIName];
        }
        return '';
    }

    get readOnlyMode() {
        console.log('get readOnlyMode');
        return this.isReadOnlyMode();
    }

    get inputMode() {
        console.log('get inputMode');
        return !this.isSelectedMode()
                && !this.isReadOnlyMode();
    }

    get selectedMode() {
        console.log('get selectedMode');
        return this.isSelectedMode()
                && !this.isReadOnlyMode();
    }
}