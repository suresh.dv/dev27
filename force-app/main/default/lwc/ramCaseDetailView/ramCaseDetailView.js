import { LightningElement, wire, track } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';

import getCaseDetail from '@salesforce/apex/RaMPortalController.getRaMCaseByCaseNumber';
import getAssignments from '@salesforce/apex/RaMPortalController.getAssignmentsByCase';
import setPrimaryTechnician from '@salesforce/apex/RaMPortalController.updatePrimaryTechnician';
import removeTechnicianAssignment from '@salesforce/apex/RaMPortalController.removeTechnicianAssignment';
import assignmentUsersByCase from '@salesforce/apex/RaMPortalController.getAssignmentUsersByCase';
import reassignTechnician from '@salesforce/apex/RaMPortalController.reassignTechnician';
import setActivityStatus from '@salesforce/apex/RaMPortalController.updateActivityStatus';
import getLocationMarkers from '@salesforce/apex/RaMPortalController.getLocationMarkers';
import getConbineAttachments from '@salesforce/apex/RaMPortalController.getConbineAttachments';
import getActivityComments from '@salesforce/apex/RaMPortalController.getActivityComments';

import USER_ID from '@salesforce/user/Id';
import ACCOUNT_NAME from '@salesforce/schema/User.Contact.Account.Name';


const actions = [
  {label: 'Set as Primary', name: 'set_primary'},
  {label: 'Remove Assignment', name: 'remove_assignment'},
  {label: 'Re-assign', name: 're_assign'}
];

const columns = [
  { label: 'Technician', fieldName: 'ContactDetailView', type: 'url',
      typeAttributes: { label:{fieldName:'technicianName'}, target: '_self' } },
  { label: 'Status', fieldName: 'status' },
  { label: 'Work Start Date', fieldName: 'workStart', type: 'date', 
        typeAttributes:{ year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" }},
  { label: 'Work Completed Date', fieldName: 'workEnd', type: 'date', 
        typeAttributes:{ year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit" }},  
  { label: 'Is Primary ?', fieldName: 'isPrimary', type: 'boolean'  }, 
  {
    type: 'action',
    typeAttributes: { rowActions: actions },
  },     
];

const fileColumns = [
  {label: 'Name', fieldName: 'URLLink', type: 'url',
    typeAttributes: {label:{fieldName:'name'}, target: '_blank'} },
  {label: 'CreatedDate', fieldName: 'uploadDate', type: 'date',
    typeAttributes: {year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit"} },
];

const commentsColumns = [
  {label: 'Name', fieldName: 'name'},
  {label: 'Comment', fieldName: 'comments'},
  {label: 'Date', fieldName: 'createdDate', type: 'date',
      typeAttributes: {year: "numeric", month: "2-digit", day: "2-digit", hour: "2-digit", minute: "2-digit"}},
];

export default class RamCaseDetailView extends LightningElement {
  caseNumber;
  _recordId;
  _data;
  
  @track ramCase;
  @track error;
  @track assignedTechnicians;
  @track availableFiles;
  @track availableComments;
  @track columns = columns;
  @track fileColumns = fileColumns;  
  @track commentsColumns = commentsColumns;
  @track showPrimaryAssignmentModal = false;
  @track showRemoveAssignmentModal = false;
  @track showReassignModal = false;
  @track selectedRow ={};
  @track confirmButtonLabel = 'Confirm';
  @track isUpdateDisabled = false;
  @track newlyAssignedTechnician = '';
  @track availabelTechnicians = [];
  @track assignedLbl = 'Assigned';
  @track inProgressLbl = 'Work In Progress';
  @track pendingPartsLbl = 'Pending Parts';
  @track pendingApprovalLbl = 'Pending Approval';
  @track closedLbl = 'Resolved';
  @track completedLbl = 'Completed';
  @track vendorCompletedLbl = 'Vendor Completed';
  @track inProgressDisabled = true;
  @track pendingPartsDisabled = true;
  @track pendingApprovalDisabled = true;
  @track closedDisabled = true;
  @track vendorCompletedDisabled = true; 
  @track showCommentsModal = false; 
  @track comments = '';  
  @track isCommentsModalLblDisabled = false;
  @track currentStep;
  @track showLocationMarkerModal = false;
  @track showActivityCommentsModal = false;  
  @track mapMarkers = [];
  

  @wire(getCaseDetail, {caseNumber: '$caseNumber'})
  wiredCase(result) {
    this._data = result;
    if (result.data) {
      this.ramCase = result.data;
      this.error = undefined;
      this._recordId = result.data.Id;
      this.setCurrentStep();
    } else if (result.error) {
      this.ramCase = undefined;
      this.error = result.error;      
    }
  }
  

  @wire(getRecord, { recordId: USER_ID, fields: [ACCOUNT_NAME] }) 
  wiredUser;   

  connectedCallback() {
    this.caseNumber = this.getUrlParamValue(window.location.href, 'caseNumber');
    this.getAssignmentRecords();
    this.getAttachmentDetail();
  }

  getAssignmentRecords() {
    //wired here to call from connectedCallback
    getAssignments({
      caseNumber: this.caseNumber
    }).then(data => {
      const rows = [];

      data.forEach(function(record) {
        const rec = Object.assign({}, record, {
          ContactDetailView: '/ram-contact-detail?id=' + record.technicianContactId,
        });
        rows.push(rec);
      });

      this.assignedTechnicians = rows;
      this.error = undefined;
    }).
    catch(error => {
      this.assignedTechnicians = undefined;
      this.error = error;
    }) 
  }

  getAttachmentDetail() {
    this.availableFiles;

    getConbineAttachments({
      caseNumber: this.caseNumber
    }).then(data => {
      const rows = [];
      data.forEach(function(record) {
        const rec = Object.assign({}, record, {
          URLLink: record.downloadUrl,
        });
        rows.push(rec);
      });

      this.availableFiles = rows;
      this.error = undefined;
    }).
    catch(error => {
      this.availableFiles = undefined;
      this.error = error;
    })
  }

  get accountName() {
    return getFieldValue(this.wiredUser.data, ACCOUNT_NAME);
  }     

  getUrlParamValue(url, key) {
    return new URL(url).searchParams.get(key);
  }  

  handleRowAction(event) {
    const actionName = event.detail.action.name;
    this.selectedRow = event.detail.row;

    switch(actionName) {
      case 'set_primary':
        this.openPrimaryAssignmentModal();
        break;
      case 'remove_assignment':
        this.openRemoveAssignmentModal();
        break;
      case 're_assign':
        this.openReassignModal();
        break;
      default:
    }    
  }

  async handlePrimaryAssignment() {
    this.disableConfirmButton();

    let assignmentInfo = {
      caseId: this._recordId,
      newTechnicianId: this.selectedRow.technicianContactId
    };

    try{
      await setPrimaryTechnician(assignmentInfo);
      this.getAssignmentRecords();
      this.closePrimaryAssignmentModal();
      this.showSuccessToast('Successfully set primary technician');
    } catch(error) {
      this.showErrorToast('Error updating assignment');
    } finally {
      this.enableConfirmButton();
    }
  }

  async removeAssignment() {
    this.disableConfirmButton();

    let assignmentInfo = {
      assignmentRecordId: this.selectedRow.assignmentRecordId
    };
    
    try{
      await removeTechnicianAssignment(assignmentInfo);
      this.getAssignmentRecords();
      this.closeRemoveAssignmentModal();
      this.showSuccessToast('Successfully removed assignment');
    } catch(error) {
      this.showErrorToast('Error removing assignment');
    } finally {
      this.enableConfirmButton();
    }
  }

  async handleReassignment() {
    if(this.newlyAssignedTechnician == '') {
      return;
    }

    this.disableConfirmButton();

    let assignmentInfo = {
      assignmentRecordId: this.selectedRow.assignmentRecordId,
      newTechnicianId: this.newlyAssignedTechnician
    };

    try{
      await reassignTechnician(assignmentInfo);
      this.getAssignmentRecords();
      this.closeReassignModal();
      this.showSuccessToast('Successfully re assigned');
    }catch(error) {
      this.showErrorToast('Error re assigning task');
    } finally {
      this.enableConfirmButton();
    }

  }

  handleAssignmentSelection(event) {
    this.newlyAssignedTechnician = event.detail.value;
  }

  openReassignModal() {
    const assigned = [];
    this.newlyAssignedTechnician = '';
    this.availabelTechnicians = [];

    assignmentUsersByCase({
      caseRecordId: this._recordId
    }).then(data => {
      data.forEach(function(record) {
        assigned.push({
          label: record.userName,
          value: record.userId              
        });
      });

      this.error = undefined;
      this.availabelTechnicians.push(...assigned);  
    }).
    catch(error => {
      this.showErrorToast('Error while loading RaM technician assignment data');
    })    

    this.showReassignModal = true;
  }

  openLocationMarkerModal() {
    this.mapMarkers = [];

    getLocationMarkers({
      caseRecordId: this._recordId
    }).then(data => {
      this.mapMarkers = [...this.mapMarkers ,
        {
          location: {
            Street: data.siteLoc.locationAddress1,
            City: data.siteLoc.city,
            State: data.siteLoc.province,
            PostalCode: data.siteLoc.postalCode,            
          },
          title: data.siteLoc.locationName,
          icon: 'standard:delegated_account',
        }
      ];

      data.activities.forEach(dataItem => {
        this.mapMarkers = [...this.mapMarkers ,
          {
            location: {
              Latitude: dataItem.Activity_Location__Latitude__s,
              Longitude: dataItem.Activity_Location__Longitude__s,
            },
            title: dataItem.To_Status__c,
            description: dataItem.Activity_Date__c,
            icon: 'standard:custom',
          }
        ];
      });

    }).
    catch(error => {
      this.showErrorToast('Error while loading location data');
      console.log(error);
    })    

    this.showLocationMarkerModal = true;
  } 

  openActivityCommentsModal() {
    this.availableComments = {};

    getActivityComments({
      caseRecordId: this._recordId
    }).then(data => {
      this.availableComments = data;
      this.error = undefined;
    }).
    catch(error => {
      this.availableComments = undefined;
      this.error = error;
    })

    this.showActivityCommentsModal = true;  
  }

  setCurrentStep() {
    if(this.ramCase.Status === 'Assigned') {
      this.currentStep = 's0';
      this.enableWorkButtons();
    } else if(this.ramCase.Status === 'Work in Progress') {
      this.currentStep = 's1';
      this.enableWorkButtons();
    } else if(this.ramCase.Status === 'Pending Parts') {
      this.currentStep = 's2';
      this.enableWorkButtons();
    } else if(this.ramCase.Status === 'Pending Approval') {
      this.currentStep = 's3';
      this.enableWorkButtons();
    } else if(this.ramCase.Status === 'Resolved') {
      this.currentStep = 's4';
      this.enableWorkButtons();
    } else if(this.ramCase.Status === 'Disputed') {
      this.currentStep = 's0';
      this.enableWorkButtons();
    } else if(this.ramCase.Status === 'Completed') {
      this.currentStep = 's5';
      this.inProgressDisabled = true;
      this.pendingPartsDisabled = true;
      this.pendingApprovalDisabled = true;
      this.closedDisabled = true;  
      this.vendorCompletedDisabled = false;          
    } else if(this.ramCase.Status === 'Vendor Completed') {
      this.currentStep = 's6';
      this.inProgressDisabled = true;
      this.pendingPartsDisabled = true;
      this.pendingApprovalDisabled = true;
      this.closedDisabled = true;  
      this.vendorCompletedDisabled = true;     
    }
  }  

  enableWorkButtons() {
    this.inProgressDisabled = false;
    this.pendingPartsDisabled = false;
    this.pendingApprovalDisabled = false;
    this.closedDisabled = false;    
    this.vendorCompletedDisabled = false;
  }  

  startWork() {
    this.commentsModalLbl = this.inProgressLbl;
    this.openCommentsModal();
  }

  pendingParts() {
    this.commentsModalLbl = this.pendingPartsLbl;
    this.openCommentsModal();
  }

  pendingApproval() {
    this.commentsModalLbl = this.pendingApprovalLbl;
    this.openCommentsModal();
  }

  workCompleted() {
    this.commentsModalLbl = this.closedLbl;
    this.openCommentsModal();
  }  

  vendorCompleted() {
    this.commentsModalLbl = this.vendorCompletedLbl;
    this.openCommentsModal();
  }    

  submitComments() {
    var getPosition = function (options) {
      return new Promise(function (resolve, reject) {
        navigator.geolocation.getCurrentPosition(resolve, reject, options);
      });
    }

    this.disableCommentsModalLbl();

    getPosition()
      .then((position) => {
        let currentPosition = {
          recordId: undefined,
          lat: position.coords.latitude,
          lon: position.coords.longitude,
          comments: this.comments,
          currentStatus: this.ramCase.Status,
          nextStatus: this.commentsModalLbl,
          errorCode: '',
          caseRecordId: this.ramCase.Id
        };

        this.updateActivity(currentPosition);
      })
      .catch((err) => {
        let currentPosition = {
          recordId: undefined,
          lat: undefined,
          lon: undefined,
          comments: this.comments,
          currentStatus: this.ramCase.Status,
          nextStatus: this.commentsModalLbl,
          errorCode: err.message,
          caseRecordId: this.ramCase.Id
        };

        this.updateActivity(currentPosition);
      })
  }  

  async updateActivity(currentPosition) {

    try{
      await setActivityStatus(currentPosition);
      this.enableCommentsModalLbl();
      this.showSuccessToast('Activity updated successfully');
      return refreshApex(this._data); 

    }catch(error) {
      this.enableCommentsModalLbl();
      this.showErrorToast('Error updating activity');
      console.log(error);
    } finally {
      this.closeCommentsModal();
    }

  }  

  handleCommentsChange(event) {
    this.comments = event.target.value;
  }  

  closeReassignModal() {
    this.showReassignModal = false;
  }

  disableConfirmButton() {
    this.confirmButtonLabel = 'Updating...';
    this.isUpdateDisabled = true;
  }

  enableConfirmButton() {
    this.confirmButtonLabel = 'Confirm';
    this.isUpdateDisabled = false;    
  }  
  
  openPrimaryAssignmentModal() {
    this.showPrimaryAssignmentModal = true;
  }

  closePrimaryAssignmentModal() {
    this.showPrimaryAssignmentModal = false;
  }

  openRemoveAssignmentModal() {
    this.showRemoveAssignmentModal = true;
  }

  closeRemoveAssignmentModal() {
    this.showRemoveAssignmentModal = false;
  }

  openCommentsModal() {
    this.comments = '';
    this.showCommentsModal = true;
  }

  closeCommentsModal() {
    this.commentsModalLbl = '';
    this.showCommentsModal = false;
  }

  closeLocationMarkerModal() {
    this.showLocationMarkerModal = false;
  } 
  
  closeActivityCommentsModal() {
    this.showActivityCommentsModal = false;
  }
  
  enableCommentsModalLbl() {
    this.isCommentsModalLblDisabled = false;
  }  

  disableCommentsModalLbl() {
    this.isCommentsModalLblDisabled = true;
  }    

  showSuccessToast(successMessage) {
    const event = new ShowToastEvent({
      variant: 'success',
      message: successMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }

  showErrorToast(errorMessage) {
    const event = new ShowToastEvent({
      variant: 'error',
      message: errorMessage,
      mode: 'pester'
    });
    this.dispatchEvent(event);
  }     

}