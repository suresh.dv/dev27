/**
 * Created by MarcelBrimus on 28/11/2019.
 */
import { LightningElement, api, wire, track } from 'lwc';
import fetchUserData from '@salesforce/apex/VTT_LTNG_PortalManagementEndpoint.loadUserData';
import fetchContacts from '@salesforce/apex/VTT_LTNG_PortalManagementEndpoint.loadAccountContacts';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class vtt_lightning_community_management extends NavigationMixin(LightningElement) {
    // Flag for loading
    @track contactsLoading = false;
    @track fetchingUserData = false;
    @track selectedAccount;
    @track showModal = false;
    @track savingContact = false;

    @api _isVendorCommunityUser;
    @api _accounts = [];
    @api _userAccount = '';
    @api _enabledContacts = [];
    @api _disabledContacts = [];
    @api _recordType;

    // GETTERS SETTERS
    get isVendorCommunityUser(){ return this._isVendorCommunityUser; }
    get accounts(){ return this._accounts; }
    get userAccount(){ return this._userAccount; }
    get enabledContacts(){ return this._enabledContacts; }
    get disabledContacts(){ return this._disabledContacts; }
    get recordType(){ return this._recordType; }

    /* CONSTRUCTOR */
    constructor() {
        super();
        this.initializeData();
    }

    initializeData(){
        this.fetchingUserData = true;

        fetchUserData().then(result => {
            if(result.success){
                if(result.resultObjects && result.resultObjects.length > 0){
                    this._userAccount = result.resultObjects[0].vendorAccount;
                    this._isVendorCommunityUser = result.resultObjects[0].isCommunityUser;
                    this._enabledContacts = result.resultObjects[0].enabledContacts;
                    this._disabledContacts = result.resultObjects[0].disabledContacts;
                    this.populateAccountPicklist(result.resultObjects[0].vendorAccounts);
                    this.selectedAccount = this._userAccount.Id;
                    this._recordType = result.resultObjects[0].hogContactRecordType;
                }
            } else {
                 var errorMessage = '';
                 result.errors.forEach(function (item, index) {
                     errorMessage = errorMessage + item + '.'
                 });
                 this.showNotification('Error getting user data ', errorMessage, 'error');
             }
             this.fetchingUserData = false;
        }).catch(error => {
             this.fetchingUserData = false;
             console.log('initializeData err ' + error)
         });
    }

    refreshPage(){
        this.refreshContacts(this.selectedAccount);
    }

    createContact(){
       this[NavigationMixin.Navigate]({
           type: 'standard__objectPage',
           attributes: {
               objectApiName: 'Contact',
               actionName: 'new'
           }
       });
    }

    handleSaveSuccess(){
        this.closeModal();
        this.refreshContacts(this.selectedAccount);
    }

    populateAccountPicklist(values){
        var tempAccounts = [];
        if(values){
            values.forEach((item) => {
                var acc = {
                    label : item.Name,
                    value : item.Id
                }
                tempAccounts.push(acc);
            });
        }
        this._accounts = tempAccounts;
    }

    refreshContacts(value){
        this.contactsLoading = true;
        fetchContacts({
            accountId: value
        }).then(result => {
            if(result.success){
                if(result.resultObjects && result.resultObjects.length > 0){
                    this._userAccount = result.resultObjects[0].vendorAccount;
                    this._enabledContacts = result.resultObjects[0].enabledContacts;
                    this._disabledContacts = result.resultObjects[0].disabledContacts;
                }
            } else {
                 var errorMessage = '';
                 result.errors.forEach(function (item, index) {
                     errorMessage = errorMessage + item + '.'
                 });
                 this.showNotification('Error getting contacts ', errorMessage, 'error');
            }
            this.contactsLoading = false;
        }).catch(error => {
            this.contactsLoading = false;
            console.log('onAccountChange err ' + error)
        });
    }

    onAccountChange(event){
        this.selectedAccount = event.detail.value;
        this.refreshContacts(event.detail.value);
    }

    // UTILITIES
    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    openmodal() {
        this.showModal = true
    }

    closeModal() {
        this.showModal = false
    }
}