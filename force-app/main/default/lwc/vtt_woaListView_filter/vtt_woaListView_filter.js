/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
const FILTER_COOKIE_NAME = 'VTT_WOAListViewFilter_CookieFilter';

import { LightningElement, track } from 'lwc';
import getOptionsWithFlags from '@salesforce/apex/VTT_WOA_SearchCriteria_Endpoint.getFilterFormOptionsForCurrentUser';
import loadTradesman from '@salesforce/apex/VTT_WOA_SearchCriteria_Endpoint.loadTradesmanForAccount';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import userId from '@salesforce/user/Id';

export default class VTT_WOAListViewFilter extends LightningElement {
//VARS
    @track selectOptions = {}; //VTT_WOA_SearchCriteria_Options
    cleanCriteria = {}; //VTT_WOA_SearchCriteria
    @track filter = {}; //VTT_WOA_SearchCriteria

    @track loading = false;
    @track initialLoading = true;

    @track errorMessage = undefined;

    @track showSavePopup = false;

//CONSTRUCTOR
    constructor() {
        super();
        this.initFilters();
    }

//HANDLERS/ACTIONS
    handleLoadSuccess(result) {
        this.selectOptions = result;
        this.cleanCriteria = result.cleanCriteria;
        this.prepopulateFilter();
        this.errorMessage = undefined;
        this.loading = false;
        this.initialLoading = false;
    }

    handleLoadTradesmanSuccess(result) {
        this.selectOptions.tradesmans = result;
        this.loading = false;
        this.errorMessage = undefined;
    }

    handleError(error) {
        if(error.body && error.body.message) {
            this.errorMessage = error.body.message;
        } else {
            this.errorMessage = error;
        }
        this.loading = false;
        this.initialLoading = false;
        this.showErrorToast();
    }

    handleSearch() {
        let filter = this.filter;
        filter.vendorName = this.getVendorName();
        filter.tradesmanName = this.getTradesmanName();
        filter.AMUName = this.getAMUName();
        this.createCookie(FILTER_COOKIE_NAME, JSON.stringify(filter), null);
        const searchEvent = new CustomEvent('search', {detail: filter});
        this.dispatchEvent(searchEvent);
    }

    handleClear() {
        this.filter = Object.assign({}, this.cleanCriteria);
        const filterStore = this.template.querySelector('c-vtt_woa-list-view_filter-store');
        if(filterStore) {
            filterStore.selectFilter('');
        }
        this.createCookie(FILTER_COOKIE_NAME, "", null);
    }

    handleSave() {
        this.showSavePopup = true;
    }

    closeSavePopup(evt) {
        this.showSavePopup = false;
        if(evt.detail === true) {
            const filterStore = this.template.querySelector('c-vtt_woa-list-view_filter-store');
            if(filterStore) {
                filterStore.reload();
            }
        }
    }

    handleStoredFilterSelection(evt) {
        this.loading = true;
        const filterStore = this.template.querySelector('c-vtt_woa-list-view_filter-store');
        if(filterStore) {
            const selectedFilter = filterStore.getSelectedFilter();
            this.setSelectedFilter(selectedFilter);
            this.createCookie(FILTER_COOKIE_NAME, "", null);
        }
    }

    handleLastFilterLoaded(evt) {
        let cookieFilter = this.getCookie(FILTER_COOKIE_NAME);
        if(!cookieFilter) {
            this.loading = true;
            this.setSelectedFilter(evt.detail);
        } else {
            console.log('Skipping stored filter preselection. Using Cookie filter instead.');
        }
    }

    handleVendorSelection(evt) {
        this.filter.vendorFilter = evt.detail.value;
        this.preselectTradesmanFilter();
        this.loadTradesmanForVendor();
    }

    handleTradesmanChange(evt) {
        this.filter.tradesmanFilter = evt.detail.value;
    }

    handleScheduleFromChange(evt) {
        this.filter.scheduledFromFilter = evt.detail.value;
    }

    handleScheduleToChange(evt) {
        this.filter.scheduledToFilter = evt.detail.value;
    }

    handleAMUChange(evt) {
        this.filter.amuFilter = evt.detail.value;
    }

    handleRouteChange(evt) {
        this.filter.routeFilter = evt.detail.value;
    }

    handlePlannerGroupChange(evt) {
        this.filter.plannerGroupFilter = evt.detail.value;
    }

    handleActivityStatusChange(evt) {
        this.filter.activityMultiStatusFilter = [];
        if(evt.detail.value !== undefined && evt.detail.value !== '') {
            this.filter.activityMultiStatusFilter = evt.detail.value;
        }
    }

    handleHideCompletedChange(evt) {
        this.filter.hideCompletedFilter = !this.filter.hideCompletedFilter;
    }

    handleAnyTextChange(evt) {
        this.filter.filterName = evt.detail.value;
    }

//HELPERS
    initFilters() {
        this.loading = true;
        getOptionsWithFlags()
                .then(result => {
                    this.handleLoadSuccess(result);
                })
                .catch(error =>{
                    this.handleError(error);
                });
    }

    prepopulateFilter() {
        let cookieFilter = this.getCookie(FILTER_COOKIE_NAME);
        if(cookieFilter) {
            this.filter = JSON.parse(cookieFilter);
            this.handleSearch();
        } else {
            this.filter = Object.assign({}, this.cleanCriteria);
        }
    }

    getVendorName() {
        if(this.filter.vendorFilter) {
            for(let i = 0; i < this.selectOptions.vendors.length; i++) {
                if(this.selectOptions.vendors[i].value === this.filter.vendorFilter) {
                    return this.selectOptions.vendors[i].label;
                }
            }
        }
        return undefined;
    }

    getTradesmanName() {
        if(this.filter.tradesmanFilter) {
            for(let i = 0; i < this.selectOptions.tradesmans.length; i++) {
                if(this.selectOptions.tradesmans[i].value === this.filter.tradesmanFilter) {
                    return this.selectOptions.tradesmans[i].label;
                }
            }
        }
        return undefined;
    }

    preselectTradesmanFilter() {
        if(this.selectOptions.tradesman.AccountId === this.filter.vendorFilter) {
            this.filter.tradesmanFilter = this.selectOptions.tradesman.Id;
        } else {
            this.filter.tradesmanFilter = '';
        }
    }

    getAMUName() {
        if(this.filter.amuFilter) {
            for(let i = 0; i < this.selectOptions.AMUs.length; i++) {
                if(this.selectOptions.AMUs[i].value === this.filter.amuFilter) {
                    return this.selectOptions.AMUs[i].label;
                }
            }
        }
        return undefined;
    }

    setSelectedFilter(selectedFilter) {
        if(selectedFilter) {
            this.filter = JSON.parse(selectedFilter.Filter_String__c);
        }
        this.loading = false;
    }

    loadTradesmanForVendor() {
        this.loading = true;
        loadTradesman({accountId: this.filter.vendorFilter})
                .then(result => {
                    this.handleLoadTradesmanSuccess(result);
                })
                .catch(error => {
                    this.handleError(error);
                });
    }

    showErrorToast() {
        const event = new ShowToastEvent({
            title: 'Error',
            type: 'error',
            message: this.errorMessage
        });
        this.dispatchEvent(event);
    }

    isPrivilegedUser() {
        return this.selectOptions.isAdmin || this.selectOptions.isVendorSupervisor;
    }

    displayFooter() {
        return this.errorMessage === undefined && this.isPrivilegedUser();
    }

    getCookie(name) {
        let cookieString = "; " + document.cookie;
        let parts = cookieString.split("; " + userId + name + "=");
        if (parts.length === 2) {
            return unescape(parts.pop().split(";").shift());
        }
        return null;
    }

    createCookie(name, value, days) {
        let expires;
        if (days) {
            const date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = userId + name + "=" + escape(value) + expires + "; path=/";
    }

}