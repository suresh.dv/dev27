/**
 * Created by MarosGrajcar on 3/4/2020.
 */

import { LightningElement, api } from 'lwc';

const WRONG_FIELD_ERROR = 'Unknown field to return';

export default class ElgAmuLookupResultItem extends LightningElement {

    @api record;
    @api field = 'Name';

    selectRecord(originalEvt) {
        const evt = new CustomEvent('selection', {detail: this.record});
        this.dispatchEvent(evt);
    }

    get resultName() {

        if(this.record !== null && this.record !== undefined && this.record[this.field]) {
            console.log(this.record[this.field]);
            return this.record[this.field];
        }

        return WRONG_FIELD_ERROR;
    }

}