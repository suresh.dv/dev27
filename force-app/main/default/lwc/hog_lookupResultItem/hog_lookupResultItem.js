/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-08-21 - Created.
*************************************************************************************************/
import { LightningElement, api } from 'lwc';

const WRONG_FIELD_ERROR = 'Unknown field to return';

export default class HOG_LookupResultItem extends LightningElement {

    @api record; //SObject
    @api field = 'Name';

    /**
        Fires selection event with detail containing the result.
    */
    selectRecord(originalEvt) {
        const evt = new CustomEvent('selection', {detail: this.record});
        this.dispatchEvent(evt);
    }

    get resultName() {

        if(this.record !== null && this.record !== undefined && this.record[this.field]) {
            return this.record[this.field];
        }

        return WRONG_FIELD_ERROR;
    }

}