/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-08-21 - Created.
*************************************************************************************************/
import { LightningElement, api, track } from 'lwc';
import fetchData from '@salesforce/apex/HOG_LookupServiceLtng.fetchData';

const MINIMAL_SEARCH_TERM_LENGTH = 2;
const SEARCH_DELAY = 300;
const HIDE_RESULT_DELAY = 300;
const NO_RESULTS_MESSAGE = 'No results found';

export default class HOG_LookupInput extends LightningElement {

//required attributes
    @api label; //String
    @api objectAPIName; //String - target SObject API Name
//optional attributes
    @api fieldAPIName = 'Name'; //String - field API Name that result would be displayed
    @api additionalFields = ''; //String - field API Names (comma delimiter) that would query additional values
    @api placeholder = ''; //String - placeholder on empty input
    @api isRequired = false;
    @api withSharing; //Boolean - with/without sharing flag. Defaulted to true

    @track searchTerm = '';
    @track hasFocus = false;
    @track searching = false;

    @track displayResults = false;
    @track searchResults = []; //List<SObject>
    @track searchMessage = '';
    @api selectedRecord = undefined; //SObject
    searchTimeOut;

    constructor() {
        super();
        this.withSharing = true;
    }

//MAIN LOGIC
    handleSelection(evt) {
        this.dispatchEvent(
            new CustomEvent(
                'selection',
                {
                    detail: evt.detail
                }
            )
        );
        this.displayResults = false;
    }

    /**
        Handles when users clicks outside of input.
        After some delay, results pane would be hidden.
    */
    handleBlur() {
        setTimeout(() => {
                this.displayResults = false
            },
            HIDE_RESULT_DELAY
        );
    }

    /**
        Handles action when users clicks on the input.
        Displays result if search term has proper length.
    */
    handleFocus() {
        if(this.searchTerm.length < MINIMAL_SEARCH_TERM_LENGTH) {
            this.displayResults = false;
            return;
        }

        this.displayResults = true;
    }

    /**
        Handles user input to lookup.
        sets Search term,
        kills previous search call timeout
        then runs search.
        if search term doesn't have valid length, results are hidden and search is not run.
    */
    handleInput(evt) {
        this.searchTerm = evt.target.value;

        this.killPreviousSearchTimeOut();

        if(this.searchTerm.length < MINIMAL_SEARCH_TERM_LENGTH) {
            this.displayResults = false;
            return;
        }

        this.runSearch();
    }

    /**
        Kills previous search timeout.
    */
    killPreviousSearchTimeOut() {
        if(this.searchTimeOut) {
            clearTimeout(this.searchTimeOut);
            this.searchTimeOut = undefined;
        }
    }

    /**
        Runs search for current search terms.
        it uses timeout to delay searching while user is still typing.
    */
    runSearch() {
        this.searching = true;
        this.displayResults = true;
        this.searchTimeOut = setTimeout(() => {
                fetchData({
                        objectName: this.objectAPIName,
                        fieldName: this.fieldAPIName,
                        stringToSearch: this.searchTerm,
                        additionalFields: this.additionalFields,
                        withSharing: this.withSharing
                    })
                    .then(result => {
                        this.handleResult(result);
                    })
                    .catch(error => {
                        this.searchMessage = error;
                        this.searching = false;
                    });
            },
            SEARCH_DELAY
        );
    }

    /**
        handles success callback for search action.
    */
    handleResult(result) {
        this.searchMessage = '';
        this.searchResults = result;
        if(result.length === 0) {
            this.searchMessage = NO_RESULTS_MESSAGE;
        }
        this.searching = false;
    }

//GETTERS
    get searchMessageEmpty() {
        return this.searchMessage === ''
                || this.searchMessage === null
                || this.searchMessage === undefined;
    }
}