/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 17/10/2019 - Created.
*************************************************************************************************/
import { LightningElement, api, track } from 'lwc';
import TIME_ZONE from '@salesforce/i18n/timeZone';
import { NavigationMixin } from 'lightning/navigation';

const H2S_STYLE = 'h2s_activity';

export default class VTT_WOAListView_WOATable extends LightningElement {
//VARs
    @track woas = []; //Work_Order_Activities__c[]
    @track allSelected = false;
    @api tradesmanInfo; //VTT_TradesmanFlags

    selectedWOAs = []; //Work_Order_Activities__c[]

    set records(values) {
        this.setWOAs(values);
    }

//APIs
    @api
    get records(){
        return this.woas;
    }

    @api
    getSelectedRecords() {
        return this.selectedWOAs;
    }

    @api
    clearSelection() {
        this.selectedWOAs = [];
        if(this.woas) {
            for(var i = 0; i < this.woas.length; i++) {
                this.woas[i].selected = false;
            }
        }
        this.allSelected = false;
    }

//HANDLERS/ACTIONS
    handleSelectAll(evt) {
        this.allSelected = !this.allSelected;
        this.runSelection();
    }

    handleSelection(evt) {
        if(this.woas && this.woas.length > evt.target.dataset.recordIndex) {
            this.woas[evt.target.dataset.recordIndex].selected = !this.woas[evt.target.dataset.recordIndex].selected;
            this.refreshSelectedWOAs();
        } else {
            console.log('Invalid index.');
        }
    }

//HELPERS
    setWOAs(records) {
        this.woas = [];
        if(records) {
            for(let i = 0; i < records.length; i++) {
                this.woas.push(JSON.parse(JSON.stringify(records[i])));//To overcome locked records
                this.woas[i].selected = false;
                if(this.woas[i].Assigned_Vendor__r !== undefined) {
                    this.woas[i].vendorName = this.woas[i].Assigned_Vendor__r.Name;
                } else {
                    this.woas[i].vendorName = '';
                }
                this.woas[i].rowClass = this.getH2SStyling(this.woas[i]);
            }
        }
    }

    getH2SStyling(woa) {
        let isWorkOrderH2S = this.isLocationH2S(woa.Maintenance_Work_Order__r.Location__r);
        let isActivityH2S = this.isLocationH2S(woa.Location__r);

        if(isWorkOrderH2S || isActivityH2S) {
            return H2S_STYLE;
        }

        return '';
    }

    isLocationH2S(location) {
        if(location) {
            return location.Hazardous_H2S__c != null && location.Hazardous_H2S__c.includes('Y');
        }

        return false;
    }

    runSelection() {
        this.selectedWOAs = [];
        if(this.woas) {
            for(let i = 0; i < this.woas.length; i++) {
                this.woas[i].selected = this.allSelected;
                if(this.allSelected) {
                    this.selectedWOAs.push(this.woas[i]);
                }
            }
        }
    }

    refreshSelectedWOAs() {
        this.selectedWOAs = [];
        if(this.woas) {
            for(let i = 0; i < this.woas.length; i++) {
                if(this.woas[i].selected) {
                    this.selectedWOAs.push(this.woas[i]);
                }
            }
        }
    }

//GETTERS
    get timeZone() {
        return TIME_ZONE;
    }

    get isPrivilegedUser() {
        if(this.tradesmanInfo) {
            return this.tradesmanInfo.isAdmin
                || this.tradesmanInfo.isVendorSupervisor;
        }
        return false;
    }

}