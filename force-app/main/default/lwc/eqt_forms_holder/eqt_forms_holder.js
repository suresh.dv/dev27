/**
 * Created by MarosGrajcar on 11/21/2019.
 */

import { LightningElement, track, api } from 'lwc';

export default class EqtFormsHolder extends LightningElement {
    @track openAddFormModal = false;
    @track openTransferToFormModal = false;
    @track openTransferFromFormModal = false;


    _recordId;
    @api
    get recordId() {
        return this._recordId;
    }

    set recordId(value) {
        this._recordId = value;
    }

    @api showforms = false;
    @track showButtons = true;

    enableButtons() {
        this.showButtons = true;
    }

    saveAddForm() {
        var saveMethod = this.template.querySelector('c-eqt_add_cru');
        if(saveMethod) {
            this.showButtons = false;
            saveMethod.getUserInputs();
        }
    }

    saveTransferForm() {
        var saveMethod = this.template.querySelector('c-eqt_transfer_cru');
        if (saveMethod) {
            this.showButtons = false;
            saveMethod.getUserInputs();
        }
    }

    handleAddEquipmentForm() {
        this.openAddFormModal = true;
        this.enableButtons();
    }

    handleTransferToEquipmentForm() {
        this.openTransferToFormModal = true;
        this.enableButtons();
    }

    handleTransferFromEquipmentForm() {
        this.openTransferFromFormModal = true;
        this.enableButtons();
    }

    closeAddFormModal() {
        var getNewForms = this.template.querySelector('c-eqt_add_transfer_forms');
        if(getNewForms) {
            getNewForms.loadForms();
        }
        this.openAddFormModal = false;
    }

    closeTransferToFormModal() {
        var getNewForms = this.template.querySelector('c-eqt_add_transfer_forms');
        if(getNewForms) {
            getNewForms.loadForms();
        }
        this.showButtons = false;
        this.openTransferToFormModal = false;
    }

    closeTransferFromFormModal() {
        var getNewForms = this.template.querySelector('c-eqt_add_transfer_forms');
        if(getNewForms) {
            getNewForms.loadForms();
        }
        this.showButtons = false;
        this.openTransferFromFormModal = false;
    }
}