<apex:page controller="AccountLocatorController">
    <html xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <script src="../../soap/ajax/40.0/connection.js" type="text/javascript"></script>
    <apex:includeScript value="{!URLFOR($Resource.jQueryCPM, 'jquery-3.3.1.min.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jquerySfMap, 'jquerySfMap.1.0.1.js')}"/>  
    <style>
        .account-locator-map {
            font-family: Arial;
            margin-top: 0.5rem;
            margin-bottom: 0.5rem;
            font-size:12px;
            line-height:normal !important;
            height:85vh;
            background:transparent;
            margin-right: 1.5rem;  
            margin-left: 1.5rem;
        }
        .account-locator-list {
            float:left;
            min-width:100px;  
            max-width:300px;
            margin-left: 1.5rem;
            padding-top: 0.5rem;
            max-height:85vh;
            overflow:auto;
        }
        .account-locator-wrapper {
            float:left;          
            padding-left:1.5rem;
            padding-top: 0.5rem;
            max-width:200px;
        }
         .account-locator-buttons {
             margin-top: 0.5rem;
         }
    
        .account-locator-detail{ 
            width:500px;
            font-family: "Salesforce Sans",Arial,sans-serif;
        }
        .account-locator-section > span {
            border-bottom: 1px solid lightgray;
            padding: 3px;
            padding-left: 0px;
            margin-bottom:3px;
            margin-top: 6px;
            display: inline-block;
            font-weight:bold;
        }
        .account-locator-section-table div {
            display: inline-block;
            width: 50%;
            vertical-align:top;
        }
        .account-locator-section-table > div {
            width: 50%;
        }
        .account-locator-detail-mobile{
            width:210px;
        }
        .account-locator-detail-mobile .account-locator-section-table > div {
            width: 100%;
        }
        .account-locator-lookup-visible {       
            visibility: visible !important;
            opacity: 1 !important;
            display: block !important;  
        }
    </style>  
    <apex:slds >
    <apex:form >
        <article class="slds-card">
            <div class="slds-card__header slds-grid">
                <header class="slds-media slds-media_center slds-has-flexi-truncate">
                    <div class="slds-media__figure">
                        <span class="slds-icon_container slds-icon-standard-contact">
                            <svg class="slds-icon slds-icon_small" aria-hidden="true">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/assets/icons/standard-sprite/svg/account_120.png" />
                            </svg>
                        </span>
                    </div>
                    <div class="slds-media__body">
                        <h2>
                            <a href="javascript:void(0);" class="slds-card__header-link slds-truncate" title="[object Object]">
                                <span class="slds-text-heading_small">Account Locator</span>
                            </a>
                        </h2>
                    </div>
                </header>
                <apex:outputPanel styleClass="slds-no-flex" rendered="{! $User.UITheme == 'Theme4t'}" >
                    <button class="slds-button slds-button_neutral" onClick="return false;" id="m_resetBtn">Reset</button>
                    <button class="slds-button slds-button_brand" onClick="return false;" id="m_searchBtn">Search</button>
                </apex:outputPanel>  
            </div>
            <div class="slds-card__body slds-card__body_inner">
            </div>
            <footer class="slds-card__footer"></footer>
        </article>
        <div>
            <apex:outputPanel >
                <div class="account-locator-wrapper">
                    <div class="slds-form-element" name="name">
                        <label class="slds-form-element__label" for="text-input-id-1">Account Name:</label>
                        <div class="slds-form-element__control">
                            <div class="slds-combobox_container slds-has-inline-listbox">
                                <div class="slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-combobox-lookup" aria-expanded="false" aria-haspopup="listbox" role="combobox">
                                    <div class="slds-combobox__form-element slds-input-has-icon slds-input-has-icon_right" role="none">    
                                        <apex:inputtext value="{!Name}" id="name" styleClass="slds-input slds-combobox__input"  html-autocomplete="off" />
                                            <span style="color:gray;" class="slds-icon_container slds-icon-utility-search slds-input__icon slds-input__icon_right">
                                            <svg aria-hidden="true" class="slds-button__icon slds-button__icon--left">
                                                <use xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#search')}">
                                                </use>
                                            </svg>   
                                        </span>
                                        <div class="slds-pill-container slds-hide">
                                              <span class="slds-pill slds-truncate" style="width:168px;">
                                                 <span class="slds-pill__label" id="namePill">
                                                 </span>
                                                 <span class="slds-pill__remove" style="color:gray;width:1.2rem;">
                                                   <svg aria-hidden="true" class="slds-button__icon slds-button__icon--left">
                                                        <use xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#close')}">
                                                        </use>
                                                    </svg>   
                                                 </span>
                                              </span>
                                        </div>
                            
                                        <div id="listbox-unique-id" role="listbox">
                                            <ul class="slds-listbox slds-listbox_vertical slds-dropdown slds-dropdown_fluid" role="presentation">
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="slds-form-element">
                        <label class="slds-form-element__label" for="cities">Account City:</label>
                        <div class="slds-form-element__control">
                            <div class="slds-select_container">
                                <apex:selectList value="{!City}" size="1" id="cities" styleClass="slds-select">
                                    <apex:selectOptions value="{!Cities}"/>
                                </apex:selectList>
                            </div>
                        </div>
                    </div>
                    <div class="slds-form-element">
                        <label class="slds-form-element__label" for="states">Account Province/State:</label>
                        <div class="slds-form-element__control">
                            <div class="slds-select_container">
                                <apex:selectList value="{!State}" size="1" id="states" styleClass="slds-select">
                                    <apex:selectOptions value="{!States}"/>
                                </apex:selectList>
                            </div>
                        </div>
                    </div>
                    <div class="slds-form-element">
                        <label class="slds-form-element__label" for="select-01">Account Postal Code/Zip:</label>
                        <div class="slds-form-element__control">
                            <apex:inputtext value="{!Zip}" id="zip" styleClass="slds-input" />
                        </div>
                    </div>       
                    <apex:outputPanel styleClass="slds-form-element" rendered="{! $User.UITheme != 'Theme4t'}" >
                        <div class="slds-form-element__control account-locator-buttons">
                            <button class="slds-button slds-button_neutral" onClick="return false;" id="resetBtn">Reset</button>
                            <button class="slds-button slds-button_brand" onClick="return false;" id="searchBtn">Search</button>
                        </div>
                    </apex:outputPanel>  
                </div>
              </apex:outputPanel>         
                <div class="account-locator-list">
                    <label class="slds-form-element__label" for="text-input-id-1">Accounts:</label>
                    <div></div>
                </div>
                <div style="overflow:auto;min-width: 300px;">
                    <div class="account-locator-map"></div>
                </div>
                <div style="clear:both;"></div>    
        </div>
    </apex:form>
    </apex:slds>
    <script type="text/javascript">                   
        sforce.connection.sessionId='{!GETSESSIONID()}';
        j$ = jQuery.noConflict();
        
        //self for Salesforce1
        function GetHrefTarget(){
            if('{! $User.UITheme}' == 'Theme4t') {
                return '_self';
            }
            else {
                return '_blank';
            }
        }

        // Finds the latest opportunity for a specific account and returns it's name and date of creation
        function getLastOpportunity(account) {
            var lastOpportunity = {id: "", name: "", date: ""};
            if (account.Opportunities) {
                var date = new Date(account.Opportunities.records.CreatedDate);
                lastOpportunity.name = account.Opportunities.records.Name;
                lastOpportunity.date = formatDate(date);
                lastOpportunity.id = account.Opportunities.records.Id;
                return lastOpportunity;
            } else {
                lastOpportunity.name = lastOpportunity.date = "Not available";
                return lastOpportunity;
            }
        }

        // Returns a String which is used in the .sfMap() method to identify this field as a hyperlink if Opportunity exists.
        function isLinkable(lastOpportunity) {
            if (lastOpportunity.id) {
                return 'detailRowHref';
            } else {
                return 'detailRow';
            }
        }
        
        // Finds the latest activity for a specific account and returns it's type and date of creation
        function getLastActivity(account) {
            var lastActivity = {name: "", date: ""};
            var lastActivityDate;
            if (account.Tasks) {
                var subject = account.Tasks.records.Subject;
                subject ? lastActivity.name = subject : lastActivity.name= "Task";
                lastActivityDate = new Date(account.Tasks.records.CreatedDate);
            }
            if (account.Events){
                var eventDate = new Date(account.Events.records.CreatedDate);
                if (lastActivity.name) {
                    if (lastActivityDate.getTime() < eventDate.getTime()) {
                        var subject = account.Events.records.Subject;
                        subject ? lastActivity.name = subject : lastActivity.name= "Event";
                        lastActivityDate = eventDate;
                    }
                } else {
                    lastActivityDate = eventDate;
                }
            }
            if (lastActivity.name) {
                lastActivity.date = formatDate(lastActivityDate);
                return lastActivity;
            } else {
                lastActivity.name = "Activity";
                lastActivity.date = "Not available";
                return lastActivity;
            }
        }
        
        function formatDate(date) {
            return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
        }

        function GetAccountContent(account){
            var lastActivity = getLastActivity(account);
            var lastOpportunity = getLastOpportunity(account);
            return '<div class="account-locator-detail' + ('{! $User.UITheme}' == 'Theme4t'? ' account-locator-detail-mobile' : '') + '">' +
                    '<div class="account-locator-section">' +
                        '<span>Account information</span>' +
                        '<div class="account-locator-section-table">' +
                            j$('.account-locator-map').sfMap('detailRow', {title:'Name', value: account.Name }) +
                            j$('.account-locator-map').sfMap('detailRow', {title:'Phone', value: account.Phone }) +
                            j$('.account-locator-map').sfMap(isLinkable(lastOpportunity), {title:'Last Opportunity', value: lastOpportunity.name, href: '/' + lastOpportunity.id}) +
                            j$('.account-locator-map').sfMap('detailRow', {title:'Opportunity created', value: lastOpportunity.date}) +
                            j$('.account-locator-map').sfMap('detailRow', {title:'Last ' + lastActivity.name, value: lastActivity.date}) +
                            //j$('.account-locator-map').sfMap('detailRowHref', {title:'Owner', value: account.Owner.Name, href: '/' + account.Owner.Id }) +  
                            //'<a href="/'+ account.Owner.Id +'">Owner</a>'+ 
                            '<div><div>Owner</div><div><a href="/'+ account.Owner.Id +'">'+account.Owner.Name+'</a></div></div>'+     
                        '</div>' +
                     '</div>' +
                      '<div class="account-locator-section">' +
                            '<span>Address information</span>' +
                            '<div class="account-locator-section-table">' +
                                j$('.account-locator-map').sfMap('detailAddress', {title:'Billing address', address: account.BillingAddress }) +
                                j$('.account-locator-map').sfMap('detailAddress', {title:'Shipping address', address: account.ShippingAddress }) +
                            '</div>' +
                        '</div>' +
                        //j$('.account-locator-map').sfMap('detailHref', { value: 'Open account', href: '/' + account.Id }) +
                        '<a href="/'+ account.Id +'">Open account</a>'+
                '</div>';
        }
                               
        function GetQueryCondition(searchedName, searchedCities,searchedZip, searchedStates){
            var conditions = [];
            if(searchedName){
                searchedName = searchedName.replace(/'/g, '\\\'');
                conditions.push("Name like '%" + searchedName + "%'");
            }
            if(searchedCities){
                conditions.push("BillingCity = '" + searchedCities + "'");
            }
            if(searchedZip){
                searchedZip = searchedZip.replace(/'/g, '\\\'');
                conditions.push("BillingPostalCode like '%" + searchedZip + "%'");
            }
            if(searchedStates){
                conditions.push("BillingState = '" + searchedStates + "'");
            }
            conditions.push('billinglatitude != null');
            conditions.push('billinglongitude != null');
            conditions.push('billingStreet != null');
            conditions.push('billingCity != null');
            conditions.push('billingCountry != null');
            return conditions.join(' AND ');
        }    
    
                   
        j$('.account-locator-map').sfMap({
             mapKey: 'AIzaSyB1XSDB1gSDxO08jUSgmUZcCmHR-wiz6v4', 
             mapCallbackFn: 'accountLocatorMapLoad',
             searchQuery: { additionalConditions: GetQueryCondition('', '','', '') },
             mainMarkerIcon: "{!URLFOR($Resource.googleMapsMarkers,'Marker/main-dot.png')}",
             aroundMarkerIcon: "{!URLFOR($Resource.googleMapsMarkers,'Marker/around-dot.png')}",
             highlightMarkerIcon: "{!URLFOR($Resource.googleMapsMarkers,'Marker/highlight-dot.png')}",
             markerClusterUrl: "{!URLFOR($Resource.markerClusterer,'markerclusterer.1.0.1.js')}",
             enableMarkerCluster: false,
             infoWindow: {
                enable: true,
                queryContent: "SELECT Id, Name, Owner.Id, Owner.Name, BillingAddress, ShippingAddress, " +
                 " (SELECT Id, Name, CreatedDate FROM Opportunities ORDER BY CreatedDate DESC LIMIT 1)," +
                 " (SELECT CreatedDate, Subject FROM Tasks ORDER BY CreatedDate DESC LIMIT 1)," +
                 " (SELECT CreatedDate, Subject FROM Events ORDER BY CreatedDate DESC LIMIT 1) FROM Account WHERE Id ='{id}'",
                content: function(account){ return GetAccountContent(account[0]); }
             }
        });
        function accountLocatorMapLoad(){
           j$('.account-locator-map').sfMap('load');
        }
        j$(document).ready(function() {
            function CreateAccList(account){
                var attrs = {
                    'href':'\\' + account.Id,
                    'target':'_blank'
                };
                //for saleforce1
                if('{! $User.UITheme}' == 'Theme4t'){
                    attrs.target = '_self';
                }
                j$('<a/>', attrs)
                .css({'display':'block'})
                .text(account.Name)
                .on('mouseenter', function(){
                    var sfId = j$(this).attr('href')
                    if(sfId){ sfId = sfId.replace('\\',''); }
                    j$('.account-locator-map').sfMap('setHighlight', [sfId]);
                })
                .on('mouseleave', function(){
                    var sfId = j$(this).attr('href')
                    if(sfId) { sfId = sfId.replace('\\',''); }
                    j$('.account-locator-map').sfMap('removeHighlight', [sfId]);
                })
                .appendTo('.account-locator-list > div');
            }
              
            function onSuccessSearch(result){
                j$('.account-locator-list > div').empty();
                if(result.records !== undefined){   
                    var records = result.getArray('records');
                    var markers = [];
                    for (var i=0; i < records.length; i++) {
                        markers.push({
                                lat: records[i].BillingLatitude,
                                lng: records[i].BillingLongitude,
                                title: records[i].Name,
                                sfId:records[i].Id
                            });
                        CreateAccList(records[i]);                       
                    }
                    j$('.account-locator-map').sfMap('load', markers);
                }
                else{
                    j$('<span/>').text('Accounts not found').css({'color':'red'}).appendTo('.account-locator-list > div');
                    j$('.account-locator-map').sfMap('clear');
                }
            }
                       
            function AppendLookUpListIem(ul, name){
                j$('<li/>').on('click', function(){ CloseLookUpList(j$(this)); }).appendTo(ul)
                    .append('<span id="listbox-option-unique-id-01" class="slds-media slds-listbox__option slds-listbox__option_entity slds-listbox__option_has-meta" role="option">' +
                                '<span class="slds-media__body">' +
                                    '<span class="slds-listbox__option-meta slds-listbox__option-meta_entity">' + name + '</span>' +
                                '</span>' +
                            '</span>');
            }
            
            function CloseLookUpList(elm){
                elm.parents('ul').removeClass('account-locator-lookup-visible');
                var listItemText = elm.find('.slds-listbox__option-meta_entity').text();
                j$('.account-locator-wrapper div[name="name"]').find('.slds-pill-container').removeClass('slds-hide');
                j$('.account-locator-wrapper div[name="name"]').find('input').addClass('slds-hide');
                j$('.account-locator-wrapper span[id$="namePill"]').text(listItemText);
                j$('.account-locator-wrapper input[id$="name"]').val(listItemText);
            }
            
            j$('#resetBtn, #m_resetBtn').click(function(){
                j$('.account-locator-list > div').empty();
                j$('.account-locator-wrapper input[id$="name"]').val('');
                j$('.account-locator-wrapper select[id$="cities"]').val('');
                j$('.account-locator-wrapper input[id$="zip"]').val('');
                j$('.account-locator-wrapper select[id$="states"]').val('');
                j$('.account-locator-map').sfMap('load', [{
                    lat: 40.0782400,
                    lng: -83.1299340,
                    title: 'Husky Marketing and Supply Company',
                    sfId: '0'
                }]);
            });
            j$('#searchBtn, #m_searchBtn').click(function(){
                var searchedName = j$('.account-locator-wrapper input[id$="name"]').val();
                var searchedCities = j$('.account-locator-wrapper select[id$="cities"]').val();
                var searchedZip = j$('.account-locator-wrapper input[id$="zip"]').val();
                var searchedStates = j$('.account-locator-wrapper select[id$="states"]').val();
                var query;
                query = 'select ID,Name,BillingLatitude,BillingLongitude from account where '
                    + GetQueryCondition(searchedName, searchedCities,searchedZip,searchedStates);
                var result = sforce.connection.query(query, {
                    onSuccess : function(result) { onSuccessSearch(result) },
                    onFailure : function(error) {}
                });
            });
            
            var evt;
            document.onmousemove = function (e) {
                e = e || window.event;
                evt = e;
            } 
            
            j$(document).delegate('input', 'keydown', function(e) {
                if(e.keyCode == 13){
                    e.preventDefault();
                }
            });
            
            j$('.account-locator-wrapper div[name$="name"]').on('focusout', function(event){
                var target = j$(evt.target)
                if(!(target.hasClass("slds-listbox__option-meta") && target.hasClass("slds-listbox__option-meta_entity"))){
                    j$(this).find('ul').removeClass('account-locator-lookup-visible');
                }
            });
            
            
            var nameInputValue = "";
            j$('.account-locator-wrapper input[id$="name"]').on('keyup', function(){
                var elm = j$(this);
                var value = elm.val();
                if(value != nameInputValue){
                    nameInputValue = value;
                    var ul =  elm.parent().find('ul');
                   
                    value = value.replace(/'/g, '\\\'');
                    var query = "Select Id,Name From Account WHERE Name like '%" + value + 
                        "%' AND billinglatitude != null AND  billinglatitude != null AND billingStreet != null AND billingCity != null AND billingCountry != null ORDER BY Name  LIMIT 5";
                    var result = sforce.connection.query(query, {
                        onSuccess : function(result) { 
                             ul.empty();
                            if(result.records){
                                if(result.records.length > 1){                            
                                    for(var i = 0; i < result.records.length; i++){                               
                                         AppendLookUpListIem(ul, result.records[i].Name);
    
                                    }
                                } else {
                                    AppendLookUpListIem(ul, result.records.Name);
                                }
                            }
                            else {
                                j$('<li/>').appendTo(ul)
                                    .append('<span id="listbox-option-unique-id-01" class="slds-media slds-listbox__option slds-listbox__option_entity slds-listbox__option_has-meta" role="option">' +
                                        '<span class="slds-media__body">' +
                                            '<span class="slds-listbox__option-meta slds-listbox__option-meta_entity">No result found</span>' +
                                        '</span>' +
                                    '</span>');
                            }
                            ul.addClass('account-locator-lookup-visible');
                        },
                        onFailure : function(error) { console.log(error);}
                    });
                }
            });
            
            j$('.account-locator-wrapper div[name$="name"] .slds-pill__remove').click(function(){
                j$('.account-locator-wrapper input[id$="name"]').val('');
                j$('.account-locator-wrapper div[name="name"]').find('.slds-pill-container').addClass('slds-hide');
                j$('.account-locator-wrapper div[name="name"]').find('input').removeClass('slds-hide');
            });
        });
    </script>
    </html>
</apex:page>