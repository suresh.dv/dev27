<apex:page showHeader="true" 
			sidebar="true" 
			standardController="FM_Load_Request__c" 
			extensions="FM_LoadRequestRecreateCtrl"
			docType="html-5.0">

	<apex:form id="loadRequestRecreateForm"
				html-novalidate="true">

        <apex:actionstatus id="ajaxStatus">
            <apex:facet name="start">

                <div class="waitingSearchDiv" id="el_loading" style="background-color: #DCD6D6; height: 100%;opacity:0.65;width:100%;">
                    <div class="waitingHolder" style="top: 150px; width: 200px; height: 40px; border:1px solid black;  background-color: white;">
                        <p>
                            <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                            <span class="waitingDescription">Please Wait...</span>
                        </p>
                    </div>
                </div>

            </apex:facet>
        </apex:actionstatus>

<!-- MAIN SECTION WITH TITLE -->
		<apex:pageBlock id="mainPageBlock" 
						title="Recreate Load Request">
<!-- ERROR MSGS -->
			<apex:pageMessages id="messages"/>
<!-- BUTTONS -->
			<apex:pageBlockButtons id="pageBlockButtons">
                <apex:commandButton value="Save" 
                					action="{!recreate}"
                					disabled="{!!oldLoadRequestValid}"/>
                <apex:commandButton value="Cancel" 
                					action="{!cancel}"
                					immediate="true"/>
            </apex:pageBlockButtons>
<!-- 2 COLUMN TABLE -->
            <apex:pageBlockSection id="mainPageBlockSection" 
            						columns="2"
            						rendered="{!oldLoadRequestValid}">
<!-- OLD LOAD REQUEST SECTION -->
            	<apex:pageBlockSectionItem id="oldLoadRequestSectionColumn">
            		<apex:pageBlock id="oldLoadRequestBlock"
            						title="Original Load Request">
            			<apex:pageBlockSection id="oldLoadRequestSection"
            									columns="1">
							<apex:outputField value="{!oldLoadRequest.Name}"/>
							<apex:outputField value="{!oldLoadRequest.Load_Type__c}"/>

					<!-- SHIFT -->
							<apex:pageBlockSectionItem >
								<apex:outputLabel for="oldShift" 
												value="{!$ObjectType.FM_Load_Request__c.fields.Shift__c.Label}"/>
								<apex:outputText id="oldShift"
												value="{!oldLoadRequest.Shift__c} - {!oldLoadRequest.Shift_Day__c}"/>
							</apex:pageBlockSectionItem>

							<apex:outputField value="{!oldLoadRequest.Product__c}"/>
							<apex:outputField value="{!oldLoadRequest.Source_Location__c}"/>
							<apex:outputField value="{!oldLoadRequest.Source_Facility__c}"/>
							<apex:outputField value="{!oldLoadRequest.Standing_Comments__c}"/>
							<apex:outputField value="{!oldLoadRequest.Run_Sheet_Lookup__c}"/>
					<!-- LOAD WEIGHT -->
							<apex:pageBlockSectionItem >
								<apex:outputLabel for="oldLoadWeight" 
												value="{!$ObjectType.FM_Load_Request__c.fields.Load_Weight__c.Label}"/>
								<apex:outputText id="oldLoadWeight" 
												value="{!oldLoadRequest.Load_Weight__c} - {!oldLoadRequest.Load_Weight_m3__c}(m3)"/>
							</apex:pageBlockSectionItem>

							<apex:outputField value="{!oldLoadRequest.Status__c}"/>
							<apex:outputField value="{!oldLoadRequest.Create_Reason__c}"/>
							<apex:outputField value="{!oldLoadRequest.Create_Comments__c}"/>
							<apex:outputField value="{!oldLoadRequest.Sour__c}"/>
							<apex:outputField value="{!oldLoadRequest.Tank_Label__c}"/>
							<apex:outputField value="{!oldLoadRequest.Act_Flow_Rate__c}"/>
							<apex:outputField value="{!oldLoadRequest.Flowline_Volume__c}"/>
							<apex:outputField value="{!oldLoadRequest.Act_Tank_Level__c}"/>
							<apex:outputField value="{!oldLoadRequest.Tank_Low_Level__c}"/>
							<apex:outputField value="{!oldLoadRequest.Tank_Size__c}"/>
							<apex:outputField value="{!oldLoadRequest.Axle__c}"/>
							<apex:outputField value="{!oldLoadRequest.Carrier__c}"/>

					<!-- CANCEL REASON -->
							<apex:pageBlockSectionItem >
								<apex:outputLabel for="oldLoadRequestCancelReason"
													value="{!$ObjectType.FM_Load_Request__c.fields.Cancel_Reason__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField id="oldLoadRequestCancelReason"
													value="{!oldLoadRequest.Cancel_Reason__c}">
										<apex:actionSupport event="onchange" 
															action="{!handleCancelReasonChange}" 
															reRender="oldLoadRequestBlock,messages"
															status="ajaxStatus"/>
									</apex:inputField>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

					<!-- CANCEL REASON COMMENTS -->
							<apex:pageBlockSectionItem rendered="{!isOldLoadRequestOtherCancelReason}">
								<apex:outputLabel for="oldLoadRequestCancelReasonComments"
													value="{!$ObjectType.FM_Load_Request__c.fields.Cancel_Comments__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField id="oldLoadRequestCancelReasonComments" 
													value="{!oldLoadRequest.Cancel_Comments__c}"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>	

						</apex:pageBlockSection>
	            	</apex:pageBlock>
	            </apex:pageBlockSectionItem>
<!-- NEW LOAD REQUEST SECTION -->
            	<apex:pageBlockSectionItem id="newLoadRequestSectionColumn">
            		<apex:pageBlock id="newLoadRequestBlock"
            						title="New Load Request">
            			<apex:pageBlockSection id="newLoadRequestSection"
            									columns="1">

					<!-- LOAD TYPE -->
            				<apex:inputField value="{!newLoadRequest.Load_Type__c}">
            					<apex:actionSupport event="onchange" 
            										reRender="newLoadRequestSection"
            										status="ajaxStatus"/>
            				</apex:inputField>

    				<!-- SHIFT -->
            				<apex:pageBlockSectionItem >
            					<apex:outputLabel value="{!$ObjectType.FM_Load_Request__c.fields.Shift__c.Label}"
        											for="newLoadRequestShift"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
		            				<apex:selectList id="newLoadRequestShift" 
		            								value="{!newLoadRequestShift}"
		            								size="1">
	    								<apex:selectOptions value="{!shiftOptions}"/>
	    							</apex:selectList>
	    						</apex:outputPanel>
	            			</apex:pageBlockSectionItem>

        			<!-- PRODUCT -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestProduct"
													value="{!$ObjectType.FM_Load_Request__c.fields.Product__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField id="newLoadRequestProduct"
													value="{!newLoadRequest.Product__c}"/>
    							</apex:outputPanel>
    						</apex:pageBlockSectionItem>

					<!-- SOURCE LOCATION -->
            				<apex:outputField value="{!newLoadRequest.Source_Location__c}"/>

					<!-- SOURCE FACILITY -->
            				<apex:outputField value="{!newLoadRequest.Source_Facility__c}"/>

					<!-- STANDING COMMENTS -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestComments"
													value="{!$ObjectType.FM_Load_Request__c.fields.Standing_Comments__c.Label}"/>
													<!-- TODO condition on requiredblock styleclass -->
								<apex:outputPanel styleClass="{!IF(isStandardLoad, '', 'requiredInput')}" layout="block" >
									<apex:outputPanel styleClass="{!IF(isStandardLoad, '', 'requiredBlock')}" layout="block"/>
									<apex:inputField id="newLoadRequestComments"
													value="{!newLoadRequest.Standing_Comments__c}"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

    				<!-- LOAD WEIGHT -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestLoadWeight"
													value="{!$ObjectType.FM_Load_Request__c.fields.Load_Weight__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField value="{!newLoadRequest.Load_Weight__c}"
        											id="newLoadRequestLoadWeight"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>
            				
            				<apex:outputField value="{!newLoadRequest.Status__c}"/>
            				<apex:outputField value="{!newLoadRequest.Sour__c}"/>

    				<!-- TANK -->
							<apex:pageBlockSectionItem > 
								<apex:outputLabel value="{!$ObjectType.FM_Load_Request__c.fields.Equipment_Tank__c.Label}"
													for="newLoadRequestTank"/>   
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>        								
									<apex:selectList size="1" 
													id="newLoadRequestTank"
													value="{!tank}">
										<apex:selectOption itemValue="" 
															itemLabel="--Standard Tanks--" 
															itemDisabled="true"/>
										<apex:selectOptions value="{!tankOptions}" 
															rendered="{!tankOptions != null && tankOptions.size > 0}"/>
										<apex:selectOption itemValue="" 
															itemLabel="--Extra Tanks--" 
															itemDisabled="true"/>
										<apex:selectOptions value="{!extraTankOptions}"/>
										<apex:actionSupport action="{!updateLoadRequestTankInfo}" 
															event="onchange" 
															reRender="newLoadRequestBlock,messages" 
															status="ajaxStatus"/>
									</apex:selectList>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

					<!-- FLOW RATE -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestFlowRate"
													value="{!$ObjectType.FM_Load_Request__c.fields.Act_Flow_Rate__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField value="{!newLoadRequest.Act_Flow_Rate__c}"
            										id="newLoadRequestFlowRate"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>
            				
    				<!-- FLOWLINE VOLUME -->
            				<apex:inputField value="{!newLoadRequest.Flowline_Volume__c}"/>

    				<!-- TANK LEVEL -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestTankLevel"
													value="{!$ObjectType.FM_Load_Request__c.fields.Act_Tank_Level__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField value="{!newLoadRequest.Act_Tank_Level__c}"
            										id="newLoadRequestTankLevel"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

    				<!-- TANK LOW LEVEL -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestLowLevel"
													value="{!$ObjectType.FM_Load_Request__c.fields.Tank_Low_Level__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField value="{!newLoadRequest.Tank_Low_Level__c}"
            										id="newLoadRequestLowLevel"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

    				<!-- TANK SIZE -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestTankSize"
													value="{!$ObjectType.FM_Load_Request__c.fields.Tank_Size__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField value="{!newLoadRequest.Tank_Size__c}"
            										id="newLoadRequestTankSize"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

    				<!-- AXLE -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestAxle"
													value="{!$ObjectType.FM_Load_Request__c.fields.Axle__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField value="{!newLoadRequest.Axle__c}"
            										id="newLoadRequestAxle"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

    				<!-- CARRIER -->
            				<apex:pageBlockSectionItem >
								<apex:outputLabel for="newLoadRequestCarrier"
													value="{!$ObjectType.FM_Load_Request__c.fields.Carrier__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:inputField value="{!newLoadRequest.Carrier__c}"
            										id="newLoadRequestCarrier"/>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

					<!-- CREATE REASON -->
							<apex:pageBlockSectionItem >
								<apex:outputLabel value="{!$ObjectType.FM_Load_Request__c.fields.Create_Reason__c.Label}"
												for="newCreateReason"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
									<apex:selectList id="newCreateReason"
													value="{!newLoadRequest.Create_Reason__c}"
													size="1">
										<apex:selectOptions value="{!createReasonOptions}"/>
										<apex:actionSupport event="onchange" 
															action="{!handleCreateReasonChange}" 
															reRender="newLoadRequestBlock,messages"
															status="ajaxStatus"/>
									</apex:selectList>
								</apex:outputPanel>
							</apex:pageBlockSectionItem>

					<!-- CREATE REASON COMMENTS -->
							<apex:pageBlockSectionItem rendered="{!isNewLoadRequestOtherCreateReason}">
								<apex:outputLabel for="newLoadRequestCreateComments"
													value="{!$ObjectType.FM_Load_Request__c.fields.Create_Comments__c.Label}"/>
								<apex:outputPanel styleClass="requiredInput" layout="block" >
									<apex:outputPanel styleClass="requiredBlock" layout="block"/>
		            				<apex:inputField id="newLoadRequestCreateComments"
		            								value="{!newLoadRequest.Create_Comments__c}"/>
		            			</apex:outputPanel>
		            		</apex:pageBlockSectionItem>
            			</apex:pageBlockSection>
	            	</apex:pageBlock>
	            </apex:pageBlockSectionItem>
        	</apex:pageBlockSection>

		</apex:pageBlock>

	</apex:form>

</apex:page>