<apex:page id="vfpage" standardcontroller="FM_Run_Sheet__c" extensions="FM_RunSheetControllerX" docType="html-5.0" cache="true">

	<style type="text/css">
		input[type="number"]
		{
			width: 80px;
		}

		html,body
		{
			margin: 0;
			padding: 0;
			height: 100%;
		}

		.ct
		{
			text-align: center;
		}

		.label
		{
			padding-right: 20px;
			text-align: right;
			font-size: 91%;
			font-weight: bold;
			width: 15%;
		}

		.header
		{
			font-weight: bold;
			color: darkgoldenrod;
		}

		.custPopup
		{
			background-color: #26ade4;
			border-width: 1px;
			border-style: solid;
			z-index: 9999;
			left: 80%;
			padding: 5px;
			position: absolute;

			/* These are the 3 css properties you will need to change so the popup
			displays in the center of the screen. First set the width. Then set
			margin-left to negative half of what the width is. You can add
			the height property for a fixed size pop up if you want.*/

			width: 450px;
			margin-left: -225px;
			top: 90px;
		}

		.tg {border-collapse: collapse; border-spacing: 0; border-color: #999;}
		.tg td {font-family: Arial, sans-serif; font-size: 11px; padding: 5px 5px; border-style: solid; border-width: 0px; overflow: hidden; word-break: normal; border-color: #999; color: #444; background-color: #f7fdfa; border-top-width: 1px; border-bottom-width: 1px;}
		.tg th {font-family: Arial, sans-serif; font-size: 11px; font-weight: normal; padding: 5px 5px; border-style: solid; border-width: 0px; overflow: hidden; word-break: normal; border-color: #999; color: #fff; background-color: #26ade4; border-top-width: 1px; border-bottom-width: 1px;}
		.tg .tg-hgcj {font-weight: bold; text-align:center}
		.tg .tg-s6z2 {text-align: center}
	</style>

	<script>
		if({!bIsViewMode} == false || {!RunSheetIsEditable} == false)
		{
			window.location.href = '/{!oRunSheet.Id}';
		}
	</script>

	<apex:pageMessages id="messages" escape="false" />

	<apex:form id="rsform">

		<apex:actionstatus id="ajaxStatus">
			<apex:facet name="start">

				<div class="waitingSearchDiv" id="el_loading" style="background-color: #dcd6d6; height: 100%; opacity: 0.65; width: 100%;">
					<div class="waitingHolder" style="top: 150px; width: 200px; height: 40px; border:1px solid black;  background-color: white;">
						<p>
							<img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
							<span class="waitingDescription">Please Wait...</span>
						</p>
					</div>
				</div>

			</apex:facet>
		</apex:actionstatus>

		<apex:pageBlock id="mainpb" title="Edit Run Sheet - {!oRunSheet.Date__c}" mode="detail">
			<apex:pageBlockButtons id="pbButtons" location="bottom">
				<apex:actionstatus id="buttonstatus">

					<apex:facet name="stop">
						<apex:outputPanel >
							<apex:commandButton id="SaveButton" rerender="pbButtons, messages" status="buttonstatus" action="{!SaveRunsheet}" value="Update Run Sheet" onclick="if(!confirm('Update this Run Sheet?')){return false};" />
							<apex:commandButton id="CancelButton" action="{!GetRedirectLocation}" value="Cancel" immediate="true" html-formnovalidate="formnovalidate" />
						</apex:outputPanel>
					</apex:facet>

					<apex:facet name="start">
						<apex:outputPanel >
							<apex:commandButton id="SaveButtonDisabled" disabled="true" value="Update Run Sheet" rendered="true" />
							<apex:commandButton id="CancelButtonDisabled" disabled="true" value="Cancel" rendered="true" />
						</apex:outputPanel>
					</apex:facet>

				</apex:actionstatus>
			</apex:pageBlockButtons>

			<div id="InternalDiv" style="min-height: 200px;">

				<!-- WELL LOCATION HEADER DISPLAY -->
				<apex:pageBlockSection id="wellsiteinfo" columns="1" title="Well Location" collapsible="false" rendered="{!oRunSheet.Is_Fluid_Facility__c == false}">

					<apex:pageBlockSectionItem >
						<apex:outputlabel >Operating Route</apex:outputlabel>
						<apex:outputField value="{!oRunsheet.Well__r.Route__c}" />
					</apex:pageBlockSectionItem>

					<apex:pageBlockSectionItem >
						<apex:outputlabel >Location</apex:outputlabel>
						<apex:outputField value="{!oRunsheet.Well__c}" />
					</apex:pageBlockSectionItem>

					<apex:pageBlockSectionItem >
						<apex:outputlabel >Operator on Call</apex:outputlabel>

						<apex:panelGrid columns="3">
							<apex:outputText value="{!oOperatorOnCall.Operator__r.Name}" style="color: #0532f3; font-weight: bold;" />
							<apex:outputText value=" is on call since " />
							<apex:outputText value="{!sOperatorOnCallFormattedDate}" style="font-weight: bold;" />
						</apex:panelGrid>
					</apex:pageBlockSectionItem>

					<apex:pageBlockSectionItem >
						<apex:outputlabel >Field Senior</apex:outputlabel>
						<apex:outputField value="{!oRunsheet.Well__r.Route__r.Field_Senior__c}" />
					</apex:pageBlockSectionItem>

				</apex:pageBlockSection>

				<!-- FLUID FACILITY HEADER DISPLAY -->
				<apex:pageBlockSection id="facinfo" columns="1" title="Facility" collapsible="false" rendered="{!oRunSheet.Is_Fluid_Facility__c == true}">

					<apex:pageBlockSectionItem >
						<apex:outputlabel >Facility</apex:outputlabel>
						<apex:outputField value="{!oRunsheet.Well_Name_Tank__c}" />
					</apex:pageBlockSectionItem>

				</apex:pageBlockSection>

				<apex:outputPanel id="wellinfo" style="width: 100%;" layout="block">
					<apex:actionRegion >

<!-- WELL LAYOUT -->

						<apex:pageBlockSection title="Well Data" columns="2" collapsible="false" rendered="{!oRunSheet.Is_Fluid_Facility__c == false}">
							<apex:pageBlockSectionItem >
								<apex:outputlabel >Current Well Status</apex:outputlabel>

								<apex:panelGrid columns="2">
									<apex:outputField value="{!oRunSheet.Well__r.Current_Well_Status__c}" label="Current Well Status" />
									<apex:outputField value="{!oRunSheet.Well__r.Statusicon__c}" label="" />
								</apex:panelGrid>
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem ></apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputlabel >Well Type</apex:outputlabel>
								<apex:outputField value="{!oRunSheet.Well__r.Well_Type__c}" />
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputlabel >Previous Reading</apex:outputlabel>

								<apex:panelGrid columns="2">
									<apex:outputText value="{!oPreviousReading.H2S_Reading__c}" style="color: #0532f3; font-weight: bold;" />
									<apex:outputText value=" on {!sFormattedReadingTime}" rendered="{!oPreviousReading.Value__c != null}" />
									<apex:outputText value="NA" rendered="{!oPreviousReading.Value__c == null}" />
								</apex:panelGrid>
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputlabel >Unit Configuration</apex:outputlabel>
								<apex:outputField value="{!oRunSheet.Well__r.Unit_Configuration__c}" />
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputLabel >Well Flow Rate (m3)</apex:outputlabel>

								<apex:outputpanel layout="block" styleClass="{!IF(oSelectedLocation.Well_Type__c != 'GAS', 'requiredInput', '')}">
									<apex:outputpanel layout="block" styleClass="{!IF(oSelectedLocation.Well_Type__c != 'GAS', 'requiredBlock', '')}" />
									<apex:inputField id="Act_Flow_Rate" value="{!oRunSheet.Act_Flow_Rate__c}" type="number" required="true" label="Well Flow Rate (m3)" html-min="0" html-max="500" html-step="1" html-pattern="\d+" html-maxlength="3" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}">
										<apex:actionSupport event="onchange"  status="ajaxStatus" action="{!RefreshTankFlowRates}" reRender="wellinfo,messages" />
									</apex:inputField>
								</apex:outputpanel>
							</apex:pageBlockSectionItem>

							<apex:inputField value="{!oRunsheet.Sour__c}" label="H2S" rendered="{!NOT(bFLOCH2S)}" />
							<apex:outputField value="{!oRunsheet.Sour__c}" label="H2S" rendered="{!bFLOCH2S}" />

							<!-- RM/County -->
							<apex:selectList value="{!oRunSheet.RM_County__c}" size="1" label="RM/County">
								<!--<apex:selectOption itemValue="" itemLabel="--None--"/>-->
                    			<apex:selectOptions value="{!pickvalsRM}"/>
               			    </apex:selectList>

							<apex:selectList value="{!oRunsheet.Load_Weight__c}" size="1" label="Load Weight" required="true">
								<apex:SelectOptions value="{!loadWeightOptions}"/>
							</apex:selectList>

						   <apex:pageBlockSectionItem >
								<apex:outputlabel >Flowline Volume</apex:outputlabel>
								<apex:inputField value="{!oRunsheet.Flowline_Volume__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="500" html-step="1" html-maxlength="3" html-title="Must be between 0 and 500" />
							</apex:pageBlockSectionItem>

						    <apex:pageBlockSectionItem >
								<apex:outputlabel >Comments Category</apex:outputlabel>

								<apex:actionRegion >
									<apex:inputField id="CommentsCategory" value="{!oRunsheet.Comments_Category__c}" />
								</apex:actionRegion>
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputlabel >Avg Oil Prod (m3)</apex:outputlabel>

								<apex:actionRegion >
									<apex:outputText value="{!oRunsheet.Well__r.PVR_AVGVOL_30D_OIL__c}" />
								</apex:actionRegion>
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
							<!--BLANK SPACE before "Comments" field to keep formatting of Avg's fields-->
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputlabel >Avg Sand Prod (m3)</apex:outputlabel>
								<apex:outputText value="{!oRunsheet.Well__r.PVR_AVGVOL_30D_SAND__c}" />
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputlabel >Comments</apex:outputlabel>

								<apex:actionRegion >
									<apex:inputField id="StandingComments" value="{!oRunsheet.Standing_Comments__c}" style="width: 300px;" />
								</apex:actionRegion>
							</apex:pageBlockSectionItem>

							<apex:pageBlockSectionItem >
								<apex:outputlabel >Avg Water Prod (m3)</apex:outputlabel>

								<apex:actionRegion >
									<apex:outputText value="{!oRunsheet.Well__r.PVR_AVGVOL_30D_WATER__c}" />
								</apex:actionRegion>
							</apex:pageBlockSectionItem>
						</apex:pageBlockSection>

<!-- FACILITY LAYOUT -->

						<apex:pageBlockSection title="Facility Data" columns="1" collapsible="false" rendered="{!oRunSheet.Is_Fluid_Facility__c == true}">
							<apex:pageBlockSectionItem >
								<apex:outputlabel >Unit Configuration</apex:outputlabel>
								T6X
							</apex:pageBlockSectionItem>

							<apex:actionRegion >
								<apex:pageBlockSection id="commentdata" columns="1">
									<apex:inputField id="CommentsCategoryFacility" value="{!oRunsheet.Comments_Category__c}" html-disabled="disabled" />
									<apex:inputField id="StandingComments" value="{!oRunsheet.Standing_Comments__c}" style="width: 300px;" />
								</apex:pageBlockSection>
							</apex:actionRegion>

							<apex:inputField value="{!oRunsheet.Sour__c}" label="H2S" />

							<apex:selectList value="{!oRunsheet.Load_Weight__c}" size="1" label="Load Weight" required="true">
								<apex:SelectOptions value="{!loadWeightOptions}"/>
							</apex:selectList>
						</apex:pageBlockSection>

<!-- TANK LAYOUT (WELLS) -->

						<apex:pageBlockSection columns="1" title="Tank Information" collapsible="false" rendered="{!oRunsheet.Is_Fluid_Facility__c == false && WrapperListSize > 0}">
							<apex:pageBlockTable value="{!lWrapperList}" var="rs" id="TankTable" style="width: 100%;">

								<!-- TANK -->
								<apex:column headerValue="Tank">
									<apex:facet name="header">
										Tank
									</apex:facet>

									<apex:outputLink value="/{!rs.oTodaysRunsheet.Equipment_Tank__c}" target="_blank" onclick="tempDisableBeforeUnload = true;">{!rs.oTodaysRunsheet.Tank_Label__c}</apex:outputLink>
								</apex:column>

								<!-- LOAD TYPE -->
								<apex:column headerValue="Load Type" rendered="{!bCanViewServiceWork == true}">
									<apex:facet name="header">
										Load Type
									</apex:facet>

									<apex:actionRegion rendered="{!rs.TankIsEditable}">
										<apex:selectList id="TankLoadType" value="{!rs.oTodaysRunsheet.Load_Type__c}" size="1" label="Load Type" disabled="{!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}">
											<apex:selectOption itemValue="" itemLabel="-- Standard Load --" />
											<apex:selectOptions value="{!rs.lServiceTypes}" />
											<apex:actionSupport event="onchange" action="{!RefreshServiceRow}" rerender="TankTable, CommentsCategoryFacility, CommentsCategory" status="ajaxStatus" />
										</apex:selectList>
									</apex:actionRegion>

									<apex:outputField value="{!rs.oTodaysRunsheet.Load_Type__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>

								<!-- TANK LEVEL -->
								<apex:column >
									<apex:facet name="header">
										Tank Level (m3)
									</apex:facet>

									<apex:outputpanel layout="block" styleClass="{!IF(rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off', '', 'requiredInput')}" rendered="{!rs.TankIsEditable}">
										<apex:outputpanel layout="block" styleClass="{!IF(rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off', '', 'requiredBlock')}" />
										<apex:inputField id="tank_level_m3" value="{!rs.oTodaysRunsheet.Act_Tank_Level__c}" type="number" style="width: 60px;" required="{!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c != 'Off'}" html-min="0" html-max="500" html-step="1" html-maxlength="3" html-title="Must be between 0 and 500" />
										<script>document.getElementById('{!$Component.tank_level_m3}').disabled = {!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}; </script>
									</apex:outputpanel>

									<apex:outputField value="{!rs.oTodaysRunsheet.Act_Tank_Level__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Flow Rate (m3/day)
									</apex:facet>

									<!--<apex:outputField value="{!rs.oTank.Flow_Rate__c}" />-->
									<apex:outputText value="{!iTankFlowRate}"/>
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tonight (Oil)
									</apex:facet>

									<apex:inputField id="tonight_oil" value="{!rs.oTodaysRunsheet.Tonight_Oil__c}" type="number" label="Oil:" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="10" html-step="1" html-maxlength="2" html-title="Tonight Oil. Must be between 0 and 10" rendered="{!rs.TankIsEditable}" />
									<script>document.getElementById('{!$Component.tonight_oil}').disabled = {!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}; </script>

									<apex:outputField value="{!rs.oTodaysRunsheet.Tonight_Oil__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tonight (Water)
									</apex:facet>

									<apex:inputField id="tonight_water" value="{!rs.oTodaysRunsheet.Tonight_Water__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="10" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" rendered="{!rs.TankIsEditable}" />
									<script>document.getElementById('{!$Component.tonight_water}').disabled = {!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}; </script>

									<apex:outputField value="{!rs.oTodaysRunsheet.Tonight_Water__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tomorrow (Oil)
									</apex:facet>

									<apex:inputField id="tomorrow_oil" value="{!rs.oTodaysRunsheet.Tomorrow_Oil__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="10" html-step="1" html-maxlength="2" html-title="Tomorrow Oil. Must be between 0 and 10" rendered="{!rs.TankIsEditable}" />
									<script>document.getElementById('{!$Component.tomorrow_oil}').disabled = {!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}; </script>

									<apex:outputField value="{!rs.oTodaysRunsheet.Tomorrow_Oil__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tomorrow (Water)
									</apex:facet>

									<apex:inputField id="tomorrow_water" value="{!rs.oTodaysRunsheet.Tomorrow_Water__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="10" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" rendered="{!rs.TankIsEditable}" />
									<script>document.getElementById('{!$Component.tomorrow_water}').disabled = {!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}; </script>

									<apex:outputField value="{!rs.oTodaysRunsheet.Tomorrow_Water__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Outstanding (Oil)
									</apex:facet>

									<apex:inputField id="outstanding_oil" value="{!rs.oTodaysRunsheet.Outstanding_Oil__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="10" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" rendered="{!rs.TankIsEditable}" />
									<script>document.getElementById('{!$Component.outstanding_oil}').disabled = {!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}; </script>

									<apex:outputField value="{!rs.oTodaysRunsheet.Outstanding_Oil__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Outstanding (Water)
									</apex:facet>

									<apex:inputField id="outstanding_water" value="{!rs.oTodaysRunsheet.Outstanding_Water__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="10" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" rendered="{!rs.TankIsEditable}" />
									<script>document.getElementById('{!$Component.outstanding_water}').disabled = {!rs.oTodaysRunsheet.Equipment_Tank__r.Door_Status__c == 'Off'}; </script>

									<apex:outputField value="{!rs.oTodaysRunsheet.Outstanding_Water__c}" rendered="{!!rs.TankIsEditable}" />
								</apex:column>
							</apex:pageBlockTable>
						</apex:pageBlockSection>

<!-- TANK LAYOUT (FACILITIES) -->

						<apex:pageBlockSection columns="1" title="Facility Tank Information" collapsible="false" rendered="{!oRunsheet.Is_Fluid_Facility__c == true}">
							<apex:pageBlockTable value="{!lWrapperList}" var="rs" id="TankTable" style="width: 100%;">

								<!-- TANK -->
								<apex:column headerValue="Tank">
									<apex:facet name="header">
										Tank
									</apex:facet>

									Tank 1
								</apex:column>

								<!-- LOAD TYPE -->
								<apex:column headerValue="Load Type">
									<apex:facet name="header">
										Load Type
									</apex:facet>

									<apex:actionRegion >
										<apex:outputpanel id="panelLoadType" layout="block" styleClass="requiredInput">
											<apex:outputpanel layout="block" styleClass="requiredBlock" />

											<apex:selectList id="TankLoadType" value="{!rs.oTodaysRunsheet.Load_Type__c}" size="1" label="Load Type" required="true">
												<apex:selectOption itemValue="" itemLabel="-- Standard Load --" />
												<apex:selectOptions value="{!rs.lServiceTypes}" />
											</apex:selectList>

										</apex:outputpanel>
									</apex:actionRegion>
								</apex:column>

								<!-- TANK LEVEL -->
								<apex:column >
									<apex:facet name="header">
										Tank Level (m3)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Act_Tank_Level__c}" type="number" style="width: 60px;" html-min="0" html-max="500" html-step="1" html-maxlength="3" html-title="Must be between 0 and 500" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tank Size (m3)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Tank_Size__c}" type="number" style="width: 60px;" html-min="0" html-max="500" html-step="1" html-maxlength="3" html-title="Must be between 0 and 500" rendered="{!rs.oTank.Tank_Size_m3__c == null}" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tonight (Oil)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Tonight_Oil__c}" type="number" label="Oil:" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="100" html-step="1" html-maxlength="2" html-title="Tonight Oil. Must be between 0 and 10" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tonight (Water)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Tonight_Water__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="100" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tomorrow (Oil)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Tomorrow_Oil__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="100" html-step="1" html-maxlength="2" html-title="Tomorrow Oil. Must be between 0 and 10" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Tomorrow (Water)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Tomorrow_Water__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="100" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Outstanding (Oil)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Outstanding_Oil__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="100" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" />
								</apex:column>

								<apex:column >
									<apex:facet name="header">
										Outstanding (Water)
									</apex:facet>

									<apex:inputField value="{!rs.oTodaysRunsheet.Outstanding_Water__c}" type="number" onkeydown="if(event.keyCode==13){this.blur();actionFunction();}" html-min="0" html-max="100" html-step="1" html-maxlength="2" html-title="Must be between 0 and 10" />
								</apex:column>
							</apex:pageBlockTable>

						</apex:pageBlockSection>
					</apex:actionRegion>

				</apex:outputPanel>
			</div>

		</apex:pageBlock>

	</apex:form>
</apex:page>