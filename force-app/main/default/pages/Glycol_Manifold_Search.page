<apex:page controller="GlycolManifoldSearchCtrl" sidebar="true">

<style type="text/css">
.col1 {width: 50%;}
.col2 {width: 4%;}
.col3 {width: 46%;}

.headerRow {
       line-height: 20px;
       background-color: #41aacb;
    }   
.headerCell {
        color: #878787;
        font-weight: bold;
        font-size: small;
        padding-left: 10px;
        
    }
    
 .dataRow {
            line-height: 25px;
        }
  .dataCell {
        font-weight: normal;
        font-size: small;
        color: #878787;
        text-align:left;
        padding-left: 30px;
        padding-right: 5px;
        vertical-align:top;
        word-wrap: break-word;
    }
    
    .trblBorder {
        border-top: 0.03cm solid #d7d7d7;
        border-right: 0.03cm solid #d7d7d7;
        border-bottom: 0.03cm solid #d7d7d7;
        border-left: 0.03cm solid #d7d7d7;
    }
    
    .blBorder {
       
        border-bottom: 0.03cm solid #e3deb8 !important;
        border-left: 0.03cm solid #d7d7d7;
    }
    
    .rbBorder {
        border-right: 0.03cm solid #d7d7d7;
        border-bottom: 0.03cm solid #e3deb8 !important;
    }
    
    .bBorder {
        border-bottom: 0.03cm solid #e3deb8 !important;
    }   
 
</style> 
 
    <apex:sectionHeader title="Glycol Manifold Search" />  
    <apex:form >
 
        <apex:pageMessages />
        <apex:pageBlock >
            <apex:pageBlockSection title="Search for a Glycol Manifold" columns="1">
                <apex:panelGrid columns="3" width="100%" columnClasses="col1,col2,col3">
                    <apex:panelGroup >
                        <apex:outputText value="Drill down through Plant -> Unit hierarchy" style="font-weight:bold" />
                        <apex:actionRegion >
                            <apex:panelGrid columns="4" id="searchByUnit">
                                <apex:outputText value="Plant" />
                                <apex:selectlist value="{!selectedPlant}" size="1" id="cboPlant" title="Choose a Plant">
                                    <apex:selectOptions value="{!plantItems}"/>                        
                                    <apex:actionSupport event="onchange"  status="searchStatus"
                                        rerender="cboUnit, id_entry"
                                        action="{!RefreshUnitList}"/>
                                </apex:selectlist>  
                                <apex:outputText value="Manifold Type"/>
                                <apex:selectList value="{!selectedMType}" size="1" >
                                    <apex:selectOptions value="{!manifoldTypes}"/>
                                </apex:selectList>
                                <apex:outputText value="Unit" />
                                <apex:selectlist value="{!selectedUnit}" size="1" title="Choose Unit" id="cboUnit">
                                    <apex:selectOptions value="{!unitItems}"/>
                                </apex:selectlist>  
                                <apex:commandButton value="Search" action="{!SearchByUnit}" status="searchStatus"
                                    reRender="searchResultsPanel, id_entry" />
                            </apex:panelGrid>
                        </apex:actionRegion>  
                    </apex:panelGroup>
                    <apex:panelGroup >
                        &nbsp;&nbsp;<apex:outputText value="|" style="font-weight:bold;"/><br/>    
                        &nbsp;&nbsp;<apex:outputText value="|" style="font-weight:bold;"/><br/>    
                        <apex:outputText value="OR" style="font-weight:bold;"/><br/>    
                        &nbsp;&nbsp;<apex:outputText value="|" style="font-weight:bold;"/><br/>    
                        &nbsp;&nbsp;<apex:outputText value="|" style="font-weight:bold;"/><br/>    
                    </apex:panelGroup>
                    <apex:panelGroup >
                        <apex:outputText value="Enter search text for filtering" style="font-weight:bold" />
                        <br/>
                        <br/>
                        <apex:panelGrid columns="4" style="line-height:2.2em" >
                            <apex:actionRegion >
                                <apex:outputLabel value="Search for Glycol Manifold" for="id_entry" />
                                <apex:selectList value="{!searchOption}" size="1">
                                    
                                    <apex:selectOption itemValue="Equip" itemLabel="Reference Line Number" />
                                    <apex:selectOption itemValue="Supply" itemLabel="Supply Manifold" />
                                    <apex:selectOption itemValue="Return" itemLabel="Return Manifold" /> 
                                </apex:selectList>
                
                                <apex:inputText value="{!searchString}" id="id_entry" />
            
                                <apex:commandButton action="{!SearchByText}" value="Search" status="searchStatus"
                                    reRender="searchResultsPanel,searchByUnit">
                                </apex:commandButton>
                            </apex:actionRegion>
                        </apex:PanelGrid>
                        
                        
                    </apex:panelGroup>
                   
                </apex:panelGrid>
                
            </apex:pageBlockSection>
            <div style="width:100%; text-align:center">
                 <!-- ############################################################## -->
                   <!-- Message shown to indicate search or update is in progress -->
                   <!-- Search Status -->
                <apex:actionStatus startText="Searching ..." id="searchStatus" startStyle="background-color:yellow"/>
            </div>
            <apex:outputPanel id="searchResultsPanel">
                <apex:pageBlockSection title="Results" columns="1"  rendered="{!searchRequested}"> 
                    <apex:facet name="header">
                       <span style="font-size:1.1em">Results:  {!displayRecordCountInfo}</span>
                    </apex:facet>
                    <apex:outputPanel >
                    <apex:commandButton value="Export Excel" action="{!exportDetail}" rendered="{!searchResults.size > 0}"/> 
                    <apex:commandButton value="New Glycol Manifold Branch" action="{!GoToNewManifold}" rendered="{!isUserHasAdminPermission}"/>
                    </apex:outputPanel>  

                    <apex:outputText value="No records to display" rendered="{!searchResults.size == 0}" />
                     
                    <!-- Result Table: the order of column depend on selected Manifold Type in search criteria 
                    If Manifold Type is Return, the order is: Return Manifold, Return Branch, Supply Manifold, Supply Branch 
                    Else If Manifold Type is Supply, the order is: Supply Manifold, Supply Branch, Return Manifold, Return Branch
                    User can edit Glycol Branch and associated Equipment by clicking Edit link in the Action column
                    -->
                    <apex:outputPanel id="searchResultSubPanel" rendered="{!searchResults.size > 0}">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                        <tr class="headerRow">
                             <apex:outputPanel layout="none" rendered="{!isUserHasAdminPermission}">
                            <td class="headerCell trblBorder" width="5%">Action</td>
                            </apex:outputPanel>
                            <td class="headerCell trblBorder" width="20%">
                               <apex:commandLink value="Return Manifold" rerender="searchResultSubPanel" style="text-decoration: none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Return' && searchOption == ''), searchOption == 'Return')}">
                                  <apex:image value="/img/sort_asc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Manifold__r.Name' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Manifold__r.Name' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Return_Manifold__r.Name" assignTo="{!sortField}"/>
                               </apex:commandLink>
                               <apex:commandLink value="Supply Manifold" rerender="searchResultSubPanel" style="text-decoration: none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Supply' && searchOption == ''), searchOption == 'Supply' || searchOption == 'Equip')}">
                                  <apex:image value="/img/sort_asc_arrow.gif" 
                                                rendered="{!sortField == 'Supply_Manifold__r.Name' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                                rendered="{!sortField == 'Supply_Manifold__r.Name' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Supply_Manifold__r.Name" assignTo="{!sortField}"/>
                               </apex:commandLink>
                            </td>
                            <td class="headerCell trblBorder" width="11%">
                              <apex:commandLink value="Return Branch" rerender="searchResultSubPanel"  style="text-decoration: none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Return' &&  searchOption == ''), searchOption == 'Return')}">
                                  <apex:image value="/img/sort_asc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Branch_Num__c' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Branch_Num__c' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Return_Branch_Num__c" assignTo="{!sortField}"/>
                               </apex:commandLink>
                               <apex:commandLink value="Supply Branch" rerender="searchResultSubPanel" style="text-decoration: none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Supply' &&  searchOption == ''), searchOption == 'Supply' || searchOption == 'Equip')}">
                                  <apex:image value="/img/sort_asc_arrow.gif" 
                                                rendered="{!sortField == 'Supply_Branch_Num__c' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                                rendered="{!sortField == 'Supply_Branch_Num__c' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Supply_Branch_Num__c" assignTo="{!sortField}"/>
                               </apex:commandLink>
                            </td>
                            <td class="headerCell trblBorder" width="20%">
                                
                               <apex:commandLink value="Return Manifold" rerender="searchResultSubPanel"  style="text-decoration: none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Supply' && searchOption == ''), searchOption == 'Supply' || searchOption == 'Equip')}">
                                  <apex:image value="/img/sort_asc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Manifold__r.Name' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Manifold__r.Name' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Return_Manifold__r.Name" assignTo="{!sortField}"/>
                               </apex:commandLink>
                               <apex:commandLink value="Supply Manifold" rerender="searchResultSubPanel" style="text-decoration: none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Return' && searchOption == ''), searchOption == 'Return')}">
                                  <apex:image value="/img/sort_asc_arrow.gif" 
                                                rendered="{!sortField == 'Supply_Manifold__r.Name' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                                rendered="{!sortField == 'Supply_Manifold__r.Name' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Supply_Manifold__r.Name" assignTo="{!sortField}"/>
                               </apex:commandLink>
                            </td>
                            <td class="headerCell trblBorder" width="11%">
                               <apex:commandLink value="Return Branch" rerender="searchResultSubPanel" style="text-decoration:none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Supply' && searchOption == ''), searchOption == 'Supply' || searchOption == 'Equip')}">
                                  <apex:image value="/img/sort_asc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Branch_Num__c' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                              rendered="{!sortField == 'Return_Branch_Num__c' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Return_Branch_Num__c" assignTo="{!sortField}"/> 
                               </apex:commandLink>
                               <apex:commandLink value="Supply Branch" rerender="searchResultSubPanel" style="text-decoration: none;"
                                         action="{!SearchManifold}" rendered="{!OR((selectedMType == 'Return' && searchOption == ''), searchOption == 'Return')}">
                                  <apex:image value="/img/sort_asc_arrow.gif"  
                                                rendered="{!sortField == 'Supply_Branch_Num__c' && SortDir == 'asc'}"/>
                                  <apex:image value="/img/sort_desc_arrow.gif" 
                                                rendered="{!sortField == 'Supply_Branch_Num__c' && SortDir == 'desc'}"/>
                                  <apex:param name="order" value="{!IF(SortDir == 'asc','desc', 'asc')}" assignTo="{!sortDir}"/>
                                  <apex:param name="columnName" value="Supply_Branch_Num__c" assignTo="{!sortField}"/>
                               </apex:commandLink>
                            </td>
                            <td class="headerCell trblBorder" width="20%"><apex:outputText value="Reference Line Number"/></td>
                            <td class="headerCell trblBorder" width="10%"><apex:outputText value="Line Size (inches)"/></td>
                        </tr> 
                     </table>
                     
                     <div style="overflow:auto;width:100%;max-height:390px">
                         <apex:outputPanel id="resultTable">
                              
                              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                  <apex:repeat value="{!searchResults}" var="item" >
                                   <tr class="dataRow">
                                       <apex:outputPanel layout="none" rendered="{!isUserHasAdminPermission}">
                                       <td class="dataCell" width="5%" style="text-align:center">
                                             <apex:commandLink action="{!GoToManifoldDetail}" reRender="resultTable" >
                                                 <apex:image id="btnEdit" value="{!$Resource.Detail_Icon}" width="20px" height="20px"/>
                                                 <apex:param name="branchId" value="{!item.BranchId}" />
                                             </apex:commandLink>
                                             
                                       </td>
                                       </apex:outputPanel>
                                       <td width="20%" class="dataCell">
                                            <apex:outputText value="{!IF(OR((selectedMType == 'Return' && searchOption == ''), searchOption == 'Return'), 
                                                           item.ReturnManifoldNum, item.SupplyManifoldNum)}"  />
                                            
                                       </td>
                                       <td width="11%" class="dataCell">
                                            <apex:outputText value="{!IF(OR((selectedMType == 'Return' && searchOption == ''), searchOption == 'Return'), 
                                                       item.ReturnBranch, item.SupplyBranch)}"/>
                                            
                                       </td>
                                       <td width="20%" class="dataCell">
                                           <apex:outputText value="{!IF(OR((selectedMType == 'Return' && searchOption == ''), searchOption == 'Return'), 
                                                           item.SupplyManifoldNum, item.ReturnManifoldNum)}"  />
                                       
                                       </td>
                                       <td width="11%" class="dataCell">
                                             <apex:outputText value="{!IF(OR((selectedMType == 'Return' && searchOption == ''), searchOption == 'Return'), 
                                                        item.SupplyBranch,item.ReturnBranch)}"/>
                                       </td>
                                       <td width="20%" class="dataCell">
                                             <apex:outputField value="{!item.EquipList[0].Piping_Line_Equipment__c}" />
                                          
                                       </td>

                                       <td width="10%" class="dataCell">
                                             <apex:outputField value="{!item.EquipList[0].Piping_Line_Equipment__r.Line_Size__c}" />
                                      
                                       </td>
                                   </tr>
                                      <apex:repeat value="{!item.EquipList}" var="e" first="1" >
                                          <tr class="dataRow">
                                              <apex:outputPanel layout="none" rendered="{!isUserHasAdminPermission}"> 
                                                   <td colspan="5"  width="67%" class="dataCell"></td>
                                              </apex:outputPanel>
                                              <apex:outputPanel layout="none" rendered="{!NOT(isUserHasAdminPermission)}">
                                                   <td colspan="4"  width="67%" class="dataCell"></td>
                                              </apex:outputPanel>
                                              <td width="20%" class="dataCell">
                                                 <apex:outputField value="{!e.Piping_Line_Equipment__c}" />
                                              </td>

                                              <td width="10%" class="dataCell">
                                                 <apex:outputField value="{!e.Piping_Line_Equipment__r.Line_Size__c}" />
                                            
                                              </td>
                                          </tr>
                                      </apex:repeat>
                                      <tr class="dataRow">
                                         <apex:outputPanel layout="none" rendered="{!isUserHasAdminPermission}">
                                               <td colspan="7" class="dataCell bBorder"></td>
                                         </apex:outputPanel>
                                         <apex:outputPanel layout="none" rendered="{!NOT(isUserHasAdminPermission)}">
                                               <td colspan="6" class="dataCell bBorder"></td>
                                         </apex:outputPanel>
                                      </tr>
                                  </apex:repeat>
                              </table>
                          </apex:outputPanel>
                      </div>
                      
                      </apex:outputPanel>
                      
                </apex:pageBlockSection>
            </apex:outputPanel>
            
            
        </apex:pageBlock>
    
    </apex:form>
</apex:page>