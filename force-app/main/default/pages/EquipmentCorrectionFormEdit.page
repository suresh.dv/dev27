<apex:page standardController="Equipment_Correction_Form__c" extensions="EquipmentCorrectionFormControllerX" title="Equipment Data Update Edit" lightningStylesheets="true">
    
<!--28-Feb-17 Miro Zelina - included Well Events into Equipment Correction Form-->
<!--03-Jul-17 Miro Zelina - included System & Sub-System into Equipment Correction Form-->
<style>
    .label
    {
        padding-right:20px;
        text-align:right;
        font-size: 91%;
        font-weight:bold;
        width:15%;
    }
    .header
    {
        font-weight:bold;
        color:darkgoldenrod;
    }
</style>
<script language='javascript'>
function display(myimage) 
{
	//KK 6/Apr/2015: change the size to try to fit the image
     html = "<HTML><HEAD><TITLE>Photo</TITLE>" +
      "</HEAD><BODY LEFTMARGIN=0 " 
      + "MARGINWIDTH=0 TOPMARGIN=0 MARGINHEIGHT=0><CENTER>" 
      + "<IMG SRC='" + myimage + "' BORDER=0 NAME=image " 
      + "onload='window.resizeTo(window.outerWidth-window.innerWidth+document.image.width+10,window.outerHeight-window.innerHeight+document.image.height+10);'>"
      + "</CENTER>" 
      + "</BODY></HTML>";
     window.opener=self;
	//KK 6/Apr/2015: add resizable and add scrollbars and add toolbar
     popup = window.open('','image', 'toolbar=1, location=0, directories=0, status=no, menubar=0, resizable=1, scrollbars=1, copyhistory=0');
     popup.document.open();
     popup.document.write(html);
     popup.document.focus();
     popup.document.close();
 };
</SCRIPT>
<!-- Don't allow user to edit "Closed" form. Display readonly fields if Status = "Closed" -->
<apex:sectionHeader title="Equipment Data Update" subtitle="{!form.Name}"/>
<apex:form >
    <apex:pageMessage severity="Info"
        strength="3"
        summary="{!$Label.Equipment_Transfer_New_Button_Info_Message}"
        escape="false"
        rendered="{!!hasEquipment}">
    </apex:pageMessage>
    <apex:pageMessages />
    <apex:pageBlock title="Equipment Data Update Edit" rendered="{!hasEquipment}">
        <apex:pageBlockButtons >
            <apex:commandButton value="{!IF(form.Id == null, 'Submit' ,'Save')}" action="{!save}"  rendered="{!isEditable}"/>
            <apex:commandButton value="Cancel" action="{!cancel}" />
        </apex:pageBlockButtons>
        <apex:pageBlockSection title="Information">
            <apex:outputField value="{!form.Equipment__c}" />
            <apex:outputField value="{!form.Status__c}" />
            <apex:outputField value="{!form.Equipment__r.Location__c}" rendered="{!form.Equipment__r.Location__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Well_Event__c}" rendered="{!form.Equipment__r.Well_Event__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.System__c}" rendered="{!form.Equipment__r.System__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Sub_System__c}" rendered="{!form.Equipment__r.Sub_System__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Yard__c}" rendered="{!form.Equipment__r.Yard__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Functional_Equipment_Level__c}" rendered="{!form.Equipment__r.Functional_Equipment_Level__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Facility__c}" 
                rendered="{!AND(form.Equipment__r.Facility__c != null, form.Equipment__r.Location__c == null, form.Equipment__r.Well_Event__c == null, form.Equipment__r.System__c == null, form.Equipment__r.Sub_System__c == null, form.Equipment__r.Functional_Equipment_Level__c == null,  form.Equipment__r.Yard__c == null)}"/>
            <!-- Equipment Status -->
            <!-- Cannot use required="true" because view state limit error when missing required field and big file 
                    + The first thing that happens in a postback request is that the view state is decoded.-->
            <apex:pageBlockSectionItem rendered="{!isEditable}">
                <apex:outputLabel value="{!$ObjectType.Equipment_Correction_Form__c.Fields.Equipment_Status__c.Label}" /> 
                <apex:outputPanel >
                    <apex:inputField value="{!form.Equipment_Status__c}" />
                </apex:outputPanel>
            </apex:pageBlockSectionItem>
            
            <apex:outputField value="{!form.Equipment_Status__c}" rendered="{!isClosed}"/>
            
            <apex:outputField value="{!form.Equipment__r.Location__r.Route__c}" rendered="{!form.Equipment__r.Location__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Well_Event__r.Route__c}" rendered="{!form.Equipment__r.Well_Event__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.System__r.Route__c}" rendered="{!form.Equipment__r.System__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Sub_System__r.Route__c}" rendered="{!form.Equipment__r.Sub_System__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Functional_Equipment_Level__r.Route__c}" rendered="{!form.Equipment__r.Functional_Equipment_Level__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Yard__r.Route__c}" rendered="{!form.Equipment__r.Yard__c != null}"/>
            <apex:outputField value="{!form.Equipment__r.Facility__r.Plant_Section__c}" 
                rendered="{!AND(form.Equipment__r.Facility__c != null, form.Equipment__r.Location__c == null, form.Equipment__r.Well_Event__c == null, form.Equipment__r.System__c == null, form.Equipment__r.Sub_System__c == null, form.Equipment__r.Functional_Equipment_Level__c == null,  form.Equipment__r.Yard__c == null)}"/>
            <apex:outputText value="" />
            
            <apex:outputField value="{!form.Equipment__r.Planner_Group__c}" />
        </apex:pageBlockSection> 
        <apex:pageBlockSection title="Data Correction" columns="1">
            <apex:panelGrid columns="3" width="100%" columnClasses="label, dataCol, dataCol" rules="rows">
                <apex:outputText />
                <apex:outputText value="Current Value" styleClass="header"/>
                <apex:outputText value="New Value" styleClass="header"/>
                
                <!-- Equipment Description -->
                <apex:outputLabel value="{!$ObjectType.Equipment__c.Fields.Description_of_Equipment__c.Label}"/>
                <apex:outputField value="{!form.Description_old__c}" />
                <apex:inputField value="{!form.Description_new__c}" rendered="{!isEditable}"/>
                <apex:outputField value="{!form.Description_new__c}" rendered="{!isClosed}"/>
                
                <!-- Equipment Manufacturer -->
                <apex:outputLabel value="{!$ObjectType.Equipment__c.Fields.Manufacturer__c.Label}" />
                <apex:outputField value="{!form.Manufacturer_old__c}" />
                <apex:inputField value="{!form.Manufacturer_new__c}" rendered="{!isEditable}"/>
                <apex:outputField value="{!form.Manufacturer_new__c}" rendered="{!isClosed}"/>
                
                <!-- Equipment Model # -->
                <apex:outputLabel value="{!$ObjectType.Equipment__c.Fields.Model_Number__c.Label}" />
                <apex:outputField value="{!form.Model_Number_old__c}" />
                <apex:inputField value="{!form.Model_Number_new__c}" rendered="{!isEditable}"/>
                <apex:outputField value="{!form.Model_Number_new__c}" rendered="{!isClosed}"/>
                
                <!-- Equipment Serial # -->
                <apex:outputLabel value="{!$ObjectType.Equipment__c.Fields.Manufacturer_Serial_No__c.Label}"/>
                <apex:outputField value="{!form.Manufacturer_Serial_No_old__c}" />
                <apex:inputField value="{!form.Manufacturer_Serial_No_new__c}" rendered="{!isEditable}"/>
                <apex:outputField value="{!form.Manufacturer_Serial_No_new__c}" rendered="{!isClosed}"/>
                
                <!-- Equipment Tag # -->
                <apex:outputLabel value="{!$ObjectType.Equipment__c.Fields.Tag_Number__c.Label}" />
                <apex:outputField value="{!form.Tag_Number_old__c}" />
                <apex:inputField value="{!form.Tag_Number_new__c}" rendered="{!isEditable}"/>
                <apex:outputField value="{!form.Tag_Number_new__c}" rendered="{!isClosed}"/>
                
                <!-- Attachment -->
                <apex:outputLabel value="Attachment" /> 
                <apex:inputFile value="{!photo.body}" filename="{!photo.Name}" contentType="{!photo.contentType}" rendered="{!isEditable && (photo.Id == null && photoFile.ContentDocumentId == null)}"/>
                <apex:outputPanel rendered="{!photo.Id != null || photoFile.ContentDocumentId != null}">
	                <apex:image height="150px" width="150px" url="/servlet/servlet.FileDownload?file={!photo.Id}" rendered="{!If(CONTAINS(photo.ContentType, 'image'), true, false) && photoFile.ContentDocumentId == null}" onclick="javascript:display('/servlet/servlet.FileDownload?file={!photo.Id}')"/>
	                <apex:outputLink value="/servlet/servlet.FileDownload?file={!photo.Id}" target="_blank" rendered="{!If(CONTAINS(photo.ContentType, 'image'), false, true) && photoFile.ContentDocumentId == null}">{!photo.Name}</apex:outputLink>
                    <apex:outputLink value="/{!photoFile.ContentDocumentId}" target="_blank" rendered="{!photoFile.ContentDocumentId != null}">{!photoFile.ContentDocument.Title}</apex:outputLink>
                    <apex:commandLink action="{!DeletePhoto}" rendered="{!isEditable && (photo.Id != null || photoFile.ContentDocumentId != null)}" onclick="return confirm('Are you sure?');">
	                   <apex:image value="/img/func_icons/remove12_on.gif" />
	                </apex:commandLink>
                </apex:outputPanel>
            </apex:panelGrid>
           
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Additional Information" columns="2">
            <apex:inputCheckbox value="{!form.Safety_Critical_Equipment__c}" rendered="{!isEditable}" />
            <apex:outputField value="{!form.Safety_Critical_Equipment__c}" rendered="{!isClosed}"/>
            <apex:inputCheckbox value="{!form.Regulatory_Equipment__c}"  rendered="{!isEditable}"/>
            <apex:outputField value="{!form.Regulatory_Equipment__c}" rendered="{!isClosed}"/>
            <apex:inputField value="{!form.Expiration_Date__c}" rendered="{!isEditable && isUserAdmin}"/>
            <apex:outputField value="{!form.Expiration_Date__c}" rendered="{!isClosed || !isUserAdmin}" />
        </apex:pageBlockSection>

        <apex:pageBlockSection title="Comments">
            <apex:inputField value="{!form.Comments__c}" style="width:250px" rendered="{!isEditable}"/>
            <apex:outputField value="{!form.Comments__c}" style="width:250px" rendered="{!isClosed}"/>
            <apex:inputField value="{!form.Reason__c}" style="width:250px" rendered="{!isEditable && isUserAdmin}"/>
            <apex:outputField value="{!form.Reason__c}" style="width:250px" rendered="{!isClosed || !isUserAdmin}"/>
        </apex:pageBlockSection>
    </apex:pageBlock>
    <!-- inputFile issue: file body always null when creating a new form with upload photo for equipments
        issue scenario: InputFile is rendered on some condition. When it is initially rendered and file is upload ok. 
            But when it is not rendered initially and I change some condition on which it is rendered then i try to attach file, File body = null
        solution: put a hidden inputfile  -->
    <apex:inputFile value="" filename="" style=" display: none" />
</apex:form>

</apex:page>