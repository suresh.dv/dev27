$(document).ready(function(){	

//menu functionality
$html = $('html');
$window = $(window);

$menuButton = $('#site-header-menu-button');
$menuButton.click(showMenu);

$navOverlay = $('#nav-overlay');
$navMenu = $('#nav-menu');

$siteHeader = $('#site-header');


//close menus by clicking close button and overlay
$closeButton = $('#nav-menu-close');
$closeButton.click(closeMenu);
$navOverlay.click(closeMenu);

//dont close menu when clicked in menu area
$navMenu.click(function(event){
	event.stopPropagation();
})



//Back to Top link
function backToTop(e){
	$("html, body").animate({ scrollTop: 0 }, 800);
	e.preventDefault();
}


function navigateToHash(e){
	var url = window.location.pathname;
	var currentFilename = url.substring(url.lastIndexOf('/')+1);
	
	var target = $(this).attr('href');
	var targetFilename = target.substring(target.lastIndexOf('/')+1, target.lastIndexOf('#'));
	
	
	if(currentFilename == targetFilename){
		$navMenu.find('.active').removeClass('active');
		$(this).addClass('active');
		closeMenu();
	}
	console.log('2222');
}



//Expanders
function expanderClick(e){
	$(this)
		.siblings('.expander-content').slideToggle()
		.parents('.expander').toggleClass('open');
	e.preventDefault();
}



//Prevent Overlay overscroll
$navOverlay
	.on('scroll mousewheel touchmove', function(e){ 
		if($siteHeader.css('position') == "fixed"){
			e.preventDefault();
		}
	});
$navMenu.on('scroll mousewheel touchmove', function(e){
		if ($navOverlay.get(0).scrollHeight >  $navOverlay.get(0).clientHeight){
			e.stopPropagation();
		}
	});



// Banner slides
$('#banner-rotations').cycle({
	timeout:5000, 
	speed: 1200,
	fx:'scrollHorz',
	slides: 'div',
	after: function(){
		$(this).css("height","100%");
		$(this).css("width","100%");
	},
	});
	

}); //end ready


function showMenu(e){
	$html.toggleClass('menu-open');
	e.preventDefault();
}

function closeMenu(){
	$html.removeClass('menu-open')
}

function scrollToTarget(e){
	$anchor = $(this).attr('href')
	$target = $($anchor);

	$scrollTop = $target.offset().top;
	$('html, body').animate({
		scrollTop: $scrollTop
		}, 500);
	
	e.preventDefault();
}