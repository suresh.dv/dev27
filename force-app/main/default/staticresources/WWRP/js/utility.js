$=jQuery;



//Detect Javascript
$('html').addClass('supports-js');



/*Detect CSS transition support*/
var s = document.createElement('p').style,
    supportsTransitions = 'transition' in s ||
                          'WebkitTransition' in s ||
                          'MozTransition' in s ||
                          'msTransition' in s ||
                          'OTransition' in s;
if(supportsTransitions){
	$('html').addClass('supports-transitions')	
}else{
	$('html').addClass('disabled-transitions')	
}





//Detect IE
function ie(){
    var undef,
        v = 3,
        div = document.createElement('div'),
        all = div.getElementsByTagName('i');
    
    while (
        div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
        all[0]
    );
    
    return v > 4 ? v : undef;
}
//IE Version
var ieVersion = ie();
if (ieVersion){
	$('html').addClass('ie' + ieVersion)
}

// Detect IE 10
if (/*@cc_on!@*/false && document.documentMode === 10) {
    document.documentElement.className+=' ie10';
}




/*Detect CSS 3D Transform support*/
$(document).ready(function(){
var el = document.createElement('p'), 
        has3D,
        transforms = {
            'webkitTransform':'-webkit-transform',
            'OTransform':'-o-transform',
            'msTransform':'-ms-transform',
            'MozTransform':'-moz-transform',
            'transform':'transform'
        };

    // Add it to the body to get the computed style.
    document.body.insertBefore(el, null);

    for (var t in transforms) {
        if (el.style[t] !== undefined) {
            el.style[t] = "translate3d(1px,1px,1px)";
            has3D = window.getComputedStyle(el).getPropertyValue(transforms[t]);
        }
    }
    document.body.removeChild(el);
    supports3D = has3D !== undefined && has3D.length > 0 && has3D !== "none";
 
if(supports3D){
	$('html').addClass('supports-3D')	
}else{
	$('html').addClass('disabled-3D')
}

    
}); //end ready


//Detect Android
function getAndroidVersion(ua) {
    ua = (ua || navigator.userAgent).toLowerCase(); 
    var match = ua.match(/android\s([0-9\.]*)/);
    return match ? match[1] : false;
};

//Disable 3D-transforms accelerated menus in Android 4.0
AndroidVersion = parseFloat(getAndroidVersion());
if(AndroidVersion < 4.1){
	$('html').removeClass('supports-3D').addClass('disabled-3D')
	/*document.getElementById("tablet-css").sheet.disabled = true
	document.getElementById("mobile-css").sheet.disabled = true*/
}



//HTML5 Video support
/*var supportsVideo = false;
	var v = document.createElement('video');
	if(v.canPlayType && v.canPlayType('video/mp4').replace(/no/, '')) {
	   supportsVideo = true;
	}
	
	v.setAttribute('onplay', 'return;');
	supportsVideoEvents = typeof v.onplay == 'function';
*/



//SVG
function supportsSvg() {
    return document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Shape", "1.0")
}
if (supportsSvg()){
	$('html').addClass('supports-SVG')
}

//Detect Touch
touchEnabled = !!('ontouchstart' in window) // works on most browsers 
      || !!('onmsgesturechange' in window); // works on ie10