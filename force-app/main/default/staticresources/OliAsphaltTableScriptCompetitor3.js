var DeliveredCost_Comp3;
var PartialDC_Comp3;
var PartialDCNetback_Comp3;

var RailCarFeeComp3Processed = 0;
var Axle5comp3VarProcessed;
var Axle6comp3VarProcessed;
var Axle7comp3VarProcessed;
var Axle8comp3VarProcessed;

var MarginComp3;
var SalesPriceComp3;
var NetBackOriginComp3;
var ApproximateMarginComp3;

// Function for Husky supplier 1 / #1 column at the table / #1 summary table
function DelivCostCOMP3Calculate(){    
    
    var ProductCost = document.getElementById("OliPage:form1:pageblock1:ProductCostcomp3").value.replace(/,/g, '');

    var FreighttoCustomer = document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp3").value.replace(/,/g, '');
    var DifferentialVal = document.getElementById("OliPage:form1:pageblock1:Differential_comp3").value.replace(/,/g, '');
 
    var MarketingFee = document.getElementById("OliPage:form1:pageblock1:Marketing_Fee_comp3").value.replace(/,/g, '');
    var TerminalThroughputs = document.getElementById("OliPage:form1:pageblock1:Terminal_Throughputs_comp3").value.replace(/,/g, '');
    var ChemicalCosts = document.getElementById("OliPage:form1:pageblock1:Chemical_Costs_comp3").value.replace(/,/g, '');
    var ModificationCosts = document.getElementById("OliPage:form1:pageblock1:Modification_Costs_comp3").value.replace(/,/g, '');
    
    var marginCheckboxPrct = document.getElementById("OliPage:form1:pageblock1:margin1Percentage").checked;
    	
	//Delivered Cost partial calculation for Netback at Summary Table use
 	   PartialDCNetback_Comp3 = parseFloat(DifferentialVal) + parseFloat(MarketingFee) + parseFloat(TerminalThroughputs) + parseFloat(ChemicalCosts) + parseFloat(ModificationCosts);
    //Delivered Cost partial calculation - For Summary Table use
 	   PartialDC_Comp3 = parseFloat(ProductCost) + parseFloat(PartialDCNetback_Comp3);
    //Delivered Cost calculation
  	  DeliveredCost_Comp3 = parseFloat(PartialDC_Comp3) + parseFloat(FreighttoCustomer) + parseFloat(RailCarFeeComp3Processed);
 	   document.getElementById("OliPage:form1:pageblock1:DelivCostcomp3").value = thousands_separators(DeliveredCost_Comp3.toFixed(6));
    
    //Decide how to calculate Margin - as $ value or % according selected checkbox
    // trigger next function:  
   
            if (marginCheckboxPrct)
        {
            MarginPrctCOMP3Calculate();
        } 
        else
        {	
            MarginCOMP3Calculate();
        }
}
// END OF DELIVERED COST CALCULATION

// MARGIN / get margin value out of delivered cost and margin %
function MarginPrctCOMP3Calculate(){ 
    var impt2 = document.getElementById("OliPage:form1:pageblock1:DelivCostcomp3").value.replace(/,/g, '');
    var impt4 = document.getElementById("OliPage:form1:pageblock1:MarginPrctcomp3").value.replace(/,/g, ''); 
    var impt3 = (parseFloat(impt2) * parseFloat(impt4)) / 100;
    document.getElementById("OliPage:form1:pageblock1:Margincomp3").value = impt3.toFixed(6);
    
    // trigger next function:    
    SetMarginVarcomp3();
}

// MARGIN % / get margin % value out of delivered cost and margin

function MarginCOMP3Calculate(){ 
    var impt2 = document.getElementById("OliPage:form1:pageblock1:DelivCostcomp3").value.replace(/,/g, '');
    var impt3 = document.getElementById("OliPage:form1:pageblock1:Margincomp3").value.replace(/,/g, '');
    var impt4 = (parseFloat(impt3) / parseFloat(impt2) * 100);
    
    if (isNaN(impt4)) impt4 = 0;
    document.getElementById("OliPage:form1:pageblock1:MarginPrctcomp3").value = impt4.toFixed(6); 
    
    // trigger next function:    
    SetMarginVarcomp3();
}

function SetMarginVarcomp3(){
    
    var setmarginvartemp = document.getElementById("OliPage:form1:pageblock1:Margincomp3").value.replace(/[^0-9.-]+/g,"");
    MarginComp3 = setmarginvartemp;
  //  alert("A" + MarginComp3);
     // trigger next function:    
    SalespriceSelectorComp3();
}

function SalespriceSelectorComp3(){
//   var SalespriceVAL = document.getElementById("OliPage:form1:pageblock1:salesPrice2").checked;
    var SalespriceBID = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2").checked;

         if (SalespriceBID)
        {
            SalespriceSelectedBIDComp3();
        } 
    else
        {
            SalespriceSelectedVALComp3();
        }
    
}
// SALES PRICE CALCULATION
function SalespriceSelectedVALComp3(){
    var marginComp3 = document.getElementById("OliPage:form1:pageblock1:Margincomp3").value.replace(/,/g, '');
    SalesPriceComp3 = parseFloat(DeliveredCost_Comp3) + parseFloat(marginComp3);
    document.getElementById("OliPage:form1:pageblock1:salesPriceComp3").value = SalesPriceComp3.toFixed(6);
// trigger next function:    
    NetBackOriginComp3Calculate();
    //added:
    USDValuesCalculateComp3();
    SummaryTableCalculate();
}

function SalespriceSelectedBIDComp3(){
	var SalespriceBid = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value.replace(/,/g, '');
    SalesPriceComp3 = SalespriceBid;
    // fill salesprice val field / doesnt enter the calculation here:
    var marginComp3 = document.getElementById("OliPage:form1:pageblock1:Margincomp3").value.replace(/,/g, '');
    var SalesPriceComp3DisplayVal = parseFloat(DeliveredCost_Comp3) + parseFloat(marginComp3);
    document.getElementById("OliPage:form1:pageblock1:salesPriceComp3").value = SalesPriceComp3DisplayVal.toFixed(6);   
    
// trigger next function:    
    NetBackOriginComp3Calculate();  
    //added
    USDValuesCalculateComp3();
    SummaryTableCalculate();
}
// SALES PRICE CALCULATION end

// NETBACK ORIGIN CALCULATION
function NetBackOriginComp3Calculate(){ 

var ProductCostComp3Current = document.getElementById("OliPage:form1:pageblock1:ProductCostcomp3").value;   
    NetBackOriginComp3 = parseFloat(SalesPriceComp3) - parseFloat(ProductCostComp3Current);
    document.getElementById("OliPage:form1:pageblock1:Net_Back_comp3").value = NetBackOriginComp3.toFixed(6);    

}

// APPROXIMATE MARGIN CALCULATION - x

function USDValuesCalculateComp3() {
    
	ExchangeRate = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value;
    
    var DeliveredCostUSD = parseFloat(ExchangeRate) * parseFloat(DeliveredCost_Comp3);
    document.getElementById("OliPage:form1:pageblock1:DeliveredCostUSDComp3").value = DeliveredCostUSD.toFixed(6);
    var ApproxMarginUSD = parseFloat(ExchangeRate) * parseFloat(MarginComp3);
    document.getElementById("OliPage:form1:pageblock1:ApproxMarginUSDComp3").value = ApproxMarginUSD.toFixed(6);
    var SalesPriceUSD = parseFloat(ExchangeRate) * parseFloat(SalesPriceComp3);
    document.getElementById("OliPage:form1:pageblock1:SalesPriceUSDComp3").value = SalesPriceUSD.toFixed(6);
    var NetBackOriginUSD = parseFloat(ExchangeRate) * parseFloat(NetBackOriginComp3);
    document.getElementById("OliPage:form1:pageblock1:Net_Back_Origin_USD_comp3").value = NetBackOriginUSD.toFixed(6);
    
	// formating of numbers
    ReplaceZerosComp3();
}



//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE LAUNCHER / launch particular variation of Summary table when conditions meet
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableCalculateComp3Launcher() {
    var SelectedSalespriceBIDcheckbox = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2").checked; 
	var SelectedCurrencyStatusforTable = document.getElementById("OliPage:form1:pageblock1:CurrencyPicklist").value;

    if(SelectedSalespriceBIDcheckbox == true && SelectedCurrencyStatusforTable == 'CAD') 
    {
    	SummaryTableComp3CalculateBID();  
    }
	else if (SelectedSalespriceBIDcheckbox == true && SelectedCurrencyStatusforTable == 'US') 
    {
    	SummaryTableComp3CalculateBID_USD();
    }
    else if (SelectedSalespriceBIDcheckbox == false && SelectedCurrencyStatusforTable == 'US') 
    {
        SummaryTableComp3CalculateUSD();
	}
    else
    {
        SummaryTableComp3Calculate();
	}
    
	fillSummaryTable();
}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE / default values
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableComp3Calculate (){   
document.getElementById("selected_supplier_display").value= " default view";
 	var partialSalesPrice =  parseFloat(PartialDC_Comp3) + parseFloat(MarginComp3);  
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = PartialDC_Comp3 + Axle5comp3VarProcessed;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = PartialDC_Comp3 + Axle6comp3VarProcessed;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = PartialDC_Comp3 + Axle7comp3VarProcessed;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = PartialDC_Comp3 + Axle8comp3VarProcessed;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = PartialDC_Comp3 + RailCarFeeComp3Processed;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
    var HS1_DC_PU = parseFloat(PartialDC_Comp3);
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
// Summary table calculation FOR MARGIN  
    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle5comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle6comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;  
    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle7comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle8comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0; 
    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDC_Comp3) + parseFloat(RailCarFeeComp3Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) * 100;
     if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
    
    var HS1_Margin_PU = ( parseFloat(partialSalesPrice) - parseFloat(PartialDC_Comp3) ) / parseFloat(partialSalesPrice) * 100;
                         if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
    
// Summary table calculation FOR NETBACK  
    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed)));
    	document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed)));
    	document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed)));
    	document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);
    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed)));
    	document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed)));
    	document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);
    var HS1_Netback_PU = parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Comp3);
    	document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);
    
// Summary table calculation for SALESPRICE
/*	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginComp3);
      document.getElementById("SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginComp3);
      document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginComp3);
      document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginComp3);
      document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginComp3);
      document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginComp3);
      document.getElementById("SalespricePU").value = HS1_SP_PU.toFixed(6);
} */

// Summary table calculation for SALESPRICE
	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginComp3);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginComp3);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginComp3);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginComp3);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginComp3);
      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginComp3);
      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
}
//____________________________________________________________________________________________________________________



//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE BID / salesprice bid included to calculations
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableComp3CalculateBID(){  
document.getElementById("selected_supplier_display").value= "   (Sales Price BID included)";
    var SalesPriceBIDvalue = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value.replace(/,/g, '');
    var partialSalesPrice =  parseFloat(PartialDC_Comp3) + parseFloat(MarginComp3);  
    AxleRateSelectorStatus = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;
        //vars for applying bid prices
    var applyBIDpricePU = document.getElementById("OliPage:form1:pageblock1:IncludePU_bid");
    var applyBIDpriceRC = document.getElementById("OliPage:form1:pageblock1:IncludeRC_bid"); 
    var applyBIDpriceA5 = document.getElementById("OliPage:form1:pageblock1:IncludeA5_bid"); 
    var applyBIDpriceA6 = document.getElementById("OliPage:form1:pageblock1:IncludeA6_bid"); 
    var applyBIDpriceA7 = document.getElementById("OliPage:form1:pageblock1:IncludeA7_bid"); 
    var applyBIDpriceA8 = document.getElementById("OliPage:form1:pageblock1:IncludeA8_bid"); 
 
    // Summary table calculation FOR DELIVERED COST BID
    var HS1_DC_A5 = PartialDC_Comp3 + Axle5comp3VarProcessed;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = PartialDC_Comp3 + Axle6comp3VarProcessed;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = PartialDC_Comp3 + Axle7comp3VarProcessed;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = PartialDC_Comp3 + Axle8comp3VarProcessed;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = PartialDC_Comp3 + RailCarFeeComp3Processed;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
	var HS1_DC_PU = parseFloat(PartialDC_Comp3);
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
   // Summary table calculation for SALESPRICE BID

    //AXLE 5 ROW AT SUMMARY TABLE
    if(AxleRateSelectorStatus == 'Axle 5 Price' && applyBIDpriceA5.checked == false) // if(AxleRateSelectorStatus == 'Axle 5 Price') 
        {
            // salesprice
            document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed)));
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDvalue - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
            
        }
        // Salesprice BID summary table controller
        else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus != 'Axle 5 Price' ) {
            
            var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed)));
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        }
        else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus == 'Axle 5 Price') {
        
       		 var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed)));
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA5; 
            SalesPriceComp3 = SalesPriceBIDA5; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();

    }    
	// Salesprice BID summary table controller - end
        
                else
                {
                    //salesprice
                    var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginComp3); 
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
                    // netback
                    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed)));
                    document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);       
                    // margin
                    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle5comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
                    if (!isFinite(HS1_Margin_A5)) HS1_Margin_A5 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
                    
                }
    //AXLE 6 ROW AT SUMMARY TABLE
       if(AxleRateSelectorStatus == 'Axle 6 Price' && applyBIDpriceA6.checked == false) // if(AxleRateSelectorStatus == 'Axle 6 Price') 
        {
            //salesprice
            document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDvalue - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
            
        }
    
     // Salesprice BID summary table controller
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus != 'Axle 6 Price' ) {
            
            var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        }
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus == 'Axle 6 Price') {
        
       		 var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA6; 
            SalesPriceComp3 = SalesPriceBIDA6; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
    }    
	// Salesprice BID summary table controller - end

                else
                {
                    //salesprice
                    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginComp3);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    // netback
                    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed)));
                    document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6); 
                    // margin
                    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle6comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
                    if (!isFinite(HS1_Margin_A6)) HS1_Margin_A6 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
                } 
    //AXLE 7 ROW AT SUMMARY TABLE
       if(AxleRateSelectorStatus == 'Axle 7 Price' && applyBIDpriceA7.checked == false) // if(AxleRateSelectorStatus == 'Axle 7 Price') 
        {
            //salesprice
            document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed)));
			document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6); 
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDvalue - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus != 'Axle 7 Price' ) {
            
            var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed)));
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus == 'Axle 7 Price') {
        
       		 var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed)));
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA7; 
            SalesPriceComp3 = SalesPriceBIDA7; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
    }    
	// Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginComp3); 
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    // netback
                    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed)));
                    document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);    
                    // margin
                    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle7comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
                    if (!isFinite(HS1_Margin_A7)) HS1_Margin_A7 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
                }
    
    //AXLE 8 ROW AT SUMMARY TABLE
       if(AxleRateSelectorStatus == 'Axle 8 Price' && applyBIDpriceA8.checked == false) // if(AxleRateSelectorStatus == 'Axle 8 Price') 
        {
            //salesprice
            document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed)));
			document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6); 
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDvalue - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus != 'Axle 8 Price' ) {
        
        var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed)));
        document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
        // margin
        var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
        if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
        if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
    }
        else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus == 'Axle 8 Price') {
            
            var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed)));
            document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA8; 
            SalesPriceComp3 = SalesPriceBIDA8; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
            
        }    
    // Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginComp3);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    // netback
                    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed)));
                    document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
                    // margin
                    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle8comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
                    if (!isFinite(HS1_Margin_A8)) HS1_Margin_A8 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
                } 
    //RAIL CAR ROW AT SUMMARY TABLE
          if(AxleRateSelectorStatus == 'Railcar' && applyBIDpriceRC.checked == false)  // if(AxleRateSelectorStatus == 'Railcar') 
        {
            //salesprice
            document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed)));
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDvalue - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
        }
    
        // Salesprice BID summary table controller
    else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus != 'Railcar' ) {
        
        var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed)));
        document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
        // margin
        var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
        if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
        if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
    }
    
        else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus == 'Railcar') {
            
            var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed)));
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDRC; 
            SalesPriceComp3 = SalesPriceBIDRC; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();  
            USDValuesCalculateComp3();
        }   
    
    // Salesprice BID summary table controller - end
                else
                {
                    //salesprice
                    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginComp3);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    // netback
                    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed)));
                    document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);     
                    // margin
                    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDC_Comp3) + parseFloat(RailCarFeeComp3Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) * 100;
                    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
                    if (!isFinite(HS1_Margin_RC)) HS1_Margin_RC = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
                } 
    
     //PICKUP ROW AT SUMMARY TABLE
   if(AxleRateSelectorStatus == 'Pickup' && applyBIDpricePU.checked == false) 
        { 
            //salesprice
            document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDvalue - parseFloat(PartialDCNetback_Comp3);
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDvalue - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus != 'Pickup' ) {

        var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Comp3) ;
        document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
        // margin
        var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
        if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
        if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
    }
    
        else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus == 'Pickup') {
 
            var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, '');
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Comp3);
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDPU; 
            SalesPriceComp3 = SalesPriceBIDPU; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();  
            USDValuesCalculateComp3();
        }    
    // Salesprice BID summary table controller - end 
    
                else
                {
                    //salesprice
                    var HS1_SP_PU = parseFloat(HS1_DC_RC) + parseFloat(MarginComp3);
                    document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
                    // netback
                    var HS1_Netback_PU = parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Comp3);
                    document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);     
                    // margin
                    var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Comp3)) / parseFloat(partialSalesPrice) * 100;
                    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
                    if (!isFinite(HS1_Margin_PU)) HS1_Margin_PU = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
                } 
}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE USD / salesprice USD included to calculations
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableComp3CalculateUSD(){  
document.getElementById("selected_supplier_display").value= "   (USD currency)";
    
    	var ExchaneRateUSD = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value.replace(/,/g, '');
    	var partialSalesPrice =  parseFloat(PartialDC_Comp3) + parseFloat(MarginComp3);  
    
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = (PartialDC_Comp3 + Axle5comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = (PartialDC_Comp3 + Axle6comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = (PartialDC_Comp3 + Axle7comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = (PartialDC_Comp3 + Axle8comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = (PartialDC_Comp3 + RailCarFeeComp3Processed) * ExchaneRateUSD;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
    var HS1_DC_PU = PartialDC_Comp3 * ExchaneRateUSD;
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
// Summary table calculation FOR MARGIN  
    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle5comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle6comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle7comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle8comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDC_Comp3) + parseFloat(RailCarFeeComp3Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) * 100;
    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
	var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Comp3)) / parseFloat(partialSalesPrice) * 100;
    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
    
// Summary table calculation FOR NETBACK  
    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);
    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);
	var HS1_Netback_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Comp3)) * ExchaneRateUSD;
    	document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);

    // Summary table calculation for SALESPRICE
	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginComp3) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginComp3) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginComp3) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginComp3) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginComp3) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginComp3) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE BID USD / salesprice bid included to calculations USD
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableComp3CalculateBID_USD(){  
document.getElementById("selected_supplier_display").value= "   (Sales Price BID at USD currency)";
    
    var ExchaneRateUSD = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value;
    var SalesPriceBIDvalue = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value.replace(/,/g, '');
    var partialSalesPrice =  parseFloat(PartialDC_Comp3) + parseFloat(MarginComp3);  
    AxleRateSelectorStatus = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;
    
            //vars for applying bid prices
    var applyBIDpricePU = document.getElementById("OliPage:form1:pageblock1:IncludePU_bid");
    var applyBIDpriceRC = document.getElementById("OliPage:form1:pageblock1:IncludeRC_bid"); 
    var applyBIDpriceA5 = document.getElementById("OliPage:form1:pageblock1:IncludeA5_bid"); 
    var applyBIDpriceA6 = document.getElementById("OliPage:form1:pageblock1:IncludeA6_bid"); 
    var applyBIDpriceA7 = document.getElementById("OliPage:form1:pageblock1:IncludeA7_bid"); 
    var applyBIDpriceA8 = document.getElementById("OliPage:form1:pageblock1:IncludeA8_bid"); 
 
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = (PartialDC_Comp3 + Axle5comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = (PartialDC_Comp3 + Axle6comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = (PartialDC_Comp3 + Axle7comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = (PartialDC_Comp3 + Axle8comp3VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = (PartialDC_Comp3 + RailCarFeeComp3Processed) * ExchaneRateUSD;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
	var HS1_DC_PU = PartialDC_Comp3 * ExchaneRateUSD;
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
   // Summary table calculation for SALESPRICE BID

    //AXLE 5 ROW AT SUMMARY TABLE
        if(AxleRateSelectorStatus == 'Axle 5 Price' && applyBIDpriceA5.checked == false) // if(AxleRateSelectorStatus == 'Axle 5 Price') 
        {
            // salesprice
			//	document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = SalesPriceBIDvalue * ExchaneRateUSD;
            document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);  
             // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
			if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        }
    
         // Salesprice BID summary table controller
        else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus != 'Axle 5 Price' ) {
            
            var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed)));
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        }
    
        else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus == 'Axle 5 Price') {
        
       		 var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA5; 
            SalesPriceComp3 = SalesPriceBIDA5; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();

    }   
	// Salesprice BID summary table controller - end

                else
                {
                    //salesprice
                    var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginComp3) * ExchaneRateUSD; 
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
                    // netback
                    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle5comp3VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
					// margin
                    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle5comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
                    if (!isFinite(HS1_Margin_A5)) HS1_Margin_A5 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
                }
    
    //AXLE 6 ROW AT SUMMARY TABLE
            if(AxleRateSelectorStatus == 'Axle 6 Price' && applyBIDpriceA6.checked == false) // if(AxleRateSelectorStatus == 'Axle 6 Price') 
        {
            //salesprice
            //document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = SalesPriceBIDvalue * ExchaneRateUSD;
			document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
			// margin
            var HS1_Margin_A6_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        }
    
       // Salesprice BID summary table controller
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus != 'Axle 6 Price' ) {
            
            var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        }
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus == 'Axle 6 Price') {
        
       		 var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed))) * ExchaneRateUSD; 
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA6; 
            SalesPriceComp3 = SalesPriceBIDA6; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
    }    
	// Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginComp3) * ExchaneRateUSD;
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    // netback
                    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle6comp3VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
                    // magrin
                    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle6comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
                    if (!isFinite(HS1_Margin_A6)) HS1_Margin_A6 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
                    
                } 
    //AXLE 7 ROW AT SUMMARY TABLE
            if(AxleRateSelectorStatus == 'Axle 7 Price' && applyBIDpriceA7.checked == false) // if(AxleRateSelectorStatus == 'Axle 7 Price') 
        {
            //salesprice
            // document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = SalesPriceBIDvalue * ExchaneRateUSD;
			document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed))) * ExchaneRateUSD;
			document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
    
         // Salesprice BID summary table controller
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus != 'Axle 7 Price' ) {
            
            var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed)));
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus == 'Axle 7 Price') {
        
       		 var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA7; 
            SalesPriceComp3 = SalesPriceBIDA7; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
    }    
	// Salesprice BID summary table controller - end

                else
                {
                    //salesprice
                    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginComp3) * ExchaneRateUSD; 
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    // netback
                    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle7comp3VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6); 
                    // margin
                    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle7comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
                    if (!isFinite(HS1_Margin_A7)) HS1_Margin_A7 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
                    
                    
                }
    //AXLE 8 ROW AT SUMMARY TABLE
            if(AxleRateSelectorStatus == 'Axle 8 Price' && applyBIDpriceA8.checked == false) // if(AxleRateSelectorStatus == 'Axle 8 Price') 
        {
            //salesprice
            //document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = SalesPriceBIDvalue * ExchaneRateUSD;
			document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed))) * ExchaneRateUSD;
			document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
             // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus != 'Axle 8 Price' ) {
        
        var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed)));
        document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
        // margin
        var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
        if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
        if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
    }
        else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus == 'Axle 8 Price') {
            
            var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDA8; 
            SalesPriceComp3 = SalesPriceBIDA8; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
            
        }    
    // Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginComp3) * ExchaneRateUSD;
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    // netback
                    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(Axle8comp3VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
                    // margin
                    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) - (parseFloat(PartialDC_Comp3) + parseFloat(Axle8comp3VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8comp3VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
                    if (!isFinite(HS1_Margin_A8)) HS1_Margin_A8 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
                } 
    
    //RAIL CAR ROW AT SUMMARY TABLE
               if(AxleRateSelectorStatus == 'Railcar' && applyBIDpriceRC.checked == false) // if(AxleRateSelectorStatus == 'Railcar') 
        {
            //salesprice
            //document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = SalesPriceBIDvalue * ExchaneRateUSD;
			document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_RC_bid = ((SalesPriceBIDvalue) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed))) * ExchaneRateUSD;
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus != 'Railcar' ) {
        
        var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed)));
        document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
        // margin
        var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
        if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
        if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
    }
        else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus == 'Railcar') {
            
            var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed))) * ExchaneRateUSD;
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDRC; 
            SalesPriceComp3 = SalesPriceBIDRC; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
            
        }    
    // Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginComp3) * ExchaneRateUSD;
                    document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    // netback
                    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDCNetback_Comp3) + parseFloat(RailCarFeeComp3Processed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);   
                    // margin
                    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) - (parseFloat(PartialDC_Comp3) + parseFloat(RailCarFeeComp3Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeComp3Processed)) * 100;
                    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
                    if (!isFinite(HS1_Margin_RC)) HS1_Margin_RC = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
                } 
    
      //PICKUP ROW AT SUMMARY TABLE
                  if(AxleRateSelectorStatus == 'Pickup' && applyBIDpricePU.checked == false) // if(AxleRateSelectorStatus == 'Pickup') 
        {
            //salesprice
            //document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = SalesPriceBIDvalue * ExchaneRateUSD;
			document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_PU_bid = (SalesPriceBIDvalue - parseFloat(PartialDCNetback_Comp3)) * ExchaneRateUSD;
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus != 'Pickup' ) {
        
        var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Comp3);
        document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
        // margin
        var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
        if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
        if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
    }
        else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus == 'Pickup') {
            
            var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Comp3) * ExchaneRateUSD;
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDComp3").value = SalesPriceBIDPU; 
            SalesPriceComp3 = SalesPriceBIDPU; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackOriginComp3Calculate();   
            USDValuesCalculateComp3();
        }    
    
    // Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_PU = (parseFloat(HS1_DC_RC) + parseFloat(MarginComp3)) * ExchaneRateUSD;
                    document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
                    // netback
                    var HS1_Netback_PU = ( parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Comp3)) * ExchaneRateUSD;
                    document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);     
                    // margin
                    var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Comp3)) / parseFloat(partialSalesPrice) * 100;
                    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
                    if (!isFinite(HS1_Margin_PU)) HS1_Margin_PU = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
                } 
    
    
    
}
//____________________________________________________________________________________________________________________
