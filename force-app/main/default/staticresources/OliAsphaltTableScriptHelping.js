//global var for for all collumns:
var ExchangeRate;

var AxleRateSelectorStatus;
var SelectedSupplierSelectorStatus;
var SelectedCurrencyStatus;

var SelectedFreightMethod;

var Axle5huskysupp1Var;
var Axle5huskysupp2Var;
var Axle5huskysupp3Var;
var Axle5comp1Var;
var Axle5comp2Var;
var Axle5comp3Var;
var Axle5comp4Var;
var Axle6huskysupp1Var;
var Axle6huskysupp2Var;
var Axle6huskysupp3Var;
var Axle6comp1Var;
var Axle6comp2Var;
var Axle6comp3Var;
var Axle6comp4Var;
var Axle7huskysupp1Var;
var Axle7huskysupp2Var;
var Axle7huskysupp3Var;
var Axle7comp1Var;
var Axle7comp2Var;
var Axle7comp3Var;
var Axle7comp4Var;
var Axle8huskysupp1Var;
var Axle8huskysupp2Var;
var Axle8huskysupp3Var;
var Axle8comp1Var;
var Axle8comp2Var;
var Axle8comp3Var;
var Axle8comp4Var;
var RailCarFeeCurrencyHuskysupp1;
var RailCarFeeCurrencyHuskysupp2;
var RailCarFeeCurrencyHuskysupp3;

// FUNCTION INITIALIZE
function Initialization() {
    
    AxleRateSelectorStatus = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;
    SelectedSupplierSelectorStatus = document.getElementById("OliPage:form1:pageblock1:SelectedSupplier").value; 
    SelectedCurrencyStatus = document.getElementById("OliPage:form1:pageblock1:CurrencyPicklist").value;
    
    // pricing method from ATS Freight 
    var SelectedFreightMethodHtml = document.getElementById("OliPage:form1:pageblock1:FreightMethod").innerHTML;
    SelectedFreightMethod = SelectedFreightMethodHtml;

    // ensure that picklist values are selected
    if(SelectedSupplierSelectorStatus == '') 
    {
    	document.getElementById("OliPage:form1:pageblock1:SelectedSupplier").value = 'Husky Supplier 1';
    }
	if(SelectedCurrencyStatus == '') 
    {
    	document.getElementById("OliPage:form1:pageblock1:CurrencyPicklist").value = 'CAD';
    }
    if(AxleRateSelectorStatus == '') 
    {
        document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value = 'Pickup';
	}
    
    //summtable bid show/hide
    addSalespriceBIDtoSumTable();

    
    // Filling Rates from ATS Freight

//product values
var ProductUnit = document.getElementById("OliPage:form1:pageblock1:Oli_Product_unit").innerHTML;
var ProductDensity = document.getElementById("OliPage:form1:pageblock1:Oli_Product_density").innerHTML;
var ProductDensityConverted = ProductDensity.replace(/[^0-9.-]+/g,"");
    
    
//creating link to related ats freight record    
document.getElementById("atsFreightLink").innerHTML = window.location.hostname;
var atsFreightIdString = document.getElementById("OliPage:form1:pageblock1:atsfreightid").innerHTML;
var atsFreightRecordLink = "/lightning/r/ATS_Freight__c/" + atsFreightIdString + "/view";
var atsFreightName = document.getElementById("OliPage:form1:pageblock1:atsfreightName").innerHTML;
    
document.getElementById("AtsFreightRedirectName").value = atsFreightName;
document.getElementById("AtsFreightRedirectLink").setAttribute("href",atsFreightRecordLink);

//creating link to related ats freight record - END        

var Huskysupp1Unit =  document.getElementById("OliPage:form1:pageblock1:ats_Huskysupp1_unit").innerHTML;
var Huskysupp2Unit =  document.getElementById("OliPage:form1:pageblock1:ats_Huskysupp2_unit").innerHTML;
var Huskysupp3Unit =  document.getElementById("OliPage:form1:pageblock1:ats_Huskysupp3_unit").innerHTML;
    
    var Comp1Unit =  document.getElementById("OliPage:form1:pageblock1:ats_comp1_unit").innerHTML;
    var Comp2Unit =  document.getElementById("OliPage:form1:pageblock1:ats_comp2_unit").innerHTML;
    var Comp3Unit =  document.getElementById("OliPage:form1:pageblock1:ats_comp3_unit").innerHTML;
    var Comp4Unit =  document.getElementById("OliPage:form1:pageblock1:ats_comp4_unit").innerHTML;
    
    var UserMessageFregightTable = "";
    
    //highlighting missing units
        if(Huskysupp1Unit == '' || Huskysupp1Unit == 'Flat') { document.getElementById("showhideunit1").className = "redBackground";
    }
        if(Huskysupp2Unit == '' || Huskysupp2Unit == 'Flat') { document.getElementById("showhideunit2").className = "redBackground";
    }
        if(Huskysupp3Unit == '' || Huskysupp3Unit == 'Flat') { document.getElementById("showhideunit3").className = "redBackground";
    }
        if(Comp1Unit == '' || Comp1Unit == 'Flat') { document.getElementById("showhideunit4").className = "redBackground";
    }
        if(Comp2Unit == '' || Comp2Unit == 'Flat') { document.getElementById("showhideunit5").className = "redBackground";
    }
        if(Comp3Unit == '' || Comp3Unit == 'Flat') { document.getElementById("showhideunit6").className = "redBackground";
    }
        if(Comp4Unit == '' || Comp4Unit == 'Flat') { document.getElementById("showhideunit7").className = "redBackground";
    }
    
    // notification in case if some of units at ats freight is not selected
    if(Huskysupp1Unit == '' || Huskysupp2Unit == '' || Huskysupp3Unit == '' ||  Comp1Unit == '' ||  Comp2Unit == '' ||  Comp3Unit == '' ||  Comp4Unit == '' ) {
        UserMessageFregightTable = "Suppliers without selected unit are not converted.";
        document.getElementById("unitConversionInfo").value = UserMessageFregightTable;
    }
    if(Huskysupp1Unit == 'Flat' || Huskysupp2Unit == 'Flat' || Huskysupp3Unit == 'Flat' ||  Comp1Unit == 'Flat' ||  Comp2Unit == 'Flat' ||  Comp3Unit == 'Flat' ||  Comp4Unit == 'Flat' ) {
        document.getElementById("unitConversionInfo").value = UserMessageFregightTable + " Flat unit conversion is not possible.";
    }

    //Axle5 Huskysupp1 rates conversion
    Axle5huskysupp1Var = document.getElementById("OliPage:form1:pageblock1:Axle5huskysupp1").innerHTML;
    var Axle5huskysupp1VarPreConv = Number(Axle5huskysupp1Var.replace(/[^0-9.-]+/g,""));    
    var Axle5huskysupp1VarConverted = Axle5huskysupp1VarPreConv / getConversionRatio(ProductUnit, Huskysupp1Unit, ProductDensityConverted);   
    Axle5huskysupp1VarProcessed = Number(Axle5huskysupp1VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp1_unitConverted_A5").value = Axle5huskysupp1VarConverted.toFixed(2);  // added to show covnerted value
    
	//Axle5 Huskysupp2 rates conversion
    Axle5huskysupp2Var = document.getElementById("OliPage:form1:pageblock1:Axle5huskysupp2").innerHTML;  
    var Axle5huskysupp2VarPreConv = Number(Axle5huskysupp2Var.replace(/[^0-9.-]+/g,""));
    var Axle5huskysupp2VarConverted = Axle5huskysupp2VarPreConv / getConversionRatio(ProductUnit, Huskysupp2Unit, ProductDensityConverted);
    Axle5huskysupp2VarProcessed = Number(Axle5huskysupp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp2_unitConverted_A5").value = Axle5huskysupp2VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle5 Huskysupp3 rates conversion
    Axle5huskysupp3Var = document.getElementById("OliPage:form1:pageblock1:Axle5huskysupp3").innerHTML;
    var Axle5huskysupp3VarPreConv = Number(Axle5huskysupp3Var.replace(/[^0-9.-]+/g,""));
    var Axle5huskysupp3VarConverted = Axle5huskysupp3VarPreConv / getConversionRatio(ProductUnit, Huskysupp3Unit, ProductDensityConverted);
    Axle5huskysupp3VarProcessed = Number(Axle5huskysupp3VarConverted.toFixed(6));   
    document.getElementById("ats_freight_huskysupp3_unitConverted_A5").value = Axle5huskysupp3VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle5 Comp1 rates conversion
    Axle5comp1Var = document.getElementById("OliPage:form1:pageblock1:Axle5comp1").innerHTML;
    var Axle5comp1VarPreConv = Number(Axle5comp1Var.replace(/[^0-9.-]+/g,""));
    var Axle5comp1VarConverted = Axle5comp1VarPreConv / getConversionRatio(ProductUnit, Comp1Unit, ProductDensityConverted);
    Axle5comp1VarProcessed = Number(Axle5comp1VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp1_unitConverted_A5").value = Axle5comp1VarConverted.toFixed(2);  // added to show covnerted value
    
	//Axle5 Comp2 rates conversion
    Axle5comp2Var = document.getElementById("OliPage:form1:pageblock1:Axle5comp2").innerHTML;
    var Axle5comp2VarPreConv = Number(Axle5comp2Var.replace(/[^0-9.-]+/g,""));
    var Axle5comp2VarConverted = Axle5comp2VarPreConv / getConversionRatio(ProductUnit, Comp2Unit, ProductDensityConverted);
    Axle5comp2VarProcessed = Number(Axle5comp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp2_unitConverted_A5").value = Axle5comp2VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle5 Comp3 rates conversion
    Axle5comp3Var = document.getElementById("OliPage:form1:pageblock1:Axle5comp3").innerHTML;
    var Axle5comp3VarPreConv = Number(Axle5comp3Var.replace(/[^0-9.-]+/g,""));
    var Axle5comp3VarConverted = Axle5comp3VarPreConv / getConversionRatio(ProductUnit, Comp3Unit, ProductDensityConverted);
    Axle5comp3VarProcessed = Number(Axle5comp3VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp3_unitConverted_A5").value = Axle5comp3VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle5 Comp4 rates conversion
    Axle5comp4Var = document.getElementById("OliPage:form1:pageblock1:Axle5comp4").innerHTML;
    var Axle5comp4VarPreConv = Number(Axle5comp4Var.replace(/[^0-9.-]+/g,""));
    var Axle5comp4VarConverted = Axle5comp4VarPreConv / getConversionRatio(ProductUnit, Comp4Unit, ProductDensityConverted);
    Axle5comp4VarProcessed = Number(Axle5comp4VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp4_unitConverted_A5").value = Axle5comp4VarConverted.toFixed(2);  // added to show covnerted value
     
    //get rate values
    //Axle6 Huskysupp1 rates conversion
    Axle6huskysupp1Var = document.getElementById("OliPage:form1:pageblock1:Axle6huskysupp1").innerHTML;
    var Axle6huskysupp1VarPreConv = Number(Axle6huskysupp1Var.replace(/[^0-9.-]+/g,""));
    var Axle6huskysupp1VarConverted = Axle6huskysupp1VarPreConv / getConversionRatio(ProductUnit, Huskysupp1Unit, ProductDensityConverted);
    Axle6huskysupp1VarProcessed = Number(Axle6huskysupp1VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp1_unitConverted_A6").value = Axle6huskysupp1VarConverted.toFixed(2); // added to show covnerted value
    
    //Axle6 Huskysupp2 rates conversion
    Axle6huskysupp2Var = document.getElementById("OliPage:form1:pageblock1:Axle6huskysupp2").innerHTML;  
    var Axle6huskysupp2VarPreConv = Number(Axle6huskysupp2Var.replace(/[^0-9.-]+/g,""));
    var Axle6huskysupp2VarConverted = Axle6huskysupp2VarPreConv / getConversionRatio(ProductUnit, Huskysupp2Unit, ProductDensityConverted);
    Axle6huskysupp2VarProcessed = Number(Axle6huskysupp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp2_unitConverted_A6").value = Axle6huskysupp2VarConverted.toFixed(2); // added to show covnerted value
    
	//Axle6 Huskysupp3 rates conversion
    Axle6huskysupp3Var = document.getElementById("OliPage:form1:pageblock1:Axle6huskysupp3").innerHTML;
    var Axle6huskysupp3VarPreConv = Number(Axle6huskysupp3Var.replace(/[^0-9.-]+/g,""));
    var Axle6huskysupp3VarConverted = Axle6huskysupp3VarPreConv / getConversionRatio(ProductUnit, Huskysupp3Unit, ProductDensityConverted);
    Axle6huskysupp3VarProcessed = Number(Axle6huskysupp3VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp3_unitConverted_A6").value = Axle6huskysupp3VarConverted.toFixed(2); // added to show covnerted value
    
    //Axle6 Comp1 rates conversion
    Axle6comp1Var = document.getElementById("OliPage:form1:pageblock1:Axle6comp1").innerHTML;
    var Axle6comp1VarPreConv = Number(Axle6comp1Var.replace(/[^0-9.-]+/g,""));
    var Axle6comp1VarConverted = Axle6comp1VarPreConv / getConversionRatio(ProductUnit, Comp1Unit, ProductDensityConverted);
    Axle6comp1VarProcessed = Number(Axle6comp1VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp1_unitConverted_A6").value = Axle6comp1VarPreConv.toFixed(2);  // added to show covnerted value
    
	//Axle6 Comp2 rates conversion
    Axle6comp2Var = document.getElementById("OliPage:form1:pageblock1:Axle6comp2").innerHTML;
    var Axle6comp2VarPreConv = Number(Axle6comp2Var.replace(/[^0-9.-]+/g,""));
    var Axle6comp2VarConverted = Axle6comp2VarPreConv / getConversionRatio(ProductUnit, Comp2Unit, ProductDensityConverted);
    Axle6comp2VarProcessed = Number(Axle6comp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp2_unitConverted_A6").value = Axle6comp2VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle6 Comp3 rates conversion
    Axle6comp3Var = document.getElementById("OliPage:form1:pageblock1:Axle6comp3").innerHTML;
    var Axle6comp3VarPreConv = Number(Axle6comp3Var.replace(/[^0-9.-]+/g,""));
    var Axle6comp3VarConverted = Axle6comp3VarPreConv / getConversionRatio(ProductUnit, Comp3Unit, ProductDensityConverted);
    Axle6comp3VarProcessed = Number(Axle6comp3VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp3_unitConverted_A6").value = Axle6comp3VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle6 Comp4 rates conversion
    Axle6comp4Var = document.getElementById("OliPage:form1:pageblock1:Axle6comp4").innerHTML;
    var Axle6comp4VarPreConv = Number(Axle6comp4Var.replace(/[^0-9.-]+/g,""));
    var Axle6comp4VarConverted = Axle6comp4VarPreConv / getConversionRatio(ProductUnit, Comp4Unit, ProductDensityConverted);
    Axle6comp4VarProcessed = Number(Axle6comp4VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp4_unitConverted_A6").value = Axle6comp4VarConverted.toFixed(2);  // added to show covnerted value
     
    //get rate values
    //Axle7 Huskysupp1 rates conversion
    Axle7huskysupp1Var = document.getElementById("OliPage:form1:pageblock1:Axle7huskysupp1").innerHTML;
    var Axle7huskysupp1VarPreConv = Number(Axle7huskysupp1Var.replace(/[^0-9.-]+/g,""));
    var Axle7huskysupp1VarConverted = Axle7huskysupp1VarPreConv / getConversionRatio(ProductUnit, Huskysupp1Unit, ProductDensityConverted);
    Axle7huskysupp1VarProcessed = Number(Axle7huskysupp1VarConverted.toFixed(6));
	document.getElementById("ats_freight_huskysupp1_unitConverted_A7").value = Axle7huskysupp1VarConverted.toFixed(2); // added to show covnerted value

    
    //Axle7 Huskysupp2 rates conversion
    Axle7huskysupp2Var = document.getElementById("OliPage:form1:pageblock1:Axle7huskysupp2").innerHTML;  
    var Axle7huskysupp2VarPreConv = Number(Axle7huskysupp2Var.replace(/[^0-9.-]+/g,""));
    var Axle7huskysupp2VarConverted = Axle7huskysupp2VarPreConv / getConversionRatio(ProductUnit, Huskysupp2Unit, ProductDensityConverted);
    Axle7huskysupp2VarProcessed = Number(Axle7huskysupp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp2_unitConverted_A7").value = Axle7huskysupp2VarConverted.toFixed(2); // added to show covnerted value
    
    //Axle7 Huskysupp3 rates conversion
    Axle7huskysupp3Var = document.getElementById("OliPage:form1:pageblock1:Axle7huskysupp3").innerHTML;
    var Axle7huskysupp3VarPreConv = Number(Axle7huskysupp3Var.replace(/[^0-9.-]+/g,""));
    var Axle7huskysupp3VarConverted = Axle7huskysupp3VarPreConv / getConversionRatio(ProductUnit, Huskysupp3Unit, ProductDensityConverted);
    Axle7huskysupp3VarProcessed = Number(Axle7huskysupp3VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp3_unitConverted_A7").value = Axle7huskysupp3VarConverted.toFixed(2); // added to show covnerted value
    
         //Axle7 Comp1 rates conversion
    Axle7comp1Var = document.getElementById("OliPage:form1:pageblock1:Axle7comp1").innerHTML;
    var Axle7comp1VarPreConv = Number(Axle7comp1Var.replace(/[^0-9.-]+/g,""));
    var Axle7comp1VarConverted = Axle7comp1VarPreConv / getConversionRatio(ProductUnit, Comp1Unit, ProductDensityConverted);
    Axle7comp1VarProcessed = Number(Axle7comp1VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp1_unitConverted_A7").value = Axle7comp1VarConverted.toFixed(2);  // added to show covnerted value
    
	//Axle7 Comp2 rates conversion
    Axle7comp2Var = document.getElementById("OliPage:form1:pageblock1:Axle7comp2").innerHTML;
    var Axle7comp2VarPreConv = Number(Axle7comp2Var.replace(/[^0-9.-]+/g,""));
    var Axle7comp2VarConverted = Axle7comp2VarPreConv / getConversionRatio(ProductUnit, Comp2Unit, ProductDensityConverted);
    Axle7comp2VarProcessed = Number(Axle7comp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp2_unitConverted_A7").value = Axle7comp2VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle7 Comp3 rates conversion
    Axle7comp3Var = document.getElementById("OliPage:form1:pageblock1:Axle7comp3").innerHTML;
    var Axle7comp3VarPreConv = Number(Axle7comp3Var.replace(/[^0-9.-]+/g,""));
    var Axle7comp3VarConverted = Axle7comp3VarPreConv / getConversionRatio(ProductUnit, Comp3Unit, ProductDensityConverted);
    Axle7comp3VarProcessed = Number(Axle7comp3VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp3_unitConverted_A7").value = Axle7comp3VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle7 Comp4 rates conversion
    Axle7comp4Var = document.getElementById("OliPage:form1:pageblock1:Axle7comp4").innerHTML;
    var Axle7comp4VarPreConv = Number(Axle7comp4Var.replace(/[^0-9.-]+/g,""));
    var Axle7comp4VarConverted = Axle7comp4VarPreConv / getConversionRatio(ProductUnit, Comp4Unit, ProductDensityConverted);
    Axle7comp4VarProcessed = Number(Axle7comp4VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp4_unitConverted_A7").value = Axle7comp4VarConverted.toFixed(2);  // added to show covnerted value
     
    //Axle8 Huskysupp1 rates conversion
    Axle8huskysupp1Var = document.getElementById("OliPage:form1:pageblock1:Axle8huskysupp1").innerHTML;
    var Axle8huskysupp1VarPreConv = Number(Axle8huskysupp1Var.replace(/[^0-9.-]+/g,""));
    var Axle8huskysupp1VarConverted = Axle8huskysupp1VarPreConv / getConversionRatio(ProductUnit, Huskysupp1Unit, ProductDensityConverted);
    Axle8huskysupp1VarProcessed = Number(Axle8huskysupp1VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp1_unitConverted_A8").value = Axle8huskysupp1VarConverted.toFixed(2); // added to show covnerted value                
    
    //Axle8 Huskysupp2 rates conversion
    Axle8huskysupp2Var = document.getElementById("OliPage:form1:pageblock1:Axle8huskysupp2").innerHTML;  
    var Axle8huskysupp2VarPreConv = Number(Axle8huskysupp2Var.replace(/[^0-9.-]+/g,""));
    var Axle8huskysupp2VarConverted = Axle8huskysupp2VarPreConv / getConversionRatio(ProductUnit, Huskysupp2Unit, ProductDensityConverted);
    Axle8huskysupp2VarProcessed = Number(Axle8huskysupp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp2_unitConverted_A8").value = Axle8huskysupp2VarConverted.toFixed(2); // added to show covnerted value
    
	//Axle8 Huskysupp3 rates conversion
    Axle8huskysupp3Var = document.getElementById("OliPage:form1:pageblock1:Axle8huskysupp3").innerHTML;
    var Axle8huskysupp3VarPreConv = Number(Axle8huskysupp3Var.replace(/[^0-9.-]+/g,""));
    var Axle8huskysupp3VarConverted = Axle8huskysupp3VarPreConv / getConversionRatio(ProductUnit, Huskysupp3Unit, ProductDensityConverted);
    Axle8huskysupp3VarProcessed = Number(Axle8huskysupp3VarConverted.toFixed(6));
    document.getElementById("ats_freight_huskysupp3_unitConverted_A8").value = Axle8huskysupp3VarConverted.toFixed(2); // added to show covnerted value
    
    //Axle8 Comp1 rates conversion
    Axle8comp1Var = document.getElementById("OliPage:form1:pageblock1:Axle8comp1").innerHTML;
    var Axle8comp1VarPreConv = Number(Axle8comp1Var.replace(/[^0-9.-]+/g,""));
    var Axle8comp1VarConverted = Axle8comp1VarPreConv / getConversionRatio(ProductUnit, Comp1Unit, ProductDensityConverted);
    Axle8comp1VarProcessed = Number(Axle8comp1VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp1_unitConverted_A8").value = Axle8comp1VarConverted.toFixed(2);  // added to show covnerted value
    
	//Axle8 Comp2 rates conversion
    Axle8comp2Var = document.getElementById("OliPage:form1:pageblock1:Axle8comp2").innerHTML;
    var Axle8comp2VarPreConv = Number(Axle8comp2Var.replace(/[^0-9.-]+/g,""));
    var Axle8comp2VarConverted = Axle8comp2VarPreConv / getConversionRatio(ProductUnit, Comp2Unit, ProductDensityConverted);
    Axle8comp2VarProcessed = Number(Axle8comp2VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp2_unitConverted_A8").value = Axle8comp2VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle8 Comp3 rates conversion
    Axle8comp3Var = document.getElementById("OliPage:form1:pageblock1:Axle8comp3").innerHTML;
    var Axle8comp3VarPreConv = Number(Axle8comp3Var.replace(/[^0-9.-]+/g,""));
    var Axle8comp3VarConverted = Axle8comp3VarPreConv / getConversionRatio(ProductUnit, Comp3Unit, ProductDensityConverted);
    Axle8comp3VarProcessed = Number(Axle8comp3VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp3_unitConverted_A8").value = Axle8comp3VarConverted.toFixed(2);  // added to show covnerted value
    
    //Axle8 Comp4 rates conversion
    Axle8comp4Var = document.getElementById("OliPage:form1:pageblock1:Axle8comp4").innerHTML;
    var Axle8comp4VarPreConv = Number(Axle8comp4Var.replace(/[^0-9.-]+/g,""));
    var Axle8comp4VarConverted = Axle8comp4VarPreConv / getConversionRatio(ProductUnit, Comp4Unit, ProductDensityConverted);
    Axle8comp4VarProcessed = Number(Axle8comp4VarConverted.toFixed(6));
    document.getElementById("ats_freight_comp4_unitConverted_A8").value = Axle8comp4VarConverted.toFixed(2);  // added to show covnerted value
    
    //Railcar huskysupp1 rates conversion
    RailCarFeeCurrencyHuskysupp1 = document.getElementById("OliPage:form1:pageblock1:Railcarhuskysupp1").innerHTML;
    var  RailCarFeeCurrencyHuskysupp1PreConv = Number(RailCarFeeCurrencyHuskysupp1.replace(/[^0-9.-]+/g,""));
    var  RailCarFeeCurrencyHuskysupp1Converted = RailCarFeeCurrencyHuskysupp1PreConv / getConversionRatio(ProductUnit, Huskysupp1Unit, ProductDensityConverted);
    RailCarFeeHuskysupp1Processed = Number(RailCarFeeCurrencyHuskysupp1Converted.toFixed(6));
	document.getElementById("ats_freight_huskysupp1_unitConverted_RC").value = RailCarFeeCurrencyHuskysupp1Converted.toFixed(2); // added to show covnerted value

    //Railcar huskysupp2 rates conversion
    RailCarFeeCurrencyHuskysupp2 = document.getElementById("OliPage:form1:pageblock1:Railcarhuskysupp2").innerHTML;
    var  RailCarFeeCurrencyHuskysupp2PreConv = Number(RailCarFeeCurrencyHuskysupp2.replace(/[^0-9.-]+/g,""));
    var  RailCarFeeCurrencyHuskysupp2Converted = RailCarFeeCurrencyHuskysupp2PreConv / getConversionRatio(ProductUnit, Huskysupp2Unit, ProductDensityConverted);
    RailCarFeeHuskysupp2Processed = Number(RailCarFeeCurrencyHuskysupp2Converted.toFixed(6));
    document.getElementById("ats_freight_huskysupp2_unitConverted_RC").value = RailCarFeeCurrencyHuskysupp2Converted.toFixed(2); // added to show covnerted value
    
    //Railcar huskysupp3 rates conversion
    RailCarFeeCurrencyHuskysupp3 = document.getElementById("OliPage:form1:pageblock1:Railcarhuskysupp3").innerHTML;
    var  RailCarFeeCurrencyHuskysupp3PreConv = Number(RailCarFeeCurrencyHuskysupp3.replace(/[^0-9.-]+/g,""));
    var  RailCarFeeCurrencyHuskysupp3Converted = RailCarFeeCurrencyHuskysupp3PreConv / getConversionRatio(ProductUnit, Huskysupp3Unit, ProductDensityConverted);
    RailCarFeeHuskysupp3Processed = Number(RailCarFeeCurrencyHuskysupp3Converted.toFixed(6));
	document.getElementById("ats_freight_huskysupp3_unitConverted_RC").value = RailCarFeeCurrencyHuskysupp3Converted.toFixed(2); // added to show covnerted value
     
    //trigger next function
    FreightToCustomerAxleCalculate();
}

// SHOW SUMMARY TABLE  UPON SELECTED SUPPLIER
function SummaryTableCalculate() {
    
    var SelectedSupplierSelector = document.getElementById("OliPage:form1:pageblock1:SelectedSupplier").value;
    
    switch (true) {
        case SelectedSupplierSelector == 'Husky Supplier 1':
            
            document.getElementById("selected_table_display").value= "Husky Supplier 1";
            SummaryTableCalculateHuskysupp1Launcher();
            break;
        case SelectedSupplierSelector == 'Husky Supplier 2':
            
            document.getElementById("selected_table_display").value= "Husky Supplier 2";
            SummaryTableCalculateHuskysupp2Launcher();
            break;
        case SelectedSupplierSelector == 'Husky Supplier 3':
            
            document.getElementById("selected_table_display").value= "Husky Supplier 3";
            SummaryTableCalculateHuskysupp3Launcher();   
            break;
        case SelectedSupplierSelector == 'Competitor 1':
            
            document.getElementById("selected_table_display").value= "Competitor 1";
            SummaryTableCalculateComp1Launcher();
            break;  
        case SelectedSupplierSelector == 'Competitor 2':
            
            document.getElementById("selected_table_display").value= "Competitor 2";
			SummaryTableCalculateComp2Launcher();    
            break;  
        case SelectedSupplierSelector == 'Competitor 3':
			  
            document.getElementById("selected_table_display").value= "Competitor 3";
            SummaryTableCalculateComp3Launcher();  
            
            break;  
        case SelectedSupplierSelector == 'Competitor 4':
			   
            document.getElementById("selected_table_display").value= "Competitor 4";
            SummaryTableCalculateComp4Launcher(); 
            break;         
    }
    
    ReplaceZerosPricingTable(); // Format values to form without unnecessary zeros or invalid inputs.
}

// FREIGHT TO CUSTOMER CALCULATION
 function FreightToCustomerAxleCalculate(){

    var AxleRateSelector = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;

 //filling Axle values moved to Init function    

if(AxleRateSelector == 'Axle 5 Price') 
    {
        //fill values to the table
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value = Axle5huskysupp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value = Axle5huskysupp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp3").value = Axle5huskysupp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp1").value = Axle5comp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp2").value = Axle5comp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp3").value = Axle5comp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp4").value = Axle5comp4VarProcessed;
        
        //set railcar to 0
		document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp3").value = 0;
    }
     
if(AxleRateSelector == 'Axle 6 Price') 
    {
        //fill values to the table
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value = Axle6huskysupp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value = Axle6huskysupp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp3").value = Axle6huskysupp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp1").value = Axle6comp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp2").value = Axle6comp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp3").value = Axle6comp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp4").value = Axle6comp4VarProcessed;
        
        //set railcar to 0
		document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp3").value = 0;
    }
     
if(AxleRateSelector == 'Axle 7 Price') 
    {
        //fill values to the table
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value = Axle7huskysupp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value = Axle7huskysupp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp3").value = Axle7huskysupp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp1").value = Axle7comp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp2").value = Axle7comp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp3").value = Axle7comp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp4").value = Axle7comp4VarProcessed;
        
        //set railcar to 0
		document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp3").value = 0;
    }
     
if(AxleRateSelector == 'Axle 8 Price') 
    {
        //fill values to the table
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value = Axle8huskysupp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value = Axle8huskysupp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp3").value = Axle8huskysupp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp1").value = Axle8comp1VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp2").value = Axle8comp2VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp3").value = Axle8comp3VarProcessed;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp4").value = Axle8comp4VarProcessed;
        
        //set railcar to 0
		document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp3").value = 0;
    }     

if(AxleRateSelector == 'Railcar') 
    {     
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value = RailCarFeeHuskysupp1Processed;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value = RailCarFeeHuskysupp2Processed;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp3").value = RailCarFeeHuskysupp3Processed;
        
		document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp3").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp3").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp4").value = 0;
    }
     
            if(AxleRateSelector == 'Pickup') 
    {
		document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp3").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp3").value = 0;
        document.getElementById("OliPage:form1:pageblock1:FreighttoCustomercomp4").value = 0;
        
		document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value = 0;
        document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp3").value = 0;
    }

DelivCostHuskySupp1Calculate();
DelivCostHuskySupp2Calculate();
DelivCostHuskySupp3Calculate(); 
DelivCostCOMP1Calculate();  
DelivCostCOMP2Calculate();
DelivCostCOMP3Calculate();  
DelivCostCOMP4Calculate();
} 
// END OF FREIGHT TO CUSTOMER CALCULATION

// helping function for formating numbers
function thousands_separators(num)
  {
    var num_parts = num.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return num_parts.join(".");
  } 


// ceckboxes controller for margin
function CheckMarginCheckboxValue(stayChecked){  
    var marginCheckboxValue = document.getElementById("OliPage:form1:pageblock1:margin1");
    var marginCheckboxPrct = document.getElementById("OliPage:form1:pageblock1:margin1Percentage");    
    if(marginCheckboxValue.checked == true && marginCheckboxValue.name != stayChecked.name)
    {
    	marginCheckboxValue.checked = false;
    }
    if(marginCheckboxPrct.checked == true && marginCheckboxPrct.name != stayChecked.name)
    {
        marginCheckboxPrct.checked = false;
    }     
    if(marginCheckboxValue.checked == false && marginCheckboxValue.name != stayChecked.name)
    {
        marginCheckboxValue.checked = true;
    }
    if(marginCheckboxPrct.checked == false && marginCheckboxPrct.name != stayChecked.name)
    {
        marginCheckboxPrct.checked = true;
    }
    if(marginCheckboxValue.checked == true && marginCheckboxPrct.checked == true && marginCheckboxValue.name != stayChecked.name)
    {
        marginCheckboxValue.checked = false;
        marginCheckboxPrct.checked = true;
    }
    if(marginCheckboxValue.checked == true && marginCheckboxPrct.checked == true && marginCheckboxPrct.name != stayChecked.name)
    {
        marginCheckboxValue.checked = true;
        marginCheckboxPrct.checked = false;
    } 
} 

// ceckboxes controller for salesprice
function CheckSalesPriceCheckboxValue(stayChecked){  
    var salespriceVALcheckbox = document.getElementById("OliPage:form1:pageblock1:salesPrice2");
    var salespriceBIDcheckbox = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2");    
    if(salespriceVALcheckbox.checked == true && salespriceVALcheckbox.name != stayChecked.name)
    {
    	salespriceVALcheckbox.checked = false;
    }
    if(salespriceBIDcheckbox.checked == true && salespriceBIDcheckbox.name != stayChecked.name)
    {
        salespriceBIDcheckbox.checked = false;
    }     
    if(salespriceVALcheckbox.checked == false && salespriceVALcheckbox.name != stayChecked.name)
    {
        salespriceVALcheckbox.checked = true;
    }
    if(salespriceBIDcheckbox.checked == false && salespriceBIDcheckbox.name != stayChecked.name)
    {
        salespriceBIDcheckbox.checked = true;
    }
    if(salespriceVALcheckbox.checked == true && salespriceBIDcheckbox.checked == true && salespriceVALcheckbox.name != stayChecked.name)
    {
        salespriceVALcheckbox.checked = false;
        salespriceBIDcheckbox.checked = true;
    }
    if(salespriceVALcheckbox.checked == true && salespriceBIDcheckbox.checked == true && salespriceBIDcheckbox.name != stayChecked.name)
    {
        salespriceVALcheckbox.checked = true;
        salespriceBIDcheckbox.checked = false;
    } 
    

    //new part
    addSalespriceBIDtoSumTable();

    //trigger recalculation
    SalespriceSelectorHuskysupp1();
    SalespriceSelectorHuskysupp2();
	SalespriceSelectorHuskysupp3();
    
    SalespriceSelectorComp1();
	SalespriceSelectorComp2();
	SalespriceSelectorComp3();
	SalespriceSelectorComp4();

} 


//showhide part at summary table test
function addSalespriceBIDtoSumTable (){
    
    var salespriceVALcheckbox = document.getElementById("OliPage:form1:pageblock1:salesPrice2");
    var salespriceBIDcheckbox = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2");  
    
    if(salespriceVALcheckbox.checked == true)
    {
 	    document.getElementById("showhidethis0").style.display = "none";
        document.getElementById("showhidethis1").style.display = "none";
        document.getElementById("showhidethis2").style.display = "none";
        document.getElementById("showhidethis3").style.display = "none";
        document.getElementById("showhidethis4").style.display = "none";
        document.getElementById("showhidethis5").style.display = "none";
        document.getElementById("showhidethis6").style.display = "none";
        
        document.getElementById("changethis1").className = "outputfields";
        document.getElementById("changethis2").className = "outputfields2";
        document.getElementById("changethis3").className = "outputfields";
        document.getElementById("changethis4").className = "outputfields2";
        document.getElementById("changethis5").className = "outputfields";
        document.getElementById("changethis6").className = "outputfields2";
         //alert("hide");
    } 
    else {
 	    document.getElementById("showhidethis0").style.display = "block";
        document.getElementById("showhidethis1").style.display = "block";
        document.getElementById("showhidethis2").style.display = "block";
        document.getElementById("showhidethis3").style.display = "block";
        document.getElementById("showhidethis4").style.display = "block";
        document.getElementById("showhidethis5").style.display = "block";
        document.getElementById("showhidethis6").style.display = "block";
           //alert("show");

        document.getElementById("changethis1").className = "inputfields";
        document.getElementById("changethis2").className = "inputfields2";
        document.getElementById("changethis3").className = "inputfields";
        document.getElementById("changethis4").className = "inputfields2";
        document.getElementById("changethis5").className = "inputfields";
        document.getElementById("changethis6").className = "inputfields2";
    }
    
} 

// Filling values for further use at Summary table (preview at Opportunity product VF)
function fillSummaryTable(){
    
    
    var DcPU = document.getElementById("supplier1_PU_DC").value.replace(/,/g, '');
    if (isNaN(DcPU)) DcPU = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_delivcost_PU").value = DcPU;
    
    var DcRC = document.getElementById("supplier1_RC_DC").value.replace(/,/g, '');
    if (isNaN(DcRC)) DcRC = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_delivcost_RC").value = DcRC;
    
    var DcA5 = document.getElementById("supplier1_A5_DC").value.replace(/,/g, '');
    if (isNaN(DcA5)) DcA5 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_delivcost_A5").value = DcA5;
    
    var DcA6 = document.getElementById("supplier1_A6_DC").value.replace(/,/g, '');
    if (isNaN(DcA6)) DcA6 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_delivcost_A6").value = DcA6;
    
    var DcA7 = document.getElementById("supplier1_A7_DC").value.replace(/,/g, '');
    if (isNaN(DcA7)) DcA7 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_delivcost_A7").value = DcA7;
    
    var DcA8 = document.getElementById("supplier1_A8_DC").value.replace(/,/g, '');
    if (isNaN(DcA8)) DcA8 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_delivcost_A8").value = DcA8;
    
    
    
    
    var MarPU = document.getElementById("supplier1_PU_Margin").value.replace(/,/g, '');
    if (isNaN(MarPU)) MarPU = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_margin_PU").value = MarPU;
    
	var MarRC = document.getElementById("supplier1_RC_Margin").value.replace(/,/g, '');
    if (isNaN(MarRC)) MarRC = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_margin_RC").value = MarRC;
    
    var MarA5 = document.getElementById("supplier1_A5_Margin").value.replace(/,/g, '');
    if (isNaN(MarA5)) MarA5 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_margin_A5").value = MarA5;
    
    var MarA6 = document.getElementById("supplier1_A6_Margin").value.replace(/,/g, '');
    if (isNaN(MarA6)) MarA6 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_margin_A6").value = MarA6;
    
    var MarA7 = document.getElementById("supplier1_A7_Margin").value.replace(/,/g, '');
    if (isNaN(MarA7)) MarA7 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_margin_A7").value = MarA7;
    
    var MarA8 = document.getElementById("supplier1_A8_Margin").value.replace(/,/g, '');
    if (isNaN(MarA8)) MarA8 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_margin_A8").value = MarA8;
    
    
    
    
    var NbPU = document.getElementById("supplier1_PU_Netback").value.replace(/,/g, '');
    if (isNaN(NbPU)) NbPU = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_netback_PU").value = NbPU;
	
    var NbRC = document.getElementById("supplier1_RC_Netback").value.replace(/,/g, '');
    if (isNaN(NbRC)) NbRC = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_netback_RC").value = NbRC;
    
    var NbA5 = document.getElementById("supplier1_A5_Netback").value.replace(/,/g, '');
    if (isNaN(NbA5)) NbA5 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_netback_A5").value = NbA5;
    
    var NbA6 = document.getElementById("supplier1_A6_Netback").value.replace(/,/g, '');
    if (isNaN(NbA6)) NbA6 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_netback_A6").value = NbA6;
    
    var NbA7 = document.getElementById("supplier1_A7_Netback").value.replace(/,/g, '');
    if (isNaN(NbA7)) NbA7 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_netback_A7").value = NbA7;
    
    var NbA8 = document.getElementById("supplier1_A8_Netback").value.replace(/,/g, '');
    if (isNaN(NbA8)) NbA8 = 0;
    document.getElementById("OliPage:form1:pageblock1:sumtab_netback_A8").value = NbA8;
    
    //fill salesprice at opportunity product VF page according selected axle rate and supplier.
}






