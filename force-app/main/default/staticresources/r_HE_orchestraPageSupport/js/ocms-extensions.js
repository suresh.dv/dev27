/* ocms-extensions.js */

var OCMS;
if (!OCMS) {
    OCMS = {
        version: '2015-09-28'
    };
}

/**
    Do NOT reference any methods or data properties whose name is prefixed with an underscore (e.g. _setCookie) from external code.

    2015-Sep-28 dwl integrated Sean’s additions into this most recent code stream; removed obsolete caret.min.js that was formerly embedded
                    as it is no longer supported and required old jQuery; changed isLengthOkay() to not rely on caret (it was the only
                    OCMS client of caret); fixed long-standing typo for warn when creating dummy console functions for ancient IE's;
                    hacked in a shim for IE8 and debug not co-operating on console.assert()
    2015-Aug-07 sri Added insertAt() to String prototype
    2015-Jun-11 dwl bug fix: .parentElement, not .parent, in _installPageEditorInstructionsExpandy()
    2015-Jun-05 dwl added OCMS.restProxyErrorLogger(); added $.fn.warnIfEmpty(); nodeDescription() now reports on all items, not just first element in collection
    2015-May-19 sri Added OCMS.getISOLangCode() which can be used to convert ocms/sf lang codes to ISO 639-1
    2015-Apr-14 dwl added OCMS._installExpandyNextSupport()
    2015-Apr-09 dwl added OCMS._installPageEditorInstructionsExpandy() inspired by Awesome Daniel; install a simulated $.fn.ocmsDynamicLoadFinished
                    if none present so local development environments can still reference this method; .OCMSTab for easy tab sheet behaviour
    2015-Mar-29 dwl added $.fn.nodeDescription() for debug logging; added simple $.fn.ocmsRequestContent() wrapper
    2015-Mar-01 dwl added experimental OCMS.pageStyleSwapper() and OCMS.installDragHandler(); namespace'd preparePage()
    2014-Dec-09 dwl new $.fn.toHTML() works on entire jQuery collection; innerHTML() redefined in terms of toHTML()
    2014-Nov-27 dwl added $.fn.sbwiLinkifier() based on my misremembering of what $.fn.removeSmallBlockLinks() does (sbwiLinkifier can
                    remove links, but can also add the link around the image); removeSmallBlockLinks() now implemented via sbwiLinkifier;
                    internal documentation; better support in hintText() for recognizing placeholder attribute in non-supporting IE9
    2014-Nov-11 dwl added date-based OCMS.version property; enhanced OCMS.lazyLoader(), removed it as 'Experimental', arg API upgrade
    2014-Oct-27 dwl made new stopWatcher utility more opaque and less fussy about parameter order
    2014-Oct-16 dwl update _getURLParameters to return an empty object when query string is empty (vs. {'': ''})
    2014-Oct-07 dwl instead of relying on document.readyState lazyExecutor() uses new private global OCMS._lazyQueueHasBeenExecuted
    2014-Sep-30 dwl rewrite of lazyExecute() and lazyExecutor() to allow for namespaced functions; also lazyExecute() will not
                    invoke functions already executed and lazyExecutor() will immediately invoke if window.readyState is 'complete'
    2014-Sep-29 dwl yetanotherfix in obligatoryBrowserSniffer(), this time for Chrome (yes, browser detection is a pain)
    2014-Sep-18 dwl fix in obligatoryBrowserSniffer() for setting .isIE7 and .isIE8 OCMS properties
    2014-Sep-17 dwl addBrowserClasses() API change! No longer using deprecated $.browser even when available. Some browser class
                    names have changed as a result – see function for details.
    2014-Sep-16 dwl supplantTable() API change! Second parameter is now options, former aliases must be options.aliases
    2014-Sep-10 dwl added nOffsetFromTop parameter to smoothScrollTo() and installSmoothScrollToForInPageLinks(); added
                    smoothScrollToLocationHash()
    2014-Sep-03 dwl correction in handling of target parameter to OCMS.smoothScrollTo()
    2014-Aug-26 dwl set isIE7 and isIE8 vars to false by default (non-fatal oversight from recent agent string parsing changes)
    2014-Aug-19 dwl added OCMS.browserSupportsTransform boolean and renamed internal getURLParameters to include an _ prefix
    2014-Aug-14 dwl added OCMS.browserSupportsTransition boolean
    2014-Jul-21 dwl added toThousandsString() to Number
    2014-Jul-10 dwl renamed lazyExecuter() [not a word] to lazyExecutor() before being released to the wild
    2014-Jul-08 dwl added appendQueryString() to String object
    2014-Jul-04 dwl upgraded supplant to find nested property values via dot notation and to output boolean values too
    2014-Jul-03 dwl added lazyExecuter(), lazyExecute(), and supporting array _lazyExecuteQueue[]
    2014-Jul-02 dwl added lazyLoader() function and lazyLoaded hash structure
    2014-Jun-25 dwl logException() now uses debug instead of console
    2014-Jun-23 dwl hacked debug() source (see below) to eliminate unwanted array notation in output
    2014-Jun-10 dwl added smoothScrollTo() and installSmoothScrollToForInPageLinks(); added OCMS.inContentEditor
                    and corresponding body class .OCMS-ContentEdit
    2014-Jun-06 dwl added _logAjaxRequestStartStops()
    2014-May-14 dwl added updateCMSPageParams()
    2014-May-13 dwl String.supplant() updated to substitute in numbers as well as strings
    2014-Apr-11 dwl buildPageURL() to supercede constructPageURL()... and ServiceAPI's getLinkToPage() would be better still
    2014-Feb-20 dwl OCMS.glassPane.show() more supportive of developmentally challenged old IEs; thanks Stu!
    2013-Nov-14 dwl docReadyBegin() now saves debugLevel session cookie and sets localTesting boolean
    2013-Aug-01 dwl OCMS.addBodyClasses(), and not called until we have a body,
                    mostly experimental waitForTrue() also included in this check-in
    2013-Jul-09 dwl added docReadyBegin(), docReadyEnd(), and logException()
    2013-Jul-08 dwl added nasty browser sniffing variables as workaround for jQuery.browser no
                    longer supported (in 1.9 and greater): OCMS.browserName and OCMS.browserVersion
    2013-Jul-05 dwl partial() now supports model arguments in a position greater than the number of
                    arguments passed to the custom function; hintText family of methods enhanced
                    with new helper method and some renamed methods (old names still supported)
    2013-Apr-25 dwl essential fix within partial()'s original code to prevent it from jumbling the arguments;
                    partial() not suitable for _setCookie() wrapper, defaults() is better
    2013-Apr-22 dwl removed unused parameters from addBrowserClasses(); created preparePage() wrapper
                    to call asap when possible, else via $doc.ready(); internal calls to consoleMsg
                    replaced with ones to debug; included jcaret.min.js for required support of isLengthOkay;
                    acceptChars() support methods updated for the different key event handling model of Firefox
    2013-Apr-15 dwl setting of OCMS.inPageEditor/.inPagePreview moved into our $doc.ready since with
                    Orchestra 5 now uses inline JS to set $doc.data('cms')
    2013-Apr-11 dwl pathToScriptFileResource() became site_prefix aware
    2013-Apr-08 dwl redefine setCookie() to always create site-wide cookies (accidental ommission);
                    and provide new setCookieWithPath()
    2013-Mar-04 dwl fix broken pad, padl, padr
    2013-Feb-12 dwl added OCMS.glassPane with OCMS.$glassPane
    2012-Dec-07 dwl added experimental tracking of IE user agent string when reported as IE7
    2012-Dec-04 dwl added 'nospaces' option to acceptChars(); reinstated missing $.fn.redraw();
                    includes experimental hintTextPreSubmit() to replace erratic hintTextSubmit()
    2012-Nov-28 dwl hintText() now also using placeholder attribute; corrected email validation
                    regex for multiple dots after @
    2012-Nov-22 dwl added $.fn.acceptChars() and its support functions
    2012-Nov-16 dwl moved window.debug to top; added waitForTrue(); added isNumberKey();
                    hacked in something to prevent active tab JS issue Orchestra has in Preview
    2012-Nov-06 dwl now includes debug object to replace console && console.log etc.
    2012-Nov-05 dwl $.fn.hintTextFromValues() moved in from DCN, added optional fourth parameter
                    for domain to setCookie()
    2012-Oct-31 dwl get/set/delete Cookie functions added to OCMS
    2012-Oct-25 dwl fixed bug in $.fn.outerHTML if called against an empty jQuery collection,
                    added Number.plural, added $.fn.removeSmallBlockLinks()
    2012-Aug-21 dwl added $.fn.redraw() from ANZ after similar problem seen for DenMat/IE8
    2012-Aug-13 dwl fixed a bug in pathToScriptFileResource()
    2012-Jun-01 man added $.fn.equalizeHeights()
    2012-May-17 dwl added getMonthName/Abbr() as standalone functions, external to Date object;
                    added getDayName/Abbr() as standalone as well as Date methods
    2012-May-04 dwl added listSplitter()
    2012-Mar-19 dwl comment typos; minor constructPageURL() changes
    2012-Mar-16 dwl consolidation of recent miscellaneous additions
    2012-Mar-07 dwl added outerHTML() to jQuery
    2012-Feb-28 dwl added Myle's context addition to installCharCounter
    2012-Feb-17 dwl function def'ns end with }; for packer compatibility
    2012-Jan-13 dwl fix operator precedance oversight in reporting page context;
                    added pathToScriptFileResource()
    2011-Dec-04 dwl reformatted source; now automatically calls addBrowserClasses and addAuthorClasses
    2011-Dec-03 man fixed a bug in hintText and added support function for submission
    2011-Dec-01 dwl added $.fn.supplantTable()
    2011-Nov-30 dwl added OCMS.urlParameters
    2011-Nov-23 dwl added fn.partial() as a super curry() and fn.defaults()
    2011-Nov-21 dwl .isIE8 joins .isIE7
    2011-Oct-01 dwl always establish properties inPageEditor and inPagePreview, without having to
                    call addAuthorClasses()
    2011-Jul-20 dwl slight upgrade to consoleMsg() for webkit; deserves more attention
    2011-Jul-19 dwl month name support added to Date object
    2011-Jun-21 dwl support for cms api's page_mode
    2011-May-06 dwl cleaning up dmStudio's version of this module that had added OCMS.lastFocus,
                     changed addBrowserClasses(), and added email address support
*/


/*
 * __MODIFIED VERSION__
 *
 * JavaScript Debug - v0.4 - 6/22/2010
 * http://benalman.com/projects/javascript-debug-console-log/
 *
 * Copyright (c) 2010 "Cowboy" Ben Alman
 * Dual licensed under the MIT and GPL licenses.
 * http://benalman.com/about/license/
 *
 * With lots of help from Paul Irish!
 * http://paulirish.com/
 */
// - modified to use Function.prototype.apply.apply(con[level] || con.log, [con, args ]) for non-Firebug output to avoid output always looking like it's printing an array
// - from a comment by Daniel Schaffer found at http://benalman.com/projects/javascript-debug-console-log/
window.debug=(function(){var i=this,b=Array.prototype.slice,d=i.console,h={},f,g,m=9,c=["error","warn","info","debug","log"],l="assert clear count dir dirxml exception group groupCollapsed groupEnd profile profileEnd table time timeEnd trace".split(" "),j=l.length,a=[];while(--j>=0){(function(n){h[n]=function(){m!==0&&d&&d[n]&&d[n].apply(d,arguments)}})(l[j])}j=c.length;while(--j>=0){(function(n,o){h[o]=function(){var q=b.call(arguments),p=[o].concat(q);a.push(p);e(p);if(!d||!k(n)){return}d.firebug?d[o].apply(i,q):Function.prototype.apply.apply(d[o]||d.log,[d,q])}})(j,c[j])}function e(n){if(f&&(g||!d||!d.log)){f.apply(i,n)}}h.setLevel=function(n){m=typeof n==="number"?n:9};function k(n){return m>0?m>n:c.length+m<=n}h.setCallback=function(){var o=b.call(arguments),n=a.length,p=n;f=o.shift()||null;g=typeof o[0]==="boolean"?o.shift():false;p-=typeof o[0]==="number"?o.shift():n;while(p<n){e(a[p++])}};return h})();


/* --- ocms-extensions --- */

try {
    (function ($) {

        // ==== EXTENSIONS TO STANDARD JAVASCRIPT OBJECTS ====

        //helper function for adding methods
        Function.prototype.method = function (name, func) {
            if (!this.prototype[name]) {
                this.prototype[name] = func;
                return this;
            } else {
                debug.warn('can\'t add ' + name + ' method because it already exists');
            }
        };   // method


        // - based on http://ejohn.org/blog/partial-functions-in-javascript/
        // - customFunc = myFunc.partial(undefined, 10, 'yes', undefined);
        //   produces a function whose 2nd and 3rd parameters have *HARD-WIRED* default values
        // - customFunc(a, b) ==> myFunc(a, 10, 'yes', b)
        // - note that customFunc now takes only 2 parameters, not the four required by the base myFunc
        // - customFunc must be called with at least as many parameters as there are undefined's in
        //   the call to partial() that creates customFunc()
        // - noParamsRequired = myFunc.partial(true, 10, 'yes', false);
        // - noParamsRequired() ==> myFunc(true, 10, 'yes', false)
        // - noParamsRequried('hello') ==> myFunc(true, 10, 'yes', false, 'hello')
        Function.method('partial', function () {
            var fn = this,
                modelArgs = Array.prototype.slice.call(arguments),
                modelArgValCount = $.map(modelArgs, function (el) { return el; }).length;

                // modelArgValCount is count of replacement parameters (not undefined (or null))

            return function () {
                var i,
                    arg = 0;

                var theseArgs = [],
                    nTheseArgs = modelArgValCount + arguments.length;
                    // it is non-sensical to have trailing undefined model arguments
                    // but let the modelArgs end with a non-undefined whose position is respected
                    nTheseArgs = Math.max(nTheseArgs, modelArgs.length);

                // without a shadow array of theseArgs for each invocation, we overwrite
                // the original modelArgs array; not good for re-use!
                // this was not present in the referenced resig source
                for (i = 0; i < nTheseArgs; i++ ) {
                    // if argument at creation time, modelArgs[i] was undefined,
                    // use the argument of the invocation
                    if (modelArgs[i] === undefined) {
                        theseArgs[i] = arguments[arg++];
                    } else {
                        theseArgs[i] = modelArgs[i];
                    }
                }

                return fn.apply(this, theseArgs);
            };
        }); // partial


        // provide default argument values, but let them be overridden by the caller
        // customFunc = myFunc.defaults(undefined, 27, 'blue');
        // produces a function whose 2nd and 3rd parameters have default values of 27 and 'blue'
        // customFunc(x, y) ==> myFunc(x, y, 'blue')
        Function.method('defaults', function () {
            var fn = this,
                defArgs = Array.prototype.slice.call(arguments);

            return function () {
                var def = 0,
                    theseArgs = Array.prototype.slice.call(arguments),
                    maxArg = Math.max(defArgs.length, theseArgs.length);

                for (def = 0; def < maxArg; def++) {
                    // if invocation argument is empty, take from the defaults
                    // always let invocation argument override defaults
                    if (theseArgs[def] === undefined) {
                        theseArgs[def] = defArgs[def];
                    }
                }

                return fn.apply(this, theseArgs);
            };
        }); // defaults


        // properly rounds both +ve and -ve numbers
        Number.method('integer', function () {
            return Math[this < 0 ? 'ceil' : 'floor'](this);
        }); // integer


        // nTotalPractices.plural(' practice')  ==> 0 practices
        // nTotalPractices.plural(' practice')  ==> 1 practice
        // nTotalPractices.plural(' practice')  ==> 2 practices
        // nTotalPractices.plural(' practice', 'practii', 'Nothing')  ==> Nothing
        Number.method('plural', function (single, plurals, none) {
            if (this.valueOf() === 0) {
                return none ? none : ('0' + (plurals || (single + 's')));
            } else {
                return this.toString() + (this.valueOf() !== 1 ? (plurals || (single + 's')) : single);
            }
        }); // Number.plural

        /**
         * get thousands-separated string from number, without using tempting, but cross-browser incompatible toLocaleString()
         * based on http://stackoverflow.com/questions/2901102/how-to-print-a-number-with-commas-as-thousands-separators-in-javascript
         * @param  {String} sep thousands separator; optional; defaults to ','
         * @return {String} 1234567.8901.toThousandsString() => '1,234,567.8901'; 1234567.8901.toThousandsString(' ') => '1 234 567.8901'
         */
        Number.method('toThousandsString', function(sep) {
            sep = sep || ',';
            var parts = this.toString().split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, sep);
            return parts.join('.');
        }); // to ThousandsString


        /** removes both leading and trailing spaces
         */
        String.method('trim', function () {
            return this.replace(/^\s+|\s+$/g, '');
        }); // trim


        // not adding this method just yet - do we have a use case for this as a String utility method?
        //
        /** multiple spaces within the text are reduced to a single space
         */
        // String.method('fulltrim', function() {
        //     return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, '').replace(/\s+/g, ' ');
        // });

        /* String padding methods */
        String.method('padr', function(size, c) {
            return this.pad(size, c, 'right');
        });


        String.method('padl', function(size, c) {
            return this.pad(size, c, 'left');
        });


        // Warning: when first added by Myles, padl always returned a 10 character string, and
        // padr didn't work correctly
        // Presumably, this was a requirement for a specific project (Sequoia?), but since that's
        // inappropriate for generic use, that behaviour has since been removed (even though
        // there may be a project out there that assumes the old behaviour)
        // - the original String is unchanged
        // - strings longer than the given pad length return a copy of the original, full string
        String.method('pad', function(size, c, end) {
            end = end || 'right';
            c = c || ' ';           // Covers undefined, null and empty string

            var ret = this;
            if (end == 'left') {
                while (ret.length < size) {
                    ret = c + ret;
                }

                return ret;
                // return ret.slice(-10);
            } else {
                while (ret.length < size) {
                    ret = ret + c;
                }

                return ret;
                // return ret.slice(10);     likely meant to be .slice(0, 10)!
            }
        }); // String.pad


        // template string fill-er in-ner
        // based on source in Crockford's JavaScript: The Good Parts
        // 'Hello {you}'.supplant({you: 'world!'});
        // 'Hello {you.where}'.supplant({you: {where: 'world!'}});
        // 'Hello {1}'.supplant(['or use an array!', 'world!', 'unused']);
        // updated to substitute in numbers and booleans as well as strings
        // ...and to accept dot notation in the template string for object properties within properties
        String.method('supplant', function (o) {
            return this.replace(/{([^{}]*)}/g,
                function (m, k) {
                    var r,
                        keys = k.split('.');

                    r = o[keys.shift()];
                    // while there are more levels to drill into, keep drilling
                    while (keys.length && r !== 'undefined') {
                        r = r[keys.shift()];
                    }
                    return typeof r === 'string' || typeof r === 'number' || typeof r === 'boolean' ? r : m;
                }
            );
        }); // supplant


        /**
         * add query string key-value pair(s) to the given string (non-destructive), with URI component encoding by default
         *
         * @param {tuple}   hash of or more key-value pairs as a JS object property-value tuple; if value is null, just output
         *                  the key name without a value component; if value is undefined, ignore this query parameter entirely
         *
         * @param {ANY} if truthy, do NOT pass values through encodeURIComponent(); do not specify, it's undefined, and encodeURIComponent() is used
         * @return {String} URL string after appending each property from the first parameter
         *
         * @example     location.href = location.href.appendQueryString({reload: true, testing: "why yes!"});
         * @example     newURL = detailURLs['career-search'].appendQueryString({locations: aLocations.toString});
         *
         */
        String.method('appendQueryString', function (tuple, doNotEncode) {
            url = this;
            var hasQ = url.indexOf('?') >= 0;

            for (p in tuple) {
                v = tuple[p];
                // skip query string key if value is undefined
                if (typeof v !== 'undefined') {
                    // if v is null, just include the key name (p), no '=value' required
                    url += (hasQ ? '&' : '?') + p + (v === null ? '' : '=' + (doNotEncode ? v : encodeURIComponent(v)));
                    hasQ = true;
                }
            }

            return url;
        });


        /**
         * Handy function for inserting strings into other strings, should already be a part of JavaScript but isn't?
         *
         *  Use:
         *  var foobar = "david has apples";
         *  var i = foobar.indexOf('apples');
         *  foobar.insertAt(i, '6 ');
         *
         *  console.log(foobar);
         *  "david has 6 apples"
         *
         * @param {integer} index - The position in the string that you want to insert at
         * @param {string} string - The string you wish to insert
         * @return {string} - return the newly formed string with the second parameter inserted
         */
        String.method('insertAt', function(index, string) {
            return this.substr(0, index) + string + this.substr(index);
        });


        // Shorthand methods for adding to specific components of dates
        Date.method('addFullYears', function(n) {
            this.setFullYear(this.getFullYear() + parseInt(n));
        });  // Date.addFullYears


        Date.method('addMonths', function(n) {
            this.setMonth(this.getMonth() + parseInt(n));
        });  // addMonths


        Date.method('addDates', function(n) {
            this.setDate(this.getDate() + parseInt(n));
        });  // addDates


        Date.method('addHours', function(n) {
            this.setHours(this.getHours() + parseInt(n));
        });  // addHours


        Date.method('addMinutes', function(n) {
            this.setMinutes(this.getMinutes() + parseInt(n));
        });  // addMinutes


        Date.method('addSeconds', function(n) {
            this.setSeconds(this.getSeconds() + parseInt(n));
        });  // addSeconds


        Date.method('addMilliseconds', function(n) {
            this.setMilliseconds(this.getMilliseconds() + parseInt(n));
        });  // addMilliseconds


        // currently only concerned with English
        OCMS.monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];

        // currently only concerned with English
        OCMS.dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday',
            'Thursday', 'Friday', 'Saturday'];


        // dateVar.getMonthName()
        Date.method('getMonthName', function () {
            return OCMS.monthNames[this.getMonth()];
        }); // getMonthName


        Date.method('getMonthNameAbbr', function () {
            return this.getMonthName().slice(0, 3);
        }); // getMonthNameAbbr


        // OMCS.getMonthName(oneBasedMonthNumber)
        OCMS.getMonthName = function (month1) {
            // input not bullet-proof
            return OCMS.monthNames[month1 - 1];
        };


        OCMS.getMonthAbbr = function (month1) {
            // input not bullet-proof
            return OCMS.monthNames[month1 - 1].slice(0, 3);
        };


        Date.method('getDayName', function () {
            return OCMS.dayNames[this.getDay()];
        }); // getMonthName


        Date.method('getDayNameAbbr', function () {
            return this.getDayName().slice(0, 3);
        }); // getMonthNameAbbr


        OCMS.getDayName = function (day1) {
            // input not bullet-proof
            return OCMS.dayNames[day1 - 1];
        };


        OCMS.getDayNameAbbr = function (day1) {
            // input not bullet-proof
            return OCMS.dayNames[day1 - 1].slice(0, 3);
        };


        // ==== EXTENSIONS TO JQUERY ====

        // - you may choose to call these when fading element has alpha transparency
        // - reverts to show/hide for IE < 9
        // - DEPRECATED, surely
        $.fn.fadIn = function (dur) {
            return this.each(function () {
                // if ($.browser.msie && $.browser.version < 9) {
                if (OCMS.browserName === 'MSIE' && OCMS.browserVersion < 9) {
                    $(this).show();
                } else {
                    $(this).fadeIn(dur);
                }
            });
        };  // $.fn.fadIn


        $.fn.fadOut = function (dur) {
            return this.each(function () {
                // if ($.browser.msie && $.browser.version < 9) {
                if (OCMS.browserName === 'MSIE' && OCMS.browserVersion < 9) {
                    $(this).hide();
                } else {
                    $(this).fadeOut(dur);
                }
            });
        };  // $.fn.fadOut


        // Sets heights of all elements in the collection to be equal to the height
        // of the tallest element in the collection. Takes border and padding into
        // account so the computed height of all elements is equal
        $.fn.equalizeHeights = function() {
            var maxHeight = 0;
            this.each(function() {
                if ($(this).outerHeight() > maxHeight) {
                    maxHeight = $(this).outerHeight();
                }
            });

            this.each(function() {
                $(this).height(maxHeight - ($(this).outerHeight() - $(this).height()));
            });

            return this;
        };  // $.fn.equalizeHeights


       /**************************************************************************
            - simple in-place hint text for input[type='text'], textarea controls
            - the hint text that will be used will come from one of the following sources, in the following priority order:
                - first parameter to hintText()
                - placeholder attribute
                - data-ocmshinttext attribute
                - value attribute
            - if your form has hint text already specified, don’t call this field at a time, but instead
              call hintTextFromValues()
            - see also hintTextPreSubmit() and hintTextPostSubmit()
            - see hintTextInstallSubmitHelper() for submit support but if it is not effective
              in your context, call hintTextPreSubmit() directly before form data is used
       */
       $.fn.hintText = function (txt, className) {
            if (className) {
                debug.warn('hintText: className parameter is deprecated');
            }

            className = className || 'hintText';

            return this.each(function () {
                function toFocus () {
                    if (this.value === $(this).data('ocmshinttext')) {
                        $(this)
                            .val('')
                            .removeClass(className);
                   }
                };  // toFocus


                function toBlur () {
                    if (this.value === '') {
                        $(this)
                            .val($(this).data('ocmshinttext'))
                            .addClass(className);
                   } else if (this.value != $(this).data('ocmshinttext')) {
                       $(this).removeClass(className);
                   } else {
                        // leaving the field and its value matches the hint text
                        // either initial conditions, or smartass user
                        $(this).addClass(className);
                   }
                };  // toBlur


                // txt need not be provided as long as data-ocmshinttext is, but the parameter
                // has priority if both have been specified
                // note: this does not support empty hint text
                // placeholder is an HTML5 attribute supported by IE10, Firefox 12, Chrome 19, Safari 5
                // it persists in a focus'd text box until its not empty
                // SHOULD hintText disable itself if placeholder is supported?
                // in IE9, this.placeholder fails but $(this),attr('placeholder') succeeds
                txt = txt || $(this).attr('placeholder') || $(this).data('ocmshinttext') || this.value;
                if (txt) {
                    if (OCMS.browserSupportsPlaceholder) {
                        if (!this.placeholder) {
                            this.placeholder = txt;
                            // assume we're called during setup, therefore value is expendable
                            this.value = '';
                        }   // else... we've got nothing to do; let placeholder do everything
                    } else {
                        // looking at you IE < 10 and other dinosaurs
                        $(this)
                            .data('ocmshinttext', txt)  // should be superceded by placeholder attribute
                            .focus(toFocus)
                            .blur(toBlur)
                            .attr('placeholder', txt);

                        // establish initial conditions
                        toBlur.apply(this);
                    }
                } else {
                    debug.warn('⦻ hint text requested but not provided for field ', $(this).attr('name') || $(this).attr('id'));
                }
           });
       };   // hintText


        /**************************************************************************
           - applied to a form
           - before submission, for all fields that are still marked as showing their
             hint text, it will clear their text and remove the hint text class
           - USE WITH CAUTION! This has been seen to fail in production situations for
             reasons that remain unknown.
           - the alternative is to have your own submit handler that will invoke
             $(theForm).hintTextPreSubmit() instead before sending or preparing the form data
        */
        $.fn.hintTextInstallSubmitHelper = function(className) {

            if (OCMS.browserSupportsPlaceholder) {
                return $(this);
            } else {

                className = className || 'hintText';
                return $(this).each(function() {
                    $(this).submit(function hintTextSubmit_clear () {
                        $('.' + className, this)
                            .val('')
                            .removeClass(className);
                       return true;
                   });
               });

            }
        };   // hintTextInstallSubmitHelper


        // support original name
        $.fn.hintTextSubmit = $.fn.hintTextInstallSubmitHelper;


       /**************************************************************************
           - applied to a form
           - call manually JUST BEFORE the form is submitted to remove the hint text values
           - an alternative if hintTextInstallSubmitHelper() is ineffective
        */
        $.fn.hintTextPreSubmit = function (className) {
            if (className) {
                debug.warn('hintTextPreSubmit: className parameter is deprecated');
            }

            if (OCMS.browserSupportsPlaceholder) {
                return $(this);
            } else {
                className = className || 'hintText';
                return $(this).each(function hintTextPreSubmit_clear () {   // for each form in collection (expecting only one!)...
                    $('.' + className, this)
                        .val('')
                        .removeClass(className);
                });
            }
        };   // hintTextPreSubmit


       /**************************************************************************
           - applied to a form
           - call AFTER the form has been submitted to restore hint text to the appropriate fields
        */
        $.fn.hintTextPostSubmit = function(className) {
            if (className) {
                debug.warn('hintTextPostSubmit: className parameter is deprecated');
            }

            if (OCMS.browserSupportsPlaceholder) {
                return $(this);
            } else {
                className = className || 'hintText';
                return $(this).each(function() {
                    $(this).find('input[type="text"], textarea').each(function() {
                        var $in = $(this);
                        if ($in.val() === '' && $in.data('ocmshinttext')) {
                           $in.val($in.data('ocmshinttext')).addClass(className);
                        }
                    });
               });
            }
        };   // hintTextPostSubmit


        // support original name
        $.fn.hintTextDoneSubmit = $.fn.hintTextPostSubmit;


        // - written to install initial field values as hint text (so an OrchestraCMS content editor
        //   may define hint text by setting an initial value which will subsequently be set blank)
        // - since this calls .hintText() on all text boxes and textareas in the jQuery collection
        //   before it gets to the control’s value attribute it it will first look at the placeholder
        //   attribute or a data-ocmshinttext attribute (i.e. don’t be misled by 'Values' in the method name)
        $.fn.hintTextFromValues = function () {
            return $(this).each(function() {
                $('input[type="text"], textarea', this).each(function() {
                    var $txtField = $(this);

                    $txtField.hintText();
                    if (!$txtField.attr('title')) {
                        // placeholder attribute has been added even for browsers that don't support it
                        $txtField.attr('title', $txtField.attr('placeholder'));
                    }
                });
            });
        };  // hintTextFromValues


        // convenience function if your text fields already have initial values that should be used
        // as hint text; and if this method of clearing values before submission works for your form
        $.fn.hintTextFormSetup = function () {
            return $(this).each(function hintTextFormSetup () {
                $(this)
                    .hintTextFromValues()
                    .hintTextInstallSubmitHelper();
            });
        };  // hintTextFormSetup


        // FUTURE: if nMaxChars not given, look for it in a data- attribute
        // element with class .charCount (or provided selector) will be updated with
        // the number of characters remaining before nMaxChars or 500 if none given
        // optional third parameter context may be used to narrow the scope of ccSelector
        $.fn.installCharCounter = function installCharCounter (ccSelector, nMaxChars, context) {
            return this.each(function () {
                ccSelector = ccSelector || '.charCount';
                var $charCount = $(ccSelector, context);

                if ($charCount.length) {
                    nMaxChars = nMaxChars || 500;

                    $(this).keyup(function () {
                        var sStory = $(this).val(),
                            nChars = nMaxChars - sStory.length; // nChars: REMAINING

                        $charCount.text(nChars);
                        if (nChars < 0) {
                            $(this).val(sStory.slice(0, nMaxChars)).scrollTop($(this)[0].scrollHeight);
                            $charCount.text(0);
                        }
                    }).keyup();
                } else {
                    debug.warn('couldn\'t find ' + ccSelector);
                }
            }
        )}; // installCharCounter


        // *****************************************************************************************
        // • The contents of given container is entirely replaced by the provided HTML template, with the content of
        // each of the TH's and TD's within the container available for replacement in to the template
        // through placeholder variables named th1, th2, td1, td2, etc. enclosed by {}.
        // • The table cell variables are indexed using a 1-based system (not 0-based).
        // • Cell attributes are ignored; we're only interested in what's in the table cell.
        // • Note that this could include TH's and TD's from multiple tables, although typically the given
        //   container (the 'this') will be a table, or often a container known to have only one table
        // • the second parameter is optional
        //      - options.aliases: {userLabel: '{td3}', userInput: '{td4}'}
        //      - whereever '{userLabel}' appears in the template, it is replaced with '{td3}' before doing
        // the replacement
        //      - options.replaceElement – if true, the given container is entirely replaced with the
        //        template markup, not just the container’s content
        $.fn.supplantTable = function (template, options) {
            var $this = $(this);

            options = options || {};

            if ($this.length > 1) {
                debug.warn('supplantTable: multiple containers!');
            }

            return $this.each(function () {
                var $ths = $('th', this),
                    $tds = $('td', this),
                    cellMap = {};

                $ths.each(function (nIt, th) {
                    cellMap['th' + (nIt + 1)] = $(th).html();
                });
                $tds.each(function (nIt, td) {
                    cellMap['td' + (nIt + 1)] = $(td).html();
                });

                if (options.aliases) {
                    template = template.supplant(options.aliases);
                }

                if (options.replaceElement) {
                    $(this).after(template.supplant(cellMap)).remove();
                } else {
                    $(this).html(template.supplant(cellMap));
                }
            });
        };  // supplantTable


        // these three password-related detable...() functions are convenience wrappers around
        // supplantTable that provide standard labels as ways to reference the contents of
        // specific table cells

        // jQuery function for converting Change Password ('old user') table to another format
        // warning for caller: remember to preserve div.cperrors
        // sample call: $formTable.detableChangePasswordOldUser('{oldPassword} {newPassword} ...');
        //      this will destroy the targetted table, replacing it with the HTML markup template
        //      specified as the first parameter to detableChangePasswordOldUser()
        $.fn.detableChangePasswordOldUser = $.fn.supplantTable.partial(undefined, {aliases: {
                oldPasswordLabel:       '{td1}',
                oldPassword:            '{td2}',
                newPasswordLabel:       '{td3}',
                newPassword:            '{td4}',
                verifyPasswordLabel:    '{td5}',
                verifyPassword:         '{td6}',
                submit:                 '{td7}'
            }
        });


        // jQuery function for converting Change Password ('new user') table to another format
        // warning for caller: remember to preserve div.cperrors
        $.fn.detableChangePasswordNewUser = $.fn.supplantTable.partial(undefined, {aliases: {
                newPasswordLabel:       '{td1}',
                newPassword:            '{td2}',
                verifyPasswordLabel:    '{td3}',
                verifyPassword:         '{td4}',
                submit:                 '{td5}'
            }
        });


        // jQuery function for converting Forgot Password table to another format
        // warning for caller: remember to preserve div.fperrors
        $.fn.detableForgotPassword = $.fn.supplantTable.partial(undefined, {aliases: {
                userNameLabel:  '{td1}',
                userName:       '{td2}',
                submit:         '{td3}'
            }
        });


        // format modified from as found at http://stackoverflow.com/questions/2419749/get-selected-elements-outer-html
        // @returns {String} a string concatenation of the outer HTML of each element in the collection
        $.fn.toHTML = function () {
            var html = '';

            this.each(function (n, el) {
                if ('outerHTML' in el) {
                    html += el.outerHTML;
                } else {
                    html += el.wrap('<div></div>').parent().html();
                    this.unwrap();
                }
            });

            return html;
        };  // $.fn.toHTML


        /**
         * returns the outerHTML of the first element in the given jQuery collection
         * @return {String} the element’s outer HTML
         */
        $.fn.outerHTML = function () {
            return this.length ? this.eq(0).toHTML() : '';
        };  // $.fn.outerHTML


        /**
         * IE8 sometimes needs to be kicked in the container when content AJAX'd in later has
         * caused a container's height to change and other elements now need to be repositioned
         * @return jQuery
         */
        $.fn.redraw = function () {
            return $(this).each(function () {
                var tn = document.createTextNode(' ');
                this.appendChild(tn);

                setTimeout(function () {
                    tn.parentNode.removeChild(tn);
                }, 0);
            });
        };  // redraw

        /**
         * Small Block with Image Linkifier
         * - remove or add links (anchor elements) for SBwI markup items within the given container
         * - by default (no options):
         *      + removes the anchor surrounding .small-block-text contents
         *      + adds an anchor around .small-block-image > img
         *        (surprisingly, standard SBwI markup does not wrap the image with the link)
         * @param  {Object} options - optional – property list that specifies what SBwI elements should have a link
         *                                       using boolean values for image, title, text, and/or more
         * @return {jQuery}
         */
        $.fn.sbwiLinkifier = function (options) {
            var $container = $(this),
                defaults = {
                    image: true,    // opposite of SBwI markup
                    title: true,
                    text: false,    // opposite of SBwI markup
                    more: true
                },
                aRemovalSelectors = {
                    title:  '.small-block-title > a',
                    text:   '.small-block-text > a',
                    more:   '.small-block-more > a'
                },
                anchorSelector = '',
                what;

            options = $.extend(true, {}, defaults, options);

            for (what in aRemovalSelectors) {
                if (options[what] === false) {
                    anchorSelector += aRemovalSelectors[what] + ', ';
                }
            }
            anchorSelector = anchorSelector.slice(0, -2);   // always remove trailing ', '

            return $container.each(function () {
                // remove specified anchors by replacing them with their contents
                if (anchorSelector) {
                    // container can be an element, valid jQuery selector string, or jQuery collection
                    $container.find(anchorSelector).each(function () {
                        $(this).replaceWith($(this).contents());
                    });
                }

                if (options.image) {
                    // gotta add the link to the img
                    // get the link from the text: even when empty it gets wrapped with an <a>, unlike the more text
                    var href = $(this).find('.small-block-text > a').attr('href');

                    if (href) {
                        $(this).find('.small-block-image > img').wrap('<a href="' + href + '"></a>');
                    }
                }
            });
        };  // sbwiLinkifier


        // - we can now redefine removeSmallBlockLinks() in terms of the more capable sbwiLinkifier()
        // - by default removeSmallBlockLinks() removes all links except the link around the More text
        // (surprisingly, the image never had a link)
        $.fn.removeSmallBlockLinks = $.fn.sbwiLinkifier.defaults({title: false, text: false, more: true});


        $.fn.acceptChars = function (filters, chars) {
            //  supported filters: digits nodigits phone alpha alphanumeric none length n lowercase uppercase
            //  chars is optional and looks like {accept: '...', reject: '....'}
            //  where both accept and reject are optional strings of characters
            //  e.g. $('input').acceptChars('currency', {reject: ' ,()'});  -- no fancy chars for this currency field
            //  e.g. $('input').acceptChars('none', {accept: 'yYnN'});      -- only y's or n's
            //  e.g. $('input').acceptChars('digits', {reject: '0'});       -- 1..9 inclusive

            function keyPressFilter (evt) {
                var accept = true               // individual tests can reject
                    ,filters = evt.data.ff      // an array of functions
                    ,inputObj = this;

                // presence in reject list will avoid further checking
                if (accept && evt.data.reject) {
                    accept = evt.data.reject.indexOf(String.fromCharCode(evt.which)) < 0;
                }

                if (accept) {
                    $.each(filters, function (nIt, fcn) {
                        // if we only need to support only one filter function per acceptChars()
                        // this array of functions to call architecture is not necessary!
                        accept = fcn.call(inputObj, evt);

                        return accept;  // if false, exit each, and we'll reject keypress
                    });
                }

                // presence in accept list can reverse an early rejection
                if (!accept && evt.data.accept) {
                    accept = evt.data.accept.indexOf(String.fromCharCode(evt.which)) >= 0;
                }

                // but the final say goes to length
                if (accept && evt.data.length) {
                    accept = OCMS.isLengthOkay.call(inputObj, evt);
                }

                return accept;
            };  // keyPressFilter


            var aOpt = filters.split(' ')
                ,evtParams = {
                    ff: []
                    ,length: null
                    ,accept: (chars && chars.accept) || null
                    ,reject: (chars && chars.reject) || null
                }
                ,forceCase = null;

            // for each option they specified, push the corresponding function into the array
            // that will be provided to our keyPressFilter() function
            $.each(aOpt, function (nIt, val) {
                switch (val) {
                    case 'digits':
                        evtParams.ff.push(OCMS.isNumberKey);
                        break;

                    case 'nodigits':
                        evtParams.ff.push(OCMS.isNotNumberKey);
                        break;

                    case 'nospaces':
                        evtParams.ff.push(OCMS.isNotSpaceKey);
                        break;

                    case 'phone':
                        evtParams.ff.push(OCMS.isPhoneNumberKey);
                        break;

                    case 'currency':
                        evtParams.ff.push(OCMS.isCurrencyKey);
                        break;

                    case 'alpha':
                        evtParams.ff.push(OCMS.isAlphaKey);
                        break;

                    case 'alphanumeric':
                        evtParams.ff.push(OCMS.isAlphaNumericKey);
                        break;

                    case 'none':    // for use with {accept: '...'} option
                        evtParams.ff.push(OCMS.rejectAllChars);
                        break;

                    case 'length':
                        // do not add this to the function queue since it needs to be be able
                        // to reject an otherwise acceptable char, it's not like the other functions
                        // evtParams.ff.push(OCMS.isLengthOkay);
                        var len = aOpt[nIt + 1];
                        if (len) {
                            evtParams.length = parseInt(len, 10);
                        }
                        break;

                    case 'uppercase':
                    case 'lowercase':
                        // experimental and limited: upper/lowercases on blur and and uses CSS text-transform
                        forceCase = val;
                        break;

                    default:
                        if (aOpt[nIt - 1] !== 'length') {
                            debug.error('unexpected acceptChars option', val);
                        }
                        break;
                }
            });

            return $(this).each(function () {
                if (forceCase === 'uppercase') {
                    $(this).css('text-transform', 'uppercase').blur(OCMS.forceUppercase);
                } else if (forceCase === 'lowercase') {
                    $(this).css('text-transform', 'lowercase').blur(OCMS.forceLowercase);
                }
                $(this).keypress(evtParams, keyPressFilter);
            });
        };  // acceptChars


        $.fn.extend({
            /**
             * Helpful for identifying an element in the debug log; originally intended for a single element (jQuery collection of 1) but
             * will iterate through a multiple element collection.
             * Not chainable.
             * @return {String} e.g. 'DIV.pg', or 'BUTTON#clickMe.submit', or 'DIV.pg, DIV#search_c.inner.sample'
             */
            nodeDescription: function () {
                var descriptions = ['No elements in jQuery collection: ' + this.selector];

                if (this.length) {
                    descriptions = [];          // we won’t be needing the above default description
                    $(this).each(function () {
                        descriptions.push(this.tagName + (this.id ? '#' + this.id : '') + (this.className ? '.' + this.className.replace(/ /g, '.') : ''));
                    });
                }
                return descriptions.join(', ');
            },  // nodeDescription

            /**
             * Writes warning message to console if the jQuery collection has no elements.
             * @param  {String} prefix optional string to precede warning message (start of same line)
             * @return {jQuery}
             */
            warnIfEmpty: function (prefix) {
                var el,
                    desc;

                if (!this.length) {
                    el = this.context;
                    desc = '#document'; // assumed default

                    if (el.tagName) {
                        desc = el.tagName + (el.id ? '#' + el.id : '') + (el.className ? '.' + el.className.replace(/ /g, '.') : '');
                    }
                    debug.warn((prefix || '') + 'No elements in jQuery collection: "' + this.selector + '" within ' + desc);
                }

                return this;    // chainable
            },  // warnIfEmpty

            /**
             * - modest convenience wrapper for calling the content loading function
             * - call this to request content using a specific content loader
             * - invoke against the jQuery container that should receive the content
             * - if not already present, params.$loadDestination is set to $(this)
             *
             * @param {Object} params – loader
             *                        - postLoader
             *                        - other properties as required by loader and/or postLoader
             */
            ocmsRequestContent: function (params) {
                debug.log('–– ocmsRequestContent for: ' + $(this).nodeDescription() + ' with:', params);
                debug.assert(params.loader, '\t- a loader function must be specified');
                debug.assert($.isFunction(params.loader), '\t- loader property must be a function');
                debug.assert(!params.preLoader || params.preLoader && $.isFunction(params.preLoader), '\t- preLoader property must be a function');

                // set this if not already present
                params.$loadDestination = params.$loadDestination || $(this);

                // single container expected, but we’ll follow the classic pattern and
                // call the loader function for each item in the jQuery collection
                return $(this).each(function () {
                    params.preLoader && params.preLoader.call(this, params);
                    params.loader.call(this, params);
                });
            }  // $.fn.ocmsRequestContent
        });


        /**
         * called when the ocmsRequestContent loader is not ready, but we want to fill out event binding code
         * @param  {[type]} what [description]
         * @return {[type]}      [description]
         */
        OCMS.placeholderContentLoader = function (what) {
            what = what || 'unspecified';
            debug.info('Creating placeholder content loader function for ' + what);

            return function _placeholderContentLoader (params) {
                debug.warn('%c Content loader function required for ' + what, 'background-color: red; color: white;');
                debug.warn('\tParameters:', params);
            }
        };  // placeholderContentLoader


        // remove hints intended only for the OCMS page editor from the DOM
        // typically now obsolete since hints should be added to page template with the following:
        //  <apex:outputText rendered="{!api.page_mode == 'edit'}"><div class="ocmsHint">...
        OCMS.removeEditorHints = function () {
            if (!OCMS.inPageEditor) {
                $('div.ocmsHint').remove();
            }
        };


        OCMS.addAuthorClasses = function () {
            var theClass;

            if (OCMS.inPageEditor) {
                theClass = 'OCMS-Edit';
            } else if (OCMS.inPagePreview) {
                theClass = 'OCMS-Preview';
            } else if (OCMS.inContentEditor) {
                theClass = 'OCMS-ContentEdit'
            }

            if (theClass) {
                $('body').addClass(theClass);
            }
        };  // addAuthorClasses


        // Immediately-Invoked Function Expression
        // borrowed from http://stackoverflow.com/questions/5916900/detect-version-of-browser/5918791
        // to replace jQuery's long-deprecated .browser object
        // updated 2014-Aug-19 after learning of IE11’s break from the past
        // if not making obsolete, consider replacing with https://github.com/WhichBrowser/WhichBrowser?
        (function obligatoryBrowserSniffer() {
            var ua = navigator.userAgent,
                tem,
                M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

            OCMS.isIE7 = OCMS.isIE8 = false;

            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                //return 'IE ' + (tem[1] || '');

                OCMS.browserName = 'MSIE';  // M[0];
                OCMS.browserVersion = tem[1] || '';
            } else if (M[1] === 'Chrome') {
                tem = ua.match(/\bOPR\/(\d+)/)
                if (tem != null) {
                    OCMS.browserName = 'OPERA';  // M[0];
                    OCMS.browserVersion = tem[1];
                    //return 'Opera ' + tem[1];
                } else {
                    OCMS.browserName = M[1];
                    OCMS.browserVersion = M[2];
                }
            } else {
                M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];

                if ((tem = ua.match(/version\/(\d+)/i)) != null) {
                    M.splice(1, 1, tem[1]);
                }

                OCMS.browserName = M[0];
                OCMS.browserVersion = M[1];
            }

            if (OCMS.browserName === 'MSIE') {
                OCMS.isIE7 = parseInt(OCMS.browserVersion, 10) === 7;
                OCMS.isIE8 = parseInt(OCMS.browserVersion, 10) === 8;
            }

            return M.join(' ');
        })();


        // ************************************************************************
        // OCMS.addBrowserClasses
        // - Note: Browser sniffing is generally a bad approach
        //
        // - add a browser-identifying class name, always to body
        // - when doing IE, always adds IEn class as well
        // - most frequently we only care about the travesty that is IE7 (or IE8 (or somewhat IE9 (or sometimes IE10)))
        // - use IE conditional comments instead of this JavaScript-based solution when possible
        // - for more detailed browser/version sniffing if necessary, do it yourself, use $.support
        //      (e.g. jQuery.support.boxModel) or feature detection techniques a la Modernizr
        //      - note that the OCMS global already includes boolean values for .supportsPlaceholder, .supportsTransition, and .supportsTransform
        // @param       none
        // @returns     undefined
        OCMS.addBrowserClasses = function () {
            var classList = 'UNKNOWN';      // generic/'unknown' browser

            // no longer using $.browser, even if it’s still available
            // jQuery.browser removed in v1.9
            // if ($.browser && false) {
            //     if ($.browser.msie) {
            //         classList = 'IE IE' + parseInt($.browser.version, 10);
            //     } else if ($.browser.webkit) {
            //         classList = 'WEBKIT';
            //     } else if ($.browser.mozilla) {
            //         classList = 'MOZILLA';
            //     } else if ($.browser.opera) {
            //         classList = 'OPERA';
            //     }
            // } else {

            // compared to the old way which used $.browser…
            // note: WEBKIT replaced by SAFARI or CHROME or other
            // note: FIREFOX replaces MOZILLA
            classList = OCMS.browserName.toUpperCase();
            if (classList === 'MSIE') {
                // leave browserName as MSIE, but normalize on previous convention for body class
                classList = 'IE IE' + OCMS.browserVersion;
            }
            // }

            $('body').addClass(classList);

            // experiment towards detection of the evil Compatibility View
            if (OCMS.isIE7) {
                var ua = navigator.userAgent,
                    ie = null;

                // Trident absent from IE7's userAgent string, it seems
                // IE 8  => Mozilla/4.0 | 5.0 or Trident/4.0
                // IE 9  => Mozilla/5.0 or Trident/5.0
                // IE 10 => 10Trident/6.0
                if (ua.indexOf('Trident/4') > 0) {
                    ie = 'IE 8';
                } else if (ua.indexOf('Trident/5') > 0) {
                    ie = 'IE 9';
                } else if (ua.indexOf('Trident/6') > 0) {
                    ie = 'IE 10';
                } else if (ua.indexOf('Trident/7') > 0) {
                    ie = 'IE 11';
                }

                if (ie) {
                    $('body').addClass('IECV');     // as in, maybe by version 105 it won't suck
                }
                debug.info((ie || 'IE unknown') + ' slumming as IE 7: ' + navigator.userAgent);
            }
        }; // OCMS.addBrowserClasses


        OCMS.addBodyClasses = function () {
            if ($('body').length) {
                OCMS.addAuthorClasses();        // e.g. body.OCMS-Edit
                OCMS.addBrowserClasses();       // e.g. body.WEBKIT, or body.IE.IE8
            } else {
                // unexpected, since preferred placement of scripts is at the bottom
                debug.debug('❧ addBodyClasses() was called before there\'s a body!');
                $(document).ready(OCMS.addBodyClasses);
            }
        };  // OCMS.addBodyClasses


        // candidate regexp's
        // ^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$      -- original: assumes too much about valid chars
        // ^([^.@]+)(\.[^.@]+)*@([^.@]+\.)+([^.@]+)$            -- doesn't sanity check a,b,c in a@b.c
        // ^[^<>\s\@]+(\@[^<>\s\@]+(\.[^<>\s\@]+)+)$            -- forbids spaces, <, > in a,b,c
        // ^[^<>\s\@]+(\@[^<>\s\@\.]+\.([^<>\s\@\.]+)+)$        -- forbids spaces, <, > in a,b,c

        OCMS.isValidEmailAddress = function (el) {
            var reEMail = OCMS.isValidEmailAddress.reEmail_ALTERNATE || OCMS.isValidEmailAddress.reEmail_DEFAULT;

            // if el is null, this: the email text field in question (invoked as a method)
            el = el || this;

            return reEMail.test($(el).val().trim());
        };  // isValidEmailAddress


// prior to Nov-26: incorrectly rejected ant@dcn.com.au
//      OCMS.isValidEmailAddress.reEmail_DEFAULT = /^[^<>\s\@]+\@[^<>\s\@\.]+\.[^<>\s\@\.]+$/;
        OCMS.isValidEmailAddress.reEmail_DEFAULT = /^[^<>\s\@]+\@[^<>\s\@\.]+(\.[^<>\s\@\.]+)+$/;
        OCMS.isValidEmailAddress.reEmail_ALTERNATE = null;


        // pass a regular expression to use a different pattern for email validity checking
        // pass null to reset to default
        OCMS.setEmailAddressRE = function (re) {
            OCMS.isValidEmailAddress.reEmail_ALTERNATE = re;
            debug.info('new email RE: ' + re);
        };  // setEmailAddressRE


        /**************************************************************************
        DEPRECATED -> use debug library instead, e.g. debug.log(), debug.debug(), etc.
        ***************************************************************************/
        OCMS.consoleMsg = function () {
            debug.log(Array.prototype.slice.call(arguments));
        };  // OCMS.consoleMsg


        /**************************************************************************
            - track what control previously had the input focus
            - intentionally ignores command buttons!
        */
        OCMS.trackLastFocus = function () {
            function trackLastFocus (evt) {
                OCMS.lastFocus = this;
                debug.info('focusin ' + this.tagName + this.value);
            }   // trackLastFocus


            if ($.fn.on) {
                $('input[type="text"], input[type="radio"], input[type="checkbox"], select, textarea')
                    .on('focusin', trackLastFocus);
            } else {
                $('body').delegate(
                    'input[type="text"], input[type="radio"], input[type="checkbox"], select, textarea',
                    'focusin',
                    trackLastFocus
                );
            }
        };  // trackLastFocus


        /**************************************************************************
            - written for DenMat.com when standard mouse enter/leave events triggering
                failed or proved unreliable
            - note that parameter is a jQuery collection of arbitrary length, and the elements
                do not have to be visible now (but are expected to not move, shrink, or grow!)
            - returns two methods: isInside and isOutside
        */
        OCMS.boundsChecker = function ($args) {
            var bounds = [];

            function _init () {
                function tlbr (el) {
                    var $this = $(el);

                    // if object isn't currently visible, set nulls but include object
                    // in order to build tlbr co-ordinates later when asked about a point
                    if ($this.is(':visible')) {
                        return {
                            t: $this.offset().top,
                            l: $this.offset().left,
                            b: $this.offset().top + $this.height(),
                            r: $this.offset().left + $this.width()
                        }
                    } else {
                        return {
                            t: null,
                            l: null,
                            b: null,
                            r: null,
                            $that: $this
                        }
                    }
                };   // tlbr


                for (var i = 0; i < $args.length; ++i) {
                    var reck = tlbr($args.eq(i));
                    bounds.push(reck);
                }
            };   // _init


            function isInside (pt) {
                // pt must have pageX and pageY properties, as found in an event object
                var r,
                    bInside = false,
                    rect;

                for (r = 0; !bInside && r < bounds.length; ++r) {
                    rect = bounds[r];

                    if (rect.t !== null) {
                        bInside =   pt.pageX >= rect.l &&
                                    pt.pageX <= rect.r &&
                                    pt.pageY >= rect.t &&
                                    pt.pageY <= rect.b;
                    } else {
                        if (rect.$that.is(':visible')) {
                            rect.t = rect.$that.offset().top;
                            rect.l = rect.$that.offset().left;
                            rect.b = rect.$that.offset().top + rect.$that.height();
                            rect.r = rect.$that.offset().left + rect.$that.width();

                            bInside =   pt.pageX >= rect.l &&
                                        pt.pageX <= rect.r &&
                                        pt.pageY >= rect.t &&
                                        pt.pageY <= rect.b;
                        }
                    }
                }

                return bInside;
            };   // isInside


            function isOutside (args) {
                return !isInside(args);
            };   // isOutside


            function dump () {
                var r;
                for (r = 0; r < bounds.length; ++r) {
                    window.status +=    '{' + bounds[r].l + '->' + bounds[r].r +
                                        '|' + bounds[r].t + 'vv' + bounds[r].b + '} ';
                }
            };   // dump

            _init();

            return {
                    isInside: isInside,
                    isOutside: isOutside
            }
        };   // OCMS.boundsChecker


        OCMS.listSplitter = function (args) {
            function breakAndAppend () {
                // traversing backwards, find an li.newColumn
                // cut li's from there to the end, removing the li.newColumn
                // paste those li's into new list created as new sibling to original list
                // repeat with original list until no .newColumns's encountered
                // note: list__Col0 will always be added to the list
                // sample output:
                //  div.list__Container
                //      ul.list__Col0
                //      ul.list__Col1
                //      ul.list__Col2

                var listTag = this.tagName.toLowerCase();

                if ("ulol".indexOf(listTag) >= 0) {
                    var $originalList = $(this),
                        $listItems = $originalList.find("> li"),
                        originalClass = "",
                        nIt,
                        $newListItems,
                        newList;

                    originalClass = $originalList.attr('class');
                    //debug.debug($listItems.length + ' items to split');

                    if (settings.wrapWithDiv) {
                        $originalList.wrap('<div class="list__Container' +
                            (settings.containerClass ? ' ' + settings.containerClass : '') +
                            '"></div>');
                    }

                    var len = $listItems.length;
                    for (nIt = len - 1; nIt >= 0; --nIt) {
                        $thisItem = $listItems.eq(nIt);
                        if ($thisItem.hasClass('newColumn')) {
                            $newListItems = $thisItem.nextAll();
                            $thisItem.remove();
                            //debug.debug('\tnew column at [' + nIt + '], ' + $newListItems.length + ' items');

                            newList = document.createElement(listTag);
                            // note: prepend performs a move
                            $(newList).prepend($newListItems).addClass(originalClass);
                            $originalList.after(newList);
                        }
                    }

                    $originalList.siblings().andSelf().each(function (nCol) {
                        $(this).addClass('list__Col' + nCol);
                        //debug.debug('\tlist__Col' + nCol);
                    });
                } else {
                    OCMS.consoleLog('ocms-listSplitter: not a list');
                }
            }   // breakAndAppend


            // --- begin listSplitter ---

            var defaults = {
                    selector: null,         // selector is expected to specify one or more ul or ol, NOT their container
                    wrapWithDiv: true,
                    containerClass: ''      // in addition to .list__Container
                },
                settings = {},
                $sourceLists = null;

            // typically, pass one parameter: a string for the ul or ol selector
            if (typeof args === "string") {
                settings = defaults;
                settings.selector = args;
            } else {
                settings = $.extend(true, {}, defaults, args);
            }

            $sourceLists = $(settings.selector);
            if ($sourceLists.length) {
                $sourceLists.each(breakAndAppend);
            }

            return;
        };  // listSplitter


        // - to support both Preview and Production modes, both the page name and its path must be
        //   either provided or derived
        // - if both are not provided, the page name is assumed to be the name of the terminal node in the
        //    page path (e.g. page path = '/hello/doggy' => page name = 'doggy'
        // - page name never includes a /
        // - page path always begins with a / and never includes the site prefix
        // - page arguments: String: will begin with either ? or &; tuples are also supported {a: 1, b: 2, c: 7}
        //      - if String, its value is used as provided, no URI encoding is done by this method
        //      - if tuple, the values WILL be processed by encodeURIComponent()
        //
        //  returns: site-absolute URL: leading /, but no protocal, host, or port

// McGrath testing
// http://dev1-mysandbox-mcgrath.cs5.force.com
// CMS  context, page_context === 'site'
//      page_mode === 'production'
//      isGuest === false
//      user_type === 'Standard'
//
// https://cms.cs5.visual.force.com/apex/main?sname=Development&name=
// CMS  context, page_context === 'intranet'
//      page_mode === 'production'
//      isGuest === true
//      user_type === 'guest'


        OCMS.buildPageURL = function (
            arg1,   // page name or page path
            arg2,   // page path or page name or page arguments
            arg3)   // page arguments (optional)
        {
            console.info('the best way to get a page URL is via the ServiceAPI\'s getLinkToPage()');

            var apiVars = $(document).data('cms') || {},
                pgName = null,
                pgPath = null,
                qString = '',
                char1,
                pageURL = '';

            // reminder - we are permitting the name and path to be specified in either order, and if either
            // is empty, we make a root-level guess at what the other should be
            function parseArgs () {
                debug.assert(arg1, 'expecting at least one argument!');
                debug.assert(typeof arg1 === 'string', 'first argument must be a String');

                if (arg1[0] !== '/') {
                    pgName = arg1;
                } else {
                    pgPath = arg1;  // might be a single / for home page
                }

                if (arg2) {
                    if (typeof arg2 === 'string') {
                        char1 = arg2[0];
                        if (char1 === '/') {
                            pgPath = arg2;
                        } else if (char1 === '?' || char1 === '&') {
                            qString = arg2;
                        } else {
                            pgName = arg2;
                        }
                    } else {
            //             qString = OCMS.queryStringFromTuple(arg2);
                        debug.error('query string cannot yet be specified as a tuple');
                    }
                }

                if (arg3) {
                    if (typeof arg3 === 'string') {
                        qString = arg3;
                    } else {
                        //qString = OCMS.queryStringFromTuple(arg2);
                        debug.error('query string cannot yet be specified as a tuple');
                    }
                }

                // build a default, hopefullly correct page name from path or vice versa if either is still null
                if (pgName === null) {
                    pgName = pgPath.slice(1 + pgPath.lastIndexOf('/'));
                }

                if (pgPath === null) {
                    pgPath = '/' + pgName;
                }
            };  // parseArgs


            // --- begin buildPageURL ---

            parseArgs();

            if (apiVars.page_mode === 'prev') {
                // Preview

                if (qString) {
                    qString = '&' + qString.slice(1);
                }

                pageURL = '/apex/Preview?sname=' + apiVars.site_name + '&name=' + pgName + qString;
            } else {
                // Production

                debug.assert('/' === pgPath[0], 'page path must begin with a /');

                // @!todo   original code assumed page_mode of production implied use path hierarchy URLs, but that’s incorrect
                //          since production pages could still be accessed via /apex/Main?sname=...&name=...
                if (/(\/apex\/Main)/gi.test(location.pathname)) {     // ignore cms__Main possibility
                    // debug.debug('buildPageURL-production: perhaps URL should be ' + '/apex/Main?sname=' + apiVars.site_name + '&name=' + pgName + qString);
                    // debug.debug('\t' + apiVars.context, apiVars.page_context, apiVars);

                    if (qString) {
                        qString = '&' + qString.slice(1);
                    }
                    pageURL = '/apex/Main?sname=' + apiVars.site_name + '&name=' + pgName + qString;
                } else {
                    if (qString) {
                        qString = '?' + qString.slice(1);
                    }
                    // recall that site_prefix, if not empty, begins with a /, e.g. '/Dentipedia'
                    pageURL = (apiVars.site_prefix || '') + pgPath + qString;
                }
            }

            return pageURL;
        };  // buildPageURL


        // this is unproven! (not ready for use)
        // returns query string-based relative URL based on either cms__Main or preview for a given page name
        // e.g. cms__Main?sname=Public&name=Hello&otherarg=world, or
        //      preview?sname=TurboCare.com&name=Marketplace
        //
        //  if page begins with /, should we instead assume proper URL and just return that, with any args,
        //  but omitting basePage + siteString + pageString?
        //
        //  NOTE: query string-based URL not compatible with OrchestraCMS "Enable Unique URL" feature!

        OCMS.constructPageURL = function (
            page,       // string: required, NOT expected to include a query string, therefore NOT
                        //          appropriate for processing Orchestra created content links
            args,       // string: optional, omits leading ?, includes embedded & but not a leading &, assumed non-empty string
            site)       // string: optional, seldom required, but its ommission assumes availability of standard api vars
        {
            debug.warn('constructPageURL not certified for production use!', page);
            debug.warn('use buildPageURL() instead, or better yet: ServiceAPI\'s getLinkToPage()');

            var cmsData = $(document).data('cms') || {},
                basePage = 'cms__Main',     // default: production
                pageString,
                siteString = '',
                argString = (args && args.length && typeof args === 'string') ? '&' + args : '';

            if (cmsData.length === 0) {
                debug.warn('missing document cms data!');
            }

            if (typeof site !== 'string') {
                // if site left empty, grab it from in-page cms api vars (assumed to be present!)
                site = cmsData.site_name;
            }
            if (!site || !site.length) {
                debug.warn('empty site name!');
                site = '';
            }
            siteString = '?sname=' + site;

            // remove leading / if present (presumably a URL was passed, not a page name)
            if (page[0] === '/') {
                page = page.slice(1);
            }
            pageString = '&name=' + page;

            if (cmsData.page_mode === 'prev') {
                basePage = 'preview';
            }

            return basePage + siteString + pageString + argString;
        };  // OCMS.constructPageURL


        // set by user JS content like: OCMS.pageOptions({opt1: value1, opt2: value2});
        // retrieved individually by page script OCMS.pageOptions('opt2'); as required
        OCMS.pageOptions = function (arg) {
            if (typeof arg === 'string') {
                // getter
                // returns undefined if not found
                debug.info('OCMS.pageOptions(' + arg + ') => ' + OCMS.pageOptions.options[arg]);
                return OCMS.pageOptions.options[arg];
            } else {
                // setter call; passed an object map of property/value pairs
                debug.info('OCMS.pageOptions [set] ' + arg);
                $.extend(true, OCMS.pageOptions.options, arg);
            }
        };
        OCMS.pageOptions.options = {};


        // returns an object whose properties and values correspond to the query string parameter names and values
        // all values are stored as String objects
        // http://example.com/?test=1&more=true ==> {test: ‘1’, more: ‘true’}
        // never needs to be called by external clients; we call it and results are in OCMS.urlParameters
        OCMS._getURLParameters = function() {
            var q = window.location.search.slice(1),
                nIt,
                keyVal,
                aQueryVars = {};    // Note: it is a property list, but we use array notation to index by string

            // do we actually have a query string part?
            if (q.length) {
                q = q.split('&');   // after this, q is now an array
            }

            // if no query string part, q was left as an empty string and q.length === 0 means this for loop does not start
            for (nIt = 0; nIt < q.length; nIt++) {
                keyVal = q[nIt].split('=');
                aQueryVars[unescape(keyVal[0])] = unescape(keyVal[1] || '');
            }

            return aQueryVars;
        };  // OCMS._getURLParameters


        OCMS.getURLParameters = function() {
            debug.warn('getURLParameters is deprecated and should never have been called from external code (looking at you social feature pack)');
            return OCMS._getURLParameters();
        };  // OCMS.getURLParameters

        /**
         * add or modify a single property within $(document).data('cms')
         *
         * @param  {String} prop – property name
         * @param  {Any} value – the property’s value
         * @return {undefined}
         */
        OCMS.updateCMSPageParams = function(prop, value) {
            var apiVars = $(document).data('cms'),
                pgParams;

            if (apiVars) {
                pgParams = JSON.parse(apiVars.page_params);         // string to object
                pgParams[prop] = value;
                apiVars.page_params = JSON.stringify(pgParams);     // object back to string
            } else {
                debug.error('updateCMSPageParams could not find API variables');
            }
        };  // OCMS.updateCMSPageParams


        OCMS.pathToScriptFileResource = function (fileName) {
            // note: by default, it returns the path to the resource that holds ocms-extensions.js
            // returns: undefined if can't find script; null or script full path if unexpected format
            var fileName = fileName || 'ocms-extensions.js',
                // note: using jQuery Ends With Selector $=
                $thisScript = $('script[src$="' + fileName + '"]'),
                pathStart,
                rPath;

            if ($thisScript.length) {
                rPath = $thisScript.attr('src');    // get full path to the fileName script

                // site prefix is typically null for a custom URL public site,
                // but will be something like '/dcn' if the site is accessed through the native
                // force.com URL, like virtually all portals are
                pathStart = ($(document).data('cms').site_prefix || '') + '/resource/';

                // if rPath does not start with pathStart, and is not simply the fileName argument
                // (as it might be when doing local development) we return what the full path to fileName
                // which is unpredictable: was it in a sub-folder?, what was that called?, how deep?
                if (rPath.slice(0, pathStart.length) === pathStart) {
                    // we're assuming path is of the form [/{site_prefix}]/resource/1234907485/{nameofresourcefile}/etc.../file.js
                    // and we'll take up to and including the named resource file, including the trailing /
                    rPath = rPath.match(pathStart + '[0-9]+/[a-zA-Z0-9_]+/');
                    // if this fails, returns null
                } else if (rPath === fileName) {
                    rPath = '';
                }
            }

            return rPath;
        };  // pathToScriptFileResource


        // utility function used to do the work of both setCookie and setCookieWithPath
        OCMS._setCookie = function (name, value, expiry, domain, bIsRootCookie) {
            // expiry [optional]    numeric - number of days before cookie expires
            //                      Date - expiration date as Date
            //                      string - expiration date as UTC string
            //                      if expiry is empty, it's a session cookie
            // domain [optional]    specify sub-domain-less domain string for cookie to span sub-domains
            // bRootCookie [optional]   truthy value => set a root-level cookie

            var expiryDate,
                expiresAt = '; expires=',
                cookie = name + '=' + escape(value);

            if (expiry) {
                if (typeof expiry === 'number') { // let 0 mean session cookie too
                    expiryDate = new Date();
                    expiryDate.setDate(expiryDate.getDate() + expiry);
                    expiresAt += expiryDate.toUTCString();
                } else if (typeof expiry === 'object') { // assumes Date! (could check)
                    expiresAt += expiry.toUTCString();
                } else if (typeof expiry === 'string') {
                    // looks like IE doesn't like empty expiry's; rather omit it entirely
                    expiresAt += expiry;
                }
            } else {
                // 0, '', and null all go through here as well as undefined
                expiresAt = '';
            }

            cookie += expiresAt + (bIsRootCookie ? '; path=/' : '');
            if (domain) {
                cookie += ';domain=' + domain;
                if (location.href.indexOf(domain) < 0) {
                    debug.error('setCookie\'s domain parameter is not within URL of current page: ' + domain);
                    // ...and setCookie() will fail
                }
            }

            document.cookie = cookie;
        };  // setCookie


        // setCookie() - create a function that always sets the cookie on the root: path=/  so that
        // it is available to all pages within the domain
        OCMS.setCookie = OCMS._setCookie.defaults(undefined, undefined, undefined, undefined, true);

        // setCookieWithPath() - create a function that sets the cookie without specifying the path,
        // therefore cookie defaults to including the path to the current page
        OCMS.setCookieWithPath = OCMS._setCookie.defaults(undefined, undefined, undefined, undefined, false);


        OCMS.getCookie = function (name) {
            var cookieValue;

            $.each(document.cookie.split(';'), function (nIt, thisCookie) {
                var nEq = thisCookie.indexOf('=');
                if (name === thisCookie.slice(0, nEq).trim()) {
                    cookieValue = unescape(thisCookie.slice(nEq + 1));
                    return false;
                }
            });

            return cookieValue;
        };  // getCookie


        // does not yet support deleting non-root cookies!
        OCMS.deleteCookie = function (name, domain) {
            OCMS._setCookie(name, 'deleted', -1, domain, true);
        };  // deleteCookie


        // e.g. from console (when previously it did not exist): debug.setCallback(OCMS.debugCallback)
        OCMS.debugCallback = function (level) {
            var args = Array.prototype.slice.call(arguments, 1);
            $('#debug').length || $('<div id="debug" />').appendTo('body');
            $('<div/>')
                .addClass('debug-' + level)
                .html('[' + level + '] ' + args)
                .appendTo('#debug');
        };  // debugCallback


        // still experimental!
        // - added properties callCount, callStart, and elapsed are only valid for 'running' functions
        // @param fail must include maxCalls and/or maxWait in addition to failureCallback
        // -!! a function should be running in only one waitForTrue() busy loop at a time
        // e.g OCMS.waitForTrue(mySuccess)
        // e.g OCMS.waitForTrue(mySuccess, 150, {failureCallback: myFailure, maxWait: 5000})
        OCMS.waitForTrue = function (func, pause, fail) {
            var doneWaiting = false;

            pause = pause || 400;
            if (!func.callCount) {
                func.callCount = 1;
                func.callStart = new Date();
                func.elapsed = 0;
            } else {
                ++func.callCount;
                func.elapsed = new Date() - func.callStart;
            }

            if (fail) {
                doneWaiting = (fail.maxCalls > 0 && fail.maxCalls < func.callCount) ||
                                (fail.maxWait > 0 && fail.maxWait < func.elapsed);
                if (doneWaiting) {
                    if (fail.failureCallback) {
                        fail.failureCallback.call();
                    } else {
                        debug.error('failure callback omitted for ', func);
                    }
                }
            }

            if (!doneWaiting && !func.call()) {                 // no arguments supported
                setTimeout(function () {
                    OCMS.waitForTrue(func, pause, fail);
                }, pause);
            } else {
                // functions can be re-used; must reset
                func.callCount = null;
                func.callStart = null;
                func.elapsed = null;
            }
        };  // waitForTrue


        // must be on KeyPress, not KeyUp or KeyDown
        OCMS.isNumberKey = function (evt) {
            var charCode = evt.which;
            // always allow 46 = DEL and control codes <= 31
            // '0'..'9' = 48..57

//            debug.log('isNumberKey - evt.keyCode: ' + evt.keyCode , ', evt.which: ' + evt.which , ', evt.metaKey: ' + evt.metaKey);

            return  charCode === 46 || charCode <= 31 || (charCode >= 48 && charCode <= 57) || evt.metaKey;
        };  // isNumberKey


        // must be on KeyPress, not KeyUp or KeyDown
        OCMS.isNotNumberKey = function (evt) {
            var charCode = evt.which;
            return charCode < 48 || charCode > 57 || evt.metaKey
        };  // isNotNumberKey


        // must be on KeyPress, not KeyUp or KeyDown
        OCMS.isNotSpaceKey = function (evt) {
            var charCode = evt.which;
            // always allow 46 = DEL and control codes <= 31
            return  charCode === 46 || charCode <= 31 || charCode !== 32 || evt.metaKey
        };  // isNotSpaceKey


        // must be on KeyPress, not KeyUp or KeyDown
        OCMS.isPhoneNumberKey = function (evt) {
            return OCMS.isNumberKey(evt) || '+- ()x'.indexOf(String.fromCharCode(evt.which)) >= 0;
        };  // isPhoneNumberKey


        OCMS.isCurrencyKey = function (evt) {
            // generic: not localized
            return  OCMS.isNumberKey(evt) || '$+-, ()£€¥¤'.indexOf(String.fromCharCode(evt.which)) >= 0;
        };  // isCurrencyKey


        // must be on KeyPress, not KeyUp or KeyDown
        OCMS.isAlphaKey = function (evt) {
            var charCode = evt.which;
            // always allow 46 = DEL and control codes <= 31
            // 'a'..'z' = 97..122, 'A'..'Z' = 65..90 -- not yet éxtènded ASCII or UnîCøde aware!
            return  charCode === 46 || charCode <= 31 ||
                    ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122)) ||
                    evt.metaKey;
        };  // isAlphaKey


        // must be on KeyPress, not KeyUp or KeyDown
        OCMS.isAlphaNumericKey = function (evt) {
            var charCode = evt.which;
            return  charCode === 46 || charCode <= 31 ||
                    (charCode >= 48 && charCode <= 57) ||
                    ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122)) ||
                    evt.metaKey;
        };  // isAlphaNumericKey


        // must be on KeyPress, not KeyUp or KeyDown
        OCMS.rejectAllChars = function (evt) {
            var charCode = evt.which;
            // always allow 46 = DEL and control codes <= 31
            return charCode === 46 || charCode <= 31 || evt.metaKey;
        };  // rejectAllChars


        OCMS.isLengthOkay = function (evt) {
            // 2015-Sep-28 removed dependency on formerly embedded jcaret.min.js which has been removed
            // Chrome (and apparently Safari and IE) never get here as DEL and arrow keys
            // don't come through KeyPress; jQuery's "normalized" .which values:
            //  BS 8, left 37, right 39, up 38, down 40
            //  chars 0
//             debug.log('isLengthOkay ' + $(this).val().length + 1 + ' <= ' +
//                 evt.data.length , ', evt.keyCode: '
//                 + evt.keyCode, ', evt.which: ' + evt.which);

            var okay = (evt.keyCode <= 40 && evt.keyCode > 0) || evt.metaKey,
                ieRange;

            if (!okay) {
                okay = $(this).val().length + 1 <= evt.data.length;
                if (!okay) {
                    // if there is a selection, return okay == true since new keystroke will replace selection
                    if ('selectionStart' in this) {
                        okay = this.selectionStart != this.selectionEnd;
                    } else {
                        // IE < 9 does not have selectionStart and selectionEnd
                        if (document.selection) {
                            ieRange = document.selection.createRange();
                            okay = ieRange.text.length > 0;
                        } else {
                            // don’t know what browser madness we are in!
                            debug.warn('isLengthOkay not supported for this browser');
                        }
                    }
                }
            }

            return okay;
        };  // isLengthOkay



        OCMS.forceUppercase = function (evt) {
            $(this).val($(this).val().toUpperCase());
        };  // forceUppercase


        OCMS.forceLowercase = function (evt) {
            $(this).val($(this).val().toLowerCase());
        };  // forceLowercase


        OCMS.glassPane = {
            init: function () {
                // we don't do styling - leave that to the site's CSS, for example:
                //  #glassPane {
                //      position: fixed;
                //      display: none;
                //      z-index: 2000;
                //      top: 0;
                //      left: 0;
                //      width: 100%;
                //      height: 100%;
                //      background: black;
                //      opacity: 0.4;
                //      -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
                //      filter: alpha(opacity=40);
                //  }

                OCMS.$glassPane = $('#glassPane');
                if (!OCMS.$glassPane.length) {
                    OCMS.$glassPane = $('<div>').attr('id', 'glassPane');
                    $('body').append(OCMS.$glassPane);
                }
            }   // glassPane.init
            ,show: function () {
                if (!OCMS.$glassPane) {
                    OCMS.glassPane.init();
                }
                var filter = OCMS.$glassPane.css('filter');
                OCMS.$glassPane.fadeIn(function() {
                    // ...or http://stackoverflow.com/questions/11499232/opacity-issue-in-ie8-when-adding-simple-jquery-fadein-fadeout/12902304#12902304
                    if (filter) {
                        OCMS.$glassPane.css('filter', filter);
                    }
                });
            }   // glassPane.show
            ,hide: function () {
                if (OCMS.$glassPane) {
                    if (OCMS.isIE8) {
                        OCMS.$glassPane.hide();
                    } else {
                        OCMS.$glassPane.fadeOut();
                    }
                } else {
                    debug.warn('glassPane.hide without OCMS.$glassPane');
                }
            }   // glassPane.hide
        };   // glassPane


        // EXPERIMENTAL!
        /**
         *  - installs global event handlers for jQuery-initiated AJAX requests that will record in
         *    the console transitions between not waiting and waiting (Start), and waiting and not
         *    waiting (Stop) for any AJAX requests to return - see http://api.jquery.com/ajaxStart/
         *  - note that this can not be used to indicate when multiple AJAX requests are pending
         *    completion at the same time: only the page-wide start waiting and stop waiting
         *    transitions are captured
         *  - uses console.{timeStamp, time, and timeEnd} if available
         *  - uses debug.info() so output requires `APP`.debugLevel 3 or greater to display
         *
         */
        OCMS._logAjaxRequestStartStops = function() {
            var msStart = 0;    // ms since 1970-Jan-01 00:00 UTC

            function ajaxStartWatcher() {
                console.timeStamp && console.timeStamp('ajaxRequest: Start');
                console.time && console.time('ajaxRequest');
                msStart = (new Date()).getTime();
                debug.info('—— ajaxStart —— ' + msStart);
            }

            function ajaxStopWatcher() {
                console.timeStamp && console.timeStamp('ajaxRequest: Stop');
                console.timeEnd && console.timeEnd('ajaxRequest');
                var msStop = (new Date()).getTime();
                debug.info('—— ajaxStop —— ' + msStop, msStart ? msStop - msStart + 'ms' : '');
            }

            $(document)
                .ajaxStart(ajaxStartWatcher)    // note: we may be too late here to catch the _first_ ajaxStart for our page
                .ajaxStop(ajaxStopWatcher);
        }   // _logAjaxRequestStartStops


        /**
         * OCMS.stopWatcher() represents an API for executing JS code in response to the completion of someone else’s AJAX
         * requests, ones for which you may not be able to provide callback functions for, nor be able to watch for explicit
         * events being triggered.
         * The self-executing in-line function createStopWatcher() returns only the method add
         *
         * @return {[type]} [description]
         */
        OCMS.stopWatcher = (function createStopWatcher() {
            var queue = [];

            function _getQueue() {
                return queue;
            }   // _getQueue


            // either arg1 or arg2 may be the argument array or the options object
            // arg1 and arg2 can be no other types, nor the same type
            // both are optional, but arg2 may not follow a falsey arg1
            // type detection code trusts developers enough to only warn, not fully prevent passing of incorrect parameter types
            function add(fcn, arg1, arg2) {
                var fcnArgs = [],
                    options = {
                        runImmediately: true,
                        functionName: ''
                    };  // default options

                //debug.assert($.isFunction(fcn), 'first parameter must be a function');
                if (arg1) {
                    if ($.isArray(arg1)) {
                        fcnArgs = arg1;
                        if (arg2) {
                            $.extend(true, options, arg2);
                            debug.assert(typeof options === 'object', '3rd parameter options must be a property list');
                        }
                    } else {
                        $.extend(true, options, arg1);
                        debug.assert(typeof options === 'object', '2nd parameter options must be a property list');
                        if (arg2) {
                            fcnArgs = arg2;
                            debug.assert($.isArray(arg2), '3rd parameter must be an array when 2nd is options');
                        }
                    }
                }

                var allDone = false,
                    fcnInfo = {
                        fcn: fcn,
                        args: fcnArgs,
                        options: options,
                        info: {
                            startTime: (new Date()).getTime(),  // ms when this function was put on the queue
                            callCount: 0,                       // incremented before each call
                            elapsed: undefined                 // calculated before each call (ms)
                        }
                    };

                if (options.runImmediately) {
                    debug.info('stopWatcher: running function immediately…');
                    allDone = _execute(fcnInfo);
                    // allDone will be true if there’s no need to wait for an AJAX request after all
                }

                if (!allDone) {
                    debug.info('stopWatcher: adding function to queue…');
                    queue.push(fcnInfo);
                }

                if (queue.length === 1) {
                    debug.info('stopWatcher: setting ajaxStop handler');
                    //$(document).ajaxStop(_stopHandler);   if event handler is established this way, unbind doesn't unbind it, at least in jQ1.5.1
                    $(document).bind('ajaxStop', _stopHandler);
                }
            }   // add


            function _execute(fcnInfo) {
                ++fcnInfo.info.callCount;
                fcnInfo.info.elapsed = (new Date()).getTime() - fcnInfo.info.startTime;
                // clone, not modify original!
                var args = JSON.parse(JSON.stringify(fcnInfo.args));
                // always insert info as first argument to the function call
                args.unshift(fcnInfo.info);

                debug.info('stopWatcher: about to execute function ' + fcnInfo.options.functionName, fcnInfo.options.context || '', args, '; callCount: ' + args[0].callCount, '; elaspsed: ' + args[0].elapsed + 'ms');
                var done = fcnInfo.fcn.apply(fcnInfo.options.context || window, args);

                return done;
            }   // _execute


            function _stopHandler() {
                var nIt,
                    nQLength = queue.length;

                debug.info('_stopHandler has ' + nQLength.plural(' item') + ' to process');

                for (nIt = 0; nIt < nQLength;) {
                    if (_execute(queue[nIt])) {
                        // remove from queue if the function call returned a truthy
                        debug.info('\tstopWatcher: done with that function – removing it from queue');
                        queue.splice(nIt, 1);
                        --nQLength;
                    } else {
                        // function call returned false: leave it in the queue, move on to the next one
                        debug.info('\tstopWatcher: not done with that function yet');
                        ++nIt;
                    }
                }   // for

                if (queue.length === 0) {
                    // we’ve removed all functions from queue, so let’s remove our ajaxStop event handler too
                    $(document).unbind('ajaxStop', _stopHandler);
                }
            }   // _stopHandler

            return {
                add: add,
                getQueue: _getQueue   // for debugging purposes only
            }
        })();  // stopWatcher


        /**
         *  Call this if all anchors on the page that begin with # should be given a click handler that
         *  causes the target item to be smoothly scrolled into position
         *
         *  @!todo  support a tuple to hold parameter values
         *
         *  @param  nDuration - optional number of ms for the scrolling to take, defaults to 750
         *  @param  nOffsetFromTop - optional number of px down from the top of the page to scroll to, defaults to 0
         *  @return jQuery collection of anchors whose href begins with '#'
         */
        OCMS.installSmoothScrollToForInPageLinks = function(nDuration, nOffsetFromTop) {
            return $('a[href^="#"]')
                .click(function () {
                    var sTarget = $(this).attr('href'),
                        $target,
                        bHandled = false;

                    if (sTarget !== '#') {
                        $target = $(sTarget);
                        if ($target.length === 0) {
                            // couldn't find a#sTarget; look for <a> with sTarget in name attribute
                            $target = $('a[name=' + sTarget.slice(1) + ']');
                        }
                        if ($target.length) {
                            nDuration = nDuration || 750;
                            $('html, body').animate({scrollTop: $target.offset().top - (nOffsetFromTop || 0)}, nDuration);
                            bHandled = true;   // we've handled the click: return false
                        }

                        // will preventDefault as well as stopPropagation
                    }
                    return !bHandled;
                });
        };  // installSmoothScrollToForInPageLinks


        /**
         *  Perform a smooth vertical scroll to the element specified in the first argument
         *
         *  @param  target - selector string or jQuery collection (expected length of 1)
         *  @param  nDuration - optional number of ms for the scrolling to take, defaults to 750
         *  @param  nOffsetFromTop - number of pixels to position the element below the top of the viewport, defaults to 0
         *  @return jQuery - the target element
         */
        OCMS.smoothScrollTo = function(target, nDuration, nOffsetFromTop) {
            var $target = (typeof target === 'string') ? $(target) : target;

            if ($target.length) {
                nDuration = nDuration || 750;
                $('html, body').animate({scrollTop: $target.offset().top - (nOffsetFromTop || 0)}, nDuration);
            }

            return $target;
        };  // smoothScrollTo


        /**
         * smooth scroll to window.location’s hash 'bookmark' value; assumed to be called in the page’s doc ready function
         *
         * @param  {tuple} options override defaults by providing numeric properties for any of: nDuration (ms to perform the
         *                 smooth scroll); nOffsetFromTop (px to top of page to scroll to); nDelay (ms to wait before doing
         *                 the smooth scroll - seems to be necessary on page load to compensate for default browser positioning)
         * @return this
         */
        OCMS.smoothScrollToLocationHash = function(options) {
            var defaults = {
                nDuration: 750,
                nOffsetFromTop: 0,
                nDelay: 1000
            };

            if (location.hash && location.hash !== '#') {
                if (typeof options === 'object') {
                    $.extend(defaults, options);
                }

                setTimeout(function() {
                    OCMS.smoothScrollTo($(location.hash), defaults.nDuration, defaults.nOffsetFromTop);
                }, defaults.nDelay);
            }

            return this;
        }   // smoothScrollToLocationHash


        OCMS.preparePage = function() {
            // code dependent upon $(document).data('cms')
            // Orchestra 5 sets this by inline JavaScript, as opposed to the inline JS block
            // formerly included at the top of the page template's <head>
            // (an alternative would be to leave it outside doc.ready, but require that this
            // script be included at the bottom of the page -- our preferred practice)
            var cmsInfo = $(document).data('cms'),
                location = window.location.href.toLowerCase();

            OCMS.inContentEditor = location.indexOf('/apex/createcontent') > 0;
            if (cmsInfo && cmsInfo.page_mode) {
                OCMS.inPageEditor = cmsInfo.page_mode === 'edit';
                OCMS.inPagePreview = cmsInfo.page_mode === 'prev';
                // or === 'production'
                // or === ... ?
                debug.info('❧ page_mode: ' + cmsInfo.page_mode);
            } else {
                OCMS.inPageEditor = location.indexOf('/apex/edit?') > 0;
                OCMS.inPagePreview = location.indexOf('/apex/preview?') > 0;
                debug.info('❧ assumed context: ' + (OCMS.inPageEditor ? 'inPageEditor' : (OCMS.inPagePreview ? 'inPagePreview' : (OCMS.inContentEditor) ? 'inContentEditor' : 'other')));
            }

            if (OCMS.inPagePreview) {
                if (typeof window.getCurrentTabId !== 'function') {
                    window.getCurrentTabId = window.tabReceivedFocus = function () {};
                    debug.info('❧ empty stubs provided for missing getCurrentTabId() and tabReceivedFocus()');
                }
            } else if (OCMS.inPageEditor) {
                $(document).ready(function _waitForDOM () {
                    OCMS._installPageEditorInstructionsExpandy()._pageStyleSwapper();
                });
            }

            // we automatically call these: their side-effects are now assumed to be standard
            // ...OCMS-Edit, -Preview, -Production are now standard in OrchestraCMS 6
            OCMS.addBodyClasses();

            // if the page (e.g. content edit page, or any other) is using our simple OCMSTab markup this
            // method will add a click handler on the tab names
            OCMS._installOCMSTabSupport()._installExpandyNextSupport();

            return this;
        }; // preparePage


        /**
         *  When the required markup is found with ocmsHint element this function adds a toggle control and behaviour
         *  for letting the content author show and hide additional instructions about using the page template.
         *
         *  The required pattern that indicates this behaviour should be installed is:
         *      .ocmsHint > .ocmsInstructions.expandy
         *  A .ocmsInstructions as a child of .ocmsHint causes no special action; it is the additional class of 'expandy'
         *  on the .ocmsInstructions that causes the following to happen:
         *      - the class of ocmsHint is added to the .ocmsInstructions.expandy container
         *      - the class ocmsHasHelpInstructions is added to the parent .ocmsHint
         *      - a click handler is installed on the parent .ocmsHint that causes the .expandy container to slideToggle
         *
         *  ocms-reset+support.css includes default styling for:
         *      - .ocmsHint
         *      - .ocmsHint.hasHelpInstructions > strong::after
         *      - .ocmsHint > .ocmsHint.ocmsInstructions
         *      - .ocmsInstructions.expandy
         *
         * @return this
         */
        OCMS._installPageEditorInstructionsExpandy = function () {
            $('.ocmsHint .ocmsInstructions.expandy').addClass('ocmsHint').parent().addClass('ocmsHasHelpInstructions');

            $('.ocmsHint.ocmsHasHelpInstructions > strong').click(function () {
                $('.ocmsInstructions.expandy', this.parentElement).slideToggle();
            });

            return this;
        };  // _installPageEditorInstructionsExpandy


        OCMS._pageStyleSwapper = function() {
            if ($('style.pageStyles').length) {
                debug.log('adding show page styles control…');
                $('body')
                    .prepend('<div class="togglePageStyles"><label for="chkShowPageStyles"><input type="checkbox" id="chkShowPageStyles" checked="checked"> Show page styles</label></div>');

                $('#chkShowPageStyles').change(function _togglePageStyles (evt) {
                    debug.debug(this.value, this.checked);

                    // checked means Show Page Styles
                    if (this.checked) {
                        $('.pageStyles').attr('disabled', '');
                        $('.noPageStyles').attr('disabled', 'disabled');
                        $('body').removeClass('OCMS-NoPageStyles');
                    } else {
                        $('.pageStyles').attr('disabled', 'disabled');
                        $('.noPageStyles').attr('disabled', '');
                        $('body').addClass('OCMS-NoPageStyles');
                    }

                }).change();
                // only checks first stylesheet: <link /> or <style>
                if ($('.pageStyles').data('default') === 'off') {
                    $('#chkShowPageStyles').attr('checked', '').change();
                }

                OCMS.installDragHandler('.togglePageStyles');
            }

            return this;
        };  // _pageStyleSwapper


        /**
         *  Installs a click handler for the tab names within .OCMSTabBar elements.
         *  Required markup for OCMSTabs:
         *      ul.OCMSTabBar
         *          li data-tabsheet="distinctTabName"
         *          li data-tabsheet="anotherDistinctTabName"
         *          …
         *      div.OCMSTabSheet_c
         *          div.OCMSTabSheet distinctTabName
         *          div.OCMSTabSheet anotherDistinctTabName
         *          …
         *
         *  TabBar li’s and the appropriate sheet may have the 'active' class name pre-added as appropriate.
         *  If there is no li.active, the first tab name and sheet will be made active.
         *
         *  @return this
         */
        OCMS._installOCMSTabSupport = function () {
            // add the click handler for the tab names
            $('.OCMSTabBar > li').click(function (evt) {
                $('.OCMSTabBar > li.active').removeClass('active');
                $(this).addClass('active');

                var targetTabSheet = $(this).data('tabsheet');

                // ideally we would know the respective tab sheet heights and animate to their height, but
                // this is fine for now and prevents the jump to the top while tabSheet_c is momentarily empty
                $('.OCMSTabSheet_c').css({height: $('.OCMSTabSheet_c').height()});
                $('.OCMSTabSheet.active')
                    .removeClass('active')
                    .fadeOut(100, function () {
                        $('.OCMSTabSheet.' + targetTabSheet).fadeIn(300, function() {
                            $('.OCMSTabSheet_c').css({height: ''});
                        }).addClass('active');
                    });
            });

            // make the first tab label (and sheet) active if no label is marked as active
            $('.OCMSTabBar').each(function () {
                if (!$('li.active', this)) {
                    $('li:first', this).click();
                }
            });

            return this;
        };  // _installOCMSTabSupport


        /**
         *  - adds 'OCMSExpandyNext' behaviour to all elements with the class name OCMSExpandyNext
         *  - (no protection for running more than once, bad things would result, as affected elements are
         *     not excepted; room for future improvement)
         *  - the DOM element immediately following .OCMSExpandyNext is the container that will be jQuery slideToggle'd
         *  - if the element is a table, it is wrapped in a div and that div is the target of the slideToggle
         *  - the 'next' element will be toggled closed if it has the class OCMSExpandyClosed, otherwise it is left as is
         * @return this
         */
        OCMS._installExpandyNextSupport = function () {
            function _slideToggleNext (evt) {
                $(this).next().slideToggle().end().toggleClass('OCMSNextClosed');
            }

            var $table;

            $('.OCMSExpandyNext').each(function() {
                var $next = $(this).next();

                if ($next.length && $next[0].tagName === 'TABLE') {
                    $table = $next;
                    $next.wrap('<div />');
                    $next = $table.parent();   // the div we just wrapped the table in.

                    if ($table.hasClass('OCMSExpandyOpen')) {
                        $next.addClass('OCMSExpandyOpen');
                    } else if ($table.hasClass('OCMSExpandyClosed')) {
                        $next.addClass('OCMSExpandyClosed');
                    }
                }

                if ($next.hasClass('OCMSExpandyClosed')) {
                    $next.hide();
                    $(this).addClass('OCMSNextClosed');
                }

                $(this).click(_slideToggleNext);
            });

            return this;
        };  // _installExpandyNextSupport


        /**
         * provide simply drag-ability to elements that match the given selector
         * @param  {String} selector jQuery selector string, or jQuery collection; if falsey '.OCMSDraggable' is used
         * @return this
         */
        OCMS.installDragHandler = function(selector) {
            var $dragging = null,
                mouseStart;

            $(document.body).bind('mousemove', function(evt) {
                if ($dragging) {
                    var b4 = $dragging.offset();
                    // debug.debug('left delta: ' + evt.pageX + ' - ' + mouseStart.left + ' = ' + (evt.pageX - mouseStart.left));
                    $dragging.offset({
                        left: b4.left + evt.pageX - mouseStart.left,
                        top: b4.top + evt.pageY - mouseStart.top
                    });

                    mouseStart = {left: evt.pageX, top: evt.pageY};
                }
            });

            $(selector || '.OCMSDraggable').bind('mousedown', function (evt) {
                $dragging = $(this);
                $dragging.addClass('.OCMSIsDragging');
                mouseStart = {left: evt.pageX, top: evt.pageY};
            });

            $(document.body).bind('mouseup', function (evt) {
                $dragging = null;
                $dragging.removeClass('.OCMSIsDragging');
            });

            return this;
        }; // installDragHandler


        // convenience function to write standard info to the console when a solution specific
        // $(document).ready function starts
        // - grabs debug level from query string and assigns to app variable's debugLevel property
        // - for ease of access, stores a reference to $(document).data('cms') in the
        //   app variable's cms property
        // @param   app - global application object, or null for subordinate doc ready's
        // @param   logMsg - optional additional message
        OCMS.docReadyBegin = function (app, logMsg) {
            debug.info('    BEGIN  ' + (logMsg || '') + '   @ ' + Date());
            if (app) {
                var dL = parseInt(OCMS.urlParameters['dL'], 10);
                if (!isNaN(dL)) {
                    app.debugLevel = dL;
                    // set a session cookie so it's still in effect if we click off this page
                    OCMS.setCookie('debugLevel', app.debugLevel);
                } else {
                    // not in URL, see if we've got a session cookie for it
                    dL = parseInt(OCMS.getCookie('debugLevel'), 10);
                    if (!isNaN(dL)) {
                        app.debugLevel = dL;
                    }
                }

                // and if the app object still doesn't have a debugLevel property, we’ll give it one with value 0
                if (typeof app.debugLevel === 'undefined') {
                    app.debugLevel = 0;
                }

                debug.debug('❧ dL' + app.debugLevel);
                debug.setLevel(app.debugLevel);

                if (app.debugLevel >= 3) {
                    OCMS._logAjaxRequestStartStops();  // experimental
                }

                app.cms = $(document).data('cms') || {};

                if (app.localTesting === undefined) {
                    app.localTesting = location.protocol === 'file:' || location.href.indexOf('localhost') >= 0;
                    if (app.localTesting) {
                        debug.info('❧ local testing');
                    }
                }
            } else {
                if (app !== null) {
                    debug.error('application variable required!');
                }
            }
        };  // docReadyBegin


        // convenience function to write standard info to the console when a solution specific
        // $(document).ready function completes
        // @param   app - global application object, or null if called for a subordinate doc ready
        // @param   logMsg - optional additional message
        OCMS.docReadyEnd = function (app, logMsg) {
            debug.info('    END    ' + (logMsg || '') + '   @ ' + Date());
        };  // docReadyEnd


        OCMS.logException = function (ex, logMsg) {
            debug.error((logMsg || '–––– JavaScript Exception'));
            debug.error(ex);    // this call makes ex become expandable in Chrome web inspector
            if (ex.type) {
                debug.error('\t' + ex.type);
            }
            if (ex.message) {
                debug.error('\t' + ex.message);
            }
            if (ex.stack) {
                debug.error('\t' + ex.stack);
            }
            if (ex.arguments && ex.arguments.length) {
                for (var nIt = 0; nIt < ex.arguments.length; ++nIt) {
                    debug.error('\t' + ex.arguments[nIt]);
                }
            }
        };  // logException


        /**
         *  - use LazyLoad library to load a single JS or CSS resource and trigger a custom event when complete
         *  - (note that the interwebs have multiple libraries using similar names;
         *     socfp uses https://github.com/rgrove/lazyload which is no longer being maintained)
         *  - the event name is 'lazyLoaded' and the name and context parameters will be passed to the event handler
         *  - also tracks loaded resources by appending to OCMS.lazyLoaded upon successful loading
         *  - callback functions registered on the returned Deferred object using .done() receive one parameter:
         *      - callbackArgs, typically empty (undefined), a single value, or an array or arguments
         *  - event handlers that trap the 'lazyLoaded' event trigger on body receive three parameters:
         *      - standard jQuery Event (evt.type === 'lazyLoaded')
         *      - name, the colloquial name of the resource, the second parameter to OCMS.lazyLoader
         *      - callbackArgs, typically empty (undefined), a single value, or an array or arguments
         *
         * @param  {String} path - URL of resource; we assume this ends with either '.js' or '.css';
         *                         note that invalid paths are not detected; callbacks and events will be fired as per normal
         * @param  {String} name - colloquial name of resource - REQUIRED
         * @param  {whatever} callbackArgs - optional - typically an Array, but can be anything the event handler and callback functions are prepared for
         * @param  {Object} context - optional - Deferred callbacks will be executed with context as this;
         * @return $.Deferred
         *
         * @!todo explicit support for most recent timestamped resource path?
         *
         */
        OCMS.lazyLoader = function(path, name, callbackArgs, context) {
            var dfd = $.Deferred();

            function _lazyCallBack() {
                debug.info('about to trigger the lazyLoaded event for "' + name + '"');
                OCMS._lazyLoadedList[name] = {name: name, callbackArgs: callbackArgs, context: context, path: path};
                $('body').trigger('lazyLoaded', [name, callbackArgs, context]);
                if (!context) {
                    dfd.resolve(callbackArgs);
                } else {
                    dfd.resolveWith(context, callbackArgs);
                }
            };


            if (typeof LazyLoad === 'undefined') {
                debug.error('LazyLoad library must be loaded before calling OCMS.lazyLoader()');
                debug.error('one source for this library could be {!URLFOR($Resource.ocms_socfp__ocms_core, "lib/lazyload/lazyload.min.js"');
                dfd.reject({errorCode: 424, errorMessage: 'LazyLoad library not present'});
            } else {
                if (!name) {
                    debug.warn('provide a resource name when calling OCMS.lazyLoader');
                    name = path.slice(path.lastIndexOf('/') + 1);
                }

                if (path.slice(-3) === '.js') {
                    // debug.debug('lazyload.js ', path);
                    LazyLoad.js(path, _lazyCallBack);
                } else if (path.slice(-4) === '.css') {
                    // debug.debug('lazyload.css ', path);
                    LazyLoad.css(path, _lazyCallBack);
                } else {
                    debug.error('OCMS.lazyLoader does not know how to load ' + path);
                    dfd.reject({errorCode: 406, errorMessage: 'OCMS.lazyLoader does not know how to load ' + path});
                }
            }

            return dfd;
        }   // lazyLoader

        // internal - troubleshooting use only
        OCMS._lazyLoadedList = {};


        /**
         * Use this method to add a function call to the OCMS._lazyExecuteQueue for later processing,
         * but process it NOW if the document has already loaded.
         * Typical use is
         *  - have content that needs to invoke JS once the page JS environment has been established use
         *    OCMS.lazyExecutor() to queue the required function (by name as string, since it may not exist yet)
         *  - the page’s doc.ready() calls OCMS.lazyExecute() to execute its queued function
         *
         * @param  {String} name – String so that the function doesn’t have to be defined at time of queueing,
         *                         typically includes a dotted namespace, but only one sub-level deep
         * @param  {Array} args – optional arguments to function
         * @param  {Object} context – optional 'this' for function execution; if not specified, window is used
         * @return undefined
         *
         * @!todo investigate is there a use case for context args being a string to access an object that
         *        may not exist just yet, e.g. OCMS.lazyExecutor('MCG.fSetTheTable', [], 'MCG');
         */
        OCMS.lazyExecutor = function(name, args, context) {
            var fcnInfo = {name: name, args: args, context: context, status: 'waiting'};

            if (OCMS._lazyQueueHasBeenExecuted) {
                // immediate execution will only work if the function name includes a namespace or is a global function
                OCMS._lazyExecuteFunction(fcnInfo);
            }

            // always add it to the queue, no matter the doc readiness state
            OCMS._lazyExecuteQueue.push(fcnInfo);
        }   // lazyExecutor


        // we should use a more elegant model to encapsulate these properties
        OCMS._lazyExecuteQueue = [];
        OCMS._lazyQueueHasBeenExecuted = false;


        /**
         * expects a tuple with:
         *
         * @param  {tuple} fcnInfo
         *     {String} name    (required)
         *     {Array}  args    (optional)
         *     {Object} context (optional)
         * @param   {Object} APP namespace object that would contain the function in question (optional - DEPRECATED)
         *
         *  fcnInfo.name - name of function, typically namespaced such as 'STG.fPrepareNewsItem'
         *
         * @return undefined
         */
        OCMS._lazyExecuteFunction = function(fcnInfo, APP) {
                var fName = fcnInfo.name,
                    aMatch = fName.match(/(\w+)\.(\w+)/) || [],
                    f,
                    APP = APP || window;

                // did function name include a dotted namespace?
                if (aMatch.length) {
                    APP = window[aMatch[1]];    // get the namespace global object
                    fName = aMatch[2];
                }

                if (APP && fName) {
                    f = APP[fName];
                    if ($.isFunction(f)) {
                        fcnInfo.status = 'executing';
                        debug.info('lazy executing ' + fName + ' with ' + fcnInfo.args);
                        f.apply(fcnInfo.context || window, fcnInfo.args);
                        fcnInfo.status = 'executed';
                    } else {
                        debug.warn(fName + ' is not a function within: ', APP);
                    }
                } else {
                    // practically could only be an empty APP from a bogus namespace
                    debug.warn('lazyExecuteFunction: empty APP or fName', fcnInfo);
                }
        };  // _lazyExecuteFunction


        /**
         * traverse OCMS.lazyExecuteQuery and invoke the functions named by each member of the
         * queue if they are found within the given application object
         *
         * support up to one optional name space on the function name (e.g. 'MCG.fDoSomethingFunky')
         * @param  {Object} APP - the primary, global application object; if not specified, window is used
         *                  *DEPRECATED*; use namespaced function name instead
         *
         * @return undefined
         */
        OCMS.lazyExecute = function(APP) {
            APP = APP || window;

            if (OCMS._lazyQueueHasBeenExecuted) {
                debug.warn('lazyExecute() being called more than once… are you sure that’s okay?')
            }

            $.each(OCMS._lazyExecuteQueue, function() {
                if (this.status === 'waiting') {
                    OCMS._lazyExecuteFunction(this, APP);
                }
            });

            // record that we have executed what’s in the queue; since we support doing this only once,
            // should there be subsequent additions to the queue they will be executed immediately
            OCMS._lazyQueueHasBeenExecuted = true;
        };  // lazyExecute


        /**
         * Used to convert Salesforce & OrchestraCMS language values to their ISO 639-1 equivalent.
         *
         * @param {string} lang - The language code supplied from OrchestraCMS or Salesforce
         * @return {string} - return the ISO 639-1 equivalent of lang if that value is found in isoLangTable,
         *                      or input lang value if not found (i.e. no indication of lookup failure)
         */

        OCMS.getISOLangCode = function(lang) {
            // Initialize returnCode to input value, if it does not exist in isoLangTable, return the same value supplied.
            var returnCode = lang,
                isoLangTable = {
                    nl_NL: 'nl',
                    es_MX: 'es',
                    zh_TW: 'zh-Hant',
                    zh_CN: 'zh-Hans',
                    en_GB: 'en',
                    en_US: 'en',
                    pt_PT: 'pt',
                    pt_BR: 'pt'
                };

            // If lang is defined in isoLangTable, use that value.
            if (isoLangTable[lang] !== undefined) {
                returnCode = isoLangTable[lang];
            }

            return returnCode;
        }


        /**
         * - provide meaningful information in the console log when an OrchestraCMS AJAX request fails
         * - sample use in your success handler:
         *         if (!status) {
         *             OCMS.restProxyErrorLogger(response, 'fNewsStreamLoadContent: getContentRenderings failed!');
         *         } else {
         *             … process the successful response
         *         }
         * @param  {[type]} response - the response object returned from doAjaxServiceRequest() or doJsonCall()
         * @param  {[type]} msg      - optional String - provide whatever additional context you wish
         * @return undefined
         */
        OCMS.restProxyErrorLogger = function (response, msg) {
            if (msg) {
                debug.error(msg);
            }

            if (response.error) {
                response.error.message && debug.error(response.error.message);
                response.error.type && debug.error(response.error.type);
                response.error.stacktrace && debug.error(response.error.stacktrace);
            } else {
                debug.info(response);
            }
        }   // ajaxErrorLogger


        // ---- begin ocms-extensions ----
        //
        // Note: this code is run automatically upon DOM inclusion,
        // NOT through a $(document).ready(), therefore:
        //      - subsequent JS must not clear body of added classes

        // provide NO-OP functions for when these functions aren't available (you know who you are)
        if (typeof console === 'undefined') {
            console = {
                log:        function () {},
                info:       function () {},
                warn:       function () {},
                assert:     function () {},
                error:      function () {}
            };
        } else {
            if (OCMS.browserName === 'MSIE' && OCMS.browserVersion < 9) {   // hello IE < 9, you Internet terror
                console.assert = function (bool, msg) {
                    if (!bool) {
                        debug.error('ASSERT FAILED - ' + msg);
                    }
                };
                // whenever the debug library tried to pass through an assert call, it failed (Object does not support this property or method)
                // this shuts it up entirely and we hope IE8 disappears off the face of the earth later today
            }
        }

        // we'll call OCMS.preparePage() as soon as possible; depending upon the placement of
        // this script file the cms data var may already be there; if not, we require that
        // it will be present after $doc.ready()
        if ($(document).data('cms')) {
            OCMS.preparePage();
        } else {
            debug.debug('❧ deferring call to OCMS.preparePage() to a document ready');
            $(document).ready(OCMS.preparePage);
        }

        /**
         *  - so local, NON-OrchestraCMS-based development can use the same JS call to $(document).ocmsDynamicLoadFinished()
         *    as if it was operating in an OrchestraCMS/Salesforce environment
         *  - this will also be installed in the Page Editor environment
         */
        if (!$.fn.ocmsDynamicLoadFinished) {
            debug.debug('installing simulated $.fn.ocmsDynamicLoadFinished');
            $.fn.ocmsDynamicLoadFinished = function (func) {
                $(document).ready(function _fakeDynLoadFinished () {
                    debug.log('Simulated ocmsDynamicLoadFinished: callback will execute momentarily…');
                    setTimeout(func, 800);
                })
            }
        }

        // other initialization code

        // public global
        OCMS.urlParameters = OCMS._getURLParameters();

        // public global for OCMS.trackLastFocus
        OCMS.lastFocus = null;

        // ...and let’s hope the browser also supports styling of placeholder!
        OCMS.browserSupportsPlaceholder = typeof $('<input type="text">').get(0).placeholder == 'string';
        OCMS.browserSupportsTransition = typeof (document.body || document.documentElement).style.transition === 'string';
        OCMS.browserSupportsTransform  = typeof (document.body || document.documentElement).style.transform === 'string';
    })(jQuery);
} catch (ex) {
    OCMS.logException(ex, 'ocms-extensions.js');
}