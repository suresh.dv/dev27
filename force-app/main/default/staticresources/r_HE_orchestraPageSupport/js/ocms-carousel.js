/* ocmsCarousel.js */

(function(window, document, $) {
	var W = $(window),
		D = $(document),
		C = $.ocmsCarousel = function($e, options) {
			options = $.extend(true, {}, C.defaults, options);
			options.$container = $e;
			
			if($(options.itemSelector, $e).length == 0) {
				this.success = false;
				this.message = 'No items in carousel.';
				
				return;
			}
		
			if(C.plugins[options.plugin] == undefined) {
				this.success = false;
				this.message = 'Plugin "'+this.options.plugin+'" not found';
				
				return;
			}
			
			var self = this;
			
			this.plugin = $.extend(true, {}, new $.ocmsCarousel.pluginBase(), C.plugins[options.plugin]);
			this.plugin.setOptions(options);
			this.plugin.init();

			this.start = function() { return this.plugin.start(); };
			this.stop = function() { return this.plugin.stop(); };
            this.isRunning = function() { return this.plugin.isRunning(); };
            this.toggle = function() {
                if(self.plugin.isRunning()) {
                    self.plugin.stop();
                    return false;
                } else {
                    self.plugin.start();
                    return true;
                }
            };
			this.next = function() {
                self.plugin.restart();
                return self.plugin.next();
            };
			this.previous = function() {
                self.plugin.restart();
                return self.plugin.previous();
            };
			this.go = function(idx) {
                self.plugin.restart();
                return self.plugin.goTo(idx);
            };

            this.getCurrentSlide = function() { return self.plugin.getSlide(self.plugin.current); };
            this.getCurrentIndex = function() { return self.plugin.current; };
				
			this.success = true;
		};

	$.extend(C, {
		version: '0.1',
		
		defaults: {
			itemSelector: 'li.ocmsCarouselItem',
			plugin: 'slideshow',
			className: 'ocmsCarouselItem',
			duration: 1000,
			auto: {
				autoStart: true,
				frequency: 4000,
				transitionFunction: 'next'
			}
		},
		
		register: function(p) {
			$.ocmsCarousel.plugins[p.name] = p;
		},
		
		plugins: {},
		
		pluginBase: function(e) {
			this.globalInit = function() {
				var $items = this.options.$container.find(this.options.itemSelector);
				var options = this.options;
				$items.each(function(idx) {
					$(this).addClass(options.className+'-'+idx);
				});
				
				this.slideCount = $items.length;
				this.current = 0;
				
				if(this.options.auto.autoStart) {
					this.start();
				}
			};
			
			this.setOptions = function(options) {
				this.options = $.extend(true, {}, this.defaults, options);
			};
			
			this.getAllSlides = function() {
				return this.options.$container.find(this.options.itemSelector);
			};
			
			this.getSlide = function(idx) {
				return this.options.$container.find('.'+this.options.className+'-'+idx);
			};
			
			this.start = function() {
				if(this.timer == undefined) {
					var transitionFunc = this.options.auto.transitionFunction;
					if(typeof transitionFunc != 'function') {
						transitionFunc = this[transitionFunc];
					}
					var plugin = this;
					this.timer = setInterval(function() { transitionFunc.call(plugin); }, this.options.auto.frequency);
				}
			};
			
			this.stop = function() {
				if(this.timer != undefined) {
					clearInterval(this.timer);
				}
				
				delete this.timer;
			};
            
            this.isRunning = function() {
                return (this.timer !== undefined);
            }
            
            this.restart = function() {
                if(this.isRunning()) {
                    this.stop();
                    this.start();
                }
            }
			
			this.next = function() {
				this.goTo('+1');
			};
			
			this.previous = function() {
				this.goTo('-1');
			};
			
			this.goTo = function(idx) {
				var expectedDistance = '';
				var sign = '';
				if(typeof idx == 'string') {
					sign = idx.charAt(0);
					var number = parseInt(idx.substr(1));
					if(sign == '+') {
						idx = this.current + number;
						expectedDistance = number;
					} else if(sign == '-') {
						idx = this.current - number;
						expectedDistance = number;
					}
				}
				
				while(idx < 0) {
					idx += this.slideCount;
				}
				
				idx = idx % this.slideCount;
				
				if(this.current != idx) {
					this.options.$container.trigger('ocmsCarouselBeforeTransition', {from:this.current, to:idx});
				
					this.go(this.current, idx, expectedDistance, sign);
				}
			}
		}
	});
	
	$.fn.ocmsCarousel = function(options) {
		for(var i = 0; i < this.length; i ++) {
			$(this[i]).trigger('ocmsCarouselBeforeInit');
			
			var instance = new C($(this[i]), options);
			$(this[i]).data('instance.ocmsCarousel', instance);
			$(this[i]).trigger('ocmsCarouselAfterInit');
		}
		
		return this;
	};
	
	$.fn.ocmsCarouselInstance = function() {
		return this.length > 0 ? $(this[0]).data('instance.ocmsCarousel') : undefined;
	};
})(window, document, jQuery);

$.ocmsCarousel.register({
	name: 'slideshow',
	
	defaults: {
		minZIndex: 5,
		maxZIndex: 10
	},
	
	init: function() {
		this.globalInit();
		
		this.options.$container.find(this.options.itemSelector)
			.css('z-index', this.options.maxZIndex)
			.not('.'+this.options.className+'-0')
			.css('z-index', this.options.minZIndex)
			.hide();
	},
	
	go: function(from, to) {
		if(from == to) {
			return;
		}
	
		var $from = this.getSlide(from);
		var $to = this.getSlide(to);
		
		if($from.length && $to.length) {
			this.getAllSlides().stop().hide();
			$from.show();
		
			$from.css('z-index', this.options.minZIndex);
			$to.css({'z-index': this.options.maxZIndex,
					'display': 'block',
					'opacity': '0'});
			this.current = to;
			
			$from.trigger('ocmsCarouselBeforeHide', from);
			$to.trigger('ocmsCarouselBeforeShow', to);
			
			var plugin = this;
			$to.animate({opacity: 1.0}, this.options.duration, function() {
				$from.hide();
				$from.trigger('ocmsCarouselAfterHide', from);
				$to.trigger('ocmsCarouselAfterShow', to);
				plugin.options.$container.trigger('ocmsCarouselAfterTransition', {from:from, to:to});
			});
		}
	}
});

$.ocmsCarousel.register({
	name: 'filmstrip',
	
	defaults: {
		visibleItems: 3,
		slideWidth: 150
	},
	
	init: function() {
		this.globalInit();
		
		var width = this.options.slideWidth;

		for(var i = 0; i < this.slideCount; i ++) {
			this.getSlide(i).css('left', (width * i)+'px').data('ocmsCarouselSlideNumber', i);
		}
	},
	
	go: function(from, to, expectedDistance, direction) {
		if(from == to) {
			return;
		}
	
		this.getAllSlides().stop();
	
		var $from = this.getSlide(from);
		var $to = this.getSlide(to);
	
		var leftScrollDistance = from - to;
		var rightScrollDistance = to - from;
		
		while(leftScrollDistance < 0) {
			leftScrollDistance += this.slideCount;
		}
		
		while(rightScrollDistance < 0) {
			rightScrollDistance += this.slideCount;
		}
		
		leftScrollDistance = leftScrollDistance % this.slideCount;
		rightScrollDistance = rightScrollDistance % this.slideCount;
		
		var currentPosition = parseInt($from.css('left'));
		var slideWidth = this.options.slideWidth;
		
		var inc, distance;
		if((leftScrollDistance == expectedDistance && direction == '-') || (!expectedDistance && leftScrollDistance < rightScrollDistance)) {
			inc = -1;
			distance = leftScrollDistance;
		} else {
			inc = 1;
			distance = -rightScrollDistance;
		}
		
		var i;
		for(i = 0; i < this.slideCount; i ++) {
			var newSlide = this.getSlide(i).clone()
				.removeClass(this.options.className+'-'+i)
				.addClass(this.options.className+'-'+i+'dup')
				.insertBefore(this.getSlide(0));
		}
		
		i = from;
		var offset = 0;
		while(i != to) {
			i += inc;
			offset += inc;
			if(i < 0) {
				i += this.slideCount;
			}
			
			i = i % this.slideCount;
			
			this.getClonedSlide(i).css('left', (currentPosition + slideWidth * offset)+'px');
		}
		
		var toPos = parseInt(this.getClonedSlide(to).css('left'));
		for(var i = to; i < to + this.options.visibleItems; i ++) {
			var idx = i % this.slideCount;
			this.getClonedSlide(idx).css('left', (toPos + slideWidth * (i-to))+'px');
		}
		
		var animateDistance = -currentPosition + slideWidth * distance;
		
		this.current = to;
		
		var plugin = this;
		var eventTriggered = false;
		this.getAllSlides().animate({left: '+='+animateDistance}, this.options.duration, function() {
			if(!eventTriggered) {
				plugin.options.$container.trigger('ocmsCarouselAfterTransition', {from:from, to:to});
				eventTriggered = true;
			}
		
			var slideNum = $(this).data('ocmsCarouselSlideNumber');
			if(slideNum != undefined) {
				var $clone = plugin.getClonedSlide(slideNum);
				$(this).css('left', $clone.css('left'));
				$clone.remove();
			}
		});
	},
	
	getClonedSlide: function(idx) {
		return this.options.$container.find('.'+this.options.className+'-'+idx+'dup');
	}
});