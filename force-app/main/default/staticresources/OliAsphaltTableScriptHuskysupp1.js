
var DeliveredCost_Huskysupp1;
var PartialDC_Huskysupp1;
var PartialDCNetback_Huskysupp1;

var RailCarFeeHuskysupp1Processed;
var Axle5huskysupp1VarProcessed;
var Axle6huskysupp1VarProcessed;
var Axle7huskysupp1VarProcessed;
var Axle8huskysupp1VarProcessed;

var MarginHuskysupp1;
var SalesPriceHuskysupp1;
var NetBackLoydminsterHuskysupp1;
var ApproximateMarginHuskysupp1;

// Function for Husky supplier 1 / #1 column at the table / #1 summary table
function DelivCostHuskySupp1Calculate(){    
        
    var ProductCost = document.getElementById("OliPage:form1:pageblock1:ProductCosthuskysupp1").value.replace(/,/g, '');
    var LloydProcessing = document.getElementById("OliPage:form1:pageblock1:LloydProcessinghuskysupp1").value.replace(/,/g, '');
    var FreightToTerminal = document.getElementById("OliPage:form1:pageblock1:Freight_to_terminal_huskysupp1").value.replace(/,/g, '');
    var FreighttoCustomer = document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value.replace(/,/g, '');
    var DifferentialVal = document.getElementById("OliPage:form1:pageblock1:Differential_huskysupp1").value.replace(/,/g, '');
    var MarketingFee = document.getElementById("OliPage:form1:pageblock1:Marketing_Fee_huskysupp1").value.replace(/,/g, '');
    var TerminalThroughputs = document.getElementById("OliPage:form1:pageblock1:Terminal_Throughputs_huskysupp1").value.replace(/,/g, '');
    var ChemicalCosts = document.getElementById("OliPage:form1:pageblock1:Chemical_Costs_huskysupp1").value.replace(/,/g, '');
    var ModificationCosts = document.getElementById("OliPage:form1:pageblock1:Modification_Costs_huskysupp1").value.replace(/,/g, '');
    var Railcarfee = document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value.replace(/,/g, '');
    
    var marginCheckboxPrct = document.getElementById("OliPage:form1:pageblock1:margin1Percentage").checked;
    	
	//Delivered Cost partial calculation for Netback at Summary Table use
 	   PartialDCNetback_Huskysupp1 = parseFloat(FreightToTerminal) + parseFloat(DifferentialVal) + parseFloat(MarketingFee) + parseFloat(TerminalThroughputs) + parseFloat(ChemicalCosts) + parseFloat(ModificationCosts);
    //Delivered Cost partial calculation - For Summary Table use
 	   PartialDC_Huskysupp1 = parseFloat(ProductCost) + parseFloat(LloydProcessing) + parseFloat(PartialDCNetback_Huskysupp1);
    //Delivered Cost calculation
  	  DeliveredCost_Huskysupp1 = parseFloat(PartialDC_Huskysupp1) + parseFloat(FreighttoCustomer) + parseFloat(Railcarfee);
 	   document.getElementById("OliPage:form1:pageblock1:DelivCosthuskysupp1").value = thousands_separators(DeliveredCost_Huskysupp1.toFixed(6));
    
    //Decide how to calculate Margin - as $ value or % according selected checkbox
    // trigger next function:  
   
            if (marginCheckboxPrct)
        {
            MarginPrctHuskySupp1Calculate();
        } 
        else
        {	
            MarginHuskySupp1Calculate();
        }
}
// END OF DELIVERED COST CALCULATION

// MARGIN / get margin value out of delivered cost and margin %
function MarginPrctHuskySupp1Calculate(){ 
    var impt2 = document.getElementById("OliPage:form1:pageblock1:DelivCosthuskysupp1").value.replace(/,/g, '');
    var impt4 = document.getElementById("OliPage:form1:pageblock1:MarginPrcthuskysupp1").value.replace(/,/g, ''); 
        
//	HighlightMssingValues(impt4, 'MarginPrcthuskysupp1', 'inputfields2'); // notifies user if value is not filled in
    
    var impt3 = (parseFloat(impt2) * parseFloat(impt4)) / 100;
        if (isNaN(impt3)) impt3 = 0;   
    document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp1").value = impt3.toFixed(6);
    
    // trigger next function:    
    SetMarginVarhuskysupp1();
}

// MARGIN % / get margin % value out of delivered cost and margin

function MarginHuskySupp1Calculate(){ 
    var impt2 = document.getElementById("OliPage:form1:pageblock1:DelivCosthuskysupp1").value.replace(/,/g, '');
    var impt3 = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp1").value.replace(/,/g, '');
    var impt4 = (parseFloat(impt3) / parseFloat(impt2) * 100);
    
    if (isNaN(impt4)) impt4 = 0;   
    document.getElementById("OliPage:form1:pageblock1:MarginPrcthuskysupp1").value = impt4.toFixed(6); 
    
    // trigger next function:    
    SetMarginVarhuskysupp1();
}

function SetMarginVarhuskysupp1(){
    
    var setmarginvartemp = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp1").value.replace(/[^0-9.-]+/g,"");
    MarginHuskysupp1 = setmarginvartemp;
  //  alert("A" + MarginHuskysupp1);
     // trigger next function:    
    SalespriceSelectorHuskysupp1();
}

function SalespriceSelectorHuskysupp1 (){
//   var SalespriceVAL = document.getElementById("OliPage:form1:pageblock1:salesPrice2").checked;
    var SalespriceBID = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2").checked;

         if (SalespriceBID)
        {
            SalespriceSelectedBIDHuskysupp1();
        } 
    else
        {
            SalespriceSelectedVALHuskysupp1();
        }
    
}
// SALES PRICE CALCULATION
function SalespriceSelectedVALHuskysupp1(){
    var marginHuskysupp1 = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp1").value.replace(/,/g, '');   
    SalesPriceHuskysupp1 = parseFloat(DeliveredCost_Huskysupp1) + parseFloat(marginHuskysupp1);
     if (isNaN(SalesPriceHuskysupp1)) SalesPriceHuskysupp1 = 0;   
    document.getElementById("OliPage:form1:pageblock1:salesPriceHuskysupp1").value = SalesPriceHuskysupp1.toFixed(6);
// trigger next function:    
    NetBackLoydminsterHuskysupp1Calculate();
    //added:
    ApproximateMarginHuskysupp1Calculate();
    USDValuesCalculateHuskysupp1();
    SummaryTableCalculate();
}

function SalespriceSelectedBIDHuskysupp1(){
	var SalespriceBid = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value.replace(/,/g, '');
    SalesPriceHuskysupp1 = SalespriceBid;
    // fill salesprice val field / doesnt enter the calculation here:
    var marginHuskysupp1 = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp1").value.replace(/,/g, '');
    var SalesPriceHuskysupp1DisplayVal = parseFloat(DeliveredCost_Huskysupp1) + parseFloat(marginHuskysupp1);
         if (isNaN(SalesPriceHuskysupp1DisplayVal)) SalesPriceHuskysupp1DisplayVal = 0; 
    document.getElementById("OliPage:form1:pageblock1:salesPriceHuskysupp1").value = SalesPriceHuskysupp1DisplayVal.toFixed(6);   
    
// trigger next function:    
    NetBackLoydminsterHuskysupp1Calculate();   
    //added:
    ApproximateMarginHuskysupp1Calculate();
    USDValuesCalculateHuskysupp1();
    SummaryTableCalculate();
    
}
// SALES PRICE CALCULATION end

// NETBACK LLOYDMINSTER CALCULATION
function NetBackLoydminsterHuskysupp1Calculate(){ 
    
    var FreightToCustomerHuskysupp1Current = document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp1").value;
    var RailcarToHuskysupp1Current = document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp1").value;
    
    NetBackLoydminsterHuskysupp1 = parseFloat(SalesPriceHuskysupp1) - ( parseFloat(RailcarToHuskysupp1Current) + parseFloat(FreightToCustomerHuskysupp1Current) + parseFloat(PartialDCNetback_Huskysupp1));
        if (isNaN(NetBackLoydminsterHuskysupp1)) NetBackLoydminsterHuskysupp1 = 0;   // avoid getting NaN value in case when salesprice BID is activated, and is empty. 
    document.getElementById("OliPage:form1:pageblock1:NetbackLloydminsterhuskysupp1").value = NetBackLoydminsterHuskysupp1.toFixed(6);

     // trigger next function:    
//  	ApproximateMarginHuskysupp1Calculate();
}

// APPROXIMATE MARGIN CALCULATION
function ApproximateMarginHuskysupp1Calculate(){
    
	ApproximateMarginHuskysupp1 = (( parseFloat(SalesPriceHuskysupp1) - parseFloat(DeliveredCost_Huskysupp1) ) / parseFloat(SalesPriceHuskysupp1)) * 100;
    
    if (isNaN(ApproximateMarginHuskysupp1)) ApproximateMarginHuskysupp1 = 0;
	if (!isFinite(ApproximateMarginHuskysupp1)) ApproximateMarginHuskysupp1 = 0;   // replacing infinity value by 0
  
    document.getElementById("OliPage:form1:pageblock1:ApproxMarginHuskysupp1id").value = ApproximateMarginHuskysupp1.toFixed(6);
    
    // trigger next function:    
//  	USDValuesCalculateHuskysupp1();
}

function USDValuesCalculateHuskysupp1() {
    
	ExchangeRate = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value;
    
    var DeliveredCostUSD = parseFloat(ExchangeRate) * parseFloat(DeliveredCost_Huskysupp1);
    document.getElementById("OliPage:form1:pageblock1:DeliveredCostUSDHuskysupp1").value = DeliveredCostUSD.toFixed(6);
    var ApproxMarginUSD = parseFloat(ExchangeRate) * parseFloat(MarginHuskysupp1);
    document.getElementById("OliPage:form1:pageblock1:ApproxMarginUSDHuskysupp1").value = ApproxMarginUSD.toFixed(6);
    var SalesPriceUSD = parseFloat(ExchangeRate) * parseFloat(SalesPriceHuskysupp1);
	if (isNaN(SalesPriceUSD)) SalesPriceUSD = 0;   // avoid getting NaN value in case when salesprice BID is activated, and is empty. 
    document.getElementById("OliPage:form1:pageblock1:SalesPriceUSDHuskysupp1").value = SalesPriceUSD.toFixed(6);
    var NetBackLloydUSD = parseFloat(ExchangeRate) * parseFloat(NetBackLoydminsterHuskysupp1);
    document.getElementById("OliPage:form1:pageblock1:NetBackLloydHuskysupp1").value = NetBackLloydUSD.toFixed(6);
    
	// formating of numbers
    ReplaceZerosHuskysupp1(); // Format values to form without unnecessary zeros or invalid inputs.
}



//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE LAUNCHER / launch particular variation of Summary table when conditions meet
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableCalculateHuskysupp1Launcher() {
    var SelectedSalespriceBIDcheckbox = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2").checked; 
	var SelectedCurrencyStatusforTable = document.getElementById("OliPage:form1:pageblock1:CurrencyPicklist").value;

    if(SelectedSalespriceBIDcheckbox == true && SelectedCurrencyStatusforTable == 'CAD') 
    {
    	SummaryTableHuskysupp1CalculateBID();  
    }
	else if (SelectedSalespriceBIDcheckbox == true && SelectedCurrencyStatusforTable == 'US') 
    {
    	SummaryTableHuskysupp1CalculateBID_USD();
    }
    else if (SelectedSalespriceBIDcheckbox == false && SelectedCurrencyStatusforTable == 'US') 
    {
        SummaryTableHuskysupp1CalculateUSD();
	}
    else
    {
        SummaryTableHuskysupp1Calculate();
	}
    
    fillSummaryTable(); //feeds salesforce fields in order to store values. 


}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE / default values
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp1Calculate (){   
document.getElementById("selected_supplier_display").value= " default view";
 	var partialSalesPrice =  parseFloat(PartialDC_Huskysupp1) + parseFloat(MarginHuskysupp1);  
    
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = PartialDC_Huskysupp1 + Axle5huskysupp1VarProcessed;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = PartialDC_Huskysupp1 + Axle6huskysupp1VarProcessed;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = PartialDC_Huskysupp1 + Axle7huskysupp1VarProcessed;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = PartialDC_Huskysupp1 + Axle8huskysupp1VarProcessed;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = PartialDC_Huskysupp1 + RailCarFeeHuskysupp1Processed;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
    var HS1_DC_PU = parseFloat(PartialDC_Huskysupp1);
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
// Summary table calculation FOR MARGIN  
    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;  
    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0; 
    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) * 100;
     if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
    
    var HS1_Margin_PU = ( parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp1) ) / parseFloat(partialSalesPrice) * 100;
                         if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
    
// Summary table calculation FOR NETBACK  
    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed)));
    	document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed)));
    	document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed)));
    	document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);
    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed)));
    	document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed)));
    	document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);
    var HS1_Netback_PU = parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp1);
    	document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);
    
// Summary table calculation for SALESPRICE (original)
/*	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp1);
      document.getElementById("SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp1);
      document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp1);
      document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp1);
      document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1);
      document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp1);
      document.getElementById("SalespricePUcalc").value = HS1_SP_PU.toFixed(6);   */
     
    // Summary table calculation for SALESPRICE (edited)
	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp1);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp1);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp1);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp1);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1);
      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp1);
      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
}
//____________________________________________________________________________________________________________________



//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE BID / salesprice bid included to calculations
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp1CalculateBID(){  
document.getElementById("selected_supplier_display").value= "   (Sales Price BID included)";
    var SalesPriceBIDvalue = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value.replace(/,/g, '');
    var partialSalesPrice =  parseFloat(PartialDC_Huskysupp1) + parseFloat(MarginHuskysupp1);  
    AxleRateSelectorStatus = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;
    //vars for applying bid prices
    var applyBIDpricePU = document.getElementById("OliPage:form1:pageblock1:IncludePU_bid");
    var applyBIDpriceRC = document.getElementById("OliPage:form1:pageblock1:IncludeRC_bid"); 
    var applyBIDpriceA5 = document.getElementById("OliPage:form1:pageblock1:IncludeA5_bid"); 
    var applyBIDpriceA6 = document.getElementById("OliPage:form1:pageblock1:IncludeA6_bid"); 
    var applyBIDpriceA7 = document.getElementById("OliPage:form1:pageblock1:IncludeA7_bid"); 
    var applyBIDpriceA8 = document.getElementById("OliPage:form1:pageblock1:IncludeA8_bid"); 
        
    
    // Summary table calculation FOR DELIVERED COST BID
    var HS1_DC_A5 = PartialDC_Huskysupp1 + Axle5huskysupp1VarProcessed;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = PartialDC_Huskysupp1 + Axle6huskysupp1VarProcessed;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = PartialDC_Huskysupp1 + Axle7huskysupp1VarProcessed;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = PartialDC_Huskysupp1 + Axle8huskysupp1VarProcessed;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = PartialDC_Huskysupp1 + RailCarFeeHuskysupp1Processed;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
	var HS1_DC_PU = parseFloat(PartialDC_Huskysupp1);
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
   // Summary table calculation for SALESPRICE BID

    //AXLE 5 ROW AT SUMMARY TABLE
    if(AxleRateSelectorStatus == 'Axle 5 Price' && applyBIDpriceA5.checked == false)   // if(AxleRateSelectorStatus == 'Axle 5 Price') 
    {
        // salesprice
        document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = SalesPriceBIDvalue;
        // netback
        var HS1_Netback_A5_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed)));
        document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
        // margin
        var HS1_Margin_A5_bid = (SalesPriceBIDvalue - HS1_DC_A5) / HS1_DC_A5 * 100;
        if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
        if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
    }
    
	// Salesprice BID summary table controller
    else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus != 'Axle 5 Price' ) {

        	var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed)));	
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing NaN value by 0
			if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
    }
    
    else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus == 'Axle 5 Price') {
        
       		 var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed)));
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
			if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA5; 
            SalesPriceHuskysupp1 = SalesPriceBIDA5; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
    }    
	// Salesprice BID summary table controller - end

                else
                {
                    //salesprice
                    var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp1); 
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
                    // netback
                    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed)));
                    document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);       
                    // margin
                    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
					if (!isFinite(HS1_Margin_A5)) HS1_Margin_A5 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
                    
                }
    //AXLE 6 ROW AT SUMMARY TABLE
    if(AxleRateSelectorStatus == 'Axle 6 Price' && applyBIDpriceA6.checked == false) //if(AxleRateSelectorStatus == 'Axle 6 Price') 
        {
            //salesprice
         //   document.getElementById("SalespriceA6").value = SalesPriceBIDvalue;
             document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDvalue - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
			if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
            
        }
    
// Salesprice BID summary table controller
    else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus != 'Axle 6 Price' ) {
        
        var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, '');
        // netback
        var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed)));
        document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
        // margin
        var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
        if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
        if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);

    }
    
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus == 'Axle 6 Price') {
            
            var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA6; 
            SalesPriceHuskysupp1 = SalesPriceBIDA6; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();

        }    
    // Salesprice BID summary table controller - end
	
                else
                {
                    //salesprice
                    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp1);
               //     document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
                           document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    // netback
                    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed)));
                    document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6); 
                    // margin
                    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
                    if (!isFinite(HS1_Margin_A6)) HS1_Margin_A6 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
                } 
    
    //AXLE 7 ROW AT SUMMARY TABLE
       if(AxleRateSelectorStatus == 'Axle 7 Price' && applyBIDpriceA7.checked == false) // if(AxleRateSelectorStatus == 'Axle 7 Price') 
        {
            //salesprice
        //    document.getElementById("SalespriceA7").value = SalesPriceBIDvalue;
            document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed)));
			document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6); 
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDvalue - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus != 'Axle 7 Price' ) {
        
        var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, '');
        // netback
        var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed)));
        document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
        // margin
        var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
        if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
        if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus == 'Axle 7 Price') {
            
            var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed)));
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA7; 
            SalesPriceHuskysupp1 = SalesPriceBIDA7; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }    
    // Salesprice BID summary table controller - end
 
                else
                {
                    //salesprice
                    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp1); 
                 //   document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
					document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    // netback
                    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed)));
                    document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);    
                    // margin
                    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
                    if (!isFinite(HS1_Margin_A7)) HS1_Margin_A7 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
                }
    
    //AXLE 8 ROW AT SUMMARY TABLE
	if(AxleRateSelectorStatus == 'Axle 8 Price' && applyBIDpriceA8.checked == false) //  if(AxleRateSelectorStatus == 'Axle 8 Price') 
        {
            //salesprice
      //      document.getElementById("SalespriceA8").value = SalesPriceBIDvalue;
            document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed)));
			document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6); 
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDvalue - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus != 'Axle 8 Price' ) {
        
        var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed)));
        document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
        // margin
        var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
        if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
        if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus == 'Axle 8 Price') {
            
            var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed)));
            document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA8; 
            SalesPriceHuskysupp1 = SalesPriceBIDA8; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }  
    
    // Salesprice BID summary table controller - end
                else
                {
                    //salesprice
                    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp1);
               //     document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    // netback
                    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed)));
                    document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
                    // margin
                    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
                    if (!isFinite(HS1_Margin_A8)) HS1_Margin_A8 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
                } 
   
    //RAIL CAR ROW AT SUMMARY TABLE
	if(AxleRateSelectorStatus == 'Railcar' && applyBIDpriceRC.checked == false) // if(AxleRateSelectorStatus == 'Railcar') 
        {
            //salesprice
        //    document.getElementById("SalespriceRC").value = SalesPriceBIDvalue;
            document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed)));
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDvalue - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus != 'Railcar' ) {
        
        var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, '');
        // netback
        var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed)));
        document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
        // margin
        var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
        if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
        if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
    }
    
        else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus == 'Railcar') {
            
            var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, '');
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed)));
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDRC; 
            SalesPriceHuskysupp1 = SalesPriceBIDRC; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }    
    // Salesprice BID summary table controller - end

                else
                {
                    //salesprice
                    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1);
                 //   document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    // netback
                    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed)));
                    document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);     
                    // margin
                    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) * 100;
                    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
					if (!isFinite(HS1_Margin_RC)) HS1_Margin_RC = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
                } 
    
     //PICKUP ROW AT SUMMARY TABLE
   	if(AxleRateSelectorStatus == 'Pickup' && applyBIDpricePU.checked == false) // if(AxleRateSelectorStatus == 'Pickup') 
        {
            //salesprice
              document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDvalue - parseFloat(PartialDCNetback_Huskysupp1);
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDvalue - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus != 'Pickup' ) {
        
        var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp1) ;
        document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
        // margin
        var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
        if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
        if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
    }
    
        else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus == 'Pickup') {
            
            var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp1);
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDPU; 
            SalesPriceHuskysupp1 = SalesPriceBIDPU; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }    
    // Salesprice BID summary table controller - end 
    
                else
                {
                    //salesprice
                    var HS1_SP_PU = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1);
                       document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
                    // netback
                    var HS1_Netback_PU = parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp1);
                    document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);     
                    // margin
                    var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp1)) / parseFloat(partialSalesPrice) * 100;
                    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
                    if (!isFinite(HS1_Margin_PU)) HS1_Margin_PU = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
                } 
}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE USD / salesprice USD included to calculations
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp1CalculateUSD(){  
document.getElementById("selected_supplier_display").value= "   (USD currency)";
    
    	var ExchaneRateUSD = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value;
    	var partialSalesPrice =  parseFloat(PartialDC_Huskysupp1) + parseFloat(MarginHuskysupp1);  
    
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = (PartialDC_Huskysupp1 + Axle5huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = (PartialDC_Huskysupp1 + Axle6huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = (PartialDC_Huskysupp1 + Axle7huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = (PartialDC_Huskysupp1 + Axle8huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = (PartialDC_Huskysupp1 + RailCarFeeHuskysupp1Processed) * ExchaneRateUSD;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
    var HS1_DC_PU = PartialDC_Huskysupp1 * ExchaneRateUSD;
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
// Summary table calculation FOR MARGIN  
    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) * 100;
    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
	var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp1)) / parseFloat(partialSalesPrice) * 100;
    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
    
// Summary table calculation FOR NETBACK  
    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);
    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);
	var HS1_Netback_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp1)) * ExchaneRateUSD;
    	document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);
    
// Summary table calculation for SALESPRICE
/*	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("SalespricePU").value = HS1_SP_PU.toFixed(6); */
    
    // Summary table calculation for SALESPRICE
	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
    
    
}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE BID USD / salesprice bid included to calculations USD
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp1CalculateBID_USD(){  
document.getElementById("selected_supplier_display").value= "   (Sales Price BID at USD currency)";
    
    var ExchaneRateUSD = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value.replace(/,/g, '');
    var SalesPriceBIDvalue = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value.replace(/,/g, '');
    var partialSalesPrice =  parseFloat(PartialDC_Huskysupp1) + parseFloat(MarginHuskysupp1);  
    AxleRateSelectorStatus = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;
    //vars for applying bid prices    
    var applyBIDpricePU = document.getElementById("OliPage:form1:pageblock1:IncludePU_bid");
    var applyBIDpriceRC = document.getElementById("OliPage:form1:pageblock1:IncludeRC_bid"); 
    var applyBIDpriceA5 = document.getElementById("OliPage:form1:pageblock1:IncludeA5_bid"); 
    var applyBIDpriceA6 = document.getElementById("OliPage:form1:pageblock1:IncludeA6_bid"); 
    var applyBIDpriceA7 = document.getElementById("OliPage:form1:pageblock1:IncludeA7_bid"); 
    var applyBIDpriceA8 = document.getElementById("OliPage:form1:pageblock1:IncludeA8_bid");
 
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = (PartialDC_Huskysupp1 + Axle5huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = (PartialDC_Huskysupp1 + Axle6huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = (PartialDC_Huskysupp1 + Axle7huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = (PartialDC_Huskysupp1 + Axle8huskysupp1VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = (PartialDC_Huskysupp1 + RailCarFeeHuskysupp1Processed) * ExchaneRateUSD;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
	var HS1_DC_PU = PartialDC_Huskysupp1 * ExchaneRateUSD;
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
   // Summary table calculation for SALESPRICE BID

    //AXLE 5 ROW AT SUMMARY TABLE
     if(AxleRateSelectorStatus == 'Axle 5 Price' && applyBIDpriceA5.checked == false) // if(AxleRateSelectorStatus == 'Axle 5 Price') 
        {
            // salesprice
         //   document.getElementById("SalespriceA5").value = SalesPriceBIDvalue * ExchaneRateUSD;
           document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
//                       document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = SalesPriceBIDvalue * ExchaneRateUSD;
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);  
             // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus != 'Axle 5 Price' ) {
        
        var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed)));
        document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
        // margin
        var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
        if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
        if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus == 'Axle 5 Price') {
            
            var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA5.toFixed(6);
            SalesPriceHuskysupp1 = SalesPriceBIDA5; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
            //alert('this');
        }    
    // Salesprice BID summary table controller - end
	
                else
                {
                    //salesprice
                    var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD; 
                //    document.getElementById("SalespriceA5").value = HS1_SP_A5.toFixed(6);
                     document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
                    // netback
                    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
					// margin
                    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle5huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
                    if (!isFinite(HS1_Margin_A5)) HS1_Margin_A5 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
                }
    //AXLE 6 ROW AT SUMMARY TABLE
         if(AxleRateSelectorStatus == 'Axle 6 Price' && applyBIDpriceA6.checked == false) // if(AxleRateSelectorStatus == 'Axle 6 Price') 
        {
            //salesprice
            //   document.getElementById("SalespriceA6").value = SalesPriceBIDvalue * ExchaneRateUSD;
            //	document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = SalesPriceBIDvalue * ExchaneRateUSD;
			document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
			// margin
            var HS1_Margin_A6_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus != 'Axle 6 Price' ) {
        
        var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed)));
        document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
        // margin
        var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
        if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
        if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        
    }
    
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus == 'Axle 6 Price') {
            
            var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA6; 
            SalesPriceHuskysupp1 = SalesPriceBIDA6; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
            
        }    
    // Salesprice BID summary table controller - end
 
                else
                {
                    //salesprice
                    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
                 //   document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    // netback
                    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
                    // magrin
                    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle6huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
                    if (!isFinite(HS1_Margin_A6)) HS1_Margin_A6 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
                    
                } 
    //AXLE 7 ROW AT SUMMARY TABLE
        if(AxleRateSelectorStatus == 'Axle 7 Price' && applyBIDpriceA7.checked == false) // if(AxleRateSelectorStatus == 'Axle 7 Price') 
        {
            //salesprice
          //  document.getElementById("SalespriceA7").value = SalesPriceBIDvalue * ExchaneRateUSD;
          document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
			//	document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = SalesPriceBIDvalue * ExchaneRateUSD;
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) * ExchaneRateUSD;
			document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
    
       // Salesprice BID summary table controller
    else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus != 'Axle 7 Price' ) {
        
        var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed)));
        document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
        // margin
        var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
        if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
        if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus == 'Axle 7 Price') {
            
            var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA7; 
            SalesPriceHuskysupp1 = SalesPriceBIDA7; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }    
    // Salesprice BID summary table controller - end
          
            else
                {
                    //salesprice
                    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD; 
                //    document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    // netback
                    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6); 
                    // margin
                    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle7huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
                    if (!isFinite(HS1_Margin_A7)) HS1_Margin_A7 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
                    
                    
                }
    //AXLE 8 ROW AT SUMMARY TABLE
         if(AxleRateSelectorStatus == 'Axle 8 Price' && applyBIDpriceA8.checked == false) // if(AxleRateSelectorStatus == 'Axle 8 Price') 
        {
            //salesprice
            //  document.getElementById("SalespriceA8").value = SalesPriceBIDvalue * ExchaneRateUSD;
            document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            //	document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = SalesPriceBIDvalue * ExchaneRateUSD;
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) * ExchaneRateUSD;
			document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
             // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus != 'Axle 8 Price' ) {
        
        var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed)));
        document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
        // margin
        var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
        if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
        if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus == 'Axle 8 Price') {
            
            var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDA8; 
            SalesPriceHuskysupp1 = SalesPriceBIDA8; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }  
    // Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
                //    document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    // netback
                    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
                    // margin
                    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(Axle8huskysupp1VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp1VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
                    if (!isFinite(HS1_Margin_A8)) HS1_Margin_A8 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
                } 
    //RAIL CAR ROW AT SUMMARY TABLE
        if(AxleRateSelectorStatus == 'Railcar' && applyBIDpriceRC.checked == false) // if(AxleRateSelectorStatus == 'Railcar') 
        {
            //salesprice
          //  document.getElementById("SalespriceRC").value = SalesPriceBIDvalue * ExchaneRateUSD;
 //             document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = SalesPriceBIDvalue * ExchaneRateUSD;
              document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);           
            // netback
            var HS1_Netback_RC_bid = ((SalesPriceBIDvalue) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) * ExchaneRateUSD;
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus != 'Railcar' ) {
        
        var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed)));
        document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
        // margin
        var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
        if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
        if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
    }
    
        else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus == 'Railcar') {
            
            var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) * ExchaneRateUSD;
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDRC; 
            SalesPriceHuskysupp1 = SalesPriceBIDRC; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }    
    // Salesprice BID summary table controller - end
               
            else
                {
                    //salesprice
                    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1) * ExchaneRateUSD;
                  //  document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
                      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    // netback
                    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDCNetback_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);   
                    // margin
                    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) - (parseFloat(PartialDC_Huskysupp1) + parseFloat(RailCarFeeHuskysupp1Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp1Processed)) * 100;
                    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
                    if (!isFinite(HS1_Margin_RC)) HS1_Margin_RC = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
                } 
    
      //PICKUP ROW AT SUMMARY TABLE
           if(AxleRateSelectorStatus == 'Pickup' && applyBIDpricePU.checked == false) // if(AxleRateSelectorStatus == 'Pickup') 
        {
            //salesprice
         //   document.getElementById("SalespricePU").value = SalesPriceBIDvalue * ExchaneRateUSD;
//            document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = SalesPriceBIDvalue * ExchaneRateUSD;
            document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_PU_bid = (SalesPriceBIDvalue - parseFloat(PartialDCNetback_Huskysupp1)) * ExchaneRateUSD;
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus != 'Pickup' ) {
        
        var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp1) ;
        document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
        // margin
        var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
        if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
        if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
    }
    
        else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus == 'Pickup') {
            
            var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp1) * ExchaneRateUSD;
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp1").value = SalesPriceBIDPU; 
            SalesPriceHuskysupp1 = SalesPriceBIDPU; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp1Calculate();   
            ApproximateMarginHuskysupp1Calculate();
            USDValuesCalculateHuskysupp1();
        }    
    // Salesprice BID summary table controller - end 
    
                else
                {
                    //salesprice
                    var HS1_SP_PU = (parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp1)) * ExchaneRateUSD;
                 //   document.getElementById("SalespricePU").value = HS1_SP_PU.toFixed(6);
                      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
                    // netback
                    var HS1_Netback_PU = ( parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp1)) * ExchaneRateUSD;
                    document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);     
                    // margin
                    var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp1)) / parseFloat(partialSalesPrice) * 100;
                    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
                    if (!isFinite(HS1_Margin_PU)) HS1_Margin_PU = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
                } 
    
    
    
}
//____________________________________________________________________________________________________________________