/*
Author: Able Sense Media
Web: ablesense.com
Date of creation: 2014/03/26
*/

/////////////////////////////////////////////////////////////////
///////////////////// NOTIFICATION HANDLER //////////////////////
/////////////////////////////////////////////////////////////////

var Notification = function(title, html){
	var n = this;

	n.title = title;
	n.message = html;
	n.notificationTypes = {alert: 1, prompt: 2, warning: 3, edit: 4};
	n.notificationType = n.notificationTypes.alert;

	n.acceptCallback = function(){};
	n.cancelCallback = function(){};

	return n;
};

Notification.prototype = {
	title : '',
	message : '',

	notificationTypes : {alert: 1, prompt: 2, warning: 3, edit: 4},
	notificationType : 1,

	acceptCallback : function(){},
	cancelCallback : function(){}
};

Notification.prototype.reset = function(){
	this.box.css({marginTop: '20px'});

	this.box.removeClass('edit').removeClass('warning');
};

Notification.prototype.show = function(){
	var n = this;

	n.overlay = $('#notification');
	n.box = n.overlay.find('#box');
	n.titleBox = n.box.find('#title');
	n.messageBox = n.box.find('#message');
	n.buttonsBox = n.box.find('#buttons');

	switch(n.notificationType){
		case 2:
			n.buttonsBox.find('#cancel').show();
			n.buttonsBox.find('#accept').text('Accept');
			break;

		case 3:
			n.buttonsBox.find('#cancel').hide();
			n.buttonsBox.find('#accept').hide();
			n.titleBox.find('#close').hide();
			n.box.addClass('warning');
			break;

		case 4:
			n.buttonsBox.find('#cancel').hide();
			n.buttonsBox.find('#accept').text('Edit');
			n.box.addClass('edit');
			break;

		default:
			n.buttonsBox.find('#cancel').hide();
			n.buttonsBox.find('#accept').text('OK');
	}

	n.titleBox.find('#close').off().on('click', function(){
		n.hide();
	});

	n.buttonsBox.find('#cancel').off().on('click', function(){
		n.hide();
		n.cancelCallback();
	});

	n.buttonsBox.find('#accept').off().on('click', function(){
		n.hide();
		n.acceptCallback();
	});

	n.titleBox.find('h3').html(n.title);
	n.messageBox.empty().append(n.message);

	n.box.css('margin-top', '100px');
	n.overlay.fadeIn(100);
};

Notification.prototype.hide = function(){
	var n = this;

	this.overlay.fadeOut(150);
	this.box.animate({marginTop: '20px'}, 150, function(){
		n.reset();
	});
};

/////////////////////////////////////////////////////////////////
///////////////////// EXTENDED DATE OBJECT //////////////////////
/////////////////////////////////////////////////////////////////

Date.prototype.getNiceString = function(){
	return this.getFullYear() + '/' + (parseInt(this.getMonth(), 10)+1) + '/' + this.getDate() + ' ' + (this.getHours()<10?'0':'') + this.getHours() + ':' + (this.getMinutes()<10?'0':'') + this.getMinutes();
};

function calcDaysBetween(soon, later) {
	return Math.abs(Math.round((new Date(later) - new Date(soon)) / (1000*60*60*24)));
}

function stringToDate(date_string) {
	if (typeof date_string != 'undefined') {
		if (date_string.match(/^(\d\d\d\d)-(\d\d)-(\d\d)$/)) {
			date_string = date_string.replace(/-/g, "/");
		}

		var d = new Date(Date.parse(date_string));
		// return new Date(d.getTime() + (d.getTimezoneOffset() * 60000));
		return new Date(d.getTime());
	} else {
		return false;
	}
}

/////////////////////////////////////////////////////////////////
//////////////////////////// APP ////////////////////////////////
/////////////////////////////////////////////////////////////////

var APP = (function () {
	var me = {},

	/////////////////////////////////////////////////////////////////
	//////////////////////////// PRIVATE ////////////////////////////
	/////////////////////////////////////////////////////////////////
		today = new Date(),
		timestamp = '',

		dayWidth = 30,

		crumbsContainer = $('.header .crumbs'),
		chartTable = $('.main table'),

		chartDataRequestURL = '/apex/GRDWellGanttJSON?projectID=',
		chartDataRequest = null,
		chartData = {},

		chartDataError = false,

		chartDateRange = [],
		chartStartDate = today,
		chartMilestones = [],

		monthLabels = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),

		milestoneRow = function(name, url) {
			var row = $('<tr></tr>'),
				th = $('<th><a target="parent" href="/' + url + '">' + name + '</a></th>'),
				td = $('<td></td>');

			td.css('width', chartDateRange.length * 30 + 'px');

			return row.append(th).append(td);
		},

		dateLayer = function() {
			var layer = $('<div class="date-layer"></div>');

			return layer;
		},

		chartLayer = function() {
			//Constructs the appointment layer for one day with the use
			//of the salesforce appointment json object

			var layer = $('<div class="chart-layer"></div>'),
				actual = $('<div class="actual"><div class="no-wrap"><span class="range"></span><span class="duration"><strong>Duration:</strong></span></div></div>');

			return layer.append(actual);
		};

	function softfail(val, showBools) {
		if(arguments.length === 1){ showBools = false; }

		try{
			if(val !== null && typeof val !== undefined){
				if(!showBools && val === false){
					return '';
				}else{
					return val;
				}
			}else{
				return '';
			}
		}catch(err){
			return '';
		}
	}

	function setChartData(callBack) {
		var GRDWellID = me.SFVars.GRDWellID;

		if (typeof GRDWellID === 'undefined' || GRDWellID === '') {
			var errornotify = new Notification('Error', '');
			errornotify.notificationType = errornotify.notificationTypes.warning;
			errornotify.title = 'Error';
			errornotify.message = '<p>No Well ID was supplied. Please reference this page with the Well ID in order to get the right chart.</p>';
			errornotify.show();
		} else {
			chartDataRequestURL += GRDWellID;

			chartDataRequest = $.getJSON(chartDataRequestURL, function(data){
				chartData = data;

				callBack();
			}).error(function(XMLHttpRequest, textStatus){
				if(textStatus !== 'abort'){
					var errornotify = new Notification('Error', '');
					errornotify.notificationType = errornotify.notificationTypes.warning;
					errornotify.title = 'Error while connecting to server';
					errornotify.message = '<p>Something went wrong while getting the data from the server.</p><p>Please try again in a moment.</p>';
					errornotify.show();
				}
			});
		}
	}
/*
	function setPageCrumbs(project) {
		if (typeof project != 'undefined') {
			if (typeof project.Well__r != 'undefined') {
				var well_li = $('<li>').append('<h1><a href="/' + softfail(Well__r.Id) + '">' + softfail(project.Well__r.Name) + '</h1></a>');
				crumbsContainer.append(well_li);
			}

			var project_li = $('<li>').append('<h2><a target="parent" href="/' + softfail(project.Id) + '">' + softfail(project.Name) + '</h2></a>');
			crumbsContainer.append(project_li);
		}
	}
*/
/*
	function seperateChartData() {
		var crumbs_set = false;

		if (chartData.length !== 0)
		{
			while(chartMilestones.length < chartData.length && !chartDataError)
			{
				$.each(chartData, function(i, milestone)
				                  {
                					  //set the page breadcrumbs
                					  if (!crumbs_set) {
                    				  	  setPageCrumbs(milestone.Project__r);

                						crumbs_set = true;
                					}

                					//fill up the milestones array in order
                					try{
                						if (chartMilestones.indexOf(milestone) == -1) {
                							if (!milestone.hasOwnProperty('Predecessor_Milestone__c') && !last_milestone_id) {
                								//this is the first one
                								last_milestone_id = milestone.Id;
                								chartMilestones.push(milestone);
                							} else {
                								if (milestone.hasOwnProperty('Predecessor_Milestone__c')) {
                									if (milestone.Predecessor_Milestone__c == last_milestone_id) {
                										last_milestone_id = milestone.Id;
                										chartMilestones.push(milestone);
                									}
                								} else {
                									//there's a second one with no predecessor, which means I could be wrong about the other one being the first one. Throw error since only one is allowed to not have a predecessor
                									chartDataError = true;
                								}
                							}
                						}
                					} catch(e) {
                						chartDataError = true;
                					}
                				});
			}
		}
	}
*/
	function setChartDateRange() {
		var startDate = 0,
			endDate = 0;
        
        startDate = Math.min(chartData.RTD_Initiated__c,
                             chartData.Go_No_Go_Decision__c,
                             chartData.RIC_Initiated__c,
                             chartData.Completion_Go_Decision_Approved__c,
                             chartData.License_Pipeline_Start__c,
                             chartData.License_Pipeline_End__c,
                             chartData.Drill_Task_Start__c,
                             chartData.Drill_Task_End__c,
                             chartData.Fracture_Formation_Complete__c,
                             chartData.UWI_Location__r.Rig_Release_Date__c,
                             chartData.Pipeline_Start__c,
                             chartData.Pipeline_End__c);
        
        endDate = Math.max(chartData.RTD_Initiated__c,
                           chartData.Go_No_Go_Decision__c,
                           chartData.RIC_Initiated__c,
                           chartData.Completion_Go_Decision_Approved__c,
                           chartData.License_Pipeline_Start__c,
                           chartData.License_Pipeline_End__c,
                           chartData.Drill_Task_Start__c,
                           chartData.Drill_Task_End__c,
                           chartData.Fracture_Formation_Complete__c,
                           chartData.UWI_Location__r.Rig_Release_Date__c,
                           chartData.Pipeline_Start__c,
                           chartData.Pipeline_End__c);
        
		for (var d = new Date(startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
			chartDateRange.push(new Date(d));
		}

		var startPadding = new Date(startDate);
		startPadding.setDate(startPadding.getDate() - 2);
		chartDateRange.unshift(startPadding);

		var endPadding = new Date(endDate);
		endPadding.setDate(endPadding.getDate() + 2);
		chartDateRange.push(endPadding);

		chartStartDate = new Date(startPadding);
	}

	function buildChartHeader() {
		var last_month = 0,
			th = $('<th scope="col"></th>'),
			month_container = false,
			month_name = false,
			day_container = false;

		$.each(chartDateRange, function(i, date) {
			var m = date.getMonth()+1;

			if (m !== last_month) {
				//pack up the previous month and add it to the th
				if (month_container && month_name && day_container) {
					th.append(month_container.append(month_name).append(day_container));
				}

				month_container = $('<div>').addClass('month');
				month_name = $('<div>').addClass('name').html(monthLabels[m-1]);
				day_container = $('<div>').addClass('days');
			}

			day_container.append($('<span>').html(date.getDate()));

			last_month = m;
		});

		th.append(month_container.append(month_name).append(day_container));

		th.css('width', chartDateRange.length * 30 + 'px');
		chartTable.find('thead tr').append(th);

		chartTable.find('thead, tbody').css('width', chartDateRange.length * 30 + 160 + 'px');
		chartTable.css('width', chartDateRange.length * 30 + 160 + 'px');
	}
    
    function buildDateRange(startDate, endDate, name)
    {
        var duration = endDate - startDate;
        var r = milestoneRow(softfail(name), softfail(name)),
			d = dateLayer(),
			c = chartLayer();
		
		c.find('.actual .range').html('<time datetime="' + softfail(startDate) + '">' + softfail(startDate) + '</time> - <time datetime="' + softfail(endDate) + '">' + softfail(endDate) + '</time>');
		c.find('.actual .duration em').html(softfail(duration));

		//Actual Start and Duration
		var actual_el = c.find('.actual'),
			actual_kickoff = stringToDate(startDate),
			actual_diff = calcDaysBetween(actual_kickoff, chartStartDate);
////			status_class = softfail(milestone.Status__c).toLowerCase().replace('(', '_').replace(')', '').replace(' ', '');

		actual_el.find('.no-wrap').css('width', softfail(duration) * dayWidth + 'px');

		if (actual_diff || actual_diff === 0) {
			actual_el.css('left', (actual_diff * dayWidth) + 'px');
		} else {
			actual_el.addClass('hidden');
		}

		actual_el.addClass(status_class);

		r.find('td').append(d).append(c);

		chartTable.find('tbody').append(r);
    }
    
	function buildChart()
	{
	    // Send each life cycle stage into build method
	    
	    // Budget
	    if(chartData.RTD_Initiated__c)
	    {
	        buildDateRange(chartData.RTD_Initiated__c - 2, chartData.RTD_Initiated__c, 'Budget');
        }
	    
	    // RTD
	    if(chartData.RTD_Initiated__c, chartData.Go_No_Go_Decision__c)
	    {
	        buildDateRange(chartData.RTD_Initiated__c, chartData.Go_No_Go_Decision__c, 'RTD');
        }
        
	    // RIC
	    if(chartData.RIC_Initiated__c, chartData.Completion_Go_Decision_Approved__c)
	    {
	        buildDateRange(chartData.RIC_Initiated__c, chartData.Completion_Go_Decision_Approved__c, 'RIC');
        }
	    
	    // EWR/PARF
	    if(chartData.License_Pipeline_End__c, chartData.License_Pipeline_Start__c)
	    {
	        buildDateRange(chartData.License_Pipeline_End__c, chartData.License_Pipeline_Start__c, 'EWR/PARF');
        }
	    
	    // Drilling
	    if(chartData.Drill_Task_Start__c, chartData.Drill_Task_End__c)
	    {
	        buildDateRange(chartData.Drill_Task_Start__c, chartData.Drill_Task_End__c, 'Drilling');
        }
        
	    // Completion
	    if(chartData.UWI_Location__r.Rig_Release_Date__c, chartData.Fracture_Formation_Complete__c)
	    {
	        buildDateRange(chartData.UWI_Location__r.Rig_Release_Date__c, chartData.Fracture_Formation_Complete__c, 'Completion');
        }
        
	    // Tie In
	    if(chartData.Pipeline_Start__c, chartData.Pipeline_End__c)
	    {
	        buildDateRange(chartData.Pipeline_Start__c, chartData.Pipeline_End__c, 'Tie In');
        }
        
	    // Production
	    if(chartData.Production_Start__c)
	    {
	        buildDateRange(chartData.Production_Start__c, chartData.Production_Start__c + 2, 'Production');
        }
	}

	/////////////////////////////////////////////////////////////////
	//////////////////////////// PUBLIC /////////////////////////////
	/////////////////////////////////////////////////////////////////

	me.SFVars = {};

	me.initChart = function(){
		//show loading animation
		chartTable.addClass('loading');

		setChartData(function() {
			setTimeout(function() {
				//extract bits of information like project, well, dates...
//				seperateChartData();

				if (!chartDataError) {
					if (chartMilestones.length !== 0) {
						//determine the dates to be displayed in the chart
						setChartDateRange();

						//build header based on start and end date of the chart
						buildChartHeader();

						//plot milestones on the chart
						buildChart();
					} else {
						chartTable.hide();

						var nomilestoneerror = new Notification('Error', '');
						nomilestoneerror.notificationType = nomilestoneerror.notificationTypes.warning;
						nomilestoneerror.title = 'No Milestones';
						nomilestoneerror.message = '<p>There are no milestones in this project. A minimum of one milestone needs to be present in order for the chart to be drawn.</p>';
						nomilestoneerror.show();
					}
				} else {
					chartTable.hide();

					var dataerror = new Notification('Error', '');
					dataerror.notificationType = dataerror.notificationTypes.warning;
					dataerror.title = 'Incomplete data';
					dataerror.message = '<p>The chart received incomplete data from Salesforce. Please complete the project for the chart to start displaying milestones.</p>';
					dataerror.show();
				}

				//remove loading class
				chartTable.removeClass('loading');
			}, 200);
		});
	};

	return me;
}());

$(function(){
	var thead_offset = 40,
		html = $('html');

	//Hide the bar when this page is sitting in an iframe{
	if (window != window.top) {
		$('body').addClass('no-header');
		thead_offset = 0;
	}

	function isIE () {
		var myNav = navigator.userAgent.toLowerCase();
		return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
	}

	if (isIE() && isIE() < 10) {
		html.addClass('lt-ie10');
	}

	//Fixed horizontal and vertical headers
	if (!html.hasClass('lt-ie10') && !html.hasClass('no-fix')) {
		$('.main').on('scroll', function(){
			$('thead').css('top', thead_offset + $(this).scrollTop() + 'px');
			$('tbody th').css('left', $(this).scrollLeft() + 'px');
		});
	}

	APP.initChart();
});
