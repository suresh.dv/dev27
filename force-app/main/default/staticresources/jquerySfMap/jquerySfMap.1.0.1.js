                            (function($){
    $.fn.extend({ 
        sfMap: function(methodOrOptions, opts) {
            
			function GetDetailHref(opts){                  
          		return '<div><a href="javascript:window.open(\'' + (opts.href ? opts.href : '') + '\', \'' +
               		GetHrefTarget() + '\');">' + (opts.value ? opts.value : '') + '</a></div>'
            }
            
            //self for Salesforce1
            function GetHrefTarget(){
                if('{! $User.UITheme}' == 'Theme4t') { return '_self'; }
                else { return '_blank'; }
            }
            
            if($.inArray( methodOrOptions, [ 'detailHref', 'detailRow', 'detailRowHref', 'detailAddress' ] ) != -1){
            	switch(methodOrOptions){
                    case 'detailHref': 
                    	return GetDetailHref(opts);
                    case 'detailRow':
                		return '<div><div>' + (opts.title ? opts.title : '') + '</div><div>' + (opts.value ? opts.value : '') + '</div></div>';
                    case 'detailRowHref':
                		return '<div><div>' + (opts.title ? opts.title : '') + '</div>' + GetDetailHref(opts) + '</div>';
                    case 'detailAddress':
                    	if(opts.address){
                            var values = []
                            if(opts.address.street){ values.push(opts.address.street); }
                            if(opts.address.city){ values.push(opts.address.city); }
                            if(opts.address.postalCode){ values.push(opts.address.postalCode); }
                            if(opts.address.country){ values.push(opts.address.country); }
                            return '<div><div>' + (opts.title ? opts.title : '') + '</div><div>' + values.join(', ') + '</div></div>';
                        }
                        else {
                            return '';
                        }
                }       
            }
            return this.each(function() {  
                var elm = this;
                function Clear(){
                    $.data(elm,'boundsLimits', {
                            latMin: 90,
                            latMax: -90,
                            lngMin: 180,
                            lngMax: -180
                        });
                    var markers = $.data(elm,'markers');
                    if(markers){
                        for (var i = 0; i < markers.length; i++) {
                          markers[i].setMap(null);
                        }
                    }
                    var mainMarkers = $.data(elm,'mainMarkers');
                    if(mainMarkers){
                        for (var i = 0; i < mainMarkers.length; i++) {
                          mainMarkers[i].setMap(null);
                        }
                    }
                    $.data(elm,'markers', []);
                    $.data(elm,'mainMarkers', []);
                    if( $.data(elm,'setup').enableMarkerCluster && $.data(elm,'markerCluster')) {
                        var mc = $.data(elm,'markerCluster');
                        mc.clearMarkers();
                    }   
                }
                function SetMarkerAndBound(mark, bounds, map, icon, onClick){
                    var latLng = new google.maps.LatLng(mark.lat, mark.lng);
                    bounds.extend(latLng);
                    marker =  new google.maps.Marker({
                        position: latLng,
                        title: mark.title,
                        map: map,
                        icon: icon,
                        sfId: mark.sfId
                    });
                    if( mark.sfId != 0){
                        marker.addListener('click', function() {
                            var mrk = this;
                            onClick(mrk.sfId);
                            if($.data(elm,'setup').infoWindow.enable) SetInfoWindow(mrk, map);
                        });
                    }                
                    return marker;
                }
                
                function UpdateboundsLimits(bounds){
                    var boundsLimits = $.data(elm,'boundsLimits');
                    var isChange = false;
                    if(bounds.latMin < boundsLimits.latMin){
                        boundsLimits.latMin = bounds.latMin;
                        isChange = true
                    }
                    if(bounds.latMax > boundsLimits.latMax){
                        boundsLimits.latMax = bounds.latMax;
                        isChange = true
                    }
                    if((bounds.lngMin < boundsLimits.lngMin) || Math.abs(bounds.lngMin - boundsLimits.lngMin) > 180){
                        boundsLimits.lngMin = bounds.lngMin;
                        isChange = true
                    }
                    if(bounds.lngMax > boundsLimits.lngMax || Math.abs(bounds.lngMax - boundsLimits.lngMax) > 180){
                        boundsLimits.lngMax = bounds.lngMax;
                        isChange = true
                    }
                     if(isChange) $.data(elm,'boundsLimits', boundsLimits)
                    return isChange;
                };
                
                function GetQuery(setup, cornerBounds){
                    var boundsLngExpresion;
                    //if longitude is 180 min around 179 and max -179
                    if(cornerBounds.lngMin > cornerBounds.lngMax){ boundsLngExpresion = 'OR'; }
                    else { boundsLngExpresion = 'AND'; }
                    var query = 'SELECT ID,{additionalFields} {title},{latField}, {lngField} FROM {table} WHERE ' +
                            '{latField} >= ' + cornerBounds.latMin + ' and {latField} <= ' + cornerBounds.latMax +
                            'AND ({lngField} >= ' + cornerBounds.lngMin + ' ' + boundsLngExpresion + ' {lngField} <= ' + cornerBounds.lngMax + ')';
                    query = query.replace('{additionalFields}', setup.searchQuery.additionalFields);
                    query = query.replace('{title}', setup.searchQuery.title);
                    query = query.replace(/{latField}/g, setup.searchQuery.latField);
                    query = query.replace(/{lngField}/g, setup.searchQuery.lngField);
                    query = query.replace('{table}', setup.searchQuery.table);
                    if(setup.searchQuery.additionalConditions){
                        query = query + 'AND (' + setup.searchQuery.additionalConditions + ')';
                    }
                    return query;
                }
                 
                function SetMarkerCluster(){ 
                    var mc = $.data(elm,'markerCluster');
                    mc = new MarkerClusterer(
                        $.data(elm,'map'), 
                        $.data(elm,'mainMarkers').concat($.data(elm,'markers')),
                        	{ imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m' });
                    $.data(elm,'markerCluster', mc);
                }
                
                function SetInfoWindow(mrk, map){
                    var infowindows = $.data(elm,'infowindows');
                    var setupInfoWindow = $.data(elm,'setup').infoWindow;
                    var iws = $.grep(infowindows, function(e) { return e.sfId == mrk.sfId });              
                    if(iws && iws.length > 0){
                        var iw = iws[0];
                        if(iw.sfOpened) {
                            iw.close();
                            iw.sfOpened = false;
                        }
                        else {
                            iw.open(map, mrk);
                            iw.sfOpened = true;
                        }
                    }
                    else{
                        if(setupInfoWindow.queryContent){
                        	var query = setupInfoWindow.queryContent.replace('{id}', mrk.sfId);
                            sforce.connection.query(query, {
                                onSuccess : function(result) {
                                    if(result.records !== undefined){   
                                        var records = result.getArray("records");                                   
                                        var iw = new google.maps.InfoWindow({
                                            content: setupInfoWindow.content(records),
                                            sfId:mrk.sfId,
                                            sfOpened: true
                                        });
                                    }
                                    iw.open(map, mrk);
                                    var infowindows = $.data(elm,'infowindows');
                                    infowindows.push(iw);
                                    $.data(elm,'infowindows', infowindows);
                                },onFailure : function(error) {}
                            });                         
                        }
                        else {                                  
                            var iw = new google.maps.InfoWindow({
                                content: setupInfoWindow.content(mrk.sfId),
                                sfId:mrk.sfId,
                                sfOpened: true
                            });
                            iw.open(map, mrk);
                            infowindows.push(iw);
                            $.data(elm,'infowindows', infowindows);
                        }
                    }
                }  
                              
                var methods = {
                    init : function(setup) {
                        var defaultSetup = {
                                mapCallbackFn: 'mapCallbackFn',
                                searchQuery: {
                                    table: 'account',
                                    latField: 'BillingLatitude',
                                    lngField: 'BillingLongitude',
                                    title: 'Name',
                                    additionalFields: '',
                                    additionalConditions: ''
                                },
                                mainMarkers: [{
                                    lat: 40.0782400,
                                    lng: -83.1299340,
                                    title: 'Husky Marketing and Supply Company',
                                    sfId: '0'
                                }],
                                mapZoom: 14,
                                mapMinZoom: 2,
                                mainMarkerIcon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                                aroundMarkerIcon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                                highlightMarkerIcon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
                                onMarkerClick: function(id){ },
                                enableMarkerCluster: false,
                                markerClusterUrl:'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js',
                            	infoWindow: {
                                    enable: false, 
                                    queryContent: "select ID,Name,BillingAddress from account where id ='{id}'",
                                	content: function(data){ return JSON.stringify(data); }
                                }
                        };                  
                        var setup =  $.extend(true, defaultSetup, setup);
                        if(setup.mapKey === undefined) {
                            console.log('Map key missing. See https://developers.google.com/maps/documentation/javascript/get-api-key');
                            return;
                        }               
                        $.data(elm,'setup',setup);
                        var mapLink = 'https://maps.googleapis.com/maps/api/js?key=' + setup.mapKey + '&callback=' + setup.mapCallbackFn;
                        if(setup.enableMarkerCluster){
                            j$.getScript(setup.markerClusterUrl)
                            .done(function( script, textStatus ) { 
                                j$.getScript(mapLink);
                            })
                            .fail(function( jqxhr, settings, exception ) { 
                                setup.enableMarkerCluster = false;
                                j$.getScript(mapLink);
                            }); 
                        }
                        else {
                            j$.getScript(mapLink);  
                        }                               
                    },
                    load : function(mainMarkersPoint) {
                        Clear();
                        var setup =  $.data(elm,'setup');
                        var bounds = new google.maps.LatLngBounds();
                        var mainMarkers = [];
                        var map = new google.maps.Map(elm ,{
                            zoom: setup.mapZoom, 
                            minZoom: setup.mapMinZoom 
                        });
                        var infowindows = [];
                        if(mainMarkersPoint !== undefined){
                            setup.mainMarkers = mainMarkersPoint;
                        }
                        for(i in setup.mainMarkers)
                            mainMarkers.push(SetMarkerAndBound(setup.mainMarkers[i], bounds, map, setup.mainMarkerIcon, setup.onMarkerClick));
                        $.data(elm,'mainMarkers', mainMarkers);
                        $.data(elm,'bounds', bounds);
                        $.data(elm,'map', map);
                        $.data(elm,'infowindows', infowindows);
                        map.fitBounds(bounds);
                        if(setup.enableMarkerCluster) SetMarkerCluster();
                        google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
                            var setup = $.data(elm,'setup');
                            if (this.getZoom() > setup.mapZoom){ this.setZoom(setup.mapZoom); }
                            google.maps.event.addListener(map, 'idle', function() {
                                var map = $.data(elm,'map');
                                var setup = $.data(elm,'setup');  
                                var mapBounds = map.getBounds();
                                var cornerBounds = {
                                    latMin: mapBounds.getSouthWest().lat(),
                                    latMax: mapBounds.getNorthEast().lat(),
                                    lngMin: mapBounds.getSouthWest().lng(),
                                    lngMax: mapBounds.getNorthEast().lng(),
                                };                                                        
                                if(!UpdateboundsLimits(cornerBounds)) return;                                                     
                                var setup = $.data(elm,'setup');
                                var markers = $.data(elm,'markers'); 
                                var mainMarkers = $.data(elm,'mainMarkers');
                            	if(mainMarkers.length == 0) return;
                                var result = sforce.connection.query(GetQuery(setup, cornerBounds), {
                                    onSuccess : function(result) {
                                        if(result.records !== undefined){
                                            var records = result.getArray('records');
                                            for (var i=0; i < records.length; i++) {
                                                if($.grep(mainMarkers, function(e) { return e.sfId == records[i].Id }).length == 0 &&
                                                   $.grep(markers, function(e) { return e.sfId == records[i].Id }).length == 0){
                                                     var marker = new google.maps.Marker({
                                                            position: new google.maps.LatLng(records[i][setup.searchQuery.latField], 
                                                                                             records[i][setup.searchQuery.lngField]),
                                                            title:records[i][setup.searchQuery.title],
                                                         	map: map,
                                                            icon: setup.aroundMarkerIcon,
                                                            sfId: records[i].Id
                                                        });
                                                   
                                                    marker.addListener('click', function() {
                                                        var mrk = this;
                                                        if( setup.infoWindow.enable) SetInfoWindow(mrk, map);
                                                        setup.onMarkerClick(mrk.sfId);
                                                    });
                                                    markers.push(marker);
                                                }
                                            }
                                            $.data(elm,'markers', markers);
                                            if( $.data(elm,'setup').enableMarkerCluster) SetMarkerCluster();                                            
                                        }
                                    },
                                    onFailure : function(error) {
                                        console.log(error);
                                    }
                                });  
                            });
                        });
                    },
                    setHighlight: function(items){
                        var setup = $.data(elm,'setup')
                        var hlItems = ($.grep($.data(elm,'mainMarkers'), function(e) { return $.inArray(e.sfId,items) != -1 }))
                            .concat($.grep($.data(elm,'markers'), function(e) { return $.inArray(e.sfId,items) != -1}));
                        for(var i = 0; i <hlItems.length; i++){
                            if(hlItems[i].icon != setup.highlightMarkerIcon){
                                hlItems[i].sfPreviousIcon = hlItems[i].icon;
                                hlItems[i].setIcon(setup.highlightMarkerIcon);
                            }
                        }
                    },
                    removeHighlight: function(items){
                        var hlItems = ($.grep($.data(elm,'mainMarkers'), function(e) { return $.inArray(e.sfId,items) != -1 }))
                            .concat($.grep($.data(elm,'markers'), function(e) { return $.inArray(e.sfId,items) != -1 }));
                        for(var i = 0; i <hlItems.length; i++){
                            if(hlItems[i].sfPreviousIcon !== undefined){
                                hlItems[i].setIcon(hlItems[i].sfPreviousIcon);
                                delete hlItems[i].sfPreviousIcon;
                            }
                        }
                    },
                    clear: function(){
                    	Clear();
                    }
                };
    
                if (methods[methodOrOptions] ) {
                    return methods[methodOrOptions](opts);
                } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
                    return methods.init(methodOrOptions);
                } else {
                    console.log( 'Method does not exist.');
                }   
            });             
        }
    });
})(jQuery);