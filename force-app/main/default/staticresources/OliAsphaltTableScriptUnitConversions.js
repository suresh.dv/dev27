	//Mass Conversion Ratios
	var TonnePerKilogram = 0.001;
	var UsTonPerKilogram = 0.0011023113;
	var TonnePerUsTon = 1/1.1023113;

	//Volume Conversion Ratios
	var LitersPerDrum = 205;
	var LitersPerTote = 1000;
	var LitersPerCubicMeter = 1000;
	var LitersPerUsGalon = 3.785411;
	var LitersPerBarrel = 158.987295; // added
	var DrumPerTote = 4.8780487;
	var DrumPerCubicMeter = 4.8780487;
	/*public static final DrumPerUsGalon = */

	function getConversionRatio(convertTo, convertFrom, density) {
        
        convertTo = (convertTo == 'Meter 3') ? 'Meter3' : convertTo;
		convertFrom = (convertFrom == 'Meter 3') ? 'Meter3' : convertFrom;
		convertTo = (convertTo == 'US Gallon') ? 'USGallon' : convertTo;
		convertFrom = (convertFrom == 'US Gallon') ? 'USGallon' : convertFrom;
		convertTo = (convertTo == 'US Ton') ? 'USTon' : convertTo;
		convertFrom = (convertFrom == 'US Ton') ? 'USTon' : convertFrom;

        if(convertTo == convertFrom) {
            return 1;
        }
        
        if(convertFrom == 'Flat') { // added to cover "flat" unit
            return 1;
        }
        
        if(convertFrom == '') { // covers missing unit
            return 1;
        }

		//Mass Conversions
		if(convertTo == 'Tonne' && convertFrom == 'Kilogram') {
			return TonnePerKilogram;
		} 
		if (convertTo == 'Tonne' && convertFrom == 'USTon') {
			return TonnePerUsTon;
		}
		if (convertTo == 'USTon' && convertFrom == 'Kilogram') {
			return UsTonPerKilogram;
		}
		if (convertTo == 'USTon' && convertFrom == 'Tonne') {
			return 1 / TonnePerUsTon;
		}
		if (convertTo == 'Kilogram' && convertFrom == 'Tonne') {
			return 1 / TonnePerKilogram;
		} 
		if (convertTo == 'Kilogram' && convertFrom == 'USTon') {
			return 1 / UsTonPerKilogram;
		}

		//Volume Conversions
		if (convertTo == 'Liter' && convertFrom == 'Drum') {
			return LitersPerDrum;
		}
		if (convertTo == 'Liter' && convertFrom == 'Tote') {
			return LitersPerTote;
		}
		if (convertTo == 'Liter' && convertFrom == 'Meter3') {
			return LitersPerCubicMeter;
		}
		if (convertTo == 'Liter' && convertFrom == 'USGallon') {
			return LitersPerUsGalon;
		}
                if (convertTo == 'Liter' && convertFrom == 'Barrel') {
                    return LitersPerBarrel; //added
                }
		if (convertTo == 'Drum' && convertFrom == 'Liter') {
			return 1/LitersPerDrum;
		}
		if (convertTo == 'Drum' && convertFrom == 'Tote') {
			return LitersPerTote / LitersPerDrum;
		}
		if (convertTo == 'Drum' && convertFrom == 'Meter3') {
			return LitersPerCubicMeter / LitersPerDrum;
		}
		if (convertTo == 'Drum' && convertFrom == 'USGallon') {
			return LitersPerUsGalon / LitersPerDrum;
		}
                if (convertTo == 'Drum' && convertFrom == 'Barrel') {
                    return LitersPerBarrel / LitersPerDrum; //added
                }
		if (convertTo == 'Tote' && convertFrom == 'Liter') {
			return 1/LitersPerTote;
		}
		if (convertTo == 'Tote' && convertFrom == 'Drum') {
			return LitersPerDrum / LitersPerTote;
		}
		if (convertTo == 'Tote' && convertFrom == 'Meter3') {
			return LitersPerCubicMeter / LitersPerTote;
		}
		if (convertTo == 'Tote' && convertFrom == 'USGallon') {
			return LitersPerUsGalon / LitersPerTote;
		}
                if (convertTo == 'Tote' && convertFrom == 'Barrel') {
                    return LitersPerBarrel / LitersPerTote; //added
                }
		if (convertTo == 'Meter3' && convertFrom == 'Liter') {
			return 1/LitersPerCubicMeter;
		}
		if (convertTo == 'Meter3' && convertFrom == 'Drum') {
			return LitersPerDrum / LitersPerCubicMeter;
		}
		if (convertTo == 'Meter3' && convertFrom == 'Tote') {
			return LitersPerTote / LitersPerCubicMeter;
		}
		if (convertTo == 'Meter3' && convertFrom == 'USGallon') {
			return LitersPerUsGalon / LitersPerCubicMeter;
		}
                if (convertTo == 'Meter3' && convertFrom == 'Barrel') {
                    return LitersPerBarrel / LitersPerCubicMeter; //added
                }
		if (convertTo == 'USGallon' && convertFrom == 'Liter') {
			return 1/LitersPerUsGalon;
		}
		if (convertTo == 'USGallon' && convertFrom == 'Drum') {
			return LitersPerDrum / LitersPerUsGalon;
		}
		if (convertTo == 'USGallon' && convertFrom == 'Tote') {
			return LitersPerTote / LitersPerUsGalon;
		}
		if (convertTo == 'USGallon' && convertFrom == 'Meter3') {
			return LitersPerCubicMeter / LitersPerUsGalon;
		}
                if (convertTo == 'USGallon' && convertFrom == 'Barrel') {
                    return LitersPerBarrel / LitersPerUsGalon; //added
                }
            // to barrel conversion
            if (convertTo == 'Barrel' && convertFrom == 'Liter') {
                return 1/LitersPerBarrel; //added
            }
            if (convertTo == 'Barrel' && convertFrom == 'Drum') {
                return LitersPerDrum / LitersPerBarrel;
            }
            if (convertTo == 'Barrel' && convertFrom == 'Meter3') {
                return LitersPerCubicMeter / LitersPerBarrel;
            }
            if (convertTo == 'Barrel' && convertFrom == 'USGallon') {
                return LitersPerUsGalon / LitersPerBarrel;
            }
            if (convertTo == 'Barrel' && convertFrom == 'Tote') {
                return LitersPerTote / LitersPerBarrel;
            }
        
        


		//Mass to Volume
		if (convertTo == 'Liter' && convertFrom == 'Tonne') {
			return 1000 / density;
		}
		if (convertTo == 'Liter' && convertFrom == 'Kilogram') {
			return (1000 / density) * TonnePerKilogram; 
		}
		if (convertTo == 'Liter' && convertFrom == 'USTon') {
			return (1000 / density) * TonnePerUsTon;
		}
		if (convertTo == 'Drum' && convertFrom == 'Tonne') {
			return (1000 / density) / LitersPerDrum;
		}
		if (convertTo == 'Drum' && convertFrom == 'Kilogram') {
			return ((1000 / density) * TonnePerKilogram) / LitersPerDrum; 
		}
		if (convertTo == 'Drum' && convertFrom == 'USTon') {
			return ((1000 / density) * TonnePerUsTon) / LitersPerDrum;
		}
		if (convertTo == 'Tote' && convertFrom == 'Tonne') {
			return (1000 / density) / LitersPerTote;
		}
		if (convertTo == 'Tote' && convertFrom == 'Kilogram') {
			return ((1000 / density) * TonnePerKilogram) / LitersPerTote; 
		}
		if (convertTo == 'Tote' && convertFrom == 'USTon') {
			return ((1000 / density) * TonnePerUsTon) / LitersPerTote;
		}
		if (convertTo == 'Meter3' && convertFrom == 'Tonne') {
			return (1000 / density) / LitersPerCubicMeter;
		}
		if (convertTo == 'Meter3' && convertFrom == 'Kilogram') {
			return ((1000 / density) * TonnePerKilogram) / LitersPerCubicMeter; 
		}
		if (convertTo == 'Meter3' && convertFrom == 'USTon') {
			return ((1000 / density) * TonnePerUsTon) / LitersPerCubicMeter;
		}
		if (convertTo == 'USGallon' && convertFrom == 'Tonne') {
			return (1000 / density) / LitersPerUsGalon;
		}
		if (convertTo == 'USGallon' && convertFrom == 'Kilogram') {
			return ((1000 / density) * TonnePerKilogram) / LitersPerUsGalon; 
		}
		if (convertTo == 'USGallon' && convertFrom == 'USTon') {
			return ((1000 / density) * TonnePerUsTon) / LitersPerUsGalon;
		}
            //added barrel
            if (convertTo == 'Barrel' && convertFrom == 'Tonne') {
                return (1000 / density) / LitersPerBarrel;
            }
            if (convertTo == 'Barrel' && convertFrom == 'Kilogram') {
                return ((1000 / density) * TonnePerKilogram) / LitersPerBarrel; 
            }
            if (convertTo == 'Barrel' && convertFrom == 'USTon') {
                return ((1000 / density) * TonnePerUsTon) / LitersPerBarrel;
            }

		//Volume to Mass
		if(convertTo == 'Tonne') {
			if (convertFrom == 'Liter') {
				return density / 1000;
			} 
			if (convertFrom == 'Drum') {
				return (density / 1000) * LitersPerDrum;
			}
			if (convertFrom == 'Tote') {
				return (density / 1000) * LitersPerTote;
			}
			if (convertFrom == 'Meter3') {
				return (density / 1000) * LitersPerCubicMeter;
			}
			if (convertFrom == 'USGallon') {
				return (density / 1000) * LitersPerUsGalon;
			}
            		if (convertFrom == 'Barrel') {
				return (density / 1000) * LitersPerBarrel; //added
				}
		}
		if(convertTo == 'Kilogram') {
			if (convertFrom == 'Liter') {
				return (density / 1000) / TonnePerKilogram;
			} 
			if (convertFrom == 'Drum') {
				return ((density / 1000) * LitersPerDrum) / TonnePerKilogram;
			}
			if (convertFrom == 'Tote') {
				return ((density / 1000) * LitersPerTote) / TonnePerKilogram;
			}
			if (convertFrom == 'Meter3') {
				return ((density / 1000) * LitersPerCubicMeter) / TonnePerKilogram;
			}
			if (convertFrom == 'USGallon') {
				return ((density / 1000) * LitersPerUsGalon) / TonnePerKilogram;
			}
            if (convertFrom == 'Barrel') {
				return ((density / 1000) * LitersPerBarrel) / TonnePerKilogram; //added
			}
		}
		if(convertTo == 'USTon') {
			if (convertFrom == 'Liter') {
				return (density / 1000) / TonnePerUsTon;
			} 
			if (convertFrom == 'Drum') {
				return ((density / 1000) * LitersPerDrum) / TonnePerUsTon;
			}
			if (convertFrom == 'Tote') {
				return ((density / 1000) * LitersPerTote) / TonnePerUsTon;
			}
			if (convertFrom == 'Meter3') {
				return ((density / 1000) * LitersPerCubicMeter) / TonnePerUsTon;
			}
			if (convertFrom == 'USGallon') {
				return ((density / 1000) * LitersPerUsGalon) / TonnePerUsTon;
			}
            if (convertFrom == 'Barrel') {
				return ((density / 1000) * LitersPerBarrel) / TonnePerUsTon; // added
			}
		}

		
		return null;


	}