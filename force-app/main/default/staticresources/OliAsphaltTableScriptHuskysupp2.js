
var DeliveredCost_Huskysupp2;
var PartialDC_Huskysupp2;
var PartialDCNetback_Huskysupp2;

var RailCarFeeHuskysupp2Processed;
var Axle5huskysupp2VarProcessed;
var Axle6huskysupp2VarProcessed;
var Axle7huskysupp2VarProcessed;
var Axle8huskysupp2VarProcessed;

var MarginHuskysupp2;
var SalesPriceHuskysupp2;
var NetBackLoydminsterHuskysupp2;
var ApproximateMarginHuskysupp2;

// Function for Husky supplier 1 / #1 column at the table / #1 summary table
function DelivCostHuskySupp2Calculate(){    
    
    var ProductCost = document.getElementById("OliPage:form1:pageblock1:ProductCosthuskysupp2").value.replace(/,/g, '');
    var LloydProcessing = document.getElementById("OliPage:form1:pageblock1:LloydProcessinghuskysupp2").value.replace(/,/g, '');
    var FreightToTerminal = document.getElementById("OliPage:form1:pageblock1:Freight_to_terminal_huskysupp2").value.replace(/,/g, '');
    var FreighttoCustomer = document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value.replace(/,/g, '');
    var DifferentialVal = document.getElementById("OliPage:form1:pageblock1:Differential_huskysupp2").value.replace(/,/g, '');
    var MarketingFee = document.getElementById("OliPage:form1:pageblock1:Marketing_Fee_huskysupp2").value.replace(/,/g, '');
    var TerminalThroughputs = document.getElementById("OliPage:form1:pageblock1:Terminal_Throughputs_huskysupp2").value.replace(/,/g, '');
    var ChemicalCosts = document.getElementById("OliPage:form1:pageblock1:Chemical_Costs_huskysupp2").value.replace(/,/g, '');
    var ModificationCosts = document.getElementById("OliPage:form1:pageblock1:Modification_Costs_huskysupp2").value.replace(/,/g, '');
    var Railcarfee = document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value.replace(/,/g, '');
    
    var marginCheckboxPrct = document.getElementById("OliPage:form1:pageblock1:margin1Percentage").checked;
    	
	//Delivered Cost partial calculation for Netback at Summary Table use
 	   PartialDCNetback_Huskysupp2 = parseFloat(FreightToTerminal) + parseFloat(DifferentialVal) + parseFloat(MarketingFee) + parseFloat(TerminalThroughputs) + parseFloat(ChemicalCosts) + parseFloat(ModificationCosts);
    //Delivered Cost partial calculation - For Summary Table use
 	   PartialDC_Huskysupp2 = parseFloat(ProductCost) + parseFloat(LloydProcessing) + parseFloat(PartialDCNetback_Huskysupp2);
    //Delivered Cost calculation
  	  DeliveredCost_Huskysupp2 = parseFloat(PartialDC_Huskysupp2) + parseFloat(FreighttoCustomer) + parseFloat(Railcarfee);
 	   document.getElementById("OliPage:form1:pageblock1:DelivCosthuskysupp2").value = thousands_separators(DeliveredCost_Huskysupp2.toFixed(6));
    
    //Decide how to calculate Margin - as $ value or % according selected checkbox
    // trigger next function:  
   
            if (marginCheckboxPrct)
        {
            MarginPrctHuskySupp2Calculate();
        } 
        else
        {	
            MarginHuskySupp2Calculate();
        }
}
// END OF DELIVERED COST CALCULATION

// MARGIN / get margin value out of delivered cost and margin %
function MarginPrctHuskySupp2Calculate(){ 
    var impt2 = document.getElementById("OliPage:form1:pageblock1:DelivCosthuskysupp2").value.replace(/,/g, '');
    var impt4 = document.getElementById("OliPage:form1:pageblock1:MarginPrcthuskysupp2").value.replace(/,/g, ''); 
    var impt3 = (parseFloat(impt2) * parseFloat(impt4)) / 100;
    document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp2").value = impt3.toFixed(6);
    
    // trigger next function:    
    SetMarginVarhuskysupp2();
}

// MARGIN % / get margin % value out of delivered cost and margin

function MarginHuskySupp2Calculate(){ 
    var impt2 = document.getElementById("OliPage:form1:pageblock1:DelivCosthuskysupp2").value.replace(/,/g, '');
    var impt3 = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp2").value.replace(/,/g, '');
    var impt4 = (parseFloat(impt3) / parseFloat(impt2) * 100);
    
     if (isNaN(impt4)) impt4 = 0;
    
    document.getElementById("OliPage:form1:pageblock1:MarginPrcthuskysupp2").value = impt4.toFixed(6); 
    
    // trigger next function:    
    SetMarginVarhuskysupp2();
}

function SetMarginVarhuskysupp2(){
    
    var setmarginvartemp = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp2").value.replace(/[^0-9.-]+/g,"");
    MarginHuskysupp2 = setmarginvartemp;
  //  alert("A" + MarginHuskysupp2);
     // trigger next function:    
    SalespriceSelectorHuskysupp2();
}

function SalespriceSelectorHuskysupp2 (){
//   var SalespriceVAL = document.getElementById("OliPage:form1:pageblock1:salesPrice2").checked;
    var SalespriceBID = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2").checked;

         if (SalespriceBID)
        {
            SalespriceSelectedBIDHuskysupp2();
        } 
    else
        {
            SalespriceSelectedVALHuskysupp2();
        }
    
}
// SALES PRICE CALCULATION
function SalespriceSelectedVALHuskysupp2(){
    var marginHuskysupp2 = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp2").value.replace(/,/g, '');
    SalesPriceHuskysupp2 = parseFloat(DeliveredCost_Huskysupp2) + parseFloat(marginHuskysupp2);
    document.getElementById("OliPage:form1:pageblock1:salesPriceHuskysupp2").value = SalesPriceHuskysupp2.toFixed(6);
// trigger next function:    
    NetBackLoydminsterHuskysupp2Calculate();
    //added:
    ApproximateMarginHuskysupp2Calculate();
    USDValuesCalculateHuskysupp2();
    SummaryTableCalculate();
}

function SalespriceSelectedBIDHuskysupp2(){
	var SalespriceBid = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value.replace(/,/g, '');
    SalesPriceHuskysupp2 = SalespriceBid;
    // fill salesprice val field / doesnt enter the calculation here:
    var marginHuskysupp2 = document.getElementById("OliPage:form1:pageblock1:Marginhuskysupp2").value.replace(/,/g, '');
    var SalesPriceHuskysupp2DisplayVal = parseFloat(DeliveredCost_Huskysupp2) + parseFloat(marginHuskysupp2);
    document.getElementById("OliPage:form1:pageblock1:salesPriceHuskysupp2").value = SalesPriceHuskysupp2DisplayVal.toFixed(6);   
    
// trigger next function:    
    NetBackLoydminsterHuskysupp2Calculate();   
    //added:
    ApproximateMarginHuskysupp2Calculate();
    USDValuesCalculateHuskysupp2();
    SummaryTableCalculate();
    
}
// SALES PRICE CALCULATION end

// NETBACK LLOYDMINSTER CALCULATION
function NetBackLoydminsterHuskysupp2Calculate(){ 
    
    var FreightToCustomerHuskysupp2Current = document.getElementById("OliPage:form1:pageblock1:FreighttoCustomerhuskysupp2").value;
    var RailcarToHuskysupp2Current = document.getElementById("OliPage:form1:pageblock1:RailCarFeehuskysupp2").value;
    
    NetBackLoydminsterHuskysupp2 = parseFloat(SalesPriceHuskysupp2) - ( parseFloat(RailcarToHuskysupp2Current) + parseFloat(FreightToCustomerHuskysupp2Current) + parseFloat(PartialDCNetback_Huskysupp2));
    document.getElementById("OliPage:form1:pageblock1:NetbackLloydminsterhuskysupp2").value = NetBackLoydminsterHuskysupp2.toFixed(6);

     // trigger next function:    
//  	ApproximateMarginHuskysupp2Calculate();
}

// APPROXIMATE MARGIN CALCULATION
function ApproximateMarginHuskysupp2Calculate(){
    
	ApproximateMarginHuskysupp2 = (( parseFloat(SalesPriceHuskysupp2) - parseFloat(DeliveredCost_Huskysupp2) ) / parseFloat(SalesPriceHuskysupp2)) * 100;
    
    if (isNaN(ApproximateMarginHuskysupp2)) ApproximateMarginHuskysupp2 = 0;
	if (!isFinite(ApproximateMarginHuskysupp2)) ApproximateMarginHuskysupp2 = 0;   // replacing infinity value by 0
    
    document.getElementById("OliPage:form1:pageblock1:ApproxMarginHuskysupp2id").value = ApproximateMarginHuskysupp2.toFixed(6);
    
    // trigger next function:    
//  	USDValuesCalculateHuskysupp2();
}

function USDValuesCalculateHuskysupp2() {
    
	ExchangeRate = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value;
    
    var DeliveredCostUSD = parseFloat(ExchangeRate) * parseFloat(DeliveredCost_Huskysupp2);
    document.getElementById("OliPage:form1:pageblock1:DeliveredCostUSDHuskysupp2").value = DeliveredCostUSD.toFixed(6);
    var ApproxMarginUSD = parseFloat(ExchangeRate) * parseFloat(MarginHuskysupp2);
    document.getElementById("OliPage:form1:pageblock1:ApproxMarginUSDHuskysupp2").value = ApproxMarginUSD.toFixed(6);
    var SalesPriceUSD = parseFloat(ExchangeRate) * parseFloat(SalesPriceHuskysupp2);
    document.getElementById("OliPage:form1:pageblock1:SalesPriceUSDHuskysupp2").value = SalesPriceUSD.toFixed(6);
    var NetBackLloydUSD = parseFloat(ExchangeRate) * parseFloat(NetBackLoydminsterHuskysupp2);
    document.getElementById("OliPage:form1:pageblock1:NetBackLloydHuskysupp2").value = NetBackLloydUSD.toFixed(6);
    
   // trigger next function:    

ReplaceZerosHuskysupp2(); // Format values to form without unnecessary zeros or invalid inputs.

}



//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE LAUNCHER / launch particular variation of Summary table when conditions meet
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableCalculateHuskysupp2Launcher() {
    var SelectedSalespriceBIDcheckbox = document.getElementById("OliPage:form1:pageblock1:salesPriceBID2").checked; 
	var SelectedCurrencyStatusforTable = document.getElementById("OliPage:form1:pageblock1:CurrencyPicklist").value;

    if(SelectedSalespriceBIDcheckbox == true && SelectedCurrencyStatusforTable == 'CAD') 
    {
    	SummaryTableHuskysupp2CalculateBID();  
    }
	else if (SelectedSalespriceBIDcheckbox == true && SelectedCurrencyStatusforTable == 'US') 
    {
    	SummaryTableHuskysupp2CalculateBID_USD();
    }
    else if (SelectedSalespriceBIDcheckbox == false && SelectedCurrencyStatusforTable == 'US') 
    {
        SummaryTableHuskysupp2CalculateUSD();
	}
    else
    {
        SummaryTableHuskysupp2Calculate();
	}
    
    fillSummaryTable(); //feeds salesforce fields in order to store values. 

}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE / default values
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp2Calculate (){   
document.getElementById("selected_supplier_display").value= " default view";
 	var partialSalesPrice =  parseFloat(PartialDC_Huskysupp2) + parseFloat(MarginHuskysupp2);  
    
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = PartialDC_Huskysupp2 + Axle5huskysupp2VarProcessed;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = PartialDC_Huskysupp2 + Axle6huskysupp2VarProcessed;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = PartialDC_Huskysupp2 + Axle7huskysupp2VarProcessed;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = PartialDC_Huskysupp2 + Axle8huskysupp2VarProcessed;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = PartialDC_Huskysupp2 + RailCarFeeHuskysupp2Processed;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
    var HS1_DC_PU = parseFloat(PartialDC_Huskysupp2);
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
// Summary table calculation FOR MARGIN  
    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;  
    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) * 100;
     if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0; 
    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) * 100;
     if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
    
    var HS1_Margin_PU = ( parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp2) ) / parseFloat(partialSalesPrice) * 100;
                         if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
    
// Summary table calculation FOR NETBACK  
    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed)));
    	document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed)));
    	document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed)));
    	document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);
    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed)));
    	document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed)));
    	document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);
    var HS1_Netback_PU = parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp2);
    	document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);
    
// Summary table calculation for SALESPRICE (original)
/*	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp2);
      document.getElementById("SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp2);
      document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp2);
      document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp2);
      document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2);
      document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp2);
      document.getElementById("SalespricePUcalc").value = HS1_SP_PU.toFixed(6);   */
     
    // Summary table calculation for SALESPRICE (edited)
	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp2);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp2);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp2);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp2);
      document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2);
      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp2);
      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
}
//____________________________________________________________________________________________________________________



//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE BID / salesprice bid included to calculations
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp2CalculateBID(){  
document.getElementById("selected_supplier_display").value= "   (Sales Price BID included)";
    var SalesPriceBIDvalue = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value.replace(/,/g, '');
    var partialSalesPrice =  parseFloat(PartialDC_Huskysupp2) + parseFloat(MarginHuskysupp2);  
    AxleRateSelectorStatus = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;
    //vars for applying bid prices
    var applyBIDpricePU = document.getElementById("OliPage:form1:pageblock1:IncludePU_bid");
    var applyBIDpriceRC = document.getElementById("OliPage:form1:pageblock1:IncludeRC_bid"); 
    var applyBIDpriceA5 = document.getElementById("OliPage:form1:pageblock1:IncludeA5_bid"); 
    var applyBIDpriceA6 = document.getElementById("OliPage:form1:pageblock1:IncludeA6_bid"); 
    var applyBIDpriceA7 = document.getElementById("OliPage:form1:pageblock1:IncludeA7_bid"); 
    var applyBIDpriceA8 = document.getElementById("OliPage:form1:pageblock1:IncludeA8_bid"); 
        
    
    // Summary table calculation FOR DELIVERED COST BID
    var HS1_DC_A5 = PartialDC_Huskysupp2 + Axle5huskysupp2VarProcessed;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = PartialDC_Huskysupp2 + Axle6huskysupp2VarProcessed;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = PartialDC_Huskysupp2 + Axle7huskysupp2VarProcessed;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = PartialDC_Huskysupp2 + Axle8huskysupp2VarProcessed;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = PartialDC_Huskysupp2 + RailCarFeeHuskysupp2Processed;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
	var HS1_DC_PU = parseFloat(PartialDC_Huskysupp2);
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
   // Summary table calculation for SALESPRICE BID

    //AXLE 5 ROW AT SUMMARY TABLE
    if(AxleRateSelectorStatus == 'Axle 5 Price' && applyBIDpriceA5.checked == false)   // if(AxleRateSelectorStatus == 'Axle 5 Price') 
    {
        // salesprice
        document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = SalesPriceBIDvalue;
        // netback
        var HS1_Netback_A5_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed)));
        document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
        // margin
        var HS1_Margin_A5_bid = (SalesPriceBIDvalue - HS1_DC_A5) / HS1_DC_A5 * 100;
        if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
        if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
    }
    
	// Salesprice BID summary table controller
    else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus != 'Axle 5 Price' ) {

        	var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed)));	
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing NaN value by 0
			if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
    }
    
    else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus == 'Axle 5 Price') {
        
       		 var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed)));
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
			if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA5; 
            SalesPriceHuskysupp2 = SalesPriceBIDA5; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
    }    
	// Salesprice BID summary table controller - end

                else
                {
                    //salesprice
                    var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp2); 
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
                    // netback
                    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed)));
                    document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);       
                    // margin
                    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
					if (!isFinite(HS1_Margin_A5)) HS1_Margin_A5 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
                    
                }
    //AXLE 6 ROW AT SUMMARY TABLE
    if(AxleRateSelectorStatus == 'Axle 6 Price' && applyBIDpriceA6.checked == false) //if(AxleRateSelectorStatus == 'Axle 6 Price') 
        {
            //salesprice
         //   document.getElementById("SalespriceA6").value = SalesPriceBIDvalue;
             document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDvalue - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
			if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
            
        }
    
// Salesprice BID summary table controller
    else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus != 'Axle 6 Price' ) {
        
        var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, '');
        // netback
        var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed)));
        document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
        // margin
        var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
        if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
        if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);

    }
    
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus == 'Axle 6 Price') {
            
            var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed)));
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA6; 
            SalesPriceHuskysupp2 = SalesPriceBIDA6; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();

        }    
    // Salesprice BID summary table controller - end
	
                else
                {
                    //salesprice
                    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp2);
               //     document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
                           document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    // netback
                    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed)));
                    document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6); 
                    // margin
                    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
                    if (!isFinite(HS1_Margin_A6)) HS1_Margin_A6 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
                } 
    
    //AXLE 7 ROW AT SUMMARY TABLE
       if(AxleRateSelectorStatus == 'Axle 7 Price' && applyBIDpriceA7.checked == false) // if(AxleRateSelectorStatus == 'Axle 7 Price') 
        {
            //salesprice
        //    document.getElementById("SalespriceA7").value = SalesPriceBIDvalue;
            document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed)));
			document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6); 
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDvalue - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus != 'Axle 7 Price' ) {
        
        var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, '');
        // netback
        var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed)));
        document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
        // margin
        var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
        if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
        if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus == 'Axle 7 Price') {
            
            var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed)));
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA7; 
            SalesPriceHuskysupp2 = SalesPriceBIDA7; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }    
    // Salesprice BID summary table controller - end
 
                else
                {
                    //salesprice
                    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp2); 
                 //   document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
					document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    // netback
                    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed)));
                    document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);    
                    // margin
                    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
                    if (!isFinite(HS1_Margin_A7)) HS1_Margin_A7 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
                }
    
    //AXLE 8 ROW AT SUMMARY TABLE
	if(AxleRateSelectorStatus == 'Axle 8 Price' && applyBIDpriceA8.checked == false) //  if(AxleRateSelectorStatus == 'Axle 8 Price') 
        {
            //salesprice
      //      document.getElementById("SalespriceA8").value = SalesPriceBIDvalue;
            document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed)));
			document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6); 
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDvalue - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus != 'Axle 8 Price' ) {
        
        var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed)));
        document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
        // margin
        var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
        if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
        if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus == 'Axle 8 Price') {
            
            var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed)));
            document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA8; 
            SalesPriceHuskysupp2 = SalesPriceBIDA8; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }  
    
    // Salesprice BID summary table controller - end
                else
                {
                    //salesprice
                    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp2);
               //     document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    // netback
                    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed)));
                    document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
                    // margin
                    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
                    if (!isFinite(HS1_Margin_A8)) HS1_Margin_A8 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
                } 
   
    //RAIL CAR ROW AT SUMMARY TABLE
	if(AxleRateSelectorStatus == 'Railcar' && applyBIDpriceRC.checked == false) // if(AxleRateSelectorStatus == 'Railcar') 
        {
            //salesprice
        //    document.getElementById("SalespriceRC").value = SalesPriceBIDvalue;
            document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed)));
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDvalue - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus != 'Railcar' ) {
        
        var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, '');
        // netback
        var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed)));
        document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
        // margin
        var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
        if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
        if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
    }
    
        else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus == 'Railcar') {
            
            var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, '');
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed)));
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDRC; 
            SalesPriceHuskysupp2 = SalesPriceBIDRC; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }    
    // Salesprice BID summary table controller - end

                else
                {
                    //salesprice
                    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2);
                 //   document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    // netback
                    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed)));
                    document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);     
                    // margin
                    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) * 100;
                    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
					if (!isFinite(HS1_Margin_RC)) HS1_Margin_RC = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
                } 
    
     //PICKUP ROW AT SUMMARY TABLE
   	if(AxleRateSelectorStatus == 'Pickup' && applyBIDpricePU.checked == false) // if(AxleRateSelectorStatus == 'Pickup') 
        {
            //salesprice
              document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = SalesPriceBIDvalue;
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDvalue - parseFloat(PartialDCNetback_Huskysupp2);
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDvalue - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus != 'Pickup' ) {
        
        var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp2) ;
        document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
        // margin
        var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
        if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
        if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
    }
    
        else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus == 'Pickup') {
            
            var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp2);
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDPU; 
            SalesPriceHuskysupp2 = SalesPriceBIDPU; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }    
    // Salesprice BID summary table controller - end 
    
                else
                {
                    //salesprice
                    var HS1_SP_PU = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2);
                       document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
                    // netback
                    var HS1_Netback_PU = parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp2);
                    document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);     
                    // margin
                    var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp2)) / parseFloat(partialSalesPrice) * 100;
                    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
                    if (!isFinite(HS1_Margin_PU)) HS1_Margin_PU = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
                } 
}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE USD / salesprice USD included to calculations
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp2CalculateUSD(){  
document.getElementById("selected_supplier_display").value= "   (USD currency)";
    
    	var ExchaneRateUSD = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value;
    	var partialSalesPrice =  parseFloat(PartialDC_Huskysupp2) + parseFloat(MarginHuskysupp2);  
    
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = (PartialDC_Huskysupp2 + Axle5huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = (PartialDC_Huskysupp2 + Axle6huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = (PartialDC_Huskysupp2 + Axle7huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = (PartialDC_Huskysupp2 + Axle8huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = (PartialDC_Huskysupp2 + RailCarFeeHuskysupp2Processed) * ExchaneRateUSD;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
    var HS1_DC_PU = PartialDC_Huskysupp2 * ExchaneRateUSD;
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
// Summary table calculation FOR MARGIN  
    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) * 100;
    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) * 100;
    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
	var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp2)) / parseFloat(partialSalesPrice) * 100;
    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
    
// Summary table calculation FOR NETBACK  
    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6);
    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) * ExchaneRateUSD;
    	document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);
	var HS1_Netback_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp2)) * ExchaneRateUSD;
    	document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);
    
// Summary table calculation for SALESPRICE
/*	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("SalespricePU").value = HS1_SP_PU.toFixed(6); */
    
    // Summary table calculation for SALESPRICE
	var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
    var HS1_SP_PU = parseFloat(HS1_DC_PU) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
    
    
}
//____________________________________________________________________________________________________________________


//--------------------------------------------------------------------------------------------------------------------
// SUMMARY TABLE SALESPRICE BID USD / salesprice bid included to calculations USD
//--------------------------------------------------------------------------------------------------------------------
function SummaryTableHuskysupp2CalculateBID_USD(){  
document.getElementById("selected_supplier_display").value= "   (Sales Price BID at USD currency)";
    
    var ExchaneRateUSD = document.getElementById("OliPage:form1:pageblock1:ExchangeRate").value.replace(/,/g, '');
    var SalesPriceBIDvalue = document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value.replace(/,/g, '');
    var partialSalesPrice =  parseFloat(PartialDC_Huskysupp2) + parseFloat(MarginHuskysupp2);  
    AxleRateSelectorStatus = document.getElementById("OliPage:form1:pageblock1:AxleRateSelector").value;
    //vars for applying bid prices    
    var applyBIDpricePU = document.getElementById("OliPage:form1:pageblock1:IncludePU_bid");
    var applyBIDpriceRC = document.getElementById("OliPage:form1:pageblock1:IncludeRC_bid"); 
    var applyBIDpriceA5 = document.getElementById("OliPage:form1:pageblock1:IncludeA5_bid"); 
    var applyBIDpriceA6 = document.getElementById("OliPage:form1:pageblock1:IncludeA6_bid"); 
    var applyBIDpriceA7 = document.getElementById("OliPage:form1:pageblock1:IncludeA7_bid"); 
    var applyBIDpriceA8 = document.getElementById("OliPage:form1:pageblock1:IncludeA8_bid");
 
// Summary table calculation FOR DELIVERED COST
    var HS1_DC_A5 = (PartialDC_Huskysupp2 + Axle5huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A5_DC").value = HS1_DC_A5.toFixed(6);
    var HS1_DC_A6 = (PartialDC_Huskysupp2 + Axle6huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A6_DC").value = HS1_DC_A6.toFixed(6);
    var HS1_DC_A7 = (PartialDC_Huskysupp2 + Axle7huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A7_DC").value = HS1_DC_A7.toFixed(6);
    var HS1_DC_A8 = (PartialDC_Huskysupp2 + Axle8huskysupp2VarProcessed) * ExchaneRateUSD;
      document.getElementById("supplier1_A8_DC").value = HS1_DC_A8.toFixed(6);
    var HS1_DC_RC = (PartialDC_Huskysupp2 + RailCarFeeHuskysupp2Processed) * ExchaneRateUSD;
      document.getElementById("supplier1_RC_DC").value = HS1_DC_RC.toFixed(6);
	var HS1_DC_PU = PartialDC_Huskysupp2 * ExchaneRateUSD;
      document.getElementById("supplier1_PU_DC").value = HS1_DC_PU.toFixed(6);
    
   // Summary table calculation for SALESPRICE BID

    //AXLE 5 ROW AT SUMMARY TABLE
     if(AxleRateSelectorStatus == 'Axle 5 Price' && applyBIDpriceA5.checked == false) // if(AxleRateSelectorStatus == 'Axle 5 Price') 
        {
            // salesprice
         //   document.getElementById("SalespriceA5").value = SalesPriceBIDvalue * ExchaneRateUSD;
           document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
//                       document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = SalesPriceBIDvalue * ExchaneRateUSD;
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);  
             // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus != 'Axle 5 Price' ) {
        
        var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed)));
        document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
        // margin
        var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
        if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
        if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA5.checked == true && AxleRateSelectorStatus == 'Axle 5 Price') {
            
            var SalesPriceBIDA5 = document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value.replace(/,/g, '');
            // netback
            var HS1_Netback_A5_bid = (SalesPriceBIDA5) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5_bid.toFixed(6);
            // margin
            var HS1_Margin_A5_bid = (SalesPriceBIDA5 - HS1_DC_A5) / HS1_DC_A5 * 100;
            if (isNaN(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;
            if (!isFinite(HS1_Margin_A5_bid)) HS1_Margin_A5_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA5.toFixed(6);
            SalesPriceHuskysupp2 = SalesPriceBIDA5; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
            //alert('this');
        }    
    // Salesprice BID summary table controller - end
	
                else
                {
                    //salesprice
                    var HS1_SP_A5 = parseFloat(HS1_DC_A5) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD; 
                //    document.getElementById("SalespriceA5").value = HS1_SP_A5.toFixed(6);
                     document.getElementById("OliPage:form1:pageblock1:SalespriceA5").value = HS1_SP_A5.toFixed(6);
                    // netback
                    var HS1_Netback_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A5_Netback").value = HS1_Netback_A5.toFixed(6);
					// margin
                    var HS1_Margin_A5 = ((parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle5huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle5huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A5)) HS1_Margin_A5 = 0;
                    if (!isFinite(HS1_Margin_A5)) HS1_Margin_A5 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A5_Margin").value = HS1_Margin_A5.toFixed(6);
                }
    //AXLE 6 ROW AT SUMMARY TABLE
         if(AxleRateSelectorStatus == 'Axle 6 Price' && applyBIDpriceA6.checked == false) // if(AxleRateSelectorStatus == 'Axle 6 Price') 
        {
            //salesprice
            //   document.getElementById("SalespriceA6").value = SalesPriceBIDvalue * ExchaneRateUSD;
            //	document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = SalesPriceBIDvalue * ExchaneRateUSD;
			document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
			// margin
            var HS1_Margin_A6_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        }
    
    // Salesprice BID summary table controller
    else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus != 'Axle 6 Price' ) {
        
        var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed)));
        document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
        // margin
        var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
        if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
        if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
        
    }
    
        else if (applyBIDpriceA6.checked == true && AxleRateSelectorStatus == 'Axle 6 Price') {
            
            var SalesPriceBIDA6 = document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A6_bid = (SalesPriceBIDA6) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6_bid.toFixed(6);
            // margin
            var HS1_Margin_A6_bid = (SalesPriceBIDA6 - HS1_DC_A6) / HS1_DC_A6 * 100;
            if (isNaN(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;
            if (!isFinite(HS1_Margin_A6_bid)) HS1_Margin_A6_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA6; 
            SalesPriceHuskysupp2 = SalesPriceBIDA6; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
            
        }    
    // Salesprice BID summary table controller - end
 
                else
                {
                    //salesprice
                    var HS1_SP_A6 = parseFloat(HS1_DC_A6) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
                 //   document.getElementById("SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA6").value = HS1_SP_A6.toFixed(6);
                    // netback
                    var HS1_Netback_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A6_Netback").value = HS1_Netback_A6.toFixed(6);
                    // magrin
                    var HS1_Margin_A6 = ((parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle6huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle6huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A6)) HS1_Margin_A6 = 0;
                    if (!isFinite(HS1_Margin_A6)) HS1_Margin_A6 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A6_Margin").value = HS1_Margin_A6.toFixed(6);
                    
                } 
    //AXLE 7 ROW AT SUMMARY TABLE
        if(AxleRateSelectorStatus == 'Axle 7 Price' && applyBIDpriceA7.checked == false) // if(AxleRateSelectorStatus == 'Axle 7 Price') 
        {
            //salesprice
          //  document.getElementById("SalespriceA7").value = SalesPriceBIDvalue * ExchaneRateUSD;
          document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
			//	document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = SalesPriceBIDvalue * ExchaneRateUSD;
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) * ExchaneRateUSD;
			document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
        }
    
       // Salesprice BID summary table controller
    else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus != 'Axle 7 Price' ) {
        
        var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed)));
        document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
        // margin
        var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
        if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
        if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA7.checked == true && AxleRateSelectorStatus == 'Axle 7 Price') {
            
            var SalesPriceBIDA7 = document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A7_bid = (SalesPriceBIDA7) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7_bid.toFixed(6);
            // margin
            var HS1_Margin_A7_bid = (SalesPriceBIDA7 - HS1_DC_A7) / HS1_DC_A7 * 100;
            if (isNaN(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;
            if (!isFinite(HS1_Margin_A7_bid)) HS1_Margin_A7_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA7; 
            SalesPriceHuskysupp2 = SalesPriceBIDA7; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }    
    // Salesprice BID summary table controller - end
          
            else
                {
                    //salesprice
                    var HS1_SP_A7 = parseFloat(HS1_DC_A7) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD; 
                //    document.getElementById("SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA7").value = HS1_SP_A7.toFixed(6);
                    // netback
                    var HS1_Netback_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A7_Netback").value = HS1_Netback_A7.toFixed(6); 
                    // margin
                    var HS1_Margin_A7 = ((parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle7huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle7huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A7)) HS1_Margin_A7 = 0;
                    if (!isFinite(HS1_Margin_A7)) HS1_Margin_A7 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A7_Margin").value = HS1_Margin_A7.toFixed(6);
                    
                    
                }
    //AXLE 8 ROW AT SUMMARY TABLE
         if(AxleRateSelectorStatus == 'Axle 8 Price' && applyBIDpriceA8.checked == false) // if(AxleRateSelectorStatus == 'Axle 8 Price') 
        {
            //salesprice
            //  document.getElementById("SalespriceA8").value = SalesPriceBIDvalue * ExchaneRateUSD;
            document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            //	document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = SalesPriceBIDvalue * ExchaneRateUSD;
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDvalue) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) * ExchaneRateUSD;
			document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
             // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus != 'Axle 8 Price' ) {
        
        var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed)));
        document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
        // margin
        var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
        if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
        if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
    }
    
        else if (applyBIDpriceA8.checked == true && AxleRateSelectorStatus == 'Axle 8 Price') {
            
            var SalesPriceBIDA8 = document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_A8_bid = (SalesPriceBIDA8) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) * ExchaneRateUSD;
            document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8_bid.toFixed(6);
            // margin
            var HS1_Margin_A8_bid = (SalesPriceBIDA8 - HS1_DC_A8) / HS1_DC_A8 * 100;
            if (isNaN(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;
            if (!isFinite(HS1_Margin_A8_bid)) HS1_Margin_A8_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDA8; 
            SalesPriceHuskysupp2 = SalesPriceBIDA8; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }  
    // Salesprice BID summary table controller - end
    
                else
                {
                    //salesprice
                    var HS1_SP_A8 = parseFloat(HS1_DC_A8) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
                //    document.getElementById("SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    document.getElementById("OliPage:form1:pageblock1:SalespriceA8").value = HS1_SP_A8.toFixed(6);
                    // netback
                    var HS1_Netback_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_A8_Netback").value = HS1_Netback_A8.toFixed(6);
                    // margin
                    var HS1_Margin_A8 = ((parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(Axle8huskysupp2VarProcessed))) / (parseFloat(partialSalesPrice) + parseFloat(Axle8huskysupp2VarProcessed)) * 100;
                    if (isNaN(HS1_Margin_A8)) HS1_Margin_A8 = 0;
                    if (!isFinite(HS1_Margin_A8)) HS1_Margin_A8 = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_A8_Margin").value = HS1_Margin_A8.toFixed(6);
                } 
    //RAIL CAR ROW AT SUMMARY TABLE
        if(AxleRateSelectorStatus == 'Railcar' && applyBIDpriceRC.checked == false) // if(AxleRateSelectorStatus == 'Railcar') 
        {
            //salesprice
          //  document.getElementById("SalespriceRC").value = SalesPriceBIDvalue * ExchaneRateUSD;
 //             document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = SalesPriceBIDvalue * ExchaneRateUSD;
              document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);           
            // netback
            var HS1_Netback_RC_bid = ((SalesPriceBIDvalue) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) * ExchaneRateUSD;
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus != 'Railcar' ) {
        
        var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed)));
        document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
        // margin
        var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
        if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
        if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
    }
    
        else if (applyBIDpriceRC.checked == true && AxleRateSelectorStatus == 'Railcar') {
            
            var SalesPriceBIDRC = document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_RC_bid = (SalesPriceBIDRC) - ((parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) * ExchaneRateUSD;
            document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC_bid.toFixed(6);
            // margin
            var HS1_Margin_RC_bid = (SalesPriceBIDRC - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;
            if (!isFinite(HS1_Margin_RC_bid)) HS1_Margin_RC_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDRC; 
            SalesPriceHuskysupp2 = SalesPriceBIDRC; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }    
    // Salesprice BID summary table controller - end
               
            else
                {
                    //salesprice
                    var HS1_SP_RC = parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2) * ExchaneRateUSD;
                  //  document.getElementById("SalespriceRC").value = HS1_SP_RC.toFixed(6);
                      document.getElementById("OliPage:form1:pageblock1:SalespriceRC").value = HS1_SP_RC.toFixed(6);
                    // netback
                    var HS1_Netback_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDCNetback_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) * ExchaneRateUSD;
                    document.getElementById("supplier1_RC_Netback").value = HS1_Netback_RC.toFixed(6);   
                    // margin
                    var HS1_Margin_RC = ((parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) - (parseFloat(PartialDC_Huskysupp2) + parseFloat(RailCarFeeHuskysupp2Processed))) / (parseFloat(partialSalesPrice) + parseFloat(RailCarFeeHuskysupp2Processed)) * 100;
                    if (isNaN(HS1_Margin_RC)) HS1_Margin_RC = 0;
                    if (!isFinite(HS1_Margin_RC)) HS1_Margin_RC = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_RC_Margin").value = HS1_Margin_RC.toFixed(6);
                } 
    
      //PICKUP ROW AT SUMMARY TABLE
           if(AxleRateSelectorStatus == 'Pickup' && applyBIDpricePU.checked == false) // if(AxleRateSelectorStatus == 'Pickup') 
        {
            //salesprice
         //   document.getElementById("SalespricePU").value = SalesPriceBIDvalue * ExchaneRateUSD;
//            document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = SalesPriceBIDvalue * ExchaneRateUSD;
            document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = (SalesPriceBIDvalue * ExchaneRateUSD).toFixed(6);
            // netback
            var HS1_Netback_PU_bid = (SalesPriceBIDvalue - parseFloat(PartialDCNetback_Huskysupp2)) * ExchaneRateUSD;
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDvalue * ExchaneRateUSD - HS1_DC_RC) / HS1_DC_RC * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
        }
    
     // Salesprice BID summary table controller
    else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus != 'Pickup' ) {
        
        var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
        // netback
        var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp2) ;
        document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
        // margin
        var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
        if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
        if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
        document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
    }
    
        else if (applyBIDpricePU.checked == true && AxleRateSelectorStatus == 'Pickup') {
            
            var SalesPriceBIDPU = document.getElementById("OliPage:form1:pageblock1:SalespricePU").value.replace(/,/g, ''); 
            // netback
            var HS1_Netback_PU_bid = SalesPriceBIDPU - parseFloat(PartialDCNetback_Huskysupp2) * ExchaneRateUSD;
            document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU_bid.toFixed(6);
            // margin
            var HS1_Margin_PU_bid = (SalesPriceBIDPU - HS1_DC_PU) / HS1_DC_PU * 100;
            if (isNaN(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;
            if (!isFinite(HS1_Margin_PU_bid)) HS1_Margin_PU_bid = 0;  // replacing infinity value by 0
            document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU_bid.toFixed(6);
            
            //fill salesprice bid in main table
            document.getElementById("OliPage:form1:pageblock1:salesPriceBIDHuskysupp2").value = SalesPriceBIDPU; 
            SalesPriceHuskysupp2 = SalesPriceBIDPU; // fill custom bid to global var for table column
            // trigger recalculating main column:    
            NetBackLoydminsterHuskysupp2Calculate();   
            ApproximateMarginHuskysupp2Calculate();
            USDValuesCalculateHuskysupp2();
        }    
    // Salesprice BID summary table controller - end 
    
                else
                {
                    //salesprice
                    var HS1_SP_PU = (parseFloat(HS1_DC_RC) + parseFloat(MarginHuskysupp2)) * ExchaneRateUSD;
                 //   document.getElementById("SalespricePU").value = HS1_SP_PU.toFixed(6);
                      document.getElementById("OliPage:form1:pageblock1:SalespricePU").value = HS1_SP_PU.toFixed(6);
                    // netback
                    var HS1_Netback_PU = ( parseFloat(partialSalesPrice) - parseFloat(PartialDCNetback_Huskysupp2)) * ExchaneRateUSD;
                    document.getElementById("supplier1_PU_Netback").value = HS1_Netback_PU.toFixed(6);     
                    // margin
                    var HS1_Margin_PU = (parseFloat(partialSalesPrice) - parseFloat(PartialDC_Huskysupp2)) / parseFloat(partialSalesPrice) * 100;
                    if (isNaN(HS1_Margin_PU)) HS1_Margin_PU = 0;
                    if (!isFinite(HS1_Margin_PU)) HS1_Margin_PU = 0;  // replacing infinity value by 0
                    document.getElementById("supplier1_PU_Margin").value = HS1_Margin_PU.toFixed(6);
                } 
    
    
    
}
//____________________________________________________________________________________________________________________