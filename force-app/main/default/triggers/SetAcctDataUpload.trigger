trigger SetAcctDataUpload on Attachment (after insert, after update) 
{
    String title;
    Id parentId;
    Id createdById;
    Datetime createdDate;
    Id fileId;
    String description;
    Integer fileSize;
    
    List<LOM_Accounting_Data_Upload__c> aduList = new List<LOM_Accounting_Data_Upload__c>();
    
    for (Attachment att : Trigger.new)
    {
        title = att.Name;
        parentId = att.parentId;    
        createdById = att.CreatedById;
        createdDate = att.createdDate;
        fileId = att.Id;
        description = att.Description;
        fileSize = att.bodyLength;
        
        List<LOM_Accounting_Data_Upload__c> tempAduList = [SELECT Filename__c, Uploaded_By__c, Uploaded_Date_Time__c,
            File_Description__c, File_Size__c 
            FROM LOM_Accounting_Data_Upload__c WHERE Id = :parentId];

        if (tempAduList.size() == 1)
        {
            tempAduList[0].Filename__c = title;
            tempAduList[0].Uploaded_By__c = createdById;
            tempAduList[0].Uploaded_Date_Time__c = createdDate;
            tempAduList[0].File_Id__c = fileId;
            tempAduList[0].File_Description__c = description;
            tempAduList[0].File_Size__c = fileSize;
            aduList.add(tempAduList[0]);
        }       
    }
    if (aduList.size() > 0)
    {
        update aduList; 
    }
}