trigger trgVendorRequestApprover on jbrvrm__Vendor_Requisition__c (before insert,before update) {
    
/*      
if Approver 2 = null, then Approver2=Approver3
if Approver 1 = null, then Approver1=Approver2
if Approver 3 = null, then Approver3= createdby
*/  for(jbrvrm__Vendor_Requisition__c requisition : Trigger.new) {
        Id approver1 = requisition.jbrvrm__Approver_1__c;
        Id approver2 = requisition.jbrvrm__Approver_2__c;
        Id approver3 = requisition.jbrvrm__Approver_3__c;
        
        if(requisition.jbrvrm__Approver_1__c == NULL) {
            if(requisition.createdbyID != NULL) {
                approver1 = requisition.createdbyID;
            } else {
                approver1 = UserInfo.getUserId();
            }
            
            requisition.jbrvrm__Approver_1__c = approver1 ;
            System.debug('#### approver1 = '+approver1 + ', requisition.jbrvrm__Approver_1__c = '+requisition.jbrvrm__Approver_1__c);
            
        } if(requisition.jbrvrm__Approver_2__c == NULL) {
            approver2 = approver1;
            requisition.jbrvrm__Approver_2__c = approver2 ;
            System.debug('#### approver2 = '+approver2 + ', requisition.jbrvrm__Approver_2__c = '+requisition.jbrvrm__Approver_2__c);
            
        } if(requisition.jbrvrm__Approver_3__c == NULL) {
            approver3 = approver2;
            requisition.jbrvrm__Approver_3__c = approver3 ;
            System.debug('#### approver3 = '+approver3 + ', requisition.jbrvrm__Approver_3__c = '+requisition.jbrvrm__Approver_3__c);
            
        }
    }
}