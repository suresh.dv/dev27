// ==========================================================================
// Check that date of the new status is not older than previous status.
// The name is legacy of old requirement implementation.
trigger CarsealStatus_UpdateDateClosedOnStatusChange on Carseal_Status__c (before insert, 
                                                             before update) {
 // List of Carseal ID
  Set<Id>CarsealIds = new Set<Id>();
  
  // List of date of the new statuses
  Map<Id, Carseal_Status__c>statusChangeDate = new Map<Id, Carseal_Status__c>();
  
  // Get the date and carseal for the new statuses
  for (Carseal_Status__c carseal_status : Trigger.new) {
//KK 12/Mar/2014: add to check that if the date is a future date, it is not allowed.    
    if (carseal_status.Date__c > System.today())
    {
        carseal_status.Date__c.addError('The Date for the Status Update cannot be a future date.');
    }
    else
    {
        CarsealIds.add(carseal_status.Carseal__c);
        statusChangeDate.put(carseal_status.Carseal__c, carseal_status);
        
      // Get the list of last statuses
      List<Carseal__c> prevStatuses = 
        [SELECT Id, Carseal_Status_Lookup__r.Date__c, Carseal_Status_Lookup__r.Status_Lookup__c
                FROM Carseal__c 
                WHERE Id IN : carsealIds];
                      
      // List of status new status
      List<Carseal_Status__c> updatedStatuses = new List<Carseal_Status__c>();
      
      // Throw error if the date is older than previous status
      for (Carseal__c prevStatus : prevStatuses) {
        if (prevStatus.Carseal_Status_Lookup__r.Date__c > 
            statusChangeDate.get(prevStatus.Id).Date__c) {
          statusChangeDate.get(prevStatus.Id).Date__c.addError('An existing Carseal Status update conflicts with this date.  Please confirm the date of your Status change.');   
        }
      }
  
      update updatedStatuses;
        
    }
  }
  
}