/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for EPD_Cylinder_Information__c SObject
Test Class:     EPD_CylinderInformationTriggerTest
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
trigger EPD_CylinderInformationTrigger on EPD_Cylinder_Information__c (before delete) {

    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            new EPD_DeletionRestrictor().restrictDeletion(Trigger.old);
        }
    }

}