trigger AssignPortalUserToPublicGroup on User (after insert, after update) {
	
	if(VendorPortalUtility.portalExecuteTriggerCode == true){
		
		if (trigger.isAfter && trigger.isInsert){
			VendorPortalUtility.AddToGroups(Trigger.New);
		}

		if (trigger.isAfter && trigger.isUpdate){
			VendorPortalUtility.UpdatePortalUserPermissionSets(trigger.newMap, trigger.oldMap);
			//VendorPortalUtility.IsContactVendorPortalUser(trigger.newMap);
		}
	}
}