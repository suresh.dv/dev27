/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for HOG_Vent_Gas_Alert__c
Test class:		HOG_VentGasAlertTriggerTest
History:        jschn 04.19.2018 - Created.
				jschn 2020-01-23 - US 001742 - added before insert (field population logic)
				jschn 2020-01-23 - US 001751 - added after insert (notification logic)
**************************************************************************************************/
trigger HOG_VentGasAlertTrigger on HOG_Vent_Gas_Alert__c (before update, before insert, after insert) {
	if (Trigger.isBefore) {
		if (Trigger.isUpdate) {
			HOG_VentGas_Utilities.ventGasTriggerBeforeUpdate(Trigger.new, Trigger.oldMap);
		}
		//jschn 2020-01-23 - US 001742
		if (Trigger.isInsert) {
			HOG_VentGas_Utilities.ventGasAlertBeforeInsert(Trigger.new);
		}
	}
	//jschn 2020-01-23 - US 001751
	if (Trigger.isAfter) {
		if (Trigger.isInsert) {
			HOG_VentGas_Utilities.ventGasAlertAfterInsert(Trigger.new);
		}
	}
}