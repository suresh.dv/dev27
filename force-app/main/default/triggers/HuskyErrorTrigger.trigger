trigger HuskyErrorTrigger on Husky_Error__e (after insert) {
    List<Error_Log__c> errorLogs = new List<Error_Log__c>();
    
    for(Husky_Error__e errors : Trigger.new) {
        Error_Log__c errorLog = new Error_Log__c();
        errorLog.Apex_Class_Trigger__c = errors.Interface__c;
        errorLog.Log_Date__c = errors.Error_Date__c;
        errorLog.Severity__c = errors.Severity__c;
        errorLog.Description__c = errors.Stack_Trace__c;
        
        errorLogs.add(errorLog);
    }

    if(!errorLogs.isEmpty()) {
        Database.insert(errorLogs);
    }
        
}