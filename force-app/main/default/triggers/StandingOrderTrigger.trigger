trigger StandingOrderTrigger on Standing_Order__c (after update) {
  if (Trigger.isAfter && Trigger.isUpdate) {
    OperationsStandingOrderController standingOrderController = new OperationsStandingOrderController();
    standingOrderController.resetReadAcknowledgement(Trigger.new, Trigger.oldMap);
  }
}