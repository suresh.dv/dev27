trigger ATSFreight_SetCompetitorNetbackFromFreight on ATS_Freight__c (after update, after delete)
{
    
    // Stores all freights that have changed
    Set<Id> freightsAffected = new Set<Id>();
    
    // If this was an update to a Freight record, check if the relevant fields have changed.
    if(Trigger.isUpdate)
    {
        for(Id freightId : Trigger.newMap.keyset())
        {
            ATS_Freight__c newFreight = Trigger.newMap.get(freightId);
            ATS_Freight__c oldFreight = Trigger.oldMap.get(freightId);
            
            if(newFreight.Emulsion_Rate5_Competitor1__c      != oldFreight.Emulsion_Rate5_Competitor1__c      ||
               newFreight.Emulsion_Rate5_Competitor2__c      != oldFreight.Emulsion_Rate5_Competitor2__c      ||
               newFreight.Emulsion_Axle5_Rate_Competitor3__c != oldFreight.Emulsion_Axle5_Rate_Competitor3__c ||
               newFreight.Emulsion_Axle5_Rate_Competitor4__c != oldFreight.Emulsion_Axle5_Rate_Competitor4__c ||
               newFreight.Emulsion_Rate6_Competitor1__c      != oldFreight.Emulsion_Rate6_Competitor1__c      ||
               newFreight.Emulsion_Rate6_Competitor2__c      != oldFreight.Emulsion_Rate6_Competitor2__c      ||
               newFreight.Emulsion_Axle6_Rate_Competitor3__c != oldFreight.Emulsion_Axle6_Rate_Competitor3__c ||
               newFreight.Emulsion_Axle6_Rate_Competitor4__c != oldFreight.Emulsion_Axle6_Rate_Competitor4__c ||
               newFreight.Emulsion_Rate7_Competitor1__c      != oldFreight.Emulsion_Rate7_Competitor1__c      ||
               newFreight.Emulsion_Rate7_Competitor2__c      != oldFreight.Emulsion_Rate7_Competitor2__c      ||
               newFreight.Emulsion_Rate7_Competitor3__c      != oldFreight.Emulsion_Rate7_Competitor3__c      ||
               newFreight.Emulsion_Rate7_Competitor4__c      != oldFreight.Emulsion_Rate7_Competitor4__c      ||
               newFreight.Emulsion_Rate8_Competitor1__c      != oldFreight.Emulsion_Rate8_Competitor1__c      ||
               newFreight.Emulsion_Rate8_Competitor2__c      != oldFreight.Emulsion_Rate8_Competitor2__c      ||
               newFreight.Emulsion_Axle8_Rate_Competitor3__c != oldFreight.Emulsion_Axle8_Rate_Competitor3__c ||
               newFreight.Emulsion_Axle8_Rate_Competitor4__c != oldFreight.Emulsion_Axle8_Rate_Competitor4__c)
            {
                freightsAffected.add(freightId);
            }
        }
    }
    // If this was a delete, no need to check if relevant fields have changed.
    if(Trigger.isDelete)
    {
        freightsAffected = Trigger.oldMap.keySet();
    }
    
    if(freightsAffected.size() == 0) return;
    
    // Get the opp record type we need
    RecordType ashpalt = [SELECT Id
                           FROM RecordType
                           WHERE DeveloperName =: 'ATS_Asphalt_Product_Category'];
    
    // Now get all olis using these freights
    List<OpportunityLineItem> olis = [SELECT Id,
                                             Competitor_1_Refinery_Netback_CAD__c,
                                             Competitor_1_Terminal_Netback_CAD__c,
                                             Competitor_1_Bid_Price__c,
                                             Competitor_1_Terminal_Freight_CAD__c,
                                             Competitor_2_Refinery_Netback_CAD__c,
                                             Competitor_2_Terminal_Netback_CAD__c,
                                             Competitor_2_Bid_Price__c,
                                             Competitor_2_Terminal_Freight_CAD__c,
                                             Competitor_3_Refinery_Netback_CAD__c,
                                             Competitor_3_Terminal_Netback_CAD__c,
                                             Competitor_3_Bid_Price__c,
                                             Competitor_3_Terminal_Freight_CAD__c,
                                             Competitor_4_Refinery_Netback_CAD__c,
                                             Competitor_4_Terminal_Netback_CAD__c,
                                             Competitor_4_Bid_Price__c,
                                             Competitor_4_Terminal_Freight_CAD__c,
                                             Unit__c,
                                             Currency__c,
                                             Exchange_Rate_to_CAD__c,
                                             Additives_CAD__c,
                                             AC_Premium_CAD__c,
                                             Handling_Fees_CAD__c,
                                             
                                             // Get the associated Opportunity
                                             
                                             OpportunityId,
                                             Opportunity.RecordTypeId,
                                             Opportunity.ATS_Freight__c,
                                             
                                             // Get the associated Product2
                                             
                                             Product2Id,
                                             Product2.Density__c,
                                             
                                             // Get the associated Freight
                                             
                                             Opportunity.ATS_Freight__r.Id,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                             Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                             Opportunity.ATS_Freight__r.Supplier_2_Unit__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor4__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor4__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor4__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor3__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor4__c
                                      FROM OpportunityLineItem
                                      WHERE Opportunity.ATS_Freight__r.Id IN: freightsAffected AND
                                            Opportunity.RecordTypeId =: ashpalt.Id];
    
    // These are the olis we'll be updating to the db
    List<OpportunityLineItem> olisToUpdate = new List<OpportunityLineItem>();
    
    for(OpportunityLineItem oli : olis)
    {
        // Build freight object
        
        ATS_Freight__c f = new ATS_Freight__c(Id                           = oli.Opportunity.ATS_Freight__r.Id,
                                              Husky_Supplier_1_Selected__c = oli.Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                              Husky_Supplier_2_Selected__c = oli.Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                              Prices_F_O_B__c              = oli.Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                              Emulsion_Rate8_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                              Emulsion_Rate8_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                              Emulsion_Rate7_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                              Emulsion_Rate7_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                              Emulsion_Rate6_Supplier_1__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                              Emulsion_Rate6_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                              Emulsion_Rate5_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                              Emulsion_Rate5_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                              Supplier_1_Unit__c           = oli.Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                              Supplier_2_Unit__c           = oli.Opportunity.ATS_Freight__r.Supplier_2_Unit__c,
                                              Emulsion_Rate5_Competitor1__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor1__c,
                                              Emulsion_Rate5_Competitor2__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Competitor2__c,
                                              Emulsion_Axle5_Rate_Competitor3__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor3__c,
                                              Emulsion_Axle5_Rate_Competitor4__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle5_Rate_Competitor4__c,
                                              Emulsion_Rate6_Competitor1__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor1__c,
                                              Emulsion_Rate6_Competitor2__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Competitor2__c,
                                              Emulsion_Axle6_Rate_Competitor3__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor3__c,
                                              Emulsion_Axle6_Rate_Competitor4__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle6_Rate_Competitor4__c,
                                              Emulsion_Rate7_Competitor1__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor1__c,
                                              Emulsion_Rate7_Competitor2__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor2__c,
                                              Emulsion_Rate7_Competitor3__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor3__c,
                                              Emulsion_Rate7_Competitor4__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Competitor4__c,
                                              Emulsion_Rate8_Competitor1__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor1__c,
                                              Emulsion_Rate8_Competitor2__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Competitor2__c,
                                              Emulsion_Axle8_Rate_Competitor3__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor3__c,
                                              Emulsion_Axle8_Rate_Competitor4__c = oli.Opportunity.ATS_Freight__r.Emulsion_Axle8_Rate_Competitor4__c);
        
        // Build Product2 object
        
        Product2 p = new Product2(Id         = oli.Product2Id,
                                  Density__c = oli.Product2.Density__c);
        
        // Calculate axle prices
        
        Decimal competitor1RefineryNetback = calculateNetback.calculateCompetitorRefineryNetback(1, oli, f);
        Decimal competitor2RefineryNetback = calculateNetback.calculateCompetitorRefineryNetback(2, oli, f);
        Decimal competitor3RefineryNetback = calculateNetback.calculateCompetitorRefineryNetback(3, oli, f);
        Decimal competitor4RefineryNetback = calculateNetback.calculateCompetitorRefineryNetback(4, oli, f);
        
        Decimal competitor1TerminalNetback = calculateNetback.calculateCompetitorTerminalNetback(1, oli, f);
        Decimal competitor2TerminalNetback = calculateNetback.calculateCompetitorTerminalNetback(2, oli, f);
        Decimal competitor3TerminalNetback = calculateNetback.calculateCompetitorTerminalNetback(3, oli, f);
        Decimal competitor4TerminalNetback = calculateNetback.calculateCompetitorTerminalNetback(4, oli, f);
        
        OpportunityLineItem oliToCommit = new OpportunityLineItem(Id                                    = oli.Id,
                                                                  Competitor_1_Refinery_Netback_CAD__c = null,
                                                                  Competitor_2_Refinery_Netback_CAD__c = null,
                                                                  Competitor_3_Refinery_Netback_CAD__c = null,
                                                                  Competitor_4_Refinery_Netback_CAD__c = null,
                                                                  Competitor_1_Terminal_Netback_CAD__c = null,
                                                                  Competitor_2_Terminal_Netback_CAD__c = null,
                                                                  Competitor_3_Terminal_Netback_CAD__c = null,
                                                                  Competitor_4_Terminal_Netback_CAD__c = null);
        
        if(competitor1RefineryNetback != null)
        {
            oliToCommit.Competitor_1_Refinery_Netback_CAD__c = competitor1RefineryNetback;
            oliToCommit.Competitor_1_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor2RefineryNetback != null)
        {
            oliToCommit.Competitor_2_Refinery_Netback_CAD__c = competitor2RefineryNetback;
            oliToCommit.Competitor_2_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor3RefineryNetback != null)
        {
            oliToCommit.Competitor_3_Refinery_Netback_CAD__c = competitor3RefineryNetback;
            oliToCommit.Competitor_3_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor4RefineryNetback != null)
        {
            oliToCommit.Competitor_4_Refinery_Netback_CAD__c = competitor4RefineryNetback;
            oliToCommit.Competitor_4_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        
        if(competitor1TerminalNetback != null)
        {
            oliToCommit.Competitor_1_Terminal_Netback_CAD__c = competitor1TerminalNetback;
            oliToCommit.Competitor_1_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor2TerminalNetback != null)
        {
            oliToCommit.Competitor_2_Terminal_Netback_CAD__c = competitor2TerminalNetback;
            oliToCommit.Competitor_2_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor3TerminalNetback != null)
        {
            oliToCommit.Competitor_3_Terminal_Netback_CAD__c = competitor3TerminalNetback;
            oliToCommit.Competitor_3_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(competitor4TerminalNetback != null)
        {
            oliToCommit.Competitor_4_Terminal_Netback_CAD__c = competitor4TerminalNetback;
            oliToCommit.Competitor_4_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        }
        
        olisToUpdate.add(oliToCommit);
    }
    
    update olisToUpdate;
}