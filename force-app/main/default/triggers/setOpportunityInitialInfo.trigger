trigger setOpportunityInitialInfo on Opportunity (before insert) {  
    
    Id oppRecordTypeId =
        Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();  
    for (Opportunity tender : trigger.new){   
        
         if(tender.RecordTypeId == oppRecordTypeId)    
        { 
            //First condition
            if(tender.cpm_Price_Valid_To__c == null ){
                //set the "Price Valid To" field                       
                Date currentDate = date.today();
                integer december = 12;
                integer lastDay = 31;              		
                tender.cpm_Price_Valid_To__c = date.newInstance(currentDate.year(), december, lastDay);
            }
            
            if (tender.cpm_Acceptance_Deadline__c !=null) { 
                //Second condition
                if(tender.cpm_Acceptance_Deadline__c == null  ){
                    //add 2 weeks to the acceptance deadline from the bid due date
                    DateTime newAcceptanceDate = tender.cpm_Bid_Due_Date_Time__c.addDays(14);
                    tender.cpm_Acceptance_Deadline__c = date.newInstance(newAcceptanceDate.year(), newAcceptanceDate.month(), newAcceptanceDate.day());
                }
                //Third condition
                if(tender.cpm_Price_Valid_From__c == null){
                    //Price Valid From date should match Bid Due Date
                    tender.cpm_Price_Valid_From__c = date.newinstance(tender.cpm_Bid_Due_Date_Time__c.year(), tender.cpm_Bid_Due_Date_Time__c.month(), tender.cpm_Bid_Due_Date_Time__c.day());   
                } 
            }
            
        }
        
        
        
    }
    
 }