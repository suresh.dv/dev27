trigger Insert_Product_Schedule on Opportunity (before insert,before update) 
{
    List<cpm_Product_Scheduling__c> customobjectList =new List<cpm_Product_Scheduling__c>();
    List<Opportunitylineitem> opplinitemlist =new List<Opportunitylineitem>();
    List<Opportunity> Opportunitylist =new List<Opportunity>();

    Id oppRecordTypeId =
        Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();  
    for(Opportunity opp :trigger.new)
    {
        if(opp.RecordTypeId == oppRecordTypeId)    
        { 
            if(opp.Stagename=='Closed Won')
            {      
                Opplinitemlist   = [select Id,PricebookEntry.Product2.Name,PricebookEntry.Product2.id from Opportunitylineitem where Opportunityid =: opp.id];
                Opportunitylist  = [Select Id from Opportunity where Id =: opp.id];
        
                for(Opportunitylineitem Opplist :opplinitemlist ){
                    list<cpm_Product_Scheduling__c> existingids=[select id from cpm_Product_Scheduling__c where cpm_Opportunity_Line_Item_Id__c=:Opplist.Id];
                    if(existingids.size()>0){}
                    else
                    {
                        cpm_Product_Scheduling__c schedulingPro = new cpm_Product_Scheduling__c();
                        schedulingPro.cpm_Opportunity__c = Opportunitylist[0].Id;
                        CustomobjectList.add(schedulingPro);
                        schedulingPro.cpm_Opportunity_Line_Item_Id__c = Opplist.Id; // Custom field on custom object to hold line item id
                    }       
                }
                insert CustomobjectList;
            }  
        }      
    }      
}