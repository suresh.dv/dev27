trigger PT_ValidateSiteId on Service__c (before insert, before update) {
    Set <Service__c> serviceList = new Set<Service__c>([Select id,name,site_id__c from service__c]);
    Map <String,Service__c> mServiceSiteId = new Map<String,Service__c>();
    
    for (Service__c service : serviceList)
    {
        mServiceSiteId.put(service.site_id__c,service);
    }
    
    if(Trigger.isUpdate == False)
    {
        for(Service__c newService : Trigger.new)
        {
            if(mServiceSiteId.containsKey(newService.site_id__c) && String.isempty(newService.site_id__c) != true && newService.site_id__c != null)
            {
                newService.addError('An existing Service contains the same site id. Please, assign a new site id to save this record.');
            }
        }        
    }
    else
    {
        for(Service__c Service : Trigger.new)
        {
            if(Service.Site_Id__c != (Trigger.oldMap.get(Service.Id)).site_id__c)
            {
                if(mServiceSiteId.containsKey(Service.site_id__c) && String.isempty(Service.site_id__c) != true && Service.site_id__c != null)
                {
                    Service.addError('An existing Service contains the same site id. Please, assign a new site id to save this record.');
                }
            }
        }         
    }

    
}