trigger SPCC_VendorAccountAssignmentTrigger on SPCC_Vendor_Account_Assignment__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

    if(SPCC_Utilities.executeTriggerCode) {
        if (Trigger.isBefore) {

            if (SPCC_Utilities.checkForDuplicate == true){
            
                if(Trigger.isInsert) {
                    SPCC_Utilities.TriggerBeforeInsertVendorAccountAssignment(Trigger.new);
                }
            }    
        
        } else if (Trigger.isAfter) {

            if(Trigger.isInsert) {
                //SPCC_Utilities.TriggerAfterInsertVendorAccountAssignment(Trigger.new);
            }
            
            else if(Trigger.isDelete){
                SPCC_Utilities.TriggerAfterDeleteVendorAccountAssignment(Trigger.old);
            }
        }
    }
}