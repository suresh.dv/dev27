trigger PT_CreatePCSOSnapshot on PCSO__c (before update) {
    List<PCSO_Snapshot__c> pcsoSnapshotList = new List<PCSO_Snapshot__c>();
    for(PCSO__c pcsoNew: Trigger.new){
        PCSO__c pcsoBeforeChange = trigger.oldMap.get(pcsoNew.id);
        if(pcsoBeforeChange.PCSO_Status__c != pcsoNew.PCSO_Status__c || pcsoBeforeChange.Cost_Saving__c != pcsoNew.Cost_Saving__c || pcsoBeforeChange.Buydown_Cost__c != pcsoNew.Buydown_Cost__c || pcsoBeforeChange.Audited_Cost_Saving__c != pcsoNew.Audited_Cost_Saving__c || pcsoBeforeChange.Audited_Buydown_cost__c != pcsoNew.Audited_Buydown_cost__c){
            String transactionID = system.now().format('Y') + system.now().format('M') + system.now().format('d') + system.now().format('k') +system.now().format('m') +system.now().format('s');
            PCSO_Snapshot__c pcsoSnapshotBefore = new PCSO_Snapshot__c();
            PCSO_Snapshot__c pcsoSnapshotAfter = new PCSO_Snapshot__c();
            
            pcsoSnapshotBefore.PCSO__c = pcsoBeforeChange.id;       
            pcsoSnapshotBefore.Snapshot_year__c = system.now().format('Y');
            pcsoSnapshotBefore.Snapshot_Month__c = system.now().format('M');        
            if(PT_PCSO_Snapshot_Settings__c.getValues(pcsoBeforeChange.PCSO_Status__c) != null){
                pcsoSnapshotBefore.PCSO_Status__c = PT_PCSO_Snapshot_Settings__c.getValues(pcsoBeforeChange.PCSO_Status__c).Status_Translation__c;
                pcsoSnapshotBefore.PCSO_Grouping_Status__c = PT_PCSO_Snapshot_Settings__c.getValues(pcsoBeforeChange.PCSO_Status__c).Status_Grouping__c;
            }
            else{
                pcsoSnapshotBefore.PCSO_Status__c = pcsoBeforeChange.PCSO_Status__c;    
            }        
            pcsoSnapshotBefore.Transaction_type__c = 'Before';
            pcsoSnapshotBefore.Transaction_id__c = transactionID;
            
            pcsoSnapshotAfter.PCSO__c = pcsoNew.id;
            pcsoSnapshotAfter.Snapshot_year__c = system.now().format('Y');
            pcsoSnapshotAfter.Snapshot_Month__c = system.now().format('M');
            if(PT_PCSO_Snapshot_Settings__c.getValues(pcsoNew.PCSO_Status__c) != null){
                pcsoSnapshotAfter.PCSO_Status__c = PT_PCSO_Snapshot_Settings__c.getValues(pcsoNew.PCSO_Status__c).Status_Translation__c;
                pcsoSnapshotAfter.PCSO_Grouping_Status__c = PT_PCSO_Snapshot_Settings__c.getValues(pcsoNew.PCSO_Status__c).Status_Grouping__c;
            }
            else{
                pcsoSnapshotAfter.PCSO_Status__c = pcsoNew.PCSO_Status__c;    
            }                
            pcsoSnapshotAfter.Transaction_type__c = 'After';
            pcsoSnapshotAfter.Transaction_id__c = transactionID;
            
            if(pcsoBeforeChange.PCSO_Status__c == pcsoNew.PCSO_Status__c){
                if(pcsoBeforeChange.Cost_Saving__c != pcsoNew.Cost_Saving__c){
                    if(pcsoBeforeChange.Cost_Saving__c != null){
                        pcsoSnapshotBefore.Cost_Saving__c = -pcsoBeforeChange.Cost_Saving__c;
                        pcsoSnapshotAfter.Cost_Saving__c = pcsoNew.Cost_Saving__c;            
                    }
                    else{
                        pcsoSnapshotBefore.Cost_Saving__c = 0;
                        pcsoSnapshotAfter.Cost_Saving__c = pcsoNew.Cost_Saving__c;
                    }
                }
            }
            else{
                if(pcsoBeforeChange.Cost_Saving__c != null){
                    pcsoSnapshotBefore.Cost_Saving__c = -pcsoBeforeChange.Cost_Saving__c;
                    pcsoSnapshotAfter.Cost_Saving__c = pcsoNew.Cost_Saving__c;            
                }
                else{
                    pcsoSnapshotBefore.Cost_Saving__c = 0;
                    pcsoSnapshotAfter.Cost_Saving__c = pcsoNew.Cost_Saving__c;
                }
            }          
            
            
            if(pcsoBeforeChange.PCSO_Status__c == pcsoNew.PCSO_Status__c){
                if(pcsoBeforeChange.Buydown_Cost__c != pcsoNew.Buydown_Cost__c){                
                    if(pcsoBeforeChange.Buydown_Cost__c != null){
                        pcsoSnapshotBefore.Buydown_Cost__c = -pcsoBeforeChange.Buydown_Cost__c;
                        pcsoSnapshotAfter.Buydown_Cost__c = pcsoNew.Buydown_Cost__c;            
                    }
                    else{
                        pcsoSnapshotBefore.Buydown_Cost__c = 0;
                        pcsoSnapshotAfter.Buydown_Cost__c = pcsoNew.Buydown_Cost__c;
                    }
                }
            }        
            else{
                if(pcsoBeforeChange.Buydown_Cost__c != null){
                    pcsoSnapshotBefore.Buydown_Cost__c = -pcsoBeforeChange.Buydown_Cost__c;
                    pcsoSnapshotAfter.Buydown_Cost__c = pcsoNew.Buydown_Cost__c;            
                }
                else{
                    pcsoSnapshotBefore.Buydown_Cost__c = 0;
                    pcsoSnapshotAfter.Buydown_Cost__c = pcsoNew.Buydown_Cost__c;
                }            
            }
            
            if(pcsoBeforeChange.PCSO_Status__c == pcsoNew.PCSO_Status__c){
                if(pcsoBeforeChange.Audited_Cost_Saving__c != pcsoNew.Audited_Cost_Saving__c){
                    if(pcsoBeforeChange.Audited_Cost_Saving__c != null){
                        pcsoSnapshotBefore.Audited_Cost_Saving__c = -pcsoBeforeChange.Audited_Cost_Saving__c;
                        pcsoSnapshotAfter.Audited_Cost_Saving__c = pcsoNew.Audited_Cost_Saving__c;            
                    }
                    else{
                        pcsoSnapshotBefore.Audited_Cost_Saving__c = 0;
                        pcsoSnapshotAfter.Audited_Cost_Saving__c = pcsoNew.Audited_Cost_Saving__c;
                    }
                }
            }
            else{
                if(pcsoBeforeChange.Audited_Cost_Saving__c != null){
                    pcsoSnapshotBefore.Audited_Cost_Saving__c = -pcsoBeforeChange.Audited_Cost_Saving__c;
                    pcsoSnapshotAfter.Audited_Cost_Saving__c = pcsoNew.Audited_Cost_Saving__c;            
                }
                else{
                    pcsoSnapshotBefore.Audited_Cost_Saving__c = 0;
                    pcsoSnapshotAfter.Audited_Cost_Saving__c = pcsoNew.Audited_Cost_Saving__c;
                }
            }          
            
            if(pcsoBeforeChange.PCSO_Status__c == pcsoNew.PCSO_Status__c){
                if(pcsoBeforeChange.Audited_Buydown_Cost__c != pcsoNew.Audited_Buydown_Cost__c){                
                    if(pcsoBeforeChange.Audited_Buydown_Cost__c != null){
                        pcsoSnapshotBefore.Audited_Buydown_Cost__c = -pcsoBeforeChange.Audited_Buydown_Cost__c;
                        pcsoSnapshotAfter.Audited_Buydown_Cost__c = pcsoNew.Audited_Buydown_Cost__c;            
                    }
                    else{
                        pcsoSnapshotBefore.Audited_Buydown_Cost__c = 0;
                        pcsoSnapshotAfter.Audited_Buydown_Cost__c = pcsoNew.Audited_Buydown_Cost__c;
                    }
                }
            }        
            else{
                if(pcsoBeforeChange.Audited_Buydown_Cost__c != null){
                    pcsoSnapshotBefore.Audited_Buydown_Cost__c = -pcsoBeforeChange.Audited_Buydown_Cost__c;
                    pcsoSnapshotAfter.Audited_Buydown_Cost__c = pcsoNew.Audited_Buydown_Cost__c;            
                }
                else{
                    pcsoSnapshotBefore.Audited_Buydown_Cost__c = 0;
                    pcsoSnapshotAfter.Audited_Buydown_Cost__c = pcsoNew.Audited_Buydown_Cost__c;
                }            
            }        
            
            pcsoSnapshotList.add(pcsoSnapshotBefore);
            pcsoSnapshotList.add(pcsoSnapshotAfter);
        }
    }
    if(pcsoSnapshotList.size() > 0 ){
        insert pcsoSnapshotList;            
    }
}