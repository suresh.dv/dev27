trigger PREP_CopySumofBVSS on PREP_Business_Value_Submission__c (after update) {
    Set<Id> changedRecords = new Set<Id>();
    system.debug('PREP_CopySumofBVSS');
    for(PREP_Business_Value_Submission__c item : Trigger.New)
    {
        System.debug('edit PREP_Business_Value_Submission__c = ' + item.Name);
        PREP_Business_Value_Submission__c beforeUpdateObj = trigger.oldMap.get(item.Id);
        if (item.Total_Value_Scheduled__c != beforeUpdateObj.Total_Value_Scheduled__c ||
            item.Total_Spend_Scheduled__c != beforeUpdateObj.Total_Spend_Scheduled__c ||
            item.Total_Avoidance_Scheduled__c != beforeUpdateObj.Total_Avoidance_Scheduled__c)
            changedRecords.add(item.Id);
    }

    List<PREP_Business_Value_Spend_Schedule__c> updateRecords = new List<PREP_Business_Value_Spend_Schedule__c>();

    for(PREP_Business_Value_Submission__c bObj : [SELECT Id, Total_Value_Scheduled__c, Total_Spend_Scheduled__c, Remaining_Spend_To_Schedule__c,
                                                    Remaining_Value_To_Schedule__c, Total_Avoidance_Scheduled__c,
                                                    (SELECT BVS_Total_Savings_Scheduled__c, BVS_Total_Spend_Scheduled__c, BVS_Total_Avoidance_Scheduled__c,
                                                    BVS_Remaining_Spend_To_Schedule__c, BVS_Remaining_Value_To_Schedule__c 
                                                    FROM Business_Value_Savings_Schedules__r)
                                                FROM PREP_Business_Value_Submission__c WHERE Id in :changedRecords])
    {
        
            for(PREP_Business_Value_Spend_Schedule__c BVS : bObj.Business_Value_Savings_Schedules__r)
            {
                
                BVS.BVS_Total_Savings_Scheduled__c = bObj.Total_Value_Scheduled__c;
                BVS.BVS_Total_Spend_Scheduled__c = bObj.Total_Spend_Scheduled__c;
                BVS.BVS_Total_Avoidance_Scheduled__c = bObj.Total_Avoidance_Scheduled__c;
                BVS.BVS_Remaining_Spend_To_Schedule__c = bObj.Remaining_Spend_To_Schedule__c;
                BVS.BVS_Remaining_Value_To_Schedule__c = bObj.Remaining_Value_To_Schedule__c;
                updateRecords.add(BVS);
            }
        
        
    }
    update updateRecords; 
}