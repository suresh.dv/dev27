//this trigger is used to make sure there is only one branch for each Manifold
trigger Glycol_ValidateUniqueManifoldBranch on Glycol_Manifold_Branch__c (before insert, before update) {
    Set<Id> manifoldIds = new Set<Id>();
    Set<Id> branchIds = new Set<Id>();
    for(Glycol_Manifold_Branch__c branch : trigger.new)
    {
    	manifoldIds.add(branch.Supply_Manifold__c);
    	manifoldIds.add(branch.Return_Manifold__c);
    	if (trigger.isUpdate)
    	   branchIds.add(branch.Id);
    }
    List<Glycol_Manifold_Branch__c> relatedBranches = [select Id, Supply_Manifold__c, Supply_Manifold__r.Name, Supply_Branch_Num__c,
                Return_Manifold__c, Return_Manifold__r.Name, Return_Branch_Num__c from Glycol_Manifold_Branch__c 
                where Id not in: branchIds and (Supply_Manifold__c in: manifoldIds or Return_Manifold__c in: manifoldIds)];
    for(Glycol_Manifold_Branch__c branch : trigger.new)
    {
    	for(Glycol_Manifold_Branch__c otherBranch : relatedBranches)
    	{
    		if (branch.Supply_Manifold__c == otherBranch.Supply_Manifold__c 
    		      && branch.Supply_Branch_Num__c == otherBranch.Supply_Branch_Num__c)
    		  branch.addError('Supply Manifold ' + otherBranch.Supply_Manifold__r.Name + 
    		          ' and Supply Branch Number ' + branch.Supply_Branch_Num__c + ' already existed');
    		
    		if (branch.Return_Manifold__c == otherBranch.Return_Manifold__c 
                  && branch.Return_Branch_Num__c == otherBranch.Return_Branch_Num__c)
              branch.addError('Return Manifold ' + otherBranch.Return_Manifold__r.Name + 
                      ' and Return Branch Number ' + branch.Return_Branch_Num__c + ' already existed');        
    	}
    }
}