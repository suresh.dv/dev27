//this trigger don't allow user to save the same Competitor twice and will display an error message
//the only exception is if the Competitor is "Unknow"
trigger OpportunityProduct_CheckDuplicateCompetitor on OpportunityLineItem (before insert, before update) 
{
	
    Set<Id> oppIds = new Set<Id>();
    for(OpportunityLineItem item : trigger.new)
    {
    	System.debug('opp id =' + item.OpportunityId + ' item id =' + item.Id);
    	oppIds.add(item.OpportunityId);
    }
    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>(
                        [select Id, RecordType.Name from Opportunity 
                        where Id in: oppIds and
                        RecordType.DeveloperName in ('ATS_Asphalt_Product_Category', 'ATS_Emulsion_Product_Category', 'ATS_Residual_Product_Category')]);
    System.debug(oppMap);
    for(OpportunityLineItem item : trigger.new)
    {
    	if (oppMap.containsKey(item.OpportunityId))
    	{
	    	if (item.Competitor_1__c != null && item.Competitor_1__c != 'Unknown')
	    	{
	    		List<String> duplicateIndex = new List<String>();
	    		duplicateIndex.add('1');
	    		if (item.Competitor_1__c == item.Competitor_2__c)	
	    	       duplicateIndex.add('2');
	    	    if (item.Competitor_1__c == item.Competitor_3__c)  
	               duplicateIndex.add('3');
	            if (item.Competitor_1__c == item.Competitor_4__c)   
	               duplicateIndex.add('4');
	            if (item.Competitor_1__c == item.Competitor_5__c)   
	               duplicateIndex.add('5');
	            
	            if (duplicateIndex.size() > 1)
		        {
		            String errorMsg = 'You cannot choose the same Competitor more than once (Competitors ';
		            String tmp = '';
		            for(String index : duplicateIndex)
		            {
		                tmp += (tmp == '' ? '' : ',') + index;
		            }
		            item.addError(errorMsg + tmp + ')');    
		            break;  
		        }   
	        }
	    	if (item.Competitor_2__c != null && item.Competitor_2__c != 'Unknown')
	    	{
	    		List<String> duplicateIndex = new List<String>();
	    		duplicateIndex.add('2');
	            if (item.Competitor_2__c == item.Competitor_3__c)  
	               duplicateIndex.add('3');
	            if (item.Competitor_2__c == item.Competitor_4__c)   
	               duplicateIndex.add('4');
	            if (item.Competitor_2__c == item.Competitor_5__c)   
	               duplicateIndex.add('5');
	               
	            if (duplicateIndex.size() > 1)
	            {
	                String errorMsg = 'You cannot choose the same Competitor more than once (Competitors ';
	                String tmp = '';
	                for(String index : duplicateIndex)
	                {
	                    tmp += (tmp == '' ? '' : ',') + index;
	                }
	                item.addError(errorMsg + tmp + ')');    
	                break;  
	            }    
	    	}
	    	if (item.Competitor_3__c != null && item.Competitor_3__c != 'Unknown')
	        {
	            List<String> duplicateIndex = new List<String>();
	            duplicateIndex.add('3');
	            
	            if (item.Competitor_3__c == item.Competitor_4__c)   
	               duplicateIndex.add('4');
	            if (item.Competitor_3__c == item.Competitor_5__c)   
	               duplicateIndex.add('5');
	            
	            if (duplicateIndex.size() > 1)
	            {
	                String errorMsg = 'You cannot choose the same Competitor more than once (Competitors ';
	                String tmp = '';
	                for(String index : duplicateIndex)
	                {
	                    tmp += (tmp == '' ? '' : ',') + index;
	                }
	                item.addError(errorMsg + tmp + ').');    
	                break;  
	            }   
	        }
	        if (item.Competitor_4__c != null && item.Competitor_4__c != 'Unknown')
	        {
	            if (item.Competitor_4__c == item.Competitor_5__c)   
	               item.addError('You cannot choose the same Competitor more than once (Competitors 4, 5).');
	        }
    	}
    }
}