trigger VRM_trgServicesOrderApprover on jbrvrm__Professional_Services_Order__c (before insert,before update) {
    for(jbrvrm__Professional_Services_Order__c servicesOrder: Trigger.new) {
        Id approver1 = servicesOrder.jbrvrm__Approver_1__c;
        Id approver2 = servicesOrder.jbrvrm__Approver_2__c;
        Id approver3 = servicesOrder.jbrvrm__Approver_3__c;
        
        if(servicesOrder.jbrvrm__Approver_1__c == NULL) {
            if(servicesOrder.createdbyID != NULL) {
                approver1 = servicesOrder.createdbyID;
            } else {
                approver1 = UserInfo.getUserId();
            }
            
            servicesOrder.jbrvrm__Approver_1__c = approver1 ;
            System.debug('#### approver1 = '+approver1 + ', servicesOrder.jbrvrm__Approver_1__c = '+servicesOrder.jbrvrm__Approver_1__c);
            
        } if(servicesOrder.jbrvrm__Approver_2__c == NULL) {
            approver2 = approver1;
            servicesOrder.jbrvrm__Approver_2__c = approver2 ;
            System.debug('#### approver2 = '+approver2 + ', servicesOrder.jbrvrm__Approver_2__c = '+servicesOrder.jbrvrm__Approver_2__c);
            
        } if(servicesOrder.jbrvrm__Approver_3__c == NULL) {
            approver3 = approver2;
            servicesOrder.jbrvrm__Approver_3__c = approver3 ;
            System.debug('#### approver3 = '+approver3 + ', servicesOrder.jbrvrm__Approver_3__c = '+servicesOrder.jbrvrm__Approver_3__c);
            
        }
    }
}