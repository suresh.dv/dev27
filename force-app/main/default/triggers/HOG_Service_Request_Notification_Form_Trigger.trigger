trigger HOG_Service_Request_Notification_Form_Trigger on HOG_Service_Request_Notification_Form__c (before update, before insert, before delete) 
{
    If (ServiceRequestNotificationUtilities.executeTriggerCode)
    {
        System.debug('\n*********************************************\n'
            + 'METHOD: HOG_Service_Request_Notification_Form_Trigger'
            + '\nTrigger: executed'
            + '\n****************************************************\n');                  

        if (Trigger.isDelete)
            ServiceRequestNotificationUtilities.TriggerBeforeDeleteServiceRequestNotificationForm(Trigger.old);
                        
        if (Trigger.isUpdate)
        {
            ServiceRequestNotificationUtilities.TriggerBeforeUpdateServiceRequestNotificationForm(Trigger.new, Trigger.oldMap);                
        }
                
        if (Trigger.isInsert)
        {
            ServiceRequestNotificationUtilities.TriggerBeforeInsertServiceRequestNotificationForm(Trigger.new);
        }        
    }

}