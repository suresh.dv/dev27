trigger FM_Run_Sheet_Trigger on FM_Run_Sheet__c (before update, before insert, before delete, after insert, after update) {
    If (FM_Utilities.executeTriggerCode)
    {
        System.debug('\n*****************************************\n'
            + 'METHOD: FM_Run_Sheet_Trigger()'
            + '\nTrigger: executed'
            + '\n************************************************\n');

        if (Trigger.isUpdate && Trigger.isBefore){
            FM_Utilities.TriggerBeforeUpdateRunSheet(Trigger.new, Trigger.oldMap);
        }

        if (Trigger.isInsert)
        {
            if (Trigger.isBefore)
            {
                // This one does not need to be re-written as the action is only dealing with run sheets itself
                FM_Utilities.TriggerBeforeInsertRunSheet(Trigger.new);
            }
        }
    }
}