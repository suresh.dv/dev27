trigger SPCC_LineItemTrigger on SPCC_Line_Item__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

        if (Trigger.isBefore) {
            if(Trigger.isInsert) {
                SPCC_Utilities.TriggerBeforeInsertLineItem(Trigger.new);
            } else if (Trigger.isUpdate) {
                SPCC_Utilities.TriggerBeforeUpdateLineItem(Trigger.new);
            }
        } else if (Trigger.isAfter) {
            //call handler.after method
        
        }
}