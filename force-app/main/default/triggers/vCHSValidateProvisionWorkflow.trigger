trigger vCHSValidateProvisionWorkflow on Request__c (after update) {
    if(trigger.isUpdate)
    {
        Id currentUserId = UserInfo.getUserId();
        Set<Id> approverIds = new Set<Id>();
                
    	//Retrieve the users     
    	Map<Id, Set<Id>> mapAcceptableApprovers = new Map<Id, Set<Id>>();
        
        for(Request__c request : Trigger.new) {
            //add the approver ids to a set            
            approverIds.add(request.EA__c);
            approverIds.add(request.Program_Manager__c); 
            approverIds.add(request.OwnerId);          
            
            //Store all 3 approvers in a Set, and store into the Map
            mapAcceptableApprovers.put(request.Id, approverIds);                      
                 
            Request__c oldRequest = trigger.oldmap.get(request.Id);
                        
            if((oldRequest.Provisioning_Stage__c == 'Submit for Approval') && (request.Provisioning_Stage__c == 'Provisioning:  In Progress')){                                
                //Retrieve the IDs in the Set and compare to current user
                if(!approverIds.contains(currentUserId)){                                                               
                    request.adderror(' Current User does NOT have approval rights to trigger Provisioning Stage. Only PgM, EA or Owner may change Provisioning Status');                                                                    
                }     
        	}
        }
                
    }

}