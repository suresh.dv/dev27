/**
 * Created by MarcelBrimus on 20/02/2019.
 */
trigger HOG_ContentDocumentLinkTrigger on ContentDocumentLink (before insert) {
    if(Trigger.isBefore) {
        if (Trigger.isInsert) {
            HOG_ContentDocumentLinkTriggerHandler.handleBeforeInsert(Trigger.new);
        }
    }
}