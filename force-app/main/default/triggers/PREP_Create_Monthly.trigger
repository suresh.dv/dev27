trigger PREP_Create_Monthly on PREP_Business_Value_Spend_Schedule__c (after insert) 
{
    List<PREP_Business_Value_Spend_Schedule__c> msgRecord = new List<PREP_Business_Value_Spend_Schedule__c>();

    Id scheRecordTypeId = Schema.SObjectType.PREP_Business_Value_Spend_Schedule__c.getRecordTypeInfosByName().get('Monthly Business Value Savings Schedule').getRecordTypeId();
    
    String[] months = new String[]{'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'};

    
    for(PREP_Business_Value_Spend_Schedule__c BVS : Trigger.New)
    {  
        
        if(BVS.RecordTypeId == scheRecordTypeId)
        {
            Date startDate = BVS.Start_Month__c;
            Date endDate = BVS.End_Month__c;
            
            Integer startMonth = BVS.Start_Month__c.month();
            Integer startYear = BVS.Start_Month__c.year();
            
            
            Integer monthDiff = startDate.monthsBetween(endDate) + 1;
            
            Double totalSpend = BVS.Spend_Allocated__c;
            Double totalSav = BVS.Savings_To_Schedule__c;
            
            Decimal spendAllocated = BVS.Spend_Allocated__c / monthDiff;
            Decimal savingsAllocated = BVS.Savings_To_Schedule__c / monthDiff;
            
            spendAllocated = spendAllocated.setScale(0, RoundingMode.DOWN);
            savingsAllocated = savingsAllocated.setScale(0, RoundingMode.DOWN);
            
            Double remainingSpend = (totalSpend - (spendAllocated*monthDiff));
            Double remainingSav = (totalSav - (savingsAllocated*monthDiff));

                        
          for(Integer i = 0; i < monthDiff; i++)
          {
                
                if(startMonth == 13)
                {
                
                    startMonth = 1;
                    startYear++;
                }
                
                PREP_Business_Value_Spend_Schedule_Month__c scheMonth = new PREP_Business_Value_Spend_Schedule_Month__c();
                
                scheMonth.Month__c = months[startMonth-1];
                scheMonth.Year__c = string.valueof(startYear);
                scheMonth.Business_Value_Spend_Schedule_Id__c = BVS.id;
                
                if((i+1) == monthDiff)
                {
                    
                   scheMonth.Scheduled_Spend__c = spendAllocated + remainingSpend;

                }
                
                else
                {
                   scheMonth.Scheduled_Spend__c = spendAllocated;
                }
                
                insert scheMonth;
                  
                PREP_Business_Value_Sav_Schedule_Month__c savScheMonth = new PREP_Business_Value_Sav_Schedule_Month__c();
                
                savScheMonth.Business_Value_Spend_Schedule_Month_Id__c = scheMonth.Id;
                savScheMonth.Current_Value_Classification__c = BVS.Current_Value_Classification__c;
                savScheMonth.Value_Source__c = BVS.Value_Source__c;
                
                if((i+1) == monthDiff)
                {
                    savScheMonth.Scheduled_Value_Realization__c = savingsAllocated + remainingSav;
                }
                
                else
                {
                    savScheMonth.Scheduled_Value_Realization__c = savingsAllocated;
                }
                
                insert savScheMonth;
                
                startMonth++;  
          }
        } 
    }
}