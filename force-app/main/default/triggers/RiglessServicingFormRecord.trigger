trigger RiglessServicingFormRecord on Rigless_Servicing_Form__c (before insert, before update) 
{
    List<Rigless_Servicing_Form__c> riglessServicingForm = Trigger.new;

    if (riglessServicingForm[0].Service_Status__c == 'Complete')
    {
        // Get the lowest recorded value of Service Time Rig Off Location
        List<Service_Time__c> serviceTime =
              [Select Rig_Off_Location__c From Service_Time__c Where Rigless_Servicing_Form__c =: riglessServicingForm[0].Id Order By Rig_Off_Location__c Asc Limit 1];

        // Get the recorded values of Vendor On Location and Off Location
        Integer vendorEmptyTimeCount =
              [Select Count() From Vendor_Personnel__c Where Rigless_Servicing_Form__c =: riglessServicingForm[0].Id And (Off_Location_Time__c = Null Or On_Location_Time__c = Null)];

        if (serviceTime.size() > 0)
        {
            if (serviceTime[0].Rig_Off_Location__c == Null)
                riglessServicingForm[0].addError('Service Time Log Rig Off Location must not be empty in order to set the Service Status to "Complete".');
        }
        else
            riglessServicingForm[0].addError('Service Time Log must have at least one record in order to set the Service Status to "Complete".');
            
        if (vendorEmptyTimeCount > 0)
            riglessServicingForm[0].addError('Vendor Invoicing & Personnel On Location or Off Location must not be empty in order to set the Service Status to "Complete".');                           
    }              
}