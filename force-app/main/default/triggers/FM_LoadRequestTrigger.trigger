trigger FM_LoadRequestTrigger on FM_Load_Request__c (
    before insert, 
    before update, 
    before delete, 
    after insert, 
    after update, 
    after delete, 
    after undelete) {

    if(FM_Utilities.executeTriggerCode) {
        if (Trigger.isBefore) {
            if(Trigger.isInsert)
                FM_LoadRequest_Utilities.TriggerBeforeInsertLoadRequest(Trigger.new);
            else if (Trigger.isUpdate)
                FM_LoadRequest_Utilities.TriggerBeforeUpdateLoadRequest(Trigger.new, Trigger.oldMap);
        } else if (Trigger.isAfter) {
            if(Trigger.isInsert)
                FM_LoadRequest_Utilities.TriggerAfterInsertLoadRequest(Trigger.new);
            else if (Trigger.isUpdate)
                FM_LoadRequest_Utilities.TriggerAfterUpdateLoadRequest(Trigger.new, Trigger.oldMap);
        }
    }
}