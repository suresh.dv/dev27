trigger ATSFreight_PopulateSupplierAndCarrierOpportunity on ATS_Freight__C (after insert, after update) {
    Set<Id> oppIds = new Set<Id>();
    
    for(ATS_Freight__c f : trigger.new) {
        if(f.ATS_Freight__c != null)
            oppIds.add(f.ATS_Freight__c);
    }

    List<Opportunity> oppList = [Select Id,
                                    Name, 
                                    Supplier_Winner__c, 
                                    Carrier_Winner_Multi__c,
                                    StageName, 
                                    (Select Id,
                                            Husky_Supplier_1_Selected__c,
                                            Husky_Supplier_2_Selected__c,
                                            Husky_Supplier_1__c,
                                            Husky_Supplier_2__c,
                                            Emulsions_Carrier_Supplier1__c,
                                            Emulsions_Carrier_Supplier2__c
                                            From Freight_Ats__r)
                                From Opportunity Where Id in :oppIds];

    for(Opportunity opp : oppList) {
        if (opp.Freight_Ats__r != null && opp.Freight_Ats__r.size() > 0)
        {
            ATS_Freight__c freight = opp.Freight_Ats__r[0];
            if(freight.Husky_Supplier_1_Selected__c == true) {
                opp.Supplier_Winner__c = freight.Husky_Supplier_1__c;
                opp.Carrier_Winner_Multi__c = freight.Emulsions_Carrier_Supplier1__c;
            } else if (freight.Husky_Supplier_2_Selected__c == true) {
                opp.Supplier_Winner__c = freight.Husky_Supplier_2__c;
                opp.Carrier_Winner_Multi__c = freight.Emulsions_Carrier_Supplier2__c;
            } else {
                if(opp.StageName != 'Won') {
                    opp.Supplier_Winner__c = null;
                    opp.Carrier_Winner_Multi__c = null;
                } else {
                    Trigger.new[0].addError('You must have one Husky Supplier selected since '
                        + opp.Name + ' is a won Opportunity');
                }
            }
        }
    }

    try {
        update oppList;
    } catch (DmlException e){
        Trigger.new[0].addError(e.getDmlMessage(0));
    }
}