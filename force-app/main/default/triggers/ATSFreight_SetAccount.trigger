trigger ATSFreight_SetAccount on ATS_Freight__c (before insert, before update) 
{
    Map<Id,Id> oppIdAccIdMap = new Map<Id, Id>(); 
    if(Trigger.isInsert)
    {
        List<Id> oppIds = new List<Id>();
        for(ATS_Freight__c  freightRecord : Trigger.new)
        {
            oppIds.add(freightRecord.ATS_Freight__c); 
        }
        List<Opportunity> opps = [SELECT Id, AccountId FROM Opportunity WHERE Id=:oppIds];
        for(Opportunity opp : opps)
        {
            oppIdAccIdMap.put(opp.Id, opp.AccountId);
        }
    }
    else if(Trigger.isUpdate)
    {    
        List<ATS_Freight__c> freightRecords = [SELECT ATS_Freight__c, ATS_Freight__r.Account.Id FROM ATS_Freight__c WHERE Id=:Trigger.new];           
        for(ATS_Freight__c  freightRecord : freightRecords)
        {        
            oppIdAccIdMap.put(freightRecord.ATS_Freight__c, freightRecord.ATS_Freight__r.Account.Id );
        }
    }   
    for(ATS_Freight__c  freightRecord : Trigger.new)
    {
        freightRecord.Account__c = oppIdAccIdMap.get(freightRecord.ATS_Freight__c);
    }        
}