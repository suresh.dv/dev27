trigger PREP_ForecastDates on PREP_Mat_Serv_Group_Update__c( after update ){
    Id matId = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Mat Group Update').getRecordTypeId();
    Id servId = Schema.SObjectType.PREP_Mat_Serv_Group_Update__c.getRecordTypeInfosByName().get('Serv Group Update').getRecordTypeId();

    if( trigger.isUpdate ){
        Set<Id> recordId = new Set<Id>();

        for(PREP_Mat_Serv_Group_Update__c MSGU : Trigger.New){
            recordId.add(MSGU.Id);
        }

        List<PREP_Mat_Serv_Group_Update__c> objList = new List<PREP_Mat_Serv_Group_Update__c>();
        List<PREP_Mat_Serv_Group_Update__c> msList = [SELECT Planned_1_Segmentation_Team_Selection__c, Actual_1_Segmentation_Team_Selection__c, Forecast_1_Segmentation_Team_Selection__c, Forecast_2_Business_Requirements__c, Actual_2_Business_Requirements__c, Forecast_3_Supplier_Market_Analysis__c, Actual_3_Supplier_Market_Analysis__c, Forecast_4_Sourcing_Options__c, Actual_4_Sourcing_Options__c, Forecast_Strategy_Approval__c, Actual_Strategy_Approval__c, Forecast_MRQ_Received__c, Actual_MRQ_Received__c, Forecast_Bid_Request_Issued_RFQ__c, Actual_Bid_Request_Issued_RFQ__c, Forecast_Bids_Received_Mat__c, Actual_Bids_Received__c, Forecast_Comm_Eval_Received_Mat__c, Actual_Comm_Eval_Received__c, Forecast_Tech_Eval_Received_Mat__c, Actual_Tech_Eval_Received__c, Forecast_Bid_Tab_Finalized_Mat__c, Actual_Bid_Tab_Finalized__c, Forecast_Decision_Sum_Issued_Approval_M__c, Actual_Decision_Sum_Issued_Approval__c, Forecast_Decision_Summary_Approved_Mat__c, Actual_Decision_Summary_Approved__c, Forecast_MRP_Received__c, Actual_MRP_Received__c, Forecast_P_O_Issued__c, Actual_P_O_Issued__c, Forecast_Vendor_Init_Drawing_Submittal__c, Actual_Vendor_Init_Drawing_Submittal__c, Forecast_Drawings_Approved__c, Actual_Drawings_Approved__c, Forecast_Ship_Date__c, Actual_Ship_Date__c, Forecast_ETA__c, Actual_ETA__c, Forecast_Business_Requirements__c, Forecast_Supplier_Market_Analysis__c, Forecast_Sourcing_Options__c, Forecast_Strategy_Approval_Cycle__c, Forecast_MRQ_To_Be_Received__c, Forecast_RFQ_Prep__c, Forecast_Bid_Period__c, Forecast_Bid_Evaluation_1__c, Forecast_Bid_Evaluation_2__c, Forecast_Bid_Evaluation_3__c, Forecast_Bid_Evaluation_4__c, Forecast_Husky_Approval__c, Forecast_MRP_Prep__c, Forecast_PO_Prep_Issued__c, Forecast_Critical_Vendor_Data_Receipt1_W__c, Forecast_Critical_Vendor_Data_Receipt2_W__c, Forecast_Shipping_Duration__c, Forecast_MFG_Lead_Time_Weeks__c, RecordTypeId, Forecast_CWP_To_Be_Received__c, Forecast_CWP_Received__c, Actual_CWP_Received__c, Forecast_RFQ_Issued__c, Actual_RFQ_Issued__c, Forecast_Bids_Received_Serv__c, Forecast_Comm_Eval_Received_Serv__c, Forecast_Tech_Eval_Received_Serv__c, Forecast_Bid_Tab_Finalized_Serv__c, Forecast_Decision_Sum_Issued_Approval_S__c, Forecast_Decision_Summary_Approved_Serv__c, Forecast_CWP_Prep__c, Actual_Final_CWP_Received__c, Forecast_Final_CWP_Received__c, Forecast_CT_Prep_Issued__c, Forecast_Contract_Executed_LOA_Effective__c, Forecast_Kickoff_Mobilization__c, Forecast_Mobilization_Construction_Start__c, Actual_Contract_Executed_LOA_Effective__c, Actual_Mobilization_Construction_Start__c, Forecast_Duration_Of_Work_Weeks__c, Forecast_Construction_Completion__c, Forecast_Demob__c, Forecast_Demob_Cycle__c, Actual_Construction_Completion__c, Delivery_Variance_Days_Mat__c, R_A_S_Date__c, Actual_Demob__c, CWP_Completion__c, Delivery_Variance_Days_Serv__c FROM PREP_Mat_Serv_Group_Update__c WHERE Id in :recordId];

        Date today = Date.today();
        for(PREP_Mat_Serv_Group_Update__c msguObj : msList){
            if(msguObj.RecordTypeId == matId){
                if(PREP_triggerHelper.firstRun == true){
                    if(msguObj.Planned_1_Segmentation_Team_Selection__c < today){
                        msguObj.Forecast_1_Segmentation_Team_Selection__c = today;
                    } else {
                        msguObj.Forecast_1_Segmentation_Team_Selection__c = msguObj.Planned_1_Segmentation_Team_Selection__c;
                    }

                    if(msguObj.Actual_1_Segmentation_Team_Selection__c == null){
                        if((msguObj.Forecast_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c)) < today){
                            msguObj.Forecast_2_Business_Requirements__c = today;
                        } else {
                            msguObj.Forecast_2_Business_Requirements__c = (msguObj.Forecast_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c));
                        }
                    } else {
                        if( (msguObj.Actual_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c)) < today) {
                            msguObj.Forecast_2_Business_Requirements__c = today;
                        } else {
                            msguObj.Forecast_2_Business_Requirements__c = msguObj.Actual_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c);
                        }   
                    }

                    if(msguObj.Actual_2_Business_Requirements__c == null) {
                        if((msguObj.Forecast_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c)) < today) {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = today;
                        } else {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = (msguObj.Forecast_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c));
                        }
                    } else {
                        if( (msguObj.Actual_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c)) < today) {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = today;
                        } else {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = msguObj.Actual_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c);
                        }   
                    }

                    if(msguObj.Actual_3_Supplier_Market_Analysis__c == null){
                        if((msguObj.Forecast_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c)) < today){
                            msguObj.Forecast_4_Sourcing_Options__c = today;
                        } else {
                            msguObj.Forecast_4_Sourcing_Options__c = (msguObj.Forecast_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c));
                        }
                    } else {
                        if( (msguObj.Actual_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c)) < today){
                            msguObj.Forecast_4_Sourcing_Options__c = today;
                        } else {
                            msguObj.Forecast_4_Sourcing_Options__c = msguObj.Actual_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c);
                        }   
                    }

                    if(msguObj.Actual_4_Sourcing_Options__c == null) {
                        if((msguObj.Forecast_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c)) < today) {
                            msguObj.Forecast_Strategy_Approval__c = today;
                        } else {
                            msguObj.Forecast_Strategy_Approval__c = (msguObj.Forecast_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c));
                        }
                    } else {
                        if( (msguObj.Actual_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c)) < today) {
                            msguObj.Forecast_Strategy_Approval__c = today;
                        } else {
                            msguObj.Forecast_Strategy_Approval__c = msguObj.Actual_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c);
                        }   
                    }
                    
                    if(msguObj.Actual_Strategy_Approval__c == null) {
                        if((msguObj.Forecast_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_MRQ_To_Be_Received__c)) < today) {
                            msguObj.Forecast_MRQ_Received__c = today;
                        } else {
                            msguObj.Forecast_MRQ_Received__c = (msguObj.Forecast_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_MRQ_To_Be_Received__c));
                        }
                    } else {
                        if( (msguObj.Actual_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_MRQ_To_Be_Received__c)) < today) {
                            msguObj.Forecast_MRQ_Received__c = today;
                        } else {
                            msguObj.Forecast_MRQ_Received__c = msguObj.Actual_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_MRQ_To_Be_Received__c);
                        }   
                    }

                    if(msguObj.Actual_MRQ_Received__c == null){
                        if((msguObj.Forecast_MRQ_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c)) < today){
                            msguObj.Forecast_Bid_Request_Issued_RFQ__c = today;
                        } else {
                            msguObj.Forecast_Bid_Request_Issued_RFQ__c = (msguObj.Forecast_MRQ_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c));
                        }
                    } else {
                        if( (msguObj.Actual_MRQ_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c)) < today){
                            msguObj.Forecast_Bid_Request_Issued_RFQ__c = today;
                        } else {
                            msguObj.Forecast_Bid_Request_Issued_RFQ__c = msguObj.Actual_MRQ_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c);
                        }   
                    }

                    if(msguObj.Actual_Bid_Request_Issued_RFQ__c == null) {
                        if((msguObj.Forecast_Bid_Request_Issued_RFQ__c + Integer.valueof(msguObj.Forecast_Bid_Period__c)) < today) {
                            msguObj.Forecast_Bids_Received_Mat__c = today;
                        } else {
                            msguObj.Forecast_Bids_Received_Mat__c = (msguObj.Forecast_Bid_Request_Issued_RFQ__c + Integer.valueof(msguObj.Forecast_Bid_Period__c));
                        }
                    } else {
                        if( (msguObj.Actual_Bid_Request_Issued_RFQ__c + Integer.valueof(msguObj.Forecast_Bid_Period__c)) < today){
                            msguObj.Forecast_Bids_Received_Mat__c = today;
                        } else {
                            msguObj.Forecast_Bids_Received_Mat__c = msguObj.Actual_Bid_Request_Issued_RFQ__c + Integer.valueof(msguObj.Forecast_Bid_Period__c);
                        }   
                    }

                    if(msguObj.Actual_Bids_Received__c == null){
                        if((msguObj.Forecast_Bids_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c)) < today){
                            msguObj.Forecast_Comm_Eval_Received_Mat__c = today;
                        } else {
                            msguObj.Forecast_Comm_Eval_Received_Mat__c = (msguObj.Forecast_Bids_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c));
                        }
                    } else {
                        if( (msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c)) < today) {
                            msguObj.Forecast_Comm_Eval_Received_Mat__c = today;
                        } else {
                            msguObj.Forecast_Comm_Eval_Received_Mat__c = msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c);
                        }   
                    }

                    if(msguObj.Actual_Bids_Received__c == null){
                        if((msguObj.Forecast_Bids_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c)) < today){
                            msguObj.Forecast_Tech_Eval_Received_Mat__c = today;
                        } else {
                            msguObj.Forecast_Tech_Eval_Received_Mat__c = (msguObj.Forecast_Bids_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c));
                        }
                    } else {
                        if( (msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c)) < today) {
                            msguObj.Forecast_Tech_Eval_Received_Mat__c = today;
                        } else {
                            msguObj.Forecast_Tech_Eval_Received_Mat__c = msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c);
                        }   
                    }

                    if(msguObj.Actual_Comm_Eval_Received__c == null || msguObj.Actual_Tech_Eval_Received__c == null) {
                        if(msguObj.Forecast_Comm_Eval_Received_Mat__c > msguObj.Forecast_Tech_Eval_Received_Mat__c){
                            if((msguObj.Forecast_Comm_Eval_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c)) < today) {
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = today;
                            } else {
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = msguObj.Forecast_Comm_Eval_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);
                            }
                        } else {
                            if(  (msguObj.Forecast_Tech_Eval_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c))  < today) {
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = today;
                            } else {
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = msguObj.Forecast_Tech_Eval_Received_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);
                            }
                        }
                    }

                    else
                    {
                        if(msguObj.Actual_Comm_Eval_Received__c > msguObj.Actual_Tech_Eval_Received__c)
                        {
                            if((msguObj.Actual_Comm_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c)) < today)
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = today;
                            }

                            else
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = msguObj.Actual_Comm_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);

                            }
                        }

                        else
                        {
                            if(today > (msguObj.Actual_Tech_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c)))
                            {
                                System.debug(PREP_triggerHelper.firstRun);
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = today;
                            }

                            else
                            {   
                                msguObj.Forecast_Bid_Tab_Finalized_Mat__c = msguObj.Actual_Tech_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);
                            }
                        }
                    }

                    if(msguObj.Actual_Bid_Tab_Finalized__c == null)
                    {
                        if((msguObj.Forecast_Bid_Tab_Finalized_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c)) < today)
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_M__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_M__c = (msguObj.Forecast_Bid_Tab_Finalized_Mat__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Bid_Tab_Finalized__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c)) < today)
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_M__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_M__c = msguObj.Actual_Bid_Tab_Finalized__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c);
                        }   
                    }


                    if(msguObj.Actual_Decision_Sum_Issued_Approval__c == null)
                    {
                        if((msguObj.Forecast_Decision_Sum_Issued_Approval_M__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c)) < today)
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Mat__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Mat__c = (msguObj.Forecast_Decision_Sum_Issued_Approval_M__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Decision_Sum_Issued_Approval__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c)) < today)
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Mat__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Mat__c = msguObj.Actual_Decision_Sum_Issued_Approval__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c);
                        }   
                    }

                    if(msguObj.Actual_Decision_Summary_Approved__c == null)
                    {
                        if((msguObj.Forecast_Decision_Summary_Approved_Mat__c + Integer.valueof(msguObj.Forecast_MRP_Prep__c)) < today)
                        {
                            msguObj.Forecast_MRP_Received__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_MRP_Received__c = (msguObj.Forecast_Decision_Summary_Approved_Mat__c + Integer.valueof(msguObj.Forecast_MRP_Prep__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Decision_Summary_Approved__c + Integer.valueof(msguObj.Forecast_MRP_Prep__c)) < today)
                        {
                            msguObj.Forecast_MRP_Received__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_MRP_Received__c = msguObj.Actual_Decision_Summary_Approved__c + Integer.valueof(msguObj.Forecast_MRP_Prep__c);
                        }   
                    }

                    if(msguObj.Actual_MRP_Received__c == null)
                    {
                        if((msguObj.Forecast_MRP_Received__c + Integer.valueof(msguObj.Forecast_PO_Prep_Issued__c)) < today)
                        {
                            msguObj.Forecast_P_O_Issued__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_P_O_Issued__c = (msguObj.Forecast_MRP_Received__c + Integer.valueof(msguObj.Forecast_PO_Prep_Issued__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_MRP_Received__c + Integer.valueof(msguObj.Forecast_PO_Prep_Issued__c)) < today)
                        {
                            msguObj.Forecast_P_O_Issued__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_P_O_Issued__c = msguObj.Actual_MRP_Received__c + Integer.valueof(msguObj.Forecast_PO_Prep_Issued__c);
                        }   
                    }


                    if(msguObj.Actual_P_O_Issued__c == null)
                    {
                        if((msguObj.Forecast_P_O_Issued__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt1_W__c)*7) < today)
                        {
                            msguObj.Forecast_Vendor_Init_Drawing_Submittal__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Vendor_Init_Drawing_Submittal__c = (msguObj.Forecast_P_O_Issued__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt1_W__c)*7);
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_P_O_Issued__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt1_W__c)*7) < today)
                        {
                            msguObj.Forecast_Vendor_Init_Drawing_Submittal__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Vendor_Init_Drawing_Submittal__c = msguObj.Actual_P_O_Issued__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt1_W__c)*7;
                        }   
                    }


                    if(msguObj.Actual_Vendor_Init_Drawing_Submittal__c == null)
                    {
                        if((msguObj.Forecast_Vendor_Init_Drawing_Submittal__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt2_W__c)*7) < today)
                        {
                            msguObj.Forecast_Drawings_Approved__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Drawings_Approved__c = (msguObj.Forecast_Vendor_Init_Drawing_Submittal__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt2_W__c)*7);
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Vendor_Init_Drawing_Submittal__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt2_W__c)*7) < today)
                        {
                            msguObj.Forecast_Drawings_Approved__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Drawings_Approved__c = msguObj.Actual_Vendor_Init_Drawing_Submittal__c + Integer.valueof(msguObj.Forecast_Critical_Vendor_Data_Receipt2_W__c)*7;
                        }   
                    }

                    
                    if(msguObj.Actual_Drawings_Approved__c == null)
                    {
                        if((msguObj.Forecast_Drawings_Approved__c + Integer.valueof(msguObj.Forecast_MFG_Lead_Time_Weeks__c)*7) < today)
                        {
                            msguObj.Forecast_Ship_Date__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Ship_Date__c = (msguObj.Forecast_Drawings_Approved__c + Integer.valueof(msguObj.Forecast_MFG_Lead_Time_Weeks__c)*7);
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Drawings_Approved__c + Integer.valueof(msguObj.Forecast_MFG_Lead_Time_Weeks__c)*7) < today)
                        {
                            msguObj.Forecast_Ship_Date__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Ship_Date__c = msguObj.Actual_Drawings_Approved__c + Integer.valueof(msguObj.Forecast_MFG_Lead_Time_Weeks__c)*7;
                        }   
                    }



                    if(msguObj.Actual_Ship_Date__c == null)
                    {
                        if((msguObj.Forecast_Ship_Date__c + Integer.valueof(msguObj.Forecast_Shipping_Duration__c)) < today)
                        {
                            msguObj.Forecast_ETA__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_ETA__c = (msguObj.Forecast_Ship_Date__c + Integer.valueof(msguObj.Forecast_Shipping_Duration__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Ship_Date__c + Integer.valueof(msguObj.Forecast_Shipping_Duration__c)) < today)
                        {
                            msguObj.Forecast_ETA__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_ETA__c = msguObj.Actual_Ship_Date__c + Integer.valueof(msguObj.Forecast_Shipping_Duration__c);
                        }   
                    }

                    if(msguObj.Actual_ETA__c == null)
                    {
                        msguObj.Delivery_Variance_Days_Mat__c = msguObj.Forecast_ETA__c.daysBetween(msguObj.R_A_S_Date__c);
                    }

                    else
                    {
                        msguObj.Delivery_Variance_Days_Mat__c = msguObj.Actual_ETA__c.daysBetween(msguObj.R_A_S_Date__c);
                    }


                    PREP_triggerHelper.firstRun = false;
                    update msguObj;
                }
            }

            else if(msguObj.RecordTypeId == servId)
            {
                if(PREP_triggerHelper.firstRun == true)
                {

                    if(msguObj.Planned_1_Segmentation_Team_Selection__c < today)
                    {
                        msguObj.Forecast_1_Segmentation_Team_Selection__c = today;
                    }

                    else
                    {
                        msguObj.Forecast_1_Segmentation_Team_Selection__c = msguObj.Planned_1_Segmentation_Team_Selection__c;
                    }


                    if(msguObj.Actual_1_Segmentation_Team_Selection__c == null)
                    {
                        if((msguObj.Forecast_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c)) < today)
                        {
                            msguObj.Forecast_2_Business_Requirements__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_2_Business_Requirements__c = (msguObj.Forecast_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c));
                        }
                    }

                    else
                    {
                        if( (msguObj.Actual_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c)) < today)
                        {
                            msguObj.Forecast_2_Business_Requirements__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_2_Business_Requirements__c = msguObj.Actual_1_Segmentation_Team_Selection__c + Integer.valueof(msguObj.Forecast_Business_Requirements__c);
                        }   
                    }



                    if(msguObj.Actual_2_Business_Requirements__c == null)
                    {
                        if((msguObj.Forecast_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c)) < today)
                        {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = (msguObj.Forecast_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c)) < today)
                        {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_3_Supplier_Market_Analysis__c = msguObj.Actual_2_Business_Requirements__c + Integer.valueof(msguObj.Forecast_Supplier_Market_Analysis__c);
                        }   
                    }


                    if(msguObj.Actual_3_Supplier_Market_Analysis__c == null)
                    {
                        if((msguObj.Forecast_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c)) < today)
                        {
                            msguObj.Forecast_4_Sourcing_Options__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_4_Sourcing_Options__c = (msguObj.Forecast_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c)) < today)
                        {
                            msguObj.Forecast_4_Sourcing_Options__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_4_Sourcing_Options__c = msguObj.Actual_3_Supplier_Market_Analysis__c + Integer.valueof(msguObj.Forecast_Sourcing_Options__c);
                        }   
                    }

                    if(msguObj.Actual_4_Sourcing_Options__c == null)
                    {
                        if((msguObj.Forecast_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c)) < today)
                        {
                            msguObj.Forecast_Strategy_Approval__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Strategy_Approval__c = (msguObj.Forecast_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c)) < today)
                        {
                            msguObj.Forecast_Strategy_Approval__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Strategy_Approval__c = msguObj.Actual_4_Sourcing_Options__c + Integer.valueof(msguObj.Forecast_Strategy_Approval_Cycle__c);
                        }   
                    }
                    

                    if(msguObj.Actual_Strategy_Approval__c == null)
                    {
                        if((msguObj.Forecast_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_CWP_To_Be_Received__c)) < today)
                        {
                            msguObj.Forecast_CWP_Received__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_CWP_Received__c = (msguObj.Forecast_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_CWP_To_Be_Received__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_CWP_To_Be_Received__c)) < today)
                        {
                            msguObj.Forecast_CWP_Received__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_CWP_Received__c = msguObj.Actual_Strategy_Approval__c + Integer.valueof(msguObj.Forecast_CWP_To_Be_Received__c);
                        }   
                    }


                    if(msguObj.Actual_CWP_Received__c == null)
                    {
                        if((msguObj.Forecast_CWP_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c)) < today)
                        {
                            msguObj.Forecast_RFQ_Issued__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_RFQ_Issued__c = (msguObj.Forecast_CWP_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_CWP_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c)) < today)
                        {
                            msguObj.Forecast_RFQ_Issued__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_RFQ_Issued__c = msguObj.Actual_CWP_Received__c + Integer.valueof(msguObj.Forecast_RFQ_Prep__c);
                        }   
                    }


                    if(msguObj.Actual_RFQ_Issued__c == null)
                    {
                        if((msguObj.Forecast_RFQ_Issued__c + Integer.valueof(msguObj.Forecast_Bid_Period__c)) < today)
                        {
                            msguObj.Forecast_Bids_Received_Serv__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Bids_Received_Serv__c = (msguObj.Forecast_RFQ_Issued__c + Integer.valueof(msguObj.Forecast_Bid_Period__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_RFQ_Issued__c + Integer.valueof(msguObj.Forecast_Bid_Period__c)) < today)
                        {
                            msguObj.Forecast_Bids_Received_Serv__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Bids_Received_Serv__c = msguObj.Actual_RFQ_Issued__c + Integer.valueof(msguObj.Forecast_Bid_Period__c);
                        }   
                    }



                    if(msguObj.Actual_Bids_Received__c == null)
                    {
                        if((msguObj.Forecast_Bids_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c)) < today)
                        {
                            msguObj.Forecast_Comm_Eval_Received_Serv__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Comm_Eval_Received_Serv__c = (msguObj.Forecast_Bids_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c)) < today)
                        {
                            msguObj.Forecast_Comm_Eval_Received_Serv__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Comm_Eval_Received_Serv__c = msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_1__c);
                        }   
                    }


                    if(msguObj.Actual_Bids_Received__c == null)
                    {
                        if((msguObj.Forecast_Bids_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c)) < today)
                        {
                            msguObj.Forecast_Tech_Eval_Received_Serv__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Tech_Eval_Received_Serv__c = (msguObj.Forecast_Bids_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c)) < today)
                        {
                            msguObj.Forecast_Tech_Eval_Received_Serv__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Tech_Eval_Received_Serv__c = msguObj.Actual_Bids_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_2__c);
                        }   
                    }


                    if(msguObj.Actual_Comm_Eval_Received__c == null || msguObj.Actual_Tech_Eval_Received__c == null)
                    {
                        if(msguObj.Forecast_Comm_Eval_Received_Serv__c > msguObj.Forecast_Tech_Eval_Received_Serv__c)
                        {

                            if((msguObj.Forecast_Comm_Eval_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c)) < today)
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = today;
                            }
                        
                            else
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = msguObj.Forecast_Comm_Eval_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);
                            }


                        }

                        else
                        {
                            if(  (msguObj.Forecast_Tech_Eval_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c))  < today)
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = today;
                            }

                            else
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = msguObj.Forecast_Tech_Eval_Received_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);

                            }
                        }

                    }

                    else
                    {
                        if(msguObj.Actual_Comm_Eval_Received__c > msguObj.Actual_Tech_Eval_Received__c)
                        {
                            if((msguObj.Actual_Comm_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c)) < today)
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = today;
                            }

                            else
                            {
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = msguObj.Actual_Comm_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);

                            }
                        }

                        else
                        {
                            if(today > (msguObj.Actual_Tech_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c)))
                            {
                                System.debug(PREP_triggerHelper.firstRun);
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = today;
                            }

                            else
                            {   
                                msguObj.Forecast_Bid_Tab_Finalized_Serv__c = msguObj.Actual_Tech_Eval_Received__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_3__c);
                            }
                        }
                    }

                    if(msguObj.Actual_Bid_Tab_Finalized__c == null)
                    {
                        if((msguObj.Forecast_Bid_Tab_Finalized_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c)) < today)
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_S__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_S__c = (msguObj.Forecast_Bid_Tab_Finalized_Serv__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Bid_Tab_Finalized__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c)) < today)
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_S__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Decision_Sum_Issued_Approval_S__c = msguObj.Actual_Bid_Tab_Finalized__c + Integer.valueof(msguObj.Forecast_Bid_Evaluation_4__c);
                        }   
                    }


                    if(msguObj.Actual_Decision_Sum_Issued_Approval__c == null)
                    {
                        if((msguObj.Forecast_Decision_Sum_Issued_Approval_S__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c)) < today)
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Serv__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Serv__c = (msguObj.Forecast_Decision_Sum_Issued_Approval_S__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Decision_Sum_Issued_Approval__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c)) < today)
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Serv__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Decision_Summary_Approved_Serv__c = msguObj.Actual_Decision_Sum_Issued_Approval__c + Integer.valueof(msguObj.Forecast_Husky_Approval__c);
                        }   
                    }


                    if(msguObj.Actual_Decision_Summary_Approved__c == null && msguObj.Forecast_CWP_Prep__c != null )
                    {
                        if((msguObj.Forecast_Decision_Summary_Approved_Serv__c + Integer.valueof(msguObj.Forecast_CWP_Prep__c)) < today)
                        {
                            msguObj.Forecast_Final_CWP_Received__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Final_CWP_Received__c = (msguObj.Forecast_Decision_Summary_Approved_Serv__c + Integer.valueof(msguObj.Forecast_CWP_Prep__c));
                        }

                    }

                    else if( msguObj.Forecast_CWP_Prep__c != null )
                    {
                        if( (msguObj.Actual_Decision_Summary_Approved__c + Integer.valueof(msguObj.Forecast_CWP_Prep__c)) < today)
                        {
                            msguObj.Forecast_Final_CWP_Received__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Final_CWP_Received__c = msguObj.Actual_Decision_Summary_Approved__c + Integer.valueof(msguObj.Forecast_CWP_Prep__c);
                        }   
                    }


                    if(msguObj.Actual_Final_CWP_Received__c == null)
                    {
                        if((msguObj.Forecast_Final_CWP_Received__c + Integer.valueof(msguObj.Forecast_CT_Prep_Issued__c)) < today)
                        {
                            msguObj.Forecast_Contract_Executed_LOA_Effective__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Contract_Executed_LOA_Effective__c = (msguObj.Forecast_Final_CWP_Received__c + Integer.valueof(msguObj.Forecast_CT_Prep_Issued__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Final_CWP_Received__c + Integer.valueof(msguObj.Forecast_CT_Prep_Issued__c)) < today)
                        {
                            msguObj.Forecast_Contract_Executed_LOA_Effective__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Contract_Executed_LOA_Effective__c = msguObj.Actual_Final_CWP_Received__c + Integer.valueof(msguObj.Forecast_CT_Prep_Issued__c);
                        }   
                    }


                    if(msguObj.Actual_Contract_Executed_LOA_Effective__c == null)
                    {
                        if((msguObj.Forecast_Contract_Executed_LOA_Effective__c + Integer.valueof(msguObj.Forecast_Kickoff_Mobilization__c)) < today)
                        {
                            msguObj.Forecast_Mobilization_Construction_Start__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Mobilization_Construction_Start__c = (msguObj.Forecast_Contract_Executed_LOA_Effective__c + Integer.valueof(msguObj.Forecast_Kickoff_Mobilization__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Contract_Executed_LOA_Effective__c + Integer.valueof(msguObj.Forecast_Kickoff_Mobilization__c)) < today)
                        {
                            msguObj.Forecast_Mobilization_Construction_Start__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Mobilization_Construction_Start__c = msguObj.Actual_Contract_Executed_LOA_Effective__c + Integer.valueof(msguObj.Forecast_Kickoff_Mobilization__c);
                        }   
                    }

                    if(msguObj.Actual_Mobilization_Construction_Start__c == null)
                    {
                        if((msguObj.Forecast_Mobilization_Construction_Start__c + Integer.valueof(msguObj.Forecast_Duration_Of_Work_Weeks__c)*7) < today)
                        {
                            msguObj.Forecast_Construction_Completion__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Construction_Completion__c = (msguObj.Forecast_Mobilization_Construction_Start__c + Integer.valueof(msguObj.Forecast_Duration_Of_Work_Weeks__c)*7);
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Mobilization_Construction_Start__c + Integer.valueof(msguObj.Forecast_Duration_Of_Work_Weeks__c)*7) < today)
                        {
                            msguObj.Forecast_Construction_Completion__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Construction_Completion__c = msguObj.Actual_Mobilization_Construction_Start__c + Integer.valueof(msguObj.Forecast_Duration_Of_Work_Weeks__c)*7;
                        }   
                    }


                    if(msguObj.Actual_Construction_Completion__c == null)
                    {
                        if((msguObj.Forecast_Construction_Completion__c + Integer.valueof(msguObj.Forecast_Demob_Cycle__c)) < today)
                        {
                            msguObj.Forecast_Demob__c = today;
                        }

                        else
                        {
                            msguObj.Forecast_Demob__c = (msguObj.Forecast_Construction_Completion__c + Integer.valueof(msguObj.Forecast_Demob_Cycle__c));
                        }

                    }

                    else
                    {
                        if( (msguObj.Actual_Construction_Completion__c + Integer.valueof(msguObj.Forecast_Demob_Cycle__c)) < today)
                        {
                            msguObj.Forecast_Demob__c = today;
                        }   

                        else
                        {
                            msguObj.Forecast_Demob__c = msguObj.Actual_Construction_Completion__c + Integer.valueof(msguObj.Forecast_Demob_Cycle__c);
                        }   
                    }

                    if(msguObj.Actual_Demob__c == null)
                    {
                        msguObj.Delivery_Variance_Days_Serv__c = msguObj.Forecast_Demob__c.daysBetween(msguObj.CWP_Completion__c);
                    }

                    else
                    {
                        msguObj.Delivery_Variance_Days_Serv__c = msguObj.Actual_Demob__c.daysBetween(msguObj.CWP_Completion__c);
                    }


                    PREP_triggerHelper.firstRun = false;
                    update msguObj;
                }
            }   
        }
    }
}