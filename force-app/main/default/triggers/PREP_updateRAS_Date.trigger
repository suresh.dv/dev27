trigger PREP_updateRAS_Date on PREP_Mat_Serv_Group_Planned_Schedule__c (after insert,after update) {
if(Trigger.isAfter && Trigger.isUpdate) {
    PREP_updateRAS_Date.updateRASDates(Trigger.new);
    }
}