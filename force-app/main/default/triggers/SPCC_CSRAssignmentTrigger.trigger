trigger SPCC_CSRAssignmentTrigger on SPCC_CSR_Assignment__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

	if(SPCC_Utilities.executeTriggerCode) {
		if (Trigger.isBefore) {
			if(Trigger.isInsert)
	    		SPCC_Utilities.TriggerBeforeInsertCSRAssignment(Trigger.new);
		} else if (Trigger.isAfter) {
	    	if(Trigger.isInsert)
	    		SPCC_Utilities.TriggerAfterInsertCSRAssignment(Trigger.new);
	    	else if (Trigger.isDelete)
	    		SPCC_Utilities.TriggerAfterDeleteCSRAssignment(Trigger.old);
		}
	}
}