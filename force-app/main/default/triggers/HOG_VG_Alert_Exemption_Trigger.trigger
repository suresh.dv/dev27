/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger Handling After Update and After Insert actions on Exemption Requests.
Test Class:     TestNewHOGTriggers
History:        jschn 18. 9. 2018 - Created.
**************************************************************************************************/
trigger HOG_VG_Alert_Exemption_Trigger on HOG_Vent_Gas_Alert_Exemption_Request__c (after update, after insert) {

    HOG_VG_Alert_Exemption_TriggerHelper handler = new HOG_VG_Alert_Exemption_TriggerHelper(Trigger.new, Trigger.oldMap);

    if(Trigger.isAfter) {
        if(Trigger.isUpdate) {
            handler.handleAfterUpdate();
        }
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            handler.handleAfterInsert();
        }
    }
}