/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 3/17/2020   
 */

trigger ELG_PostTrigger on ELG_Post__c (before insert, before update) {
	if(Trigger.isBefore){
		ELG_Utilities.postBeforeInsert(Trigger.new);
	}
}