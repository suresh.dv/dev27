trigger ScaffoldInspectionTrigger on Scaffold_Inspection__c (before update, after update, after insert) {

	if(Trigger.isUpdate && Trigger.isBefore) {
		ScaffoldInspectionActions.setIsCertifiedFlag(Trigger.New);
	}
    
    if((Trigger.isUpdate || Trigger.isInsert) && Trigger.isAfter) {
        ScaffoldInspectionActions.setLastPassedInspection(Trigger.New);
    }
}