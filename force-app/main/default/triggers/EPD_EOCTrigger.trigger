/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for EPD_Engine_Operating_Condition__c SObject
Test Class:     EPD_EOCTriggerTest
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
trigger EPD_EOCTrigger on EPD_Engine_Operating_Condition__c (before delete) {

    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            new EPD_DeletionRestrictor().restrictDeletion(Trigger.old);
        }
    }

}