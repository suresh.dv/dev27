trigger WeldingRequestTrigger on Welding_Request__c (after update, before insert, before update) {

    if(Trigger.isBefore && (Trigger.isUpdate || Trigger.isInsert)) {
        WeldingRequestAction.setSearchablefields(Trigger.new);
    }
    if(Trigger.isUpdate && Trigger.isAfter) {
        WeldingRequestAction.GenerateReviewSheetAndSend(Trigger.newMap, Trigger.oldMap);
    }
}