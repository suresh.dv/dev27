trigger FM_Equipment_Trigger on Equipment__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {
	
	If (FM_Utilities.executeTriggerCode)
    {
        System.debug('\n*****************************************\n'
            + 'METHOD: FM_Equipment_Trigger()'
            + '\nTrigger: executed'
            + '\n************************************************\n');

        if(Trigger.isBefore) {

    	} else if (Trigger.isAfter) {
    		if(Trigger.isInsert) {
    			FM_Utilities.TriggerAfterInsertEquipment(Trigger.new);
    		}
    	}
    }
}