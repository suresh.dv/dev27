// ============================================================================
// Delete rigless service form when deleting associated service request form.
trigger deleteRiglessServiceForm on Service_Request_Form__c (Before delete) {

  // Get the list of the rigless service form to be deleted
  List <Rigless_Servicing_Form__c> oldRiglessServices = 
    [SELECT Id, Name FROM Rigless_Servicing_Form__c 
            WHERE Service_Request_Form__c IN :Trigger.oldMap.keySet()];
            
  // Delete service form.
  delete oldRiglessServices;
}