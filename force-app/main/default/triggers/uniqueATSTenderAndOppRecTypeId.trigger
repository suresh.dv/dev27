trigger uniqueATSTenderAndOppRecTypeId on Opportunity (before insert, before update) {
    for(Opportunity opp : Trigger.New) {
        if(opp.Opportunity_ATS_Product_Category__c != Null) {
            opp.ATSTenderAndOppRecTypeId__c = '' + opp.Opportunity_ATS_Product_Category__c + opp.RecordTypeId;
        }
    }
}