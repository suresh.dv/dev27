trigger SPCC_BlanketPO_LineItemTrigger on SPCC_BPO_Line_Item__c (
    before insert,
    before update,
    before delete,
    after insert,
    after update,
    after delete,
    after undelete) {

    if(SPCC_Utilities.executeTriggerCode) {

        if (Trigger.isBefore) {
            if (Trigger.isInsert) {
                SPCC_Utilities.TriggerBeforeInsertBpoLineItem(Trigger.new);
            } else if (Trigger.isUpdate) {
                SPCC_Utilities.TriggerBeforeUpdateBpoLineItem(Trigger.new);
            }
        } else if (Trigger.isAfter) {
            if (Trigger.isUpdate) {
                SPCC_Utilities.TriggerAfterUpdateBpoLineItem(Trigger.new, Trigger.oldMap);
            }
        }
    }
}