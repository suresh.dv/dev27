trigger UpdateOperatorRoute on Service_Request_Form__c (before insert, before update) 
{
    List<Service_Request_Form__c> serviceRequestForm = Trigger.new;
    
    List<Route__c> route =
        [Select Route__c.Id From Route__c Where Route__c.Route_Number__c =: serviceRequestForm[0].Route_if_different__c Limit 1];
    
    if (route.size() > 0)
        serviceRequestForm[0].Operator_Route__c = route[0].Id;    
}