trigger ChangeRequestTrigger on Change_Request__c ( before insert, before update ){
    if( trigger.isBefore ){
        if( trigger.isInsert || trigger.isUpdate ){
            Set<String> poNumberSet = new Set<String>();
            for( Change_Request__c chq : trigger.new )
            {
                if( chq.Document_Number__c != null )
                    poNumberSet.add( chq.Document_Number__c );
            }
            
            List<Change_Request__c> requests = [ Select Id, PCN_Number__c, Name, Document_Number__c from Change_Request__c
                                                    where Document_Number__c IN :poNumberSet AND Id NOT IN :trigger.new ];
            
            Map<String, List<String>> docNumberToPONumber = new Map<String, List<String>>();
            for( Change_Request__c chq : requests ){
                List<String> pcnNos = new List<String>();
                if( docNumberToPONumber.containsKey( chq.Document_Number__c ))
                    pcnNos = docNumberToPONumber.get( chq.Document_Number__c );
                
                pcnNos.add( chq.PCN_Number__c );
                docNumberToPONumber.put( chq.Document_Number__c, pcnNos );
            }
            
            for( Change_Request__c chq : trigger.new )
            {
                if( chq.Document_Number__c != null ){
                    if( docNumberToPONumber.containsKey( chq.Document_Number__c )){
                        Integer countNo = docNumberToPONumber.get( chq.Document_Number__c ).size() + 1;
                        if( countNo < 10 )
                            chq.PCN_Number__c = chq.Document_Number__c + ' - PCN - 00' + countNo;
                        else if( countNo < 100 )
                            chq.PCN_Number__c = chq.Document_Number__c + ' - PCN - 0' + countNo;
                        else
                            chq.PCN_Number__c = chq.Document_Number__c + ' - PCN - ' + countNo;
                    } else {
                        chq.PCN_Number__c = chq.Document_Number__c + ' - PCN - 001';
                    }
                }
                else
                    chq.PCN_Number__c = 'PCN - 000';
            }
        }
    }
}