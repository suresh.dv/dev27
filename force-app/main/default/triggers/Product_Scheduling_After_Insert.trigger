trigger Product_Scheduling_After_Insert on cpm_Product_Scheduling__c (after insert) 
{
    for(cpm_Product_Scheduling__c productScheduling:trigger.new)
    {
        Id lineitemid=productScheduling.cpm_Opportunity_Line_Item_Id__c;
        OpportunityLineItem oplnitm=[select id from OpportunityLineItem where id=:lineitemid limit 1];
        oplnitm.cpm_Product_Scheduling__c=productScheduling.Id;
        update oplnitm;
    }
}