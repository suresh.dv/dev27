trigger SPCC_PurchaseOrderTrigger on SPCC_Purchase_Order__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

	if(SPCC_Utilities.executeTriggerCode) {
		if (Trigger.isBefore) {
			if(Trigger.isDelete) {
				SPCC_Utilities.TriggerBeforeDeletePurchaseOrder(Trigger.old);
			}
		} else if (Trigger.isAfter) {
			if(Trigger.isInsert) {
				SPCC_Utilities.TriggerAfterInsertPurchaseOrder(Trigger.new);
			} else if(Trigger.isUpdate) {
	    		SPCC_Utilities.TriggerAfterUpdatePurchaseOrder(Trigger.new, Trigger.oldMap);
	    	}
		}
	}
}