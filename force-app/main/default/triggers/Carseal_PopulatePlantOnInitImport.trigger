//KK 7/Apr/2014: need to do that on before update too - this way data load can be carried out, otherwise
// I won't be able to able to upsert data (insert part is fine but not the update part)

// KK 13/Mar/2014: this trigger is to populate the Plant field on initial carseal import load.
// In the initial load we have the Unit and will not have Plant.  So this trigger is only for
// import load.  If the Plant field is in fact already in the trigger input, then don't have to
// re-populate the field; otherwise, populate the Plant based on the information in Unit__c.

//trigger Carseal_PopulatePlantOnInitImport on Carseal__c (before insert) 
trigger Carseal_PopulatePlantOnInitImport on Carseal__c (before insert, before update) 
{
    List<Id> unitList = new List<Id>();
    for (Carseal__c c : trigger.new)
    {
        unitList.add(c.Unit__c);
    }
system.debug('trigger unitList ' + unitList);
    // first get the plant for each unit within the trigger
    Map<Id, Id> unitPlantMap = new Map<Id, Id>();
    for (List<Unit__c> unitList2 : [SELECT Id, Plant__c FROM Unit__c WHERE Id IN :unitList])
    {
        for (Unit__c unit : unitList2)
        {
            unitPlantMap.put(unit.Id, unit.Plant__c);
        }
    }
system.debug('trigger unitPlantMap ' + unitPlantMap);
    
    for (Carseal__c c : Trigger.new)
    {
        if (null == c.Plant__c)
        {
            c.Plant__c = unitPlantMap.get(c.Unit__c);
        }
system.debug('trigger c ' + c.Unit__c + ' and ' + c.Plant__c);        
    }
}