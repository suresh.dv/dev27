/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/22/2019   
 */

trigger ELG_TaskTrigger on ELG_Task__c (before update, after update) {


	if (Trigger.isBefore) {
		if (Trigger.new[0].Status__c != ELG_Constants.TASK_ACTIVE) {
			System.debug('Show me trigger new ' + Trigger.new);
			ELG_Utilities.taskBeforeUpdate(Trigger.new);
		}
	} else if (Trigger.isAfter) {

	}
}