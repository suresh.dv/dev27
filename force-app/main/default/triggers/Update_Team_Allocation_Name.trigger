trigger Update_Team_Allocation_Name on Team_Allocation__c (before insert, before update) {
  	// TODO[CB]: Implement more centralized Trigger handler framework and move this.
  
  	// Sanchivan's Change Start
	Set<Id> shiftIds = new Set<Id>();
	Map<Id, Shift_Configuration__c> shiftMap = new Map<Id, Shift_Configuration__c>();
	
  	for (Team_Allocation__c ta : trigger.new) {
		shiftIds.add(ta.Shift__c);	
  	}
  	
  	for(Shift_Configuration__c shiftConfig : [SELECT Id, Operating_District__c, Name, Start_Time__c, End_Time__c FROM Shift_Configuration__c]) {
  		shiftMap.put(shiftConfig.Id, shiftConfig);
  	}
	
	for (Team_Allocation__c ta : trigger.new) {
    	// Lookup shift configuration.
      	Shift_Configuration__c sc = shiftMap.get(ta.Shift__c);

	 	// Make sure operating districts match
	 	if (ta.Operating_District__c == null) {
	 		ta.Operating_District__c = sc.Operating_District__c;
	 	}
	 
	 	if (sc.Operating_District__c != ta.Operating_District__c) {
	 		ta.addError('Operating district must match shift\'s value.');
	 	}
	             
     	// Set the name
     	Datetime taDatetime = Datetime.newInstance(ta.Date__c.year(), ta.Date__c.month(), ta.Date__c.day());
     
     	ta.Team_Allocation_Name__c = 
         	+ taDatetime.format('dd-MMMM-yyyy') 
         	+ ' ' 
         	+ StringUtilities.stripWhiteSpaces(sc.Name)
         	+ ' (' + sc.Start_Time__c + ' - ' + sc.End_Time__c + ')';
  	}
  	
  
  // Sanchivan's Change End
	/*
  for (Team_Allocation__c ta : trigger.new) {
      // Lookup shift configuration.
      Shift_Configuration__c sc = ShiftConfigurationUtilities.lookupShift(ta.Shift__c);

	 // Make sure operating districts match
	 if (ta.Operating_District__c == null) {
	 	ta.Operating_District__c = sc.Operating_District__c;
	 }
	 
	 if (sc.Operating_District__c != ta.Operating_District__c) {
	 	ta.addError('Operating district must match shift\'s value.');
	 }
	             
     // Set the name
     Datetime taDatetime = Datetime.newInstance(ta.Date__c.year(), 
                                                ta.Date__c.month(), 
                                                ta.Date__c.day());
     
     ta.Team_Allocation_Name__c = 
         + taDatetime.format('dd-MMMM-yyyy') 
         + ' ' 
         + StringUtilities.stripWhiteSpaces(sc.Name)
         + ' (' + sc.Start_Time__c + ' - ' + sc.End_Time__c + ')';
  }*/
}