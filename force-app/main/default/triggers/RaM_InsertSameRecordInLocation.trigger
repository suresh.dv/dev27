/**********************************************************************************
  CreatedBy :       Accenture
  Organization:     Accenture
  Purpose   :       Trigger For insert/update Retail_Location__c record in RaM_Location__c object
  Handler Class:    RaM_RetailLocationHandler 
  Test Class:       RaM_InsertSameRecordInLocationTest
  Version:          1.0.0.1
**********************************************************************************/
trigger RaM_InsertSameRecordInLocation on Retail_Location__c (after insert, after update) {
    // for insert
    if(Trigger.isAfter && Trigger.isInsert){
        system.debug('-------Trigger.New----------insert---------'+ Trigger.New);
        RaM_RetailLocationHandler.insertRetailLocationRecord(Trigger.New,Trigger.oldMap,true);
    }
    //for update
    if(Trigger.isAfter && Trigger.isUpdate){
    system.debug('-------Trigger.New----------update---------'+ Trigger.New);
        RaM_RetailLocationHandler.insertRetailLocationRecord(Trigger.New,Trigger.oldMap,false);
    }

}