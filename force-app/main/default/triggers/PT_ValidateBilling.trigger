trigger PT_ValidateBilling on Utility_Billing__c (before insert) {
    	Map<String,Service__c> ser = PTValidateBillingCacheQuery.queryServiceList();
    	Map<id,Utility_Rate__c> rat = PTValidateBillingRateCacheQuery.queryRateList();

        public double FARoundingErrorLowerLimit = double.valueOf(returnControllerSettingValue('FARoundingErrorLowerLimit'));
        public double FARoundingErrorUpperLimit = double.valueOf(returnControllerSettingValue('FARoundingErrorUpperLimit'));   

        public decimal returnControllerSettingValue(String name){
            Decimal controllerSettingValue;
            if(PT_PCSO_Settings__c.getValues(name) != null){
                controllerSettingValue = PT_PCSO_Settings__c.getValues(name).PCSO_Value__c;
            }
            else{
                controllerSettingValue = 0;
            }
            return controllerSettingValue;
        }
    
        for(Utility_Billing__c ub: Trigger.new){
        Boolean answer=false;
            if(ub.Site_ID__c != null && ser.get(ub.Site_ID__c) != null){
            Service__c sl = ser.get(ub.Site_ID__c);
            Utility_Rate__c rt = rat.get(ub.Utility_Rate__c);
            ub.service_id__c = sl.id;
            ub.service_rate__c = sl.utility_rate_id__c;
            Decimal totalBillingDemand;
            Decimal totalMeteredDemand;
            Decimal totalKVADemand;
            Decimal totalTotalVolume;
            Decimal totalWireCost;
            Decimal totalTotalCost;
            integer amountMonthsTW;
            integer amountMonthsBM;
                /*if(ub.Wire_Cost__c == 0){
                    totalBillingDemand = 0;
                    totalMeteredDemand = 0;
                    totalKVADemand = 0;
                    amountMonthsBM = 0;
                    totalTotalVolume = ub.Total_Volume__c;
                    totalWireCost = ub.Wire_Cost__c;
                    totalTotalCost = ub.total_cost__c;
                    amountMonthsTW = 1;                                      
                }
                else{*/
                    totalBillingDemand = 0;
                    totalMeteredDemand = 0;
                    totalKVADemand = 0;
                    amountMonthsBM = 0;
                    totalTotalVolume = 0;
                    totalWireCost = 0;
                    totalTotalCost = 0;
                    amountMonthsTW = 0;                                        
                //}
                if (sl.utility_billings__r.size() > 0){
                    for(Utility_Billing__c uBilling : sl.utility_billings__r){
                        	if(uBilling.Billing_Demand_KW__c != null){totalBillingDemand = totalBillingDemand + uBilling.Billing_Demand_KW__c;}
                        	if( uBilling.Metered_Demand_KW__c != null){totalMeteredDemand = totalMeteredDemand + uBilling.Metered_Demand_KW__c;}
                        	if(uBilling.Metered_Demand_KVA__c != null){totalKVADemand = totalKVADemand + uBilling.Metered_Demand_KVA__c;}
                        	if(uBilling.Total_Volume__c != null){totalTotalVolume = totalTotalVolume + uBilling.Total_Volume__c;}
                        	if(uBilling.Wire_Cost__c != null){totalWireCost = totalWireCost + uBilling.Wire_Cost__c;}
                         	if(uBilling.Total_Cost__c != null){totalTotalCost = totalTotalCost + uBilling.Total_Cost__c;}
                        if(uBilling.Wire_Cost__c == 0){
                        	amountMonthsTW++;
                        }
                        else{
                            amountMonthsBM++;
                        	amountMonthsTW++;
                        }
                    }
                    
                    if(amountMonthsBM == 0 ){
                        amountMonthsBM = 1;
                    }
                    
                    ub.Average_Billing_Demand_last_12_months__c = totalBillingDemand/amountMonthsBM;
                    ub.Average_Metered_Demand_last_12_months__c = totalMeteredDemand/amountMonthsBM;
                    ub.Average_kVA_Demand_last_12_months__c = totalKVADemand/amountMonthsBM;
                    ub.Average_Total_Volume_last_12_months__c = totalTotalVolume/amountMonthsTW;
                    ub.Average_Wire_Cost_last_12_months__c = totalWireCost/amountMonthsTW;
                    ub.Average_Total_Cost_last_12_months__c = totalTotalCost/amountMonthsTW;
                }
                else{
                	ub.Average_Billing_Demand_last_12_months__c = totalBillingDemand ;
                    ub.Average_Metered_Demand_last_12_months__c = totalMeteredDemand;
                    ub.Average_kVA_Demand_last_12_months__c = totalKVADemand;
                    ub.Average_Total_Volume_last_12_months__c = totalTotalVolume;
                    ub.Average_Wire_Cost_last_12_months__c = totalWireCost;
                    ub.Average_Total_Cost_last_12_months__c = totalTotalCost;
                }
                
                //The following lines of code is to validate if the billing is a ratchet or not
                if(sl.utility_company__r.name != 'Saskpower'){
                    if(ub.Wire_Cost__c <= 0){
                        ub.Ratchet_billing__c = false;
                    }
                    else{
                        if(sl.utility_company__c != null && sl.utility_rate_id__c != null){
                            if(sl.Utility_Company__r.name == 'FortisAlberta'){
                                if(sl.Minimum_Contract_Demand__c != null && rt.Rate_Minimum__c != null && ub.Billing_Demand_KW__c > sl.Minimum_Contract_Demand__c && ub.Billing_Demand_KW__c > rt.Rate_Minimum__c && ub.Billing_Demand_KW__c > (ub.Metered_Demand_KW__c + 0.01)){
                                    if((ub.Metered_Demand_KVA__c*0.9).setScale(2,RoundingMode.HALF_UP)+FARoundingErrorUpperLimit <double.valueof(ub.Billing_Demand_KW__c) || (ub.Metered_Demand_KVA__c*0.9).setScale(2,RoundingMode.HALF_UP)-FARoundingErrorLowerLimit >double.valueof(ub.Billing_Demand_KW__c)){
                                    	ub.Ratchet_billing__c = true;
                                    }
                                }
                                else{
                                    ub.Ratchet_billing__c = false;
                                }
                            }
                            else if(sl.Utility_Company__r.name == 'ATCO Electric'){                            
                                if(sl.Minimum_Contract_Demand__c != null && rt.Rate_Minimum__c != null && ub.Billing_Demand_KW__c > sl.Minimum_Contract_Demand__c && ub.Billing_Demand_KW__c > rt.Rate_Minimum__c && ub.Billing_Demand_KW__c > (ub.Metered_Demand_KW__c + 0.01) ){
                                    ub.Ratchet_billing__c = true;    
                                }                            
                                else{
                                    ub.Ratchet_billing__c = false;
                                }
                            }                        
                        }
                    }
                }
                //End of the code to identify if the billing is a ratchet or not
            answer = true;     
            }
            else{
            	ub.addError('Site Id not found');    
            }    
    }
}