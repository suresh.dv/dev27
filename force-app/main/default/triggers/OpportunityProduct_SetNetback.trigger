trigger OpportunityProduct_SetNetback on OpportunityLineItem (after update, after insert)
{
    // Get the opp record type we need
    RecordType asphalt = [SELECT Id
                          FROM RecordType
                          WHERE DeveloperName =: 'ATS_Asphalt_Product_Category'];
    
    // All Opportunity Products to update the netback of
    Map<Id, OpportunityLineItem> olis = new Map<Id, OpportunityLineItem>();
    
    Set<Id> opportunitiesOfOlis = new Set<Id>();
    Set<Id> productsOfOlis = new Set<Id>();
    
    if(Trigger.isUpdate)
    {
        for(Id oliId : Trigger.newMap.keySet())
        {
            OpportunityLineItem newOli = Trigger.newMap.get(oliId);
            OpportunityLineItem oldOli = Trigger.oldMap.get(oliId);
            
            // Check if it needs an update
            if(newOli.Axle_8_Price__c         != oldOli.Axle_8_Price__c         ||
               newOli.Axle_7_Price__c         != oldOli.Axle_7_Price__c         ||
               newOli.Axle_6_Price__c         != oldOli.Axle_6_Price__c         ||
               newOli.Axle_5_Price__c         != oldOli.Axle_5_Price__c         ||
               newOli.Unit__c                 != oldOli.Unit__c                 ||
               newOli.Handling_Fees_CAD__c    != oldOli.Handling_Fees_CAD__c    ||
               newOli.Additives_CAD__c        != oldOli.Additives_CAD__c        ||
               newOli.Terminal_Freight_CAD__c != oldOli.Terminal_Freight_CAD__c ||
               newOli.AC_Premium_CAD__c       != oldOli.AC_Premium_CAD__c       ||
               newOli.OpportunityId           != oldOli.OpportunityId           ||
               newOli.Currency__c             != oldOli.Currency__c             ||
               newOli.Exchange_Rate_to_CAD__c != oldOli.Exchange_Rate_to_CAD__c ||
               newOli.Product2Id              != oldOli.Product2Id)
            {
                olis.put(oliId, newOli);
                opportunitiesOfOlis.add(newOli.OpportunityId);
                productsOfOlis.add(newOli.Product2Id);
            }
        }
    }
    if(Trigger.isInsert)
    {
        for(Id oliId : Trigger.newMap.keySet())
        {
            OpportunityLineItem newOli = Trigger.newMap.get(oliId);
            
            olis.put(oliId, newOli);
            opportunitiesOfOlis.add(newOli.OpportunityId);
            productsOfOlis.add(newOli.Product2Id);
        }
    }
    
    if(opportunitiesOfOlis.size() == 0) return;
    
    // Get all freights for these olis
    Map<Id, Opportunity> opps = new Map<Id, Opportunity>([SELECT Id,
                                                                 ATS_Freight__c,
                                                                 ATS_Freight__r.Id,
                                                                 ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                                                 ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                                                 ATS_Freight__r.Prices_F_O_B__c,
                                                                 ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                                                 ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                                                 ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                                                 ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                                                 ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                                                 ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                                                 ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                                                 ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                                                 ATS_Freight__r.Supplier_1_Unit__c,
                                                                 ATS_Freight__r.Supplier_2_Unit__c
                                                          FROM Opportunity
                                                          WHERE Id IN: opportunitiesOfOlis AND
                                                                RecordTypeId =: asphalt.Id]);
     
     // Get all products for the olis
     Map<Id, Product2> products = new Map<Id, Product2>([SELECT Id,
                                                                Density__c
                                                         FROM Product2
                                                         WHERE Id IN: productsOfOlis]);
     
     // These are the olis with the updated Netback numbers
     List<OpportunityLineItem> olisToCommit = new List<OpportunityLineItem>();
     
     // For each oli we need to update, recalc both netbacks
     for(OpportunityLineItem oli : olis.values())
     {
         if(!opps.containsKey(oli.OpportunityId) || !products.containsKey(oli.Product2Id))
         {
             olisToCommit.add(new OpportunityLineItem(Id                      = oli.Id,
                                                      Refinery_Netback_CAD__c = null,
                                                      Terminal_Netback_CAD__c = null));
             
             continue;
         }
        
         // Build freight object
        
         ATS_Freight__c f = new ATS_Freight__c(Id                           = opps.get(oli.OpportunityId).ATS_Freight__r.Id,
                                               Husky_Supplier_1_Selected__c = opps.get(oli.OpportunityId).ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                               Husky_Supplier_2_Selected__c = opps.get(oli.OpportunityId).ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                               Prices_F_O_B__c              = opps.get(oli.OpportunityId).ATS_Freight__r.Prices_F_O_B__c,
                                               Emulsion_Rate8_Supplier1__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                               Emulsion_Rate8_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                               Emulsion_Rate7_Supplier1__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                               Emulsion_Rate7_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                               Emulsion_Rate6_Supplier_1__c = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                               Emulsion_Rate6_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                               Emulsion_Rate5_Supplier1__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                               Emulsion_Rate5_Supplier2__c  = opps.get(oli.OpportunityId).ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                               Supplier_1_Unit__c           = opps.get(oli.OpportunityId).ATS_Freight__r.Supplier_1_Unit__c,
                                               Supplier_2_Unit__c           = opps.get(oli.OpportunityId).ATS_Freight__r.Supplier_2_Unit__c);
         
         Product2 p = products.get(oli.Product2Id);
         
         Decimal refineryNetback = calculateNetback.calculateAsphaltRefineryNetback(oli, f, p);
         if(refineryNetback != null) refineryNetback.setScale(4, RoundingMode.HALF_UP);
         Decimal terminalNetback = calculateNetback.calculateAsphaltTerminalNetback(oli, f, p);
         if(terminalNetback != null) terminalNetback.setScale(4, RoundingMode.HALF_UP);
         
         olisToCommit.add(new OpportunityLineItem(Id                      = oli.Id,
                                                  Refinery_Netback_CAD__c = refineryNetback,
                                                  Terminal_Netback_CAD__c = terminalNetback));
     }
     
     update olisToCommit;
}