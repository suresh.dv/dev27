trigger PREP_CheckSche_Dates on PREP_Business_Value_Spend_Schedule__c (before insert) 
{
    
    List< PREP_Business_Value_Spend_Schedule__c > scheList = new List< PREP_Business_Value_Spend_Schedule__c >();

    for(PREP_Business_Value_Spend_Schedule__c spendSche : Trigger.New)
    {       
        
       Date New_Start_Date = date.newinstance(spendSche.Start_Month__c.year(), spendSche.Start_Month__c.month(), 01);
       Date New_End_Date = date.newinstance(spendSche.End_Month__c.year(), spendSche.End_Month__c.month(), 01);

       scheList = [SELECT Start_Month__c, End_Month__c FROM PREP_Business_Value_Spend_Schedule__c WHERE Business_Value_Submission_Id__c = :spendSche.Business_Value_Submission_Id__c];
       
              
       for(PREP_Business_Value_Spend_Schedule__c BVSS : scheList)
       {
           Date Start_Date = date.newinstance(BVSS.Start_Month__c.year(), BVSS.Start_Month__c.month(), 01);
           Date End_Date = date.newinstance(BVSS.End_Month__c.year(), BVSS.End_Month__c.month(), 01);
                      
           if(New_Start_Date >= Start_Date && End_Date >= New_End_Date)
           {
               spendSche.addError('ERROR: The Start Month and End Month entered overlap with the dates entered.');
           }
           
           else if(New_Start_Date <= Start_Date && End_Date <= New_End_Date)
           {
               spendSche.addError('ERROR: The Start Month and End Month entered overlap with the dates entered.');
           }
           
           else if(New_Start_Date <= Start_Date && Start_Date <= New_End_Date)
           {
               spendSche.addError('ERROR: The End Month entered overlaps with an existing Start Date.');
           }
           
           else if(New_Start_Date <= End_Date && End_Date <= New_End_Date)
           {
               spendSche.addError('ERROR: The Start Month entered overlaps with an existing End Date.');
           }
          
       }
    }
}