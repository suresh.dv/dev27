//this will look for Role of Operational Event Shift Resource in associated Team Allocation
trigger OperationalEvent_FindShiftResourceRole on Operational_Event__c (before insert, before update) {
    Set<Id> taIds = new Set<Id>();
    Set<Id> contactIds = new Set<Id>();
    for(Operational_Event__c oe : trigger.new)
    {
        if (trigger.isUpdate)
        {
            Operational_Event__c beforeUpdate = trigger.oldMap.get(oe.Id);
            if (oe.Operator_Contact__c != beforeUpdate.Operator_Contact__c)
            {
                taIds.add(oe.Team_Allocation__c);
                contactIds.add(oe.Operator_Contact__c);
            }
        }
        else if (trigger.isInsert)
        {
            taIds.add(oe.Team_Allocation__c);
            contactIds.add(oe.Operator_Contact__c);
        }
    }
        
    Map<Id, Contact> filteredContactMap = new Map<Id, Contact>([select Id, Name, 
                               (select Team_Allocation__c, Role__r.Name from Shift_Operators_O__r where Team_Allocation__c in: taIds) 
                               from Contact where Id in: contactIds]);         
    for(Operational_Event__c oe: trigger.new)
    {
        if (filteredContactMap.containsKey(oe.Operator_Contact__c))
        {
            Contact cont = filteredContactMap.get(oe.Operator_Contact__c);
            if (cont.Shift_Operators_O__r != null)
            {
                for(Shift_Operator__c operator : cont.Shift_Operators_O__r)
                {
                    if (operator.Team_Allocation__c == oe.Team_Allocation__c)
                        oe.Shift_Resource_Role__c = operator.Role__r.Name;
                }
            }
        }
    }   
}