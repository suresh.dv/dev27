trigger PREP_PSG_BVS_Create on PREP_Business_Value_Submission__c (after insert, after update) 
{
    Id bvsRecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('PS&G/MM Business Value Submission').getRecordTypeId();
     
    List< PREP_Discipline__c > DiscRecord = new List< PREP_Discipline__c >();

     
    for(PREP_Business_Value_Submission__c BVS : Trigger.New)
    {
    
        if(bvsRecordTypeId == BVS.RecordTypeId)
        {
            DiscRecord = [SELECT Name FROM PREP_Discipline__c WHERE Id = :BVS.Discipline__c];
            
            if(trigger.isInsert)
            {
                
                if(DiscRecord[0].Name == 'Atlantic MM')
                {
                    PREP_BVS_Util.createAtlanticBUSA(BVS);
                }
                
                else if(DiscRecord[0].Name == 'Lima MM')
                {
                    PREP_BVS_Util.createLimaBUSA(BVS);
                }
                
                else if(DiscRecord[0].Name == 'WCP MM')
                {
                    PREP_BVS_Util.createWCPBUSA(BVS);
                }
        
            }
            
            if(trigger.isUpdate)
            {
                 PREP_Business_Value_Submission__c beforeUpdate = trigger.oldMap.get(BVS.Id);

                 if(beforeUpdate.Discipline__c != BVS.Discipline__c)
                 {
                     
                    PREP_BVS_Util.deleteBUSA(BVS);
                    
                    if(DiscRecord[0].Name == 'Atlantic MM')
                    {
                        PREP_BVS_Util.createAtlanticBUSA(BVS);
                    }
                
                    else if(DiscRecord[0].Name == 'Lima MM')
                    {
                        PREP_BVS_Util.createLimaBUSA(BVS);
                    }
                
                    else if(DiscRecord[0].Name == 'WCP MM')
                    {
                        PREP_BVS_Util.createWCPBUSA(BVS);
                    }
                 
                 }
            }
        }
    }

}