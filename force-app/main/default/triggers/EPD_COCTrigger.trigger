/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for EPD_Compressor_Operating_Condition_Stage__c SObject
Test Class:     EPD_COCTriggerTest
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
trigger EPD_COCTrigger on EPD_Compressor_Operating_Condition_Stage__c (before delete) {

    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            new EPD_DeletionRestrictor().restrictDeletion(Trigger.old);
        }
    }

}