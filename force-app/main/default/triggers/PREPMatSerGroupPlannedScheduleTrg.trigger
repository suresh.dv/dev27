trigger PREPMatSerGroupPlannedScheduleTrg on PREP_Mat_Serv_Group_Planned_Schedule__c( before update ) 
{
    PREPMatSerGroupPlannedScheduleTrgHelper.runTrigger = true;
    PREPMatSerGroupPlannedScheduleTrgHelper.newPlannedScheduleList = trigger.new;
    PREPMatSerGroupPlannedScheduleTrgHelper.oldPlannedScheduleList = trigger.old;
    PREPMatSerGroupPlannedScheduleTrgHelper.newPlannedScheduleMap = trigger.newMap;
    PREPMatSerGroupPlannedScheduleTrgHelper.oldPlannedScheduleMap = trigger.oldMap;
    
    if( trigger.isBefore && Trigger.isUpdate ){
        if( PREPMatSerGroupPlannedScheduleTrgHelper.runTrigger ) {
            PREPMatSerGroupPlannedScheduleTrgHelper.addUpdateAcqCycle();
        }
    }
        /*for(PREP_Mat_Serv_Group_Planned_Schedule__c sche : Trigger.New)
    {
        
        List<PREP_Acquisition_Cycle__c> AC = [SELECT id FROM PREP_Acquisition_Cycle__c WHERE Mat_Serv_Group_Baseline_Allocation__c = :sche.Mat_Serv_Group_Baseline_Allocation_Id__c AND Type__c = :sche.Acquisition_Cycle_Type__c];
        
        
        if(AC.size() == 0)
        {
            sche.Acquisition_Cycle__c = null;

        }
        
        else
        {
        
            sche.Acquisition_Cycle__c = AC[0].id;
        }
        
    }

*/ 
}