/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for EPD_Engine_Performance_Data__c SObject.
Test Class:     EPD_EPDSelectorTest
                EPD_EPDTriggerTest
History:        jschn 2019-06-11 - Created. - EPD R1
*************************************************************************************************/
trigger EPD_EPDTrigger on EPD_Engine_Performance_Data__c (after insert, before delete) {

    EPD_EPDTriggerHandler handler = new EPD_EPDTriggerHandler();

    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            handler.handleAfterInsert(Trigger.new);
        }
    }
    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            new EPD_DeletionRestrictor().restrictDeletion(Trigger.old);
        }
    }

}