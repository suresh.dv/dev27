trigger PREP_LockScheAC on PREP_Mat_Serv_Group_Baseline_Allocation__c (before insert, before update) 
{

    List< PREP_Acquisition_Cycle__c > acRecord = new List< PREP_Acquisition_Cycle__c >();
    List< PREP_Mat_Serv_Group_Planned_Schedule__c > scheRecord = new List< PREP_Mat_Serv_Group_Planned_Schedule__c >();

    Id matRTI = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Mat Group Baseline Allocation').getRecordTypeId();
    Id servRTI = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Serv Group Baseline Allocation').getRecordTypeId();

    Id scheMatRTI = Schema.SObjectType.PREP_Mat_Serv_Group_Planned_Schedule__c.getRecordTypeInfosByName().get('Locked Mat Group Baseline Schedule').getRecordTypeId();
    Id scheServRTI = Schema.SObjectType.PREP_Mat_Serv_Group_Planned_Schedule__c.getRecordTypeInfosByName().get('Locked Serv Group Baseline Schedule').getRecordTypeId();

    Id acMatRTI = Schema.SObjectType.PREP_Acquisition_Cycle__c.getRecordTypeInfosByName().get('Locked Mat Acquisition Cycle').getRecordTypeId();
    Id acServRTI = Schema.SObjectType.PREP_Acquisition_Cycle__c.getRecordTypeInfosByName().get('Locked Serv Acquisition Cycle').getRecordTypeId();

    for(PREP_Mat_Serv_Group_Baseline_Allocation__c MSGBA : Trigger.New)
    {  
    	if(MSGBA.Status__c == 'Approved')
    	{
            acRecord = [SELECT Id, RecordTypeId FROM PREP_Acquisition_Cycle__c WHERE Mat_Serv_Group_Baseline_Allocation__c = :MSGBA.Id];
            scheRecord = [SELECT Id, RecordTypeId FROM PREP_Mat_Serv_Group_Planned_Schedule__c WHERE Mat_Serv_Group_Baseline_Allocation_Id__c = :MSGBA.Id];

    		if(MSGBA.RecordTypeId == matRTI)
    		{

                for(PREP_Acquisition_Cycle__c AC : acRecord)
                {
                    AC.RecordTypeId = acMatRTI;
                    update AC;
                }

                for(PREP_Mat_Serv_Group_Planned_Schedule__c sche : scheRecord)
                {
                    sche.RecordTypeId = scheMatRTI;
                    update sche;
                }

    		}

    		else if(MSGBA.RecordTypeID == servRTI)
    		{
                for(PREP_Acquisition_Cycle__c AC : acRecord)
                {
                    AC.RecordTypeId = acServRTI;
                    update AC;
                }

                for(PREP_Mat_Serv_Group_Planned_Schedule__c sche : scheRecord)
                {
                    sche.RecordTypeId = scheServRTI;
                    update sche;
                }
    		}
    	}
    }
}