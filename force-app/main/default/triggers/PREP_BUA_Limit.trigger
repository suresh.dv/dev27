trigger PREP_BUA_Limit on PREP_Business_Unit_Baseline_Allocation__c (before insert) {
    
    List<PREP_Business_Unit_Baseline_Allocation__c> buaRecord = new List<PREP_Business_Unit_Baseline_Allocation__c>();
    
    for(PREP_Business_Unit_Baseline_Allocation__c BUA : Trigger.New)
    {   
        
        buaRecord = [SELECT Id, Allocation_Level__c, Business_Unit__c FROM PREP_Business_Unit_Baseline_Allocation__c WHERE Baseline_Id__c = :BUA.Baseline_Id__c];
        
        for(Integer i = 0; i < buaRecord.size(); i++)
        {
            if(BUA.Allocation_Level__c == buaRecord[i].Allocation_Level__c && BUA.Business_Unit__c == buaRecord[i].Business_Unit__c)
            {
               BUA.addError('ERROR: The Business Unit/Allocation Level Combination you selected is already in use');
            }
        }
    }
}