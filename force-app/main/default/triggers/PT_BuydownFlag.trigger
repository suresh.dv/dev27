trigger PT_BuydownFlag on PCSO__c (before insert,before update) {
	for(PCSO__c pcso: Trigger.new){
        if(pcso.buydown_cost__c != null){
        	pcso.Buydown_Investment_flag__c = 'Estimated';            
        }
        else{
            pcso.Buydown_Investment_flag__c = 'Not Calculated';
        }
    }
}