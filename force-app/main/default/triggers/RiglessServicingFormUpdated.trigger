// ============================================================================
// Change record type if status has changed to In Progress or Complete.
// If the user has accepted job, change the status to Booked.
// If the status has been changed to Requires Reassignment, 
// reassign it to the queue and set satus to new
trigger RiglessServicingFormUpdated 
        on Rigless_Servicing_Form__c (before update) { 
 
  // Rigless Service Hopper Queue ID 
  QueueSobject riglessQueue = 
    [Select q.queueId From QueueSobject q 
                      WHERE q.Queue.Name = 'Rigless Service Hopper'];
  
  // List of rigless servicing form record types
  Map<String, Id> riglessRecordTypes = new Map<String, Id>();
  Map<Id, String> riglessRecordTypeNames = new Map<Id, String>();
  for (RecordType riglessRecordType : 
       [SELECT Name, Id FROM RecordType 
                        WHERE SobjectType = 'Rigless_Servicing_Form__c']) {
    riglessRecordTypes.put(riglessRecordType.Name.trim(), riglessRecordType.Id);
    riglessRecordTypeNames.put(riglessRecordType.Id, riglessRecordType.Name.trim());
  }
  
  // Update record type
  for (Rigless_Servicing_Form__c currForm: Trigger.new) {
    String recordTypeName = riglessRecordTypeNames.get(currForm.RecordTypeId);
    
    // If an user has accepted a job, changed the status to booked.
    if (Trigger.oldMap.get(currForm.Id).OwnerId == riglessQueue.queueId &&
        currForm.OwnerId != riglessQueue.queueId) {
      currForm.Service_Status__c = 'Booked';
    }
    
    // If the status of the job has been changed to Requires Reassignment
    // Change the ownership to the Queue and reset the status
    //
    // N.B. if we want to have different layout for different stage,
    // this will need to be updated.
    if (currForm.Service_Status__c == 'Requires Reassignment') {
      currForm.OwnerId = riglessQueue.queueId;
      currForm.Service_Status__c = 'New Request';
    }

    if (currForm.Service_Status__c == 'Complete') {
      currForm.Service_Completed__c = Date.today();    
    }
    else {
        currForm.Service_Completed__c = null;    
    }
        
  }  
}