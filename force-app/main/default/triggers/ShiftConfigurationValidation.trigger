trigger ShiftConfigurationValidation on Shift_Configuration__c (before insert, before update) {
	 // Creating object of ValidateShiftController class
    ShiftConfigurationValidation validateShift = new ShiftConfigurationValidation();
    
    // Validating all records of Shift_Configuration__c
    validateShift.validateAll(Trigger.new);	
}