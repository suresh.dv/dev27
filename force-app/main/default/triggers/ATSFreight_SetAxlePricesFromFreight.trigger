trigger ATSFreight_SetAxlePricesFromFreight on ATS_Freight__c (after update, after delete)
{
    // Stores all freights that have changed
    Set<Id> freightsAffected = new Set<Id>();
    
    // If this was an update to a Freight record, check if the relevant fields have changed.
    if(Trigger.isUpdate)
    {
        for(Id freightId : Trigger.newMap.keyset())
        {
            ATS_Freight__c newFreight = Trigger.newMap.get(freightId);
            ATS_Freight__c oldFreight = Trigger.oldMap.get(freightId);
            
            if(newFreight.Husky_Supplier_1_Selected__c != oldFreight.Husky_Supplier_1_Selected__c ||
               newFreight.Husky_Supplier_2_Selected__c != oldFreight.Husky_Supplier_2_Selected__c ||
               newFreight.Prices_F_O_B__c              != oldFreight.Prices_F_O_B__c              ||
               newFreight.Emulsion_Rate8_Supplier1__c  != oldFreight.Emulsion_Rate8_Supplier1__c  ||
               newFreight.Emulsion_Rate8_Supplier2__c  != oldFreight.Emulsion_Rate8_Supplier2__c  ||
               newFreight.Emulsion_Rate7_Supplier1__c  != oldFreight.Emulsion_Rate7_Supplier1__c  ||
               newFreight.Emulsion_Rate7_Supplier2__c  != oldFreight.Emulsion_Rate7_Supplier2__c  ||
               newFreight.Emulsion_Rate6_Supplier_1__c != oldFreight.Emulsion_Rate6_Supplier_1__c ||
               newFreight.Emulsion_Rate6_Supplier2__c  != oldFreight.Emulsion_Rate6_Supplier2__c  ||
               newFreight.Emulsion_Rate5_Supplier1__c  != oldFreight.Emulsion_Rate5_Supplier1__c  ||
               newFreight.Emulsion_Rate5_Supplier2__c  != oldFreight.Emulsion_Rate5_Supplier2__c  ||
               newFreight.Supplier_1_Unit__c           != oldFreight.Supplier_1_Unit__c           ||
               newFreight.Supplier_2_Unit__c           != oldFreight.Supplier_2_Unit__c)
            {
                freightsAffected.add(freightId);
            }
        }
    }
    // If this was a delete, no need to check if relevant fields have changed.
    if(Trigger.isDelete)
    {
        freightsAffected = Trigger.oldMap.keySet();
    }
    
    if(freightsAffected.size() == 0)
        return;
        
    // Get the opp record type we need
    RecordType emulsion = [SELECT Id
                          FROM RecordType
                          WHERE DeveloperName =: 'ATS_Emulsion_Product_Category'];
    
    // Now get all olis using these freights
    List<OpportunityLineItem> olis = [SELECT Id,
                                             Axle_8_Price__c,
                                             Axle_8_Price_Set_Manually__c,
                                             Axle_7_Price__c,
                                             Axle_7_Price_Set_Manually__c,
                                             Axle_6_Price__c,
                                             Axle_6_Price_Set_Manually__c,
                                             Axle_5_Price__c,
                                             Axle_5_Price_Set_Manually__c,
                                             Unit__c,
                                             Emulsion_Cost_CAD__c,
                                             Emulsion_Margin_CAD__c,
                                             Currency__c,
                                             Exchange_Rate_to_CAD__c,
                                             
                                             // Get the associated Opportunity
                                             
                                             OpportunityId,
                                             Opportunity.RecordTypeId,
                                             Opportunity.ATS_Freight__c,
                                             
                                             // Get the associated Product2
                                             
                                             Product2Id,
                                             Product2.Density__c,
                                             
                                             // Get the associated Freight
                                             
                                             Opportunity.ATS_Freight__r.Id,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                             Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                             Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                             Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                             Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                             Opportunity.ATS_Freight__r.Supplier_2_Unit__c
                                      FROM OpportunityLineItem
                                      WHERE Opportunity.ATS_Freight__r.Id IN: freightsAffected AND
                                            Opportunity.RecordTypeId =: emulsion.Id];
    
    // These are the olis we'll be updating to the db
    List<OpportunityLineItem> olisToUpdate = new List<OpportunityLineItem>();
    
    for(OpportunityLineItem oli : olis)
    {
        // Check if an Opportunity exists
        
        if(oli.OpportunityId == null)
        {
            olisToUpdate.add(new OpportunityLineItem(Id              = oli.Id,
                                                     Axle_5_Price__c = null,
                                                     Axle_6_Price__c = null,
                                                     Axle_7_Price__c = null,
                                                     Axle_8_Price__c = null));
            continue;
        }
        
        // Build freight object
        
        ATS_Freight__c f = new ATS_Freight__c(Id                           = oli.Opportunity.ATS_Freight__r.Id,
                                              Husky_Supplier_1_Selected__c = oli.Opportunity.ATS_Freight__r.Husky_Supplier_1_Selected__c,
                                              Husky_Supplier_2_Selected__c = oli.Opportunity.ATS_Freight__r.Husky_Supplier_2_Selected__c,
                                              Prices_F_O_B__c              = oli.Opportunity.ATS_Freight__r.Prices_F_O_B__c,
                                              Emulsion_Rate8_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier1__c,
                                              Emulsion_Rate8_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate8_Supplier2__c,
                                              Emulsion_Rate7_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier1__c,
                                              Emulsion_Rate7_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate7_Supplier2__c,
                                              Emulsion_Rate6_Supplier_1__c = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier_1__c,
                                              Emulsion_Rate6_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate6_Supplier2__c,
                                              Emulsion_Rate5_Supplier1__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier1__c,
                                              Emulsion_Rate5_Supplier2__c  = oli.Opportunity.ATS_Freight__r.Emulsion_Rate5_Supplier2__c,
                                              Supplier_1_Unit__c           = oli.Opportunity.ATS_Freight__r.Supplier_1_Unit__c,
                                              Supplier_2_Unit__c           = oli.Opportunity.ATS_Freight__r.Supplier_2_Unit__c);
        
        // Build Product2 object
        
        Product2 p = new Product2(Id         = oli.Product2Id,
                                  Density__c = oli.Product2.Density__c);
        
        // Calculate axle prices
        
        Map<Integer,Decimal> axlePrices = calculateNetback.calculateEmulsionAxlePrices(oli, f, p);
        
        OpportunityLineItem oliToCommit = new OpportunityLineItem(Id              = oli.Id,
                                                                  Axle_5_Price__c = oli.Axle_5_Price__c,
                                                                  Axle_6_Price__c = oli.Axle_6_Price__c,
                                                                  Axle_7_Price__c = oli.Axle_7_Price__c,
                                                                  Axle_8_Price__c = oli.Axle_8_Price__c,
                                                                  Axle_5_Price_Set_Manually__c = oli.Axle_5_Price_Set_Manually__c,
                                                                  Axle_6_Price_Set_Manually__c = oli.Axle_6_Price_Set_Manually__c,
                                                                  Axle_7_Price_Set_Manually__c = oli.Axle_7_Price_Set_Manually__c,
                                                                  Axle_8_Price_Set_Manually__c = oli.Axle_8_Price_Set_Manually__c);
        
//        if(axlePrices.containsKey(5) && oli.Axle_5_Price_Set_Manually__c == false && oli.Axle_5_Price__c != null)
        if(axlePrices.containsKey(5) && oli.Axle_5_Price__c != null)
        {
            oliToCommit.Axle_5_Price__c = axlePrices.get(5);
            if(oliToCommit.axle_5_Price__c != null) oliToCommit.axle_5_Price__c = oliToCommit.axle_5_Price__c.setScale(4, RoundingMode.HALF_UP);
            oliToCommit.Axle_5_Price_Set_Manually__c = false;
        }
//        if(axlePrices.containsKey(6) && oli.Axle_6_Price_Set_Manually__c == false && oli.Axle_6_Price__c != null)
        if(axlePrices.containsKey(6) && oli.Axle_6_Price__c != null)
        {
            oliToCommit.Axle_6_Price__c = axlePrices.get(6);
            if(oliToCommit.axle_6_Price__c != null) oliToCommit.axle_6_Price__c = oliToCommit.axle_6_Price__c.setScale(4, RoundingMode.HALF_UP);
            oliToCommit.Axle_6_Price_Set_Manually__c = false;
        }
//        if(axlePrices.containsKey(7) && oli.Axle_7_Price_Set_Manually__c == false && oli.Axle_7_Price__c != null)
        if(axlePrices.containsKey(7) && oli.Axle_7_Price__c != null)
        {
            oliToCommit.Axle_7_Price__c = axlePrices.get(7);
            if(oliToCommit.axle_7_Price__c != null) oliToCommit.axle_7_Price__c = oliToCommit.axle_7_Price__c.setScale(4, RoundingMode.HALF_UP);
            oliToCommit.Axle_7_Price_Set_Manually__c = false;
        }
//        if(axlePrices.containsKey(8) && oli.Axle_8_Price_Set_Manually__c == false && oli.Axle_8_Price__c != null)
        if(axlePrices.containsKey(8) && oli.Axle_8_Price__c != null)
        {
            oliToCommit.Axle_8_Price__c = axlePrices.get(8);
            if(oliToCommit.axle_8_Price__c != null) oliToCommit.axle_8_Price__c = oliToCommit.axle_8_Price__c.setScale(4, RoundingMode.HALF_UP);
            oliToCommit.Axle_8_Price_Set_Manually__c = false;
        }
        
        olisToUpdate.add(oliToCommit);
    }
    
    update olisToUpdate;
}