trigger SM_Run_Sheet_Detail_Trigger on SM_Run_Sheet_Detail__c(before update)
{
    if(SM_Utilities.bExecuteTriggerCode == true)
    {
        if(Trigger.isBefore && Trigger.isUpdate)
        {
            SM_Utilities.TriggerBeforeUpdateSandDetail(Trigger.new, Trigger.oldMap);   
        }
    }
}