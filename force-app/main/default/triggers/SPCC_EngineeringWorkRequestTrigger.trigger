/**
 * Created by mzelina@sk.ibm.com on 28/11/2019.
 */

trigger SPCC_EngineeringWorkRequestTrigger on SPCC_Engineering_Work_Request__c(
        before insert,
        before update,
        before delete,
        after insert,
        after update,
        after delete,
        after undelete) {

    if(SPCC_Utilities.executeTriggerCode) {
        if (Trigger.isBefore) {
            if (Trigger.isDelete) {
                SPCC_Utilities.TriggerBeforeDeleteEWR(Trigger.old);
            }
        } else if (Trigger.isAfter) {
            //call handler.before method
        }
    }
}