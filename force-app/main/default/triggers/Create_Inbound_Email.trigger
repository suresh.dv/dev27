trigger Create_Inbound_Email on EmailMessage (after insert) 
{
    List<Inbound_Email__c> inboundEmails = new List<Inbound_Email__c>();
    Map<Id, List<Attachment>> atchMap = new Map<Id, List<Attachment>>();
    for(EmailMessage emailMsg : Trigger.New)
    {
        if( (emailMsg.Subject).contains('Ticket:') )
        {
            //System.debug('Related To is '+emailMsg.RelatedTo);
            //System.debug('Related To is '+emailMsg.RelatedTo.Id);
            Inbound_Email__c inboundEmail = new Inbound_Email__c();
            inboundEmail.Name='Email: '+emailMsg.Subject;             
            inboundEmail.FromAddress__c=emailMsg.FromAddress; 
            inboundEmail.CcAddress__c=emailMsg.CcAddress;
            inboundEmail.BccAddress__c=emailMsg.BccAddress;
            inboundEmail.ToAddress__c=emailMsg.ToAddress;
            inboundEmail.FromName__c=emailMsg.FromName;
            inboundEmail.Related_Email_Msg_Id__c = emailMsg.Id;
            inboundEmail.HasAttachment__c=emailMsg.HasAttachment; //Headers__c=emailMsg.Headers__c,
            if(emailMsg.HasAttachment)
            {
                System.debug('Email Msg has attachment');
                //List<EmailMessage> emailMsgWithAttch = [Select Id,createddate from EmailMessage where RelatedToId=:emailMsg.RelatedToId order by createddate desc limit 1];
                //if(emsilMsgWithAttch!=null && emailMsgWithAttch.size>0)
                //{
                    System.debug('ID is... '+emailMsg.Id);
                    List<Attachment> a = [Select Id, ParentId from Attachment where ParentId=:emailMsg.Id];
                    system.debug('attachments are... '+a);
                    for(Attachment att : a)
                    {
                        if(att.parentId == emailMsg.Id)
                            system.debug('This is the one');   
                    }
                    System.debug('a must not be null...'+a);
                    atchMap.put(emailMsg.Id, a);
                    system.debug('atchMap shud contain attachment...'+atchMap);
                    
                    //a.RelatedToId = 
                //}
                

            }
            if(emailMsg.HtmlBody!=null)
                system.debug('HtmlBody is not null');
            else
                system.debug('HtmlBody is null');
            inboundEmail.HtmlBody__c=emailMsg.TextBody; //
            inboundEmail.MessageDate__c=System.now();//, MessageSize__c=emailMsg.MessageSize, 
            inboundEmail.RelatedTo__c=emailMsg.RelatedToId;            
            if(emailMsg.Status=='0')
                inboundEmail.Status__c='New';
            else if(emailMsg.Status=='0')
                inboundEmail.Status__c='New';
            else if(emailMsg.Status=='1')
                inboundEmail.Status__c='Read';
            else if(emailMsg.Status=='2')
                inboundEmail.Status__c='Replied';
            else if(emailMsg.Status=='3')
                inboundEmail.Status__c='Sent';
            else if(emailMsg.Status=='4')
                inboundEmail.Status__c='Forwarded';                             
            inboundEmails.add(inboundEmail);                                                                                       
        }
    }
    List<Database.SaveResult> sr =Database.insert(inboundEmails);
    List<Attachment> attsToUpdate = new List<Attachment>();
    for(Database.SaveResult s : sr)
    {
        if(s.isSuccess())
        {
            System.debug('Inbound Email successfully created');
            List<Inbound_Email__c> em = [Select Id, Related_Email_Msg_Id__c  from Inbound_Email__c where Id=:s.getId()];
            if(em!=null && em.size()>0 )
            {
                System.debug('Inbound Email record fetched....'+em[0].Related_Email_Msg_Id__c);
                //List<Attachment> atts = atchMap.get(em[0].Related_Email_Msg_Id__c );
                List<Attachment> atts = [SELECT Id from Attachment where ParentId=:em[0].Related_Email_Msg_Id__c ];
                if(atts!=null && atts.size()>0)
                {
                    System.debug('Attachments....'+atts);
                    for(Attachment atch : atts)
                    {
                        System.debug('Iterate attachments....');
                        Attachment a = atch.clone(false, false, false, false);
                        a.ParentId= s.getId();
                        attsToUpdate.add(a);
                    }
                }
            }
        }
    }
    if(attsToUpdate !=null && attsToUpdate.size()>0)
    {
        Database.insert(attsToUpdate);
    }
    else
    {
        System.debug('attsToUpdate is null...'+attsToUpdate);
    }
}