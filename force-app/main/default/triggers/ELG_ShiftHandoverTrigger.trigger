/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Trigger for ELG_Shift_Handover__c
Test class:		ELG_UtilitiesTest
History:        mbrimus 01.07.2019 - Created.
**************************************************************************************************/
trigger ELG_ShiftHandoverTrigger on ELG_Shift_Handover__c (before insert) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            ELG_Utilities.shiftHandoverBeforeInsert(Trigger.new);
        }
    }
}