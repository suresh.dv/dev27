trigger UpdateOperationalEvent on Operational_Event__c (before insert, before update) {
    String insertErrorMsg = 'Cannot create new operational event for a team allocation that is not active.';
    String updateErrorMsg = 'Cannot update operational event for a team allocation that is not active.';
    Map<String, Schema.Recordtypeinfo> recTypesByName = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    // Sanchivan's Change Start
    
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
        Set<Id> teamAllocationIds = new Set<Id>();
        Map<Id, Team_Allocation__c> teamAllocationMap = new Map<Id, Team_Allocation__c> ();
        
        for (Operational_Event__c curEvent : trigger.new) {
            teamAllocationIds.add(curEvent.Team_Allocation__c);
        }
        
        for(Team_Allocation__c ta : [SELECT Id, Status__c, Operating_District__c, RecordTypeId FROM Team_Allocation__c WHERE Id in :teamAllocationIds]) {
            teamAllocationMap.put(ta.Id, ta);
        }
        
         for (Operational_Event__c curEvent : trigger.new) {
            Team_Allocation__c ta = teamAllocationMap.get(curEvent.Team_Allocation__c);
            // Don't allow edits to in-active items.
            //allow Maintenance user to edit Shift Operator on Inactive Team Allocation. When Shift Operator is edited, Shift_Resource_Role__c field on Operational Events might change
            // so that Shift_Resource_Role__c field is editable on Inactive TA
            if (ta.Status__c == TeamAllocationUtilities.INACTIVE_STATUS_NAME ) 
            {
                if (ta.RecordTypeId == recTypesByName.get('Maintenance').getRecordTypeId() && trigger.isUpdate)
                {
                    Operational_Event__c beforeUpdate = trigger.oldMap.get(curEvent.Id);
                    if (beforeUpdate.Shift_Resource_Role__c != curEvent.Shift_Resource_Role__c)
                        continue;
                }
                String errorMsg = (trigger.isInsert) ? insertErrorMsg : updateErrorMsg;
                curEvent.addError(errorMsg);
            }
            
            // Make sure operating districts match
            if (curEvent.Operating_District__c == null) {
                curEvent.Operating_District__c = ta.Operating_District__c;
            }
             
            if (curEvent.Operating_District__c != ta.Operating_District__c) {
                curEvent.addError('Operating district must match parent team allocation\'s value.');
            }
        }
    }
    
    // Sanchivan's Change End
    /*
    if (trigger.isBefore && (trigger.isInsert || trigger.isUpdate)) {
        // Don't allow edits to in-active events.
        for (Operational_Event__c curEvent : trigger.new) {
            Team_Allocation__c ta = TeamAllocationUtilities.getTeamAllocation(curEvent.Team_Allocation__c);
            if (ta.Status__c == TeamAllocationUtilities.INACTIVE_STATUS_NAME) {
                String errorMsg = (trigger.isInsert) ? insertErrorMsg : updateErrorMsg;
                curEvent.addError(errorMsg);
            }
            
             // Make sure operating districts match
             if (curEvent.Operating_District__c == null) {
                curEvent.Operating_District__c = ta.Operating_District__c;
             }
             
             if (curEvent.Operating_District__c != ta.Operating_District__c) {
                curEvent.addError('Operating district must match parent team allocation\'s value.');
             }
        }
    }*/
}