trigger SetExternalId on Route__c (Before Insert, Before Update) {
  for (Route__c currRoute : Trigger.new) {
    currRoute.Route_Number__c = currRoute.Name;
  }
}