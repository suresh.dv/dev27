trigger PREP_CopySumOfBUA on PREP_Baseline__c (after update) 
{
	Set<Id> recordId = new Set<Id>();
	system.debug('PREP_CopySumOfBUA');
	for(PREP_Baseline__c BL : Trigger.New)
	{
		PREP_Baseline__c beforeUpdateObj = trigger.oldMap.get(BL.id);
		if(BL.Total_BU_Spend_Baseline_Allocated_Percen__c != beforeUpdateObj.Total_BU_Spend_Baseline_Allocated_Percen__c)
			recordId.add(BL.Id);
	}

	List<PREP_Baseline__c> baselineList = [SELECT Id, Total_BU_Spend_Baseline_Allocated_Percen__c,
											(SELECT Total_BU_Spend_Baseline_Allocated__c, Baseline_Spend_Allocation_Percent__c 
											FROM Initiative_Baselines_3__r)
										FROM PREP_Baseline__c WHERE Id in :recordId];

	List<PREP_Business_Unit_Baseline_Allocation__c> buaUpdate = new List<PREP_Business_Unit_Baseline_Allocation__c>();

	for(PREP_Baseline__c bObj : baselineList)
	{
		for(PREP_Business_Unit_Baseline_Allocation__c BUA : bObj.Initiative_Baselines_3__r)
		{
			BUA.Total_BU_Spend_Baseline_Allocated__c = bObj.Total_BU_Spend_Baseline_Allocated_Percen__c;
			buaUpdate.add(BUA);
		}
	}
	update buaUpdate; 
}