trigger UpdateWorkOrder on Service_Request_Form__c (after update) {
  // List of rigless servicing forms to be updated
  List <Rigless_Servicing_Form__c> updatedServiceForms = 
    new List<Rigless_Servicing_Form__c>();
    
  // List of rigless servicing form record types
  Map<String, Id> riglessRecordTypes = new Map<String, Id>();
  Map<Id, String> riglessRecordTypeIds = new Map<Id, String>();
  for (RecordType riglessRecordType :
       [SELECT Name, Id FROM RecordType
                        WHERE sObjectType = 'Rigless_Servicing_Form__c']) {
    riglessRecordTypes.put(riglessRecordType.Name.trim(), riglessRecordType.Id);
    riglessRecordTypeIds.put(riglessRecordType.Id, riglessRecordType.Name.trim());
  } 
  
  // Create rigless servicing form for each new service request form
  for (Rigless_Servicing_Form__c currForm: 
       [SELECT Id, Name, Service_Request_Form__r.Name, RecordTypeId,
               Service_Request_Form__r.Service_Specifics__c,
               Service_Request_Form__r.Work_Details__c 
               FROM Rigless_Servicing_Form__c
               WHERE Service_Request_Form__c IN :Trigger.newMap.keySet()]) {
    currForm.Name = currForm.Service_Request_Form__r.Name;
    currForm.Work_Details__c = currForm.Service_Request_Form__r.Work_Details__c;
    
    // Update the record type
    String currRecordTypeName = riglessRecordTypeIds.get(currForm.RecordTypeId);
    String recordTypeAppend = '';
    if (currRecordTypeName.contains('In Progress')) {
      recordTypeAppend = ' - In Progress';
    }
    else if (currRecordTypeName.contains('Complete')) {
      recordTypeAppend = ' - Complete';
    }
    currForm.RecordTypeId = 
      riglessRecordTypes.get(currForm.Service_Request_Form__r.Service_Specifics__c +
                             recordTypeAppend); 
    
    updatedServiceForms.add(currForm);  
  }
  update updatedServiceForms;
}