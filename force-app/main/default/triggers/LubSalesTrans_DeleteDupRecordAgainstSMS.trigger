trigger LubSalesTrans_DeleteDupRecordAgainstSMS on Lubricant_Sales_Transaction_By_Quarter__c (after insert) {
    // this trigger cleans up after the before insert trigger by deleting entries marked duplicate entries
    List<Lubricant_Sales_Transaction_By_Quarter__c> duplicateEntries = [Select Id From Lubricant_Sales_Transaction_By_Quarter__c
                                                                        Where Reference_Id__c like 'Duplicate Insert %'];


    if (duplicateEntries.size() > 0)
    {
        delete duplicateEntries;
    }
}