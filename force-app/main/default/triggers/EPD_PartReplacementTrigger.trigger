/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for EPD_Part_Replacement__c SObject
Test Class:     EPD_PartReplacementTriggerTest
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
trigger EPD_PartReplacementTrigger on EPD_Part_Replacement__c (before delete) {

    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            new EPD_DeletionRestrictor().restrictDeletion(Trigger.old);
        }
    }

}