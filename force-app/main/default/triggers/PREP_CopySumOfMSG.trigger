trigger PREP_CopySumOfMSG on PREP_Baseline__c (after update) 
{
	Set<Id> recordId = new Set<Id>();

	for(PREP_Baseline__c BL : Trigger.New)
	{
		recordId.add(BL.Id);
	}

	List<PREP_Baseline__c> baselineList = 
	[SELECT Id, Total_M_S_Group_Baseline_Allocated_Perce__c,
	(SELECT Total_M_S_Spend_Baseline_Allocated__c
	FROM Material_Group_Baselines__r)
	FROM PREP_Baseline__c WHERE Id in :recordId];

	List<PREP_Mat_Serv_Group_Baseline_Allocation__c> msgUpdate = new List<PREP_Mat_Serv_Group_Baseline_Allocation__c>();

	for(PREP_Baseline__c bObj : baselineList)
	{
		PREP_Baseline__c beforeUpdateObj = trigger.oldMap.get(bObj.Id);

		if(bObj.Total_M_S_Group_Baseline_Allocated_Perce__c != beforeUpdateObj.Total_M_S_Group_Baseline_Allocated_Perce__c)
		{
			for(PREP_Mat_Serv_Group_Baseline_Allocation__c MSG : bObj.Material_Group_Baselines__r)
			{
				MSG.Total_M_S_Spend_Baseline_Allocated__c = bObj.Total_M_S_Group_Baseline_Allocated_Perce__c;
				msgUpdate.add(MSG);
			}
		}
		update msgUpdate;
	}
}