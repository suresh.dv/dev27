trigger PREP_MSG_Limit on PREP_Mat_Serv_Group_Baseline_Allocation__c (before insert) {
    
    List< PREP_Mat_Serv_Group_Baseline_Allocation__c > msgRecord = new List< PREP_Mat_Serv_Group_Baseline_Allocation__c >();
    
    RecordType matId = [SELECT Id From RecordType WHERE Name = 'Projects Mat Group Baseline Allocation'];
    RecordType servId = [SELECT Id From RecordType WHERE Name = 'Projects Serv Group Baseline Allocation'];
    
    for(PREP_Mat_Serv_Group_Baseline_Allocation__c MSG : Trigger.New)
    {   
    
        if( MSG.RecordTypeId != matId.Id && MSG.RecordTypeId != servId.Id)
        {
            msgRecord = [SELECT Id, Material_Service_Group__c FROM PREP_Mat_Serv_Group_Baseline_Allocation__c WHERE Baseline_Id__c = :MSG.Baseline_Id__c];
        
            for(Integer i = 0; i < msgRecord.size(); i++)
            {
                if(MSG.Material_Service_Group__c == msgRecord[i].Material_Service_Group__c)
                {
                   MSG.addError('ERROR: You are unable to create a Material / Service Group Baseline Allocation with the same Material / Service Group');
                }
            }
        }
    }
}