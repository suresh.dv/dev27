/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger for EPD_Block_Information__c SObject
Test Class:     EPD_BlockInformationTriggerTest
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
trigger EPD_BlockInformationTrigger on EPD_Block_Information__c (before delete) {

    if(Trigger.isBefore) {
        if(Trigger.isDelete) {
            new EPD_DeletionRestrictor().restrictDeletion(Trigger.old);
        }
    }

}