trigger HOG_Maintenance_Servicing_Vendor_Trigger on HOG_Maintenance_Servicing_Vendor__c (before update, before insert, before delete)
{    
    If (MaintenanceServicingUtilities.executeTriggerCode)
    {
        System.debug('\n*****************************************\n'
            + 'METHOD: HOG_Maintenance_Servicing_Vendor_Trigger()'
            + '\nTrigger: executed'
            + '\n************************************************\n');                  

        if (Trigger.isDelete)
        {    
            for (HOG_Maintenance_Servicing_Vendor__c maintenanceServicingForm : Trigger.old)
            {
                if (maintenanceServicingForm.Maintenance_Servicing_Form__r.ALT_Confirmed__c || maintenanceServicingForm.Maintenance_Servicing_Form__r.TECO__c)
                    maintenanceServicingForm.addError('Work order has already been set to either "ALT Confirmed" or "TECO". You can not delete this record.');
                
                if (maintenanceServicingForm.Activity_Status__c != 'Cancelled')
                    maintenanceServicingForm.addError('Activity Status must be set to "Cancelled" before you can delete this record.');
            }        
        }
        else
        {                                            
            for (HOG_Maintenance_Servicing_Vendor__c maintenanceServicingForm : Trigger.new)
                maintenanceServicingForm.addError('You are not allowed to update or insert record from this page.');            
        }
    }        
}