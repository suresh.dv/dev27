trigger LubSalesTrans_CheckForDupRecordsFromSMS on Lubricant_Sales_Transaction_By_Quarter__c (before insert) 
{
    // this trigger will only need to fire for newly created records for records generated NOT from ERDI or SMS, but from Salesforce.  If External ID is populated, the
    // record is generated from ERDI or SMS, so the following does not need to fire.  If it is blank, it is generated from Salesforce and the code needs to execute.

    // If External ID is blank, it needs to be populated like this:    
    // LUBESALES-2015-Q1-xxxxxxxxxxxxxxx-yyyyyyyyyyyyyyy-Lube-zzzzzzzzzzzzzzz, where
    // xxxxxxxxxxxxxxx is the 15 digit Salesforce ID for Account__c (lookup to Account)
    // yyyyyyyyyyyyyyy is the 15 digit Salesforce ID for Sales_Rep__c (lookup to User)
    // zzzzzzzzzzzzzzz is the 15 digit Salesforce ID for Product_Segment__c (lookup to Lubricant_Product_Segment__c)

    //List<Lubricant_Sales_Transaction_By_Quarter__c> objList = new list<Lubricant_Sales_Transaction_By_Quarter__c>();
    //Lubricant_Sales_Transaction_By_Quarter__c singleObj = new Lubricant_Sales_Transaction_By_Quarter__c();

    //Build up field data for doplicates comparison
    Set<String> quarterAndYear = new Set<String>();
    Set<String> accountIds = new Set<String>();
    Set<String> salesRepIds =  new Set<String>();
    Set<String> productSectors = new Set<String>();
    Set<String> productSegmentIds = new Set<String>();
    //This counter is used to ensure we are not always creating records with the same external ID field, because the field has to be unique.
    Integer counter = 1;

    
    for (Lubricant_Sales_Transaction_By_Quarter__c lubObj : Trigger.New)
    {
        //Parse Quarter String from Latest Transaction Date
        String year = String.valueOf(lubObj.Latest_Transaction_Date__c.year());
        Integer month = lubObj.Latest_Transaction_Date__c.month();
        String quarter =    (month == 1 || month == 2 || month == 3) ? '1' :
                            (month == 4 || month == 5 || month == 6) ? '2' :
                            (month == 7 || month == 8 || month == 9) ? '3' :
                            (month == 10 || month == 11 || month == 12) ? '4' : '4';
        String quarterString = year + '-Q' + quarter;


        quarterAndYear.add(quarterString);
        accountIds.add(lubObj.Account__c);
        salesRepIds.add(lubObj.Sales_Rep__c);
        productSectors.add(lubObj.Product_Sector__c);
        productSegmentIds.add(lubObj.Product_Segment__c);
    }

    //Get All Potential Duplicates
    List<Lubricant_Sales_Transaction_By_Quarter__c> potentialDuplicates = 
    [
        Select Id, Reference_Id__c, Quarter__c, Account__c, Sales_Rep__c, Product_Sector__c, Product_Segment__c,
        Gross_Profit_Goal__c, Volume_Goal_L__c, SOW_Gross_Profit_Goal__c, SOW_Volume_Goal_L__c
        From Lubricant_Sales_Transaction_By_Quarter__c
        Where   Quarter__c In :quarterAndYear And
                Account__c In :accountIds And
                Sales_Rep__c In :salesRepIds And
                Product_Sector__c In :productSectors And
                Product_Segment__c In :productSegmentIds
    ];

    //Go through the potentials and create a list of duplicates and notDuplicates
    Boolean isDuplicate = false;
    List<Lubricant_Sales_Transaction_By_Quarter__c> definiteDuplicates = new List<Lubricant_Sales_Transaction_By_Quarter__c>();
    List<Lubricant_Sales_Transaction_By_Quarter__c> notDuplicates = new List<Lubricant_Sales_Transaction_By_Quarter__c>();
    //List<Lubricant_Sales_Transaction_By_Quarter__c> duplicateInserts = new List<Lubricant_Sales_Transaction_By_Quarter__c>();
    for(Lubricant_Sales_Transaction_By_Quarter__c lubObj : Trigger.New) {
        isDuplicate = false;
        for (Lubricant_Sales_Transaction_By_Quarter__c potentialDuplicate : potentialDuplicates) {
            if( lubObj.Quarter__c == potentialDuplicate.Quarter__c &&
                lubObj.Account__c == potentialDuplicate.Account__c &&
                lubObj.Sales_Rep__c == potentialDuplicate.Sales_Rep__c &&
                lubObj.Product_Sector__c == potentialDuplicate.Product_Sector__c &&
                lubObj.Product_Segment__c == potentialDuplicate.Product_Segment__c) {

                //Update fields that need to updated
                potentialDuplicate.Gross_Profit_Goal__c = lubObj.Gross_Profit_Goal__c;
                potentialDuplicate.Volume_Goal_L__c = lubObj.Volume_Goal_L__c;
                potentialDuplicate.SOW_Gross_Profit_Goal__c = lubObj.SOW_Gross_Profit_Goal__c;
                potentialDuplicate.SOW_Volume_Goal_L__c = lubObj.SOW_Volume_Goal_L__c;

                //Set duplicate flag and mark record for deletion
                //duplicateInserts.add(lubObj);
                lubObj.Reference_Id__c = 'Duplicate Insert ' + counter;
                counter++;
                definiteDuplicates.add(potentialDuplicate);
                isDuplicate = true;
            }
        }
        if(!isDuplicate) notDuplicates.add(lubObj);
    }

    //Give New Ids to NotDuplicates
    for(Lubricant_Sales_Transaction_By_Quarter__c notDuplicate : notDuplicates) {
        if(notDuplicate.Reference_Id__c == null) {
            notDuplicate.Reference_Id__c = 'LUBESALES-' + notDuplicate.Year__c + '-' + notDuplicate.Quarter_1_to_4__c + '-' + notDuplicate.Account__c + '-' + notDuplicate.Sales_Rep__c + '-Lube-' + notDuplicate.Product_Segment__c;
        }
    }

    //DML
    if(definiteDuplicates.size() > 0) {
        /*for(Lubricant_Sales_Transaction_By_Quarter__c lub : definiteDuplicates) {
            System.debug(   'Duplicate Name: ' + lub.Reference_Id__c + 
                            ' Gross_Profit_Goal__c: ' + lub.Gross_Profit_Goal__c + 
                            ' Volume_Goal_L__c: ' + lub.Volume_Goal_L__c);
        }*/
        Database.update(definiteDuplicates, false);
        //update definiteDuplicates;
    }

}