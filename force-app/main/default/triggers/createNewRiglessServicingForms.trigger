// ============================================================================
// Create new Rigless Servicing Form when a new service request form is 
// created
trigger createNewRiglessServicingForms 
         on Service_Request_Form__c (after insert) {
  // List of new rigless servicing forms
  List <Rigless_Servicing_Form__c> newServiceForms = 
    new List<Rigless_Servicing_Form__c>();
  
  // Queue that will be the owner of the servicing form
  QueueSobject riglessQueue = 
    [Select q.queueId From QueueSobject q 
                      WHERE q.Queue.Name = 'Rigless Service Hopper'];
  
  // List of rigless servicing form record types
  Map<String, Id> riglessRecordTypes = new Map<String, Id>();
  for (RecordType riglessRecordType : 
       [SELECT Name, Id FROM RecordType 
                        WHERE SobjectType = 'Rigless_Servicing_Form__c']) {
    riglessRecordTypes.put(riglessRecordType.Name.trim(), riglessRecordType.Id);
  }
     
  // Create rigless servicing form for each new service request form
  for (Service_Request_Form__c currForm: Trigger.new) {
    String currRecordType;
    if (currForm.Service_Specifics__c.equals('Rod Side Entry')) {
      currRecordType = 
        riglessRecordTypes.get('Rod Side Entry');  
    }
    else if (currForm.Service_Specifics__c.equals('PTS Clean Out')) {
      currRecordType = 
        riglessRecordTypes.get('PTS Cleanout');
    }
    else if (currForm.Service_Specifics__c.endsWith('Rigless Fishing')) {
      currRecordType = 
        riglessRecordTypes.get('Rigless Fishing');
    }
    else {
      currRecordType = 
        riglessRecordTypes.get('Insert Pump Change');
    }

    // Get Route record
    List<Route__c> route = 
        [Select Route__c.Id, Route__c.Operator_1__c, Route__c.Operator_2__c, Route__c.Operator_3__c, Route__c.Operator_4__c From Route__c Where Route__c.Route_Number__c =: currForm.Route_formula__c.substringBeforeLast('_HL__').substringAfterLast('_HL_') Limit 1];

    // Get Operating Field's production engineer
    List<Field__c> field = 
        [Select Field__c.Production_Engineer__c From Field__c Where Field__c.Name =: currForm.Operating_Field_AMU__c.substringBeforeLast('_HL__').substringAfterLast('_HL_') Limit 1];
            
    Rigless_Servicing_Form__c newServiceForm = 
      new Rigless_Servicing_Form__c(RecordTypeId = currRecordType,
                                    Name = currForm.Name,
                                    Work_Details__c = currForm.Work_Details__c,
                                    OwnerId = riglessQueue.queueId,
                                    Service_Status__c = 'New Request',
                                    Location__c = currForm.Location__c,
                                    Service_Request_Form__c = currForm.Id,
                                    Operator_Route__c = route.size() > 0 ? route[0].Id : null,
                                    Operator_1__c = route.size() > 0 ? route[0].Operator_1__c : null,
                                    Operator_2__c = route.size() > 0 ? route[0].Operator_2__c : null,
                                    Operator_3__c = route.size() > 0 ? route[0].Operator_3__c : null,
                                    Operator_4__c = route.size() > 0 ? route[0].Operator_4__c : null,
                                    Production_Engineer__c = field.size() > 0 ? field[0].Production_Engineer__c : null);
                                                   
    newServiceForms.add(newServiceForm);    
  }
  System.debug(newServiceForms);
  insert newServiceForms;
}