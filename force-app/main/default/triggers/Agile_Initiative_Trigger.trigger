trigger Agile_Initiative_Trigger on AA_Initiative__c (after insert, after update) {
	
	if (Trigger.isInsert) {                      
		if (Trigger.isAfter){
        	AA_Utilities.TriggerAfterUpdateAgileInitiative(Trigger.new);
        }
    } 

}