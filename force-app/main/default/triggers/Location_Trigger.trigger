/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Trigger Handling After Update actions on Location.
Test Class:     TestNewHOGTriggers
History:        jschn 19. 9. 2018 - Created.
**************************************************************************************************/
trigger Location_Trigger on Location__c (after update) {

    if(HOG_TriggerRecursionPreventController.flag) {
        HOG_TriggerRecursionPreventController.flag = false;

        Location_TriggerHelper handler = new Location_TriggerHelper(Trigger.new, Trigger.oldMap);
        if (Trigger.isAfter) {
            if (Trigger.isUpdate) {
                handler.handleAfterUpdate();
            }
        }

    }
}