trigger SEP_PopulateFieldsInCommonTask on Common_Task__c (after insert, after update) 
{

    String RTName = Schema.SObjectType.Common_Task__c.getRecordTypeInfosByName().get('SEP - Project Services').getName();
   
    //check if this trigger is already executed, if yes then stop execution
    //check if the common task record type is SEP - Project Services, if not then stop execution
    if(SEP_TriggerUtility.executedTriggers.contains('SEP_PopulateFieldsInCommonTask') || RTName !='SEP - Project Services')
    {
        return;
    }
    else
    {
    	SEP_TriggerUtility.executedTriggers.add('SEP_PopulateFieldsInCommonTask');
        System.debug('\n'+'Trigger: SEP_PopulateFieldsInCommonTask - Executing...');
        if(Trigger.isInsert)
        {
            SEP_TriggerUtility.triggerAfterInsertUpdateCommonTaskFields(Trigger.newMap.keySet(), 'insert');
		}
        if(Trigger.isUpdate)
        {
			SEP_TriggerUtility.triggerAfterInsertUpdateCommonTaskFields(Trigger.newMap.keySet(), 'update');
        }
        System.debug('\n Trigger: SEP_PopulateFieldsInCommonTask - Completed');
    }
}