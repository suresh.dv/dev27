//this will update Role of Shift Resource on All Operational Event when user edit Shift Resource of selected Team Allocation
trigger ShiftOperator_UpdateShiftResourceRoleOfOE on Shift_Operator__c (after delete, after insert, after update, after undelete) 
{
    Set<Id> taIds = new Set<Id>();
    
    List<Shift_Operator__c> triggerValues = new List<Shift_Operator__c>();
    if (trigger.isInsert || trigger.isUnDelete || trigger.isUpdate)
    {
        triggerValues = trigger.new;
    }
    else if (trigger.isDelete)
    {
        triggerValues = trigger.old;
    }
    for(Shift_Operator__c operator : triggerValues)
    {
        taIds.add(operator.Team_Allocation__c);
       
    }
    List<Shift_Operator__c> allResourceList = [select Id, Role__r.Name, Team_Allocation__c, Operator_Contact__c 
                                            from Shift_Operator__c where Team_Allocation__c in: taIds];        
   
    List<Operational_Event__c> eventList = [select Id, Operator_Contact__c, Team_Allocation__c from Operational_Event__c 
                                            where Team_Allocation__c in: taIds ];
    List<Operational_Event__c> updateList = new List<Operational_Event__c>();
    for(Operational_Event__c event : eventList)
    {
        event.Shift_Resource_Role__c = '';
        for(Shift_Operator__c operator : allResourceList)
        {
            if (event.Team_Allocation__c == operator.Team_Allocation__c && event.Operator_Contact__c == operator.Operator_Contact__c)
            {
              event.Shift_Resource_Role__c = operator.Role__r.Name;
              updateList.add(event);
              break;
              
            }
        }
    }
    update updateList;
}