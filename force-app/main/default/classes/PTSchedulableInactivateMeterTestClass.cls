@isTest(SeeAllData=true)
private class PTSchedulableInactivateMeterTestClass{
    
    public static String CRON_TIME = '0 1 * * * ?';
    
    static testmethod void test(){
        PTTestData.createPTTestRecords();
        PTTestData.TestUtilityMeterInactivation('Test1');
        PTTestData.TestUtilityMeterInactivation('Test2');
        Test.startTest();
    
        String jobId = System.schedule('testScheduledApexFromTestMethod',CRON_TIME, new PTSchedulableInactivateMeter());
    	
        CronTrigger ct = [SELECT id, CronExpression,TimesTriggered,NextFireTime FROM CronTrigger WHERE id=:jobId];
        
        System.assertEquals(CRON_TIME,ct.CronExpression);
        
        System.assertEquals(0,ct.TimesTriggered);
        
        Test.stopTest();
    }
    
}