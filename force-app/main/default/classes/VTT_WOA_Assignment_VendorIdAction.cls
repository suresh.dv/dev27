/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Enum that is used to determine what should happen to Vendor ID during assign/un-assign
                action.
                Currently there are 2 supported modes:
                    - CLEAR - If this value is provided, Vendor Id on Activity will be cleared
                    - PRESERVE -  If this value is provided, Vendor Id on Activity will be preserved
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public enum VTT_WOA_Assignment_VendorIdAction {
    CLEAR,
    PRESERVE
}