/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Service class containing Business Logic for Service Request Notifications
Test Class:     SNR_ServiceTest, SNR_EndpointTest
History:        mbrimus 2020-01-08 - Created.
*************************************************************************************************/
public inherited sharing class SNR_Service {
    private static final String CLASS_NAME = String.valueOf(SNR_Service.class);

    /**
    *
    * Loads initial picklist values and state model
    *
    * @param recordId      - Floc ID (Well Location, Facility, Equipment, Well Event, System, SubSystem, Yard)
    * @param objectName    - API Name of object where SNR is being created
    *
    * @return              - SNR_Response
    **/
    public SNR_Response loadInitialValues(String recordId, String objectName) {
        SNR_Response response = new SNR_Response();

        // Validated passed in data
        SNR_Utilities.LightningNotificationRecordType notificationRecordType =
                new SNR_Utilities.LightningNotificationRecordType();
        if (!notificationRecordType.OBJECTNAMETORECORDTYPEMAP.containsKey(objectName)) {
            return new SNR_Response(
                    'Invalid request - record type is unsupported'
            );
        }

        List<AggregateResult> serviceCategories = SNR_DAOProvider.snrDao.getServiceCategories();
        List<AggregateResult> serviceTypes = new List<AggregateResult>();
        List<AggregateResult> serviceActivityNames = new List<AggregateResult>();
        List<AggregateResult> serviceRequired = new List<AggregateResult>();
        List<HOG_Notification_Type_Priority__c> servicePriorities = new List<HOG_Notification_Type_Priority__c>();

        response.addServiceCategories(serviceCategories);

        // For community users there should be just one category available so we can get serviceTypes for them immediately
        // As this picklist will not be visible for them
        if (serviceCategories.size() > 0) {
            String notificationTypeId = (String) serviceCategories.get(0).get('Id');
            serviceTypes = SNR_DAOProvider.snrDao.getServiceTypesNoServiceRigProgram(notificationTypeId, true);
            response.addServiceType(serviceTypes);
        }

        // Preset service activity names
        if (serviceTypes.size() > 0 && serviceCategories.size() > 0) {
            // Preselect other picklists if these have only one option
            String notificationTypeId = (String) serviceCategories.get(0).get('Id');
            String serviceTypeId = (String) serviceTypes.get(0).get('Id');

            serviceActivityNames = SNR_DAOProvider.snrDao.getServiceActivityNames(notificationTypeId, serviceTypeId);
            response.addServiceActivities(serviceActivityNames);

            servicePriorities = SNR_DAOProvider.snrDao.getServicePriorities(serviceTypeId);
            response.addServicePriorities(servicePriorities);

            // Add model data
            SNR_FLOCModel dataFromFLOC;
            List<HOG_Notification_Type__c> selectedNotificationType = SNR_DAOProvider.snrDao.getNotificationTypeById(serviceTypeId);
            if (selectedNotificationType.size() > 0) {
                dataFromFLOC = new SNR_Utilities().getModel(recordId, objectName, selectedNotificationType.get(0));
                response.addFLOCData(dataFromFLOC);
            }

            if (serviceActivityNames.size() > 0) {
                String serviceActivityId = (String) serviceActivityNames.get(0).get('Id');
                serviceRequired = SNR_DAOProvider.snrDao.getServiceRequired(
                        notificationTypeId,
                        serviceTypeId,
                        serviceActivityId,
                        dataFromFLOC.wellLocationStatus == 'Producing');
                response.addServiceRequired(serviceRequired);
            }
        }

        return response;
    }

    public SNR_Response loadServiceSpecifics(
            String serviceCategoryId,
            String serviceTypeId,
            String serviceActivityId,
            String serviceRequiredId,
            String recordId,
            String objectName) {

        SNR_Response response = new SNR_Response();
        List<HOG_Notification_Type__c> selectedNotificationType = SNR_DAOProvider.snrDao.getNotificationTypeById(serviceTypeId);
        List<HOG_Service_Required__c> serviceRequired = SNR_DAOProvider.snrDao.getOneServiceRequired(serviceRequiredId);
        SNR_FLOCModel dataFromFLOC;

        if (selectedNotificationType.size() > 0 && serviceRequired.size() > 0) {
            dataFromFLOC = new SNR_Utilities().getModelWithServiceRequired(
                    recordId, objectName, serviceRequired.get(0), selectedNotificationType.get(0));
        }
        List<AggregateResult> serviceSpecifics = SNR_DAOProvider.snrDao.getServiceSpecifics(
                serviceCategoryId,
                serviceTypeId,
                serviceActivityId,
                serviceRequiredId,
                dataFromFLOC.wellLocationStatus == 'Producing');
        response.addServiceSpecifics(serviceSpecifics);
        response.addFLOCData(dataFromFLOC);

        return response;
    }

}