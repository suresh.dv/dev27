@isTest
public class HuskyExceptionManagerTest {

    @isTest
    static void testLogWithSeverity() {
        Test.startTest();
        HuskyExceptionManager.log('TestInterface', 'Test Exception Message', 'INFO');
        Test.stopTest();

        List<Error_Log__c> errors = [SELECT Id, Apex_Class_Trigger__c, Severity__c, Description__c FROM Error_Log__c];
        System.assertEquals(1, errors.size());
        System.assertEquals('TestInterface', errors[0].Apex_Class_Trigger__c);
        System.assertEquals('INFO', errors[0].Severity__c);
        System.assertEquals('Test Exception Message', errors[0].Description__c);

    }

    @isTest
    static void testLogErrorWithSingleMessage() {
        Test.startTest();
        HuskyExceptionManager.log('TestInterface', 'Test Exception Message');
        Test.stopTest();

        List<Error_Log__c> errors = [SELECT Id, Apex_Class_Trigger__c, Severity__c, Description__c FROM Error_Log__c];
        System.assertEquals(1, errors.size());
        System.assertEquals('TestInterface', errors[0].Apex_Class_Trigger__c);
        System.assertEquals('ERROR', errors[0].Severity__c);
        System.assertEquals('Test Exception Message', errors[0].Description__c);

    }

    @isTest
    static void testLogErrorWithMultipleMessages() {
        List<String> exceptions = new List<String>();

        for(Integer i = 0; i < 100; i++) {
            exceptions.add('Test Exception Message' + i);
        }

        Test.startTest();
        HuskyExceptionManager.log('TestInterface', exceptions);
        Test.stopTest();

        List<Error_Log__c> errors = [SELECT Id, Apex_Class_Trigger__c, Severity__c, Description__c FROM Error_Log__c];
        System.assertEquals(100, errors.size());

    }    

    @isTest
    static void testLogErrorWithSingleExceptionMessage() {
        Test.startTest();
        HuskyExceptionManager.log('TestInterface', new HuskyTestException('Test Exception Message'));
        Test.stopTest();

        List<Error_Log__c> errors = [SELECT Id, Apex_Class_Trigger__c, Severity__c, Description__c FROM Error_Log__c];
        System.assertEquals(1, errors.size());
        System.assertEquals('TestInterface', errors[0].Apex_Class_Trigger__c);
        System.assertEquals('ERROR', errors[0].Severity__c);
        System.assertEquals('Test Exception Message', errors[0].Description__c);

    }

    @isTest
    static void testLogErrorWithMultipleExceptionMessages() {
        List<HuskyTestException> exceptions = new List<HuskyTestException>();

        for(Integer i = 0; i < 100; i++) {
            exceptions.add(new HuskyTestException('Test Exception Message' + i));
        }

        Test.startTest();
        HuskyExceptionManager.log('TestInterface', exceptions);
        Test.stopTest();

        List<Error_Log__c> errors = [SELECT Id, Apex_Class_Trigger__c, Severity__c, Description__c FROM Error_Log__c];
        System.assertEquals(100, errors.size());

    }     

    public class HuskyTestException extends Exception {}    

}