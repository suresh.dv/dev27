public class ATSQuoteTemplateController{
    public ATSWrapperUtilities.ATSQuoteWrapper quote{get; set;}
    
    public Id tenderId {get; set;}
    public Id accountId {get; set;}

    List<String> opportunityList;
    
    List<ATSWrapperUtilities.ATSQuoteWrapper> quoteWrapperList{get; set;}
    public ATSQuoteTemplateController() {
        
        tenderId = ApexPages.currentPage().getParameters().get('tenderId');
        accountId = ApexPages.currentPage().getParameters().get('accountId');
        opportunityList =  ApexPages.currentPage().getParameters().get('opportunityList').split(',');
        initializeQuoteWrapper();   
    }
    
    public void initializeQuoteWrapper() {
        
        quote = new ATSWrapperUtilities.ATSQuoteWrapper();
        
        ATS_Parent_Opportunity__c tender = [SELECT Id, Name,Tender__c, Destination_City_Province__c, Price_Valid_From__c, Price_Valid_To__c, Bid_Due_Date_Time__c, Bid_Description__c, Acceptance_Deadline__c, Marketer__r.Name, Marketer__c, 
                                            Marketer__r.Id
                                            FROM ATS_Parent_Opportunity__c WHERE Id =:tenderId];
        
        ATS_Tender_Customers__c customer = [SELECT Id, Account__c, Account__r.Name, ATS_Parent_Opportunity__c, Contact__c, Contact__r.Name, Contact__r.Fax, Comments__c, Contact__r.Email
                                            FROM ATS_Tender_Customers__c WHERE Account__c =:accountId AND ATS_Parent_Opportunity__c =:tenderId][0];

        System.Debug('**InitializeQuoteWrapper()');        
        
        User salesRepresentative = [SELECT Email, Fax, Name FROM User where User.Id= :tender.Marketer__r.Id];                                            
        
        System.Debug('**Created SalesRep()');
        quote.tender = tender;
        quote.accountId = this.accountId;
        quote.accountName = customer.Account__r.Name;

        quote.acceptanceDeadline = tender.Acceptance_Deadline__c;             
        quote.offerEndDate = tender.Bid_Due_Date_Time__c;
        //need to do this to include proper time zone into the output
        quote.offerEndDateString = tender.Bid_Due_Date_Time__c.format('h:mm a (z) MM/dd/yyyy ');
        
       
        
        quote.contactId = customer.Contact__c;
        quote.contactName = customer.Contact__r.Name;
        quote.contactFaxNumber = customer.Contact__r.Fax;
        quote.contactComments = customer.Comments__c;
        quote.contactEmailAddress = customer.Contact__r.Email;
        
        quote.tenderId = this.tenderId;
        quote.tenderNumber = tender.Tender__c;
        quote.tenderName = tender.Name;
        
        quote.priceValid = tender.Price_Valid_From__c.format() + ' - ' + tender.Price_Valid_To__c.format(); 
        quote.project = tender.Bid_Description__c;
        quote.destination = tender.Destination_City_Province__c;
//        quote.additionalComments = tender.Bid_Description__c; //need to change this value to customer specific comments (from junction)
        quote.signatureName = tender.Marketer__r.Name;
        quote.marketerFax = salesRepresentative.Fax;
        quote.marketerEmail = salesRepresentative.Email;
        


        //we suppose to have signatures of marketors as static resource with name ATSQuoteTemplateSignatures
        //in format of 'Firstname_Lastname.gif'
        String baseStaticResourceURL = GetResourceURL('ATSQuoteTemplateSignatures');
        if( baseStaticResourceURL.length() > 0 && quote.signatureName != null )
        {
            baseStaticResourceURL+= '/' + quote.signatureName.replace(' ' ,'_')  + '.gif';
            quote.signatureImageURL  = baseStaticResourceURL;
        }                      
                        
        quote.setOpportunities([SELECT Id, 
                                Opportunity_ATS_Product_Category__c, 
                                Product_Category_Comments__c,
                                RecordType.Name,
                                (SELECT Id, 
                                    PricebookEntry.Name, 
                                    PricebookEntry.Id, 
                                    Price_Type__c, 
                                    UnitPrice, 
                                    Unit__c, 
                                    Quantity, 
                                    TotalPrice, 
                                    PricebookEntryId, 
                                    OpportunityId, 
                                    ListPrice,
                                    Description, 
                                    Anti_Strip__c, 
                                    Additive__c, 
                                    Currency__c,
                                    Sales_Price__c,
                                    Axle_5_Price__c,
                                    Axle_6_Price__c,
                                    Axle_7_Price__c,
                                    Axle_8_Price__c,
                                    Sales_Price_4_decimals__c,
                                    Sales_Price_Formula__c,
                                    Axle_5_Price_Formula__c,
                                    Axle_6_Price_Formula__c,
                                    Axle_7_Price_Formula__c,
                                    Axle_8_Price_Formula__c,
                                    Include_Sales_Price__c,
                                    Include_Axle_5__c,
                                    Include_Axle_6__c,
                                    Include_Axle_7__c,
                                    Include_Axle_8__c,
                                    PricebookEntry.Product2.Density__c,
                                    PricebookEntry.Product2.Product_AKA_SMS_Name__c
                                    FROM OpportunityLineItems
                                    Order By PricebookEntry.Name),
                                (Select Id, Name,
                                    RecordType.Name,
                                    Prices_F_O_B__c,
                                    Husky_Supplier_1_Selected__c,
                                    Husky_Supplier_2_Selected__c,
                                    Husky_Supplier_1__c,
                                    Husky_Supplier_2__c,
                                    Supplier_1_Min_Load__c,
                                    Supplier_2_Min_Load__c,
                                    Supplier_1_Unit__c,
                                    Supplier_2_Unit__c,
                                    Emulsion_Min_Load5_Supplier1__c,
                                    Emulsion_Min_Load5_Supplier2__c,
                                    Emulsion_Min_Load6_Supplier1__c,    
                                    Emulsion_Min_Load6_Supplier2__c,
                                    Emulsion_Min_Load8_Supplier1__c,
                                    Emulsion_Min_Load8_Supplier2__c,
                                    Supplier_1_Rate__c,
                                    Supplier_2_Rate__c,
                                    Emulsion_Rate5_Supplier1__c,
                                    Emulsion_Rate5_Supplier2__c,
                                    Emulsion_Rate6_Supplier_1__c,
                                    Emulsion_Rate6_Supplier2__c,
                                    Emulsion_Rate7_Supplier1__c,
                                    Emulsion_Rate7_Supplier2__c,
                                    Emulsion_Rate8_Supplier1__c,
                                    Emulsion_Rate8_Supplier2__c,
                                    Emulsion_Rate7_Competitor1__c,
                                    Emulsion_Rate7_Competitor2__c,
                                    Emulsion_Rate7_Competitor3__c,
                                    Emulsion_Rate7_Competitor4__c,
                                    Emulsion_Min_Load7_Supplier1__c,
                                    Emulsion_Min_Load7_Supplier2__c
                                    FROM Freight_Ats__r) 
                                FROM Opportunity WHERE id in :opportunityList], null);                                             
    }
    
    private static String GetResourceURL(String resourceName) {
    
    List<StaticResource> resourceList = [
       SELECT Name, NamespacePrefix, SystemModStamp 
       FROM StaticResource 
       WHERE Name = :resourceName
    ];
                        
    if (resourceList.size() == 1) {
       String namespace = resourceList[0].NamespacePrefix;
       return '/resource/' 
          + resourceList[0].SystemModStamp.getTime() + '/' 
          + (namespace != null && namespace != '' ? namespace + '__' : '') 
          + resourceName; 
    } else return '';
    
    }    
    
    
}