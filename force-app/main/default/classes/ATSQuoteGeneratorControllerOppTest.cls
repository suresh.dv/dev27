/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = True)
private class ATSQuoteGeneratorControllerOppTest {

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            
            PageReference pageRef = Page.ATSQuoteGeneratorV2Opp;
            Test.setCurrentPageReference(pageRef);
            
            // Create a Test Account with the name Asphalt
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
            
            insert account;
            System.AssertNotEquals(account.Id, Null);
        
            Contact cont = ContactTestData.createContact(account.Id, 'FName', 'LName');
            insert cont; 
            
            //ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];
			Opportunity  tender =  ATSParentOpportunityTestData.createTender(account.Id);
			insert tender;
            
            //ATS_Tender_Customers__c customer = ATSTestData.createTenderCustomer(account.Id, tender.id, cont.id);
            Cpm_Customer_Opportunity__c customer = ATSTestData.createOpportunityCustomer(account.Id, tender.id, cont.id);
            customer.cpm_Won__c = true;
            insert customer; 
            
            // Create Products        
            List<Product2> productList = ProductTestData.createProducts(101);
            insert productList;
            
            // Create Price Book Entries
            /*List<PriceBookEntry> priceBookEntryList = PriceBookEntryTestData.createPriceBookEntry(productList);
            insert priceBookEntryList;*/
            
            Map<String, Id> recordTypeMap = new Map<String, Id>();
    
            for(RecordType recType : [SELECT Id, DeveloperName FROM RecordType 
                                      WHERE DeveloperName in ('ATS_Asphalt_Product_Category', 'ATS_Emulsion_Product_Category', 'ATS_Residual_Product_Category', 'Asphalt') AND 
                                      SObjectType = 'Opportunity']) {
                recordTypeMap.put(recType.DeveloperName, recType.Id);            
            }
            
            List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
            
            Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
            Opportunity opp = new Opportunity();//OpportunityTestData.createOpportunity(recordTypeMap.get('Asphalt'));
            //opp.Opportunity_ATS_Product_Category__c = tender.Id;
            opp.Name = 'Test Opportunity';
            opp.AccountId = account.Id;
            opp.CloseDate = Date.today();
            opp.StageName = 'Won'; 
            opp.Confirmation_Method__c = 'PO';
            opp.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            opp.PO__c = 'PO Test';
            opp.recordTypeId = recordTypeId;
            insert opp;
            opp.Pricebook2Id = standardPriceBook[0].Id;
            opp.priceBook2 = standardPriceBook[0];
            update opp;
            System.debug('opp tom: ' +opp);
            
            List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: standardPriceBook[0].Id];
            priceBookEntryList[0].IsActive = true;
            update priceBookEntryList[0];
            OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
            insert oli;
            
            Opportunity theOpp = [select Id, Pricebook2Id, PriceBook2.Name, RecordTypeId from Opportunity where Id = :opp.Id limit 1];
            System.assertEquals(theOpp.Pricebook2Id, standardPriceBook[0].Id);
            System.assertNotEquals(theOpp.Pricebook2, Null);
            System.assertNotEquals(theOpp.PriceBook2.Name, Null);

            //supplier
            cpm_Supplier__c sup1 = new cpm_Supplier__c();
            sup1.name = 'a9s290000008OKYAA2';
            insert sup1;
            
            cpm_Supplier__c sup2 = new cpm_Supplier__c();
            sup2.name = 'a9s290000008OJlAAM';
            insert sup2;
            
            cpm_Supplier__c sup3 = new cpm_Supplier__c();
            sup3.name = 'a9s290000008OJ5AAM';
            insert sup3;
            
            System.assertNotEquals(null, sup1);
            System.assertNotEquals(null, sup2);
            System.assertNotEquals(null, sup3);
            
            //freight
            ATS_Freight__c  freight = new ATS_Freight__c();
            freight.cpm_HuskySupplier1_ats_pricing__c = sup1.Id;
            freight.cpm_HuskySupplier2_ats_pricing__c = sup2.Id;
            freight.cpm_HuskySupplier3_ats_pricing__c = sup3.Id;
            freight.Husky_Supplier_1__c= 'Husky_Supplier_1';
            freight.Husky_Supplier_2__c= 'Husky_Supplier_2';          
            freight.Competitor_Supplier_1__c= 'Competitor_Supplier_1';
            freight.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
            freight.ATS_Freight__c  =   opp.Id;
            freight.Husky_Supplier_1_Selected__c = true;
            freight.Prices_F_O_B__c = 'Origin + Freight';                       
            insert freight;

            //create standard controller
            ApexPages.StandardController sc = new ApexPages.standardController(tender);
            
            //Create Controller
            ATSQuoteGeneratorControllerOpp controller = new ATSQuoteGeneratorControllerOpp(sc);
            System.AssertNotEquals(controller.getCustomers().size(), 0);
            System.AssertNotEquals(controller.getSelectedCustomers().size(), 0);                         

            //I'll need to create opportunity item first - so for now I just want to get code coverage
//-->
            System.assertEquals(controller.getCategoryItems().size(), 3); 
            
            // Set Categories
            String[] dataSetCategories = new String[3];
            dataSetCategories[0] = 'Residual';
            dataSetCategories[1] = 'Emulsion';
            datasetCategories[2] = 'Asphalt';
           
            controller.setSelectedCategories(dataSetCategories);
			
            System.AssertEquals(3,controller.getSelectedCategories().size());
            
            PageReference pageBackRef = controller.BackMethod();
            System.AssertEquals('/'+tender.id,pageBackRef.getUrl());
            
            controller.GenerateQuotes();
            List<Attachment> attachments = [select id from Attachment where ParentId =: tender.id]; 
            
            for(ApexPages.Message msg : ApexPages.getMessages())
            {
                system.debug('messages: ' + msg);                    
            }
            
            System.AssertNotEquals(0, attachments.size());
        }
    }
}