/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/25/2019   
 */

public inherited sharing class ELG_Task_QuerySelector implements ELG_Task_DAO {

	public List<ELG_Task__c> getActiveTasks(String postId) {
		return [
				SELECT Name,
						Post__r.Post_Name__c,
						CreatedBy.Name,
						CreatedDate,
						Short_Description__c,
						Task_Description__c
				FROM ELG_Task__c
				WHERE Post__c = :postId AND Status__c = :ELG_Constants.TASK_ACTIVE
				ORDER BY CreatedDate DESC
		];
	}

	public List<ELG_Post__c> getAMUPosts(String amuId) {
		return [
				SELECT Id,
						Post_Name__c, (
						SELECT Id
						FROM Shift_Assignements__r
						ORDER BY CreatedDate DESC
						LIMIT 1
				)
				FROM ELG_Post__c
				WHERE Operating_Field_AMU__c = :amuId
				ORDER BY Post_Name__c ASC
		];
	}

	public ELG_Task__c getTask(String taskId) {
		return [
				SELECT Id,
						Name,
						Post__r.Post_Name__c,
						Short_Description__c,
						Task_Description__c,
						CreatedBy.Name,
						CreatedDate,
						Finished_By__r.Name,
						Finished_At__c,
						Status__c,
						Comment__c,
						Reason__c,
						Post_Name__c
				FROM ELG_Task__c
				WHERE Id = :taskId
		];
	}

	public ELG_Shift_Assignement__c getActiveShift(String postId) {
		return [
				SELECT
						Id
				FROM ELG_Shift_Assignement__c
				WHERE Post__c = :postId
				AND Shift_Status__c = :ELG_Constants.SHIFT_NEW
		];
	}
}