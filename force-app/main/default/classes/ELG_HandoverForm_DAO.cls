/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:
 *  Test Class:		ELG_HandoverFrom_EndpointTest
 *  History:        Created on 7/11/2019   
 */

public interface ELG_HandoverForm_DAO {


	/**
	 * Returns Handover form
	 * @param handoverFormId
	 * @return  just one handover
	 */
	ELG_Shift_Handover__c handoverForm(String handoverFormId);

	/**
	 * Returns Shift Assignement
	 * @param handoverForm
	 * @return  one Shift Assignement that was created on handover pass
	 */
	ELG_Shift_Assignement__c getNextShift(ELG_Shift_Handover__c handoverForm);

	/**
	 * Returns list of categories and inner list of questions
	 *
	 * To build up sections and questions
	 * @param categoryName
	 * @return  List of Categories and inner List of Category Questions
	 */
	List<ELG_Handover_Category__c> getQuestionsBasedOnCategory(String categoryName);

	/**
	 * Returns log entries grouped by Category order, Question order and Created Date
	 * @param shiftAssignementId
	 * @return  List of Log Entries
	 */
	List<ELG_Log_Entry__c> getLogEntriesByCategories(String shiftAssignementId);

	/**
	 * Returns List of Log Entries for Selected Shift that are in Generic record type
	 *
	 * @param shiftAssignmentId
	 * @param recordTypeId
	 *
	 * @return List of Log Entries
	 */
	List<ELG_Log_Entry__c> getGenericLogEntries(String shiftAssignmentId, String recordTypeId);

	/**
	 * Return Permission set of User based on Permission set name
	 *
	 * @param permissionSetName
	 * @param userId
	 *
	 * @return Permission set
	 */
	List<PermissionSetAssignment> getUserPermissions(String permissionSetName, String userId);

}