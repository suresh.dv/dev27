/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for SNR_Service
History:        mbrimus 2020-01-28 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class SNR_ServiceTest {

    @IsTest
    static void loadInitialValues_withProperObjectNameParam() {
        SNR_Response result;

        Test.startTest();
        SNR_DAOProvider.snrDao = new SNRSelectorMock();
        result = new SNR_Service().loadInitialValues('id', 'Location__c');
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(true, result.success);
    }

    @IsTest
    static void loadInitialValues_withInvalidObjectNameParam() {
        SNR_Response result;

        Test.startTest();
        SNR_DAOProvider.snrDao = new SNRSelectorMock();
        result = new SNR_Service().loadInitialValues('id', 'Invalid');
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(false, result.success);
        System.assertEquals(1, result.errors.size());
    }

    @IsTest
    static void loadServiceSpecifics_withEmptyParameters() {
        SNR_Response result;

        Test.startTest();
        SNR_DAOProvider.snrDao = new SNRSelectorMock();
        result = new SNR_Service().loadServiceSpecifics(
                null,
                null,
                null,
                null,
                null,
                null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(true, result.success);
    }

    public class SNRSelectorMock implements SNR_DAO {


        public List<AggregateResult> getServiceCategories() {
            return new List<AggregateResult>();
        }

        public List<AggregateResult> getServiceTypesNoServiceRigProgram(String serviceCategoryId, Boolean isCommunityUser) {
            return new List<AggregateResult>();
        }

        public List<HOG_Notification_Type__c> getNotificationTypeById(String Id) {
            return new List<HOG_Notification_Type__c>{
                    NotificationTypeTestData.createNotificationType(
                            null,
                            false,
                            false,
                            false,
                            true)
            };
        }

        public List<AggregateResult> getServiceActivityNames(String serviceCategoryId, String serviceType) {
            return new List<AggregateResult>();
        }

        public List<HOG_Notification_Type_Priority__c> getServicePriorities(String serviceType) {
            return new List<HOG_Notification_Type_Priority__c>{
                    NotificationTypePriorityTestData.createNotificationTypePriority(
                            null,
                            null,
                            false)
            };
        }

        public List<AggregateResult> getServiceRequired(String serviceCategoryId, String serviceType, String serviceActivity, Boolean wellLocationStatus) {
            return new List<AggregateResult>();
        }

        public List<AggregateResult> getServiceSpecifics(String serviceCategoryId, String serviceTypeId, String serviceActivityId, String serviceRequiredId, Boolean wellLocationStatusProducing) {
            return new List<AggregateResult>();
        }

        public List<HOG_Service_Required__c> getOneServiceRequired(String serviceRequiredId) {
            return new List<HOG_Service_Required__c>{
                    ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true)
            };
        }

        public List<Location__c> getOneWellLocation(String locationId) {
            return null;
        }

        public List<Facility__c> getOneFacility(String facilityId) {
            return null;
        }

        public List<Equipment__c> getOneEquipment(String equipmentId) {
            return null;
        }

        public List<Well_Event__c> getOneWellEvent(String wellEventId) {
            return null;
        }

        public List<System__c> getOneSystem(String systemId) {
            return null;
        }

        public List<Sub_System__c> getOneSubSystem(String subSystemId) {
            return null;
        }

        public List<Yard__c> getOneYard(String yardId) {
            return null;
        }

        public List<Functional_Equipment_Level__c> getFEL(String felId) {
            return null;
        }
    }
}