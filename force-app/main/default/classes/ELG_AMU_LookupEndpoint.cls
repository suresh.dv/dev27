/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:
 *  Test Class		ELG_ListView_EndpointTest
 *  History:        Created on 3/4/2020   
 */

public class ELG_AMU_LookupEndpoint {
	private static final String CLASS_NAME = 'ELG_AMU_LookupEndpoint';

	@AuraEnabled(Cacheable = true)
	public static List<Field__c> fetchPostAMUs(String stringToSearch) {
		try {
			List<String> postAMUs = new List<String>();
			List<ELG_Post__c> posts = [SELECT Operating_Field_AMU__c FROM ELG_Post__c];
			stringToSearch = '%' + stringToSearch + '%';

			for (ELG_Post__c post : posts) {
				postAMUs.add(post.Operating_Field_AMU__c);
			}

			List<Field__c> operatingFields = [SELECT Name FROM Field__c WHERE Id IN :postAMUs AND Name LIKE :stringToSearch];
			return operatingFields;

		} catch (Exception ex) {

			System.debug(CLASS_NAME + '-> Exception: ' + ex.getMessage());
			return null;
		}
	}
}