/**
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:    Test class for SPCC_BPOControllerX
 *  History:        Created on 12/3/2018
 *                  mz: 8-Jan-2019 - fixed & updated test for some changes in controller
 *                                 - refactored
 */

@IsTest
public class SPCC_BPOControllerXTest {

	public static Final Date startDate = Date.today();
	public static Final Date endDate = Date.today() + 1;

	@TestSetup
	static void setup() {

		//Create a SPCC Data

		User epcUser = SPCC_TestData.createEPCUser();
		User pcUser = SPCC_TestData.createPCUser();

		Test.startTest();

		System.runAs(pcUser) {

			Account spccAccount = SPCC_TestData.createSPCCVendor('SPCC Vendor');
			insert spccAccount;

			Account epcAccount = SPCC_TestData.createEPCVendor('EPC Vendor');
			insert epcAccount;

			Contact epcContact = SPCC_TestData.createContact(epcAccount.Id, 'EPC', 'Acc', epcUser.Id);
			insert epcContact;

			SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('Test Project', '12345', '', 'Facilities');
			insert ewr;

			SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('12-123A56-12-AB-12', '12-123A56-12-AB-12', 'Test AFE', ewr.Id);
			insert afe;

			SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, null, 'Test GL Code', '0123456', 'Service', 30, 200);
			insert costElement;

			SPCC_Cost_Code__c costCode = SPCC_TestData.createCostCode(costElement.Id, '9999', 'Test Codt Code', 300);
			insert costCode;

			SPCC_Vendor_Account_Assignment__c vaa2 = SPCC_TestData.createVAA(ewr.Id, spccAccount.Id);
			insert vaa2;

			SPCC_Blanket_Purchase_Order__c po2 = SPCC_TestData.createBPO('0000000001', spccAccount.Id, startDate, endDate);
			insert po2;
			SPCC_Blanket_Purchase_Order__c po3 = SPCC_TestData.createBPO('0000000002', spccAccount.Id, startDate, endDate);
			insert po3;

			Test.stopTest();
		}
	}

	@IsTest
	static void testSaveBPOWithoutRequiredFields() {

		Test.startTest();

		//Set Page
		PageReference pref = Page.SPCC_BPOCreate;
		Test.setCurrentPage(pref);
		System.currentPageReference().getParameters();
		SPCC_Blanket_Purchase_Order__c newBPO = new SPCC_Blanket_Purchase_Order__c();

		//Create Controller
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(newBPO);
		SPCC_BPOControllerX bpoCtrl = new SPCC_BPOControllerX(stdCtrl);


		Account acc = [SELECT Name,Id FROM Account WHERE Name = 'SPCC Vendor' LIMIT 1];

		//Add Line Item
		System.assertEquals(null, bpoCtrl.addBPOLineItem());
		System.assertEquals(bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c, null);
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c = '0010';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Short_Text__c = 'Test Line Item 1';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Committed_Amount__c = 100;
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.GL_Number__c = '9999999';

		System.assertEquals(true, bpoCtrl.getDisabledAddNewBPOButton());

		//Try to save BPO without BPO without Name, Vendor / Validity Start and End Date (all BPO required fields)
		System.assertEquals(null, bpoCtrl.saveBPO());

		try {
			System.assertEquals(null, bpoCtrl.saveBPO());

		} catch (Exception e) {
			System.assert(e.getMessage().contains('Blanket PO Number is required field. Please enter <b> Blanket PO Number </b>'));
			System.assert(e.getMessage().contains('Vendor is required field. Please select <b> Vendor Account </b>'));
			System.assert(e.getMessage().contains('Validity Start and End Dates are required fields. Please select <b> Validity Start and End Date </b>'));
		}

		Test.stopTest();
	}


	@IsTest
	static void testSaveBPOWithoutLineItem() {

		Test.startTest();

		//Set Page
		PageReference pref = Page.SPCC_BPOCreate;
		Test.setCurrentPage(pref);
		System.currentPageReference().getParameters();

		//Create Controller
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(GetBPOData()[0]);
		SPCC_BPOControllerX bpoCtrl = new SPCC_BPOControllerX(stdCtrl);

		//Try to save po without line item to check validation is working (testing Save button)
		System.assertEquals(null, bpoCtrl.saveBPO());

		Set<String> errMsgSet = new Set<String>();
		for (ApexPages.Message msg : ApexPages.getMessages()) {

			errMsgSet.add(msg.getSummary());
		}

		System.assertEquals(true, errMsgSet.contains('No line items added to the Blanket Purchase Order. ' +
				'At least one <b>Line Item</b> is required. ' +
				'Click "<b>Add New Blanket PO Line Item</b>" button to Add Line Item.'));

		Test.stopTest();
	}


	@IsTest
	static void testSaveBpoWithoutBpoLiRequiredFields() {

		Test.startTest();

		//Set Page
		PageReference pref = Page.SPCC_BPOCreate;
		Test.setCurrentPage(pref);
		System.currentPageReference().getParameters();
		SPCC_Blanket_Purchase_Order__c newBPO = new SPCC_Blanket_Purchase_Order__c();

		//Create Controller
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(newBPO);
		SPCC_BPOControllerX bpoCtrl = new SPCC_BPOControllerX(stdCtrl);

		Account acc = [SELECT Name,Id FROM Account WHERE Name = 'SPCC Vendor' LIMIT 1];

		//Add BPO Details
		bpoCtrl.bpo.Name = '0000000003';
		bpoCtrl.bpo.Vendor_Account__c = acc.Id;
		bpoCtrl.bpo.Validity_Start_Date__c = startDate;
		bpoCtrl.bpo.Validity_End_Date__c = endDate;

		//Add Line Item
		System.assertEquals(null, bpoCtrl.addBPOLineItem());
		System.assertEquals(bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c, null);

		//Try to save BPO without BPOLI Required Fields
		System.assertEquals(null, bpoCtrl.saveBPO());

		try {
			System.assertEquals(null, bpoCtrl.saveBPO());

		} catch (Exception e) {
			System.assert(e.getMessage().contains('G/L Number is required field. Please select <b>G/L Number</b> for Line Item #' +
					bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c));
			System.assert(e.getMessage().contains('Short Text is required field. Please type <b>Short Text</b> for Line Item #' +
					bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c));
			System.assert(e.getMessage().contains('Amount is required field. Please enter <b>Amount</b> for Line Item #' +
					bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c));
		}

		Test.stopTest();
	}

	@IsTest
	static void testSaveAndNew() {

		Test.startTest();

		//Set Page
		PageReference pref = Page.SPCC_BPOCreate;
		Test.setCurrentPage(pref);
		System.currentPageReference().getParameters();
		SPCC_Blanket_Purchase_Order__c newBPO = new SPCC_Blanket_Purchase_Order__c();

		//Create Controller
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(newBPO);
		SPCC_BPOControllerX bpoCtrl = new SPCC_BPOControllerX(stdCtrl);

		Account acc = [SELECT Name,Id,SAP_ID__c FROM Account WHERE Name = 'SPCC Vendor' LIMIT 1];

		bpoCtrl.vendorId = acc.Name + ' (' + acc.SAP_ID__c + ')';

		//Add BPO Information
		bpoCtrl.bpo.Name = '0000000003';
		//bpoCtrl.bpo.Vendor_Account__c = bpoCtrl.vendorId;
		bpoCtrl.bpo.Validity_Start_Date__c = startDate;
		bpoCtrl.bpo.Validity_End_Date__c = endDate;

		//Add Line Item
		System.assertEquals(null, bpoCtrl.addBPOLineItem());
		System.assertEquals(bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c, null);
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c = '0010';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Short_Text__c = 'Test Line Item 1';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Committed_Amount__c = 100;
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.GL_Number__c = '9999999';

		//Remove line Item
		System.currentPageReference().getParameters().put('newBPOLIIndex', '0');
		System.assertEquals(null, bpoCtrl.removeBPOLineItem());
		System.assert(bpoCtrl.blanketPOlineItems.isEmpty());

		//Add Line Item again, because can't create BPO without line items
		System.assertEquals(null, bpoCtrl.addBPOLineItem());
		System.assertEquals(bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c, null);
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c = '0010';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Short_Text__c = 'Test Line Item 1';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Committed_Amount__c = 100;
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.GL_Number__c = '9999999';
		System.assertEquals(false, bpoCtrl.blanketPOlineItems.isEmpty());

		System.assertEquals(2, GetBPOData().size());

		//Test redirect to BPO Create page after save and new button click
		System.assertEquals('/apex/spcc_bpocreate', bpoCtrl.saveAndNew().getURL());

		System.assertEquals(bpoCtrl.bpo.Name, GetBPOData()[2].Name);
		System.assertEquals(3, GetBPOData().size());

		Test.stopTest();
	}

	@IsTest
	static void testSaveWithoutError() {

		Test.startTest();

		//Set Page
		PageReference pref = Page.SPCC_BPOCreate;
		Test.setCurrentPage(pref);
		System.currentPageReference().getParameters();
		SPCC_Blanket_Purchase_Order__c newBPO = new SPCC_Blanket_Purchase_Order__c();

		//Create Controller
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(newBPO);
		SPCC_BPOControllerX bpoCtrl = new SPCC_BPOControllerX(stdCtrl);

		Account acc = [SELECT Name,Id FROM Account WHERE Name = 'SPCC Vendor' LIMIT 1];

		//Add BPO Information
		bpoCtrl.bpo.Name = '0000000003';
		bpoCtrl.bpo.Vendor_Account__c = acc.Id;
		bpoCtrl.bpo.Validity_Start_Date__c = startDate;
		bpoCtrl.bpo.Validity_End_Date__c = endDate;

		//Add Line Item
		System.assertEquals(null, bpoCtrl.addBPOLineItem());
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c = '0010';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Short_Text__c = 'Test Line Item 1';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Committed_Amount__c = 100;
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.GL_Number__c = '9999999';

		System.assertEquals(2, GetBPOData().size());

		System.assertEquals(false, bpoCtrl.getDisabledAddNewBPOButton());

		//Try to save valid BPO (testing Save button)
		bpoCtrl.saveBPO();
		System.assertEquals(bpoCtrl.bpo.Name, GetBPOData()[2].Name);
		System.assertEquals(3, GetBPOData().size());

		//just a coverage for vendorId getter & setter
		bpoCtrl.vendorId = bpoCtrl.bpo.Vendor_Account__r.Name;
		bpoCtrl.vendorId = null;
		Test.stopTest();
	}


	@IsTest
	static void updateBpoRecordTest() {

		Test.startTest();

		//Set Page
		PageReference pref = Page.SPCC_BPOCreate;
		Test.setCurrentPage(pref);
		System.currentPageReference().getParameters();

		//Create Controller
		SPCC_Blanket_Purchase_Order__c bpoRetrieved = [
				SELECT Id, Name, Vendor_Account__c, Validity_Start_Date__c, Validity_End_Date__c,
						Vendor_Account__r.Name, Vendor_Account__r.SAP_ID__c
				FROM SPCC_Blanket_Purchase_Order__c
				WHERE Name = '0000000002'
				LIMIT 1
		];
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(bpoRetrieved);
		SPCC_BPOControllerX bpoCtrl = new SPCC_BPOControllerX(stdCtrl);

		Account acc = [SELECT Name,Id,SAP_ID__c FROM Account WHERE Name = 'SPCC Vendor' LIMIT 1];

		System.assertEquals('SPCC Vendor (' + acc.SAP_ID__c + ')', bpoCtrl.vendorId);

		//Update Description on BPO
		bpoRetrieved.Description__c = 'Test BPO 3';

		//Add Line Item
		System.assertEquals(null, bpoCtrl.addBPOLineItem());
		System.assertEquals(bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c, null);
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Item__c = '0010';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Short_Text__c = 'Test Line Item 1';
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.Committed_Amount__c = 100;
		bpoCtrl.blanketPOlineItems[0].blanketPOlineItem.GL_Number__c = '9999999';

		System.assertEquals(2, GetBPOData().size());
		//Try to save and update BPO (testing Save button)
		bpoCtrl.saveBPO();
		SPCC_Blanket_Purchase_Order__c updateConfirm = [
				SELECT Description__c
				FROM SPCC_Blanket_Purchase_Order__c
				WHERE Name = '0000000002'
				LIMIT 1
		];
		System.assertEquals('Test BPO 3', updateConfirm.Description__c, 'Record haven\'t been updated.');
		System.assertEquals(bpoCtrl.bpo.Name, GetBPOData()[1].Name);
		System.assertEquals(2, GetBPOData().size());

		Test.stopTest();
	}



	private static List<SPCC_Blanket_Purchase_Order__c> GetBPOData() {

		return [
				SELECT Name, Vendor_Account__c, Description__c, Validity_Start_Date__c,
						Validity_End_Date__c, Vendor_Account__r.Name
				FROM SPCC_Blanket_Purchase_Order__c
		];
	}

}