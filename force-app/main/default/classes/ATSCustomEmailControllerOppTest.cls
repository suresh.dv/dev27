/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest (SeeAllData = true)
private class ATSCustomEmailControllerOppTest {
    
    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
            
            // Create a Test Account with the name Asphalt
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
            
            insert account;
            System.AssertNotEquals(account.Id, Null);
            
            Contact cont = ContactTestData.createContact(account.Id, 'FName', 'LName');
            insert cont; 
            
            ATS_Parent_Opportunity__c  tender =  ATSParentOpportunityTestData.createTender(1)[0];
            
            ATS_Tender_Customers__c customer = ATSTestData.createTenderCustomer(account.Id, tender.id, cont.id);
            customer.Won__c = true;
            insert customer; 
            
            // Create Products        
            List<Product2> productList = ProductTestData.createProducts(101);
            insert productList;
            
            // Create Price Book Entries
            /*List<PriceBookEntry> priceBookEntryList = PriceBookEntryTestData.createPriceBookEntry(productList);
insert priceBookEntryList;*/
            
            Map<String, Id> recordTypeMap = new Map<String, Id>();
            
            for(RecordType recType : [SELECT Id, DeveloperName FROM RecordType 
                                      WHERE DeveloperName like 'CPM_Asphalt' AND 
                                      SObjectType = 'Opportunity']) {
                                          recordTypeMap.put(recType.DeveloperName, recType.Id);            
                                      }
            
            List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
            
            Opportunity opp = OpportunityTestData.createOpportunity(recordTypeMap.get('CPM_Asphalt'));
            opp.Opportunity_ATS_Product_Category__c = tender.Id;
            opp.StageName = 'Won'; 
            opp.Confirmation_Method__c = 'PO';
            opp.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            opp.PO__c = 'PO Test';
            opp.cpm_Bid_Due_Date_Time__c = date.today();
            insert opp;
            opp.Pricebook2Id = standardPriceBook[0].Id;
            opp.priceBook2 = standardPriceBook[0];
            update opp;
            
            List<PriceBookEntry> priceBookEntryList = [Select Id From PriceBookEntry Where Pricebook2Id =: standardPriceBook[0].Id];
            priceBookEntryList[0].IsActive = true;
            update priceBookEntryList[0]; 
            OpportunityLineItem oli = OpportunityLineItemTestData.createOpportunityLineItem(opp, priceBookEntryList[0]);
            insert oli;
            
            Opportunity theOpp = [select Id, Pricebook2Id, PriceBook2.Name, RecordTypeId from Opportunity where Id = :opp.Id limit 1];
            theOpp.Confirmation_Method__c = 'PO';
            theOpp.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            update theOpp;
            System.assertEquals(theOpp.Pricebook2Id, standardPriceBook[0].Id);
            System.assertNotEquals(theOpp.Pricebook2, Null);
            System.assertNotEquals(theOpp.PriceBook2.Name, Null);        
            
            //freight
            
            cpm_Supplier__c sup1 = new cpm_Supplier__c();
            sup1.name = 'a9s290000008OKYAA2';
            insert sup1;
            
            cpm_Supplier__c sup2 = new cpm_Supplier__c();
            sup2.name = 'a9s290000008OJlAAM';
            insert sup2;
            
            cpm_Supplier__c sup3 = new cpm_Supplier__c();
            sup3.name = 'a9s290000008OJ5AAM';
            insert sup3;
            
            cpm_Supplier__c com1 = new cpm_Supplier__c();
            com1.name = 'a9s290000004QLMAA2';
            insert com1;

            cpm_Supplier__c com2 = new cpm_Supplier__c();
            com2.name = 'a9s290000004QLMAA3';
            insert com2;

            cpm_Supplier__c com3 = new cpm_Supplier__c();
            com3.name = 'a9s290000004QLMAA4';
            insert com3;

            cpm_Supplier__c com4 = new cpm_Supplier__c();
            com4.name = 'a9s290000004QLMAA5';
            insert com4;
            
            ATS_Freight__c  freight = new ATS_Freight__c();
            freight.cpm_HuskySupplier1_ats_pricing__c = sup1.Id;
            freight.cpm_HuskySupplier2_ats_pricing__c = sup2.Id;
            freight.cpm_HuskySupplier3_ats_pricing__c = sup3.Id;
            freight.cpm_Competitor1_ats_pricing__c= com1.Id;
            freight.cpm_Competitor2_ats_pricing__c= com2.Id;
            freight.cpm_Competitor3_ats_pricing__c= com3.Id;
            freight.cpm_Competitor4_ats_pricing__c= com4.Id;
            freight.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';    
            freight.ATS_Freight__c  =   theOpp.Id;
            freight.Husky_Supplier_1_Selected__c = true;                       
            insert freight;      
            
            //Create Controller
            ATSCustomEmailControllerOpp controller = new ATSCustomEmailControllerOpp();
            controller.OpportunityID = tender.id;
            
            ATSCustomEmailControllerOpp.ATSTenderWrapper  tenderWrapper = controller.getATSTender();
            System.AssertNotEquals(tenderWrapper , null);
            
            /* NEW START */
            List<Opportunity> opportunity_Obj  =  [SELECT IsPrivate,Name,StageName,CloseDate,Received_Credit_Application__c,Product__c,Solution__c,Contract_Renewal__c,cpm_Mail_sent__c,cpm_Estimate__c from Opportunity];
            System.assertEquals(true,opportunity_Obj.size()>0);
            
            List<OpportunityLineItem > OLI_Obj  = [Select PricebookEntry.Product2.id, PricebookEntry.Product2.Name, cpm_Product_Family__c,Unit__c, Quantity From OpportunityLineItem ];
            System.assertEquals(true,OLI_Obj.size()>0);
            
            List<ATS_Freight__c> ats_freight_Obj  =  [SELECT Name,RecordTypeId,Freight_Comments__c,Freight_Rate__c,Husky_Supplier_1_Selected__c,Husky_Supplier_1__c,Husky_Supplier_2_Selected__c,Husky_Supplier_2__c,Local_Freight_Via__c,Account__c,Freight_to_COLLECT__c,Freight_to_Pay__c,Husky_Supplier_3__c,Rail_Car_Routing__c,Husky_Supplier_3_Selected__c from ATS_Freight__c];
            System.assertEquals(true,ats_freight_Obj.size()>0);



            ATSCustomEmailControllerOpp obj01 = new ATSCustomEmailControllerOpp();
            obj01.OpportunityID = '01p90000006uk2w';
            
            
            ATSCustomEmailControllerOpp.ATSTenderWrapper obj31 = new ATSCustomEmailControllerOpp.ATSTenderWrapper('01p90000006uk2w');
            obj31.parentTenderID = '01p90000006uk2w';
            obj31.Opportunities = new List<ATSCustomEmailControllerOpp.ATSOpportunityWrapper>();
            obj31.Destination = 'test data';
            ATSCustomEmailControllerOpp.ATSProductWrapper obj81 = new ATSCustomEmailControllerOpp.ATSProductWrapper('test data','test data',10, 'prod family');
            obj81.Name = 'test data';
            obj81.Unit = 'test data';
            obj81.Qty = 10;
            
            ATSCustomEmailControllerOpp.ATSOpportunityWrapper obj131 = new ATSCustomEmailControllerOpp.ATSOpportunityWrapper(theOpp.Id,'test data');
            obj131.Products = new List<ATSCustomEmailControllerOpp.ATSProductWrapper>();
            obj131.Suppliers = new List<ATSCustomEmailControllerOpp.ATSSupplierWrapper>();
            obj131.ProductCategory = 'test data';
            obj131.FreightComments = 'test data';
            obj131.OpportunityName = 'test data';
            obj131.SalesContractDueDate = Date.today();     
            
            ATSCustomEmailControllerOpp.ATSSupplierWrapper obj141 = new ATSCustomEmailControllerOpp.ATSSupplierWrapper('test data','test data');
            obj141.supplierName = 'test data';
            obj141.productFamily = 'test data';
            ATSCustomEmailControllerOpp.ATSFreightCommentWrapper obj181 = new ATSCustomEmailControllerOpp.ATSFreightCommentWrapper('test data','test data');
            obj181.freightComment = 'test data';            
            /* NEW END */
            //1 opportunity
            //            System.AssertEquals(tenderWrapper.Opportunities.size(),1); 
            //            System.AssertEquals(tenderWrapper.Destination,'Test Destination');   
            /*
ATSCustomEmailControllerOpp.ATSOpportunityWrapper oppWrapper =  tenderWrapper.Opportunities[0];
//1 product          
System.AssertEquals(oppWrapper.Products .size(),1); 
//3 suppliers
System.AssertEquals(oppWrapper.Suppliers.size(),3);   */
        }
        
    }
    static testMethod void test_getATSTender_UseCase1(){
        List<Opportunity> opportunity_Obj  =  [SELECT IsPrivate,Name,StageName,CloseDate,Received_Credit_Application__c,Product__c,Solution__c,Contract_Renewal__c,cpm_Mail_sent__c,cpm_Estimate__c from Opportunity];
        System.assertEquals(true,opportunity_Obj.size()>0);
        List<ATS_Freight__c> ats_freight_Obj  =  [SELECT Name,RecordTypeId,Freight_Comments__c,Freight_Rate__c,Husky_Supplier_1_Selected__c,Husky_Supplier_1__c,Husky_Supplier_2_Selected__c,Husky_Supplier_2__c,Local_Freight_Via__c,Account__c,Freight_to_COLLECT__c,Freight_to_Pay__c,Husky_Supplier_3__c,Rail_Car_Routing__c,Husky_Supplier_3_Selected__c from ATS_Freight__c];
        System.assertEquals(true,ats_freight_Obj.size()>0);
        ATSCustomEmailControllerOpp obj01 = new ATSCustomEmailControllerOpp();
        obj01.OpportunityID = '01p90000006uk2w';
        obj01.getATSTender();
    }
    
}