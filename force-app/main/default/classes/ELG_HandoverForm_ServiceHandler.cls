/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    Class to provide responses to Endpoint
 *  Test Class:		ELG_HandoverFrom_EndpointTest
 *  History:        Created on 7/11/2019   
 */

public inherited sharing class ELG_HandoverForm_ServiceHandler {
	private static final String CLASS_NAME = String.valueOf(ELG_HandoverForm_ServiceHandler.class);
	private static String shiftId;

	public HOG_CustomResponseImpl loadHandoverForm(String handoverFormId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Shift_Handover__c handoverForm = ELG_DAOProvider.handoverDAO.handoverForm(handoverFormId);
		shiftId = handoverForm.Shift_Assignement__c;
		response.addResult(handoverForm);

		response.addResult(setHandoverStatus(handoverForm, handoverForm.Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c));
		return response;
	}

	private Boolean setHandoverStatus(ELG_Shift_Handover__c handoverForm, Boolean shiftEngineerRequired) {
		Boolean isReadOnly = false;

		if (shiftEngineerRequired) {
			if (handoverForm.Sr_Supervisor__c != null
					&& handoverForm.Steam_Chief__c != null
					&& handoverForm.Shift_Engineer__c != null
					&& handoverForm.Shift_Lead__c != null) {
				isReadOnly = true;
			}
		} else {
			if (handoverForm.Sr_Supervisor__c != null
					&& handoverForm.Steam_Chief__c != null
					&& handoverForm.Shift_Lead__c != null) {
				isReadOnly = true;
			}
		}

		return isReadOnly;
	}

	public HOG_CustomResponseImpl preparedLogEntries(String categoryName) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<ELG_Log_Entry__c> listOfLogEntries = ELG_HandoverForm_Service.prepareLogEntries(categoryName);
		if (listOfLogEntries.isEmpty()) {
			response.addError(System.Label.ELG_Wrong_Handover_Category_Name);
		} else {
			response.addResult(listOfLogEntries);
		}
		return response;
	}

	public HOG_CustomResponseImpl logEntriesToInsert(List<ELG_Log_Entry__c> listOfEntries, ELG_Shift_Assignement__c shiftAssignment) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<ELG_Log_Entry__c> listOfLogEntries = ELG_HandoverForm_Service.insertLogs(listOfEntries, shiftAssignment);
		if (listOfLogEntries.isEmpty()) {
			response.addError(System.Label.ELG_No_Questions_Answered);
		} else {
			response.addResult(listOfLogEntries);
		}
		return response;
	}

	public HOG_CustomResponseImpl updateHandoverIncOpAndStatus(String HOFormId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		ELG_Shift_Handover__c handoverForm = ELG_DAOProvider.handoverDAO.handoverForm(HOFormId);

		if (ELG_Utilities.hasEditAccessToSignOffAndStartNewShift()) {
			if (handoverForm.Status__c == ELG_Constants.HANDOVER_ACCEPTED || handoverForm.Status__c == ELG_Constants.HANDOVER_IN_PROGRESS) {
				response.addError(System.Label.ELG_Handover_Cannot_Be_Updated);
			} else if (handoverForm.Shift_Assignement__r.Primary__c != UserInfo.getUserId()) {
				response.addError(System.Label.ELG_Only_Primary_Can_Start_Handover);
			} else {
				response.addResult(ELG_HandoverForm_Service.updateHandoverStatusAndSignOff(handoverForm));
			}
		} else {
			response.addError(System.Label.ELG_Wrong_Permission_Set);
		}

		return response;
	}


	public HOG_CustomResponseImpl acceptHandoverAndChangeStatus(ELG_Shift_Handover__c HOForm, String currentUserId, String traineeId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		ELG_Shift_Handover__c handoverForm = ELG_DAOProvider.handoverDAO.handoverForm(HOForm.Id);

		if (ELG_Utilities.hasEditAccessToAcceptHandover()) {
			if (handoverForm.Status__c == ELG_Constants.HANDOVER_ACCEPTED) {
				response.addError(System.Label.ELG_Handover_Cannot_Be_Updated);
			} else {
				ELG_Shift_Assignement__c nextShift = ELG_DAOProvider.handoverDAO.getNextShift(handoverForm);
				System.debug(CLASS_NAME + ' trainee: ' + traineeId);
				response.addResult(ELG_HandoverForm_Service.acceptHandoverAndChangeStatus(HOForm, currentUserId, nextShift, traineeId));
			}
		} else {
			response.addError(System.Label.ELG_Wrong_Permission_Set);
		}

		return response;
	}

	public HOG_CustomResponseImpl genericLogEntries(String shiftAssignmentId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		Id recordTypeId = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();
		response.addResult(ELG_DAOProvider.handoverDAO.getGenericLogEntries(shiftAssignmentId, recordTypeId));
		return response;
	}

	public HOG_CustomResponseImpl updateReviewOnHandover(ELG_Shift_Handover__c HOForm,
			List<String> sectionsForUpdate,
			String currentUser) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		if (ELG_Utilities.hasEditAccessForReview()) {
			if (setHandoverStatus(HOForm, HOForm.Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c)) {
				response.addError(System.Label.ELG_Handover_Cannot_Be_Updated);
			} else {
				if (sectionsForUpdate.contains('shiftEngineer')) {
					updateReviewOnHandover(HOForm, currentUser);
				}

				if (sectionsForUpdate.isEmpty()) {
					response.addError(System.Label.ELG_Empty_Review_Selection);
				} else {
					response.addResult(ELG_HandoverForm_Service.updateHandoverReview(HOForm,
							sectionsForUpdate,
							currentUser));
					response.addResult(HOForm);
				}
			}
		} else {
			response.addError(System.Label.ELG_Wrong_Permission_Set);
		}


		return response;
	}

	public HOG_CustomResponseImpl updateReviewOnHandover(ELG_Shift_Handover__c HOForm, String currentUser) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		if (ELG_Utilities.hasEditAccessForReviewShiftEngineer()) {
			response.addResult(ELG_HandoverForm_Service.updateHandoverReview(HOForm, currentUser));
			response.addResult(HOForm);
		} else {
			response.addError(System.Label.ELG_Wrong_Permission_Set);
		}
		return response;
	}

	public HOG_CustomResponseImpl getAllHandoverLogEntries() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		response.addResult(ELG_HandoverForm_Service.getAllHandoverLogEntries(shiftId));
		return response;
	}

	public HOG_CustomResponseImpl userCannotReview(String currentUserId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		response.addResult(ELG_DAOProvider.handoverDAO.getUserPermissions(ELG_Constants.REVIEWER_PERMISSION_SET, currentUserId).isEmpty());
		return response;
	}

}