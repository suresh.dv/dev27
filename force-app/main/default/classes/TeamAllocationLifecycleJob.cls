public class TeamAllocationLifecycleJob implements Schedulable {
	public void execute(SchedulableContext ctx) {
    	TeamAllocationLifecycleManager.process();
    }
}