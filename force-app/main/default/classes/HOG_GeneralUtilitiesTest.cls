/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_GeneralUtilities.cls
History:        jschn 05.21.2018 - Created.
                jschn 06.05.2018 - added getUrlParam, addPageError test
**************************************************************************************************/
@IsTest
private class HOG_GeneralUtilitiesTest {
	
	@IsTest static void testGeneralUtilities() {
		Test.startTest();
		
		List<Account> accounts = [SELECT Id FROM Account];
		List<Id> accountIds = HOG_GeneralUtilities.getListOfIds(accounts);
		for (Integer i = 0; i < accounts.size(); i++) {
			System.assertEquals(accounts.get(i).Id, accountIds.get(i));
		}

		Set<Id> setAccountIds = HOG_GeneralUtilities.getSetOfIds(accounts);
		System.assertEquals(accounts.size(), setAccountIds.size());

		List<Contact> contacts = [SELECT Id, AccountId FROM Contact];
		List<Id> contactAccountIds = HOG_GeneralUtilities.getListOfParentIds(contacts, 'AccountId');
		System.assertEquals(1, contactAccountIds.size());
		for (Contact contact : contacts) {
			System.assertEquals(contact.AccountId, contactAccountIds.get(0));
		}

		Set<Id> contactAccountSetIds = HOG_GeneralUtilities.getSetOfParentIds(contacts, 'AccountId');
		System.assertEquals(1, contactAccountSetIds.size());

		String defaultTaskPriorityRetrieved = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(Task.Priority.getDescribe().getSobjectField());
		System.assertEquals('Normal', defaultTaskPriorityRetrieved);

		String defaultTaskTypeRetrieved = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(Task.Subject.getDescribe().getSobjectField());
		System.assertEquals('Call', defaultTaskTypeRetrieved);

		String wrongPicklistField = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(Task.Description.getDescribe().getSobjectField());
		System.assertEquals('', wrongPicklistField);
		//added 06.05.2018
		String param = 'id';
		String value = 'value';
        HOG_GeneralUtilities.setUrlParam(param, value);
        System.assertEquals(value, HOG_GeneralUtilities.getUrlParam(param));

        System.assert(!HOG_GeneralUtilities.hasPageError());

        System.assert(!ApexPages.hasMessages());
        String errorMsg = 'Error msg';
        HOG_GeneralUtilities.addPageError(errorMsg);
        System.assert(ApexPages.hasMessages());
        System.assertEquals(errorMsg, ApexPages.getMessages().get(0).getSummary());

        System.assert(HOG_GeneralUtilities.hasPageError());

		//buildKey test
		Account acc = new Account(Name = 'Test', Active__c = true);
		String buildKeyResult = HOG_GeneralUtilities.buildKey(null, null);
		System.assertEquals('', buildKeyResult);
		buildKeyResult = HOG_GeneralUtilities.buildKey(new Account(), null);
		System.assertEquals('', buildKeyResult);
		buildKeyResult = HOG_GeneralUtilities.buildKey(acc, null);
		System.assertEquals('', buildKeyResult);
		buildKeyResult = HOG_GeneralUtilities.buildKey(acc, new List<String> {'Name', 'Active__c'});
		System.assertEquals('Testtrue', buildKeyResult);
		Boolean buildKeyFailFlag = false;
		Boolean expectedFailFlag = true;
		try{
			HOG_GeneralUtilities.buildKey(acc, new List<String> {'WrongFieldName'});
		} catch (Exception ex) {
			buildKeyFailFlag = true;
		}
		System.assertEquals(expectedFailFlag, buildKeyFailFlag);

		//getSubstring test
		String subStringString = 'abc';
		String getSubstringResult;
		getSubstringResult = HOG_GeneralUtilities.getSubstring(subStringString, 2);
		System.assertEquals('ab', getSubstringResult);
		getSubstringResult = HOG_GeneralUtilities.getSubstring(subStringString, 4);
		System.assertEquals('abc', getSubstringResult);
		getSubstringResult = HOG_GeneralUtilities.getSubstring('', 4);
		System.assertEquals(null, getSubstringResult);

		Test.stopTest();
	}

	@TestSetup static void prepareData() {
		List<Account> accounts = (List<Account>) HOG_SObjectFactory.createSObjectList(new Account(), 3, true);
		List<Contact> contacts = (List<Contact>) HOG_SObjectFactory.createSObjectList(new Contact(), 3);
		Account acc = accounts.get(0);
		for(Contact contact : contacts) {
			contact.AccountId = acc.Id;
		}
		insert contacts;
	}
	
}