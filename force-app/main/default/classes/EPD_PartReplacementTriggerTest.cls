/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_PartReplacementTrigger
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
@IsTest
private class EPD_PartReplacementTriggerTest {

    @IsTest
    static void delete_allowed() {
        EPD_Part_Replacement__c partReplacement = EPD_TestData.createPartReplacement();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = false;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete partReplacement;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void delete_restricted() {
        EPD_Part_Replacement__c partReplacement = EPD_TestData.createPartReplacement();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = true;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete partReplacement;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}