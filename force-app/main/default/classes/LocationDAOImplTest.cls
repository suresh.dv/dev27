/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for LocationDAOImpl
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
@IsTest
private class LocationDAOImplTest {

    @IsTest
    static void getLocationById_withoutId() {
        List<Location__c> locations;
        LocationDAO dao = new LocationDAOImpl();
        Integer expectedCount = 0;

        Test.startTest();
        locations = dao.getLocationById(null);
        Test.stopTest();

        System.assertNotEquals(null, locations);
        System.assertEquals(expectedCount, locations.size());
    }

    @IsTest
    static void getLocationById_withWrongId() {
        List<Location__c> locations;
        LocationDAO dao = new LocationDAOImpl();
        Integer expectedCount = 0;

        Test.startTest();
        locations = dao.getLocationById(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, locations);
        System.assertEquals(expectedCount, locations.size());
    }

    @IsTest
    static void getLocationById_withGoodId() {
        Location__c loc = [SELECT Id FROM Location__c LIMIT 1];
        List<Location__c> locations;
        LocationDAO dao = new LocationDAOImpl();
        Integer expectedCount = 1;

        Test.startTest();
        locations = dao.getLocationById(loc.Id);
        Test.stopTest();

        System.assertNotEquals(null, locations);
        System.assertEquals(expectedCount, locations.size());
    }

    @TestSetup
    static void prepareData() {
        HOG_VentGas_TestData.createLocation();
    }

}