/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Helper for scheduling VTT_WorkOrderFLOCReparent job.
Test Class:     VTT_WorkOrderFLOCReparentSchedulerTest
History:        jschn 2018-12-14 - Created. (US - 001323)
*************************************************************************************************/
public without sharing class VTT_WorkOrderFLOCReparentScheduler {

    @TestVisible
    private static final String JOB_NAME = 'VTT - Work Order and Activity Re-parenting job'
            + (Test.isRunningTest() ? ' Test' : '');

    @TestVisible
    private static final String CRON = '0 0 5 * * ?';

    public static Id schedule(String cronString) {
        VTT_WorkOrderFLOCReparentSchedulable job = new VTT_WorkOrderFLOCReparentSchedulable();
        return System.schedule(JOB_NAME, cronString, job);
    }

    public static Id schedule() {
        return schedule(CRON);
    }

}