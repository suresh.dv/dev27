global virtual with sharing class DSP_CMS_ReportCardController extends cms.ContentTemplateController
{
    // Edit page constructor
    global DSP_CMS_ReportCardController(cms.CreateContentController cc)
    {
        super(cc);
    }
    
    global DSP_CMS_ReportCardController(cms.GenerateContent cc){
        super(cc);
    }
    
    // Constructor
    global DSP_CMS_ReportCardController()
    {}
    
    global virtual override String getHTML()
    {
        return '';
    }
    
    public String title
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('title')) ?  getProperty('title') : '';
            return returnValue;
        }
        set;
    }
    
    public String image
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('image')) ?  getProperty('image') : '';
            return returnValue;
        }
        set;
    }
    
    public String remoteImage
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('remoteImage')) ?  getProperty('remoteImage') : '';
            return returnValue;
        }
        set;
    }
    
    
    public String URL
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('URL')) ?  getProperty('URL') : '';
            return returnValue;
        }
        set;
    }
    
    public String mobileURL
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('mobileURL')) ?  getProperty('mobileURL') : '';
            return returnValue;
        }
        set;
    }
    
    public String open
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('open')) ?  getProperty('open') : '';
            return returnValue;
        }
        set;
    }
    
    public String googleAnalytics
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('googleAnalytics')) ?  getProperty('googleAnalytics') : '';
            return returnValue;
        }
        set;
    }
    
    public String GetResourceURL(String resourceName){

        // Fetching the resource
        List<StaticResource> resourceList= [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName];

        // Checking if the result is returned or not
        if(resourceList.size() == 1){

           // Getting namespace
           String namespace = resourceList[0].NamespacePrefix;
           // Resource URL
           return '/resource/' + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName; 
        }
        else return '';
    }
    
    public String getReportCardHTML()
    {        
        String html = '';
        
        html += '<div class="col s12 m6 l4">';
        html +=     '<div class="card extra-padding reportcard">';
        html += '<div class="reportcard-inner">';
        html +=         '<div class="center-align card-content">';
        html +=             '<div class="col s12 m12 l12 imageHide" style="display:none">';
        
        if(image != '')
            html +=             '<img src="'+image+'" style="width: 100%;"/>';
        else if(remoteImage != '')
            html +=             '<img src="'+remoteImage+'" style="width: 100%;"/>';
        else
            html +=             '<img src="'+GetResourceURL('DownstreamDashboardImage')+'" style="width: 100%;"/>';
        
        html +=             '</div>';
        html +=         '</div>';
        html +=         '<div class="col s12 m12 l12" style="position:absolute;bottom:0;left:0">';
        html +=             '<div class="section">';
        
        if(mobileURL == '')
        {
            if(open == 'iframe')
            {
                html+='<a href="#" onclick="showIFrame(\''+String.escapeSingleQuotes(URL)+'\',\''+String.escapeSingleQuotes(googleAnalytics)+'\');">' + title + '</a>';
            }
            else if(open == 'samewindow')
            {
                html+='<a href="#" onclick="showInSameWindow(\''+String.escapeSingleQuotes(URL)+'\',\''+String.escapeSingleQuotes(googleAnalytics)+'\');">' + title + '</a>';
            }
            else
            {
                html+='<a href="#" onlick="showInNewWindow(\''+String.escapeSingleQuotes(URL)+'\',\''+String.escapeSingleQuotes(googleAnalytics)+'\');">' + title + '</a>';
            }
        }
        else
        {
            if(open == 'iframe')
            {
                html+='<a href="#" onclick="if($jq110.browser.mobile){window.open(\'' + mobileURL + '\');}else{showIFrame(\''+String.escapeSingleQuotes(URL)+'\',\''+String.escapeSingleQuotes(googleAnalytics)+'\');}">' + title + '</a>';
            }
            else if(open == 'samewindow')
            {
                html+='<a href="#" onclick="if($jq110.browser.mobile){window.open(\'' + mobileURL + '\');}else{showInSameWindow(\''+String.escapeSingleQuotes(URL)+'\',\''+String.escapeSingleQuotes(googleAnalytics)+'\');}">' + title + '</a>';
            }
            else
            {
                html+='<a href="#" onlick="if($jq110.browser.mobile){window.open(\'' + mobileURL + '\');}else{showInNewWindow(\''+String.escapeSingleQuotes(URL)+'\',\''+String.escapeSingleQuotes(googleAnalytics)+'\');}">' + title + '</a>';
            }
        }

        html +=             '</div>';
        html +=         '</div>';
        html +=         '</div>';
        html +=     '</div>';
        html += '</div>';
        
        return html;
    }
}