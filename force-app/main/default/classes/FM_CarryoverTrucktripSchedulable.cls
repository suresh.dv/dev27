global class FM_CarryoverTrucktripSchedulable implements Schedulable {
	public static String cronString = '0 10 0 ? * MON-SUN';

	public FM_CarryoverTrucktripSchedulable() {

	}

	global void execute(SchedulableContext sc) {
		FM_CarryoverTrucktripBatch carryoverUpdateBatch = new FM_CarryoverTrucktripBatch();
		Database.executeBatch(carryoverUpdateBatch, 200);
	}

	global static String scheduleJob() {
		System.debug('Scheduling FM_CarryoverTrucktripBatch ...');
		FM_CarryoverTrucktripSchedulable carryoverSchedulable = new FM_CarryoverTrucktripSchedulable();
		String jobId = System.schedule(((Test.isRunningTest()) ? 'Test ' : '')
			+ 'HOG - Fluid Management Carryover Truck Trips Update', cronString, carryoverSchedulable);
		return jobId;
	}
}