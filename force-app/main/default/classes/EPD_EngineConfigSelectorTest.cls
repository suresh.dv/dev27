/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EngineConfigSelector
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EngineConfigSelectorTest {

    @IsTest
    static void getEngineConfigs() {
        Integer configCount = [SELECT COUNT() FROM EPD_Engine__mdt];

        Test.startTest();
        List<EPD_Engine__mdt> engineConfigs = new EPD_EngineConfigSelector().getEngineConfigs();
        Test.stopTest();

        System.assertNotEquals(null, engineConfigs);
        System.assertEquals(configCount, engineConfigs.size());
    }

    @IsTest
    static void getEngineConfig_withoutParams() {
        Integer configCount = 0;

        Test.startTest();
        List<EPD_Engine__mdt> engineConfigs = new EPD_EngineConfigSelector().getEngineConfig(null, null);
        Test.stopTest();

        System.assertNotEquals(null, engineConfigs);
        System.assertEquals(configCount, engineConfigs.size());
    }

    @IsTest
    static void getEngineConfig_withWrongParam1() {
        Integer configCount = 0;

        Test.startTest();
        List<EPD_Engine__mdt> engineConfigs = new EPD_EngineConfigSelector().getEngineConfig('WrongManufacturer', 'UnitTestData');
        Test.stopTest();

        System.assertNotEquals(null, engineConfigs);
        System.assertEquals(configCount, engineConfigs.size());
    }

    @IsTest
    static void getEngineConfig_withWrongParam2() {
        Integer configCount = 0;

        Test.startTest();
        List<EPD_Engine__mdt> engineConfigs = new EPD_EngineConfigSelector().getEngineConfig('UnitTestData', 'WrongModel');
        Test.stopTest();

        System.assertNotEquals(null, engineConfigs);
        System.assertEquals(configCount, engineConfigs.size());
    }

    @IsTest
    static void getEngineConfig_withProperParams() {
        Integer configCount = 1;

        Test.startTest();
        List<EPD_Engine__mdt> engineConfigs = new EPD_EngineConfigSelector().getEngineConfig('UnitTestData', 'UnitTestData');
        Test.stopTest();

        System.assertNotEquals(null, engineConfigs);
        System.assertEquals(configCount, engineConfigs.size());
    }

}