/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint for Lightning Experience components providing access to Utility classes
                and constants.
                !WARNING! This endpoint is used in Community => without sharing
Test Class:     VTT_Utility_EndpointTest
History:        jschn 04/09/2019 - Created.
*************************************************************************************************/
public without sharing class VTT_Utility_Endpoint {

    /**
     * Gets tradesman info, which hold flags and other information about current user.
     * To see which flags are provided, check VTT_TradesmanFlags class
     *
     * @return VTT_TradesmanFlags
     */
    @AuraEnabled
    public static VTT_TradesmanFlags getTradesmanInfo() {
        return new VTT_Utility_EndpointHandler().getTradesmanInfo();
    }

}