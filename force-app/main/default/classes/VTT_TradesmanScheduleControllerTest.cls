/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VTT_TradesmanScheduleControllerTest {
	
	@isTest static void Test_Controller_AsAdminUser() {  

        User runningUser = VTT_TestData.createVTTAdminUser();
        System.AssertNotEquals(runningUser.Id, Null);

        System.runAs(runningUser) {
        	MaintenanceServicingUtilities.executeTriggerCode = false;
        	VTT_TestData.SetupRelatedTestData(false);

			Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
			Account vendor2 = VTT_TestData.createVendorAccount('Vendor2');

			Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);
			Contact tradesman2 = VTT_TestData.createTradesmanContact('Brad', 'Pitt',  vendor1.id);				

			HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';       
            workOrder1.Work_Order_Priority_Number__c  = '1';    
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;
			update workOrder1;
			 

	        List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 5);
	        //fine tuning of the one activity  to be able to filter it
	        Work_Order_Activity__c woActivity = activityList1[0];
	        woActivity.Scheduled_Start_Date__c = System.now();
	        update woActivity;

	        Work_Order_Activity__c woActivityCopy = VTT_TestData.reloadWorkOrderActivity(woActivity.id);

		    Test.startTest();
	        

			System.AssertEquals(true, VTT_Utilities.IsAdminUser());
			System.assertEquals(5, activityList1.size());			

            PageReference pageRef = Page.VTT_TradesmanCalendar;
            Test.setCurrentPageReference(pageRef);	


			//create some timelog data
		    ApexPages.StandardController stdController = new ApexPages.StandardController(woActivityCopy);
            VTT_ActivityDetailTimeLogControllerX controller1 = new VTT_ActivityDetailTimeLogControllerX(stdController);			
			VTT_WorkFlowEngine workflowEngine = controller1.workflowEngine;			
            workflowEngine.GenericActivity_Start1();
			workflowEngine.ActionComment = 'Starting Job';
            workflowEngine.GenericActivity_End();
            workflowEngine.GenericActivity_Start1();
			workflowEngine.ActionComment = 'Starting At Site 1';
            workflowEngine.GenericActivity_End();            





            VTT_TradesmanScheduleController controller = new VTT_TradesmanScheduleController();

            System.AssertEquals(vendor1.id, controller.AccountID); 
            System.AssertEquals(2, controller.VendorAccoountOptions.size());  
            System.AssertEquals(2, controller.TradesmenOptions.size());              
            controller.TradesmanID = tradesman1.id;
            controller.ReloadEvents();
            System.AssertEquals(1, controller.events.size());


			controller.AccountID = vendor2.id;
			controller.LoadTradesmanList();
			//no tradesmen for second vendor
            System.AssertEquals(0, controller.TradesmenOptions.size());

            System.AssertEquals(null, controller.TradesmanID);

			Datetime currentDatetime = Datetime.now();
			String currentDatetimeStr = currentDatetime.format('yyyy-MM-dd');            
            controller.CurrentDate = currentDatetimeStr;
            System.AssertEquals(null, controller.ReloadLogEntries()); 
            System.AssertEquals(0, controller.LogEntryList.size());     

            System.AssertNotEquals(null, controller.PageReload());     
            
            //runningUser should have Work_Order_Activity__c woActivity as Current_Work_Order_Activity__c
            //so when he click on Job In Progress button he should be taken to detail page of this activity
            PageReference pagere = new PageReference('/apex/VTT_ActivityDetailTimeLog?id=' + woActivity.Id);
			System.assertEquals(pagere.getUrl(), controller.JobInProgress().getUrl());
        }
	    /////////////////
	    //* STOP TEST *//
	    /////////////////
	    Test.stopTest();    		


	}

	@isTest static void Test_Controller_AsVendorSupervisorUser() {

        User runningUser = VTT_TestData.createVendorSupervisorUser();
        System.AssertNotEquals(runningUser.Id, Null);

        System.runAs(runningUser) {
        	MaintenanceServicingUtilities.executeTriggerCode = false;  
			VTT_TestData.SetupRelatedTestData(false);

			Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
			Account vendor2 = VTT_TestData.createVendorAccount('Vendor2');

			Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);
			Contact tradesman2 = VTT_TestData.createTradesmanContact('Brad', 'Pitt',  vendor1.id);				
 		
			HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
			workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';       
            workOrder1.Work_Order_Priority_Number__c  = '1';    
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;
			update workOrder1;
			 

	        List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 5);
	        //fine tuning of the one activity  to be able to filter it //
	        Work_Order_Activity__c woActivity1 = activityList1[0];
	        woActivity1.Scheduled_Start_Date__c = System.now();
	        update woActivity1;

	        Work_Order_Activity__c woActivity2 = activityList1[1];
	        woActivity2.Scheduled_Start_Date__c = System.now();
	        update woActivity2;

	        Work_Order_Activity__c woActivityCopy = VTT_TestData.reloadWorkOrderActivity(woActivity1.id);

		    
	        

			System.AssertEquals(true, VTT_Utilities.IsVendorSupervisor());
			System.assertEquals(5, activityList1.size());			

            PageReference pageRef = Page.VTT_TradesmanCalendar;
            Test.setCurrentPageReference(pageRef);	

            VTT_Utilities.executeTriggerCode = false;
            VendorPortalUtility.portalExecuteTriggerCode = false;
			//create some timelog data
		    ApexPages.StandardController stdController = new ApexPages.StandardController(woActivityCopy);
            VTT_ActivityDetailTimeLogControllerX controller1 = new VTT_ActivityDetailTimeLogControllerX(stdController);			
			VTT_WorkFlowEngine workflowEngine = controller1.workflowEngine;			
            workflowEngine.GenericActivity_Start1();
			workflowEngine.ActionComment = 'Starting Job';
            workflowEngine.GenericActivity_End();

            Test.startTest();
			
            //reload controllers
            woActivityCopy = VTT_TestData.reloadWorkOrderActivity(woActivityCopy.id);
			stdController = new ApexPages.StandardController(woActivityCopy);
			controller1 = new VTT_ActivityDetailTimeLogControllerX(stdController);			
			workflowEngine = controller1.workflowEngine;

            workflowEngine.GenericActivity_Start1();
			workflowEngine.ActionComment = 'Starting At Site 1';
            workflowEngine.GenericActivity_End();

            VTT_TradesmanScheduleController controller = new VTT_TradesmanScheduleController();

            System.AssertEquals(vendor1.id, controller.AccountID); 
            System.AssertEquals(tradesman1.id, controller.TradesmanID); 
            System.AssertEquals(null, controller.VendorAccoountOptions);  
            System.AssertEquals(2, controller.TradesmenOptions.size());              
            controller.ReloadEvents();
            System.AssertEquals(2, controller.events.size());

			Datetime currentDatetime = Datetime.now();
			String currentDatetimeStr = currentDatetime.format('yyyy-MM-dd');            
            controller.CurrentDate = currentDatetimeStr;
            System.AssertEquals(null, controller.ReloadLogEntries()); 
            System.AssertEquals(2, controller.LogEntryList.size());

            /////////////////
		    //* STOP TEST *//
		    /////////////////
		    Test.stopTest();

			//	lets update TimeStamp__c on the first LogEntry so we get some hours
			system.debug(controller.LogEntryList);
			Work_Order_Activity_Log_Entry__c logEntryItem = controller.LogEntryList[0];
			logEntryItem.TimeStamp__c = logEntryItem.TimeStamp__c.addHours(-1);
			update logEntryItem;
			controller.ReloadLogEntries();

			system.debug('*****************controller.LogEntryList****************');
			system.debug(controller.LogEntryList);


			System.AssertEquals(1, controller.WorkOrderSummaryList.size()); 

            System.AssertNotEquals(null, controller.PageReload());     
        }
	}
	
	    //test Stale & On Hold Activities worklogs as a Tradesman
		@isTest 
		static void Test_onHoldAndStaleActivity() {

        User runningUser = VTT_TestData.createVendorSupervisorUser();
        System.AssertNotEquals(runningUser.Id, Null);

        System.runAs(runningUser) {
        	MaintenanceServicingUtilities.executeTriggerCode = false; 
        	VTT_Utilities.executeTriggerCode = false;
			VTT_TestData.SetupRelatedTestData(false);

			Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
			Account vendor2 = VTT_TestData.createVendorAccount('Vendor2');

			Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);
			Contact tradesman2 = VTT_TestData.createTradesmanContact('Brad', 'Pitt',  vendor1.id);				
 		
			HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
			
			//Create orders and populate differnt flocs
			HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestDataFactory.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id, false);
			workOrder1.Well_Event__c = LocationTestData.createEvent().Id;
			
			HOG_Maintenance_Servicing_Form__c workOrder2 = VTT_TestDataFactory.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id, false);
			workOrder2.Yard__c = LocationTestData.createYard('Test Yard').Id;
			
			HOG_Maintenance_Servicing_Form__c workOrder3 = VTT_TestDataFactory.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id, false);
            workOrder3.Functional_Equipment_Level__c = LocationTestData.createFEL('Test FEL').Id;
			
			HOG_Maintenance_Servicing_Form__c workOrder4 = VTT_TestDataFactory.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id, false);
            workOrder4.Sub_System__c = LocationTestData.createSubSystem('Test SubSystem').Id;
			
			HOG_Maintenance_Servicing_Form__c workOrder5 = VTT_TestDataFactory.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id, false);
			workOrder5.System__c = LocationTestData.createSystem('Test System').Id;
			
			HOG_Maintenance_Servicing_Form__c workOrder6 = VTT_TestDataFactory.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id, false);
			workOrder6.Location__c = VTT_TestData.location.Id;
			
			HOG_Maintenance_Servicing_Form__c workOrder7 = VTT_TestDataFactory.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id, false);
			Facility__c facility = FacilityTestData.createFacility('Test facility', VTT_TestData.route.Id, VTT_TestData.field.Id);
			insert facility;
			workOrder7.Facility__c = facility.Id;

			List <HOG_Maintenance_Servicing_Form__c> woList = new List<HOG_Maintenance_Servicing_Form__c>{workOrder1, workOrder2, workOrder3, workOrder4, workOrder5, workOrder6, workOrder7};
			insert woList;		
            
            Integer numberOfActivitiesPerOrder = 3;
	        List<Work_Order_Activity__c> activityList = VTT_TestDataFactory.createWorkOrderActivitiesForMultipleWOs(woList,numberOfActivitiesPerOrder,true);
	        
            for (Work_Order_Activity__c a : activityList){
                Work_Order_Activity_Assignment__c woaa = new Work_Order_Activity_Assignment__c();
                woaa = VTT_TestData.createWorkOrderActivityAssignment(a.Id,tradesman1.ID);
                a.Scheduled_Start_Date__c = System.now().addDays(1);
                System.debug('dbg_inside a.Scheduled_Start_Date__c ' + a.Scheduled_Start_Date__c);
            }
            update activityList;

            List<Work_Order_Activity_Log_Entry__c> LogEntryList = new List<Work_Order_Activity_Log_Entry__c>();
            //Check for correct flocks
            Integer activityListSize = activityList.Size();
            for (Integer i = 0; i < activityListSize; i=i+numberOfActivitiesPerOrder){
            	//Create Logs and Log Entries for activities on different flocs
                LogEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                	activityList[i], VTT_Utilities.LOGENTRY_STARTJOB, null, 
                	null, null, 'Start Job'));
                LogEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                	activityList[i], VTT_Utilities.LOGENTRY_STARTATSITE, null,
                	null, null, 'Start at Equipment'));
                LogEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                	activityList[i], VTT_Utilities.LOGENTRY_JOBONHOLD, null,
                	null, null, 'On Hold'));

                activityList[i].Status__c = VTT_Utilities.ACTIVITY_STATUS_ONHOLD;
        		//check if all flocs were tested
	            if(i==0 && workOrder1.Id == activityList[i].Maintenance_Work_Order__c){
            		System.AssertEquals(workOrder1.Well_Event__c, activityList[i].Well_Event__c);
            	} else if(i==numberOfActivitiesPerOrder && workOrder2.Id == activityList[i].Maintenance_Work_Order__c) {
            		System.AssertEquals(workOrder2.Yard__c, activityList[i].Yard__c);
            	} else if(i==2*numberOfActivitiesPerOrder && workOrder3.Id == activityList[i].Maintenance_Work_Order__c) {
            		System.AssertEquals(workOrder3.Functional_Equipment_Level__c, activityList[i].Functional_Equipment_Level__c);
            	} else if(i==3*numberOfActivitiesPerOrder && workOrder4.Id == activityList[i].Maintenance_Work_Order__c) {
            		System.AssertEquals(workOrder4.Sub_System__c, activityList[i].Sub_System__c);
            	} else if(i==4*numberOfActivitiesPerOrder && workOrder5.Id == activityList[i].Maintenance_Work_Order__c) {
            		System.AssertEquals(workOrder5.System__c, activityList[i].System__c);
            	} else if(i==5*numberOfActivitiesPerOrder && workOrder6.Id == activityList[i].Maintenance_Work_Order__c) {
            		System.AssertEquals(workOrder6.Location__c, activityList[i].Location__c);
            	} else if(i==6*numberOfActivitiesPerOrder && workOrder7.Id == activityList[i].Maintenance_Work_Order__c) {
            		System.AssertEquals(workOrder7.Facility__c, activityList[i].Facility__c);
            	}	
	        }
        	
        	insert logEntryList;
   			update activityList;
	       	System.AssertEquals(21, activityList.size());
	       
        
	        //second activity (activityList[1]) on workOrder1
            ApexPages.StandardController stdController = new ApexPages.StandardController(activityList[1]);
            VTT_ActivityDetailTimeLogControllerX controller = new VTT_ActivityDetailTimeLogControllerX(stdController);			
			VTT_WorkFlowEngine workflowEngine = controller.workflowEngine;	

			//Start working on Activity the second activity (activityList[1]) on workOrder1
   			workflowEngine.GenericActivity_Start1();
			workflowEngine.ActionComment = 'Starting Job test';
   			workflowEngine.GenericActivity_End();

            //Create On Hold Activity
   			workflowEngine.ActivityOnHold_Start();
			workflowEngine.ActionComment = 'Putting Activity On Hold';
   			workflowEngine.ActivityOnHold_End();

            
            PageReference pageRef = Page.VTT_TradesmanCalendar;
            Test.setCurrentPageReference(pageRef);	

            //VTT_Utilities.executeTriggerCode = false;
            VendorPortalUtility.portalExecuteTriggerCode = false;
			//create some timelog data
			
			System.debug('dbg_activityList.Size()_' + activityList.Size());

			Test.startTest();

			//Reload controllers
   			Work_Order_Activity__c woActivityCopy = VTT_TestData.reloadWorkOrderActivity(activityList[1].Id);
			ApexPages.StandardController stdController1 = new ApexPages.StandardController(woActivityCopy);
			VTT_ActivityDetailTimeLogControllerX controller1 = new VTT_ActivityDetailTimeLogControllerX(stdController1);			
			VTT_WorkFlowEngine workflowEngine1 = controller1.workflowEngine;

            VTT_TradesmanScheduleController tradesmanScheduleController = new VTT_TradesmanScheduleController();
            
            System.AssertEquals(true,tradesmanScheduleController.ShowOnHoldActivities);
            System.AssertEquals('/apex/VTT_ActivityListView?filter=%7B%22vendorFilter%22%3A%22' + tradesmanScheduleController.AccountID +
                                '%22%2C%22tradesmanFilter%22%3A%22' + tradesmanScheduleController.TradesmanID +
                                '%22%2C%22hideONHOLDFilter%22%3Afalse%2C%22hideCompletedFilter%22%3Atrue%2C%22activityMultiStatusFilter%22%3A%5B%22On+Hold%22%5D%7D',tradesmanScheduleController.onHoldActivitiesURLString);
            System.AssertEquals(vendor1.id, tradesmanScheduleController.AccountID); 
            System.AssertEquals(tradesman1.id, tradesmanScheduleController.TradesmanID); 
            System.AssertEquals(null, tradesmanScheduleController.VendorAccoountOptions);  
            System.AssertEquals(2, tradesmanScheduleController.TradesmenOptions.size());              
            tradesmanScheduleController.ReloadEvents();
            System.AssertEquals(21, tradesmanScheduleController.events.size());

			Datetime currentDatetime = Datetime.now();
			String currentDatetimeStr = currentDatetime.format('yyyy-MM-dd');            
		    tradesmanScheduleController.CurrentDate = currentDatetimeStr;
		    System.debug('tradesmanScheduleController.LogEntryList.size(): '+tradesmanScheduleController.LogEntryList.size());
		    System.AssertEquals(null, tradesmanScheduleController.ReloadLogEntries()); 
		    System.AssertEquals(21, tradesmanScheduleController.LogEntryList.size()); //7 activities with 3 log etnries each = 21
            
            /////////////////
		    //* STOP TEST *//
		    /////////////////
		    Test.stopTest();

			//create Stale Asctivity
			//Create Logs for activities with different flocs
			List<Work_Order_Activity_Log__c> listOfWorkLogs = new List<Work_Order_Activity_Log__c>();
			for (Integer i = 0; i < activityList.Size(); i=i+numberOfActivitiesPerOrder){
				Work_Order_Activity_Log__c workLog  = [SELECT Id, Work_Order_Activity__c, Started_new__c FROM Work_Order_Activity_Log__c WHERE Work_Order_Activity__c =: activityList[i].Id];
				workLog.Started_new__c = Datetime.now().addDays(-2);
				listOfWorkLogs.add(workLog);
			}
			update listOfWorkLogs;

			Work_Order_Activity_Log_Entry__c logEntryItem = tradesmanScheduleController.LogEntryList[0];
			logEntryItem.TimeStamp__c = logEntryItem.TimeStamp__c.addHours(-1);
			update logEntryItem;
			tradesmanScheduleController.ReloadLogEntries();
			tradesmanScheduleController.ReloadEvents();
            
			System.AssertEquals(7, tradesmanScheduleController.WorkOrderSummaryList.size()); 
			System.AssertEquals(7, tradesmanScheduleController.onHoldActivityList.size()); 
   			System.AssertNotEquals(null, tradesmanScheduleController.PageReload()); 
            
        }
	}
}