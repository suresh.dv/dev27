@isTest
public class ATSForecastControllerOppTest 
{
    //lists
    static list<Opportunity> lstOpp = new list<Opportunity>();
    static list<OpportunityLineItem> lstOppLI = new list<OpportunityLineItem>(); 
    static list<cpm_Product_Scheduling__c> lstPS = new list<cpm_Product_Scheduling__c>();
    static list<cpm_Product_Scheduling__c> lstPSNegative = new list<cpm_Product_Scheduling__c>();
    static list<cpm_Product_Scheduling_Values__c> lstPSVal = new list<cpm_Product_Scheduling_Values__c>();
    static list<cpm_Supplier__c> lstSupp = new list<cpm_Supplier__c>();
    
    //objects
    static Account acc;
    static Product2 prod;
    static Opportunity opp1;
    static Opportunity opp2;
    static PriceBookEntry customPrice;
    static OpportunityLineItem oppLI1;
    static OpportunityLineItem oppLI2;
    static OpportunityLineItem oppLI3;
    static OpportunityLineItem oppLI4;
    static cpm_Product_Scheduling__c ps1;
    static cpm_Product_Scheduling__c ps2;
    static cpm_Product_Scheduling__c ps3;
    static cpm_Product_Scheduling__c ps4;
    static cpm_Product_Scheduling__c ps5;
    static cpm_Product_Scheduling_Values__c psVal1;
    static cpm_Product_Scheduling_Values__c psVal2;
    static cpm_Product_Scheduling_Values__c psVal3;
    static cpm_Product_Scheduling_Values__c psVal4;
    static cpm_Supplier__c supp1;
    static cpm_Supplier__c supp2;
    static Cpm_Customer_Opportunity__c cust;
    
    //variables
    static Id pricebookId = Test.getStandardPricebookId();
       
    private static void dataCreation(String prodcutFamily)
    {
        //CreateUser();
        insert account();
        insert opportunities();
        insert pricebook2();
        insert product(prodcutFamily);
        insert pricebookEntry();
        insert suppliers();
        insert opportunityLineItems();
        insert productSchedulings();
        insert productSchedulingValues();
        insert customer();
	}
    
    private static Account account()
    {
        //Create account
        acc = new Account(
            Name = 'testAcc',
            BillingStreet = 'testStreet',
            BillingCity = 'tectcity',
            BillingState = 'testState',
            BillingPostalCode = '123',
            BillingCountry = 'testcountry',
            Description = 'testdesc'
        );      
        return acc;
	}
    
    private static List<Opportunity> opportunities()
    {
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
        //Create opportunity 1
        opp1 = new Opportunity(
            AccountId = acc.id,
            OwnerId = UserInfo.getUserId(),
            Name = 'testOpp1',
            CloseDate = System.Today(),
            StageName = 'Closed Won',
            cpm_Bid_Due_Date_Time__c = System.Today(),
            cpm_Country__c = 'Canada',
            cpm_For_Season__c = '2018',
            cpm_Freight_Due_Date_Time__c = System.Today(),
            cpm_Trade_Class__c = 'Emulsion',
            //record type 'Asphalt'
            recordTypeId = recordTypeId
        ); 
        
        //Create opportunity 2
        opp2 = new Opportunity(
            AccountId = acc.id,
            Name = 'testOpp2',
            CloseDate = System.Today(),
            StageName = 'Bid Initiation',
            cpm_Bid_Due_Date_Time__c = System.Today(),
            cpm_Country__c = 'Canada',
            cpm_For_Season__c = '2018',
            cpm_Freight_Due_Date_Time__c = System.Today(),
            cpm_Trade_Class__c = 'Emulsion',
            recordTypeId = recordTypeId
        ); 
        lstOpp.add(opp1);
        lstOpp.add(opp2);
        return lstOpp;
	}

    private static Pricebook2 pricebook2()
    {
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook', 
            isActive=true
        );
        return customPB;
    }  
	
	private static Product2 product(String family)
    {
        //Create Product
    	prod = new Product2(
           Name = 'Test Product', 
           Family = family
        );
        return prod;   
    }
    
    private static PricebookEntry pricebookEntry()
    {
     	//Create pricebookEntry 
        customPrice = new PricebookEntry(
         	Pricebook2Id = pricebookId, 
            Product2Id = prod.Id,
         	UnitPrice = 12000, 
            IsActive = true
        );
        return customPrice;  
    }
    
    //Create suppliers
    private static List<cpm_Supplier__c> suppliers()   
    {
        supp1 = new cpm_Supplier__c(
        	Name = 'supp1'
        );
        
        supp2 = new cpm_Supplier__c(
        	Name = 'supp2'
        );
        
        lstSupp.add(supp1);
        lstSupp.add(supp2);
        return lstSupp;
    }
    
    private static List<OpportunityLineItem> opportunityLineItems()
    {
        //Create opportunityLineItem 1
        oppLI1 = new OpportunityLineItem(
            OpportunityId = opp1.Id, 
            PricebookEntryId = customPrice.Id,
            Quantity = 1,
            UnitPrice = customPrice.UnitPrice,
            ServiceDate = System.today()
        );
        
        //getting id of supplier
   //     Id opLISupp = [SELECT cpm_Supplier_Winner_lookup__r.Id FROM OpportunityLineItem WHERE cpm_Supplier_Winner_lookup__r.Name =: 'supp1' LIMIT 1].cpm_Supplier_Winner_lookup__r.Id;
        
       // Id idSupp = opLISupp[0].cpm_Supplier_Winner_lookup__c;
        //Create opportunityLineItem 2
        
        //TO BE DELETED, JUST FOR TEST PURPOSES
        cpm_Supplier__c testsupplier = [SELECT Id from cpm_Supplier__c WHERE Name =: 'supp1' LIMIT 1];
            
        oppLI2 = new OpportunityLineItem(
            Product2Id = prod.Id,
            OpportunityId = opp1.Id, 
           // Opportunity__r.Name = 'testOpp1',
            //cpm_Product_Family__c = 'Asphalt',
            cpm_Product_Won__c = 'Yes',
            PricebookEntryId = customPrice.Id,
            Quantity = 1,
            UnitPrice = customPrice.UnitPrice,
            ServiceDate = System.today(),
            cpm_Supplier_Winner_lookup__c = lstSupp[0].Id
            //[SELECT cpm_Supplier_Winner_lookup__c FROM OpportunityLineItem WHERE cpm_Supplier_Winner_lookup__r.Name =: 'supp1' LIMIT 1].cpm_Supplier_Winner_lookup__r.Id
        );
        

        
        //Create opportunityLineItem 3
        oppLI3 = new OpportunityLineItem(
            OpportunityId = opp2.Id, 
            PricebookEntryId = customPrice.Id,
            Quantity = 1,
            UnitPrice = customPrice.UnitPrice,
            ServiceDate = System.today()
        );
        
        //Create opportunityLineItem 4
        oppLI4 = new OpportunityLineItem(
            OpportunityId = opp2.Id, 
            PricebookEntryId = customPrice.Id,
            Quantity = 1,
            UnitPrice = customPrice.UnitPrice,
            ServiceDate = System.today()
        );
        lstOppLI.add(oppLI1);
        lstOppLI.add(oppLI2);
        lstOppLI.add(oppLI3);
        lstOppLI.add(oppLI4);
        return lstOppLI;
	}
    
    private static List<cpm_Product_Scheduling__c> productSchedulings()
    {
        //create ProductScheduling
        ps1 = new cpm_Product_Scheduling__c(
            cpm_Opportunity_Line_Item_Id__c = oppLI1.Id,
            PS_Remaining_Volume__c = 1000
        );
        
        //create ProductScheduling
        ps2 = new cpm_Product_Scheduling__c(
            cpm_Opportunity_Line_Item_Id__c = oppLI1.Id,
            PS_Remaining_Volume__c = 1000
        );
        
        //create ProductScheduling
        ps3 = new cpm_Product_Scheduling__c(
            cpm_Opportunity_Line_Item_Id__c = oppLI2.Id,
            PS_Remaining_Volume__c = 1000
        );
        
        //create ProductScheduling
        ps4 = new cpm_Product_Scheduling__c(
            cpm_Opportunity_Line_Item_Id__c = oppLI3.Id,
            PS_Remaining_Volume__c = 1000
        );
        
       //create ProductScheduling
        ps5 = new cpm_Product_Scheduling__c(
            cpm_Opportunity_Line_Item_Id__c = oppLI3.Id,
            PS_Remaining_Volume__c = -1000
        );
        
        lstPSNegative.add(ps5);
        
        lstPS.add(ps1);
        lstPS.add(ps2);
        lstPS.add(ps3);
        lstPS.add(ps4);
        return lstPS;
    }
    
    private static List<cpm_Product_Scheduling_Values__c> productSchedulingValues()
    {
        //create ProductSchedulingValues 1
        psVal1 = new cpm_Product_Scheduling_Values__c(
            cpm_Product_Scheduling_Id__c = ps1.Id,
            cpm_PS_Value_April_01__c = 100,
            cpm_PS_Value_August_02__c = 200,
            cpm_PS_Year__c = '2020'
        );
        
        //create ProductSchedulingValues 2
        psVal2 = new cpm_Product_Scheduling_Values__c(
            cpm_Product_Scheduling_Id__c = ps2.Id,
            cpm_PS_Value_April_01__c = 200,
            cpm_PS_Value_August_02__c = 300,
            cpm_PS_Year__c = '2020'
        );
        
        //create ProductSchedulingValues 3
        psVal3 = new cpm_Product_Scheduling_Values__c(
            cpm_Product_Scheduling_Id__c = ps3.Id,
            cpm_PS_Value_April_01__c = 300,
            cpm_PS_Value_August_02__c = 400,
            cpm_PS_Year__c = '2020'
        );
        
        //create ProductSchedulingValues 4
        psVal4 = new cpm_Product_Scheduling_Values__c(
            cpm_Product_Scheduling_Id__c = ps4.Id,
            cpm_PS_Value_April_01__c = 400,
            cpm_PS_Value_August_02__c = 500,
            cpm_PS_Year__c = '2020'
        );
        
        lstPSVal.add(psVal1);
        lstPSVal.add(psVal2);
        lstPSVal.add(psVal3);
        lstPSVal.add(psVal4);
        return lstPSVal;
    }
    
    private static Cpm_Customer_Opportunity__c customer()
    {
        cust = new Cpm_Customer_Opportunity__c(
//        String customerName = 'customer1';              
            cpm_OpportunityLookup__c = opp1.Id,
            cpm_Won__c = true
        );

          //  Name = 'customer1',

        return cust;
	}
    
    //picklists tests start------------------------------------------------------------------------------------
    static testmethod void testPicklistProductFamily()
    {
		dataCreation('Asphalt');
        Test.StartTest();
            lstOppLI.sort();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
            
            //test product family picklist - OK
            List<SelectOption> expectedPsProductFamilyOptions = new List<SelectOption>();
            expectedPsProductFamilyOptions.add(new SelectOption('--None--', '--None--')); 
            expectedPsProductFamilyOptions.add(new SelectOption('Asphalt', 'Asphalt'));  
            expectedPsProductFamilyOptions.add(new SelectOption('Residual', 'Residual'));
            expectedPsProductFamilyOptions.add(new SelectOption('Emulsion', 'Emulsion')); 
            List<SelectOption> productFamilysList = cntrl.getProductFamilys();
            productFamilysList.sort();
            expectedPsProductFamilyOptions.sort();
       	Test.StopTest();
        System.assertEquals(expectedPsProductFamilyOptions, productFamilysList, 'Values don`t match!');
    }
    
     static testmethod void testPicklistYear()
     {
     	dataCreation('Asphalt');
        Test.StartTest();
          	Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
         
            //test years picklist - OK
            List<SelectOption> expectedPsYearsOptions = new List<SelectOption>();
            expectedPsYearsOptions.add(new SelectOption('2018', '2018')); 
            expectedPsYearsOptions.add(new SelectOption('2019', '2019')); 
            expectedPsYearsOptions.add(new SelectOption('2020', '2020'));  
            expectedPsYearsOptions.add(new SelectOption('2021', '2021'));
            expectedPsYearsOptions.add(new SelectOption('2022', '2022'));
         Test.StopTest();
         for(Integer i=0; i<expectedPsYearsOptions.size(); i++)
         {
             System.assertEquals(expectedPsYearsOptions[i], cntrl.getYears()[i], 'Values don`t match!');    
         }       
     }
    
     static testmethod void testPicklistSupplierFamily()
     { 
         dataCreation('Asphalt');
         Test.StartTest();
             Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
             ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
         
             //test suppliers picklist - OK        
             List<SelectOption> expectedSuppliersOptions = new List<SelectOption>();
             expectedSuppliersOptions.add(new SelectOption('--None--', '--None--'));
             expectedSuppliersOptions.add(new SelectOption('supp1', 'supp1'));     
         Test.StopTest();
         for(Integer i=0; i<expectedSuppliersOptions.size(); i++)
         {
             System.assertEquals(expectedSuppliersOptions[i], cntrl.getSuppliers()[i], 'Values don`t match!');    
         }
     }
    
    static testmethod void testPicklistOpportunity()
    { 
         //family 'Asphalt'
         dataCreation('Asphalt');
         Test.StartTest();
         	Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        
        	//test opportunities picklist
            List<SelectOption> expectedOpportunitiesOptions = new List<SelectOption>();
       		expectedOpportunitiesOptions.add(new SelectOption('--None--', '--None--'));
            expectedOpportunitiesOptions.add(new SelectOption('testOpp1', 'testOpp1'));
         Test.StopTest();
         for(Integer i=0; i<expectedOpportunitiesOptions.size(); i++)
         {
             System.assertEquals(expectedOpportunitiesOptions[i], cntrl.getOpp()[i], 'Values don`t match!');    
         }
     }
	//picklists tests end------------------------------------------------------------------------------------
  
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //getData main function tests
    //Opportunity id not null (opening volume scheduling from Opportunity)
    static testmethod void getDataTestFamilyNotEmptySupplierNotEmptyElseOppr()
    {
        //family 'Asphalt'
        dataCreation('Asphalt');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	cntrl.getData(null, opp1.Name, oppLI2.cpm_Product_Family__c, oppLI2.cpm_Supplier_Winner_lookup__r.Id, '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
	}
    
    static testmethod void getDataTestFamilyEmptySupplierNotEmptyOppr()
    {
        //family '--None--'
        dataCreation('--None--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	cntrl.getData(null, opp1.Name, '--None--', 'supp1', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
	}
    
    static testmethod void getDataTestFamilyNotEmptySupplierEmptyOppr()
    {
        //family 'Asphalt'
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	cntrl.getData(null, opp1.Name, '--Asphalt--', '--None--', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
	}
    
    static testmethod void getDataTestFamilyNotEmptySupplierNotEmptyOppr()
    {
        //family 'Asphalt'
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	cntrl.getData(null, opp1.Name, '--Asphalt--', 'supp1', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
	}
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Opportunity id null (opening volume scheduling from Tab)
    static testmethod void getDataTestFamilyEmptySupplierEmptyElseOpprEmpty()
    {
        //family 'Asphalt'
		dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, opp1.Name,'--Asphalt--', 'supp1', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }
    
    static testmethod void getDataTestOppNotEmptyFamilyEmptySupplierEmptyOpprEmpty()
    {
        //family 'Asphalt'
        dataCreation('--None--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, opp1.Name, '--None--', '--None--', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }
    
    static testmethod void getDataTestOppEmptyFamilyNotEmptySupplierEmptyOpprEmpty()
    {
        //family 'Asphalt'
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, '--None--', '--Asphalt--', '--None--', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }
    
    static testmethod void getDataTestOppEmptyFamilyEmptySupplierNotEmptyOpprEmpty()
    {
        //family 'Asphalt'
        dataCreation('--None--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, '--None--', '--None--', 'supp1', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }
    
    static testmethod void getDataTestOppEmptyFamilyNotEmptySupplierNotEmptyOpprEmpty()
    {
        //family 'Asphalt'
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, '--None--', '--Asphalt--', 'supp1', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }
    
    static testmethod void getDataTestOppNotEmptyFamilyEmptySupplierNotEmptyOpprEmpty()
    {
        //family 'Asphalt'
        dataCreation('--None--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, opp1.Name, '--None--', 'supp1', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }
    
    static testmethod void getDataTestOppNotEmptyFamilyNotEmptySupplierEmptyOpprEmpty()
    {
        //family 'Asphalt'
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, opp1.Name, '--Asphalt--', '--None--', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }
    
    static testmethod void getDataTestOppNotEmptyFamilyNotEmptySupplierNotEmptyOpprEmpty()
    {
        //family 'Asphalt'
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
			cntrl.oppr.Id = null;
        	cntrl.getData(null, opp1.Name, '--Asphalt--', 'supp1', '--None--', '--None--');
        Test.StopTest();
        System.assert(!cntrl.oppLineItems.isEmpty());
    }   
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    static testmethod void addPsvaluesPerYearTest()
    {
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	Decimal psvSum = psVal3.cpm_PS_Value_April_01__c + psVal3.cpm_PS_Value_August_02__c;
        	Decimal psvResult = cntrl.addPsvaluesPerYear(psVal3);
        Test.StopTest();
        System.assertEquals(psvSum, psvResult, 'Values don`t match!');
    }   
    
    static testmethod void productScheduleValuesPerYear()
    {
        dataCreation('--Asphalt--');
        Test.StartTest();
            Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	Decimal psvSum = psVal3.cpm_PS_Value_April_01__c + psVal3.cpm_PS_Value_August_02__c;
        	Decimal psvResult = cntrl.addPsvaluesPerYear(psVal3);
        System.debug('psVal3 :' +psVal3);
        System.debug('psvSum :' +psvSum);
        System.debug('psvResult :' +psvResult);
        Test.StopTest();
        System.assertEquals(psvSum, psvResult, 'Values don`t match!');
    }   
    
    static testmethod void productSchedulingWrapperTest()
    {
        dataCreation('--Asphalt--');
        Test.StartTest();
      	ATSForecastControllerOpp.productSchedulingWrapper wrapper = new ATSForecastControllerOpp.productSchedulingWrapper(oppLI2, ps3, psVal3, 0,0, 'customer1');
        Test.StopTest();
        System.assertEquals(oppLI2, wrapper.oppLineItem, 'Values don`t match!');
        System.assertEquals(ps3, wrapper.productScheduling, 'Values don`t match!');
    }
    
    static testmethod void remainingVolumeValidationTestPositive()
    {
        dataCreation('--Asphalt--');
        Boolean result = false;
        Test.StartTest();
        	Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	if(ps4.PS_Remaining_Volume__c < 0)
            {
                result = true;
			}
        	//ATSForecastControllerOpp.productSchedulingWrapper wrapper = new ATSForecastControllerOpp.productSchedulingWrapper(oppLI2, ps3, psVal3, 0, cust);
        Test.StopTest();
        System.assertEquals(result, cntrl.remainingVolumeValidation(lstPS), 'Values don`t match!');
    }
    
    static testmethod void remainingVolumeValidationTestNegative()
    {
        dataCreation('--Asphalt--');
        Boolean result = false;
        Test.StartTest();
        	Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	if(ps5.PS_Remaining_Volume__c < 0)
            {
                result = true;
			}
        	//ATSForecastControllerOpp.productSchedulingWrapper wrapper = new ATSForecastControllerOpp.productSchedulingWrapper(oppLI2, ps3, psVal3, 0, cust);
        Test.StopTest();
        System.assertEquals(result, cntrl.remainingVolumeValidation(lstPSNegative), 'Values don`t match!');
    }
    
    static testmethod void picklistFiltersChangeTest()
    {
        dataCreation('--Asphalt--');
        Test.StartTest();
        	Apexpages.StandardController sc = new Apexpages.StandardController(opp1);
            ATSForecastControllerOpp cntrl = new ATSForecastControllerOpp(sc);
        	cntrl.productScheduleValuesPerYear();
            cntrl.opportunityChange();
        	cntrl.productFamilyChange();
        	cntrl.supplierChange();	
	        cntrl.productChange();
        	cntrl.customerChange();
        	//ATSForecastControllerOpp.productSchedulingWrapper wrapper = new ATSForecastControllerOpp.productSchedulingWrapper(oppLI2, ps3, psVal3, 0, cust);
        Test.StopTest();
    }
}    
    

  /*  static void createUser()
    {
        User myUser = new User();
    }*/
    


	//Create SPCC User
/*	private static User createUser() {

		User user = new User();

		Profile p = [SELECT Id FROM Profile WHERE Name = :SPCC_Utilities.SPCC_USER_PROFILE];

		Double random = Math.Random();

		user.email = 'SPCCUser' + random + '@spcc.com';
		user.Alias = 'SPCCUser' ;
		user.EmailEncodingKey = 'UTF-8';
		user.LastName = 'User';
		user.LanguageLocaleKey = 'en_US';
		user.LocaleSidKey = 'en_CA';
		user.ProfileId = p.Id;
		user.TimeZoneSidKey = 'America/Denver';
		user.UserName = 'SPCCUser' + random + '@SPCC.com.unittest';

		insert user;

		return user;
	}*/