@isTest
private class DSP_CMS_CustomContentTypes_Test
{
    public class TestContentHelper
    {
        public TestContentHelper(){}
        
        public List<cms__Attribute__c> content_properties {get; set;}
        public cms.CreateContentController cc {get; set;}
        public cms__Sites__c theSite {get; set;}
        
        public void createContext() {
            // Setup context; required as of OrchestraCMS 6.11
            Map<String, String> contextProperties = new Map<String, String>{'runtime' => 'Intranet', 'page_mode' => 'prod'};
            theSite = cms.TestExtensionFixtures.InitiateTest('Husky Downstream Portal', contextProperties);
        }
        
        public Id createContent(String contentName, List<Map<String, String>> attr_map)
        {
            Map<String, SObject> objs = createContentAsObject(contentName, attr_map);
            SObject content = objs.get('content');
            return content.Id;
        }
        
        public Map<String, SObject> createContentAsObject(String contentName, List<Map<String, String>> attr_map)
        {
            String site_name = 'TestSite';
            String type_name = 'TestType';
            String page_name = 'TestPage';
    
            // get the default language info
            Id defaultLang = theSite.cms__Default_Language__c;
            cms__Language__c theLanguage = [SELECT id, cms__Language_Code__c FROM cms__Language__c Where Id = :defaultLang LIMIT 1];
    
            cms__Content_Type__c ct = new cms__Content_Type__c(cms__Name__c = type_name, cms__Site_Name__c = site_name);
            insert ct;
    
            cms__Content__c txc = new cms__Content__c(
                    cms__Content_Type__c         = ct.Id,
                    cms__Name__c                 = contentName,
                    cms__Description__c          = 'Testing',
                    cms__Preview__c              = true,
                    cms__Published__c            = false,
                    cms__Published_Start_Date__c = System.now(),
                    cms__Site_Name__c            = site_name,
                    cms__Revision_Number__c      = 0,
                    cms__Revision_Origin__c      = null,
                    cms__Version_Number__c       = 1,
                    cms__Version_Origin__c       = null,
                    cms__Version_Original__c     = true,
                    cms__Version_Parent__c       = null,
                    cms__Depth__c                = 0
            );
            insert txc;
    
            cms__Content_Layout__c cl = new cms__Content_Layout__c(cms__Name__c = 'TestLayout');
            insert cl;
    
            cms__Page__c page = new cms__Page__c(cms__Name__c = page_name, cms__Site_Name__c = site_name);
            insert page;
            String PAGE_ID = page.Id;
    
            cms__Content_Layout_Instance__c cli = new cms__Content_Layout_Instance__c(cms__Content__c = txc.Id, cms__Content_Layout__c = cl.Id);
            insert cli;
    
            cms__Page_Content_Layout_Instance__c pcli = new cms__Page_Content_Layout_Instance__c(cms__Content_Layout_Instance__c=cli.Id,cms__Page__c=page.Id);
            insert pcli;
    
            this.content_properties = new List<cms__Attribute__c>();
    
            if (attr_map.size() > 0)
            {
                for (Map<String, String> am : attr_map)
                {
                    cms__Attribute__c attr = new cms__Attribute__c();
                    attr.cms__Content__c = txc.Id;
                    attr.cms__Page_Content_Layout_Instance__c = pcli.Id;
                    attr.cms__Language__c = theLanguage.Id;
                    attr.cms__Index_Key__c = txc.id + ':' + theLanguage.Id;
    
                    String name = am.get('name');
                    String atype = am.get('type');
                    String value = am.get('value');
                    if (atype == 'Link' || atype == 'LongText')
                    {
                        attr.cms__Is_Simple__c = false;
                        attr.cms__Name__c = name;
                    }
                    else
                    {
                        attr.cms__Is_Simple__c = true;
                        attr.cms__Simple_Name__c = name;
                    }
    
                    if (atype == 'Link')
                    {
/////                        attr.cms__Value__c = 'Internal,' + PAGE_ID + ',,' + page_name + ',,,';
                        attr.cms__Value__c = value;
                    }
                    else if (atype == 'LongText')
                    {
                        attr.cms__Value__c = value;
                    }
                    else  // simple value
                    {
                        attr.cms__Simple_Value__c = value;
                    }
                    insert attr;
                    this.content_properties.add(attr);
                }
            }
    
            cms.API anAPI = new cms.API(null, 'prev');
            anAPI.site_name = site_name;
    
            Test.setCurrentPage(new PageReference('/apex/cms__CreateContent'));
    
            System.currentPageReference().getParameters().put('ecms', anAPI.getSerialize());
            System.currentPageReference().getParameters().put('content_id', txc.Id);
            System.currentPageReference().getParameters().put('cli_id', cli.Id);
            System.currentPageReference().getParameters().put('pcli_id', pcli.Id);
            
            //  Create content controller for editor constructor
            this.cc = new cms.CreateContentController();
            this.cc.content = txc;
            this.cc.content_properties = content_properties;
    
            Map<String, SObject> contentMap = new Map<String, SObject>();
            contentMap.put('content', txc);
            contentMap.put('content_layout', cl);
            contentMap.put('content_layout_instance', cli);
            contentMap.put('page_content_layout_instance', pcli);
            return contentMap;
        }
    }
    
    static testMethod void test_Aticle()
    {
        User thisUser = [SELECT Id FROM User WHERE Id =: UserInfo.getUserId()];
        System.runAs(thisUser)
        {

            DSP_Settings__c settings = DSP_Settings__c.getInstance();
            
            if(settings == null || settings.Id == null)
            {
                 settings = new DSP_Settings__c();
                 settings.Article_Detail_URL__c = '';
                 insert settings;
            }
            else
            {
                settings.Article_Detail_URL__c = '';
                update settings;
            }
        
            List<Map<String, String>> attr_map = new List<Map<String, String>>();
            attr_map.add(new Map<String, String>{'name'=>'image', 'type'=>'text', 'value'=>'image'});
            attr_map.add(new Map<String, String>{'name'=>'title', 'type'=>'text', 'value'=>'title'});
            attr_map.add(new Map<String, String>{'name'=>'tagline', 'type'=>'text', 'value'=>'tagline'});
            attr_map.add(new Map<String, String>{'name'=>'text', 'type'=>'text', 'value'=>'text'});
            attr_map.add(new Map<String, String>{'name'=>'summary', 'type'=>'text', 'value'=>'summary'});
            attr_map.add(new Map<String, String>{'name'=>'icon', 'type'=>'text', 'value'=>'icon'});
            
            TestContentHelper tch = new TestContentHelper();
            tch.createContext();
            
            Id txc = tch.createContent('TESTCONTENT', attr_map);
            
            cms.GenerateContent gc = new cms.GenerateContent();
            gc.content.cms__Published__c = true;
            gc.content.cms__Published_End_Date__c = System.now();
            
            // get static resource path
            
            List<StaticResource> resourceList = [SELECT Name, NamespacePrefix, SystemModStamp 
                                                 FROM StaticResource
                                                 WHERE Name = :'DownstreamPortalHuskyLogo'];
            String resourcePath = '';
            if(resourceList.size() == 1)
            {
               resourcePath = '/resource/'+resourceList[0].SystemModStamp.getTime() + '/'+ 'DownstreamPortalHuskyLogo';
            }
            
            DSP_CMS_ArticleController a = new DSP_CMS_ArticleController(gc);
            
            a.getHTML();
            
            DSP_CMS_ArticleDetail b = new DSP_CMS_ArticleDetail();
    
            System.assertEquals('<div class="col s12 m12 l12"><h2></h2></div><div class="col s12 m12 l12"><div class="col s12 m12 l12"><img src="'+resourcePath+'"/></div><div class="col s12 m12 l12"><p></p></div></div>',
                                b.getHTML());
            System.assertEquals('<div class="col s12 m12 l12"><h2><img src="icon"/>title</h2></div><div class="col s12 m12 l12"><div class="col s12 m12 l12"><img src="image"/></div><div class="col s12 m12 l12"><h5>tagline</h5></div><div class="col s12 m12 l12"><p>text</p></div></div>',
                                a.articleDetailHTML());
            
            DSP_CMS_ArticleSummary d = new DSP_CMS_ArticleSummary(gc);
            
            DSP_CMS_ArticleSummary d_default = new DSP_CMS_ArticleSummary();
            
            System.assertEquals('<div class="col s12 m12 l6"><div class="card"><div class="card-content"><div class="center-align col s12 m12 l12" style="height:267px;" ><img src="image" style="max-height:267px;"/></div><div class="col s12 m12 l12"><a href="'+a.content.Id+'"><h4>title</h4></a></div><div class="col s12 m12 l12"><p>summary</p></div><div class="card-date col s12 m12 l12">',
                                d.getHTML().substring(0,357));
            System.assertEquals('<div class="col s12 m12 l6"><div class="card"><div class="card-content"><div class="center-align col s12 m12 l12" style="height:267px;" ><img src="image" style="max-height:267px;"/></div><div class="col s12 m12 l12"><a href="'+a.content.Id+'"><h4>title</h4></a></div><div class="col s12 m12 l12"><p>summary</p></div><div class="card-date col s12 m12 l12">',
                                a.articleSummaryHTML().substring(0,357));
            
            DSP_CMS_ArticleTitle f = new DSP_CMS_ArticleTitle(gc);
            
            DSP_CMS_ArticleTitle f_default = new DSP_CMS_ArticleTitle();
            
            System.assertEquals('<div class="col s12 m12 l12"><div class="card inner"><div class="center-align card-content"><div class="col s12 m12 l12" style="height:267px;"><img src="image" style="max-height:267px;" /></div><div class="col s12 m12 l12"><a href="'+a.content.Id+'"><h5>title</h5></a></div><div class="left-align card-date col s12 m12 l12"><p><em>',
                                f.getHTML().substring(0,333));
            System.assertEquals('<div class="col s12 m12 l12"><div class="card inner"><div class="center-align card-content"><div class="col s12 m12 l12" style="height:267px;"><img src="image" style="max-height:267px;" /></div><div class="col s12 m12 l12"><a href="'+a.content.Id+'"><h5>title</h5></a></div><div class="left-align card-date col s12 m12 l12"><p><em>',
                                a.articleTitleHTML().substring(0,333));
            
            DSP_CMS_ArticleListItem g = new DSP_CMS_ArticleListItem(gc);
            
            DSP_CMS_ArticleListItem g_default = new DSP_CMS_ArticleListItem();
            
            System.assertEquals('<div style="clear:both;border-bottom:1px solid #DDD;padding-top:5px;padding-bottom:5px;margin-bottom:10px !important;" class="row ArticleListItem"><div style="float:left"><img src="icon" /></div><div class="col s10 m10 l10">',
                                g.getHTML().substring(0,224));
            System.assertEquals('<div style="clear:both;border-bottom:1px solid #DDD;padding-top:5px;padding-bottom:5px;margin-bottom:10px !important;" class="row ArticleListItem"><div style="float:left"><img src="icon" /></div><div class="col s10 m10 l10">',
                                a.articleListItemHTML().substring(0,224));
        }
    }
    
    static testMethod void test_CardTitleLink()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'External,,NewWindow,,,,http://google.ca'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_CardTitleLink a_default = new DSP_CMS_CardTitleLink();
        
        System.assertEquals('',
                            a_default.getHTML());
        
        DSP_CMS_CardTitleLinkController a = new DSP_CMS_CardTitleLinkController(gc);
        
        a.getHTML();

        System.assertEquals('<a href="http://google.ca"target="_blank">http://google.ca</a>',
                            a.linkHTML());

        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'Media,/path,PopupWindow,filename,100,100,'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        a = new DSP_CMS_CardTitleLinkController(gc);
        
        System.assertEquals('<a href="#" onlick=\'window.open("/servlet/servlet.FileDownload?file=/path","","location=1,status=1,scrollbars=1,height=100, width=100")\'>filename</a>',
                            a.linkHTML());
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'Internal,123456789012345,CurrentWindow,pagename,,,'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        a = new DSP_CMS_CardTitleLinkController(gc);
        
        System.assertEquals('',
                            a.linkHTML());
    }
    
    static testMethod void test_CardMore()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'External,,NewWindow,,,,http://google.ca'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_CardMore a_default = new DSP_CMS_CardMore();
        
        System.assertEquals('<a href="" class="small right">More</a>',
                            a_default.getHTML());
        
        DSP_CMS_CardMoreController a = new DSP_CMS_CardMoreController(gc);
        
        a.getHTML();

        System.assertEquals('<a href="http://google.ca"target="_blank" class="small right">More</a>',
                            a.linkHTML());
        
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'Media,/path,PopupWindow,filename,100,100,'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        a = new DSP_CMS_CardMoreController(gc);
        
        System.assertEquals('<a href="#" onlick=\'window.open("/servlet/servlet.FileDownload?file=/path","","location=1,status=1,scrollbars=1,height=100, width=100")\' class="small right">More</a>',
                            a.linkHTML());
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'Internal,123456789012345,CurrentWindow,pagename,,,'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        a = new DSP_CMS_CardMoreController(gc);
        
        System.assertEquals('<a href="/apex/Main?sname=Husky Downstream Portal&name=pagename" class="small right">More</a>',
                            a.linkHTML());
    }
    
    static testMethod void test_Metric1()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'name', 'type'=>'text', 'value'=>'name'});
        attr_map.add(new Map<String, String>{'name'=>'value', 'type'=>'text', 'value'=>'value'});
        attr_map.add(new Map<String, String>{'name'=>'colour', 'type'=>'text', 'value'=>'Red'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_Metric a_default = new DSP_CMS_Metric();
        DSP_CMS_MetricController a = new DSP_CMS_MetricController(gc);
        
        a.getHTML();
        
        System.assertEquals('<div class="center-align col s12 m12 l12 dashboardlist"><div class="center-align col s5 m5 l5"></div><div class="center-align col s4 m4 l4"></div><div class="center-align col s3 m3 l3"><div class="circle green"></div></div></div>',
                            a_default.getHTML());
        System.assertEquals('<div class="center-align col s12 m12 l12 dashboardlist"><div class="center-align col s5 m5 l5">name</div><div class="center-align col s4 m4 l4">value</div><div class="center-align col s3 m3 l3"><div class="circle red"></div></div></div>',
                            a.getMetric());
    }
    
    static testMethod void test_Metric2()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'name', 'type'=>'text', 'value'=>'name'});
        attr_map.add(new Map<String, String>{'name'=>'value', 'type'=>'text', 'value'=>'value'});
        attr_map.add(new Map<String, String>{'name'=>'colour', 'type'=>'text', 'value'=>'Yellow'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_Metric a_default = new DSP_CMS_Metric();
        DSP_CMS_MetricController a = new DSP_CMS_MetricController(gc);
        
        a.getHTML();
        
        System.assertEquals('<div class="center-align col s12 m12 l12 dashboardlist"><div class="center-align col s5 m5 l5"></div><div class="center-align col s4 m4 l4"></div><div class="center-align col s3 m3 l3"><div class="circle green"></div></div></div>',
                            a_default.getHTML());
        System.assertEquals('<div class="center-align col s12 m12 l12 dashboardlist"><div class="center-align col s5 m5 l5">name</div><div class="center-align col s4 m4 l4">value</div><div class="center-align col s3 m3 l3"><div class="circle yellow"></div></div></div>',
                            a.getMetric());
    }
    
    static testMethod void test_Glossary()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'abbreviation', 'type'=>'text', 'value'=>'abbreviation'});
        attr_map.add(new Map<String, String>{'name'=>'fullname', 'type'=>'text', 'value'=>'fullname'});
        attr_map.add(new Map<String, String>{'name'=>'definition', 'type'=>'text', 'value'=>'definition'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_Glossary a_default = new DSP_CMS_Glossary();
        DSP_CMS_GlossaryController a = new DSP_CMS_GlossaryController(gc);
        
        a.getHTML();
        
        System.assertEquals('<tr><td>abbreviation</td><td>fullname</td><td>definition</td></tr>',
                            a.glossaryHTML());
        System.assertEquals('<tr><td></td><td></td><td></td></tr>',
                            a_default.getHTML());
    }
    
    static testMethod void test_FAQ()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'question', 'type'=>'text', 'value'=>'question'});
        attr_map.add(new Map<String, String>{'name'=>'answer', 'type'=>'text', 'value'=>'answer'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_FAQ a_default = new DSP_CMS_FAQ();
        DSP_CMS_FAQController a = new DSP_CMS_FAQController(gc);
        
        a.getHTML();
        
        System.assertEquals('<li><div class="collapsible-header">Q: question</div><div style="display: none;" class="collapsible-body"><p>A: answer</p></div></li>',
                            a.FAQHTML());
        System.assertEquals('<li><div class="collapsible-header">Q: </div><div style="display: none;" class="collapsible-body"><p>A: </p></div></li>',
                            a_default.getHTML());
    }
    
    static testMethod void test_ImageCard()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'title', 'type'=>'text', 'value'=>'title'});
        attr_map.add(new Map<String, String>{'name'=>'image', 'type'=>'text', 'value'=>'image'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_ImageCard a_default = new DSP_CMS_ImageCard();
        DSP_CMS_ImageCardController a = new DSP_CMS_ImageCardController(gc);
        
        a.getHTML();
        
        System.assertEquals('<div class="col s12 m12 l12"><div class="card extra-padding"><div class="square"><div class="center-align"><i class="mdi-action-question-answer"></i></div></div><div class="card-title-icon"><span class="title-smallcaps card-title title-text">title</span></div><div class="center-align card-content"><div class="col s12 m12 l12"><img src="image" /></div><div class="col s12 m12 l12"><div class="section"><a target="_blank" href="image" class="btn full-view">View Full</a></div></div></div></div></div>',
                            a.getImageCardHTML());
        System.assertEquals('<div class="col s12 m12 l12"><div class="card extra-padding"><div class="square"><div class="center-align"><i class="mdi-action-question-answer"></i></div></div><div class="card-title-icon"><span class="title-smallcaps card-title title-text"></span></div><div class="center-align card-content"><div class="col s12 m12 l12"><img src="" /></div><div class="col s12 m12 l12"><div class="section"><a target="_blank" href="" class="btn full-view">View Full</a></div></div></div></div></div>',
                            a_default.getHTML());
    }
    
    static testMethod void test_Link()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'External,,NewWindow,,,,http://google.ca'});
        attr_map.add(new Map<String, String>{'name'=>'name', 'type'=>'text', 'value'=>'name'});
        attr_map.add(new Map<String, String>{'name'=>'linkDate', 'type'=>'text', 'value'=>'date'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_Link a_default = new DSP_CMS_Link(gc);
        
        DSP_CMS_Link c = new DSP_CMS_Link();
        
        System.assertEquals('<a href="http://google.ca"target="_blank" >name</a>',
                            a_default.getHTML());
        
        DSP_CMS_LinkController a = new DSP_CMS_LinkController(gc);
        
        a.getHTML();
        
        System.assertEquals('<a href="http://google.ca"target="_blank" >name</a>',
                            a.linkHTML());
        
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'Media,/path,PopupWindow,filename,100,100,'});
        attr_map.add(new Map<String, String>{'name'=>'name', 'type'=>'text', 'value'=>'name'});
        attr_map.add(new Map<String, String>{'name'=>'linkDate', 'type'=>'text', 'value'=>'date'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        a = new DSP_CMS_LinkController(gc);
        
        System.assertEquals('<a href="#" onlick=\'window.open("/servlet/servlet.FileDownload?file=/path","","location=1,status=1,scrollbars=1,height=100, width=100")\' >name</a>',
                            a.linkHTML());
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'Internal,123456789012345,CurrentWindow,pagename,,,'});
        attr_map.add(new Map<String, String>{'name'=>'name', 'type'=>'text', 'value'=>'name'});
        attr_map.add(new Map<String, String>{'name'=>'linkDate', 'type'=>'text', 'value'=>'date'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        a = new DSP_CMS_LinkController(gc);
        
        System.assertEquals('<a href="/apex/Main?sname=Husky Downstream Portal&name=pagename" >name</a>',
                            a.linkHTML());
        
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'website', 'type'=>'Link', 'value'=>'External,,NewWindow,,,,http://google.ca'});
        attr_map.add(new Map<String, String>{'name'=>'name', 'type'=>'text', 'value'=>'name'});
        attr_map.add(new Map<String, String>{'name'=>'linkDate', 'type'=>'text', 'value'=>'date'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_NewsLink b_default = new DSP_CMS_NewsLink(gc);
        
        b_default.getHTML();
        
        DSP_CMS_NewsLink x = new DSP_CMS_NewsLink();
        
        DSP_CMS_LinkController b = new DSP_CMS_LinkController(gc);
        
        b.getHTML();
        
        b.newsLinkHTML();
    }
    
    static testMethod void test_Contact()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'description', 'type'=>'text', 'value'=>'description'});
        attr_map.add(new Map<String, String>{'name'=>'users', 'type'=>'text', 'value'=>'{"users":[{"name":"a","role":"b","email":"c","phone":"d"}]}'});
        attr_map.add(new Map<String, String>{'name'=>'responsibleFor', 'type'=>'text', 'value'=>'{"responsibleFors":["a","b"]}'});
        attr_map.add(new Map<String, String>{'name'=>'name', 'type'=>'text', 'value'=>'name'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_Contact a_default = new DSP_CMS_Contact();
        DSP_CMS_ContactController a = new DSP_CMS_ContactController(gc);
        
        a.getHTML();
        
        System.assertEquals('<div class="col s12 m6 l6"><div class="col s12 m12 l12"><a name="null"></a><h5 class="title-smallcaps">name</h5></div><div class="col s12 m12 l12"><p>description</p></div><div class="col s12 m12 l12"><p><div>a</div><div>b</div><div><a href="mailto:c">c</a></div><div><a href="tel:+13068251664">+1-306-825-1664</a></div></p></div><div class="col s12 m12 l12"><h6>RESPONSIBLE FOR ...</h6></div><div class="col s12 m12 l6"><ul><li>a</li><li>b</li></ul></div></div>',
                            a.contactHTML());
        System.assertEquals('<div class="col s12 m6 l6"><div class="col s12 m12 l12"><a name="null"></a><h5 class="title-smallcaps"></h5></div><div class="col s12 m12 l12"><p></p></div><div class="col s12 m12 l12"><h6>RESPONSIBLE FOR ...</h6></div><div class="col s12 m12 l6"><ul></ul></div></div>',
                            a_default.getHTML());
    }
    
    static testMethod void test_PageTitle()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'title', 'type'=>'text', 'value'=>'title'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_PageTitle a_default = new DSP_CMS_PageTitle();
        DSP_CMS_PageTitleController a = new DSP_CMS_PageTitleController(gc);
        
        a.getHTML();
        
        System.assertEquals('title',
                            a.titleHTML());
        System.assertEquals(null,
                            a_default.getHTML());
    }
    
    static testMethod void test_ReportCard()
    {
        List<Map<String, String>> attr_map = new List<Map<String, String>>();
        attr_map.add(new Map<String, String>{'name'=>'title', 'type'=>'text', 'value'=>'title'});
        attr_map.add(new Map<String, String>{'name'=>'image', 'type'=>'text', 'value'=>'image'});
        attr_map.add(new Map<String, String>{'name'=>'remoteImage', 'type'=>'text', 'value'=>'remoteImage'});
        attr_map.add(new Map<String, String>{'name'=>'URL', 'type'=>'text', 'value'=>'http://google.ca'});
        attr_map.add(new Map<String, String>{'name'=>'open', 'type'=>'text', 'value'=>'iframe'});
        
        TestContentHelper tch = new TestContentHelper();
        tch.createContext();
        
        Id txc = tch.createContent('TESTCONTENT', attr_map);
        
        cms.GenerateContent gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_ReportCard a_default = new DSP_CMS_ReportCard();
        DSP_CMS_ReportCardController a = new DSP_CMS_ReportCardController(gc);
        
        a.getHTML();
        
        System.assertEquals('<div class="col s12 m6 l4"><div class="card extra-padding reportcard"><div class="reportcard-inner"><div class="center-align card-content"><div class="col s12 m12 l12 imageHide" style="display:none"><img src="image" style="width: 100%;"/></div></div><div class="col s12 m12 l12" style="position:absolute;bottom:0;left:0"><div class="section"><a href="#" onclick="showIFrame(\'http://google.ca\',\'\');">title</a></div></div></div></div></div>',
                            a.getReportCardHTML());
        
        String regex = '<div class="col s12 m6 l4"><div class="card extra-padding reportcard"><div class="reportcard-inner"><div class="center-align card-content"><div class="col s12 m12 l12 imageHide" style="display:none"><img src="/resource/[^/]*/DownstreamDashboardImage" style="width: 100%;"/></div></div><div class="col s12 m12 l12" style="position:absolute;bottom:0;left:0"><div class="section"><a href="#" onlick="showInNewWindow\\(\'\',\'\'\\);"></a></div></div></div></div></div>';
        Pattern regexPattern = Pattern.compile(regex);
        Matcher patternMatcher = regexPattern.matcher(a_default.getHTML());
        
        System.assert(patternMatcher.matches());
        
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'title', 'type'=>'text', 'value'=>'title'});
        attr_map.add(new Map<String, String>{'name'=>'remoteImage', 'type'=>'text', 'value'=>'remoteImage'});
        attr_map.add(new Map<String, String>{'name'=>'URL', 'type'=>'text', 'value'=>'http://google.ca'});
        attr_map.add(new Map<String, String>{'name'=>'open', 'type'=>'text', 'value'=>'newwindow'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_ReportCardController b = new DSP_CMS_ReportCardController(gc);
        
        System.assertEquals('<div class="col s12 m6 l4"><div class="card extra-padding reportcard"><div class="reportcard-inner"><div class="center-align card-content"><div class="col s12 m12 l12 imageHide" style="display:none"><img src="remoteImage" style="width: 100%;"/></div></div><div class="col s12 m12 l12" style="position:absolute;bottom:0;left:0"><div class="section"><a href="#" onlick="showInNewWindow(\'http://google.ca\',\'\');">title</a></div></div></div></div></div>',
                            b.getReportCardHTML());
        
        attr_map.clear();
        attr_map.add(new Map<String, String>{'name'=>'title', 'type'=>'text', 'value'=>'title'});
        attr_map.add(new Map<String, String>{'name'=>'image', 'type'=>'text', 'value'=>'image'});
        attr_map.add(new Map<String, String>{'name'=>'URL', 'type'=>'text', 'value'=>'http://google.ca'});
        attr_map.add(new Map<String, String>{'name'=>'open', 'type'=>'text', 'value'=>'samewindow'});
        
        txc = tch.createContent('TESTCONTENT', attr_map);
        
        gc = new cms.GenerateContent();
        gc.content.cms__Published__c = true;
        gc.content.cms__Published_End_Date__c = System.now();
        
        DSP_CMS_ReportCardController c = new DSP_CMS_ReportCardController(gc);
        
        System.assertEquals('<div class="col s12 m6 l4"><div class="card extra-padding reportcard"><div class="reportcard-inner"><div class="center-align card-content"><div class="col s12 m12 l12 imageHide" style="display:none"><img src="image" style="width: 100%;"/></div></div><div class="col s12 m12 l12" style="position:absolute;bottom:0;left:0"><div class="section"><a href="#" onclick="showInSameWindow(\'http://google.ca\',\'\');">title</a></div></div></div></div></div>',
                            c.getReportCardHTML());
    }
}