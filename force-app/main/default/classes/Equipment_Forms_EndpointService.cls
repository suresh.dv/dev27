/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 12/2/2019   
 */

public class Equipment_Forms_EndpointService {


	@TestVisible
	private static String deleteAttachment(String attachmentId) {
		List<Attachment> attachment = [SELECT Id FROM Attachment WHERE Id = :attachmentId];
		delete attachment;
		return null;
	}

	private static List<ContentDocumentLink> getFormFiles(String formId) {
		List<ContentDocumentLink> formFile = [
				SELECT Id, ContentDocument.Title, ContentDocumentId
				FROM ContentDocumentLink
				WHERE LinkedEntityId = :formId
		];

		if (formFile.isEmpty()) {
			return null;
		}

		return formFile;
	}

	private static List<Attachment> getFormAttachments(String formId) {
		List<Attachment> attachments = [
				SELECT Id, Name
				FROM Attachment
				WHERE ParentId = :formId
		];

		if (attachments.isEmpty()) {
			return null;
		}

		return attachments;
	}

	public static HOG_SObject_Information getFloc(String flockId) {
		Id locationId = flockId;
		SObjectType flockAPI = locationId.getSObjectType();
		String flockName = String.valueOf(flockAPI);

		String whereClause = 'Id = \'' + flockId + '\'';

		String preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				'Name',
				flockName,
				whereClause
		});

		return new HOG_SObject_Information(Database.query(preparedQuery));
	}


	/* Add Form Methods */
	public static void insertAddEquipmentForm(
			String flocId,
			Equipment_Missing_Form__c addForm,
			String serialPlateBase64Data,
			String serialPlateName,
			String attachmentBase64Data,
			String attachmentFileName) {

		Id locationId = flocId;
		SObjectType flocAPI = locationId.getSObjectType();
		addForm.put(String.valueOf(flocAPI), flocId);
		addForm.Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN;
		insert addForm;

		// inserting serial plate file
		if (serialPlateName != null) {
			addForm.Serial_plate_of_asset__c = Equipment_Forms_FileHandler.insertFile(serialPlateBase64Data, serialPlateName, addForm.Id);
		}

		// inserting attachment file
		if (attachmentFileName != null) {
			addForm.Attachment__c = Equipment_Forms_FileHandler.insertFile(attachmentBase64Data, attachmentFileName, addForm.Id);
		}

		if (serialPlateName != null || attachmentFileName != null) {
			update addForm;
		}

	}

	public static void updateAddEquipmentForm(
			Equipment_Missing_Form__c addForm,
			Object handleUploads) {

		Equipment_Missing_Form__c originalForm = Equipment_Forms_QuerySelector.getAddEquipmentForm(addForm.Id);

		if (originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_OPEN || originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {

			if (originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {
				addForm.Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN;
			}

			Equipment_AddForm_File uploadHandler = (Equipment_AddForm_File) JSON.deserialize(JSON.serialize(handleUploads), Equipment_AddForm_File.class);

			if (uploadHandler.originalSerialPlateFileId != null) {
				addForm.Serial_plate_of_asset__c = Equipment_Forms_FileHandler.deleteFile(uploadHandler.originalSerialPlateFileId);
			} else if (uploadHandler.originalSerialPlateAttachmentId != null) {
				addForm.Serial_plate_of_asset__c = deleteAttachment(uploadHandler.originalSerialPlateAttachmentId);
			}

			if (uploadHandler.insertSerialPlate) {
				addForm.Serial_plate_of_asset__c = Equipment_Forms_FileHandler.insertFile(uploadHandler.serialPlateBase64Data,
						uploadHandler.serialPlateName,
						addForm.Id);
			}

			if (uploadHandler.originalAttachmentFileId != null) {
				addForm.Attachment__c = Equipment_Forms_FileHandler.deleteFile(uploadHandler.originalAttachmentFileId);
			} else if (uploadHandler.originalAttachmentId != null) {
				addForm.Attachment__c = deleteAttachment(uploadHandler.originalAttachmentId);
			}


			if (uploadHandler.insertAttachment) {
				addForm.Attachment__c = Equipment_Forms_FileHandler.insertFile(uploadHandler.attachmentBase64Data,
						uploadHandler.attachmentFileName,
						addForm.Id);
			}

			update addForm;
		} else {
			throw new HOG_Exception(EquipmentUtilities.FORM_IS_CLOSED_MESSAGE);
		}
	}

	public static Equipment_AddForm loadAddEquipmentForm(String formId) {

		Equipment_AddForm wrapper = new Equipment_AddForm();

		wrapper.addForm = Equipment_Forms_QuerySelector.getAddEquipmentForm(formId);

		wrapper.addFormFiles = getFormFiles(formId);
		wrapper.addFormAttachments = getFormAttachments(formId);

		return wrapper;

	}

	public static void rejectAddEquipmentForm(String formId, String rejectionReason) {
		Equipment_Missing_Form__c form = Equipment_Forms_QuerySelector.getAddEquipmentForm(formId);
		form.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;
		form.Reason__c = rejectionReason;
		update form;

		EquipmentFormService addEquipmentService = new EquipmentFormService();

		EquipmentFormService.requestRejectionTemplateEmail(form.Id, form.CreatedById, new Set<String>{
				addEquipmentService.addEquipmentPlannerGroup(form)
						+ EquipmentFormService.COMBINATION_DELIMITER
						+ addEquipmentService.getFunctionalLocationForDestination(form)
		});

	}

	public static void processAddEquipmentForm(String formId) {
		update new Equipment_Missing_Form__c(Id = formId, Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED);
	}


	/* Equipment Hierarchy */
	public static List<Object> getFLOCKEquipment(String flockId) {
		List<Equipment__c> equipmentList = new List<Equipment__c>();


		System.debug('Dlock id ' + flockId);
		Id flockName = flockId;
		System.debug('API: ' + flockName.getSObjectType());
		SObjectType flockAPI = flockName.getSObjectType();

		String whereClause = flockAPI + ' = \'' + flockId + '\'';

		String preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				EquipmentUtilities.EQUIPMENT_HIERARCHY_FIELDS,
				'Equipment__c',
				whereClause
		});

		preparedQuery += ' ORDER BY Name';


		equipmentList = Database.query(preparedQuery);

		//equipmentList = Equipment_Forms_QuerySelector.getActiveAddForms(flockId);
		return buildMasterEquipment(equipmentList);
	}

	public static List<Object> buildMasterEquipment(List<Equipment__c> equipmentList) {

		Map<Id, Equipment_Hierarchy_MasterEquipment> equipmentMapEquipmentIdKey = new Map<Id, Equipment_Hierarchy_MasterEquipment>();
		Map<Id, List<Equipment_Hierarchy_MasterEquipment>> equipmentMapSuperiorLookupKey = new Map<Id, List<Equipment_Hierarchy_MasterEquipment>>();
		List<Object> mapList = new List<Object>();

		for (Equipment__c equipment : equipmentList) {

			Equipment_Hierarchy_MasterEquipment preparedEquipment = new Equipment_Hierarchy_MasterEquipment();
			preparedEquipment.equipment = equipment;

			if (equipment.Superior_Equipment__c == null) {
				preparedEquipment.isMaster = true;
			} else {
				preparedEquipment.isMaster = false;
				equipmentMapSuperiorLookupKey.put(equipment.Superior_Equipment__c, new List<Equipment_Hierarchy_MasterEquipment>{
						preparedEquipment
				});
			}

			equipmentMapEquipmentIdKey.put(equipment.Id, preparedEquipment);
		}

		System.debug('Show me equipmentMapEquipmentIdKey: ' + JSON.serialize(equipmentMapEquipmentIdKey));
		System.debug('Show me equipmentMapSuperiorLookupKey: ' + JSON.serialize(equipmentMapSuperiorLookupKey));

		for (Equipment_Hierarchy_MasterEquipment obj : equipmentMapEquipmentIdKey.values()) {

			if (obj.isMaster) {
				if (!equipmentMapSuperiorLookupKey.containsKey(obj.equipment.Id)) {
					obj.hasChild = false;
				}
			}

		}

		System.debug('Show me equipmentMapEquipmentIdKey SECOND TIME: ' + JSON.serialize(equipmentMapEquipmentIdKey));

		mapList.add(equipmentMapEquipmentIdKey.values());
		mapList.add(equipmentMapSuperiorLookupKey);

		return mapList;

	}

	public static List<Equipment_Hierarchy_SlaveEquipment> getSlaveEquipment(String equipmentId, String flockId) {
		List<Equipment__c> equipmentList = new List<Equipment__c>();
		System.debug('params ' + equipmentId + ' params ' + flockId);
		List<Equipment_Hierarchy_SlaveEquipment> toLtng = new List<Equipment_Hierarchy_SlaveEquipment>();


		Id flockName = flockId;
		System.debug('API: ' + flockName.getSObjectType());
		SObjectType flockAPI = flockName.getSObjectType();

		String whereClause = 'Superior_Equipment__c = ' + '\'' + equipmentId + '\'' + ' AND ' + flockAPI + ' = \'' + flockId + '\'';

		String preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				EquipmentUtilities.EQUIPMENT_FIELDS,
				'Equipment__c',
				whereClause
		});
		System.debug('prepared query ' + preparedQuery);

		equipmentList = Database.query(preparedQuery);

		System.debug('Equipment List ' + equipmentList);

		List<Equipment__c> eqList = [SELECT Superior_Equipment__c FROM Equipment__c WHERE Superior_Equipment__c IN :equipmentList];
		System.debug('EQLIST: ' + eqList);
		List<String> str = new List<String>();
		for (Equipment__c omg : eqList) {
			str.add(omg.Superior_Equipment__c);
		}

		for (Equipment__c eq : equipmentList) {
			Equipment_Hierarchy_SlaveEquipment obj = new Equipment_Hierarchy_SlaveEquipment();
			obj.equipment = eq;
			if (str.contains(eq.Id)) {
				System.debug('Nope');
				obj.hasChild = true;
			}
			toLtng.add(obj);
		}

		System.debug('this to ltng ' + toLtng);


		return toLtng;

	}


	/* Equipment Transfer Form*/

	public static String insertTransferFormEquipmentForm(
			String fromSource,
			String toDestination,
			Equipment_Transfer_Form__c transferForm,
			List<Object> itemTransfer) {
		String message = '';

		System.debug('STARTING');

		if (toDestination != null) {


			Id fromSourceId = fromSource;
			System.debug('API: ' + fromSourceId.getSObjectType());
			SObjectType flockAPI = fromSourceId.getSObjectType();
			transferForm.put(String.valueOf('From_' + flockAPI), fromSource);

			Id toDestinationId = toDestination;
			System.debug('API: ' + toDestinationId.getSObjectType());
			SObjectType fromFlocAPI = toDestinationId.getSObjectType();
			transferForm.put(String.valueOf('To_' + fromFlocAPI), toDestination);
		} else {
			Id singleSource = fromSource;
			System.debug('API: ' + singleSource.getSObjectType());
			SObjectType singleflockAPI = singleSource.getSObjectType();
			transferForm.put(String.valueOf('To_' + singleflockAPI), fromSource);
		}
		System.debug('Show me form ' + transferForm);
		insert transferForm;
		System.debug('Show me fromSource ' + fromSource);
		System.debug('Show me toDestination ' + toDestination);
		System.debug('Show me transferForm ' + transferForm);
		System.debug('Show me itemTransfer ' + itemTransfer);

		System.debug('JSON ' + JSON.serialize(itemTransfer));

		List<Equipment_TransferForm_ItemForInsert> filesToInsert = (List<Equipment_TransferForm_ItemForInsert>) JSON.deserialize(JSON.serialize(itemTransfer), List<Equipment_TransferForm_ItemForInsert>.class);

		System.debug('filesToInset ' + filesToInsert);

		List<ContentVersion> files = new List<ContentVersion>();


		Map<String, Equipment_Transfer_Item__c> itemsToInsert = new Map<String, Equipment_Transfer_Item__c>();
		for (Equipment_TransferForm_ItemForInsert transferItem : filesToInsert) {
			Equipment_Transfer_Item__c item = new Equipment_Transfer_Item__c();
			item.Equipment__c = transferItem.equipment.Id;
			item.Tag_Colour__c = transferItem.tagColour;
			item.Equipment_Transfer_Form__c = transferForm.Id;
			itemsToInsert.put(item.Equipment__c, item);
		}


		insert itemsToInsert.values();

		for (Equipment_TransferForm_ItemForInsert obj : filesToInsert) {
			if (obj.itemFile.fileName != null) {
				Equipment_Transfer_Item__c item = itemsToInsert.get(obj.equipment.Id);
				String base64Data = EncodingUtil.urlDecode(obj.itemFile.fileContent, 'UTF-8');

				ContentVersion cv = new ContentVersion();
				cv.Title = obj.itemFile.fileName;
				cv.PathOnClient = '/' + obj.itemFile.fileName;
				cv.FirstPublishLocationId = item.Id;
				cv.VersionData = EncodingUtil.base64Decode(base64Data);
				files.add(cv);
			}

		}

		insert files;


		message = 'Huray?';


		System.debug('FINISHING');

		return message;
	}


	public static String updateTransferEquipmentForm(
			Equipment_Transfer_Form__c transferForm,
			Boolean isFromForm,
			String destination,
			List<String> itemsToRemove,
			List<Object> equipmentItems,
			List<String> filesToRemove,
			List<String> attachmentsToRemove,
			List<Object> itemTransfer,
			List<Object> itemFilesToInsert) {

		String message = '';


			System.debug('itemsToRemove: ' + itemsToRemove);
			System.debug('equipmentItems to update: ' + equipmentItems);
			System.debug('filesToRemove: ' + filesToRemove);
			System.debug('attachmentsToRemove: ' + attachmentsToRemove);
			System.debug('itemTransfer to insert: ' + itemTransfer);
			System.debug('itemFilesToInsert: ' + itemFilesToInsert);

			Equipment_Transfer_Form__c originalForm = Equipment_Forms_QuerySelector.getEquipmentTransferForm(transferForm.Id);

			if (isFromForm) {
				transferForm.To_Location__c = null;
				transferForm.To_Facility__c = null;
				transferForm.To_Functional_Equipment_Level__c = null;
				transferForm.To_Yard__c = null;
				transferForm.To_System__c = null;
				transferForm.To_Sub_System__c = null;
				transferForm.To_Well_Event__c = null;

				Id toDestinationId = destination;
				System.debug('API: ' + toDestinationId.getSObjectType());
				SObjectType fromFlocAPI = toDestinationId.getSObjectType();
				transferForm.put(String.valueOf('To_' + fromFlocAPI), destination);
			}


			if (originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_OPEN || originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {

				if (originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {
					transferForm.Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN;
				}
				update transferForm;
				message = 'Updated transfer form';


				if (itemsToRemove.size() > 0) {
					//List<Con>

					List<Equipment_Transfer_Item__c> itemToDelete = new List<Equipment_Transfer_Item__c>();

					for (Id recordId : itemsToRemove) {
						itemToDelete.add(new Equipment_Transfer_Item__c(Id = recordId));
					}

					delete itemToDelete;

					List<ContentDocumentLink> cdl = [SELECT Id, ContentDocument.Id FROM ContentDocumentLink WHERE LinkedEntityId IN :itemsToRemove];
					List<ContentDocument> documents = new List<ContentDocument>();

					for (ContentDocumentLink link : cdl) {
						documents.add(link.ContentDocument);
					}

					delete documents;

				}

				if (filesToRemove.size() > 0) {
					List<ContentDocument> documents = [SELECT Id FROM ContentDocument WHERE Id IN :filesToRemove];

					delete documents;
				}

				if (attachmentsToRemove.size() > 0) {
					List<Attachment> attachments = [SELECT Id FROM Attachment WHERE ParentId IN :attachmentsToRemove];

					delete attachments;
				}

				List<Equipment_TransferForm_Item> listOfItems = (List<Equipment_TransferForm_Item>) JSON.deserialize(JSON.serialize(equipmentItems), List<Equipment_TransferForm_Item>.class);
				List<Equipment_Transfer_Item__c> itemsToUpdate = new List<Equipment_Transfer_Item__c>();

				for (Equipment_TransferForm_Item item : listOfItems) {
					itemsToUpdate.add(item.item);
				}

				update itemsToUpdate;

				if (itemTransfer.size() > 0) {

					List<Equipment_TransferForm_ItemForInsert> filesToInsert = (List<Equipment_TransferForm_ItemForInsert>) JSON.deserialize(JSON.serialize(itemTransfer), List<Equipment_TransferForm_ItemForInsert>.class);

					System.debug('filesToInset ' + filesToInsert);

					List<ContentVersion> files = new List<ContentVersion>();


					Map<String, Equipment_Transfer_Item__c> itemsToInsert = new Map<String, Equipment_Transfer_Item__c>();
					for (Equipment_TransferForm_ItemForInsert transferItem : filesToInsert) {
						Equipment_Transfer_Item__c item = new Equipment_Transfer_Item__c();
						item.Equipment__c = transferItem.equipment.Id;
						item.Tag_Colour__c = transferItem.tagColour;
						item.Equipment_Transfer_Form__c = transferForm.Id;
						itemsToInsert.put(item.Equipment__c, item);
					}

					insert itemsToInsert.values();

					for (Equipment_TransferForm_ItemForInsert obj : filesToInsert) {
						if (obj.itemFile.fileName != null) {
							Equipment_Transfer_Item__c item = itemsToInsert.get(obj.equipment.Id);
							String base64Data = EncodingUtil.urlDecode(obj.itemFile.fileContent, 'UTF-8');

							ContentVersion cv = new ContentVersion();
							cv.Title = obj.itemFile.fileName;
							cv.PathOnClient = '/' + obj.itemFile.fileName;
							cv.FirstPublishLocationId = item.Id;
							cv.VersionData = EncodingUtil.base64Decode(base64Data);
							files.add(cv);
						}

					}

					insert files;

				}


				if (itemFilesToInsert.size() > 0) {
					List<Equipment_TransferForm_ItemFile> filesToInsert = (List<Equipment_TransferForm_ItemFile>) JSON.deserialize(JSON.serialize(itemFilesToInsert), List<Equipment_TransferForm_ItemFile>.class);
					List<ContentVersion> files = new List<ContentVersion>();

					for (Equipment_TransferForm_ItemFile obj : filesToInsert) {
						String base64Data = EncodingUtil.urlDecode(obj.itemFile.fileContent, 'UTF-8');

						ContentVersion cv = new ContentVersion();
						cv.Title = obj.itemFile.fileName;
						cv.PathOnClient = '/' + obj.itemFile.fileName;
						cv.FirstPublishLocationId = obj.equipment.Id;
						cv.VersionData = EncodingUtil.base64Decode(base64Data);
						files.add(cv);

					}

					insert files;

				}

				//TODO Add debug everywhere here and set default values on front end

			} else {
				HOG_ExceptionUtils.throwError('The form is already closed and cannot be updated.');
			}


		return message;
	}

	public static Equipment_TransferForm loadTransferForm(String formId) {
		Equipment_TransferForm transferWrapper = new Equipment_TransferForm();

		transferWrapper.transferForm = Equipment_Forms_QuerySelector.getEquipmentTransferForm(formId);
		transferWrapper.flocInfo = flocInformation(transferWrapper.transferForm);

		return transferWrapper;
	}

	private static Equipment_TransferForm_FLOC_Info flocInformation(Equipment_Transfer_Form__c form) {
		Equipment_TransferForm_FLOC_Info flocInfo = new Equipment_TransferForm_FLOC_Info();
		flocInfo.toFlocId = '';
		flocInfo.toFlocName = '';
		flocInfo.toRouteId = '';
		flocInfo.toRouteNumber = '';
		flocInfo.toPlannerGroup = '';
		flocInfo.toAPIName = '';

		flocInfo.fromFlocId = '';
		flocInfo.fromFlocName = '';
		flocInfo.fromRouteId = '';
		flocInfo.fromRouteNumber = '';
		flocInfo.fromPlannerGroup = '';
		flocInfo.fromAPIName = '';

		if (form.To_Location__c != null) {
			flocInfo.toFlocId = form.To_Location__c;
			flocInfo.toFloc = form.To_Location__r;
			flocInfo.toFlocName = form.To_Location__r.Name;
			flocInfo.toRouteId = form.To_Location__r.Route__c;
			flocInfo.toRouteNumber = form.To_Location__r.Route__r.Name;
			flocInfo.toPlannerGroup = form.To_Location__r.Planner_Group__c;
			flocInfo.toAPIName = 'Location__c';
		} else if (form.To_Facility__c != null) {
			flocInfo.toFlocId = form.To_Facility__c;
			flocInfo.toFloc = form.To_Facility__r;
			flocInfo.toFlocName = form.To_Facility__r.Name;
			flocInfo.toRouteId = form.To_Facility__r.Plant_Section__c;
			flocInfo.toRouteNumber = form.To_Facility__r.Plant_Section__r.Name;
			flocInfo.toPlannerGroup = form.To_Facility__r.Planner_Group__c;
			flocInfo.toAPIName = 'Facility__c';
		} else if (form.To_Functional_Equipment_Level__c != null) {
			flocInfo.toFlocId = form.To_Functional_Equipment_Level__c;
			flocInfo.toFloc = form.To_Functional_Equipment_Level__r;
			flocInfo.toFlocName = form.To_Functional_Equipment_Level__r.Name;
			flocInfo.toRouteId = form.To_Functional_Equipment_Level__r.Route__c;
			flocInfo.toRouteNumber = form.To_Functional_Equipment_Level__r.Route__r.Name;
			flocInfo.toPlannerGroup = form.To_Functional_Equipment_Level__r.Planner_Group__c;
			flocInfo.toAPIName = 'Functional_Equipment_Level__c';
		} else if (form.To_Yard__c != null) {
			flocInfo.toFlocId = form.To_Yard__c;
			flocInfo.toFloc = form.To_Yard__r;
			flocInfo.toFlocName = form.To_Yard__r.Name;
			flocInfo.toRouteId = form.To_Yard__r.Route__c;
			flocInfo.toRouteNumber = form.To_Yard__r.Route__r.Name;
			flocInfo.toPlannerGroup = form.To_Yard__r.Planner_Group__c;
			flocInfo.toAPIName = 'Yard__c';
		} else if (form.To_System__c != null) {
			flocInfo.toFlocId = form.To_System__c;
			flocInfo.toFloc = form.To_System__r;
			flocInfo.toFlocName = form.To_System__r.Name;
			flocInfo.toRouteId = form.To_System__r.Route__c;
			flocInfo.toRouteNumber = form.To_System__r.Route__r.Name;
			flocInfo.toPlannerGroup = form.To_System__r.Planner_Group__c;
			flocInfo.toAPIName = 'System__c';
		} else if (form.To_Sub_System__c != null) {
			flocInfo.toFlocId = form.To_Sub_System__c;
			flocInfo.toFloc = form.To_Sub_System__r;
			flocInfo.toFlocName = form.To_Sub_System__r.Name;
			flocInfo.toRouteId = form.To_Sub_System__r.Route__c;
			flocInfo.toRouteNumber = form.To_Sub_System__r.Route__r.Name;
			flocInfo.toPlannerGroup = form.To_Sub_System__r.Planner_Group__c;
			flocInfo.toAPIName = 'Sub_System__c';
		} else if (form.To_Well_Event__c != null) {
			flocInfo.toFlocId = form.To_Well_Event__c;
			flocInfo.toFloc = form.To_Well_Event__r;
			flocInfo.toFlocName = form.To_Well_Event__r.Name;
			flocInfo.toRouteId = form.To_Well_Event__r.Route__c;
			flocInfo.toRouteNumber = form.To_Well_Event__r.Route__r.Name;
			flocInfo.toPlannerGroup = form.To_Well_Event__r.Planner_Group__c;
			flocInfo.toAPIName = 'Well_Event__c';
		}

		if (form.FROM_Source__c != null) {
			flocInfo.isFromForm = true;
			if (form.From_Location__c != null) {
				flocInfo.fromFlocId = form.From_Location__c;
				flocInfo.fromFloc = form.From_Location__r;
				flocInfo.fromFlocName = form.From_Location__r.Name;
				flocInfo.fromRouteId = form.From_Location__r.Route__c;
				flocInfo.fromRouteNumber = form.From_Location__r.Route__r.Name;
				flocInfo.fromPlannerGroup = form.From_Location__r.Planner_Group__c;
				flocInfo.fromAPIName = 'Location__c';
			} else if (form.From_Facility__c != null) {
				flocInfo.fromFlocId = form.From_Facility__c;
				flocInfo.fromFloc = form.From_Facility__r;
				flocInfo.fromFlocName = form.From_Facility__r.Name;
				flocInfo.fromRouteId = form.From_Facility__r.Plant_Section__c;
				flocInfo.fromRouteNumber = form.From_Facility__r.Plant_Section__r.Name;
				flocInfo.fromPlannerGroup = form.From_Facility__r.Planner_Group__c;
				flocInfo.fromAPIName = 'Facility__c';
			} else if (form.From_Functional_Equipment_Level__c != null) {
				flocInfo.fromFlocId = form.From_Functional_Equipment_Level__c;
				flocInfo.fromFloc = form.From_Functional_Equipment_Level__r;
				flocInfo.fromFlocName = form.From_Functional_Equipment_Level__r.Name;
				flocInfo.fromRouteId = form.From_Functional_Equipment_Level__r.Route__c;
				flocInfo.fromRouteNumber = form.From_Functional_Equipment_Level__r.Route__r.Name;
				flocInfo.fromPlannerGroup = form.From_Functional_Equipment_Level__r.Planner_Group__c;
				flocInfo.fromAPIName = 'Functional_Equipment_Level__c';
			} else if (form.From_Yard__c != null) {
				flocInfo.fromFlocId = form.From_Yard__c;
				flocInfo.fromFloc = form.From_Yard__r;
				flocInfo.fromFlocName = form.From_Yard__r.Name;
				flocInfo.fromRouteId = form.From_Yard__r.Route__c;
				flocInfo.fromRouteNumber = form.From_Yard__r.Route__r.Name;
				flocInfo.fromPlannerGroup = form.From_Yard__r.Planner_Group__c;
				flocInfo.fromAPIName = 'Yard__c';
			} else if (form.From_System__c != null) {
				flocInfo.fromFlocId = form.From_System__c;
				flocInfo.fromFloc = form.From_System__r;
				flocInfo.fromFlocName = form.From_System__r.Name;
				flocInfo.fromRouteId = form.From_System__r.Route__c;
				flocInfo.fromRouteNumber = form.From_System__r.Route__r.Name;
				flocInfo.fromPlannerGroup = form.From_System__r.Planner_Group__c;
				flocInfo.fromAPIName = 'System__c';
			} else if (form.From_Sub_System__c != null) {
				flocInfo.fromFlocId = form.From_Sub_System__c;
				flocInfo.fromFloc = form.From_Sub_System__r;
				flocInfo.fromFlocName = form.From_Sub_System__r.Name;
				flocInfo.fromRouteId = form.From_Sub_System__r.Route__c;
				flocInfo.fromRouteNumber = form.From_Sub_System__r.Route__r.Name;
				flocInfo.fromPlannerGroup = form.From_Sub_System__r.Planner_Group__c;
				flocInfo.fromAPIName = 'Sub_System__c';
			} else if (form.From_Well_Event__c != null) {
				flocInfo.fromFlocId = form.From_Well_Event__c;
				flocInfo.fromFloc = form.From_Well_Event__r;
				flocInfo.fromFlocName = form.From_Well_Event__r.Name;
				flocInfo.fromRouteId = form.From_Well_Event__r.Route__c;
				flocInfo.fromRouteNumber = form.From_Well_Event__r.Route__r.Name;
				flocInfo.fromPlannerGroup = form.From_Well_Event__r.Planner_Group__c;
				flocInfo.fromAPIName = 'Well_Event__c';
			}
		}

		return flocInfo;

	}

	public static List<Equipment_TransferForm_Item> loadEquipmentTransferItems(String formId) {
		//List<Equipment_Transfer_Item__c> listItems = Equipment_Forms_QuerySelector.getEquipmentTransferItems(formId);


		Map<Id, Equipment_Transfer_Item__c> transferItemMap = new Map<Id, Equipment_Transfer_Item__c>(Equipment_Forms_QuerySelector.getEquipmentTransferItems(formId));

		System.debug('transferItemMap');


		List<Equipment_TransferForm_Item> wrapperList = new List<Equipment_TransferForm_Item>();

		List<ContentDocumentLink> cdlList = [SELECT Id, ContentDocument.Title, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :transferItemMap.keySet()];
		List<Attachment> attachmentList = [SELECT Id, Name, ParentId FROM Attachment WHERE ParentId IN :transferItemMap.keySet()];

		Set<Id> usedIds = new Set<Id>();


		for (ContentDocumentLink cld : cdlList) {
			if (transferItemMap.containsKey(cld.LinkedEntityId)) {
				Equipment_Transfer_Item__c item = transferItemMap.get(cld.LinkedEntityId);
				Equipment_TransferForm_Item wrapper = new Equipment_TransferForm_Item();
				wrapper.item = item;
				wrapper.files.add(cld);
				wrapper.hasAttachment = true;
				wrapperList.add(wrapper);
				usedIds.add(cld.LinkedEntityId);
			}
		}

		for (Attachment attachment : attachmentList) {
			if (transferItemMap.containsKey(attachment.ParentId)) {
				Equipment_Transfer_Item__c item = transferItemMap.get(attachment.ParentId);
				Equipment_TransferForm_Item wrapper = new Equipment_TransferForm_Item();
				wrapper.item = item;
				wrapper.hasAttachment = true;
				wrapper.attachments.add(attachment);
				wrapperList.add(wrapper);
				usedIds.add(attachment.ParentId);
			}
		}


		for (Equipment_Transfer_Item__c item : transferItemMap.values()) {
			if (!usedIds.contains(item.Id)) {
				Equipment_TransferForm_Item wrapper = new Equipment_TransferForm_Item();
				wrapper.item = item;
				wrapper.hasAttachment = false;
				wrapperList.add(wrapper);
			}
		}


		System.debug('Show me Transfer Wrapper: ' + wrapperList);

		return wrapperList;
	}

	/* Equipment Transfer Form - Equipment Selection */

	public static List<Equipment__c> loadEquipments(String userInput, Boolean isFromForm, String flocId, Boolean getInitialEquipment, List<String> selectedTransferItems) {
		System.debug('User Input before ' + userInput);
		userInput = '%' + userInput + '%';
		System.debug('User Input after ' + userInput);
		if (isFromForm) {
			if (!getInitialEquipment) {
				return Equipment_Forms_QuerySelector.getEquipmentBasedOnSearchCriteriaFromForm(userInput, flocId, selectedTransferItems);
			} else {
				return Equipment_Forms_QuerySelector.initialEquipmentForFromForm(flocId, selectedTransferItems);
			}
		} else {
			return Equipment_Forms_QuerySelector.getEquipmentBasedOnSearchCriteria(userInput, selectedTransferItems);
		}
	}

	/* Equipment Data Update Form */

	public static String insertNewDataUpdateForm(
			String equipmentId,
			Equipment_Correction_Form__c correctionForm,
			String attachmentBase64Data,
			String attachmentFileName) {

		String message = '';
			Equipment_Correction_Form__c newCorrectionForm = correctionForm;
			newCorrectionForm.Equipment__c = equipmentId;
			insert newCorrectionForm;

			if (attachmentFileName != null) {
				String base64Data = '';
				// Decoding base64Data
				base64Data = EncodingUtil.urlDecode(attachmentBase64Data, 'UTF-8');

				ContentVersion cv = new ContentVersion();
				cv.Title = attachmentFileName;
				cv.PathOnClient = '/' + attachmentFileName;
				cv.FirstPublishLocationId = newCorrectionForm.Id;
				cv.VersionData = EncodingUtil.base64Decode(base64Data);
				cv.IsMajorVersion = true;
				insert cv;
			}

			message = 'Data Update Form has been successfully created.';


		return message;
	}

	public static Boolean updateDataUpdateForm(
			Equipment_Correction_Form__c correctionForm,
			Object handleUploads) {

		Boolean formWasUpdated = false;


			Equipment_Correction_Form__c originalForm = Equipment_Forms_QuerySelector.getDataUpdateForm(correctionForm.Id);

			if (originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_OPEN || originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {

				if (originalForm.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {
					correctionForm.Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN;
				}


				Equipment_CorrectionForm_File uploadHandler = (Equipment_CorrectionForm_File) JSON.deserialize(JSON.serialize(handleUploads), Equipment_CorrectionForm_File.class);

				if (uploadHandler.originalAttachmentFileId != null) {
					Equipment_Forms_FileHandler.deleteFile(uploadHandler.originalAttachmentFileId);
					System.debug('delete file');
				} else if (uploadHandler.originalAttachmentId != null) {
					System.debug('delete attachament');
					deleteAttachment(uploadHandler.originalAttachmentId);
				}


				if (uploadHandler.insertAttachment) {
					System.debug('Inserting ');
					Equipment_Forms_FileHandler.insertFile(uploadHandler.attachmentBase64Data,
							uploadHandler.attachmentFileName,
							correctionForm.Id);
				}

				update correctionForm;

				//formWasUpdated = Equipment_Forms_EndpointService.checkChangesBeforeUpdate(originalForm, newDataUpdateForm);
				formWasUpdated = true;
			} else {
				HOG_ExceptionUtils.throwError('The form is already closed and cannot be updated.');
			}

		System.debug('Show me return update ' + formWasUpdated);
		return formWasUpdated;
	}

	public static Equipment_CorrectionForm loadCorrectionForm(String formId) {

		Equipment_CorrectionForm wrapper = new Equipment_CorrectionForm();

		wrapper.correctionForm = Equipment_Forms_QuerySelector.getDataUpdateForm(formId);

		wrapper.correctionFormFiles = getFormFiles(formId);
		wrapper.correctionFormAttachments = getFormAttachments(formId);

		System.debug('Show me wrapper ' + wrapper);
		return wrapper;
	}

	public static void rejectCorrectionEquipmentForm(String formId, String rejectionReason) {
		Equipment_Correction_Form__c form = Equipment_Forms_QuerySelector.getDataUpdateForm(formId);
		form.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;
		form.Reason__c = rejectionReason;
		update form;

		EquipmentFormService.requestRejectionTemplateEmail(
				form.Id,
				form.CreatedById,
				new Set<String>{
						form.Equipment__r.Planner_Group__c +
								EquipmentFormService.COMBINATION_DELIMITER +
								form.Equipment__r.Functional_Location__c
				}
		);

	}

	public static void processCorrectionEquipmentForm(String formId) {
		Equipment_Correction_Form__c correctionForm = new Equipment_Correction_Form__c();
		correctionForm.Id = formId;
		correctionForm.Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED;
		update correctionForm;
	}


	/* OTHERS */

	public static HOG_SObject_Information loadFLOC(String flockId) {
		Id flockName = flockId;
		String objectName = flockName.getSObjectType().getDescribe().getName();

		String whereClause = 'Id' + ' = \'' + flockId + '\'';
		String preparedQuery;

		if (objectName == 'Facility__c') {

			preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
					EquipmentUtilities.FACILITY_FIELDS,
					objectName,
					whereClause
			});
		} else {
			preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
					EquipmentUtilities.FLOC_FIELDS,
					objectName,
					whereClause
			});
		}

		System.debug('Prepared query ' + preparedQuery);
		System.debug('Query ' + Database.query(preparedQuery));
		//Show me wrapper flocWrapper:[flocObject=Location__c:{Id=a0t0q000000PNboAAG, Name=05-13-050-19W5}, flockObjectName=Location__c]

		HOG_SObject_Information wrapper = new HOG_SObject_Information(Database.query(preparedQuery));

		System.debug('Show me wrapper ' + wrapper);
		return wrapper;
	}


	/* RELATED FORMS */

	public static Equipment_ActiveForms equipmentFormsForWorkOrder(String flocId) {
		List<Equipment_Missing_Form__c> activeAddEquipmentForms = new List<Equipment_Missing_Form__c>();
		List<Equipment_Transfer_Form__c> activeEquipmentTransferForms = new List<Equipment_Transfer_Form__c>();
		Equipment_ActiveForms listsOfForms = new Equipment_ActiveForms();


		Id flockName = flocId;
		System.debug('Floc ID ' + flocId);
		SObjectType flockAPI = flockName.getSObjectType();
		String preparedQuery = '';


		String whereClauseForAdd = flockAPI
				+ ' = \''
				+ flocId
				+ '\''
				+ ' AND (Status__c != '
				+ '\''
				+ EquipmentUtilities.REQUEST_STATUS_PROCESSED
				+ '\''
				+ ' OR Status__c != '
				+ '\''
				+ EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED
				+ '\'' +
				+' OR Status__c != '
				+ '\''
				+ EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED
				+ '\''
				+ ')';

		String whereClauseForTransfer = '( To_'
				+ flockAPI
				+ ' = \''
				+ flocId
				+ '\''
				+ ' OR From_'
				+ flockAPI
				+ ' = \''
				+ flocId
				+ '\''
				+ ')'
				+ ' AND (Status__c != '
				+ '\''
				+ EquipmentUtilities.REQUEST_STATUS_PROCESSED
				+ '\''
				+ ' OR Status__c != '
				+ '\''
				+ EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED
				+ '\'' +
				+' OR Status__c != '
				+ '\''
				+ EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED
				+ '\''
				+ ')';

		whereClauseForAdd += ' ORDER BY Name DESC';
		whereClauseForTransfer += ' ORDER BY Name DESC';

		System.debug('before query');
		preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				EquipmentUtilities.ADD_FORM_FIELDS,
				'Equipment_Missing_Form__c',
				whereClauseForAdd
		});

		System.debug('After query');

		System.debug('before query 2 ');
		activeAddEquipmentForms = Database.query(preparedQuery);

		if (activeAddEquipmentForms.size() > 0) {
			System.debug('condition');
			listsOfForms.addForms = activeAddEquipmentForms;
		}

		preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				EquipmentUtilities.TRANSFER_FORM_FIELDS,
				'Equipment_Transfer_Form__c',
				whereClauseForTransfer
		});

		activeEquipmentTransferForms = Database.query(preparedQuery);
		System.debug('after query 2 ');

		if (activeEquipmentTransferForms.size() > 0) {

			for (Equipment_Transfer_Form__c transferForm : activeEquipmentTransferForms) {
				if (transferForm.FROM_Source__c == null) {
					listsOfForms.transferToForms.add(transferForm);
				}

				if (transferForm.FROM_Source__c != null) {
					if (transferForm.To_Location__c == flocId
							|| transferForm.To_Facility__c == flocId
							|| transferForm.To_Functional_Equipment_Level__c == flocId
							|| transferForm.To_Yard__c == flocId
							|| transferForm.To_System__c == flocId
							|| transferForm.To_Sub_System__c == flocId
							|| transferForm.To_Well_Event__c == flocId) {
						listsOfForms.transferToForms.add(transferForm);
					} else {
						listsOfForms.transferFromForms.add(transferForm);
					}
				}
			}
		}
		System.debug('after all');

		System.debug('Show me result ' + listsOfForms);

		return listsOfForms;

	}

	/*public static Boolean checkChangesBeforeUpdate(
			Equipment_Correction_Form__c originalDataUpdateForm,
			Equipment_Correction_Form__c newDataUpdateForm) {

		List<String> dataFormFields = EquipmentUtilities.equipmentDataUpdateFields;
		System.debug('Show original form: ' + originalDataUpdateForm);
		System.debug('Show new form: ' + newDataUpdateForm);
		Boolean hasChanged = false;

		System.debug('Show me list of field to check ' + dataFormFields);

		for (String fieldToCheck : dataFormFields) {
			System.debug('Show original: ' + originalDataUpdateForm.get(fieldToCheck));
			System.debug('Show new: ' + newDataUpdateForm.get(fieldToCheck));
			hasChanged |= (originalDataUpdateForm.get(fieldToCheck) != newDataUpdateForm.get(fieldToCheck));
			if (hasChanged) break;
		}

		if (hasChanged) {
			update newDataUpdateForm;
		}
		return hasChanged;
	}*/


}