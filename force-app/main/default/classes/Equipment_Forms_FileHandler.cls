/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/12/2020   
 */

public class Equipment_Forms_FileHandler {

	public static String insertFile(String base64Data, String fileName, String fileParentId) {
		base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

		ContentVersion cv = new ContentVersion();
		cv.Title = fileName;
		cv.PathOnClient = '/' + fileName;
		cv.FirstPublishLocationId = fileParentId;
		cv.VersionData = EncodingUtil.base64Decode(base64Data);
		cv.IsMajorVersion = true;
		insert cv;

		return getContentDocumentId(cv.Id);
	}

	public static String deleteFile(String documentId) {
		List<ContentDocument> document = [SELECT Id FROM ContentDocument WHERE Id = :documentId];
		delete document;
		return null;
	}

	private static String getContentDocumentId(String versionId) {
		return [SELECT Id,ContentDocumentId FROM ContentVersion WHERE Id = :versionId].ContentDocumentId;
	}

}