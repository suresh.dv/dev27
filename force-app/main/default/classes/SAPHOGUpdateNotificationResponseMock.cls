@isTest
global with sharing class SAPHOGUpdateNotificationResponseMock implements WebServiceMock {

	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
		SAPHOGNotificationServices.UpdateNotificationResponse responseElement = new SAPHOGNotificationServices.UpdateNotificationResponse();
   		responseElement.Message = 'SAPHOGNotificationServices.UpdateNotificationResponse Test Message';
   		responseElement.type_x = True;
   		responseElement.Notification = new SAPHOGNotificationServices.Notification();
   		responseElement.Notification.NotificationNumber = '7521256';
   		responseElement.Notification.Items = new SAPHOGNotificationServices.NotificationItems();
   		responseElement.Notification.Items.NotificationItem = new SAPHOGNotificationServices.NotificationItem[]{};
   		
   		SAPHOGNotificationServices.NotificationItem item = new SAPHOGNotificationServices.NotificationItem();
   		item.Causes = new SAPHOGNotificationServices.NotificationItemCauses();
   		item.Causes.NotificationItemCause = new SAPHOGNotificationServices.NotificationItemCause[]{};
   		
   		SAPHOGNotificationServices.NotificationItemCause cause = new SAPHOGNotificationServices.NotificationItemCause();
   		
   		item.Causes.NotificationItemCause.add(cause); 
   		
   		responseElement.Notification.Items.NotificationItem.add(item);
   		
   		response.put('response_x', responseElement);
	}
}