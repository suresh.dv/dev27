/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class that holds information about current user related to VTT Workflows and actions.
Test Class:     VTT_Utility_EndpointHandlerTest
History:        jschn 04/09/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_TradesmanFlags {

    @AuraEnabled
    public Boolean isAdmin;

    @AuraEnabled
    public Boolean isVendorSupervisor;

    @AuraEnabled
    public Contact tradesman;

    @AuraEnabled
    public Id currentWorkOrderActivityId;

    /**
     * Sets flags based on provided parameters
     *
     * @param isAdmin
     * @param isVendorSupervisor
     *
     * @return VTT_TradesmanFlags
     */
    public VTT_TradesmanFlags setFlags(Boolean isAdmin, Boolean isVendorSupervisor) {
        this.isAdmin = isAdmin;
        this.isVendorSupervisor = isVendorSupervisor;
        return this;
    }

    /**
     * Sets tradesman contact based on provided parameter
     *
     * @param tradesman
     *
     * @return VTT_TradesmanFlags
     */
    public VTT_TradesmanFlags setTradesman(Contact tradesman) {
        this.tradesman = tradesman;
        return this;
    }

    /**
     * Sets Id of Current Work Order Activity based on provided parameter
     *
     * @param currentWorkOrderActivityId
     *
     * @return VTT_TradesmanFlags
     */
    public VTT_TradesmanFlags setCurrentWOAId(Id currentWorkOrderActivityId) {
        this.currentWorkOrderActivityId = currentWorkOrderActivityId;
        return this;
    }

}