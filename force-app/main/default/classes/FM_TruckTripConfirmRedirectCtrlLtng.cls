/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint class for FM_TruckTripConfirmRedirectQA lightning component.
                It queries all necessary records, do validation and return target URL for redirect
                or error messages.
Test Class:     FM_TruckTripConfirmRedirectCtrlLtngTest
History:        jschn 2019-01-30 - Created.
*************************************************************************************************/
public with sharing class FM_TruckTripConfirmRedirectCtrlLtng {

    private static final String CLASS_NAME = String.valueOf(FM_TruckTripConfirmRedirectCtrlLtng.class);

    private static final String ID_PLACEHOLDER = '###';
    private static final String CONFIRM_TARGET_URL = '/apex/FM_LoadConfirmationEdit?retURL=/' + ID_PLACEHOLDER
            + '&ttid=' + ID_PLACEHOLDER;

    public static final String VENDOR_ERROR_WRONG_STATUS = 'Only exported truck trip can be confirmed.';
    public static final String HUSKY_ERROR_WRONG_STATUS = 'Only dispatched, add on and exported truck trips can be confirmed.';
    public static final String ERROR_UNKNOWN_TRUCK_TRIP = 'Unknown Truck Trip. Truck Trip retrieve unsuccessful.';

    @AuraEnabled
    public static HOG_CustomResponse checkConfirmRedirect(String recordId) {
        FM_Truck_Trip__c truckTrip = FM_TruckTrip_Utilities.getTruckTripById(recordId);
        UserRole role = HOG_UserInfoLtng.getCurrentUserRole();

        HOG_CustomResponse response;

        if(truckTrip != null
                && hasCorrectRoleOrStatus(truckTrip, role)) {
            HOG_CustomResponseImpl resp = new HOG_CustomResponseImpl();
            resp.addResult(CONFIRM_TARGET_URL.replaceAll(ID_PLACEHOLDER, recordId));
            response = resp;
        } else {
            response = new HOG_CustomResponseImpl(getErrorMsg(truckTrip, role));
        }

        System.debug(CLASS_NAME + ' -> checkConfirmRedirect - Id param: '    + recordId);
        System.debug(CLASS_NAME + ' -> checkConfirmRedirect - Truck Trip: '  + JSON.serialize(truckTrip));
        System.debug(CLASS_NAME + ' -> checkConfirmRedirect - Role: '        + JSON.serialize(role));
        System.debug(CLASS_NAME + ' -> checkConfirmRedirect - response: '    + JSON.serialize(response));
        return response;
    }

    private static Boolean hasCorrectRoleOrStatus(FM_Truck_Trip__c truckTrip, UserRole role) {
        return hasCorrectStatusForVendorRole(truckTrip, role)
                || hasCorrectStatusForHuskyRole(truckTrip, role);
    }

    private static Boolean hasCorrectStatusForVendorRole(FM_Truck_Trip__c truckTrip, UserRole role) {
        String status = truckTrip.Truck_Trip_Status__c;
        return !isVendorRole(role)
                && (FM_TruckTrip_Utilities.STATUS_DISPATCHED.equals(status)
                            || FM_TruckTrip_Utilities.STATUS_ADDON.equals(status)
                            || FM_TruckTrip_Utilities.STATUS_EXPORTED.equals(status));
    }

    private static Boolean hasCorrectStatusForHuskyRole(FM_Truck_Trip__c truckTrip, UserRole role) {
        return isVendorRole(role)
                && FM_TruckTrip_Utilities.STATUS_EXPORTED.equals(truckTrip.Truck_Trip_Status__c);
    }

    private static Boolean isVendorRole(UserRole role) {
        return role != null
                && role.Name == FM_Utilities.FLUID_MANAGEMENT_VENDOR_USER_ROLE;
    }

    private static String getErrorMsg(FM_Truck_Trip__c truckTrip, UserRole role) {
        String errorMsg;

        if(truckTrip == null) {
            errorMsg = ERROR_UNKNOWN_TRUCK_TRIP;
        } else if(role == null) {
            errorMsg = HOG_UserInfoLtng.ERROR_USER_WITHOUT_ROLE;
        } else if(isVendorRole(role)) {
            errorMsg = VENDOR_ERROR_WRONG_STATUS;
        } else {
            errorMsg = HUSKY_ERROR_WRONG_STATUS;
        }
        return errorMsg;
    }

}