/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non parallel Unit Test for EquipmentEngineEndpointHandler
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest
private class EquipmentEngineEndpointHandlerTestNP {

    @IsTest
    static void refreshEngineData_mockedSuccess() {
        HOG_SimpleResponse response;
        Boolean expectedSuccessFlag = true;
        Id equipmentId = EPD_TestData.createEquipmentForBatch();

        Test.startTest();
        response = new EquipmentEngineEndpointHandler().refreshEngineData(equipmentId);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
    }

}