/**
 * Created by MarcelBrimus on 03/09/2019.
 */

public inherited sharing class VTT_CalendarEventModel {
    @AuraEnabled
    public String title { get; set; }

    @AuraEnabled
    public Boolean allDay { get; set; }

    @AuraEnabled
    public String startString { get; set; }

    @AuraEnabled
    public String endString { get; set; }

    @AuraEnabled
    public String url { get; set; }

    @AuraEnabled
    public String className { get; set; }

    @AuraEnabled
    public Id activityId { get; set; }

    @AuraEnabled
    public String woPriorityNumber { get; set; }

    @AuraEnabled
    public String amuName { get; set; }

    @AuraEnabled
    public String woWONumber { get; set; }

    @AuraEnabled
    public String equipmentTagNumber { get; set; }

    @AuraEnabled
    public String woFlocType { get; set; }

    @AuraEnabled
    public String woFlocName { get; set; }


}