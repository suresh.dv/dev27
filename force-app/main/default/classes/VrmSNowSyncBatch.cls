/* Created Date - 11-06-2019
Description - Salesforce - ServiceNow Integration
*/

global class VrmSNowSyncBatch implements Database.Batchable<sObject>, Database.AllowsCallouts
{
    global Database.QueryLocator start (Database.BatchableContext BC)
    {
        String query = 'Select id from jbrvrm__Vendor_Subscription_Title__c LIMIT 1';
        return Database.getQueryLocator(query);
    }
    
    global void execute (Database.BatchableContext BC, List <jbrvrm__Vendor_Subscription_Title__c> scope)
    {
        system.debug('#### scope: ' + scope);
        SNow_Credentials__c sNow = SNow_Credentials__c.getValues('SNow_Credentials');
        
        Map<String,Id> userNameMap = new Map<String,Id>();
        
        Set<String> sysIdSet = new Set<String>();
        
        List<jbrvrm__Vendor_Subscription_Title__c> existingVSTList = new List<jbrvrm__Vendor_Subscription_Title__c>();
        List<User> userList = [Select id, name, Alias from User LIMIT 50000];
        if(userList != null && userList.size()>0)
        {
            for(User u : userList)
            {
                userNameMap.put(u.name.toLowerCase()+' ('+u.alias.toLowerCase()+')',u.id);
            }
        }
        system.debug('#### userNameMap --- ' + userNameMap);
        
        // 1st API call to fetch data from SNow
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        String endpoint = sNow.API_URL__c.trim()+'/api/now/table/cmdb_ci_appl?sysparm_query=sys_class_name%3Dcmdb_ci_appl%5Einstall_status!%3D7&sysparm_display_value=true&sysparm_fields=name%2Csys_id%2Cu_service_delivery_specialist';
        String encodedAuth = EncodingUtil.base64Encode(blob.valueOf(sNow.Username__c + ':' + sNow.Password__c));
        req.setHeader('Authorization', 'Basic ' + encodedAuth);
        req.setHeader('Accept', 'application/json');
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        if(!Test.isRunningTest())
        {
            res = http.send(req);
        }
        else
        {
            res.setBody('{"result":[{"sys_id": "0021295fdd2916446b6dbf97591750cc","u_service_delivery_specialist": "Test User (UserT)","name": "Active Risk Manager (Atlantic Region)", "id":"12345"}]}');
            res.setStatusCode(200);
        }
        
        system.debug('#### status: ' + res.getStatus());
        system.debug('#### body: ' + res.getBody());
        
        SNowResultWrapper deserializeResults;
        
        if(res.getStatusCode() == 200)
        {
            String jsonResponse = res.getBody();
            Type resultType = Type.forName('SNowResultWrapper');
            deserializeResults = (SNowResultWrapper)JSON.deserialize(jsonResponse , resultType);
            
        }
        System.debug('#### List Result -- ' + deserializeResults);
        
        if(deserializeResults != null)
        {
            for (SNowResultWrapper.Results wr : deserializeResults.result)
            {
                //System.debug('#### get -- ' + wr.sys_id);  
                sysIdSet.add(wr.sys_id);
            }
            //System.debug('#### List Result -- ' + (List<Results>)deserializeResults.get('result'));
        }
        if(sysIdSet != null && sysIdSet.size()>0)
        {
            existingVSTList = [Select id, name, jbrvrm__ServiceNow_CI__c, jbrvrm__ServiceNow_SYS_ID__c, jbrvrm__Ops_Reviewer__c, jbrvrm__Contract_End_Date__c, jbrvrm__Vendor_Analyst__r.Name from jbrvrm__Vendor_Subscription_Title__c where jbrvrm__ServiceNow_SYS_ID__c in : sysIdSet AND jbrvrm__Status__c = 'Active'];
        }
        
        if(existingVSTList != null && existingVSTList.size()>0)
        {
            for(jbrvrm__Vendor_Subscription_Title__c vst : existingVSTList)
            {
                for(SNowResultWrapper.Results wr : deserializeResults.result)
                {
                    if(wr.sys_id == vst.jbrvrm__ServiceNow_SYS_ID__c)
                    {
                        String[] deliverySpecialists = wr.u_service_delivery_specialist.split(',');
                        if(userNameMap.containsKey(deliverySpecialists[0].toLowerCase()))
                        {
                            
                            vst.jbrvrm__Ops_Reviewer__c = userNameMap.get(deliverySpecialists[0].toLowerCase());
                            System.debug('deliverySpecialists[0] --- '+ deliverySpecialists[0].toLowerCase() + '---- vst.jbrvrm__Ops_Reviewer__c -- '+ vst.jbrvrm__Ops_Reviewer__c);
                        }
                        //vst.jbrvrm__ServiceNow_CI__c = wr.name.length()>=40? wr.name.substring(0, 40) : wr.name;
                        if(wr.name != null && wr.name != ''){vst.jbrvrm__ServiceNow_CI_New__c = wr.name;}
                    }
                }
            }
        }
        System.debug('#### After update --- ' + existingVSTList); //Values from ServiceNow are stored in this list
        
        
        
        //2nd Callout to push records to SNow
        if(existingVSTList != null && existingVSTList.size()>0)
        {
            for(jbrvrm__Vendor_Subscription_Title__c vst : existingVSTList)
            {
                System.debug('#### Vendor Analyst -- ' + vst.jbrvrm__Vendor_Analyst__r.Name);
                
                
                String vendorAnalystSysId = '';
                if(vst.jbrvrm__Vendor_Analyst__c != null)
                {
                    String vendorName = EncodingUtil.urlEncode(vst.jbrvrm__Vendor_Analyst__r.Name, 'UTF-8').replace('+', '%20') ;
                    String endpoint2 = sNow.API_URL__c.trim()+'/api/now/table/sys_user?sysparm_query=name%3D'+ vendorName +'&sysparm_fields=sys_id&sysparm_limit=1';
                    
                    //Callout to get the Sys Id of the User from SNow
                    HttpRequest req2 = new HttpRequest();
                    HttpResponse res2 = new HttpResponse();
                    Http http2 = new Http();
                    req2.setHeader('Authorization', 'Basic ' + encodedAuth);
                    req2.setHeader('Accept', 'application/json');
                    req2.setEndpoint(endpoint2);
                    req2.setMethod('GET');
                    if(!Test.isRunningTest())
                    {
                        res2 = http2.send(req2);
                        String jsonResponse2 = '';
                    
                        if(res2.getStatusCode() == 200)
                        {
                            jsonResponse2 = res2.getBody();
                            Map<String, Object> userBodyMap = (Map<String, Object>)JSON.deserializeUntyped(jsonResponse2 );
                            List<Object> items = (List<Object>)userBodyMap.get('result');
        
                            for (Object item : items) 
                            {
                                Map<String, Object> i = (Map<String, Object>)item;   vendorAnalystSysId = (String)i.get('sys_id');
                            }
                        }
                    }
                    else
                    {res2.setBody('"result" : [{"sys_id":"123456"}]');res2.setStatusCode(200);vendorAnalystSysId = '123456';}
            
                }
                
                //Callout to push records to SNow
                String endpoint3 = sNow.API_URL__c.trim()+'/api/now/table/cmdb_ci_appl/'+vst.jbrvrm__ServiceNow_SYS_ID__c;
                    
                //Callout to get the Sys Id of the User from SNow
                try
                {
                    HttpRequest req3 = new HttpRequest();
                    HttpResponse res3 = new HttpResponse();
                    Http http3 = new Http();
                    req3.setHeader('Authorization', 'Basic ' + encodedAuth);
                    req3.setHeader('Accept', 'application/json');
                    req3.setEndpoint(endpoint3);
                    req3.setMethod('PUT');
                    
                    String req3Body = '{"u_vrm_id" : "'+vst.name+'",';
                    req3Body = req3Body + '"u_mss_owner" : "' + vendorAnalystSysId + '",';
                    
                    
                    req3Body = req3Body + '"u_mss_url" : "' + System.URL.getSalesforceBaseUrl().toExternalForm() + '/'+vst.Id+'",';
                    
                    if(vst.jbrvrm__Contract_End_Date__c != null)
                    {
                        String month = '';
                        String day = '';
                        if(vst.jbrvrm__Contract_End_Date__c.month() >= 1 && vst.jbrvrm__Contract_End_Date__c.month() <=9)
                        {
                            month = '0'+vst.jbrvrm__Contract_End_Date__c.month();
                        }
                        else
                        {
                            month = ''+vst.jbrvrm__Contract_End_Date__c.month();
                        }
                        if(vst.jbrvrm__Contract_End_Date__c.day() >=1 && vst.jbrvrm__Contract_End_Date__c.day() <=9)
                        {
                            day = '0'+vst.jbrvrm__Contract_End_Date__c.day();
                        }
                        else
                        {
                            day = ''+vst.jbrvrm__Contract_End_Date__c.day();
                        }
                        req3Body = req3Body + '"u_license_expiration" : "'+ vst.jbrvrm__Contract_End_Date__c.year()+'-'+ month + '-' + day +'"}';
                    }
                    else {req3Body = req3Body + '"u_license_expiration" : ""}';}
                    req3.setBody(req3Body);
                    System.debug('#### req3Body -- ' + req3Body);
                    
                    if(!Test.isRunningTest()){res3 = http3.send(req3);}
                    else{res3.setStatusCode(200);}
                    
                    
                    res3 = http3.send(req3);
                    
                    if(res3.getStatusCode() !=200){System.debug('Error - Code - ' + res3.getStatusCode());}
                    
                }
                catch(Exception ex){System.debug('Error - ' + ex.getMessage());}
            }
            
            //update existingVSTList;
            Database.update(existingVSTList, false);
        }
    }
    global void finish (Database.BatchableContext BC)
    {
    
    }
}