/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EPDService
History:        jschn 2019-07-26 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EPDServiceTest {

    @IsTest
    static void getFormStructuresForEPDs_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        List<EPD_FormStructure> result;

        Test.startTest();
        try {
            result = new EPD_EPDService().getFormStructuresForEPDs(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getFormStructuresForEPDs_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructure> result;

        Test.startTest();
        try {
            result = new EPD_EPDService().getFormStructuresForEPDs(new List<EPD_Engine_Performance_Data__c>());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());
    }

    @IsTest
    static void getFormStructuresForEPDs_mocked() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructure> result;

        EPD_DAOProvider.EPDDAO = new EPDMockFor_getFormStructuresForEPDs();
        EPD_DAOProvider.blockInformationDAO = new BlockInfoMockFor_getFormStructuresForEPDs();

        Test.startTest();
        try {
            result = new EPD_EPDService().getFormStructuresForEPDs(new List<EPD_Engine_Performance_Data__c>());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(1, result.size());
    }

    public class EPDMockFor_getFormStructuresForEPDs implements EPD_EPDDAO {

        public List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds) {
            EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                    new EPD_Engine_Performance_Data__c(),
                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                    false
            );
            epd.Work_Order__r = (HOG_Maintenance_Servicing_Form__c) HOG_SObjectFactory.createSObject(
                    new HOG_Maintenance_Servicing_Form__c(),
                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                    false
            );
            epd.Equipment_Engine__r = (Equipment_Engine__c) HOG_SObjectFactory.createSObject(
                    new Equipment_Engine__c(),
                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                    false
            );
            epd.Data_Collected_By__r = (User) HOG_SObjectFactory.createSObject(
                    new User(),
                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                    false
            );
            epd.Company__r = (Account) HOG_SObjectFactory.createSObject(
                    new Account(),
                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                    false
            );
            return new List<EPD_Engine_Performance_Data__c> {epd};
        }

        public List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId) {return null;}
        public List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds) {return null;}
        public Integer getCountOfSubmittedRecords(Id workOrderActivityId) {return 0;}
        public List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId) {return null;}
    }
    public class BlockInfoMockFor_getFormStructuresForEPDs implements EPD_BlockInformationDAO {

        public List<EPD_Block_Information__c> getBlockInformationByEPDs(Set<Id> epdIds) {
            return new List<EPD_Block_Information__c> {
                    (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                            new EPD_Block_Information__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            };
        }

    }

    @IsTest
    static void isEPDRequired_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean result;

        Test.startTest();
        try {
            result = new EPD_EPDService().isEPDRequired(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void isEPDRequired_withParam1() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean result;

        Test.startTest();
        try {
            result = new EPD_EPDService().isEPDRequired(UserInfo.getUserId(), null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void isEPDRequired_withParam2() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean result;
        HOG_Maintenance_Servicing_Form__c wo = (HOG_Maintenance_Servicing_Form__c) HOG_SObjectFactory.createSObject(
                new HOG_Maintenance_Servicing_Form__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );

        Test.startTest();
        try {
            result = new EPD_EPDService().isEPDRequired(null, wo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void isEPDRequired_withWrongParams() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean result;
        HOG_Maintenance_Servicing_Form__c wo = (HOG_Maintenance_Servicing_Form__c) HOG_SObjectFactory.createSObject(
                new HOG_Maintenance_Servicing_Form__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );

        Test.startTest();
        try {
            result = new EPD_EPDService().isEPDRequired(UserInfo.getUserId(), wo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void isEPDRequired_mocked_true() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean result;
        Boolean expectedResult = true;
        HOG_Maintenance_Servicing_Form__c wo = (HOG_Maintenance_Servicing_Form__c) HOG_SObjectFactory.createSObject(
                new HOG_Maintenance_Servicing_Form__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_DAOProvider.equipmentEngineDAO = new EquipmentEngineMockFor_isEPDRequired();
        EPD_DAOProvider.engineConfigDAO = new EngineConfigMockFor_isEPDRequired();

        Test.startTest();
        try {
            result = new EPD_EPDService().isEPDRequired(UserInfo.getUserId(), wo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isEPDRequired_mocked_false() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean result;
        Boolean expectedResult = false;
        HOG_Maintenance_Servicing_Form__c wo = (HOG_Maintenance_Servicing_Form__c) HOG_SObjectFactory.createSObject(
                new HOG_Maintenance_Servicing_Form__c(),
                EPD_FieldDefaultsWrong.CLASS_NAME,
                false
        );
        EPD_DAOProvider.equipmentEngineDAO = new EquipmentEngineMockFor_isEPDRequired();
        EPD_DAOProvider.engineConfigDAO = new EngineConfigMockFor_isEPDRequired();

        Test.startTest();
        try {
            result = new EPD_EPDService().isEPDRequired(UserInfo.getUserId(), wo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isEPDRequired_mocked_withoutConfig() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean result;
        Boolean expectedResult = false;
        HOG_Maintenance_Servicing_Form__c wo = (HOG_Maintenance_Servicing_Form__c) HOG_SObjectFactory.createSObject(
                new HOG_Maintenance_Servicing_Form__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_DAOProvider.equipmentEngineDAO = new EquipmentEngineMockFor_isEPDRequired();
        EPD_DAOProvider.engineConfigDAO = new BadEngineConfigMockFor_isEPDRequired();

        Test.startTest();
        try {
            result = new EPD_EPDService().isEPDRequired(UserInfo.getUserId(), wo);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    public class EquipmentEngineMockFor_isEPDRequired implements EquipmentEngineDAO {

        public List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId) {
            return new List<Equipment_Engine__c> {
                    (Equipment_Engine__c) HOG_SObjectFactory.createSObject(
                            new Equipment_Engine__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            };
        }

        public List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds) {return null;}
    }
    public class EngineConfigMockFor_isEPDRequired implements EPD_EngineConfigDAO {

        public List<EPD_Engine__mdt> getEngineConfig(String manufacturer, String model) {
            return new List<EPD_Engine__mdt> {
                    new EPD_Engine__mdt(
                            Manufacturer__c = 'Stark Industries',
                            Model__c = 'Mark 85'
                    )
            };
        }

        public List<EPD_Engine__mdt> getEngineConfigs() {return null;}
    }
    public class BadEngineConfigMockFor_isEPDRequired implements EPD_EngineConfigDAO {

        public List<EPD_Engine__mdt> getEngineConfig(String manufacturer, String model) {
            return new List<EPD_Engine__mdt> ();
        }

        public List<EPD_Engine__mdt> getEngineConfigs() {return null;}
    }

    @IsTest
    static void sendThresholdAlertsForWOs_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EPDService().sendThresholdAlertsForWOs(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void sendThresholdAlertsForWOs_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;

        Test.startTest();
        try {
            new EPD_EPDService().sendThresholdAlertsForWOs(new Set<Id>());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void getIdForLastEPDOnEngine_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id expectedResult;
        Id result;

        Test.startTest();
        try {
            result = new EPD_EPDService().getIdForLastEPDOnEngine(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getIdForLastEPDOnEngine_withWrongParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id expectedResult;
        Id result;

        Test.startTest();
        try {
            result = new EPD_EPDService().getIdForLastEPDOnEngine(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getIdForLastEPDOnEngine_mocked() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Id expectedResult; //result will be null as in mock I can't generate proper ID.
        Id result;
        EPD_DAOProvider.EPDDAO = new EPDMockFor_getIdForLastEPDOnEngine();

        Test.startTest();
        try {
            result = new EPD_EPDService().getIdForLastEPDOnEngine(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    public class EPDMockFor_getIdForLastEPDOnEngine implements EPD_EPDDAO {

        public List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId) {
            return new List<EPD_Engine_Performance_Data__c> {
                    (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(new EPD_Engine_Performance_Data__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false)
            };
        }

        public List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId) {return null;}
        public List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds) {return null;}
        public List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds) {return null;}
        public Integer getCountOfSubmittedRecords(Id workOrderActivityId) {return 0;}
    }

}