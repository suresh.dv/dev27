@isTest
public class DEFT_EOJ_EditExtensionTest {

    @isTest
    static void testEOJEditPage() {

        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();
        HOG_Service_Rig_Program__c serviceRigProgram;

        System.runAs(serviceRigPlanner) {
            serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
            serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
            serviceRigProgram.Technical_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.First_Financial_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.Execution_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.Status__c = 'Started';
            update serviceRigProgram;

            DEFT_UtilitiesTest.approveSrpFinancial(serviceRigProgram, runningUser);
            DEFT_UtilitiesTest.approveSrpPET(serviceRigProgram, runningUser);
        }

        System.runAs(runningUser) {
            HOG_EOJ__c eoj = DEFT_TestData.createEOJReport(serviceRigProgram, 'New');

            ApexPages.StandardController std = new ApexPages.StandardController(eoj);
            DEFT_EOJ_EditExtension extension = new DEFT_EOJ_EditExtension(std);

            System.assert(extension.isSubmitted() == null);

            eoj.Status__c = 'Submitted';
            update eoj;

            std = new ApexPages.StandardController(eoj);
            extension = new DEFT_EOJ_EditExtension(std);

            System.assert(extension.isSubmitted() != null);
        }
    }

    @isTest
    static void testEOJNewPage() {

        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

        HOG_Service_Rig_Program__c serviceRigProgram;

        System.runAs(serviceRigPlanner) {
            serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
            serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
            serviceRigProgram.Technical_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.First_Financial_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.Execution_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.Status__c = 'Started';
            update serviceRigProgram;

            DEFT_UtilitiesTest.approveSrpFinancial(serviceRigProgram, runningUser);
            DEFT_UtilitiesTest.approveSrpPET(serviceRigProgram, runningUser);
        }

        System.runAs(runningUser) {
            HOG_EOJ__c eojPrev = DEFT_TestData.createEOJReport(serviceRigProgram, 'New');
            eojPrev.Pump_Elastomer1__c = 'pumptest';
            update eojPrev;
            HOG_EOJ__c eoj = new HOG_EOJ__c();
            eoj.Service_Rig_Program__c = serviceRigProgram.Id;
            ApexPages.StandardController std = new ApexPages.StandardController(eoj);
            System.assert(eojPrev.Pump_Elastomer2__c == eoj.Pump_Elastomer1__c);
        }
    }

    @isTest
    static void testRigCompanyDependentPicklist() {

        User runningUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

        HOG_Service_Rig_Program__c serviceRigProgram;

        System.runAs(serviceRigPlanner) {
            serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
            serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
            serviceRigProgram.Technical_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.First_Financial_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.Execution_Approver__c = serviceRigPlanner.Id;
            serviceRigProgram.Status__c = 'Started';
            update serviceRigProgram;

            DEFT_UtilitiesTest.approveSrpFinancial(serviceRigProgram, runningUser);
            DEFT_UtilitiesTest.approveSrpPET(serviceRigProgram, runningUser);
        }

        System.runAs(runningUser) {
            HOG_EOJ__c eojPrev = DEFT_TestData.createEOJReport(serviceRigProgram, 'New');
            eojPrev.Pump_Elastomer1__c = 'pumptest';
            update eojPrev;

            HOG_EOJ__c eoj = new HOG_EOJ__c();
            eoj.Service_Rig_Program__c = serviceRigProgram.Id;

            ApexPages.StandardController std = new ApexPages.StandardController(eoj);
            DEFT_EOJ_EditExtension extension = new DEFT_EOJ_EditExtension(std);

            extension.companyId = eojPrev.Rig_Company__c;
            extension.rigId = eojPrev.Rig__c;

            List<SelectOption> companiesSO = extension.getRigCompanies();
            System.assertEquals(2, companiesSO.size());

            List<SelectOption> rigsSO = extension.getRigs();
            System.assertEquals(2, rigsSO.size());

            extension.populateCompanyId();
            extension.populateRigId();
            System.assertEquals(false, extension.getPicklistDisabled());
            extension.saveRecord();
            extension.companyId = '000000000000000000';
            extension.saveRecord();
        }
    }
}