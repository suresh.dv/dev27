/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WOA_ListView_Endpoint class
History:        jschn 31/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_ListView_EndpointTest {

    @IsTest
    static void getRecordsByFilter_nullError() {
        List<HOG_Maintenance_Servicing_Form__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = VTT_WOA_ListView_Endpoint.getRecordsByFilter(null, null, null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getRecordsByFilter_emptyError() {
        List<HOG_Maintenance_Servicing_Form__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = VTT_WOA_ListView_Endpoint.getRecordsByFilter('', null, null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getRecordsByFilter_mock() {
        List<HOG_Maintenance_Servicing_Form__c> result;
        Integer expectedSize = 0;
        VTT_WOA_ListView_Endpoint.woaSelector = new WOSelector_EmptyList_Mock();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_WOA_ListView_Endpoint.getRecordsByFilter(JSON.serialize(new VTT_WOA_SearchCriteria()), null, null, null);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedSize, result.size());
    }

    class WOSelector_EmptyList_Mock implements VTT_WOA_ListViewDAO {
        public List<HOG_Maintenance_Servicing_Form__c> getRecordsByFilter(VTT_WOA_SearchCriteria filter, String sorting, Integer offset, Integer queryLimit) {
            return new List<HOG_Maintenance_Servicing_Form__c>();
        }
    }

    @IsTest
    static void runAutoAssignmentOnWOAs_null() {
        String result;
        String expectedResult = 'SUCCESS';
        VTT_WOA_ListView_Endpoint.woaSelector = new WOSelector_EmptyList_Mock();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_WOA_ListView_Endpoint.runAutoAssignmentOnWOAs(null);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void runAutoAssignmentOnWOAs_empty() {
        String result;
        String expectedResult = 'SUCCESS';
        VTT_WOA_ListView_Endpoint.woaSelector = new WOSelector_EmptyList_Mock();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_WOA_ListView_Endpoint.runAutoAssignmentOnWOAs(new List<Work_Order_Activity__c>());
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void runAutoAssignmentOnWOAs_success() {
        String result;
        String expectedResult = 'SUCCESS';
        VTT_WOA_ListView_Endpoint.woaSelector = new WOSelector_EmptyList_Mock();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            Test.startTest();
            try {
                result = VTT_WOA_ListView_Endpoint.runAutoAssignmentOnWOAs(new List<Work_Order_Activity__c>{activity});
            } catch (Exception ex) {
                System.debug(ex.getMessage());
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void startWork_nullError() {
        Contact result;
        VTT_WOA_ListView_Endpoint.woaSelector = new WOSelector_EmptyList_Mock();
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = VTT_WOA_ListView_Endpoint.startWork(null);
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void startWork_emptyError() {
        Contact result;
        VTT_WOA_ListView_Endpoint.woaSelector = new WOSelector_EmptyList_Mock();
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = VTT_WOA_ListView_Endpoint.startWork(new Contact());
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void startWork_success() {
        Contact result;
        VTT_WOA_ListView_Endpoint.woaSelector = new WOSelector_EmptyList_Mock();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Work_Order_Activity__c activity;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);

            Test.startTest();
            try {
                result = VTT_WOA_ListView_Endpoint.startWork(tradesman);
            } catch (Exception ex) {
                System.debug(ex.getMessage());
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
    }

}