/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for FM_TruckTripCancelRedirectCtrlLtng
History:        jschn 2019-01-30 - Created.
*************************************************************************************************/
@IsTest
private class FM_TruckTripCancelRedirectCtrlLtngTest {

    @IsTest
    static void testRedirect_vendorUser() {
        User runningUser = createUser(TestUserRole.VENDOR);
        FM_Truck_Trip__c truckTrip = null;

        Test.startTest();
        HOG_CustomResponse response = null;
        System.runAs(runningUser) {
            truckTrip = createTruckTrip(TruckTripStatus.CORRECT);
            response = FM_TruckTripCancelRedirectCtrlLtng.checkCancelRedirect(truckTrip.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.success);
        System.assert(response.resultObjects != null);
        System.assertEquals(1, response.resultObjects.size());
        System.assert(String.isNotBlank(String.valueOf(response.resultObjects.get(0))));
        System.assert(String.valueOf(response.resultObjects.get(0)).contains(truckTrip.Id));
    }

    @IsTest
    static void testRedirect_huskyUser() {
        User runningUser = createUser(TestUserRole.HUSKY);
        FM_Truck_Trip__c truckTrip = null;

        Test.startTest();
        HOG_CustomResponse response = null;
        System.runAs(runningUser) {
            truckTrip = createTruckTrip(TruckTripStatus.CORRECT);
            response = FM_TruckTripCancelRedirectCtrlLtng.checkCancelRedirect(truckTrip.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.success);
        System.assert(response.resultObjects != null);
        System.assertEquals(1, response.resultObjects.size());
        System.assert(String.isNotBlank(String.valueOf(response.resultObjects.get(0))));
        System.assert(String.valueOf(response.resultObjects.get(0)).contains(truckTrip.Id));

    }

    @IsTest
    static void testError_userWithoutRole() {
        User runningUser = createUser(TestUserRole.NO_ROLE);
        FM_Truck_Trip__c truckTrip = null;

        Test.startTest();
        HOG_CustomResponse response = null;
        System.runAs(runningUser) {
            truckTrip = createTruckTrip(TruckTripStatus.CORRECT);
            response = FM_TruckTripCancelRedirectCtrlLtng.checkCancelRedirect(truckTrip.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assert(response.resultObjects == null);
        System.assert(response.errors != null);
        System.assertEquals(1, response.errors.size());
        System.assert(String.isNotBlank(response.errors.get(0)));
        System.assert(response.errors.get(0).equals(HOG_UserInfoLtng.ERROR_USER_WITHOUT_ROLE));
    }

    @IsTest
    static void testError_nonExistingTruckTrip() {
        User runningUser = createUser(TestUserRole.HUSKY);
        FM_Truck_Trip__c truckTrip = null;

        Test.startTest();
        HOG_CustomResponse response = null;
        System.runAs(runningUser) {
            truckTrip = createTruckTrip(TruckTripStatus.CORRECT);
            Id truckTripId = truckTrip.Id;
            delete truckTrip;
            response = FM_TruckTripCancelRedirectCtrlLtng.checkCancelRedirect(truckTripId);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assert(response.resultObjects == null);
        System.assert(response.errors != null);
        System.assertEquals(1, response.errors.size());
        System.assert(String.isNotBlank(response.errors.get(0)));
        System.assert(response.errors.get(0).equals(FM_TruckTripCancelRedirectCtrlLtng.ERROR_UNKNOWN_TRUCK_TRIP));
    }

    @IsTest
    static void testError_vendorUserWrongTruckTripStatus() {
        User runningUser = createUser(TestUserRole.VENDOR);
        FM_Truck_Trip__c truckTrip = null;

        Test.startTest();
        HOG_CustomResponse response = null;
        System.runAs(runningUser) {
            truckTrip = createTruckTrip(TruckTripStatus.WRONG);
            response = FM_TruckTripCancelRedirectCtrlLtng.checkCancelRedirect(truckTrip.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assert(response.resultObjects == null);
        System.assert(response.errors != null);
        System.assertEquals(1, response.errors.size());
        System.assert(String.isNotBlank(response.errors.get(0)));
        System.assert(response.errors.get(0).equals(FM_CancelTruckTripControllerX.ERROR_WRONG_STATUS_FOR_VENDOR));
    }

    @IsTest
    static void testError_withoutRoleUserWrongTruckTripStatus() {
        User runningUser = createUser(TestUserRole.NO_ROLE);
        FM_Truck_Trip__c truckTrip = null;

        Test.startTest();
        HOG_CustomResponse response = null;
        System.runAs(runningUser) {
            truckTrip = createTruckTrip(TruckTripStatus.WRONG);
            response = FM_TruckTripCancelRedirectCtrlLtng.checkCancelRedirect(truckTrip.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assert(response.resultObjects == null);
        System.assert(response.errors != null);
        System.assertEquals(1, response.errors.size());
        System.assert(String.isNotBlank(response.errors.get(0)));
        System.assert(response.errors.get(0).equals(HOG_UserInfoLtng.ERROR_USER_WITHOUT_ROLE));

    }

    private enum TestUserRole {
        HUSKY,
        VENDOR,
        NO_ROLE
    }

    private enum TruckTripStatus {
        WRONG,
        CORRECT
    }

    private static User createUser(TestUserRole usrRole) {
        User usr = HOG_TestDataFactory.createUser('First', 'second', 'nck');
        if(usrRole == TestUserRole.HUSKY) {
            usr = HOG_TestDataFactory.assignRole(usr, 'Dispatcher');
        } else if (usrRole == TestUserRole.VENDOR) {
            usr = HOG_TestDataFactory.assignRole(usr, FM_Utilities.FLUID_MANAGEMENT_VENDOR_USER_ROLE);
        } else if(usrRole == TestUserRole.NO_ROLE) {
            //do nothing
        }
        return usr;
    }

    private static FM_Truck_Trip__c createTruckTrip(TruckTripStatus status) {
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
        Field__c field = HOG_TestDataFactory.createAMU(true);

        Route__c route = HOG_TestDataFactory.createRoute(true);
        Carrier__c carrier = FM_TestDataFactory.createCarrier(acc.Id, true);
        Carrier_Unit__c carrierUnit = FM_TestDataFactory.createCarrierUnit(carrier.Id, true);
        Location__c loc = HOG_TestDataFactory.createLocation(route.Id, field.Id, true);


        FM_Run_Sheet__c rs = FM_TestDataFactory.createRunSheetOnLocation(loc.Id, true);

        FM_Load_Request__c lr = FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null, null,
                'O',
                rs.Id,
                'Night',
                loc.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED,
                true);
        FM_Truck_Trip__c truckTrip = [SELECT Name FROM FM_Truck_Trip__c WHERE Load_Request__c =: lr.Id LIMIT 1];
        truckTrip.Unit__c = carrierUnit.Id;
        update truckTrip;
        if(status == TruckTripStatus.CORRECT) {
            truckTrip.Truck_Trip_Status__c = FM_TruckTrip_Utilities.STATUS_EXPORTED;
        } else if(status == TruckTripStatus.WRONG) {
            truckTrip.Truck_Trip_Status__c = FM_TruckTrip_Utilities.STATUS_NEW;
        }
        update truckTrip;
        return truckTrip;
    }

}