/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_CalendarService class
History:        mbrimus 18/11/2019 - Created.
*************************************************************************************************/
@IsTest(IsParallel=true)
private class VTT_LTNG_CalendarServiceTest {

    @IsTest
    static void getEntryDataForDate() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.calendarDao = new CalendarDao();
        result = new VTT_LTNG_CalendarService().getEntryDataForDate('2019-10-12', '');

        Test.stopTest();
    }

    @IsTest
    static void getCurrentUserData() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.calendarDao = new CalendarDao();
        result = new VTT_LTNG_CalendarService().getCurrentUserData();

        Test.stopTest();
    }

    @IsTest
    static void getVendorAccounts() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.calendarDao = new CalendarDao();
        result = new VTT_LTNG_CalendarService().getVendorAccounts();

        Test.stopTest();
    }

    @IsTest
    static void getTradesmanForAccount() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.calendarDao = new CalendarDao();
        result = new VTT_LTNG_CalendarService().getTradesmanForAccount(null);

        Test.stopTest();
    }

    @IsTest
    static void getOnHoldActivities() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.calendarDao = new CalendarDao();
        result = new VTT_LTNG_CalendarService().getOnHoldActivities(null);

        Test.stopTest();
    }

    @IsTest
    static void getTradesmanEvents() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.calendarDao = new CalendarDao();
        result = new VTT_LTNG_CalendarService().getTradesmanEvents(null, null, null);

        Test.stopTest();
    }

    @IsTest
    static void getTradesmanEvents_targetAddAutoCompletedActivitiesAsEvent() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.calendarDao = new CalendarDaoNegative();
        result = new VTT_LTNG_CalendarService().getTradesmanEvents(null, null, null);

        Test.stopTest();
    }

    @IsTest
    static void testSummaryModel_Sort() {

        VTT_WorkOrderSummaryModel firstModel = new VTT_WorkOrderSummaryModel();
        firstModel.WorkOrderName = '1';
        firstModel.LocationName = 'TestLoca';

        VTT_WorkOrderSummaryModel secondModel = new VTT_WorkOrderSummaryModel();
        firstModel.WorkOrderName = '2';
        firstModel.LocationName = 'TestLoca';

        System.assertEquals(1, firstModel.compareTo(secondModel));
    }

    public class CalendarDao implements VTT_LTNG_CalendarDAO {

        public List<Work_Order_Activity_Log_Entry__c> getLogEntriesForTradesmanOnDate(String tradesmanId, Date currDate) {
            return VTT_TestDataFactory.createLogEntries(10, false, null);
        }

        public List<Account> getVendorAccounts() {
            return new List<Account>{
                    (Account) HOG_SObjectFactory.createSObject(new Account())
            };
        }

        public List<Contact> getTradesmanForAccount(String accountId) {
            return new List<Contact>{
                    (Contact) HOG_SObjectFactory.createSObject(new Contact())
            };
        }

        public List<Work_Order_Activity_Log__c> getActivityLogsForTradesman(String tradesmanID, Date startDate, Date endDate) {
            return new List<Work_Order_Activity_Log__c>{
                    VTT_TestDataFactory.createLogOnDateForTradesman(null, null, false)
            };
        }

        public List<Work_Order_Activity_Log__c> getAutoCompletedActivities(Set<Id> workLogActivitySet) {
            return new List<Work_Order_Activity_Log__c>{
                    VTT_TestDataFactory.createLogOnDateForTradesman(null, System.today(), false)
            };
        }

        public List<Work_Order_Activity__c> getScheduledActivities(Set<Id> workedActivities, String tradesmanId, Date startDate, Date endDate) {
            return new List<Work_Order_Activity__c>{
                    (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(),
                            VTT_FieldDefaults.CLASS_NAME)
            };
        }

        public List<Work_Order_Activity__c> getOnHoldActivities(String tradesmanID) {
            return new List<Work_Order_Activity__c>{
                    (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(),
                            VTT_FieldDefaults.CLASS_NAME)
            };
        }

        public Contact getTradesmanInfo() {
            return (Contact) HOG_SObjectFactory.createSObject(new Contact());
        }

        public Map<Id, PermissionSet> getSupervisorPermissionSets() {
            return new Map<Id, PermissionSet>{
                    Id.valueOf('001xa000003DIlo') => new PermissionSet()
            };
        }

        public List<PermissionSetAssignment> getPermissionSetAssignmentsOfCurrentUser(Set<Id> psIds) {
            return new List<PermissionSetAssignment>{
                    new PermissionSetAssignment()
            };
        }

        public Boolean isUserSystemAdmin() {
            return true;
        }
    }

    public class CalendarDaoNegative implements VTT_LTNG_CalendarDAO {

        public List<Work_Order_Activity_Log_Entry__c> getLogEntriesForTradesmanOnDate(String tradesmanId, Date currDate) {
            return VTT_TestDataFactory.createLogEntries(10, false, null);
        }

        public List<Account> getVendorAccounts() {
            return new List<Account>{
                    (Account) HOG_SObjectFactory.createSObject(new Account())
            };
        }

        public List<Contact> getTradesmanForAccount(String accountId) {
            return new List<Contact>{
                    (Contact) HOG_SObjectFactory.createSObject(new Contact())
            };
        }

        public List<Work_Order_Activity_Log__c> getActivityLogsForTradesman(String tradesmanID, Date startDate, Date endDate) {
            return new List<Work_Order_Activity_Log__c>{
                    VTT_TestDataFactory.createLogOnDateForTradesman(null, null, false)
            };
        }

        public List<Work_Order_Activity_Log__c> getAutoCompletedActivities(Set<Id> workLogActivitySet) {
            Work_Order_Activity_Log__c log = VTT_TestDataFactory.createLogOnDateForTradesman(null, System.today(), false);
            log.Started_New__c = System.today().addDays(-5);
            log.Finished_New__c = null;
            return new List<Work_Order_Activity_Log__c>{
                    log
            };
        }

        public List<Work_Order_Activity__c> getScheduledActivities(Set<Id> workedActivities, String tradesmanId, Date startDate, Date endDate) {
            return new List<Work_Order_Activity__c>{
                    (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(),
                            VTT_FieldDefaults.CLASS_NAME)
            };
        }

        public List<Work_Order_Activity__c> getOnHoldActivities(String tradesmanID) {
            return new List<Work_Order_Activity__c>{
                    (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(),
                            VTT_FieldDefaults.CLASS_NAME)
            };
        }

        public Contact getTradesmanInfo() {
            return (Contact) HOG_SObjectFactory.createSObject(new Contact());
        }

        public Map<Id, PermissionSet> getSupervisorPermissionSets() {
            return new Map<Id, PermissionSet>{
                    Id.valueOf('001xa000003DIlo') => new PermissionSet()
            };
        }

        public List<PermissionSetAssignment> getPermissionSetAssignmentsOfCurrentUser(Set<Id> psIds) {
            return new List<PermissionSetAssignment>{
                    new PermissionSetAssignment()
            };
        }

        public Boolean isUserSystemAdmin() {
            return true;
        }
    }


}