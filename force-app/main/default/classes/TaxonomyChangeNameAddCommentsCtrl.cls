public with sharing class TaxonomyChangeNameAddCommentsCtrl{
    public PREP_Change_Detail__c detail { get; set; }
    public PREP_Material_Service_Group__c taxonomy { get; set; }
    public Material_Service_Group_Clone__c taxonomyClone { get; set; }
    String headerId;
    
    public TaxonomyChangeNameAddCommentsCtrl(){
        headerId = ApexPages.currentPage().getParameters().get( 'Id' );
        
        if( headerId != null && headerId != '' ){
            List<PREP_Change_Detail__c> detailRecords = [ SELECT Id, Name, PREP_Change_Header__r.ID, Field_Name__c, From_Field_value__c, Record_Id__c,
                                                            To_Field_value__c, Update_Record_Name__c, Update_Record_Id__c, PREP_Change_Header__c,
                                                            From_Number__c, To_Number__c, To_Field_Id__c, From_Field__c, To_Field__c FROM PREP_Change_Detail__c
                                                            WHERE PREP_Change_Header__c = :headerId ];
            if( detailRecords.size() > 0 ){
                detail = detailRecords[0];
                
                List<PREP_Material_Service_Group__c> taxonomyList = [ SELECT Id, Name, Owner.Name, Material_Service_Group_Name__c, Category__c, Discipline__c,
                                                                        SAP_Short_Text_Name__c, Type__c, Includes__c, Category_Manager__r.Name, Additional_Comments__c,
                                                                        Sub_Category__r.Category__r.Name, Category_Specialist__r.Name, Sub_Category__r.Name,
                                                                        Category_Link__c, Category_Link__r.Name, Discipline_Link__c, Discipline_Link__r.Name,
                                                                        Sub_Category__c FROM PREP_Material_Service_Group__c where Id =: detail.Update_Record_Id__c ];   
                if( taxonomyList.size() > 0 ){
                    taxonomy = taxonomyList[0];
                }
                else {
                    List<Material_Service_Group_Clone__c> matClone = [ Select Id, Name, Material_Service_Group_Name__c, SAP_Short_Text_Name__c, Sub_Category__c,
                                                        Active__c, Category_Link__c, Type__c, Discipline__c, Category_Manager__c, Category_Specialist__c,
                                                        GL_Account__c, GL_Account__r.Name, GL_Account_Number__c, Includes__c, Does_Not_Include__c, Additional_Comments__c from Material_Service_Group_Clone__c
                                                        where Id = :detail.To_Field_Id__c ];
                
                    if( matClone.size() > 0 )
                        taxonomyClone = matClone[0];
                }
            }        
        }
    }
    
    public PageReference saveTaxonomy(){
        if( taxonomy != null )
            update taxonomy;
        
        if( taxonomyClone != null )
            update taxonomyClone;
                    
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
    
    public PageReference cancelTaxonomy(){
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect( true );
        return rageRef;
    }
}