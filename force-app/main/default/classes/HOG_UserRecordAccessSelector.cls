/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Contains queries for UserRecordAccess SObject
Test Class:     HOG_UserRecordAccessSelectorTest
History:        jschn 2019-07-04 - Created.
*************************************************************************************************/
public with sharing class HOG_UserRecordAccessSelector {

    /**
     * Returns number of User Record Access records per User and record.
     * 0 means no access
     * 1 means has access
     *
     * @param recordId
     * @param userId
     *
     * @return Integer
     */
    public Integer countOfUserEditAccessPerSObject(Id recordId, Id userId) {
        return [
                SELECT RecordId
                FROM UserRecordAccess
                WHERE UserId =: userId
                AND RecordId =: recordId
                AND HasEditAccess = TRUE
        ].size();
    }

}