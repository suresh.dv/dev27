/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database queries for VTT Lightning WorkFlow Engine
Test Class:     VTT_LTNG_WorkFlowEngineSelectorTest
History:        mbrim 2019-09-03 - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_WorkFlowEngineSelector implements VTT_LTNG_WorkFlowEngineDAO {

    public List<Work_Order_Activity__c> getActivitiesWithAssignment(String woaId) {
        return [
                SELECT
                        Id,
                        Name,
                        Status__c,
                        Status_Reason__c,
                        LastModifiedById,
                        Gas_Permit__c,
                        Equipment__c,
                        Electrical_Permit__c,
                        Estimated_Rescheduled_Date__c,
                        Damage_Text__c,
                        Cause_Text__c,
                        Maintenance_Work_Order__c,
                        Maintenance_Work_Order__r.MAT_Code__c,
                        Maintenance_Work_Order__r.Notification_Number__c,
                        Maintenance_Work_Order__r.Facility__c,
                        Maintenance_Work_Order__r.HOG_Service_Request_Notification_Form__c,
                        Maintenance_Work_Order__r.Functional_Location__c,
                        Maintenance_Work_Order__r.Location__c,
                        Maintenance_Work_Order__r.Well_Event__c,
                        Maintenance_Work_Order__r.System__c,
                        Maintenance_Work_Order__r.Sub_System__c,
                        Maintenance_Work_Order__r.Functional_Equipment_Level__c,
                        Maintenance_Work_Order__r.Yard__c,
                        Maintenance_Work_Order__r.Equipment__c,
                        Maintenance_Work_Order__r.Equipment__r.Catalogue_Code__c,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c,
                        Maintenance_Work_Order__r.Operating_District_Lookup__c,
                        Maintenance_Work_Order__r.Business_Unit_Lookup__c,
                        Part__c,
                        Assigned_Vendor__r.Name,
                        Assigned_Text__c,
                        Maintenance_Work_Order__r.Planner_Group__c,
                        Maintenance_Work_Order__r.Order_Type__c,
                        Maintenance_Work_Order__r.EPD_Suppressed__c,
                        Maintenance_Work_Order__r.Work_Order_Priority_Number__c,
                        Maintenance_Work_Order__r.User_Status_Code__c,
                        Maintenance_Work_Order__r.Plant_Section__c,
                        Maintenance_Work_Order__r.Name,
                        Work_Center__c,
                        Location_Name__c,
                        Daily_Logs__c,
                        Part__r.Part_Code__c,
                        Part__r.Part_Description__c,
                        Scheduled_Start_Date__c,
                        Damage__c,
                        Cause__c,
                        Location__c,
                        Facility__c,
                        SAP_Deleted__c,
                        LastModifiedDate,
                        Maintenance_Work_Order__r.Work_Order_Number__c,
                        Maintenance_Work_Order__r.On_Hold__c,
                        Operating_Field_AMU__c,
                        Operating_Field_AMU__r.Is_Thermal__c,
                        Assigned_Vendor__c, (
                        SELECT
                                Id
                        FROM Work_Order_Activity_Assignments__r
                        WHERE Rejected__c <> TRUE AND Tradesman__r.User__c = :UserInfo.getUserId()
                        LIMIT 1
                ), (
                        SELECT Id
                        FROM Working_Tradesmen__r
                )
                FROM Work_Order_Activity__c
                WHERE Id = :woaId
                LIMIT 1
        ];
    }

    public List<Contact> getTradesmanContacts() {
        return [
                SELECT Id, Name, Tradesman_Status__c, AccountId, Account.Name, Tradesman_Status_Date__c,
                        Current_Work_Order_Activity__c, Last_Work_Order_Activity__c,
                        Tradesman_Still_Working__c,
                        Current_Work_Order_Activity__r.Name,
                        Tradesman_Start_Work_Enabled__c
                FROM Contact
                WHERE User__c = :UserInfo.getUserId()
        ];
    }

    public List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesmanAndActivity(String woaId, String tradesmanId) {
        return [
                SELECT Id, Name, Status__c, TimeStamp__c
                FROM Work_Order_Activity_Log_Entry__c
                WHERE Work_Order_Activity__c = :woaId
                AND Work_Order_Activity_Log_Lookup__r.Tradesman__c = :tradesmanId
                ORDER BY Name DESC
                LIMIT 1
        ];
    }

    public Work_Order_Activity__c getLatestVersionOfActivity(String woaId) {
        List<Work_Order_Activity__c> result = [
                SELECT Id, LastModifiedById, LastModifiedDate, (
                        SELECT Id, Name, Tradesman_Status__c, Account.Name,
                                Phone, Tradesman_Status_Date__c
                        FROM Working_Tradesmen__r
                )
                FROM Work_Order_Activity__c
                WHERE Id = :woaId
                LIMIT 1
        ];

        if (result.size() == 1) {
            return result[0];
        } else {
            return new Work_Order_Activity__c();
        }
    }

    public List<HOG_Work_Execution_Close_Out_Checklist__c> getCloseOutChecklistForToday(Work_Order_Activity__c activity, Contact tradesman) {
        return [
                SELECT
                        Id,
                        Permit_Holder_Name__c,
                        Q1_1_Reviewed_work_package__c,
                        Q1_2_Valid_procedures_for_tasks__c,
                        Q2_1_Reviewed_the_work_permit__c,
                        Q2_2_Site_hazard_assessment__c,
                        Q2_3_Completed_a_job_hazard_assessment__c,
                        Q3_1_Reviewed_the_applied_lock_out_tag__c,
                        Q3_2_Equipment_at_a_zero_energy_state__c,
                        Q4_1_Reviewed_confined_rescue_plan__c,
                        Q5_1_Reported_deviations__c,
                        Q5_2_Reported_unsafe_activities__c,
                        Q6_1_Reported_work_scope_changes__c,
                        Q6_2_Work_scope_change_occurred__c,
                        Q7_1_Submitted_close_out_documents__c,
                        Q7_2_Notified_Maintenance_personnel__c,
                        Q8_1_Notified_permit_issuer__c,
                        Q8_2_Submitted_hard_copy_of_job_hazard__c,
                        Q8_3_Signed_copy_of_the_work_permit__c,
                        Maintenance_Work_Order__c,
                        Company_Name_Lookup__c,
                        Work_Order_Activity__c,
                        Permit_Number__c
                FROM HOG_Work_Execution_Close_Out_Checklist__c
                WHERE Maintenance_Work_Order__c = :activity.Maintenance_Work_Order__c
                AND CreatedDate = TODAY
                AND Company_Name_Lookup__c = :tradesman.AccountId
                LIMIT 1
        ];
    }

    public List<Contact> getAssignedTradesman(Id workOrderActivityId, Id userId) {
        Set<Id> assignedTradesmanIdSet = new Set<Id>();
        List<Contact> assignedTradesmenList = new List<Contact>();

        for (Work_Order_Activity_Assignment__c woaa : [
                SELECT Id, Tradesman__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Rejected__c <> TRUE
                AND Work_Order_Activity__c = :workOrderActivityId
        ]) {
            assignedTradesmanIdSet.add(woaa.Tradesman__c);
        }

        for (Contact tradesmancontact : [
                SELECT
                        Id,
                        Name,
                        Phone,
                        Email,
                        Tradesman_Status__c,
                        Current_Work_Order_Activity__c,
                        Tradesman_Status_Date__c,
                        User__c
                FROM Contact
                WHERE Id IN :assignedTradesmanIdSet
                ORDER BY Name
        ]) {
            if (tradesmancontact.User__c <> userId) {
                assignedTradesmenList.add(tradesmancontact);
            }
        }

        List<Contact> tradesmanListForUpdate = new List<Contact>();
        for (Contact tradesmenRec : assignedTradesmenList) {
            //update statuses for assigned tradesmen only if they currently working on the same activity
            if (tradesmenRec.Current_Work_Order_Activity__c == workOrderActivityId) {
                tradesmanListForUpdate.add(tradesmenRec);
            }
        }
        return tradesmanListForUpdate;
    }

    public List<Work_Order_Activity_Assignment__c> getNonRejectedAssignment(
            Id activityId,
            Id tradesmanId) {
        return [
                SELECT Id, Tradesman__c, Rejected__c, Reject_Reason__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Tradesman__c = :tradesmanId
                AND Work_Order_Activity__c = :activityId
                AND Rejected__c <> TRUE
                LIMIT 1
        ];
    }

    public List<Contact> getOtherWorkingTradesman(
            Work_Order_Activity__c workOrderActivity,
            List<Contact> assignedTradesmenList,
            Contact tradesman) {
        List<Contact> otherTradesmenStillWorking = new List<Contact>();
        //Get Log Entries for the assigned tradesman on this activity
        for (Contact assignedTradesman : [
                SELECT Id, Name, Tradesman_Status__c, Account.Name,
                        Phone, Tradesman_Status_Date__c, (
                        SELECT Id, Name, Status__c
                        FROM Work_Order_Activity_Log_Entries__r
                        WHERE Work_Order_Activity__c = :workOrderActivity.Id
                        ORDER BY TimeStamp__c DESC
                )
                FROM Contact
                WHERE Id IN :assignedTradesmenList
                ORDER BY Name
        ]) {
            //if no log entries for current activity assigned tradesman not working
            if (assignedTradesman.Work_Order_Activity_Log_Entries__r == null || assignedTradesman.Work_Order_Activity_Log_Entries__r.isEmpty()) {
                continue;
            } //Tradesman didn't start work on this activity

            //if log entry but "Job Complete" or "Finished for the day" assigned tradesman not (finished) working
            if (assignedTradesman.Work_Order_Activity_Log_Entries__r[0].Status__c == VTT_Utilities.LOGENTRY_JOBCOMPLETE ||
                    assignedTradesman.Work_Order_Activity_Log_Entries__r[0].Status__c == VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY) {
                continue;
            } //Tradesman finished work on this activity

            //Otherwise he is working since he has logs and not Job Complete or Finished for the day
            if (assignedTradesman.Id != tradesman.Id) {
                otherTradesmenStillWorking.add(assignedTradesman);
            }
        }

        return otherTradesmenStillWorking;
    }

    public List<VTT_LTNG_AvailableActivity> getAvailableActivitiesToComplete(Contact tradesman, Work_Order_Activity__c workOrderActivity) {
        List<VTT_LTNG_AvailableActivity> activities = new List<VTT_LTNG_AvailableActivity>();

        for (Work_Order_Activity__c woaRec : [
                SELECT Id,
                        Name,
                        Status__c,
                        System_Condition__c,
                        Work__c,
                        Scheduled_Start_Date__c,
                        Scheduled_Finish_Date__c,
                        Equipment__r.Description_of_Equipment__c,
                        Equipment__r.Tag_Number__c,
                        Equipment__r.Location__c,
                        Location__c,
                        Location__r.Name,
                        User_Status__c,
                        Operating_Field_AMU__c,
                        Operating_Field_AMU__r.Is_Thermal__c,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c,
                        Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c, (
                        SELECT Id
                        FROM Working_Tradesmen__r
                        WHERE Id <> :tradesman.Id
                ), (
                        SELECT Id, Tradesman__c
                        FROM Work_Order_Activity_Assignments__r
                        WHERE Tradesman__c = :tradesman.Id AND Rejected__c = FALSE
                )
                FROM Work_Order_Activity__c
                WHERE Maintenance_Work_Order__c = :workOrderActivity.Maintenance_Work_Order__c
                AND Id <> :workOrderActivity.Id
                AND (Status__c = :VTT_Utilities.ACTIVITY_STATUS_NEW OR Status__c = :VTT_Utilities.ACTIVITY_STATUS_SCHEDULED)
                AND SAP_Deleted__c = FALSE
                //mz 3-Jan-18
                //Use specific filter on user status only if activity is thermal activity (that is if AMU Planner Group is Thermal)
                AND (((Operating_Field_AMU__c != NULL AND ((Operating_Field_AMU__r.Is_Thermal__c = TRUE AND (User_Status__c LIKE '2SCH %FIX%' OR User_Status__c LIKE '3BEX%')) OR Operating_Field_AMU__r.Is_Thermal__c = FALSE))
                OR (Operating_Field_AMU__c = NULL AND Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c != NULL AND ((Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = TRUE AND (User_Status__c LIKE '2SCH %FIX%' OR User_Status__c LIKE '3BEX%')) OR Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c = FALSE))
                OR (Operating_Field_AMU__c = NULL AND Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c = NULL)))
        ]) {
            System.debug('woaRec.Work_Order_Activity_Assignments__r.size() ' + tradesman.Id);
            System.debug('woaRec.Working_Tradesmen__r.size() ' + woaRec.Work_Order_Activity_Assignments__r.size());
            //if Activity assigned to me and there are no other tradesmen currently working on this activity, show it in the list
            if (woaRec.Work_Order_Activity_Assignments__r.size() > 0 && woaRec.Working_Tradesmen__r.size() == 0) {
                activities.add(new VTT_LTNG_AvailableActivity(woaRec));
            }
            System.debug('AvailableActivitiesToComplete.size() IS' + activities.size());
        }

        return activities;
    }

    public Double getRunningTallyTime(Work_Order_Activity__c activity, List<Work_Order_Activity_Log__c> activityLogs) {
        Double runningTally = 0;
        if (activityLogs.size() > 0) {

            // Time that WOA took up until now
            runningTally = activityLogs[0].WorkTime_New__c * 60;

            // Check if we have any opened Start at Equipment entries
            if (activityLogs[0].Work_Order_Activity_Log_Entries2__r.size() > 0) {

                // Add time of opened log entry into taly time
                Long minutesNow = System.now().getTime() / 1000 / 60;
                Long minutesLogEntry = activityLogs[0].Work_Order_Activity_Log_Entries2__r[0].TimeStamp__c.getTime() / 1000 / 60;

                runningTally += (minutesNow - minutesLogEntry);
                System.debug('runningTally ' + runningTally);
                System.debug('opened entry time ' + (minutesNow - minutesLogEntry));
            }

        }

        runningTally = Math.floor(runningTally);

        return runningTally;
    }

    public List<Work_Order_Activity_Log__c> getActivityLogs(Id activityId, Id userId) {
        return [
                SELECT Id, WorkTime_New__c, (
                        SELECT Id, TimeStamp__c, StatusEnd__c
                        FROM Work_Order_Activity_Log_Entries2__r
                        WHERE Status__c = :'Start at Equipment'
                        AND StatusEnd__c = NULL
                        AND TimeStamp__c <> NULL
                        ORDER BY CreatedDate DESC
                        LIMIT 1
                )
                FROM Work_Order_Activity_Log__c
                WHERE Work_Order_Activity__c = :activityId
                AND Tradesman__r.User__c = :userId
                ORDER BY CreatedDate DESC
        ];
    }

    public List<HOG_Part__c> getPartsByCatalogueCode(String catalogueCode) {
        return [
                SELECT
                        Id,
                        Part_Code__c,
                        Part_Description__c
                FROM HOG_Part__c
                WHERE Catalogue_Code__c = :catalogueCode
                ORDER BY Part_Description__c
        ];
    }

    public List<HOG_Damage__c> getDamageByPartCode(String partCode) {
        return [
                SELECT
                        Id,
                        Damage_Code__c,
                        Damage_Description__c
                FROM HOG_Damage__c
                WHERE Part_Code__c = :partCode
                ORDER BY Damage_Description__c
        ];
    }

    public List<HOG_Cause__c> getCause() {
        return [
                SELECT
                        Id,
                        Cause_Code__c,
                        Cause_Description__c
                FROM HOG_Cause__c
                ORDER BY Cause_Description__c
        ];
    }
}