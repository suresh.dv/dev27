global class USCP_DataCleanupSchedule implements Schedulable {
    global USCP_DataCleanupSchedule ()
    {
    }
    
    global void execute(SchedulableContext SC)
    {

        System.debug('USCP Main Record Clean Up Process Started');
        /*
        USCP_DataCleanup bol_b = new USCP_DataCleanup('BOL');
        Database.executeBatch(bol_b); 
        USCP_DataCleanup inv_b = new USCP_DataCleanup('INV');
        Database.executeBatch(inv_b);
        USCP_DataCleanup eft_b = new USCP_DataCleanup('EFT');
        Database.executeBatch(eft_b);                        
        */
        //lets chain batches
        USCP_DataCleanup eft_b = new USCP_DataCleanup('EFT'); //this would be the last one
        USCP_DataCleanup inv_b = new USCP_DataCleanup('INV', eft_b ); // after invoices we will delete EFTs
        USCP_DataCleanup bol_b = new USCP_DataCleanup('BOL', inv_b ); // after BOLs we will delete invoices
        Database.executeBatch(bol_b);  
    }
}