/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Generic Field Defaults for EPD unit test purpose
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
@SuppressWarnings('ApexUnusedDeclaration')
public class EPD_FieldDefaultsGeneral {

    public static final String CLASS_NAME = String.valueOf(EPD_FieldDefaultsGeneral.class);

    public class EPD_Engine_Performance_DataDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        Date today = Date.today();

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object>{
                    EPD_Engine_Performance_Data__c.Submitted__c => true,
                    EPD_Engine_Performance_Data__c.Required_Exhaust_Measurements__c => true,
                    EPD_Engine_Performance_Data__c.LSD__c => 'TestLSD',
                    EPD_Engine_Performance_Data__c.EI_Ignition_Timing__c => 21,
                    EPD_Engine_Performance_Data__c.EI_RPM__c => 1200,
                    EPD_Engine_Performance_Data__c.Vendor_Work_Order_Number__c => 'TestVWON',
                    EPD_Engine_Performance_Data__c.Hours__c => 1000,
                    EPD_Engine_Performance_Data__c.Planner_Group__c => '100',
                    EPD_Engine_Performance_Data__c.EOC_Crankcase_Pressure__c => 20,
                    EPD_Engine_Performance_Data__c.EOC_Oil_Pressure__c => 20,
                    EPD_Engine_Performance_Data__c.EOC_Oil_Temperature__c => 20,
                    EPD_Engine_Performance_Data__c.COC_Oil_Temperature__c => 20,
                    EPD_Engine_Performance_Data__c.COC_Oil_Pressure__c => 20,
                    EPD_Engine_Performance_Data__c.Operation_Group__c => 'CHOPS',
                    EPD_Engine_Performance_Data__c.EJW_Temperature_IN__c => 20,
                    EPD_Engine_Performance_Data__c.EJW_Temperature_OUT__c => 20,
                    EPD_Engine_Performance_Data__c.Notes__c => 'Test Notes',
                    EPD_Engine_Performance_Data__c.Data_Collected__c => today,
                    EPD_Engine_Performance_Data__c.Data_Collected_By__c => UserInfo.getUserId()
            };
        }
    }

    public class EPD_Block_InformationDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    EPD_Block_Information__c.Block_Type__c => EPD_Constants.BLOCK_TYPE_STRAIGHT,
                    EPD_Block_Information__c.Intake_Manifold_Pressure__c => 2,
                    EPD_Block_Information__c.Exhaust_O2__c => 21
            };
        }
    }

    public class EPD_Part_ReplacementDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    EPD_Part_Replacement__c.Name => 'TestPart',
                    EPD_Part_Replacement__c.Replaced__c => false,
                    EPD_Part_Replacement__c.Inspected__c => false,
                    EPD_Part_Replacement__c.Comments__c => 'TestComment'
            };
        }
    }

    public class EPD_Engine_Operating_ConditionDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    EPD_Engine_Operating_Condition__c.Block_Name__c => '',
                    EPD_Engine_Operating_Condition__c.RecordTypeId => Schema.SObjectType.EPD_Engine_Operating_Condition__c.getRecordTypeInfosByName()
                            .get(EPD_Constants.EOC_ADVANCED_RECORD_TYPE).getRecordTypeId(),
                    EPD_Engine_Operating_Condition__c.Pre_Chamber_Fuel_Pressure__c => 20,
                    EPD_Engine_Operating_Condition__c.Regulated_Fuel_Pressure__c => 20,
                    EPD_Engine_Operating_Condition__c.Supplied_Fuel_Pressure__c => 20
            };
        }
    }

    public class EPD_Cylinder_InformationDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        String recordTypeName = EPD_Constants.MEASUREMENT_TYPE_TO_CYLINDER_RECORD_TYPE_NAME.get(
                'Wear'
        );
        Id cylinderRecordType = Schema.SObjectType.EPD_Cylinder_Information__c.getRecordTypeInfosByName()
                .get(recordTypeName).getRecordTypeId();
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    EPD_Cylinder_Information__c.Cylinder_Number__c=> 1,
                    EPD_Cylinder_Information__c.Exhaust_Temperature__c => 20,
                    EPD_Cylinder_Information__c.Wear_Intake__c => 0.090,
                    EPD_Cylinder_Information__c.Wear_Exhaust__c => 0.090,
                    EPD_Cylinder_Information__c.Compression__c => 220,
                    EPD_Cylinder_Information__c.RecordTypeId => cylinderRecordType
            };
        }
    }

    public class EPD_Compressor_Operating_Condition_StageDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    EPD_Compressor_Operating_Condition_Stage__c.Stage_Number__c => 1,
                    EPD_Compressor_Operating_Condition_Stage__c.Suction_Temperature__c => 90,
                    EPD_Compressor_Operating_Condition_Stage__c.Suction_Pressure__c => 100,
                    EPD_Compressor_Operating_Condition_Stage__c.Discharge_Temperature__c => 90,
                    EPD_Compressor_Operating_Condition_Stage__c.Discharge_Pressure__c => 100
            };
        }
    }

    public class Equipment_EngineDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Equipment_Engine__c.Serial_Number__c => 'TestSerial',
                    Equipment_Engine__c.Manufacturer__c => 'UnitTestData',
                    Equipment_Engine__c.Model__c => 'UnitTestData',
                    Equipment_Engine__c.Tag__c => 'TestTag'
            };
        }
    }

    public class EquipmentDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Equipment__c.Name => 'TestEquipment',
                    Equipment__c.Serial_Number__c => 'TestSerial',
                    Equipment__c.Manufacturer__c => 'TestManufacturer',
                    Equipment__c.Model_Number__c => 'TestModel',
                    Equipment__c.Description_of_Equipment__c => 'TestDescription',
                    Equipment__c.Manufacturer_Serial_No__c => 'TestSerialNumber',
                    Equipment__c.Tag_Number__c => 'TestTag',
                    Equipment__c.Object_Type__c => (String) EPD_Constants.SUPPORTED_EQUIPMENT_OBJECT_TYPE.iterator().next()
            };
        }
    }

    public class HOG_Engine_Performance_DataDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    HOG_Engine_Performance_Data__c.Equipment_Engine_Persist_Fail_Addresses__c => 'jakub.schon@huskyenergy.com.invalid1;jakub.schon@huskyenergy.com.invalid2',
                    HOG_Engine_Performance_Data__c.PDF_Senders_Name__c => 'HOG Lloyd SF',
                    HOG_Engine_Performance_Data__c.Threshold_Alert_Cc_Addresses__c => 'jakub.schon@huskyenergy.com.invalid1;jakub.schon@huskyenergy.com.invalid2',
                    HOG_Engine_Performance_Data__c.Compression_Minor_Threshold__c => 20.0
            };
        }
    }

    public class UserDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

        Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User'];
        Double random = Math.random();

        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    User.FirstName => 'Tony',
                    User.LastName => 'Stark',
                    User.ProfileId => p.Id,
                    User.Alias => 'IronMan',
                    User.Email => 'tony' +  random + '@stark.com.test',
                    User.UserName => 'tony' + random + '@stark.com.unittest',
                    User.EmailEncodingKey => 'UTF-8',
                    User.LanguageLocaleKey => 'en_US',
                    User.LocaleSidKey => 'en_US',
                    User.TimeZoneSidKey => 'America/Denver'
            };
        }
    }

    public class AccountDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Account.Name => 'Avengers'
            };
        }
    }

    public class HOG_Maintenance_Servicing_FormDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    HOG_Maintenance_Servicing_Form__c.Work_Order_Number__c => 'Mark 85',
                    HOG_Maintenance_Servicing_Form__c.Order_Type__c => 'MPX1'
            };
        }
    }

}