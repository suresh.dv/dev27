/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for FM_LoadRequestRecreateCtrl class.
History:        jschn 06.06.2018 - Created.
**************************************************************************************************/
@isTest private class FM_LoadRequestRecreateCtrlTest {
	//TODO run as user
	private static final String CLASS_NAME = 'FM_LoadRequestRecreateCtrlTest';
	
	@isTest static void recreateOnLocationTest() {
		User dispatcher = HOG_TestDataFactory.createUser('Steve', 'Rogers', 'Captain');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher');

		System.runAs(dispatcher) {

			List<FM_Load_Request__c> loadRequests = [SELECT Id 
													FROM FM_Load_Request__c 
													WHERE Status__c != :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED
														AND Source_Location__c != null];
			Integer startingNumOfLRs = [SELECT Count() FROM FM_Load_Request__c];
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(loadRequests.get(0));
			FM_LoadRequestRecreateCtrl controller;

			Test.startTest();
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(0).Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			controller.newLoadRequest.Create_Reason__c = FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault().get(0).getValue();
			controller.oldLoadRequest.Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
			controller.tank = controller.tankOptions.get(0).getValue();
			controller.updateLoadRequestTankInfo();
			controller.recreate();
			Test.stopTest();

			Integer finalNumOfLRs = [SELECT Count() FROM FM_Load_Request__c];
			System.assertNotEquals(startingNumOfLRs, finalNumOfLRs);
			System.assertEquals(startingNumOfLRs+1, finalNumOfLRs);
		}
	}

	@isTest static void recreateOnFacilityTest() {
		User dispatcher = HOG_TestDataFactory.createUser('Steve', 'Rogers', 'Captain');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher');

		System.runAs(dispatcher) {

			List<FM_Load_Request__c> loadRequests = [SELECT Id 
													FROM FM_Load_Request__c 
													WHERE Status__c != :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED
														AND Source_Facility__c != null];
			Integer startingNumOfLRs = [SELECT Count() FROM FM_Load_Request__c];
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(loadRequests.get(0));
			FM_LoadRequestRecreateCtrl controller;

			Test.startTest();
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(0).Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			controller.newLoadRequest.Create_Reason__c = FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault().get(0).getValue();
			controller.oldLoadRequest.Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
			controller.tank = controller.extraTankOptions.get(0).getValue();
			controller.updateLoadRequestTankInfo();
			controller.newLoadRequest.Act_Tank_Level__c = 100;
			controller.newLoadRequest.Tank_Low_Level__c = 40;
			controller.newLoadRequest.Tank_Size__c = 160;
			controller.recreate();
			Test.stopTest();

			Integer finalNumOfLRs = [SELECT Count() FROM FM_Load_Request__c];
			System.assertNotEquals(startingNumOfLRs, finalNumOfLRs);
			System.assertEquals(startingNumOfLRs+1, finalNumOfLRs);
		}
	}

	@isTest static void valuesAndHandlersTest() {
		User dispatcher = HOG_TestDataFactory.createUser('Steve', 'Rogers', 'Captain');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher');

		System.runAs(dispatcher) {

			List<FM_Load_Request__c> loadRequests = [SELECT Id 
													FROM FM_Load_Request__c 
													WHERE Status__c != :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED];
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(loadRequests.get(2));
			FM_LoadRequestRecreateCtrl controller;

			Test.startTest();

			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(0).Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			String other = 'Other';
			//CANCEL REASON HANDLER
			controller.oldLoadRequest.Cancel_Comments__c = other;
			controller.oldLoadRequest.Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
			controller.handleCancelReasonChange();
			System.assert(String.isBlank(controller.oldLoadRequest.Cancel_Comments__c));

			controller.oldLoadRequest.Cancel_Reason__c = other;
			controller.handleCancelReasonChange();
			System.assert(String.isNotBlank(controller.oldLoadRequest.Cancel_Comments__c));
			System.assertEquals(other, controller.oldLoadRequest.Cancel_Comments__c);
			//CREATE REASON HANDLER
			controller.newLoadRequest.Create_Comments__c = other;
			controller.newLoadRequest.Create_Reason__c = FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault().get(0).getValue();
			controller.handleCreateReasonChange();
			System.assert(String.isBlank(controller.newLoadRequest.Create_Comments__c));

			controller.newLoadRequest.Create_Reason__c = other;
			controller.handleCreateReasonChange();
			System.assert(String.isNotBlank(controller.newLoadRequest.Create_Comments__c));
			System.assertEquals(other, controller.newLoadRequest.Create_Comments__c);
			
			Test.stopTest();
		}
	}

	@isTest static void selectListsTest() {
		User dispatcher = HOG_TestDataFactory.createUser('Steve', 'Rogers', 'Captain');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher');

		System.runAs(dispatcher) {

			List<FM_Load_Request__c> loadRequests = [SELECT Id 
													FROM FM_Load_Request__c 
													WHERE Status__c != :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED];
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(loadRequests.get(2));
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(0).Id);
			FM_LoadRequestRecreateCtrl controller = new FM_LoadRequestRecreateCtrl(stdCtrl);

			Test.startTest();

			for(SelectOption tankOption : controller.tankOptions) 
				for(SelectOption extraTankOption : controller.extraTankOptions) 
					System.assertNotEquals(tankOption.getLabel(), extraTankOption.getLabel());

			String defaultCreateReason = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Create_Reason__c.getDescribe().getSObjectField());
			for(SelectOption createReason : controller.createReasonOptions)
				System.assertNotEquals(defaultCreateReason, createReason.getValue());

			System.assert(controller.shiftOptions.size() > 0);
			for(SelectOption shiftOptions : controller.shiftOptions) {
				controller.newLoadRequestShift = shiftOptions.getValue();
				System.assertEquals(shiftOptions.getValue(), controller.newLoadRequestShift);
			}

			controller.newLoadRequest.Shift__c = '';
			controller.newLoadRequest.Shift_Day__c = '';
			controller.newLoadRequestShift = FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_NIGHT;
			System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_NIGHT + ' - ' + FM_LoadRequest_Utilities.LOADREQUEST_DAYSHIFT_TODAY, controller.newLoadRequestShift);
			controller.newLoadRequest.Shift__c = '';
			controller.newLoadRequest.Shift_Day__c = '';
			controller.newLoadRequestShift = FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_DAY;
			System.assertEquals(FM_LoadRequest_Utilities.LOADREQUEST_SHIFT_DAY + ' - ' + FM_LoadRequest_Utilities.LOADREQUEST_DAYSHIFT_TOMORROW, controller.newLoadRequestShift);
			controller.newLoadRequest.Shift__c = '';
			controller.newLoadRequest.Shift_Day__c = '';
			controller.newLoadRequestShift = '';
			System.assertEquals(' - ', controller.newLoadRequestShift);

			Test.stopTest();
		}
	}
	
	@isTest static void validationsTest() {
		User dispatcher = HOG_TestDataFactory.createUser('Steve', 'Rogers', 'Captain');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher');

		System.runAs(dispatcher) {

			FM_Load_Request__c dispatchedLoadRequest = [SELECT Id 
														FROM FM_Load_Request__c 
														WHERE Status__c = :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED].get(0);
			FM_Load_Request__c submittedLoadRequest = [SELECT Id 
														FROM FM_Load_Request__c 
														WHERE Status__c != :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED].get(0);
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(dispatchedLoadRequest);
			FM_LoadRequestRecreateCtrl controller;
			Test.startTest();
			//NO LOAD REQUEST ERROR 
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			Integer expectedNumOfErrors = 1;
			System.assert(ApexPages.hasMessages());
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());
			//WRONG STATUS ERROR
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, dispatchedLoadRequest.Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			System.assert(ApexPages.hasMessages());
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());
			System.assert(!controller.oldLoadRequestValid);
			System.assertNotEquals(null, controller.oldLoadRequest);
			System.assertEquals(null, controller.newLoadRequest);

			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, submittedLoadRequest.Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			controller.updateLoadRequestTankInfo();
			controller.newLoadRequest.Act_Tank_Level__c = 100;
			controller.newLoadRequest.Tank_Low_Level__c = 40;
			controller.newLoadRequest.Tank_Size__c = 160;
			String loadType = controller.newLoadRequest.Load_Type__c;
			String product = controller.newLoadRequest.Product__c;
			//String shift = controller.newLoadRequest.newLoadRequestShift;
			String comments = controller.newLoadRequest.Standing_Comments__c;
			String loadWeight = controller.newLoadRequest.Load_Weight__c;
			String axle = controller.newLoadRequest.Axle__c;
			Id carrier = controller.newLoadRequest.Carrier__c;
			Integer flowRate = Integer.valueOf(controller.newLoadRequest.Act_Flow_Rate__c);
			Integer tankLevel = Integer.valueOf(controller.newLoadRequest.Act_Tank_Level__c);
			Integer tankLowLevel = Integer.valueOf(controller.newLoadRequest.Tank_Low_Level__c);
			Integer tankSize = Integer.valueOf(controller.newLoadRequest.Tank_Size__c);
			//START TESTING VALIDATIONS
			controller.oldLoadRequest.Cancel_Reason__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.oldLoadRequest.Cancel_Reason__c = 'Other';
			controller.oldLoadRequest.Cancel_Comments__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.oldLoadRequest.Cancel_Comments__c = 'Other';
			controller.newLoadRequest.Load_Type__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Load_Type__c = FM_Load_Request__c.Load_Type__c.getDescribe().getSObjectField().getDescribe().getPicklistValues().get(1).getValue();
			System.assert(!controller.getIsStandardLoad());
			controller.newLoadRequest.Standing_Comments__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Standing_Comments__c = 'Nonstandard load require comment';
			controller.newLoadRequest.Product__c = '';
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Product__c = product;
			controller.newLoadRequest.Load_Weight__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Load_Weight__c = loadWeight;
			controller.tank = '';
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.tank = controller.extraTankOptions.get(0).getValue();
			controller.newLoadRequest.Act_Flow_Rate__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Act_Flow_Rate__c = flowRate;
			controller.newLoadRequest.Act_Tank_Level__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Act_Tank_Level__c = 100;
			controller.newLoadRequest.Tank_Low_Level__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Tank_Low_Level__c = 40;
			controller.newLoadRequest.Tank_Size__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Tank_Size__c = 160;
			controller.newLoadRequest.Axle__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());
			
			controller.newLoadRequest.Axle__c = axle;
			controller.newLoadRequest.Carrier__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Carrier__c = carrier;
			controller.newLoadRequest.Create_Reason__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			controller.newLoadRequest.Create_Reason__c = 'Other';
			controller.newLoadRequest.Create_Comments__c = null;
			controller.recreate();
			expectedNumOfErrors++;
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			//SUCCESSFUL RECREATE
			controller.newLoadRequest.Create_Comments__c = 'Other';
			controller.recreate();
			System.assertEquals(expectedNumOfErrors, ApexPages.getMessages().size());

			Test.stopTest();
		}
	}

	@isTest static void exceptionCatchTest() {
		User dispatcher = HOG_TestDataFactory.createUser('Steve', 'Rogers', 'Captain');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher');

		System.runAs(dispatcher) {

			List<FM_Load_Request__c> loadRequests = [SELECT Id 
													FROM FM_Load_Request__c 
													WHERE Status__c != :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED
														AND Source_Location__c != null];
			Integer startingNumOfLRs = [SELECT Count() FROM FM_Load_Request__c];
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(loadRequests.get(0));
			FM_LoadRequestRecreateCtrl controller;

			Test.startTest();
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(0).Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			controller.newLoadRequest.Create_Reason__c = FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault().get(0).getValue();
			controller.oldLoadRequest.Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
			controller.tank = controller.tankOptions.get(0).getValue();
			controller.updateLoadRequestTankInfo();
			insert controller.newLoadRequest;
			System.assert(!ApexPages.hasMessages());
			controller.recreate();
			Test.stopTest();
			System.assert(ApexPages.hasMessages());
		}
	}

	@isTest static void redirectsTest() {
		User dispatcher = HOG_TestDataFactory.createUser('Steve', 'Rogers', 'Captain');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher');

		System.runAs(dispatcher) {

			List<FM_Load_Request__c> loadRequests = [SELECT Id 
													FROM FM_Load_Request__c 
													WHERE Status__c != :FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED];
			Integer expectedNumOfLRs = [SELECT Count() FROM FM_Load_Request__c];
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(loadRequests.get(2));
			FM_LoadRequestRecreateCtrl controller;

			Test.startTest();
			//CANCEL REDIRECT THRU ID
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(2).Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			System.assertEquals(new PageReference('/' + loadRequests.get(2).Id).getUrl(), controller.cancel().getUrl());
			
			//SAVE REDIRECT THRU ID
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(0).Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			controller.newLoadRequest.Create_Reason__c = FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault().get(0).getValue();
			controller.oldLoadRequest.Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
			controller.newLoadRequest.Act_Tank_Level__c = 90;
			controller.newLoadRequest.Tank_Low_Level__c = 40;
			controller.newLoadRequest.Tank_Size__c = 160;
			String resultUrl = controller.recreate().getUrl();
			FM_Load_Request__c newLoadRequest = [SELECT Id FROM FM_Load_Request__c ORDER BY CreatedDate DESC].get(0);
			System.assert(new PageReference('/' + newLoadRequest.Id).getUrl().contains(resultUrl));
			expectedNumOfLRs++;
			System.assertEquals(expectedNumOfLRs, [SELECT Count() FROM FM_Load_Request__c]);
			
			//CANCEL REDIRECT THRU SIMPLE PARAM
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(2).Id);
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_CANCEL, FM_LoadRequestRecreateCtrl.URL_PARAM_CANCEL);
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_SAVE, FM_LoadRequestRecreateCtrl.URL_PARAM_SAVE);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			System.assertEquals(new PageReference('/' + FM_LoadRequestRecreateCtrl.URL_PARAM_CANCEL).getUrl(), controller.cancel().getUrl());
			
			//SAVE REDIRECT THRU SIMPLE PARAM
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(1).Id);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			controller.newLoadRequest.Create_Reason__c = FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault().get(0).getValue();
			controller.oldLoadRequest.Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
			controller.newLoadRequest.Act_Tank_Level__c = 90;
			controller.newLoadRequest.Tank_Low_Level__c = 40;
			controller.newLoadRequest.Tank_Size__c = 160;
			System.assertEquals(new PageReference('/' + FM_LoadRequestRecreateCtrl.URL_PARAM_SAVE).getUrl(), controller.recreate().getUrl());
			expectedNumOfLRs++;
			System.assertEquals(expectedNumOfLRs, [SELECT Count() FROM FM_Load_Request__c]);

			//CANCEL REDIRECT THRU ADVANCED PARAM
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ID, loadRequests.get(2).Id);
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_ROUTE_ID, FM_LoadRequestRecreateCtrl.URL_PARAM_ROUTE_ID);
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_ID, FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_ID);
			HOG_GeneralUtilities.setUrlParam(FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_TYPE, FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_TYPE);
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			String returnFilterString = '';
	        String sourceType = FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_TYPE;
	        String sourceId = FM_LoadRequestRecreateCtrl.URL_PARAM_SOURCE_ID;
	        String routeId = FM_LoadRequestRecreateCtrl.URL_PARAM_ROUTE_ID;
	        returnFilterString += '?' + sourceType + '=' + sourceType;
	        returnFilterString += '&' + sourceId + '=' + sourceId;
	        returnFilterString += '&' + routeId + '=' + routeId;
			System.assertEquals(new PageReference('/' + FM_LoadRequestRecreateCtrl.URL_PARAM_CANCEL + returnFilterString).getUrl(), controller.cancel().getUrl());
			
			//SAVE REDIRECT THRU ADVANCED PARAM
			controller = new FM_LoadRequestRecreateCtrl(stdCtrl);
			controller.newLoadRequest.Create_Reason__c = FM_LoadRequest_Utilities.getCreateReasonOptionsWithoutDefault().get(0).getValue();
			controller.oldLoadRequest.Cancel_Reason__c = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(FM_Load_Request__c.Cancel_Reason__c.getDescribe().getSObjectField());
			controller.newLoadRequest.Act_Tank_Level__c = 90;
			controller.newLoadRequest.Tank_Low_Level__c = 40;
			controller.newLoadRequest.Tank_Size__c = 160;
			System.assertEquals(new PageReference('/' + FM_LoadRequestRecreateCtrl.URL_PARAM_SAVE + returnFilterString).getUrl(), controller.recreate().getUrl());
			expectedNumOfLRs++;
			System.assertEquals(expectedNumOfLRs, [SELECT Count() FROM FM_Load_Request__c]);

			Test.stopTest();
		}
	}

	@testSetup static void prepareData() {
		Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);

        Field__c field = HOG_TestDataFactory.createAMU(true);

        Carrier__c carrier = FM_TestDataFactory.createCarrier(acc.Id, true);
        Carrier_Unit__c unit = FM_TestDataFactory.createCarrierUnit(carrier.Id, true);

        Route__c route = HOG_TestDataFactory.createRoute(true);

        Location__c location = HOG_TestDataFactory.createLocation(route.Id, field.Id, true);

        Equipment_Tank__c equipmentTank = HOG_TestDataFactory.createEquipmentTank(location, true);

        Facility__c facility = HOG_TestDataFactory.createFacility(route.Id, field.Id, true);

        List<FM_Load_Request__c> loadRequestList = new List<FM_Load_Request__c>();
        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                                                                        facility.Id,
                                                                        'O', null,
                                                                        'Night',
                                                                        location.Id,
                                                                        FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                                                                        false)); 
        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                                                                        facility.Id,
                                                                        'O', null,
                                                                        'Night',
                                                                        location.Id,
                                                                        FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED,
                                                                        false)); 
        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnFacility(carrier.Id, 
                                                                        location.Id, null, 
                                                                        'W', null, 
                                                                        'Day', 
                                                                        facility.Id, 
                                                                        FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, 
                                                                        false));
        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnFacility(carrier.Id, 
                                                                        location.Id, null, 
                                                                        'W', null, 
                                                                        'Day', 
                                                                        facility.Id, 
                                                                        FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED, 
                                                                        false));

        insert loadRequestList;


	}
	
}