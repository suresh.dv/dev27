//Generated by wsdl2apex

public class HOG_SAPCancelConfirmationsService {
    public class DT_SFDC_Confirmation_Resp {
        public HOG_SAPCancelConfirmationsService.Confirmation_Resp_element[] Confirmation_Resp;
        private String[] Confirmation_Resp_type_info = new String[]{'Confirmation_Resp','http://salesforce.com/PM',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://salesforce.com/PM','false','false'};
        private String[] field_order_type_info = new String[]{'Confirmation_Resp'};
    }
    public class Confirmation_Resp_element {
        public String Conf_No;
        public String OBJNR;
        public String Status;
        private String[] Conf_No_type_info = new String[]{'Conf_No','http://salesforce.com/PM',null,'0','1','false'};
        private String[] OBJNR_type_info = new String[]{'OBJNR','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Status_type_info = new String[]{'Status','http://salesforce.com/PM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://salesforce.com/PM','false','false'};
        private String[] field_order_type_info = new String[]{'Conf_No','OBJNR','Status'};
    }
    public class DT_SFDC_CancelConfirmation {
        public HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation[] Confirmation;
        private String[] Confirmation_type_info = new String[]{'Confirmation','http://salesforce.com/PM',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://salesforce.com/PM','false','false'};
        private String[] field_order_type_info = new String[]{'Confirmation'};
    }
    public class DT_SFDC_Cancel_Confirmation {
        public String Order_Number;
        public String Operation_Number;
        public String Operation_Text;
        public String System_Status;
        public String Work_Center;
        public String Plant;
        public String Personal_Number;
        public String Actual_Work;
        public String Unit_of_Work;
        public String Activity_Type;
        public String Final_Confirmation;
        public String No_Remaining_Work;
        public String Clear_Open_Reservation;
        public String Work_Start_Date;
        public String Work_Start_Time;
        public String Work_Finish_Date;
        public String Work_Finish_Time;
        public String Remaining_Work;
        public String Remaining_Work_Unit;
        public String Actual_Duration;
        public String Actual_Duration_Unit;
        public String Forecast_End_Date;
        public String Forecast_End_Time;
        public String Reason;
        public String Confirmation_Text;
        public String Confirmation_Text_Long_Text;
        public String Cancellation_Text;
        private String[] Order_Number_type_info = new String[]{'Order_Number','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Operation_Number_type_info = new String[]{'Operation_Number','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Operation_Text_type_info = new String[]{'Operation_Text','http://salesforce.com/PM',null,'0','1','false'};
        private String[] System_Status_type_info = new String[]{'System_Status','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Work_Center_type_info = new String[]{'Work_Center','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Plant_type_info = new String[]{'Plant','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Personal_Number_type_info = new String[]{'Personal_Number','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Actual_Work_type_info = new String[]{'Actual_Work','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Unit_of_Work_type_info = new String[]{'Unit_of_Work','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Activity_Type_type_info = new String[]{'Activity_Type','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Final_Confirmation_type_info = new String[]{'Final_Confirmation','http://salesforce.com/PM',null,'0','1','false'};
        private String[] No_Remaining_Work_type_info = new String[]{'No_Remaining_Work','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Clear_Open_Reservation_type_info = new String[]{'Clear_Open_Reservation','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Work_Start_Date_type_info = new String[]{'Work_Start_Date','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Work_Start_Time_type_info = new String[]{'Work_Start_Time','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Work_Finish_Date_type_info = new String[]{'Work_Finish_Date','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Work_Finish_Time_type_info = new String[]{'Work_Finish_Time','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Remaining_Work_type_info = new String[]{'Remaining_Work','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Remaining_Work_Unit_type_info = new String[]{'Remaining_Work_Unit','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Actual_Duration_type_info = new String[]{'Actual_Duration','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Actual_Duration_Unit_type_info = new String[]{'Actual_Duration_Unit','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Forecast_End_Date_type_info = new String[]{'Forecast_End_Date','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Forecast_End_Time_type_info = new String[]{'Forecast_End_Time','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Reason_type_info = new String[]{'Reason','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Confirmation_Text_type_info = new String[]{'Confirmation_Text','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Confirmation_Text_Long_Text_type_info = new String[]{'Confirmation_Text_Long_Text','http://salesforce.com/PM',null,'0','1','false'};
        private String[] Cancellation_Text_type_info = new String[]{'Cancellation_Text','http://salesforce.com/PM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://salesforce.com/PM','false','false'};
        private String[] field_order_type_info = new String[]{'Order_Number','Operation_Number','Operation_Text','System_Status','Work_Center','Plant','Personal_Number','Actual_Work','Unit_of_Work','Activity_Type','Final_Confirmation','No_Remaining_Work','Clear_Open_Reservation','Work_Start_Date','Work_Start_Time','Work_Finish_Date','Work_Finish_Time','Remaining_Work','Remaining_Work_Unit','Actual_Duration','Actual_Duration_Unit','Forecast_End_Date','Forecast_End_Time','Reason','Confirmation_Text','Confirmation_Text_Long_Text','Cancellation_Text'};
    }
    public class HTTPS_Port {
        public String endpoint_x = 'https://salesforce-hog-workorder-dev1.huskyenergy.com:8007/XISOAPAdapter/MessageServlet?senderParty=&senderService=SFDC&receiverParty=&receiverService=&interface=SI_SFDC_CancelConfirmation_Sync_OB&interfaceNamespace=http%3A%2F%2Fsalesforce.com%2FPM';
        private HOG_Settings__c settingsHOG = HOG_Settings__c.getInstance();
        //public String endpoint_x = settingsHOG.SAP_Create_Confirmations_Endpoint__c;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x = settingsHOG.Client_Certificate_Name__c;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x = Integer.valueof(settingsHOG.SAP_Create_Notification_Timeout__c);
        private String[] ns_map_type_info = new String[]{'http://salesforce.com/PM', 'HOG_SAPCancelConfirmationsService'};
        public HOG_SAPCancelConfirmationsService.Confirmation_Resp_element[] SI_SFDC_CancelConfirmation_Sync_OB(HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation[] Confirmation) {
            HOG_SAPCancelConfirmationsService.DT_SFDC_CancelConfirmation request_x = new HOG_SAPCancelConfirmationsService.DT_SFDC_CancelConfirmation();
            request_x.Confirmation = Confirmation;
            HOG_SAPCancelConfirmationsService.DT_SFDC_Confirmation_Resp response_x;
            Map<String, HOG_SAPCancelConfirmationsService.DT_SFDC_Confirmation_Resp> response_map_x = new Map<String, HOG_SAPCancelConfirmationsService.DT_SFDC_Confirmation_Resp>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'http://salesforce.com/PM',
              'MT_SFDC_CancelConfirmation',
              'http://salesforce.com/PM',
              'MT_SFDC_CancelConfirmation_Resp',
              'HOG_SAPCancelConfirmationsService.DT_SFDC_Confirmation_Resp'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.Confirmation_Resp;
        }
    }
}