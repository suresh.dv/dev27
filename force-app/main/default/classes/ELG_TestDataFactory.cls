/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Related Class:  ELG_FieldDefaults
				HOG_SObjectFactory
Description:    Test Data Factory for E-Log application
History:        mbrimus 05.31.2018 - Created.
****************************************************************************************************
WARNING.. WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..
****************************************************************************************************
DO NOT CHANGE THIS CLASS, you can extend but dont change any values these, if you have to then rerun these tests

ELG_UtilitiesTest
**************************************************************************************************/
@IsTest
public with sharing class ELG_TestDataFactory {

	/****************************************************************************************************************
	*                                               USERS                                                           *
	*****************************************************************************************************************/
	private static User createUser() {
		User user = new User();
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User'];

		Double random = Math.random();
		user.Email = 'E-LogNeo' + random + '@vtt.com';
		user.Alias = 'theOne' ;
		user.EmailEncodingKey = 'UTF-8';
		user.FirstName = 'Thomas';
		user.LastName = 'Anderson';
		user.LanguageLocaleKey = 'en_US';
		user.LocaleSidKey = 'en_US';
		user.ProfileId = p.Id;
		user.TimeZoneSidKey = 'America/Denver';
		user.Username = 'E-LogNeo' + random + '@ELG.com.matrix';
		insert user;

		return user;
	}

	private static User createSecondUser() {
		User user = new User();
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User'];

		Double random = Math.random();
		user.Email = 'E-Wick' + random + '@vtt.com';
		user.Alias = 'BabaJaga' ;
		user.EmailEncodingKey = 'UTF-8';
		user.FirstName = 'John';
		user.LastName = 'Wick';
		user.LanguageLocaleKey = 'en_US';
		user.LocaleSidKey = 'en_US';
		user.ProfileId = p.Id;
		user.TimeZoneSidKey = 'America/Denver';
		user.Username = 'E-Wick' + random + '@ELG.com.assassin';
		insert user;

		return user;
	}

	private static User assignPermissionSet(User user, String userPermissionSetName) {
		PermissionSet permissionSet = [SELECT Id FROM PermissionSet WHERE Name = :userPermissionSetName];
		PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
		permissionSetAssignment.PermissionSetId = permissionSet.Id;
		permissionSetAssignment.AssigneeId = user.Id;

		insert permissionSetAssignment;

		return user;
	}

	public static User createReviewerUser() {
		User user = createUser();
		user = assignPermissionSet(user, 'ELG_Review_Acknowledge');
		return user;
	}

	public static User createLogEntryUser() {
		User user = createUser();
		user = assignPermissionSet(user, 'ELG_Add_Log_Entries');
		return user;
	}

	public static List<User> createReviewerAndLogEntryUsers() {
		List<User> listOfUsers = new List<User>{
				createUser(), createSecondUser()
		};
		assignPermissionSet(listOfUsers[0], 'ELG_Add_Log_Entries');
		assignPermissionSet(listOfUsers[1], 'ELG_Review_Acknowledge');
		return listOfUsers;
	}

	public static User createShiftEngineerUser() {
		User user = createUser();
		assignPermissionSet(user, 'ELG_Review_Acknowledge');
		return user;
	}

	/****************************************************************************************************************
	*                                       Question Categories                                                     *
	*****************************************************************************************************************/
	private static ELG_Handover_Category__c createQuestionCategory() {
		return (ELG_Handover_Category__c) HOG_SObjectFactory.createSObject(new ELG_Handover_Category__c(),
				ELG_FieldDefaults.CLASS_NAME, false);
	}

	private static List<ELG_Handover_Category__c> createQuestionCategories(Integer numOfRecords) {
		return (List<ELG_Handover_Category__c>) HOG_SObjectFactory.createSObjectList(new ELG_Handover_Category__c(),
				numOfRecords,
				ELG_FieldDefaults.CLASS_NAME, false);
	}

	/**
	 * Creates Question Category with recordType and Name
	 *
	 * @param recordType        recordType DeveloperName - values from ELG_Log_Entry__c recordTypes
	 * @param categoryName      name of category
	 * @param doInsert          indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static ELG_Handover_Category__c createQuestionCategory(String recordType, String categoryName, Boolean doInsert) {
		ELG_Handover_Category__c cat = createQuestionCategory();
		cat.Name = categoryName;
		cat.RecordType_DeveloperName__c = recordType;
		if (doInsert) insert cat;
		return cat;
	}

	/**
	 * Creates number of questionCategories
	 *
	 * @param numOfRecords  defined number of question Categories to insert
	 * @param doInsert      indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static List<ELG_Handover_Category__c> createQuestionCategories(Integer numOfRecords, Boolean doInsert) {
		List<ELG_Handover_Category__c> categories = createQuestionCategories(numOfRecords);
		Integer orderOfRecords = 0;
		for (ELG_Handover_Category__c cat : categories) {
			cat.Order__c = orderOfRecords;
			cat.External_ID__c = String.valueOf(orderOfRecords);
			orderOfRecords++;
		}

		if (doInsert) insert categories;
		return categories;
	}


	/****************************************************************************************************************
	*                                               Questions                                                       *
	*****************************************************************************************************************/
	private static ELG_Handover_Category_Question__c createQuestion() {
		return (ELG_Handover_Category_Question__c) HOG_SObjectFactory.createSObject(new ELG_Handover_Category_Question__c(),
				ELG_FieldDefaults.CLASS_NAME);
	}

	private static List<ELG_Handover_Category_Question__c> createQuestions(Integer numOfRecords) {
		return (List<ELG_Handover_Category_Question__c>) HOG_SObjectFactory.createSObjectList(new ELG_Handover_Category_Question__c(),
				numOfRecords,
				ELG_FieldDefaults.CLASS_NAME);
	}

	/**
	 * Creates Question
	 *
	 * @param questionName  Question for handover
	 * @param doInsert      indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static ELG_Handover_Category_Question__c createQuestion(String questionName, Boolean doInsert) {
		ELG_Handover_Category_Question__c question = createQuestion();
		question.Question__c = questionName;
		if (doInsert) insert question;
		return question;
	}

	/**
	 * Creates number of Questions
	 *
	 * @param questionName  Question for handover
	 * @param numOfRecords  defined number of question Categories to insert
	 * @param doInsert      indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static List<ELG_Handover_Category_Question__c> createQuestions(String questionName,
			Integer numOfRecords, Boolean doInsert) {
		ELG_Handover_Category__c category = ELG_TestDataFactory.createQuestionCategory('Generic', 'Test Cat', true);
		List<ELG_Handover_Category_Question__c> questions = createQuestions(numOfRecords);

		Integer orderOfRecords = 0;

		for (ELG_Handover_Category_Question__c que : questions) {
			que.Question__c = questionName + String.valueOf(orderOfRecords);
			que.Order__c = orderOfRecords;
			que.Handover_Category__c = category.Id;
			orderOfRecords++;
		}

		if (doInsert) insert questions;
		return questions;
	}

	/**
	 * Creates number of Questions for Category
	 *
	 * @param questionName  Question for handover
	 * @param numOfRecords  defined number of question Categories to insert
	 * @param category      categoryToTieQuestionTo
	 * @param doInsert      indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static List<ELG_Handover_Category_Question__c> createQuestionsForCategory(String questionName,
			Integer numOfRecords, ELG_Handover_Category__c category, Boolean doInsert) {
		List<ELG_Handover_Category_Question__c> questions = createQuestions(numOfRecords);
		Integer orderOfRecords = 0;

		for (ELG_Handover_Category_Question__c que : questions) {
			que.Question__c = questionName + String.valueOf(orderOfRecords);
			que.Order__c = orderOfRecords;
			que.Handover_Category__c = category.Id;
			orderOfRecords++;
		}

		if (doInsert) insert questions;
		return questions;
	}

	/****************************************************************************************************************
	*                                               Post                                                            *
	*****************************************************************************************************************/
	private static ELG_Post__c createPost() {
		return (ELG_Post__c) HOG_SObjectFactory.createSObject(new ELG_Post__c(),
				ELG_FieldDefaults.CLASS_NAME, false);
	}

	private static List<ELG_Post__c> createPosts(Integer numOfRecords) {
		return (List<ELG_Post__c>) HOG_SObjectFactory.createSObjectList(new ELG_Post__c(),
				numOfRecords,
				ELG_FieldDefaults.CLASS_NAME);
	}

	/**
	 * Creates Question
	 *
	 * @param postName      PostName
	 * @param doInsert      indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static ELG_Post__c createPost(String postName, Boolean doInsert) {
		Field__c amu = HOG_TestDataFactory.createAMU(true);

		ELG_Post__c post = createPost();
		post.Operating_Field_AMU__c = amu.Id;
		post.Post_Name__c = postName;

		if (doInsert) insert post;
		return post;
	}

	/**
	 * Create multiple posts
	 *
	 * @param postName      PostName
	 * @param numOfRecords  defined number of question Categories to insert
	 * @param doInsert      indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static List<ELG_Post__c> createPosts(String postName,
			Integer numOfRecords, Boolean doInsert) {
		Field__c amu = HOG_TestDataFactory.createAMU(true);

		List<ELG_Post__c> posts = createPosts(numOfRecords);
		Integer orderOfRecords = 0;
		for (ELG_Post__c post : posts) {
			post.Operating_Field_AMU__c = amu.Id;
			post.Post_Name__c = postName + String.valueOf(orderOfRecords);
			orderOfRecords++;
		}

		if (doInsert) insert posts;
		return posts;
	}

	/**
	 * Create multiple posts for specific AMU
	 *
	 * @param postName      PostName
	 * @param numOfRecords  defined number of question Categories to insert
	 * @param amu           specific AMU
	 * @param doInsert      indicates if returned record is stored in DB
	 *
	 * @return
	 */
	public static List<ELG_Post__c> createPostsForAMU(String postName,
			Integer numOfRecords, Field__c amu, Boolean doInsert) {
		List<ELG_Post__c> posts = createPosts(numOfRecords);
		Integer orderOfRecords = 0;
		for (ELG_Post__c post : posts) {
			post.Operating_Field_AMU__c = amu.Id;
			post.Post_Name__c = postName + String.valueOf(orderOfRecords);
			orderOfRecords++;
		}

		if (doInsert) insert posts;
		return posts;
	}

	/****************************************************************************************************************
	*                                               UserSetting                                                     *
	*****************************************************************************************************************/
	private static ELG_User_Setting__c creteUserSetting() {
		return (ELG_User_Setting__c) HOG_SObjectFactory.createSObject(new ELG_User_Setting__c(),
				ELG_FieldDefaults.CLASS_NAME, false);
	}

	/**
	 * Create User settings
	 *
	 * @param doInsert  indicates if returned record is stored in DB
	 * @return
	 */
	public static ELG_User_Setting__c creteUserSetting(Boolean doInsert) {
		Field__c amu = HOG_TestDataFactory.createAMU(true);

		ELG_User_Setting__c settings = creteUserSetting();
		settings.Operating_Field_AMU__c = amu.Id;

		if (doInsert) insert settings;
		return settings;
	}

	/****************************************************************************************************************
	*                                               ShiftHandover                                                   *
	*****************************************************************************************************************/
	// TODO change to private when we start to add more methods
	public static ELG_Shift_Handover__c createHandover() {
		return (ELG_Shift_Handover__c) HOG_SObjectFactory.createSObject(new ELG_Shift_Handover__c(),
				ELG_FieldDefaults.CLASS_NAME, false);
	}

	/****************************************************************************************************************
	*                                               ShiftAssignement                                                *
	*****************************************************************************************************************/
	private static ELG_Shift_Assignement__c createShiftAssignement() {
		return (ELG_Shift_Assignement__c) HOG_SObjectFactory.createSObject(new ELG_Shift_Assignement__c(),
				ELG_FieldDefaults.CLASS_NAME, false);
	}

	/**
	 * Create Shift Assignement For User
	 *
	 * @param doInsert  indicates if returned record is stored in DB
	 * @return
	 */
	public static ELG_Shift_Assignement__c createShiftAssignement(Boolean doInsert) {
		ELG_Shift_Assignement__c assignement = createShiftAssignement();
		assignement.Primary__c = UserInfo.getUserId();
		if (doInsert) insert assignement;
		return assignement;
	}

	/**
	 * Create Shift Assignement For Post and User
	 *
	 * @param doInsert  indicates if returned record is stored in DB
	 * @return
	 */
	public static ELG_Shift_Assignement__c createShiftAssignement(ELG_Post__c post, Boolean doInsert) {
		ELG_Shift_Assignement__c assignement = createShiftAssignement();
		assignement.Post__c = post.Id;
		assignement.Primary__c = UserInfo.getUserId();
		if (doInsert) insert assignement;
		return assignement;
	}

	/****************************************************************************************************************
	*                                               LOG Entries                                                     *
	*****************************************************************************************************************/
	private static List<ELG_Log_Entry__c> createLogEntries(Integer numOfRecords) {
		return (List<ELG_Log_Entry__c>) HOG_SObjectFactory.createSObjectList(new ELG_Log_Entry__c(),
				numOfRecords,
				ELG_FieldDefaults.CLASS_NAME,
				false);
	}

	private static ELG_Log_Entry__c createLogEntry() {
		return (ELG_Log_Entry__c) HOG_SObjectFactory.createSObject(new ELG_Log_Entry__c(),
				ELG_FieldDefaults.CLASS_NAME, false);
	}

	/**
	 * Creates entry
	 *
	 * @param doInsert
	 *
	 * @return
	 */
	public static ELG_Log_Entry__c createLogEntry(Boolean doInsert) {
		ELG_Log_Entry__c entry = createLogEntry();
		if (doInsert) insert entry;
		return entry;
	}

	/**
	 * Creates multiple entries for amu post
	 *
	 * @param numOfRecords
	 * @param amu
	 * @param post
	 * @param assignement
	 * @param doInsert
	 *
	 * @return
	 */
	public static List<ELG_Log_Entry__c> createLogEntries(
			Integer numOfRecords,
			Field__c amu,
			ELG_Post__c post,
			ELG_Shift_Assignement__c assignement,
			Boolean doInsert) {
		List<ELG_Log_Entry__c> entries = createLogEntries(numOfRecords);
		Id recordType = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();

		for (ELG_Log_Entry__c entry : entries) {
			entry.Post__c = post.Id;
			entry.Operating_Field_AMU__c = amu.Id;
			entry.Shift_Assignement__c = assignement.Id;
			entry.RecordTypeId = recordType;
		}
		if (doInsert) insert entries;
		return entries;
	}

	/****************************************************************************************************************
	*                                               TASKS                                                     *
	*****************************************************************************************************************/

	/**
	 * Create task
	 *
	 * @param postId
	 * @param shortDescription
	 * @param taskDescription
	 * @param doInsert
	 *
	 * @return
	 */
	public static ELG_Task__c createPostTask(
			String postId,
			String shortDescription,
			String taskDescription,
			Boolean doInsert) {
		ELG_Task__c task = new ELG_Task__c(Post__c = postId,
				Short_Description__c = shortDescription,
				Task_Description__c = taskDescription);
		if (doInsert) insert task;
		return task;
	}

	/**
	 * Creates list of tasks with different statuses. Keep short description short because for loop is adding status and
	 * number to there and limit of field is 80 characters. e.g. for Active tasks adding
	 * A-Test will result in A-Test Active 0 || Active 1 || Active 2 ... and so on
	 *
	 * @param amuId
	 * @param postId
	 * @param shortDescription
	 * @param taskDescription
	 * @param numberOfActiveTasks
	 * @param numberOfCancelledTasks
	 * @param numberOfCompletedTasks
	 * @param userId
	 * @param finishedDatetime
	 * @param doInsert
	 *
	 * @return
	 */

	public static List<ELG_Task__c> createTasksWithDifferentStatuses(
			String amuId,
			String postId,
			String shortDescription,
			String taskDescription,
			Integer numberOfActiveTasks,
			Integer numberOfCancelledTasks,
			Integer numberOfCompletedTasks,
			String userId,
			Datetime finishedDatetime,
			Boolean doInsert) {

		List<ELG_Task__c> tasksToInsert = new List<ELG_Task__c>();

		if (numberOfActiveTasks > 0) {
			for (Integer i = 0; i < numberOfActiveTasks; i++) {
				tasksToInsert.add(new ELG_Task__c(
						Operating_Field_AMU__c = amuId,
						Post__c = postId,
						Short_Description__c = shortDescription + ' ' + ELG_Constants.TASK_ACTIVE + ' ' + i,
						Task_Description__c = taskDescription + ' ' + ELG_Constants.TASK_ACTIVE + ' ' + i,
						Status__c = ELG_Constants.TASK_ACTIVE));
			}
		}

		if (numberOfCancelledTasks > 0) {
			for (Integer i = 0; i < numberOfCancelledTasks; i++) {
				tasksToInsert.add(new ELG_Task__c(
						Operating_Field_AMU__c = amuId,
						Post__c = postId,
						Short_Description__c = shortDescription + ' ' + ELG_Constants.TASK_CANCELLED + ' ' + i,
						Task_Description__c = taskDescription + ' ' + ELG_Constants.TASK_CANCELLED + ' '  + i,
						Status__c = ELG_Constants.TASK_CANCELLED,
						Finished_By__c = userId,
						Finished_At__c = finishedDatetime));
			}
		}


		if (numberOfCompletedTasks > 0) {
			for (Integer i = 0; i < numberOfCancelledTasks; i++) {
				tasksToInsert.add(new ELG_Task__c(
						Operating_Field_AMU__c = amuId,
						Post__c = postId,
						Short_Description__c = shortDescription + ' ' + ELG_Constants.TASK_COMPLETED + ' ' + i,
						Task_Description__c = taskDescription + ' ' + ELG_Constants.TASK_COMPLETED + ' ' + i,
						Status__c = ELG_Constants.TASK_COMPLETED,
						Finished_By__c = userId,
						Finished_At__c = finishedDatetime));
			}
		}

		if (doInsert) insert tasksToInsert;
		return tasksToInsert;
	}


}