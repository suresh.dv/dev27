/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
Test Class:     XXX
History:        jschn 22/10/2019 - Created.
*************************************************************************************************/
public interface VTT_WOA_ListViewDAO {

    List<HOG_Maintenance_Servicing_Form__c> getRecordsByFilter(VTT_WOA_SearchCriteria filter, String sorting, Integer offset, Integer queryLimit);

}