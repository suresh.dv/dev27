@isTest
public with sharing class RaMPortalUtilityTest {

    @testSetUp 
    static void setup() {
        String accountRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();

        Profile headOfficeProfile = [SELECT Id FROM Profile WHERE Name = 'Category Manager User'];
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'RaM Lightning Vendor Community'];
        UserRole role = [SELECT Id, Name FROM UserRole WHERE Name = 'Retail Fuels & Convenience - Staff'];

        User headOffice = TestUtilities.createTestUser(headOfficeProfile);
        headOffice.Email = 'head_office_unit_test@huskyenergy.huskyram.com';
        headOffice.UserName = 'head_office_unit_test@huskyenergy.huskyram.com';
        headOffice.UserRoleId = role.Id;

        insert headOffice;       

        System.runAs(headOffice) {
            Account vendor1 = new Account(Name = 'RaM Vendor Account 1');
            vendor1.RecordTypeId = accountRecordType;
    
            Account vendor2 = new Account(Name = 'RaM Vendor Account 2');
            vendor2.RecordTypeId = accountRecordType;
    
            insert vendor1;
            insert vendor2;

            Contact vendor1Contact = new Contact(FirstName = 'V1', LastName = 'Vendor 1 Dispatcher');
            vendor1Contact.AccountId = vendor1.id;
            vendor1Contact.Email = 'vendor1_dispatcher@vendor1.com';
            vendor1Contact.RecordTypeId = contactRecordType;          
    
            Contact vendor2Contact = new Contact(FirstName = 'V2', LastName = 'Vendor 2 Dispatcher');
            vendor2Contact.AccountId = vendor2.id;
            vendor2Contact.Email = 'vendor2_dispatcher@vendor2.com';
            vendor2Contact.RecordTypeId = contactRecordType;                  
    
            insert vendor1Contact;
            insert vendor2Contact;     

            RaM_Location__c locationRecord = new RaM_Location__c(Name = '1234', Service_Area__c = 'Island', SAPFuncLOC__c = 'C1-04-002-00042', 
            Location_Classification__c = 'Urban', R_M_Location__c = true, LocationName__c = 'Unit Test Location', Retailer__c = 'Unit Test Retailer',
            Physical_Address_1__c = 'Unit Test Address 1', Physical_Address_2__c = 'Unit Test Address 2', City__c = 'Calgary', Province__c = 'AB', 
            Postal_Code__c = 'T2W8T6', Phone__c = '4031236655', District_Manager__c = 'Test Manager', Maintenance_Tech_User__c = null);

            insert locationRecord; 
            
            RaM_Equipment__c ramEquipment = new RaM_Equipment__c(Name = 'RP0042SSDVR0001-16 Channel DVR--');
            ramEquipment.FunctionalLocation__c = 'C1-04-002-00042';
            ramEquipment.Equipment_Type__c = 'ELECTRICAL';
            ramEquipment.Equipment_Class__c = 'ELECTRICAL PANEL';
            ramEquipment.Status__c = 'Active';
            
            insert ramEquipment;

            RaM_VendorAssignment__c vendorAssignment = new RaM_VendorAssignment__c();
            vendorAssignment.Account__c = vendor1.id;
            vendorAssignment.Equipment_Type_VA__c = 'ELECTRICAL';
            vendorAssignment.Equipment_Class_VA__c = 'ELECTRICAL PANEL';
            vendorAssignment.Priority__c = 1;
            vendorAssignment.ServiceArea__c = 'Island';
            vendorAssignment.Location_Classification_VA__c = 'Urban';
            
            insert vendorAssignment;

            RaM_Setting__c cusSetting = new RaM_Setting__c();
            cusSetting.name = 'Case Next Counter';
            cusSetting.R_M_Ticket_Number_Next_Sequence__c = 123456;
            insert cusSetting;             

            Case kase1 = new Case(RecordTypeId = caseRecordType,
                                    Status = 'New',
                                    Priority = 'P2-Emergency',
                                    Origin = 'Maintenance Tech',
                                    LocationNumber__c = locationRecord.id,
                                    EquipmentName__c = ramEquipment.id,                                 
                                    Equipment_Type__c='ELECTRICAL',
                                    Equipment_Class__c='ELECTRICAL PANEL',
                                    Location_Contact__c = 'Retailer Contact', 
                                    Location_Classification__c = 'Urban',
                                    Service_Area__c = 'Island' , 
                                    subject = 'Unit Test RaM Case', 
                                    description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat purus odio. Donec lacus mi, condimentum a diam eget, posuere mollis purus. Donec ac velit.' ); 

            insert kase1;   

            kase1.Status = 'Assigned';
            kase1.Vendor_Assignment__c = vendorAssignment.id;
            update kase1; 
        }
    }

    @isTest
    private static void test_getRaMOpenCasesByVendor() {
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'RaM Vendor' AND Name = 'RaM Vendor Account 1'];
        System.assertEquals(1, accounts.size());

        //TODO : Check this
        List<Case> vendor1Cases = RaMPortalUtility.getRaMOpenCasesByVendor(accounts[0].Id);
        System.assertEquals(1, vendor1Cases.size());
    }

    @isTest
    private static void test_getRaMCaseByCaseNumber() {
        Case currentKase = [SELECT Id, CaseNumber FROM Case LIMIT 1];

        Case validCase = RaMPortalUtility.getRaMCaseByCaseNumber(currentKase.CaseNumber);
        Case inValidCase = RaMPortalUtility.getRaMCaseByCaseNumber('741852');

        System.assertEquals(currentKase.CaseNumber, validCase.CaseNumber);
        System.assertEquals(null, inValidCase);
    }

    @isTest
    private static void test_createPortalUser() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Dispatcher');
        }

        List<User> users = [SELECT Id, Name, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        ///TODO : Check this assert
        // List<PermissionSetAssignment> permSets = [SELECT Id, AssigneeId, PermissionSet.Label FROM PermissionSetAssignment WHERE AssigneeId = :users[0].Id];
        // System.assertEquals(1, permSets.size());
        // System.assertEquals('RaM Dispatcher', permSets[0].PermissionSet.Label);
    }

    @isTest
    private static void test_activateAndDeactivateUser() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Dispatcher');
        }

        List<User> users = [SELECT Id, Name, IsActive FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(true, users[0].IsActive);

        RaMPortalUtility.deactivateUser(users[0].Id);
        users = [SELECT Id, Name, IsActive FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(false, users[0].IsActive);        

        RaMPortalUtility.activateUser(users[0].Id, false);
        users = [SELECT Id, Name, IsActive FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(true, users[0].IsActive);
    }

    @isTest
    private static void test_updateContact() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];
        vendor1Contact.LastName = 'Updated Last Name';

        RaMPortalUtility.updateContact(vendor1Contact);
        vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE Id = :vendor1Contact.Id];
        System.assertEquals('Updated Last Name', vendor1Contact.LastName);

        ///TODO: Check this assert
        // try{
        //     RaMPortalUtility.updateContact(null);
        // } catch(Exception exp) {
        //     System.assertEquals(null, exp.getMessage());
        // }
    }

    @isTest
    private static void test_createContact() {
        String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        Account vendor1 = [SELECT Id FROM Account WHERE Name = 'RaM Vendor Account 1' LIMIT 1];

        Contact vendor1Contact = new Contact(FirstName = 'Vendor 1', LastName = 'New Contact');
        vendor1Contact.AccountId = vendor1.id;
        vendor1Contact.Email = 'vendor1_new_contact@vendor1.com';
        vendor1Contact.RecordTypeId = contactRecordType;    

        RaMPortalUtility.createContact(vendor1Contact);
        List<Contact> contacts = [SELECT Id, Name FROM Contact WHERE AccountId = :vendor1.id];
        System.assertEquals(2, contacts.size());
    }

    @isTest
    private static void test_createAndAssignCase() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);
        List<RaM_Case_Assignment__c> assignments = [SELECT Id, Assign_To__c FROM RaM_Case_Assignment__c];
        System.assertEquals(1, assignments.size());     
        System.assertEquals(users[0].Id, assignments[0].Assign_To__c);     
    }

    @isTest
    private static void test_getAssignmentsByCase() {
        Case currentKase = [SELECT Id, CaseNumber FROM Case LIMIT 1];

        List<RaM_Case_Assignment__c> assignments = RaMPortalUtility.getAssignmentsByCase(currentKase.CaseNumber);
        System.assertEquals(0, assignments.size()); 
        
        try{
            List<RaM_Case_Assignment__c> assignments2 = RaMPortalUtility.getAssignmentsByCase(null);
        }catch(Exception exp) {
            System.assertNotEquals(null, exp); 
        }
    }

    @isTest
    private static void test_getAssignments() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);
        
        Map<String, RaM_Case_Assignment__c> assignmentMap = RaMPortalUtility.getAssignments(kase.Id);
        System.assertEquals(1, assignmentMap.size());

        try{
            assignmentMap = RaMPortalUtility.getAssignments(null);
        }catch(Exception exp) {
            System.assertNotEquals(null, exp); 
        }
    }

    @isTest
    private static void test_getAssignmentByContact() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);
        
        List<RaM_Case_Assignment__c> assignments = RaMPortalUtility.getAssignmentByContact(vendor1Contact.Id);
        System.assertEquals(1, assignments.size());

        try{
            assignments = RaMPortalUtility.getAssignmentByContact(null);
        }catch(Exception exp) {
            System.assertNotEquals(null, exp); 
        }
    }

    @isTest
    private static void test_getOpenActivityList() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);
        
        List<RaM_Case_Assignment__c> assignments = RaMPortalUtility.getOpenActivityList(users[0].Id);
        System.assertEquals(1, assignments.size());

        try{
            assignments = RaMPortalUtility.getAssignmentByContact(null);
        }catch(Exception exp) {
            System.assertNotEquals(null, exp); 
        }
    }

    @isTest
    private static void test_getActivityById() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaM_Location__c ramLocation = [SELECT Id FROM RaM_Location__c LIMIT 1];
        RaM_Equipment__c ramEquipment = [SELECT Id FROM RaM_Equipment__c LIMIT 1];
        RaM_VendorAssignment__c vAssignment = [SELECT Id FROM RaM_VendorAssignment__c LIMIT 1];

        Case kase2 = new Case(RecordTypeId = caseRecordType,
                                Status = 'New',
                                Priority = 'P2-Emergency',
                                Origin = 'Maintenance Tech',
                                LocationNumber__c = ramLocation.id,
                                EquipmentName__c = ramEquipment.id,                                 
                                Equipment_Type__c='ELECTRICAL',
                                Equipment_Class__c='ELECTRICAL PANEL',
                                Location_Contact__c = 'Retailer Contact', 
                                Location_Classification__c = 'Urban',
                                Service_Area__c = 'Island' , 
                                subject = 'Unit Test RaM Case 2', 
                                description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat purus odio. Donec lacus mi, condimentum a diam eget, posuere mollis purus. Donec ac velit.' ); 

        insert kase2;   

        kase2.Status = 'Assigned';
        kase2.Vendor_Assignment__c = vAssignment.id;
        update kase2; 

        kase2 = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE Id = :kase2.Id];

        RaMPortalUtility.createAndAssignCase(kase, users);
        RaMPortalUtility.createAndAssignCase(kase2, users);

        List<RaM_Case_Assignment__c> assignments = [SELECT Id, Parent_Case__c FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];

        RaM_Case_Assignment__c assignment = RaMPortalUtility.getActivityById(assignments[0].Id, users[0].Id);
        System.assertEquals(kase.CaseNumber, assignment.Parent_Case__r.CaseNumber);

        try{
            assignment = RaMPortalUtility.getActivityById(null, users[0].Id);
        }catch(Exception exp) {
            System.assertNotEquals(null, exp); 
        }
    }

    @isTest
    private static void test_updatePrimaryTechnician() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);
        
        List<RaM_Case_Assignment__c> assignments = [SELECT Id, Is_Primary_Assignment__c FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id AND Assign_To__c = :users[0].Id];
        System.assertEquals(1, assignments.size());
        System.assertEquals(true, assignments[0].Is_Primary_Assignment__c);

        String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        Contact vendor1Contact2 = new Contact(FirstName = 'Technician', LastName = 'Full Stack');
        vendor1Contact2.AccountId = vendor1Contact.AccountId;
        vendor1Contact2.Email = 'full_stack_technician@vendor1.unittest.com';
        vendor1Contact2.RecordTypeId = contactRecordType;
        
        insert vendor1Contact2;

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact2, 'RaM Technician');
        } 
        
        User usr2 = [SELECT Id FROM User WHERE ContactId = :vendor1Contact2.Id];
        List<User> usrList = new List<User>();
        usrList.add(usr2);
        RaMPortalUtility.createAndAssignCase(kase, users);

        RaMPortalUtility.updatePrimaryTechnician(kase.Id, usr2.Id);

        ///TODO: Check this assert
        // assignments = [SELECT Id, Is_Primary_Assignment__c, Assign_To__c FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];
        // System.assertEquals(2, assignments.size());
        // System.assertEquals(true, assignments[0].Is_Primary_Assignment__c);
    }

    @isTest
    private static void test_unassignTechnician() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);

        List<RaM_Case_Assignment__c> assignments = [SELECT Id, Assign_To__c FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id AND Assign_To__c = :users[0].Id];
        System.assertEquals(users[0].Id, assignments[0].Assign_To__c);

        Test.startTest();
        RaMPortalUtility.unassignTechnician(assignments[0].Id);
        Test.stopTest();

        assignments = [SELECT Id, Assign_To__c FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];
        System.assertEquals(null, assignments[0].Assign_To__c);
    }

    @isTest
    private static void test_reassignNewTechnician() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);
        
        List<RaM_Case_Assignment__c> assignments = [SELECT Id, Is_Primary_Assignment__c, Assign_To__c FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];
        System.assertEquals(1, assignments.size());
        System.assertEquals(true, assignments[0].Is_Primary_Assignment__c);
        System.assertEquals(users[0].Id, assignments[0].Assign_To__c);

        String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        Contact vendor1Contact2 = new Contact(FirstName = 'Technician', LastName = 'Full Stack');
        vendor1Contact2.AccountId = vendor1Contact.AccountId;
        vendor1Contact2.Email = 'full_stack_technician@vendor1.unittest.com';
        vendor1Contact2.RecordTypeId = contactRecordType;
        
        insert vendor1Contact2;

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact2, 'RaM Dispatcher');
        } 
        
        User usr2 = [SELECT Id FROM User WHERE ContactId = :vendor1Contact2.Id];

        RaMPortalUtility.reassignNewTechnician(assignments[0].Id, usr2.Id);

        assignments = [SELECT Id, Is_Primary_Assignment__c, Assign_To__c FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];
        System.assertEquals(1, assignments.size());
        System.assertEquals(true, assignments[0].Is_Primary_Assignment__c);
        System.assertEquals(usr2.Id, assignments[0].Assign_To__c);
    }

    @isTest
    private static void test_getAssignmentsById() {
        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];
        System.assertEquals(1, users.size());

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalUtility.createAndAssignCase(kase, users);

        List<RaM_Case_Assignment__c> assignments = RaMPortalUtility.getAssignmentsById(kase.Id);
        System.assertEquals(1, assignments.size());

        try{
            assignments = RaMPortalUtility.getAssignmentsById(null);
        }catch(Exception exp) {
            System.assertNotEquals(null, exp); 
        }
    }

    @isTest
    private static void test_getCaseLocation() {
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];
        
        Case kaseResult = RaMPortalUtility.getCaseLocation(kase.Id);
        System.assertEquals(kaseResult.Subject, kase.Subject);
    }

    @isTest
    private static void test_getActivityCommentsByCase() {
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];        
        
        RaM_Case_Assignment__c assignment = new RaM_Case_Assignment__c();
        assignment.Parent_Case__c = kase.Id;
        assignment.Assign_To__c = users[0].Id;
        assignment.Is_Primary_Assignment__c = true;
        assignment.Subject__c = kase.Subject;
        assignment.Ticket_Number__c = kase.Ticket_Number_R_M__c;
        insert assignment;

        RaM_Case_Assignment_Activity__c activity = new RaM_Case_Assignment_Activity__c();
        activity.RaM_Case_Assignment__c = assignment.Id;
        activity.Activity_Date__c = System.Now();
        activity.From_Status__c = 'Assigned';
        activity.To_Status__c = 'Pending Parts';
        activity.Reason__c = 'Unit test reason';
        activity.Location_Error__c = 'User declined';
        insert activity;

        List<RaM_Case_Assignment_Activity__c> activities = RaMPortalUtility.getActivityCommentsByCase(kase.Id);
        System.assertEquals(1, activities.size());
    }

    @isTest
    private static void test_getTechnicianAcivities() {
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];        
        
        RaM_Case_Assignment__c assignment = new RaM_Case_Assignment__c();
        assignment.Parent_Case__c = kase.Id;
        assignment.Assign_To__c = users[0].Id;
        assignment.Is_Primary_Assignment__c = true;
        assignment.Subject__c = kase.Subject;
        assignment.Ticket_Number__c = kase.Ticket_Number_R_M__c;
        insert assignment;

        RaM_Case_Assignment_Activity__c activity = new RaM_Case_Assignment_Activity__c();
        activity.RaM_Case_Assignment__c = assignment.Id;
        activity.Activity_Date__c = System.Now();
        activity.From_Status__c = 'Assigned';
        activity.To_Status__c = 'Pending Parts';
        activity.Reason__c = 'Unit test reason';
        activity.Location_Error__c = 'User declined';
        insert activity;
        
        List<RaM_Case_Assignment_Activity__c> activities = RaMPortalUtility.getTechnicianAcivities(kase.Id);
        System.assertEquals(1, activities.size());        
    }

    @isTest
    private static void test_getAttachmentsByCase() {
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];
        
        Attachment attach = new Attachment();    
        attach.Name='Unit Test Attachment';
        attach.body = Blob.valueOf('Unit Test Attachment Body');
        attach.parentId = kase.id;
        insert attach;
        
        List<Attachment> attachments = RaMPortalUtility.getAttachmentsByCase(kase.CaseNumber);
        System.assertEquals(1, attachments.size());           
    }

    @isTest
    private static void test_updateActivity() {
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        Contact vendor1Contact = [SELECT Id, Email, FirstName, LastName, AccountId FROM Contact WHERE LastName = 'Vendor 1 Dispatcher'];

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalUtility.createPortalUser(vendor1Contact, 'RaM Technician');
        }

        List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact.Id];         
        
        RaM_Case_Assignment__c assignment = new RaM_Case_Assignment__c();
        assignment.Parent_Case__c = kase.Id;
        assignment.Assign_To__c = users[0].Id;
        assignment.Is_Primary_Assignment__c = true;
        assignment.Subject__c = kase.Subject;
        assignment.Ticket_Number__c = kase.Ticket_Number_R_M__c;
        insert assignment; 
        
        RaM_Case_Assignment_Activity__c activity = new RaM_Case_Assignment_Activity__c();
        activity.RaM_Case_Assignment__c = assignment.Id;
        activity.Activity_Date__c = System.Now();
        activity.From_Status__c = 'Assigned';
        activity.To_Status__c = 'Pending Parts';
        activity.Reason__c = 'Unit test reason';
        activity.Location_Error__c = 'User declined';
        
        RaM_WorkInformation__c workInfo = new RaM_WorkInformation__c();
        workInfo.Ticket__c = kase.Id;
        workInfo.Worklog__c = 'Unit test work log';

        RaMPortalUtility.updateActivity(assignment, activity, kase, workInfo);
        
        List<RaM_Case_Assignment_Activity__c> activities = [SELECT Id FROM RaM_Case_Assignment_Activity__c];
        System.assertEquals(1, activities.size());

        List<RaM_WorkInformation__c> works = [SELECT Id FROM RaM_WorkInformation__c];
        System.assertEquals(1, works.size());
    }

}