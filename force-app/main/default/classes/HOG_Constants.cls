/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class that holds HOG generic constants used across multiple HOG applications
Test Class:     HOG_AccountDAOImplTest
                VTT_WOA_Assignment_EndpointTest
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public with sharing class HOG_Constants {

    public static final String HUSKY_AD_COMPANY_NAME = '0002';

    public static final String FIELD_API_ID = 'Id';
    public static final String FIELD_API_NAME = 'Name';

}