/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database queries related to Shift Handover part of E-Logs
Test Class:     ELG_ShiftHandoverSelectorTest
History:        mbrim 2019-07-08 - Created.
*************************************************************************************************/
public inherited sharing class ELG_ShiftHandoverSelector implements ELG_ShiftHandoverDAO {
	private static final String CLASS_NAME = String.valueOf(ELG_ShiftHandoverSelector.class);

	public List<ELG_User_Setting__c> getUsersSettings() {
		return [
				SELECT
						Id,
						User__r.Name,
						Operating_Field_AMU__r.Name,
						Operating_Field_AMU__c
				FROM ELG_User_Setting__c
				WHERE User__c = :UserInfo.getUserId()
				LIMIT 1
		];
	}

	public List<ELG_Post__c> getAvailablePostsForAMU(String amuId) {
		return [
				SELECT
						Id,
						Name,
						Post_Name__c,
						Shift_Engineer_Review_Required__c,
						Shift_Lead_Post__c,
						Operating_Field_AMU__c, (
						SELECT Id,
								Shift_Status__c,
								Shift_Cycle__c,
								CreatedDate,
								Primary__r.Name,
								Primary__c,
								Shift_Engineer__c,
								Shift_Engineer__r.Name,
								Post__r.Post_Name__c,
								Post__r.Shift_Engineer_Review_Required__c,
								Post__r.Operating_Field_AMU__c
						FROM Shift_Assignements__r
						ORDER BY CreatedDate DESC
						LIMIT 1
				)
				FROM ELG_Post__c
				WHERE Operating_Field_AMU__c = :amuId
				ORDER BY Post_Name__c ASC
		];
	}

	public List<ELG_Shift_Assignement__c> getCurrentShiftAssignmentForUser(String amuId) {
		return [
				SELECT
						Id,
						Shift_Status__c,
						Shift_Cycle__c,
						Post__r.Post_Name__c,
						Post__r.Shift_Lead_Post__c,
						Post__r.Operating_Field_AMU__c
				FROM ELG_Shift_Assignement__c
				WHERE Primary__c = :UserInfo.getUserId()
				AND Shift_Status__c = :ELG_Constants.SHIFT_NEW
				AND Post__r.Operating_Field_AMU__c = :amuId
				ORDER BY CreatedDate DESC
				LIMIT 1
		];
	}

	public List<ELG_Log_Entry__c> getAllLogsForActiveShiftAndPost(String postId, String facilityId) {
		Id recordType = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();

		return [
				SELECT
						Id,
						Post__c,
						Shift_Assignement__c,
						User__c,
						User__r.Name,
						User_Log_Entry__c,
						CreatedDate,
						Operating_Field_AMU__c,
						Correctable__c,
						RecordTypeId, (
						SELECT
								Id,
								User_Log_Entry__c,
								User__c,
								User__r.Name,
								CreatedDate
						FROM Correction_Entries__r
				)
				FROM ELG_Log_Entry__c
				WHERE Post__c = :postId
				AND RecordTypeId = :recordType
				AND Operating_Field_AMU__c = :facilityId
				AND Shift_Assignement__r.Shift_Status__c = :ELG_Constants.SHIFT_NEW
				AND Original_Log_Entry__c = NULL
				ORDER BY CreatedDate DESC
		];
	}

	public ELG_Shift_Handover__c getHandoverByShift(String shiftId) {
		List<ELG_Shift_Handover__c> handover = [
				SELECT Id, Shift_Assignement__r.Shift_Status__c
				FROM ELG_Shift_Handover__c
				WHERE Shift_Assignement__c = :shiftId
				LIMIT 1
		];

		if (handover.size() > 0) {
			return handover.get(0);
		}

		return null;
	}

	public List<ELG_Shift_Handover__c> getHandoverForTakePost(String amuId) {
		List<ELG_Shift_Handover__c> handover = [
				SELECT
						Id,
						Incoming_Operator__r.Name,
						Incoming_Operator__c,
						Outgoing_Operator__c,
						Status__c,
						Shift_Engineer__c,
						Shift_Assignement__c,
						Shift_Assignement__r.Shift_Engineer__c,
						Shift_Assignement__r.Post__c,
						Shift_Assignement__r.Post__r.Post_Name__c,
						Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
						Shift_Assignement__r.Post__r.Shift_Lead_Post__c,
						Shift_Assignement__r.Shift_Cycle__c
				FROM ELG_Shift_Handover__c
				WHERE Status__c = :ELG_Constants.HANDOVER_IN_PROGRESS
				AND Shift_Assignement__r.Post__r.Operating_Field_AMU__c = :amuId
				LIMIT 10000
		];

		if (handover.size() > 0) {
			return handover;
		}

		return null;
	}

	public List<ELG_Handover_Category__c> getAllQuestionCategories() {
		return [
				SELECT
						Id,
						Name,
						Icon__c,
						RecordType_DeveloperName__c
				FROM ELG_Handover_Category__c
				ORDER BY Order__c
		];
	}

	public List<ELG_Handover_Category_Question__c> getAllActiveQuestions() {
		return [
				SELECT
						Id,
						Question_UUID__c
				FROM ELG_Handover_Category_Question__c
				WHERE Active__c = TRUE
				ORDER BY Order__c
		];
	}

	public List<ELG_Shift_Assignement__c> getSteamShiftWithMissingShiftEngineer(String amuId) {
		return [
				SELECT
						Id,
						Post__r.Post_Name__c
				FROM ELG_Shift_Assignement__c
				WHERE (Shift_Status__c = :ELG_Constants.SHIFT_NEW OR Shift_Status__c = :ELG_Constants.SHIFT_HANDOVER_IN_PROGRESS)
				AND Post__r.Operating_Field_AMU__c = :amuId
				AND Post__r.Shift_Engineer_Review_Required__c = TRUE
				AND Shift_Engineer__c = NULL
				ORDER BY Post__r.Post_Name__c ASC
		];
	}
}