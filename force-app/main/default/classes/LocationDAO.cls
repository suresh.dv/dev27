/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO interface for accessing Location__c records
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
public interface LocationDAO {

    List<Location__c> getLocationById(Id wellId);

}