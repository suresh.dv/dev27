/*------------------------------------------------------------------------------------------------------------
Author      :
Company     : Husky
Description : Submit buttpon
History     :
             04.02.19 mbrim W-001394 - nav for lightning
Test Class - DEFT_EOJ_SubmitButtonExtensionTest
-------------------------------------------------------------------------------------------------------------*/
public class DEFT_EOJ_SubmitButtonExtension {

    private List<Attachment> 	attList;
    private List<ContentDocumentLink> relatedFiles;
    public HOG_EOJ__c			eojToValidate {get; private set;}

    public Boolean				renderButtons {get; private set;}

    public DEFT_EOJ_SubmitButtonExtension(ApexPages.StandardController stdController) {
        String objId = stdController.getId();

        attList = [SELECT Name FROM Attachment WHERE ParentId = :objId];
        relatedFiles = [SELECT LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId =: objId];
        System.debug('DATA ' + relatedFiles);
        eojToValidate = [SELECT Id, Bail_Well__c, Budget_SRP__c, Coil_Kit2__c ,Coil_Kit1__c, Drain_Type2__c,
                        		Comments2__c, Comments1__c, Drain_Placement2__c, Drain_Placement1__c,
                         		Drain_Type1__c, EOJ_Details__c, Final_Cost__c, Final_Tag_Depth__c,
                         		Fluid_Inj__c, Fluid_Recovered__c, Initial_Tag_Depth__c, Intake_Depth2__c,
                         		Intake_Depth1__c, Job_Cost_Comments__c, Length_Off_Tag2__c, Length_Off_Tag1__c,
                         		Next_Location__c, NTT_Type2__c, NTT_Type1__c, PBTD__c, Perf_Interval__c, Pin_Size2__c,
                         		Pin_Size1__c, Polished_Rod_Length2__c, Polished_Rod_Length1__c, Previous_Failure_Detail__c,
                         		Previous_Service_Date__c, Producing_Zone__c, Pull_Out_of_Sand__c,
                         		Pump_Displacement_Size2__c, Pump_Displacement_Size1__c,
                         		Pump_Elastomer2__c, Pump_Elastomer1__c, Pump_Lift2__c, Pump_Lift1__c,
                         		Pump_Type2__c, Pump_Type1__c, Pump_Vendor2__c, Pump_Vendor1__c,
                         		Rig_Hours__c, Rig__c, Rig_Company__c, Rod_Comments2__c, Rod_Comments1__c,
                         		Rod_Condition2__c, Rod_Condition1__c, Rod_Failure__c, Rod_Grade2__c, Rod_Grade1__c,
                         		Rod_Size2__c, Rod_Size1__c, Rod_Type2__c, Rod_Type1__c, Rotor_Condition2__c,
                         		Rotor_Condition1__c, Rotor_Length2__c, Rotor_Length1__c, Rotor_Tag_Type2__c,
                         		Rotor_Tag_Type1__c, Scope_Joint2__c, Scope_Joint1__c, Service_Completed__c,
                         		Service_Detail__c, Service_General__c, Service_Rig_Program__c, Service_Started__c,
                         		Shear_Placement2__c, Shear_Placement1__c, Shear_Type2__c,
                         		Shear_Type1__c, Stator_Condition2__c, Stator_Condition1__c, Status__c,
                         		Test_Efficiency2__c, Test_Efficiency1__c, Tubing_Failure__c, Tubing_JIH2__c,
                         		Tubing_JIH1__c, Tubing_Size2__c, Tubing_Size1__c
                        		//Service_Type__c,	Submitted__c,, Well_Servicing_Manager__c
                         FROM HOG_EOJ__c WHERE Id = :objId];

        renderButtons = true;
    }

    // W-001394 - check users context and then render buttons accordingly
    public Boolean getIsLightningUser(){
        return HOG_GeneralUtilities.isUserUsingLightning();
    }

	public Boolean getHasErrors() {
		if(ApexPages.hasMessages()) {
			return true;
		} else {
			return false;
		}
	}

    public PageReference submit(){

        Boolean isError = false;
        renderButtons = false;

        if((attList.size() + relatedFiles.size()) <= 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'You can\'t submit EOJ without attachment.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Bail_Well__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Bail Well is empty.'));
        	isError = true;
        }
        if(eojToValidate.Budget_SRP__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Budget is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Coil_Kit2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Coil Kit [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Coil_Kit1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Coil Kit [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Drain_Type2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Drain Type [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Drain_Type1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Drain Type [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Drain_Placement2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Drain Placement [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Drain_Placement1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Drain Placement [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Comments2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Comments [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Comments1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Comments [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.EOJ_Details__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'EOJ Details is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Final_Tag_Depth__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Final Tag Depth is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Fluid_Inj__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Fluid Inj (m3) is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Fluid_Recovered__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Fluid Recovered (m3) is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Initial_Tag_Depth__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Initial Tag Depth is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Intake_Depth2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Intake Depth [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Intake_Depth1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Intake Depth [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Job_Cost_Comments__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Job Cost Comments is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Length_Off_Tag2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Length Off Tag [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Length_Off_Tag1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Length Off Tag [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Next_Location__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Next Location is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.NTT_Type2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'NTT Type [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.NTT_Type1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'NTT Type [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.PBTD__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'PBTD is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Perf_Interval__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Perf Interval is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pin_Size2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pin Size [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pin_Size1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pin Size [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Polished_Rod_Length2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Polished Rod Length [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Polished_Rod_Length1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Polished Rod Length [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Previous_Failure_Detail__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Previous Failure - Detail is empty.'));
        	isError = true;
        }
        if(eojToValidate.Previous_Service_Date__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Previous Service Date is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Producing_Zone__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Producing Zone is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pull_Out_of_Sand__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pull Out of Sand is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pump_Elastomer2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Elastomer [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pump_Elastomer1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Elastomer [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pump_Lift2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Lift [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pump_Lift1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Lift [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pump_Type2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Type (Installed) is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pump_Type1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Type (Pulled) is empty.'));
        	isError = true;
        }

        if(String.isBlank(eojToValidate.Pump_Vendor2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Vendor [I] is empty.'));
        	isError = true;
        } else if( eojToValidate.Pump_Vendor2__c != 'Other' && String.isBlank(eojToValidate.Pump_Displacement_Size2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Displacement/Size [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Pump_Vendor1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Vendor [P] is empty.'));
        	isError = true;
        } else if( eojToValidate.Pump_Vendor1__c != 'Other' && String.isBlank(eojToValidate.Pump_Displacement_Size1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Pump Displacement/Size [P] is empty.'));
        	isError = true;
        }

        if(eojToValidate.Rig_Hours__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rig Hours is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rig__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rig Name is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rig_Company__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rig Name Company is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Comments2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Comments [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Comments1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Comments [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Condition2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Condition [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Condition1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Condition [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Failure__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Failure is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Grade2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Grade [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Grade1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Grade [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Size2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Size [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Size1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Size [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Type2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Type [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rod_Type1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rod Type [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rotor_Condition2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rotor Condition [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rotor_Condition1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rotor Condition [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rotor_Length2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rotor Length [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rotor_Length1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rotor Length [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rotor_Tag_Type2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rotor Tag Type [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Rotor_Tag_Type1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Rotor Tag Type [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Scope_Joint2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Scope Joint [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Scope_Joint1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Scope Joint [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Shear_Placement2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Shear Placement [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Shear_Placement1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Shear Placement [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Shear_Type2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Shear Type [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Shear_Type1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Shear Type [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Stator_Condition2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Stator Condition [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Stator_Condition1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Stator Condition [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Test_Efficiency2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Test Efficiency [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Test_Efficiency1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Test Efficiency [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Tubing_Failure__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Tubing Failure is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Tubing_JIH2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Tubing JIH [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Tubing_JIH1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Tubing JIH [P] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Tubing_Size2__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Tubing Size [I] is empty.'));
        	isError = true;
        }
        if(String.isBlank(eojToValidate.Tubing_Size1__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,'Tubing Size [P] is empty.'));
        	isError = true;
        }
        if(!isError){
            eojToValidate.Status__c = 'Submitted';

            try{
                update eojToValidate;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'EOJ successfully submitted.'));
            } catch (Exception ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'An unexpected error has occurred: ' + ex.getMessage()));
            }

        }


        return null;

    }

    public PageReference navigateBack(){
        PageReference p = new PageReference('/' + eojToValidate.Id);
        return p;
    }

	public String getNavigateToEOJ() {
		String EOJId = '/' + eojToValidate.Id;
		return EOJId;
	}

}