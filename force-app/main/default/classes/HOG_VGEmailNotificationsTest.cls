/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VGEmailNotifications.cls
History:        jschn 05.02.2018 - Created.
                mz: 23-May-2018 - Test method for HOG_VGEmailNotifications.generateEmailNotification()
**************************************************************************************************/
@IsTest
private class HOG_VGEmailNotificationsTest {
	
	@IsTest static void testBatchNotifications_NonUrgent() {
		Test.startTest();
		Database.executeBatch(new HOG_VGEmailNotifications(
			HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS));

		Integer dmlRowsBfr = Limits.getDmlRows();
		Integer dmlStatementsBfr = Limits.getDmlStatements();
		Integer queriesBfr = Limits.getQueries();

		Test.stopTest();
		Integer dmlRowsAftr = Limits.getDmlRows();
		Integer dmlStatementsAftr = Limits.getDmlStatements();
		Integer queriesAftr = Limits.getQueries();


		System.assertNotEquals(dmlStatementsBfr, dmlStatementsAftr);
		System.assertNotEquals(dmlRowsBfr, dmlRowsAftr);
		System.assertNotEquals(queriesBfr, queriesAftr);
		System.assertEquals(12,dmlRowsAftr);
		System.assertEquals(12,dmlStatementsAftr);
	}

	@IsTest static void testBatchNotifications_Urgent() {
		Test.startTest();
		Database.executeBatch(new HOG_VGEmailNotifications(
			HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS));

		Integer dmlRowsBfr = Limits.getDmlRows();
		Integer dmlStatementsBfr = Limits.getDmlStatements();
		Integer queriesBfr = Limits.getQueries();

		Test.stopTest();
		Integer dmlRowsAftr = Limits.getDmlRows();
		Integer dmlStatementsAftr = Limits.getDmlStatements();
		Integer queriesAftr = Limits.getQueries();


		System.assertNotEquals(dmlStatementsBfr, dmlStatementsAftr);
		System.assertNotEquals(dmlRowsBfr, dmlRowsAftr);
		System.assertNotEquals(queriesBfr, queriesAftr);
		System.assertEquals(12,dmlRowsAftr);
		System.assertEquals(12,dmlStatementsAftr);
	}
	

	@IsTest
	static void testGenerateEmailNotification() {

    	List<HOG_Vent_Gas_Alert__c> alertList = [SELECT Id, Comments__c
    	                                        FROM HOG_Vent_Gas_Alert__c
    	                                        LIMIT 1];
    	                                        
    	List<User> UserList = [SELECT Id, Email 
    	                       FROM User 
    	                       WHERE Email = 'Lois@Lane.com.test' 
    	                       LIMIT 1];
    	                       
    	alertList[0].Status__c = HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS;
    	alertList[0].Production_Engineer__c = UserList[0].Id;
    	
    	//There should be no one email invocation
    	System.assertEquals(0,Limits.getEmailInvocations());

    	update alertList[0];
    	
    	//There should be one email invocation
	}


	@TestSetup static void createData() {
		HOG_Vent_Gas_Alert_Configuration__c config = new HOG_Vent_Gas_Alert_Configuration__c(
				Urgent_Priority_Alert_Daily_Notification__c = true,
				High_Priority_Alert_Daily_Notification__c = true,
				Medium_Priority_Alert_Daily_Notification__c = true,
				Low_Priority_Alert_Daily_Notification__c = true);
		insert config;
		Location__c loc = HOG_VentGas_TestData.createLocation();
		User user1 = HOG_VentGas_TestData.createUser('Clark', 'Kent', 'Superman');
		HOG_VentGas_TestData.createUser('Lois', 'Lane', 'Lo');

		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
																		'TestDescription', 
																		loc.Id,
																		user1.Id, 
																		HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
																		HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
																		HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
												'TestDescription', 
												loc.Id,
												user1.Id, 
												HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
												HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, 
												HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
												'TestDescription', 
												loc.Id,
												user1.Id, 
												HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
												HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, 
												HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);
		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_ACKNOWLEDGE;
		update alert;
	}
}