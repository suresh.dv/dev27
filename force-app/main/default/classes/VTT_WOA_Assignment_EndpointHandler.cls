/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Handler for WOA Assignment Endpoint that handles base logic routing
Test Class:     VTT_WOA_Assignment_EndpointTest
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_Assignment_EndpointHandler {

    public static HOG_AccountDAO accountDAO = new HOG_AccountDAOImpl();
    public static HOG_ContactDAO contactDAO = new HOG_ContactDAOImpl();
    public static VTT_WOA_AssignmentDAO assignmentDAO = new VTT_WOA_AssignmentDAOImpl();

    /**
     * This method will build list of HOG PicklistItems (simple version) for Lightning combobox element displaying
     * Accounts.
     *
     * @return List<HOG_PicklistItem_Simple>
     */
    public List<HOG_PicklistItem_Simple> getVendorsPicklistValues() {
        return new HOG_PicklistUtils().simpleWrap(
                accountDAO.getHuskyAccountsByRecordTypeId(VTT_Utilities.GetVendorAccountRecordTypeID()),
                HOG_Constants.FIELD_API_ID,
                HOG_Constants.FIELD_API_NAME
        );
    }

    /**
     * This method will build list of HOG PicklistItems (Simple version) for lightning combobox element displaying
     * Contacts related to Account provided as param.
     *
     * @param accountId
     *
     * @return List<HOG_PicklistItem_Simple>
     */
    public List<HOG_PicklistItem_Simple> getTradesmenByAccountId(Id accountId) {
        return new HOG_PicklistUtils().simpleWrap(
                contactDAO.getActiveContactsByAccId(accountId),
                HOG_Constants.FIELD_API_ID,
                HOG_Constants.FIELD_API_NAME
        );
    }

    /**
     * This method will return list of Tradesmen Ids which are assigned to activity provided as param
     *
     * @param activityId
     *
     * @return List<String>
     */
    public List<String> getAssignedTradesmenForActivity(Id activityId) {
        List<Work_Order_Activity_Assignment__c> assignments = assignmentDAO.getNonRejectedAssignmentsByActivity(activityId);
        return HOG_GeneralUtilities.getListOfParentIds(assignments, 'Tradesman__c');
    }

    /**
     * Run assign logic for provided params.
     * First it try if it is possible to run assignment without tradesmen param.
     * If yes, it runs logic and returns response.
     * If not it will try to run assignment logic with tradesmen param and then returns response
     *
     * @param activities
     * @param tradesmenIds
     * @param vendorAccountId
     *
     * @return VTT_WOA_Assignment_Response
     */
    public VTT_WOA_Assignment_Response assign(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds, Id vendorAccountId) {

        VTT_WOA_Assignment_Response response = new VTT_WOA_Assignment_Service().runAssignmentWithoutTradesmen(
                activities,
                tradesmenIds,
                vendorAccountId
        );

        if(VTT_WOA_Assignment_Response.SUCCESS.equals(response)) return response;

        response = new VTT_WOA_Assignment_Service().runAssignmentWithTradesmen(
                activities,
                tradesmenIds,
                vendorAccountId
        );

        return response;
    }

    /**
     * Run un-assign logic for provided params.
     * First it try if it is possible to run un-assignment without tradesmen param.
     * If yes, it runs logic and returns response.
     * If not it will try to run un-assignment logic with tradesmen param and then returns response
     *
     * @param activities
     * @param tradesmenIds
     * @param vendorAccountId
     *
     * @return VTT_WOA_Assignment_Response
     */
    public VTT_WOA_Assignment_Response unassign(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds, Id vendorAccountId) {

        VTT_WOA_Assignment_Response response = new VTT_WOA_Assignment_Service().runUnAssignmentWithoutTradesmen(
                activities,
                tradesmenIds,
                vendorAccountId
        );

        if(VTT_WOA_Assignment_Response.SUCCESS.equals(response)) return response;

        response = new VTT_WOA_Assignment_Service().runUnAssignmentWithTradesmen(
                activities,
                tradesmenIds
        );

        return response;
    }

}