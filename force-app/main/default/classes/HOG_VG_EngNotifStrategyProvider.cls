/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of HOG_VG_AlertStrategyProvider for Engineering Notification Alerts.
                This class contains logic for deciding whether Engineering Notification Alert can be
                created. As this type of Alert is created on demand by user interaction, this strategy
                provider is used for validation whether it can Alert be created on that location.
Test Class:     HOG_VG_EngNotifStrategyProviderTest
History:        jschn 21/01/2020 - Created.
*************************************************************************************************/
public with sharing class HOG_VG_EngNotifStrategyProvider extends HOG_VG_AlertStrategyProvider {

    public HOG_VG_EngNotifStrategyProvider() {
        super(
                new List<Location__c>(),
                new Map<Id, Location__c>(),
                new Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c>(),
                new Map<String, HOG_Vent_Gas_Alert__c>()
        );
    }

    /**
     * Implementation of decision (validation) logic for creating Engineering Notification Alert.
     * Checks if there are all necessary conditions met.
     *
     * @param location
     *
     * @return Boolean
     */
    public override Boolean shouldCreate(Location__c location) {
        return super.isCorrectLocation(location)
                && correctDateToCreate();
    }

    /**
     * As for this Alert type, it is irrelevant what date it is since it is User based Alert.
     *
     * @return true
     */
    protected override Boolean correctDateToCreate() {
        return true;
    }

}