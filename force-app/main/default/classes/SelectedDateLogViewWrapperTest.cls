@isTest
private class SelectedDateLogViewWrapperTest {
    static Map<String, Schema.Recordtypeinfo> shiftRTMap = Shift_Configuration__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> roleRTMap = Team_Role__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> taRTMap = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> areaRTMap = Process_Area__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> eventRTMap = Operational_Event__c.SObjectType.getDescribe().getRecordTypeInfosByName();
    static Map<String, Schema.Recordtypeinfo> doiRTMap = DOI__c.SObjectType.getDescribe().getRecordTypeInfosByName();

    @testSetup
    static void setUp() {
        Date testDate = Date.today();

        Id OperationsShiftRT = shiftRTMap.get('Operations').getRecordTypeId();
        Id OperationsRoleRT = roleRTMap.get('Operations').getRecordTypeId();
        Id OperationsTART = taRTMap.get('Operations').getRecordTypeId();
        Id OperationsAreaRT = areaRTMap.get('Operations').getRecordTypeId();
        Id OperationsEventRT = eventRTMap.get('Operations').getRecordTypeId();
        Id OperationsDOIRT = doiRTMap.get('Operations').getRecordTypeId();

        Business_Unit__c businessUnit = TestUtilities.createBusinessUnit('Sunrise Field');
        insert businessUnit;

        Business_Department__c businessDepartment = new Business_Department__c(Name = 'Test Business Department');            
        insert businessDepartment;

        Operating_District__c operatingDistrict = TestUtilities.createOperatingDistrict(businessUnit.Id, businessDepartment.Id, 'Sunrise');
        insert operatingDistrict;

        Plant__c plant = TestUtilities.createPlant('test-plant');
        insert plant;

        Unit__c unit = TestUtilities.createUnit('Test Unit', plant.Id);
        insert unit; 

        Shift_Configuration__c dayShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Day', true, '0600', '1800', OperationsShiftRT);
        insert dayShift;
        
        Shift_Configuration__c nightShift = TestUtilities.createShiftConfiguration(operatingDistrict.Id, 'Another Night', true, '1800', '0600', OperationsShiftRT);
        insert nightShift;

        Team_Allocation__c ta = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, dayShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
        insert ta;
    
        Team_Allocation__c taNight = TestUtilities.createTeamAllocation(operatingDistrict.Id, testDate, nightShift.Id, TeamAllocationUtilities.ACTIVE_STATUS_NAME, OperationsTART);
        insert taNight;        

        Account testAccount = TestUtilities.createTestAccount();
        insert testAccount;

        Contact testOperator = TestUtilities.createTestContact(operatingDistrict.Id, testAccount.Id);
        insert testOperator;

        Team_Role__c leaderRole = TestUtilities.createTeamRole('Chief Steam Engineer', true, 1, OperationsRoleRT);
        insert leaderRole;
        
        Shift_Operator__c shiftOp = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, ta.Id, plant.Id, leaderRole.Id);
        insert shiftOp;
        
        Team_Role__c operatorRole = TestUtilities.createTeamRole('Operator', false, -1, OperationsRoleRT);
        insert operatorRole;
        
        Shift_Operator__c shiftOpNight = TestUtilities.createShiftOperator(operatingDistrict.Id, testOperator.Id, taNight.Id, plant.Id, operatorRole.Id);
        insert shiftOpNight;   

        Process_Area__c area = TestUtilities.createProcessArea('Test Area', OperationsAreaRT);
        insert area;
        
        Operational_Event__c opEvent = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, ta.Id, unit.Id, area.Id, plant.Id, OperationsEventRT);
        insert opEvent;
        
        Operational_Event__c opEventNight = TestUtilities.createOperationalEvent(operatingDistrict.Id, testOperator.Id, taNight.Id, unit.Id, area.Id, plant.Id, OperationsEventRT);
        insert opEventNight;  

        DOI__c doi = TestUtilities.createDOI(plant.Id, OperationsDOIRT);
        insert doi;

    }

    @isTest
    static void testLookupShifts() {
        Set<Id> shiftIds = new Set<Id>();

        List<Shift_Configuration__c> shifts = [SELECT Id FROM Shift_Configuration__c];

        for(Shift_Configuration__c shift : shifts) {
            shiftIds.add(shift.Id);
        }

        SelectedDateLogViewWrapper logViewWrapper = new SelectedDateLogViewWrapper(Date.today(), taRTMap.get('Operations').getRecordTypeId());
        List<Shift_Configuration__c> shiftList = logViewWrapper.lookupShifts(shiftIds);

        System.assertEquals(2, shiftList.size());
    }



}