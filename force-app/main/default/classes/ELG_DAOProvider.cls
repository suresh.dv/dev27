/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    DAO Provider. Holds all DAO Implementation providing required functionality for E-Logs
                service.
History:        mbrimus 2019-07-11 - Created.
*************************************************************************************************/
public with sharing class ELG_DAOProvider {
	public static ELG_ShiftHandoverDAO shiftHandoverDao = new ELG_ShiftHandoverSelector();
	public static ELG_HandoverForm_DAO handoverDAO = new ELG_HandoverForm_QuerySelector();
	public static ELG_Task_DAO taskDAO = new ELG_Task_QuerySelector();
}