@IsTest
private class DEFT_EngineeringDiagnosisControllerXTest {

	@IsTest static void testEntireController() {
		User runningUser = DEFT_TestData.createDeftUser();
		User productionCoordinator = DEFT_TestData.createProductionCoordinator();
		User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
		User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

		HOG_Service_Rig_Program__c serviceRigProgram;

		System.runAs(serviceRigPlanner) {
			serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
			serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
			serviceRigProgram.Technical_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.First_Financial_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.Execution_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.Status__c = 'Started';
			update serviceRigProgram;

			DEFT_UtilitiesTest.approveSrpFinancial(serviceRigProgram, runningUser);
			DEFT_UtilitiesTest.approveSrpPET(serviceRigProgram, runningUser);
		}

		System.runAs(runningUser) {
			System.debug('User and srp ' + serviceRigProgram.Status__c);
			HOG_EOJ__c eoj = DEFT_TestData.createEOJReport(serviceRigProgram, 'Submitted');

			HOG_Engineering_Diagnosis__c engDiag = DEFT_TestData.createEngDiagReport(serviceRigProgram);

			Test.startTest();

			List<HOG_Engineering_Diagnosis__c> testEmptyValues = [
					SELECT Oil_Viscosity_20_C__c, Oil_Density_15_C__c
					FROM HOG_Engineering_Diagnosis__c
			];

			System.assertEquals(1, testEmptyValues.size(), 'More than one Engineering report was created.');
			System.assertEquals(null, testEmptyValues[0].Oil_Viscosity_20_C__c, 'Oil Viscosity was populated.');
			System.assertEquals(null, testEmptyValues[0].Oil_Density_15_C__c, 'Oil Density was populated.');

			engDiag.Oil_Viscosity_20_C__c = 50;
			engDiag.Oil_Density_15_C__c = 25;
			update engDiag;

			ApexPages.StandardController stdController = new ApexPages.StandardController(engDiag);
			DEFT_EngineeringDiagnosisControllerX con = new DEFT_EngineeringDiagnosisControllerX(stdController);
			con.retrievePickListValues('HOG_Engineering_Diagnosis__c', 'Diagnosis_General__c');

			engDiag.Status__c = 'Complete';
			con.saveED();

			HOG_Engineering_Diagnosis__c engDiag2 = new HOG_Engineering_Diagnosis__c();
			engDiag2.Service_Rig_Program__c = serviceRigProgram.Id;
			engDiag2.Status__c = 'In Progress';
			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(engDiag2);
			DEFT_EngineeringDiagnosisControllerX edCtrl = new DEFT_EngineeringDiagnosisControllerX(stdCtrl);
			edCtrl.saveED();

			System.assertEquals(engDiag.Oil_Density_15_C__c, engDiag2.Oil_Density_15_C__c, 'Contructor haven\'t prepopulate Density value');
			System.assertEquals(engDiag.Oil_Viscosity_20_C__c, engDiag2.Oil_Viscosity_20_C__c, 'Contructor haven\'t prepopulate Viscosity value');

			Test.stopTest();
		}
	}

	@IsTest static void testErrorMessages() {
		User runningUser = DEFT_TestData.createDeftUser();
		//User productionEnegineer = DEFT_TestData.createProductionEnegineer();
		User productionCoordinator = DEFT_TestData.createProductionCoordinator();
		User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
		User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();
		HOG_Service_Rig_Program__c serviceRigProgram;
		Approval.ProcessResult result;

		System.runAs(serviceRigPlanner) {
			serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
			serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
			serviceRigProgram.Technical_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.First_Financial_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.Execution_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.Status__c = 'Started';
			update serviceRigProgram;

			DEFT_UtilitiesTest.approveSrpFinancial(serviceRigProgram, runningUser);
			DEFT_UtilitiesTest.approveSrpPET(serviceRigProgram, runningUser);
		}

		System.runAs(runningUser) {
			//This will force fist message because engDiag will not have Service_Rig_Program__c field prepopulated
			HOG_Engineering_Diagnosis__c engDiag = new HOG_Engineering_Diagnosis__c();

			HOG_EOJ__c eoj = DEFT_TestData.createEOJReport(serviceRigProgram, 'Submitted');

			Test.startTest();
			ApexPages.StandardController stdController = new ApexPages.StandardController(engDiag);
			DEFT_EngineeringDiagnosisControllerX con = new DEFT_EngineeringDiagnosisControllerX(stdController);

			//This will force second message, because user is
			//trying to create Engineering Diagnosis on SRP that has no EOJ with status Submitted
			con.initializeEndOfJobReport();
			Test.stopTest();

		}

	}

	@IsTest static void testPVRServiceCallout() {
		User runningUser = DEFT_TestData.createDeftUser();
		//User productionEnegineer = DEFT_TestData.createProductionEnegineer();
		User productionCoordinator = DEFT_TestData.createProductionCoordinator();
		User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
		User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

		HOG_Service_Rig_Program__c serviceRigProgram;
		Approval.ProcessResult result;
		List<Id> newWorkItemIds;

		System.runAs(serviceRigPlanner) {
			//Data Setup - Step 1 (Create Service Rig Program)
			serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
			serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
			serviceRigProgram.Technical_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.First_Financial_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.Execution_Approver__c = serviceRigPlanner.Id;
			serviceRigProgram.Status__c = 'Started';
			update serviceRigProgram;
			DEFT_UtilitiesTest.approveSrpFinancial(serviceRigProgram, runningUser);
			DEFT_UtilitiesTest.approveSrpPET(serviceRigProgram, runningUser);
		}

		System.runAs(runningUser) {
			//Create Custom Setting Data
			HOG_Settings__c hogCustomSettings = new HOG_Settings__c();
			hogCustomSettings.SAP_DPVR_Endpoint__c = 'https://test.pvrservice.com';
			hogCustomSettings.SAP_DPVR_Timeout__c = 120000;
			hogCustomSettings.Client_Certificate_Name__c = 'HuskyCertificate';
			insert hogCustomSettings;

			HOG_EOJ__c eoj = DEFT_TestData.createEOJReport(serviceRigProgram, 'Submitted');

			HOG_Engineering_Diagnosis__c engDiag = DEFT_TestData.createEngDiagReport(serviceRigProgram);

			Test.startTest();
			PageReference viewPage = Page.DEFT_EngineeringDiagnosisView;
			viewPage.getParameters().put('id', engDiag.Id);
			Test.setCurrentPage(viewPage);

			ApexPages.StandardController stdController = new ApexPages.StandardController(engDiag);
			DEFT_EngineeringDiagnosisControllerX con = new DEFT_EngineeringDiagnosisControllerX(stdController);
			System.debug('eoj: ' + eoj + ' con.endOfJobReport: ' + con.endOfJobReport);
			System.assertEquals(eoj.Id, con.endOfJobReport.Id);
			System.assertEquals(engDiag.Id, con.eDiagnosis.Id);

			DEFT_Utilities.PVRProductionValuesResponse response = new DEFT_Utilities.PVRProductionValuesResponse();
			response.producedOilBetweenServices = 50.54;
			response.producedWaterBetweenServices = 65.45;
			response.producedGasBetweenServices = 34.45;
			response.producedSandBetweenServices = 45.22;
			response.operatingHoursBetweenServices = 72;

			//Test Webservice Invalid Reponse
			response.pvrUwiRaw = 'invalidPVRRawUWI';
			Test.setMock(HttpCalloutMock.class, new DEFT_PVRProductionServiceCalloutMock(200, 'OK', JSON.serialize(response),
					new Map<String, String>{
							'Content-Type' => 'application/json'
					}));
			System.assertEquals(null, con.updatePVRProductionValues());
			System.assert(wasMessageAdded(
					new ApexPages.Message(ApexPages.Severity.INFO,
							'Invalid response from PVR Service'),
					ApexPages.getMessages()
			));

			//TEST Webservice Error Response (StatusCode <> 200)
			Test.setMock(HttpCalloutMock.class, new DEFT_PVRProductionServiceCalloutMock(404, 'Not Found', JSON.serialize(response),
					new Map<String, String>()));
			System.assertEquals(null, con.updatePVRProductionValues());
			System.assert(wasMessageAdded(
					new ApexPages.Message(ApexPages.Severity.ERROR,
							'Received following error code from PVR service: ' + 404),
					ApexPages.getMessages()
			));

			//Test Webservice Successful Response
			response.pvrUwiRaw = con.endOfJobReport.Well_Event_PVR_UWI_RAW__c;
			Test.setMock(HttpCalloutMock.class, new DEFT_PVRProductionServiceCalloutMock(200, 'OK', JSON.serialize(response),
					new Map<String, String>{
							'Content-Type' => 'application/json'
					}));
			System.assertEquals(null, con.updatePVRProductionValues());
			engDiag = [
					SELECT Id, Name, Total_Oil_Production_Between_Services__c, Total_Water_Production_Between_Services__c,
							Total_Gas_Production_Between_Services__c, Total_Sand_Production_Between_Services__c, PVR_Runtime__c
					FROM HOG_Engineering_Diagnosis__c
					WHERE Id = :engDiag.Id
			];
			System.assertEquals(response.producedOilBetweenServices, engDiag.Total_Oil_Production_Between_Services__c);
			System.assertEquals(response.producedWaterBetweenServices, engDiag.Total_Water_Production_Between_Services__c);
			System.assertEquals(response.producedGasBetweenServices, engDiag.Total_Gas_Production_Between_Services__c);
			System.assertEquals(response.producedSandBetweenServices, engDiag.Total_Sand_Production_Between_Services__c);
			Test.stopTest();
		}
	}

	private static Boolean wasMessageAdded(ApexPages.Message message, ApexPages.Message[] pageMessages) {
		Boolean messageFound = false;

		for (ApexPages.Message msg : pageMessages) {
			System.debug('msg: ' + msg + ' message: ' + message);
			if (msg.getSummary() == message.getSummary()
					&& msg.getDetail() == message.getDetail()
					&& msg.getSeverity() == message.getSeverity()) {
				messageFound = true;
			}
		}

		return messageFound;
	}

}