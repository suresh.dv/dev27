/*-----------------------------------------------------------------------------------------------
Author     : Miro Zelina
Company    : Husky Energy
Description: A test class to test hog sap equipment service classes: HOG_SAPEquipmentService, HOG_EquipmentFactoryUtils, HOG_SAPApiUtils
History    : 20-Mar-2017 - Created
             07.Jul-2017 - Included System & Sub-System
             03.Okt-2018 - Mbrimus update case when removing superior eq W-001231
-------------------------------------------------------------------------------------------------*/
@IsTest
private class HOG_SAPEquipmentServiceTest {


    static testMethod void processEquipment() {

        /*****************************     DT_SAP_RIHEQUI_LIST     ********************************/

        //Create instances of DT_SAP_RIHEQUI_LIST webservice class
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_SU_FAC = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_FAC = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_LOC = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_EVE = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_SYS = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_SUB = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_FEL = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_YARD = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();

        //Superiror Equipmnent for Facility
        RIHEQUI_SU_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Superior Equipment on Facility',
                '000000000062041400',
                'Model A',
                'H1-09-001-40018-000001',
                'X',
                null);

        //Inferior Equipmnent for Facility                                                                  
        RIHEQUI_IN_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferiro Equipment on Facility',
                '000000000062041401',
                'Model 01',
                'H1-09-001-40018-000001',
                'X',
                '000000000062041400');
        //Inferior Equipmnent for Location                                                                   
        RIHEQUI_IN_LOC = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferior Equipment on Location',
                '000000000062041402',
                'Model 02',
                'H1-09-001-40018-000002',
                'X',
                '000000000062041400');
        //Inferior Equipmnent for Well Event 
        RIHEQUI_IN_EVE = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferior Equipment on Event',
                '000000000062041403',
                'Model 03',
                'H1-09-001-40018-000003',
                'X',
                '000000000062041400');

        //Inferior Equipmnent for System
        RIHEQUI_IN_SYS = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferior Equipment on System',
                '000000000062041404',
                'Model 04',
                'H1-09-001-40018-000004',
                'X',
                '000000000062041400');

        //Inferior Equipmnent for Sub-System
        RIHEQUI_IN_SUB = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferior Equipment on Sub-System',
                '000000000062041405',
                'Model 05',
                'H1-09-001-40018-000005',
                'X',
                '000000000062041400');
        //Inferior Equipmnent for Fel
        RIHEQUI_IN_FEL = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferior Equipment on FEL',
                '000000000062041406',
                'Model 05',
                'H1-09-001-40018-000006',
                'X',
                '000000000062041400');
        //Inferior Equipmnent for Yard
        RIHEQUI_IN_YARD = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferior Equipment on Yard',
                '000000000062041407',
                'Model 05',
                'H1-09-001-40018-000007',
                'X',
                '000000000062041400');


        /*****************************   DT_SAP_ZIPM_CHAR_DATA   ********************************/

        //Create instance of DT_SAP_ZIPM_CHAR_DATA webservice class
        HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA ZIPM_CHAR_DATA = new HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA();
        ZIPM_CHAR_DATA = HOG_SAPEquipmentServiceTestData.createDT_SAP_ZIPM_CHAR_DATA();


        /*****************************       DT_SAP_EQUIP_ITEM       ********************************/

        //Create instances of DT_SAP_EQUIP_ITEM webservice class
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_SU_FAC = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_FAC = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_LOC = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_EVE = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_SYS = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_SUB = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_FEL = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_YARD = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();

        List<HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA> ZIPM_CHAR_DATA_LIST = new List<HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA>{
                ZIPM_CHAR_DATA
        };


        //Superiror Equipmnent for Facility
        EQUIP_ITEM_SU_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_SU_FAC,
                ZIPM_CHAR_DATA_LIST);

        //Inferior Equipmnent for Facility                                                               
        EQUIP_ITEM_IN_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_FAC,
                ZIPM_CHAR_DATA_LIST);

        //Inferior Equipmnent for Location
        EQUIP_ITEM_IN_LOC = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_LOC,
                ZIPM_CHAR_DATA_LIST);

        //Inferior Equipmnent for Well Event                                                               
        EQUIP_ITEM_IN_EVE = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_EVE,
                ZIPM_CHAR_DATA_LIST);

        //Inferior Equipmnent for System                                                               
        EQUIP_ITEM_IN_SYS = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_SYS,
                ZIPM_CHAR_DATA_LIST);

        //Inferior Equipmnent for Sub-System                                                               
        EQUIP_ITEM_IN_SUB = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_SUB,
                ZIPM_CHAR_DATA_LIST);

        //Inferior Equipmnent for FEL                                                               
        EQUIP_ITEM_IN_FEL = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_FEL,
                ZIPM_CHAR_DATA_LIST);
        //Inferior Equipmnent for Yard                                                          
        EQUIP_ITEM_IN_YARD = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_YARD,
                ZIPM_CHAR_DATA_LIST);


        /*****************************  DT_SAP_EQUIP_LIST  ********************************/
        HOG_SAPApiUtils.DT_SAP_EQUIP_LIST eq_list = new HOG_SAPApiUtils.DT_SAP_EQUIP_LIST();
        List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM> item_list = new List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM>{
                EQUIP_ITEM_SU_FAC,
                EQUIP_ITEM_IN_FAC,
                EQUIP_ITEM_IN_LOC,
                EQUIP_ITEM_IN_EVE,
                EQUIP_ITEM_IN_SYS,
                EQUIP_ITEM_IN_SUB,
                EQUIP_ITEM_IN_FEL,
                EQUIP_ITEM_IN_YARD
        };

        eq_list.EQUIPMENT = item_list;

        /*****************************          START TEST           ********************************/
        Test.startTest();


        HOG_SAPEquipmentService.processEquipment(eq_list);


        List<Equipment__c> eq_su_fac = [
                SELECT Description_of_Equipment__c, Superior_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Superior Equipment on Facility'
        ];

        List<Equipment__c> eq_in_fac = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferiro Equipment on Facility'
        ];

        List<Equipment__c> eq_in_loc = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferior Equipment on Location'
        ];

        List<Equipment__c> eq_in_eve = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferior Equipment on Event'
        ];

        List<Equipment__c> eq_in_sys = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferior Equipment on System'
        ];

        List<Equipment__c> eq_in_sub = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferior Equipment on Sub-System'
        ];

        List<Equipment__c> eq_in_fel = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferior Equipment on FEL'
        ];

        List<Equipment__c> eq_in_yard = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferior Equipment on Yard'
        ];

        System.assertEquals(1, eq_su_fac.size());
        System.assertEquals(1, eq_in_fac.size());
        System.assertEquals(1, eq_in_loc.size());
        System.assertEquals(1, eq_in_eve.size());
        System.assertEquals(1, eq_in_sys.size());
        System.assertEquals(1, eq_in_sub.size());
        System.assertEquals(1, eq_in_fel.size());
        System.assertEquals(1, eq_in_yard.size());

        System.assertEquals(null, eq_su_fac[0].Superior_Equipment__c);
        System.assertEquals('Test Superior Equipment on Facility', eq_in_fac[0].Superior_Equipment__r.Description_of_Equipment__c);
        System.assertEquals('Test Superior Equipment on Facility', eq_in_loc[0].Superior_Equipment__r.Description_of_Equipment__c);
        System.assertEquals('Test Superior Equipment on Facility', eq_in_eve[0].Superior_Equipment__r.Description_of_Equipment__c);
        System.assertEquals('Test Superior Equipment on Facility', eq_in_sys[0].Superior_Equipment__r.Description_of_Equipment__c);
        System.assertEquals('Test Superior Equipment on Facility', eq_in_sub[0].Superior_Equipment__r.Description_of_Equipment__c);

        Test.stopTest();
        /*****************************           END TEST           ********************************/
    }

    static testMethod void testWhenEquipmentLosesSuperiorEquipment_LookupOnEquipmentShouldNull() {

        /*****************************     DT_SAP_RIHEQUI_LIST     ********************************/

        //Create instances of DT_SAP_RIHEQUI_LIST webservice class
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_SU_FAC = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_IN_FAC = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();

        //Superiror Equipmnent for Facility
        RIHEQUI_SU_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Superior Equipment on Facility',
                '000000000062041400',
                'Model A',
                'H1-09-001-40018-000001',
                'X',
                null);

        //Inferior Equipmnent for Facility
        RIHEQUI_IN_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferiro Equipment on Facility',
                '000000000062041401',
                'Model 01',
                'H1-09-001-40018-000001',
                'X',
                '000000000062041400');

        /*****************************   DT_SAP_ZIPM_CHAR_DATA   ********************************/

        //Create instance of DT_SAP_ZIPM_CHAR_DATA webservice class
        HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA ZIPM_CHAR_DATA = new HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA();
        ZIPM_CHAR_DATA = HOG_SAPEquipmentServiceTestData.createDT_SAP_ZIPM_CHAR_DATA();


        /*****************************       DT_SAP_EQUIP_ITEM       ********************************/

        //Create instances of DT_SAP_EQUIP_ITEM webservice class
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_SU_FAC = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM EQUIP_ITEM_IN_FAC = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();

        List<HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA> ZIPM_CHAR_DATA_LIST = new List<HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA>{
                ZIPM_CHAR_DATA
        };


        //Superiror Equipmnent for Facility
        EQUIP_ITEM_SU_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_SU_FAC,
                ZIPM_CHAR_DATA_LIST);

        //Inferior Equipmnent for Facility
        EQUIP_ITEM_IN_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_FAC,
                ZIPM_CHAR_DATA_LIST);



        /*****************************  DT_SAP_EQUIP_LIST  ********************************/
        HOG_SAPApiUtils.DT_SAP_EQUIP_LIST eq_list = new HOG_SAPApiUtils.DT_SAP_EQUIP_LIST();
        List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM> item_list = new List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM>{
                EQUIP_ITEM_SU_FAC,
                EQUIP_ITEM_IN_FAC

        };

        eq_list.EQUIPMENT = item_list;

        /*****************************          START TEST           ********************************/
        Test.startTest();

        HOG_SAPEquipmentService.processEquipment(eq_list);

        List<Equipment__c> eq_in_fac = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferiro Equipment on Facility'
        ];

        System.assertEquals(1, eq_in_fac.size());
        System.assertNotEquals(null, eq_in_fac[0].Superior_Equipment__c, 'Inferior equipment should have parent');

        //Remove superior from child equipment
        RIHEQUI_IN_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_RIHEQUI_LIST('Test Inferiro Equipment on Facility',
                '000000000062041401',
                'Model 01',
                'H1-09-001-40018-000001',
                'X',
                '');

        // Setup and process inferior eq again
        ZIPM_CHAR_DATA = HOG_SAPEquipmentServiceTestData.createDT_SAP_ZIPM_CHAR_DATA();

        EQUIP_ITEM_IN_FAC = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        ZIPM_CHAR_DATA_LIST = new List<HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA>{
                ZIPM_CHAR_DATA
        };
        EQUIP_ITEM_IN_FAC = HOG_SAPEquipmentServiceTestData.createDT_SAP_EQUIP_ITEM(RIHEQUI_IN_FAC,
                ZIPM_CHAR_DATA_LIST);

        eq_list = new HOG_SAPApiUtils.DT_SAP_EQUIP_LIST();
        item_list = new List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM>{
                EQUIP_ITEM_IN_FAC
        };

        eq_list.EQUIPMENT = item_list;
        HOG_SAPEquipmentService.processEquipment(eq_list);

        eq_in_fac = [
                SELECT Description_of_Equipment__c, Superior_Equipment__r.Description_of_Equipment__c
                FROM Equipment__c
                WHERE Description_of_Equipment__c = 'Test Inferiro Equipment on Facility'
        ];
        System.assertEquals(1, eq_in_fac.size());
        System.assertEquals(null, eq_in_fac[0].Superior_Equipment__c, 'Inferior equipment should have parent');

        Test.stopTest();
        /*****************************           END TEST           ********************************/
    }

    //Create test data for equipment processing
    @TestSetup
    private static void testSetup() {


        //Create Route record
        Route__c route = new Route__c();
        route.Name = 'Test Route';
        insert route;

        //Create Business Deparment record
        Business_Department__c busDept = new Business_Department__c();
        busDept.Name = 'Test Business Department';
        insert busDept;

        //Create Operating District
        Operating_District__c operDist = new Operating_District__c();
        operDist.Name = 'Test Operating District';
        operDist.Business_Department__c = busDept.Id;
        insert operDist;

        //Create Oper. Field (AMU)
        Field__c field = new Field__c();
        field.Name = 'Test Operating Field';
        field.Operating_District__c = operDist.Id;
        insert field;

        //Create Facility
        Facility__c facility = new Facility__c();
        facility.Name = 'Test Facility';
        facility.Functional_Location__c = 'H1-09-001-40018-000001';
        facility.Operating_Field_AMU__c = field.Id;
        insert facility;

        //Create Location
        Location__c location = new Location__c();
        location.Name = 'Test Location';
        location.Functional_Location__c = 'H1-09-001-40018-000002';
        location.Functional_Location_Description_SAP__c = 'H1-09-001-40018-000002';
        location.Operating_Field_AMU__c = field.Id;
        location.Route__c = route.Id;
        insert location;

        //Create Well Event
        Well_Event__c wellEvent = new Well_Event__c();
        wellEvent.Name = 'Test Well Event';
        wellEvent.Functional_Location__c = 'H1-09-001-40018-000003';
        wellEvent.Well_ID__c = location.Id;
        insert wellEvent;

        //Create System
        System__c sys = new System__c();
        sys.Name = 'Test System';
        sys.Functional_Location_Description_SAP__c = 'H1-09-001-40018-000004';
        sys.Operating_Field_AMU__c = field.Id;
        insert sys;

        //Create Sub-System
        Sub_System__c subSys = new Sub_System__c();
        subSys.Name = 'Test Sub-System';
        subSys.Functional_Location_Description_SAP__c = 'H1-09-001-40018-000005';
        insert subSys;

        //Create Fel
        Functional_Equipment_Level__c fel = new Functional_Equipment_Level__c();
        fel.Name = 'Test fel';
        fel.Operating_Field_AMU__c = field.Id;
        fel.Functional_Location_Description_SAP__c = 'H1-09-001-40018-000006';
        insert fel;

        //Create Yard
        Yard__c yard = new Yard__c();
        yard.Name = 'Test yard';
        yard.Operating_Field_AMU__c = field.Id;
        yard.Functional_Location_Description_SAP__c = 'H1-09-001-40018-000007';
        insert yard;

    }

}