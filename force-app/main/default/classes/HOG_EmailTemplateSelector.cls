/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class for EmailTemplate SObject.
Test Class:     HOG_EmailTemplateSelectorTest
History:        jschn 2019-07-03 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class HOG_EmailTemplateSelector {

    /**
     * Queries Email Template based on developer name.
     * Returned fields:
     * Id
     *
     * @param developerName
     *
     * @return EmailTemplate
     */
    public EmailTemplate getTemplateByDevName(String developerName) {
        return [
                SELECT Id
                FROM EmailTemplate
                WHERE DeveloperName = :developerName
                LIMIT 1
        ];
    }

}