public class PTValidateBillingCacheQuery {
	public static Map<String,Service__c> serviceMap = new Map<String,Service__c>();
    public static boolean serviceListQueried = false;

    
    public static Map<String,Service__c> queryServiceList(){
        if(serviceListQueried == false){
            for(Service__c service : [select id,site_id__c,utility_rate_id__c,minimum_contract_demand__c,utility_company__r.name,utility_rate_id__r.rate_minimum__c,(select billing_demand_kw__c,metered_demand_kw__c,total_volume__c,wire_cost__c,total_cost__c,Utility_Billing__c.Metered_Demand_KVA__c  from utility_billings__r order by billing_date__c desc limit 12 ) from service__c limit 50000]){
                if(service.Site_Id__c != null){
                    serviceMap.put(service.site_id__c,service);
                }
            }
            
        }        
        serviceListQueried = true;
       	return serviceMap;
    }
}