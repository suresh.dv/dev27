public with sharing class ScaffoldInspectionTestData {

	public static Scaffold_Inspection__c createScaffoldInspection(Scaffold__c scaffold) {
		Scaffold_Inspection__c scaffoldInspection = new Scaffold_Inspection__c();
		scaffoldInspection.Scaffold__c = scaffold.Id;
		scaffoldInspection.Site__c = 'HLU';
		scaffoldInspection.Date__c = Date.today();
		
		insert scaffoldInspection;
		return scaffoldInspection;
	}
    
    public static Scaffold_Inspection__c approveScaffoldInspection(Scaffold_Inspection__c inspection) {
        inspection.Toe_board_surface__c = 'Yes';
        inspection.Toe_board_point_load__c = 'Yes';
        inspection.Toe_board_above_platform__c = 'Yes';
        inspection.Tags_updated__c = 'Yes';
        inspection.Tags_accessess__c = 'Yes';
        inspection.Stairs_wide__c = 'Yes';
        inspection.Stairs_vertical__c = 'Yes';
        inspection.Stairs_height__c = 'Yes';
        inspection.Stairs_depth__c = 'Yes';
        inspection.Stairs_angle__c = 'Yes';
        inspection.Sills_Secured_from_movement__c = 'Yes';
        inspection.Sills_Load_Distribution__c = 'Yes';
        inspection.Sills_Hard_Surfaces__c = 'Yes';
        inspection.Sills_Base_plates_upright__c = 'Yes';
        inspection.Sills_Base_plates_mudsills__c = 'Yes';
        inspection.Safe_Distance_walkway__c = 'Yes';
        inspection.Safe_Distance_impeed_acess__c = 'Yes';
        inspection.Safe_Distance_conductors__c = 'Yes';
        inspection.Ramps_resistance__c = 'Yes';
        inspection.Plum_And_Level_height__c = 'Yes';
        inspection.Plum_And_Level_distance__c = 'Yes';
        inspection.Platforms_span__c = 'Yes';
        inspection.Platforms_painted__c = 'Yes';
        inspection.Platforms_mud_sills__c = 'Yes';
        inspection.Platforms_movement__c = 'Yes';
        inspection.Platforms_minimum_wide__c = 'Yes';
        inspection.Platforms_light_duty__c = 'Yes';
        inspection.Platforms_Scafford_plank__c = 'Yes';
        inspection.Platforms_Planks__c = 'Yes';
        inspection.Platforms_No_gaps__c = 'Yes';
        inspection.Ladders_deck_height__c = 'Yes';
        inspection.Ladders_cage_from_ladder__c = 'Yes';
        inspection.Ladders_cage_extends__c = 'Yes';
        inspection.Ladders_cage_begin__c = 'Yes';
        inspection.Ladders_access_point__c = 'Yes';
        inspection.Ladder_rest_platform__c = 'Yes';
        inspection.Ladder_clear_span__c = 'Yes';
        inspection.Ladder_cage_verticals__c = 'Yes';
        inspection.Hangers_main_clamp__c = 'Yes';
        inspection.Hangers_joiners__c = 'Yes';
        inspection.Guardrails_vertical_members__c = 'Yes';
        inspection.Guardrails_point_load__c = 'Yes';
        inspection.Guardrails_over_height__c = 'Yes';
        inspection.Guardrails_grade_lumber__c = 'Yes';
        inspection.Guardrails_Top_Guard_Rails__c = 'Yes';
        inspection.Guardrails_Midway__c = 'Yes';
        inspection.Guardrails_Gap__c = 'Yes';
        inspection.Engineering__c = 'Yes';
        inspection.Connections_wedges__c = 'Yes';
        inspection.Connections_clamps__c = 'Yes';
        inspection.Connections_Transoms__c = 'Yes';
        inspection.Connections_Node__c = 'Yes';
        inspection.Connections_Ledgers__c = 'Yes';
        inspection.Connections_Joints__c = 'Yes';
        inspection.Connections_Bottom__c = 'Yes';
        inspection.Bracing_wind_force__c = 'Yes';
        inspection.Bracing_head_room__c = 'Yes';
        inspection.Bracing_guy_wired__c = 'Yes';
        inspection.Bracing_free_standing__c = 'Yes';
        inspection.Bracing_csa__c = 'Yes';
        inspection.Access_scafford__c = 'Yes';
        inspection.Access_opening__c = 'Yes';
        inspection.isLocked__c = True;
        
        update inspection;
        
        return inspection;
    }
}