public class WeldingProcedureReviewTemplate {
    public Id relatedWeldingProcedureId {get; set;}
 
    public Related_Welding_Procedure__c relatedWP{get;set;}
    
    public String vendorAddress{get; set;}
    
    public String comments{get; set;}
    
    public WeldingProcedureReviewTemplate() {
        relatedWeldingProcedureId = ApexPages.currentPage().getParameters().get('relatedWeldingProcedureId');
    
        relatedWP = [SELECT Id, Comments__c, LastModifiedBy.Name, Reviewed_Date__c, Status__c, Welding_Procedure__r.Vendor__r.Name, Welding_Procedure__r.Vendor__r.BillingStreet,
                            Welding_Procedure__r.Vendor__r.BillingCity, Welding_Procedure__r.Vendor__r.BillingState, Welding_Procedure__r.Vendor__r.BillingPostalCode, 
                            Welding_Procedure__r.Vendor__r.BillingCountry, Welding_Procedure__r.Revision_Number__c,
                            Welding_Procedure__r.Vendor__r.Phone, Welding_Procedure__r.Vendor__r.Fax, Welding_Request__r.Project_Name__c, Welding_Request__r.Business_Unit__c, Welding_Procedure__r.WPS_Number__c,
                            Welding_Procedure__r.Application__c, Welding_Procedure__r.Registered_Locations__c, Welding_Procedure__r.PQR_Number__c, Welding_Procedure__r.Process__c,
                            Welding_Procedure__r.Position__c, Welding_Procedure__r.Material_1__c, Welding_Procedure__r.Material_2__c, Welding_Procedure__r.Material_1_Other__c,
                            Welding_Procedure__r.Material_2_Other__c, Welding_Procedure__r.Electrode__c, Welding_Procedure__r.Min_Thickness__c, Welding_Procedure__r.Max_Thickness__c,
                            Welding_Procedure__r.Min_Diameter__c, Welding_Procedure__r.Max_Diameter__c, Welding_Procedure__r.Hardness__c, Welding_Procedure__r.Service__c,
                            Welding_Procedure__r.AsWelded__c, Welding_Procedure__r.PWHT__c, Welding_Procedure__r.LowTemp__c, Welding_Procedure__r.NormTemp__c 
                     FROM Related_Welding_Procedure__c
                     WHERE Id =: relatedWeldingProcedureId];
        
        vendorAddress = relatedWP.Welding_Procedure__r.Vendor__r.BillingStreet != Null ? relatedWP.Welding_Procedure__r.Vendor__r.BillingStreet:''  + ' ' + 
                        relatedWP.Welding_Procedure__r.Vendor__r.BillingCity != Null ? relatedWP.Welding_Procedure__r.Vendor__r.BillingCity:''  + ', ' + 
                        relatedWP.Welding_Procedure__r.Vendor__r.BillingState != Null ? relatedWP.Welding_Procedure__r.Vendor__r.BillingState:'' + ', ' + 
                        relatedWP.Welding_Procedure__r.Vendor__r.BillingCountry != Null ? relatedWP.Welding_Procedure__r.Vendor__r.BillingCountry:'' + ', ' + 
                        relatedWP.Welding_Procedure__r.Vendor__r.BillingPostalCode != Null ? relatedWP.Welding_Procedure__r.Vendor__r.BillingPostalCode:'';
        
        comments = relatedWP.Comments__c;
    }
}