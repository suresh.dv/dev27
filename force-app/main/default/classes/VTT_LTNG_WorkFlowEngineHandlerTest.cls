/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_WorkFlowEngineHandler class
History:        mbrimus 20/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_WorkFlowEngineHandlerTest {

    @IsTest
    static void handleSaveAction_withoutParam() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;

        Test.startTest();
        try {
            handler.handleSaveAction(null, null, null);
        } catch (Exception e) {
            exceptionThrown = true;
        }
        Test.stopTest();

        System.assertEquals(true, exceptionThrown);
    }

    @IsTest
    static void handleSaveAction_StartJob() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Start_Job_Action;
        Integer expectedSize = 1;

        String startJobAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveAction(startJobAction, activity, tradesmanContact);
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTJOB LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_StartAtEquipment() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Start_At_Equipment_Action;
        Integer expectedSize = 1;

        String startAtEquipmentAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveAction(startAtEquipmentAction, activity, tradesmanContact);
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_StartJobAtEquipment() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Start_Job_At_Equipment_Action;
        Integer expectedSize = 1;

        String startJobActionAtEquipmentAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result_startJob;
        List<Work_Order_Activity_Log_Entry__c> result_startJobAtEq;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveAction(startJobActionAtEquipmentAction, activity, tradesmanContact);
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result_startJob = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTJOB LIMIT 1];
        result_startJobAtEq = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result_startJob.size());
        System.assertEquals(expectedSize, result_startJobAtEq.size());
    }

    @IsTest
    static void handleSaveAction_FinishedAtEquipment() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Finished_Job_At_Equipment_Action;
        Integer expectedSize = 1;

        String finishAtEquipmentAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveAction(finishAtEquipmentAction, activity, tradesmanContact);
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_FINISHEDATSITE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_FinishedForTheDay() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Finished_for_the_Day_Action;
        Integer expectedSize = 1;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String finishForToday = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);

            try {
                handler.handleSaveAction(startAtEq, activity, tradesmanContact);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;
                handler.handleSaveAction(finishForToday, activity, tradesmanContact);
            } catch (Exception e) {
                exceptionThrown = true;
            }

            System.assertEquals(1, [SELECT Id FROM Work_Order_Activity_Log__c].size());
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_JobOnHold() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Job_On_Hold_Action;
        Integer expectedSize = 1;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());
        Test.startTest();

        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveAction(startAtEq, activity, tradesmanContact);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;

                handler.handleSaveAction(jobOnHoldAction, activity, tradesmanContact);
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_JOBONHOLD LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_RejectActivity() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Reject_Activity_Action;
        Integer expectedSize = 0;

        String rejectActivity = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveAction(rejectActivity, activity, tradesmanContact);
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_JobComplete_NoReschedule() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Job_Complete_Action;
        Integer expectedSize = 1;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobComplete = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());
        Test.startTest();

        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);
            handler.handleSaveAction(startAtEq, activity, tradesmanContact);
            Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
            entry.TimeStamp__c = System.now().addHours(-5);
            update entry;

            handler.handleSaveAction(jobComplete, activity, tradesmanContact);

        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_JOBCOMPLETE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_JobComplete_Reschedule() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Job_Complete_Action;
        Integer expectedSize = 1;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobComplete = JSON.serialize(VTT_TestDataFactory.getRequestData(action, false, true));

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());
        Test.startTest();

        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);
            handler.handleSaveAction(startAtEq, activity, tradesmanContact);
            Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
            entry.TimeStamp__c = System.now().addHours(-5);
            update entry;

            handler.handleSaveAction(jobComplete, activity, tradesmanContact);

        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_JOBCOMPLETE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveAction_JobComplete_OtherTradesmanWorking() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Job_Complete_Action;
        Integer expectedSize = 1;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobComplete = JSON.serialize(VTT_TestDataFactory.getRequestData(action, true, true));

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());
        Test.startTest();

        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            activity = VTT_TestData.reloadWorkOrderActivity(activity.Id);
            handler.handleSaveAction(startAtEq, activity, tradesmanContact);
            Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
            entry.TimeStamp__c = System.now().addHours(-5);
            update entry;

            handler.handleSaveAction(jobComplete, activity, tradesmanContact);

        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveActionWithCheckList_withoutParam() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;

        Test.startTest();
        try {
            handler.handleSaveActionWithCheckList(null, null, null, null);
        } catch (Exception e) {
            exceptionThrown = true;
        }
        Test.stopTest();

        System.assertEquals(true, exceptionThrown);
    }

    @IsTest
    static void handleSaveActionWithCheckList_StartAtEquipment() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Start_At_Equipment_Action;
        Integer expectedSize = 1;

        String startAtEquipmentAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveActionWithCheckList(startAtEquipmentAction, activity, tradesmanContact, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesmanContact.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveActionWithCheckList_StartJobAtEquipment() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Start_Job_At_Equipment_Action;
        Integer expectedSize = 1;

        String startJobActionAtEquipmentAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result_startJob;
        List<Work_Order_Activity_Log_Entry__c> result_startJobAtEq;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveActionWithCheckList(startJobActionAtEquipmentAction, activity, tradesmanContact, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesmanContact.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result_startJob = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTJOB LIMIT 1];
        result_startJobAtEq = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result_startJob.size());
        System.assertEquals(expectedSize, result_startJobAtEq.size());
    }

    @IsTest
    static void handleSaveActionWithCheckList_FinishedAtEquipment() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Finished_Job_At_Equipment_Action;
        Integer expectedSize = 1;

        String finishAtEquipmentAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.startTest();
        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveActionWithCheckList(finishAtEquipmentAction, activity, tradesmanContact, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesmanContact.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_FINISHEDATSITE LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }

    @IsTest
    static void handleSaveActionWithCheckList_JobOnHold() {
        VTT_LTNG_WorkFlowEngineHandler handler = new VTT_LTNG_WorkFlowEngineHandler();
        Boolean exceptionThrown = false;
        String action = System.Label.VTT_LTNG_Job_On_Hold_Action;
        Integer expectedSize = 1;

        String startAtEq = VTT_TestDataFactory.getRequestData(System.Label.VTT_LTNG_Start_At_Equipment_Action);
        String jobOnHoldAction = VTT_TestDataFactory.getRequestData(action);

        List<Work_Order_Activity_Log_Entry__c> result;

        User tradesman = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;

        Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());
        Test.startTest();

        System.runAs(tradesman) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, tradesman.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            try {
                handler.handleSaveAction(startAtEq, activity, tradesmanContact);
                Work_Order_Activity_Log_Entry__c entry = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_STARTATSITE LIMIT 1];
                entry.TimeStamp__c = System.now().addHours(-5);
                update entry;

                handler.handleSaveActionWithCheckList(jobOnHoldAction, activity, tradesmanContact, new HOG_Work_Execution_Close_Out_Checklist__c(
                        Permit_Holder_Name__c = tradesmanContact.Id,
                        Maintenance_Work_Order__c = workOrder.Id,
                        Company_Name_Lookup__c = vendor.Id,
                        Permit_Number__c = '314159265359'
                ));
            } catch (Exception e) {
                exceptionThrown = true;
            }
        }
        Test.stopTest();

        result = [SELECT Id FROM Work_Order_Activity_Log_Entry__c WHERE Status__c = :VTT_Utilities.LOGENTRY_JOBONHOLD LIMIT 1];

        System.assertEquals(false, exceptionThrown);
        System.assertEquals(expectedSize, result.size());
    }
}