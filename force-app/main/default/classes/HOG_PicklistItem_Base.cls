/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Base Wrapper class for rendering picklist values for Lightning experience.
Test Class:     HOG_PicklistItem_SimpleTest
History:        jschn 12/12/2019 - Created.
*************************************************************************************************/
public inherited sharing abstract class HOG_PicklistItem_Base {

    @AuraEnabled
    public String value;

    @AuraEnabled
    public String label;

    public HOG_PicklistItem_Base(String value, String label) {
        this.value = value;
        this.label = label;
    }

}