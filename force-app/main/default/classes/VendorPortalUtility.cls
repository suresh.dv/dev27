public without sharing class VendorPortalUtility {
    public final static String VENDOR_PORTAL_PROFILE = 'HOG Vendor Community';
    public final static String VENDOR_PORTAL_PROFILE_LIGHTNING = 'HOG Vendor Community Lightning';
    public final static String VENDOR_PORTAL_TRADESMAN = 'HOG_Vendor_Community_VTT_User';
    public final static String VENDOR_PORTAL_SUPERVISOR = 'HOG_Vendor_Community_VTT_Vendor_Supervisor';
    public final static String VENDOR_PORTAL_TRADESMAN_LIGHTNING = 'HOG_Vendor_Community_VTT_User_Lightning';
    public final static String VENDOR_PORTAL_SUPERVISOR_LIGHTNING = 'HOG_Vendor_Community_VTT_Vendor_Supervisor_Lightning';
    public final static String GROUP_NAME = 'Vendor_Portal_Users';

    // OLD and NEW permission sets for lightning migration
    public static Map<String, String> PERMISSION_SET_MIGRATION_MAP_TO_LIGHTNING = new Map<String, String>{
            VENDOR_PORTAL_TRADESMAN => VENDOR_PORTAL_TRADESMAN_LIGHTNING,
            VENDOR_PORTAL_SUPERVISOR => VENDOR_PORTAL_SUPERVISOR_LIGHTNING
    };

    // New and Old permission sets for lightning migration
    public static Map<String, String> PERMISSION_SET_MIGRATION_MAP_FROM_LIGHTNING = new Map<String, String>{
            VENDOR_PORTAL_TRADESMAN_LIGHTNING => VENDOR_PORTAL_TRADESMAN,
            VENDOR_PORTAL_SUPERVISOR_LIGHTNING => VENDOR_PORTAL_SUPERVISOR
    };

    public static Boolean portalExecuteTriggerCode = true;

    //This method is used in the apex trigger on User that
    //Adds portal users to Public Group that is used for 
    //Sharing HOG Notification Type records to portal users.
    //11.12.2019 Added new Profile to logic
    public static void AddToGroups(List<User> portalUsers) {
        Set<Id> userIds = new Set<Id>();
        for (User u : portalUsers) {
            System.debug('Adding user to group HERE ' + u.IsPortalEnabled + ' ' + u.ContactId);
            if (u.IsPortalEnabled && u.ContactId != null) {
                userIds.add(u.Id);
            }
        }

        if (userIds != null && userIds.size() > 0) {
            //Profile that Vendor portal users use
            List<Profile> p = [
                    SELECT
                            Id,
                            Name
                    FROM Profile
                    WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE
                    OR Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE_LIGHTNING
            ];
            Set<Id> profileIds = HOG_GeneralUtilities.getSetOfIds(p);

            List<User> users = [
                    SELECT Id, Name, IsPortalEnabled, ProfileId, ContactId, Vendor_Portal_User_Type__c
                    FROM User
                    WHERE Id IN :userIds
                    AND IsPortalEnabled = TRUE
                    AND ProfileId IN :profileIds
                    AND ContactId <> NULL
                    AND Vendor_Portal_User_Type__c <> NULL
            ];

            System.debug('Users that will be added to group: ' + users);

            List<GroupMember>listGroupMember = new List<GroupMember>();

            if (users != null && users.size() > 0) {
                //Group used for sharing containing Vendor portal users
                Group g = [SELECT Id FROM Group WHERE DeveloperName = :VendorPortalUtility.GROUP_NAME];

                // loop the users that have been created
                for (User user : users) {
                    GroupMember gm = new GroupMember();
                    gm.GroupId = g.Id;
                    gm.UserOrGroupId = user.Id;
                    listGroupMember.add(gm);
                }
                System.debug('listGroupMember: ' + listGroupMember);
                insert listGroupMember;
                System.debug('Adding Permission Sets');
                AddPermissionSets(userIds, profileIds);
            }
        }

    }

    //This method update contacts records of vendor portal users - it will update User__c field
    //on contact to reference portal user that is assigned to this contact (needed for VTT)
    @Future
    public static void UpdatePortalContacts(Set<Id> userIds, Set<Id> vendorProfiles) {
        List<User> users = [
                SELECT Id, Name,
                        IsPortalEnabled, ProfileId,
                        ContactId, Vendor_Portal_User_Type__c, IsActive
                FROM User
                WHERE Id IN :userIds
                AND IsPortalEnabled = TRUE
                AND ProfileId IN :vendorProfiles
                AND ContactId <> NULL
                AND Vendor_Portal_User_Type__c <> NULL
        ];

        List<Contact>contactsToUpdate = new List<Contact>();

        if (users != null && users.size() > 0) {
            for (User user : users) {
                //update relevant contact associated with user,
                //so that Contact records User__c field correctly references portal user
                Contact c = new Contact();
                c.Id = user.ContactId;
                c.User__c = user.Id;

                contactsToUpdate.add(c);
            }
            System.debug('contactsToUpdate: ' + contactsToUpdate);
            update contactsToUpdate;
        }

    }

    //Check if user that has been updated and disabled is vendor portal user, if so then also update contacts
    //Which will fire trigger on contact to recount number of vendor portal account users
    public static void IsContactVendorPortalUser(Map<Id, User> newMap, Set<Id> profileIds) {
        Set<Id> disabledUsersIds = new Set<Id>();

        //Check if value on Vendor_Portal_User_Type__c has changed
        for (User u : newMap.values()) {
            if (profileIds.contains(u.ProfileId)
                    && u.ContactId != null
                    && u.Vendor_Portal_User_Type__c != null
                    && u.IsPortalEnabled) {

                disabledUsersIds.add(u.Id);
            }
        }

        //Call future method to update contacts
        if (disabledUsersIds.size() > 0) {
            UpdatePortalContacts(disabledUsersIds, profileIds);
        }
    }

    //This method adds permission set to Vendor portal users based on Vendor_Portal_User_Type__c value 
    //which is name of permission set and is  set on vendor portal management 
    //page upon clicking tradesmen or supervisor button and creating new user
    public static void AddPermissionSets(Set<Id> userIds, Set<Id> vendorProfiles) {

        List<User> users = [
                SELECT Id, Name, IsPortalEnabled, ProfileId, ContactId, Vendor_Portal_User_Type__c
                FROM User
                WHERE Id IN :userIds
                AND IsPortalEnabled = TRUE
                AND ProfileId IN :vendorProfiles
                AND ContactId <> NULL
                AND Vendor_Portal_User_Type__c <> NULL
        ];

        Set<String> setPermissionSetNames = new Set<String>();
        if (!users.isEmpty() && users.size() > 0) {
            for (User user : users) {
                setPermissionSetNames.add(user.Vendor_Portal_User_Type__c);
            }

            Map<String, Id> permissionSetMap = new Map<String, Id>();
            for (PermissionSet permissionSet : [SELECT Id, Name FROM PermissionSet WHERE Name IN :setPermissionSetNames]) {
                permissionSetMap.put(permissionSet.Name, permissionSet.Id);
            }

            List<PermissionSetAssignment> permissionSetAssignments = new List<PermissionSetAssignment>();
            if (users != null) {
                for (User user : users) {
                    PermissionSetAssignment psa = new PermissionSetAssignment();
                    psa.PermissionSetId = permissionSetMap.get(user.Vendor_Portal_User_Type__c);
                    psa.AssigneeId = user.Id;
                    //update relevant permission sets associated with user,
                    permissionSetAssignments.add(psa);
                }
            }

            System.debug('Updating Users: ' + userIds + ' with permission set: ' + permissionSetAssignments);
            insert permissionSetAssignments;

            //update contact record for these user
            System.debug('Updating contacts for portal users...');
            UpdatePortalContacts(userIds, vendorProfiles);
        }

    }

    //This method is used in update trigger on user object - it will PSA for users
    //This is all build with 1 PS per VendorPortal User in mind!!!!
    public static void UpdatePortalUserPermissionSets(Map<Id, User> newMap, Map<Id, User> oldMap) {
        Set<Id> userPSAtoUpdate = new Set<Id>();
        List<Profile> vendorPortalProfile = [
                SELECT
                        Id,
                        Name
                FROM Profile
                WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE OR Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE_LIGHTNING
        ];
        Set<Id> profileIds = HOG_GeneralUtilities.getSetOfIds(vendorPortalProfile);

        System.debug('oldMap.values() ' + oldMap.values());
        //Check if value on Vendor_Portal_User_Type__c has changed
        // Only for HOG Vendor portal users
        for (User u : oldMap.values()) {
            if (newMap.get(u.Id).Vendor_Portal_User_Type__c != oldMap.get(u.Id).Vendor_Portal_User_Type__c
                    && profileIds.contains(u.ProfileId)
                    && u.ContactId != null
                    && u.Vendor_Portal_User_Type__c != null
                    && u.IsPortalEnabled) {
                userPSAtoUpdate.add(u.Id);
            }
        }
        System.debug('userPSAtoUpdate ' + userPSAtoUpdate);
        if (userPSAtoUpdate.size() > 0) {
            System.debug('Vendor portal user type has been changed for these users..: ' + userPSAtoUpdate);

            Set<String> permissionSetNames = new Set<String>();

            for (User us : newMap.values()) {
                permissionSetNames.add(us.Vendor_Portal_User_Type__c);
            }

            //Map of permission name and its id so we get Id based on Vendor_Portal_User_Type__c value
            Map<String, Id> permissionSetMap = new Map<String, Id>();
            for (PermissionSet permissionSet : [SELECT Id, Name FROM PermissionSet WHERE Name IN :permissionSetNames]) {
                permissionSetMap.put(permissionSet.Name, permissionSet.Id);
            }

            //Owned by profile check need to be in place because
            //there is some sort of system permission set attached to users that cannot be deleted
            List<PermissionSetAssignment> permissionSetAssignments = [
                    SELECT Id, AssigneeId, Assignee.Vendor_Portal_User_Type__c
                    FROM PermissionSetAssignment
                    WHERE AssigneeId IN :userPSAtoUpdate
                    AND PermissionSetId
                            IN (SELECT Id FROM PermissionSet WHERE IsOwnedByProfile = FALSE)
            ];
            System.debug('Deleting old PS: ' + permissionSetAssignments);
            delete permissionSetAssignments;

            List<PermissionSetAssignment> newPSA = new List<PermissionSetAssignment>();

            for (PermissionSetAssignment psa : permissionSetAssignments) {
                PermissionSetAssignment nPs = new PermissionSetAssignment();
                nPs.PermissionSetId = permissionSetMap.get(psa.Assignee.Vendor_Portal_User_Type__c);
                nPs.AssigneeId = psa.AssigneeId;
                newPSA.add(nPs);
            }

            System.debug('Inserting new permision sets for these users: ' + newPSA);
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.emailHeader.triggerUserEmail = true;
            insert newPSA;
            System.debug('Updating related contacts for these users' + newMap.keySet());
            //UpdatePortalContacts(newMap.keySet());
        }

        IsContactVendorPortalUser(newMap, profileIds);
    }

    //This method is used on contact trigger - it basicly counts number of vendor portal users that partner account has enabled
    public static void UpdateUserCountOnAccount(List<Contact> contacts) {
        //Account ids
        Set<Id> aId = new Set<Id>();
        RecordType recId = [SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact' LIMIT 1];
        for (Contact c : contacts) {
            if (c.RecordTypeId == recId.Id && c.User__c != null) {

                aId.add(c.AccountId);
            }
        }

        if (aId != null && aId.size() > 0) {
            //List of Portal enabled Accounts and related Contacts
            List<Account> acc = [
                    SELECT id, Used_Licenses__c, IsPartner, (SELECT id FROM Contacts WHERE is_Vendor_Portal_User__c = true)
                    FROM Account
                    WHERE Id IN :aId
                    AND IsPartner = true
            ];
            if (acc != null && acc.size() > 0) {
                for (Account a : acc) {
                    a.Used_Licenses__c = a.Contacts.size();
                }
                System.debug('Updating used licence count on account: ' + acc);
                update acc;
            }
        }


    }

    public static void createOrReactivatePortalUser(String contactId, String email, String firstName, String lastName, String profileId, String userType) {
        List<Profile> vendorPortalProfile = [
                SELECT
                        Id,
                        Name
                FROM Profile
                WHERE Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE OR Name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE_LIGHTNING
        ];
        Set<Id> profileIds = HOG_GeneralUtilities.getSetOfIds(vendorPortalProfile);

        // Check if portal user is already present for this contact (previously enabled)
        List<User> enabledUser = [
                SELECT Id, Username
                FROM User
                WHERE ContactId = :contactId
                AND IsActive = FALSE
                AND ProfileId IN :profileIds
                AND ContactId <> NULL
        ];
        String space = ' ';
        String noSpace = '';
        firstName = firstName.replace(space, noSpace);
        lastName = lastName.replace(space, noSpace);

        String nickName = firstName + '.' + lastName;
        nickName = nickName.length() < 37 ? nickName + randomRandomInteger(100) : nickName.substring(0, 37) + randomRandomInteger(100);

        if (!enabledUser.isEmpty() || enabledUser.size() < 0) {
            // We will have to reactivate this user
            User reactivatedUser = enabledUser.get(0);
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.emailHeader.triggerUserEmail = true;
            reactivatedUser.Email = email;
            reactivatedUser.FirstName = firstName;
            reactivatedUser.LastName = lastName;
            reactivatedUser.CommunityNickname = nickName;
            reactivatedUser.Alias = String.valueOf(firstName.substring(0, 1) + lastName.substring(0, 1));
            reactivatedUser.ProfileId = profileId;
            reactivatedUser.EmailEncodingKey = 'UTF-8';
            reactivatedUser.LanguageLocaleKey = 'en_US';
            reactivatedUser.LocaleSidKey = 'en_CA';
            reactivatedUser.TimeZoneSidKey = 'America/Denver';
            reactivatedUser.Vendor_Portal_User_Type__c = userType;
            reactivatedUser.IsActive = true;
            System.debug('Re-enabling this user: ' + reactivatedUser);
            try {
                update reactivatedUser;
                if (ApexPages.currentPage() != null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'User has been re-enabled. Please refresh this page after a while'));
                }
            } catch (Exception ex) {
                if (ApexPages.currentPage() != null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Error when reactivating user - ' + ex.getMessage()));
                } else {
                    throw ex;
                }
                System.debug('EXCEPTION: ' + ex);
            }

        } else {

            User u = new User();

            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.emailHeader.triggerUserEmail = true;
            u.ContactId = contactId;
            u.Username = firstName + '.' + lastName + '@huskyvttportal.com';
            u.CommunityNickname = nickName;
            u.Email = email;
            u.FirstName = firstName;
            u.LastName = lastName;
            u.Alias = String.valueOf(firstName.substring(0, 1) + lastName.substring(0, 1));
            u.ProfileId = profileId;
            u.EmailEncodingKey = 'UTF-8';
            u.LanguageLocaleKey = 'en_US';
            u.LocaleSidKey = 'en_CA';
            u.TimeZoneSidKey = 'America/Denver';
            u.Vendor_Portal_User_Type__c = userType;

            try {
                insert u;
                if (ApexPages.currentPage() != null) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'User has been created. Please refresh this page after a while'));
                }
            } catch (Exception ex) {
                if (ex.getMessage().contains('DUPLICATE_USERNAME')) {
                    if (ApexPages.currentPage() != null) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please change firstName or lastName of this contacts before enabling him as portal user'));
                    } else {
                        throw ex;
                    }
                    System.debug('Duplicate: ' + ex);
                } else {
                    if (ApexPages.currentPage() != null) {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Error when creating new user - ' + ex.getMessage()));
                    } else {
                        throw ex;
                    }
                    System.debug('EXCEPTION: ' + ex);
                }
            }
        }
    }

    public static Integer randomRandomInteger(Integer upperLimit){
        Integer rand = Math.round(Math.random()*upperLimit);
        return rand;
    }
}