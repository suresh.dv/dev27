/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector for Work_Order_Activity_Log_Entry__c SObject
Test Class:     VTT_WOA_LogEntryDAOImplTest
History:        jschn 25/10/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_LogEntryDAOImpl implements VTT_Utility_WOA_LogEntryDAO {

    /**
     * Retrieves List<Work_Order_Activity_Log_Entry__c> containing one Log Entry based on tradesman Id ordered by
     * Name Descending (newest first)
     *
     * @param tradesmanId
     *
     * @return List<Work_Order_Activity_Log_Entry__c>
     */
    public List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesman(Id tradesmanId) {
        return [
                SELECT Name, Status__c, Work_Order_Activity__c
                FROM Work_Order_Activity_Log_Entry__c
                WHERE Tradesman__c = :tradesmanId
                AND Work_Order_Activity__r.SAP_Deleted__c != TRUE
                ORDER BY Name DESC
                LIMIT 1
        ];
    }

}