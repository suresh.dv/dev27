/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for SNR_Endpoint, SNR_EndpointHandler, SNR_Service, SNR_FLOCModel
History:        mbrimus 2020-01-28 - Created.
*************************************************************************************************/
@IsTest
private class SNR_EndpointTest {
    static List<String> WORKORDERRECORDTYPE = new List<String>{
            'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'
    };

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_WellLocation() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Location__c.getSObjectType().getDescribe().getName();
        createTestData();

        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);

        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(wellLocation.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_Facility() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Facility__c.getSObjectType().getDescribe().getName();
        createTestData();

        Facility__c facility = HOG_TestDataFactory.createFacility(true);
        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(facility.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_System() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = System__c.getSObjectType().getDescribe().getName();
        createTestData();

        System__c systemRecord = HOG_TestDataFactory.createSystem(true);
        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(systemRecord.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_SubSystem() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Sub_System__c.getSObjectType().getDescribe().getName();
        createTestData();

        Sub_System__c subSystem = HOG_TestDataFactory.createSubSystem(true);
        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(subSystem.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_Equipment() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Equipment__c.getSObjectType().getDescribe().getName();
        createTestData();

        Facility__c facility = HOG_TestDataFactory.createFacility(true);
        Equipment__c equipment = HOG_TestDataFactory.createEquipment(facility, true);
        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(equipment.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_FEL() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Functional_Equipment_Level__c.getSObjectType().getDescribe().getName();
        createTestData();

        Functional_Equipment_Level__c floc = HOG_TestDataFactory.createFunctionalEquipmentLevel(true);
        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(floc.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_Yard() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Yard__c.getSObjectType().getDescribe().getName();
        createTestData();

        Yard__c floc = HOG_TestDataFactory.createYard(true);
        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(floc.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadSNRData_supportedRecordTypeWithData_WellEvent() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Well_Event__c.getSObjectType().getDescribe().getName();
        createTestData();

        Location__c wellLocation = HOG_TestDataFactory.createLocation(true);
        Well_Event__c floc = HOG_TestDataFactory.createWellEvent(wellLocation.Id, 'H1-01-004-10007-100001', null, true);
        Test.startTest();
        try {
            result = SNR_Endpoint.loadSNRData(floc.Id, flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void loadServiceSpecifics_supportedRecordTypeWithData_WellLocation() {
        SNR_Response result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        String flocType = Location__c.getSObjectType().getDescribe().getName();

        HOG_Service_Category__c serviceCategory =
                ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;

        HOG_Notification_Type__c notificationType =
                NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, false, false, true);
        insert notificationType;

        HOG_Service_Priority__c servicePriority =
                ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriority;

        HOG_Service_Required__c serviceRequired = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequired;

        HOG_User_Status__c userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;

        List<HOG_Notification_Type_Priority__c> notificationTypePriority = new List<HOG_Notification_Type_Priority__c>{
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, false),
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, true)
        };
        insert notificationTypePriority;

        List<HOG_Service_Code_MAT__c> serviceCodeMAT = new List<HOG_Service_Code_MAT__c>{
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]),
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT2', 'TS2', WORKORDERRECORDTYPE[1])
        };
        insert serviceCodeMAT;

        List<HOG_Work_Order_Type__c> workOrderType = new List<HOG_Work_Order_Type__c>{
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[0].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true),
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[1].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true)
        };
        insert workOrderType;

        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);

        Test.startTest();
        try {
            result = SNR_Endpoint.loadServiceSpecifics(
                    serviceCategory.Id,
                    notificationType.Id,
                    userStatus.Id,
                    serviceRequired.Id,
                    wellLocation.Id,
                    flocType);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();
        SNR_Utilities.LightningNotificationRecordType notificationRecordType = new SNR_Utilities.LightningNotificationRecordType();
        String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(flocType);

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName), result.FLOCData.functionalLocationObjectLabel);
    }

    @IsTest
    static void saveSNR_noData() {
        SNR_Response result;
        Boolean expectedResult = false;

        Test.startTest();
        result = SNR_Endpoint.save(null, null, null);
        Test.stopTest();
        System.assertEquals(expectedResult, result.success);
    }

    @IsTest
    static void saveSNR_supportedRecordTypeWithData_Positive() {
        SNR_Response result;
        Boolean expectedResult = true;
        String flocType = Location__c.getSObjectType().getDescribe().getName();

        HOG_Service_Category__c serviceCategory =
                ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;

        HOG_Notification_Type__c notificationType =
                NotificationTypeTestData.createNotificationType(serviceCategory.Id, true, false, false, true);
        notificationType.Generate_Numbers_From_SAP__c = true;
        insert notificationType;

        HOG_Service_Priority__c servicePriority =
                ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriority;

        HOG_Service_Required__c serviceRequired = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequired;

        HOG_User_Status__c userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;

        List<HOG_Notification_Type_Priority__c> notificationTypePriority = new List<HOG_Notification_Type_Priority__c>{
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, false),
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, true)
        };
        insert notificationTypePriority;

        List<HOG_Service_Code_MAT__c> serviceCodeMAT = new List<HOG_Service_Code_MAT__c>{
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]),
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT2', 'TS2', WORKORDERRECORDTYPE[1])
        };
        insert serviceCodeMAT;

        List<HOG_Work_Order_Type__c> workOrderType = new List<HOG_Work_Order_Type__c>{
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[0].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true),
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[1].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true)
        };
        insert workOrderType;

        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);

        Test.startTest();

        SNR_Response dataResponse = SNR_Endpoint.loadSNRData(wellLocation.Id, flocType);
        result = SNR_Endpoint.save(
                flocType,
                getServiceNotificationRequest(workOrderType.get(0).Id),
                JSON.serialize(dataResponse.FLOCData)
        );

        Test.stopTest();
        List<HOG_Service_Request_Notification_Form__c> snr = [SELECT Id FROM HOG_Service_Request_Notification_Form__c];
        System.assertEquals(1, snr.size());
        System.assertEquals(expectedResult, result.success);
    }

    @IsTest
    static void saveSNR_supportedRecordTypeWithData_Negative() {
        SNR_Response result;
        Boolean expectedFailFlag = false;
        String flocType = Location__c.getSObjectType().getDescribe().getName();

        createTestData();

        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);

        Test.startTest();

        SNR_Response dataResponse = SNR_Endpoint.loadSNRData(wellLocation.Id, flocType);
        result = SNR_Endpoint.save(
                flocType,
                getServiceNotificationRequest(null),
                JSON.serialize(dataResponse.FLOCData)
        );

        Test.stopTest();
        List<HOG_Service_Request_Notification_Form__c> snr = [SELECT Id FROM HOG_Service_Request_Notification_Form__c];
        System.assertEquals(expectedFailFlag, result.success);
        System.assertEquals(0, snr.size());
    }

    @IsTest
    static void requestModelCoverage() {
        Test.startTest();
        SNR_Request request = new SNR_Request();
        request.workDetails = 'test';
        request.title = 'test';
        request.malfunctionStartDate = null;
        request.serviceTypeId = 'test';
        request.serviceCategoryId = 'test';
        request.serviceActivityId = 'test';
        request.servicePriorityId = 'test';
        request.serviceRequiredId = 'test';
        request.serviceSpecificsId = 'test';
        request.vendorCompanyId = 'test';
        request.equipmentDown = true;
        request.productionImpacted = true;
        Test.stopTest();
    }

    private static void createTestData() {
        HOG_Service_Category__c serviceCategory =
                ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;

        HOG_Notification_Type__c notificationType =
                NotificationTypeTestData.createNotificationType(serviceCategory.Id, true, false, false, true);
        notificationType.Generate_Numbers_From_SAP__c = true;
        insert notificationType;

        HOG_Service_Priority__c servicePriority =
                ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriority;

        HOG_Service_Required__c serviceRequired = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequired;

        HOG_User_Status__c userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;

        List<HOG_Notification_Type_Priority__c> notificationTypePriority = new List<HOG_Notification_Type_Priority__c>{
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, false),
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, true)
        };
        insert notificationTypePriority;

        List<HOG_Service_Code_MAT__c> serviceCodeMAT = new List<HOG_Service_Code_MAT__c>{
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]),
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT2', 'TS2', WORKORDERRECORDTYPE[1])
        };
        insert serviceCodeMAT;

        List<HOG_Work_Order_Type__c> workOrderType = new List<HOG_Work_Order_Type__c>{
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[0].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true),
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[1].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true)
        };
        insert workOrderType;
    }

    private static String getServiceNotificationRequest(
            String serviceSpecificId
    ) {
        SNR_Request request = new SNR_Request();
        request.equipmentDown = true;
        request.productionImpacted = true;
        request.serviceSpecificsId = serviceSpecificId;
        return JSON.serialize(request);
    }
}