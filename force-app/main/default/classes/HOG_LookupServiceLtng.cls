/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Test Class: 	HOG_LookupServiceLtngTest
Description:    HOG Service class for lightning containing endpoints for Custom Lookup component.
History:        jschn 07.10.2018 - Created.
				jschn 08.20.2019 - added fetchData (for HOG_Lookup LWC)
**************************************************************************************************/
public with sharing class HOG_LookupServiceLtng {

	private static final String CLASS_NAME = 'HOG_LookupServiceLtng';

	/**
	 * Retrieves list records based on provided object API Name and Name String of the record
	 * which you are looking for.
	 * @param  objectName     For which you want to search record
	 * @param  stringToSearch Name of the record (even partial) for which you are searching
	 * @return                List of records with name starting with provided keyword.
	 */
	@AuraEnabled
	public static List<SObject> prefetchLookupData(String objectName, String stringToSearch, Boolean withSharing) {
		String queryString = HOG_GeneralUtilities.BASIC_QUERY_STRING.replace(HOG_GeneralUtilities.SOBJECT_STRING
                                                                             , objectName);
		queryString += ' WHERE Name like \'%'
            		+ String.escapeSingleQuotes(stringToSearch)
            		+ '%\' LIMIT 5';

		if(withSharing){
			return new HOG_LookupServiceDAO().getData(queryString);
		} else {
			return new HOG_LookupServiceWithoutSharing().getData(queryString);
		}
	}

	/**
	 * Retrieves list records based on provided object API Name and Name String of the record
	 * which you are looking for.
	 *
	 * @param objectName
	 * @param fieldName
	 * @param stringToSearch
	 * @param additionalFields
	 * @param withSharing
	 *
	 * @return List of records with name starting with provided keyword.
	 */
	@AuraEnabled
	public static List<SObject> fetchData(String objectName, String fieldName, String stringToSearch, String additionalFields, Boolean withSharing) {
		if(withSharing == null) {
			withSharing = true;
		}
		String queryString = 'SELECT Id, ' + fieldName + (String.isNotBlank(additionalFields) ? ', ' + additionalFields : '')
				+ ' FROM ' + objectName
				+ ' WHERE ' + fieldName + ' like \'%'
				+ String.escapeSingleQuotes(stringToSearch)
				+ '%\' LIMIT 5';

		if(withSharing){
			return new HOG_LookupServiceDAO().getData(queryString);
		} else {
			return new HOG_LookupServiceWithoutSharing().getData(queryString);
		}
	}

}