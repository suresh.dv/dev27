/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller for PDF version of the form.
Test Class:     EPD_PDFPageControllerTest
History:        jschn 2019-06-14 - Created. - EPD R1
*************************************************************************************************/
public without sharing class EPD_PDFPageController {

    private static final String CLASS_NAME = String.valueOf(EPD_PDFPageController.class);

    public EPD_FormStructure formStructure {get;set;}

    public EPD_PDFPageController(ApexPages.StandardController std) {
        System.debug(CLASS_NAME + ' -> Constructor start.');

        Id recordId = std.getId();

        System.debug(CLASS_NAME + ' -> Constructor. Record ID: ' + recordId);
        List<EPD_Engine_Performance_Data__c> epdRecords = EPD_DAOProvider.EPDDAO.getEPDRecords(
                new Set<Id> {recordId}
        );

        this.formStructure = new EPD_FormService().handleLoadForExistingEPD(
                epdRecords.get(0),
                EPD_LoadMode.DIRECT_LOAD
        ).formStructure;

        System.debug(CLASS_NAME + ' -> Constructor Finished. EPD Form Structure loaded: ' + JSON.serialize(formStructure));
    }

}