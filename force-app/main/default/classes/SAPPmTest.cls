/**
 *  Author:
 *  Company:        Husky Energy
 *  Description:    Test for the SAPPm class that communicate with the SAP
 *  History:        Created on 8/24/2018   
 */

@isTest public class SAPPmTest {

	public static final String CLASS_NAME = 'SAPPmTest';


	@isTest static void EMPLOYEE_ASSIGNMENT_elementTest() {

		SAPPm.EMPLOYEE_ASSIGNMENT_element newObject = new SAPPm.EMPLOYEE_ASSIGNMENT_element();

		Test.startTest();
		Boolean result = newObject.USRID == null;
		Test.stopTest();

		System.assert(result);
	}

	@isTest static void DT_ECC_ALM_ORDER_HEADERTest() {

		SAPPm.DT_ECC_ALM_ORDER_HEADER newObject = new SAPPm.DT_ECC_ALM_ORDER_HEADER();

		Test.startTest();
		Boolean result = newObject.AUFNR == null;
		Test.stopTest();

		System.assert(result);
	}

	@isTest static void DT_ECC_ALM_ORDER_OPERATIONTest() {

		SAPPm.DT_ECC_ALM_ORDER_OPERATION newObject = new SAPPm.DT_ECC_ALM_ORDER_OPERATION();

		Test.startTest();
		Boolean result = newObject.VORNR == null;
		Test.stopTest();

		System.assert(result);
	}

	@isTest static void DT_ECC_ALM_ORDERTest() {

		SAPPm.DT_ECC_ALM_ORDER newObject = new SAPPm.DT_ECC_ALM_ORDER();

		Test.startTest();
		Boolean result = newObject.Order_Header == null;
		Test.stopTest();

		System.assert(result);
	}


	@testSetup static void prepareData(){

	}



}