@isTest
private class VTT_ConfirmationsUtilitiesTest {
	
	@isTest static void testTriggerAfterInsert() {
		User runningUser = VTT_TestData.createVTTUser();

        System.runAs(runningUser) {
                //Set Mock
                Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

                MaintenanceServicingUtilities.executeTriggerCode = false;  
                VTT_TestData.SetupRelatedTestData(false);

                System.AssertNotEquals(runningUser.Id, Null);

                Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
                Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);           

                HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
                HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
                workOrder1.User_Status_Code__c = '5X';
                workOrder1.Order_Type__c = 'WP01';
                workOrder1.Plant_Section__c  = '200';       
                workOrder1.Work_Order_Priority_Number__c  = '1';    
                workOrder1.Equipment__c = VTT_TestData.equipment.Id;
                workOrder1.Location__c = VTT_TestData.location.Id;
                workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;  
                update workOrder1;

                MaintenanceServicingUtilities.executeTriggerCode = true; 

                List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 1);
                Work_Order_Activity__c woActivity = activityList1[0];
                woActivity.Operation_Number__c = '0010';
                update woActivity;

                Test.startTest();
    		        //Create Logs and Log Entries
                    List<Work_Order_Activity_Log_Entry__c> logEntryList = new List<Work_Order_Activity_Log_Entry__c>();
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    	woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null, 
                    	null, null, 'Start Job'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    	woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
                    	null, null, 'Start at Equipment'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    	woActivity, VTT_Utilities.LOGENTRY_JOBONHOLD, null,
                    	null, null, 'On Hold'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    	woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null,
                    	null, null, 'Start Job Again'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    	woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
                    	null, null, 'Start Job at Equipment'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    	woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
                    	null, null, 'Finish Job at Equipment'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                            woActivity, VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY, null,
                            null, null, 'Finish for the day'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                            woActivity, VTT_Utilities.LOGENTRY_STARTJOB, null, 
                            null, null, 'Start Job'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                            woActivity, VTT_Utilities.LOGENTRY_STARTATSITE, null,
                            null, null, 'Start at Equipment'));
                    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                            woActivity, VTT_Utilities.LOGENTRY_FINISHEDATSITE, null,
                            null, null, 'Finish Job at Equipment'));
            	    logEntryList.add(VTT_Utilities.CreateLogEntry(tradesman1, 
                    	woActivity, VTT_Utilities.LOGENTRY_JOBCOMPLETE, null,
                    	null, null, 'Job Complete'));
            		insert logEntryList;
                Test.stopTest();
        }

		//Test Confirmations
        HOG_Maintenance_Servicing_Form__c workOrder1 = [Select Id, Name, Work_Order_Number__c From HOG_Maintenance_Servicing_Form__c];
        Work_Order_Activity__c woActivity = [Select Id, Name, Operation_Number__c From Work_Order_Activity__c];
        Contact tradesman1 = [Select Id, Name From Contact];
		List<Work_Order_Activity_Confirmation__c> confirmationList = [Select Id, Confirmation_Text__c, Final_Confirmation__c, Start_Log_Entry__r.Status__c,
                                                                             End_Log_Entry__r.Status__c, Status__c, Tradesman__c, Work_Order_Activity__c, 
                                                                             Confirmation_Number__c
                                                                      From Work_Order_Activity_Confirmation__c];
        System.assertEquals(confirmationList.size(), 3);

		Boolean valuesFound = false;
		for(Work_Order_Activity_Confirmation__c confirmation : confirmationList) {
			valuesFound = !confirmation.Final_Confirmation__c
				&& confirmation.Start_Log_Entry__r.Status__c == VTT_Utilities.LOGENTRY_STARTATSITE
				&& confirmation.End_Log_Entry__r.Status__c == VTT_Utilities.LOGENTRY_JOBONHOLD
				&& confirmation.Tradesman__c == tradesman1.Name;
			if(valuesFound) break;
		}
		System.assert(valuesFound, 'Not found confirmation with proper values.');

		valuesFound = false;
		for(Work_Order_Activity_Confirmation__c confirmation : confirmationList) {
			valuesFound = confirmation.Start_Log_Entry__r.Status__c == VTT_Utilities.LOGENTRY_STARTATSITE
				&& confirmation.End_Log_Entry__r.Status__c == VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY
				&& confirmation.Tradesman__c == tradesman1.Name;
			if(valuesFound) break;
		}
		System.assert(valuesFound, 'Not found confirmation with proper values.');

		valuesFound = false;
		for(Work_Order_Activity_Confirmation__c confirmation : confirmationList) {
			valuesFound = confirmation.Final_Confirmation__c
				&& confirmation.Start_Log_Entry__r.Status__c == VTT_Utilities.LOGENTRY_STARTATSITE
				&& confirmation.End_Log_Entry__r.Status__c == VTT_Utilities.LOGENTRY_JOBCOMPLETE
				&& confirmation.Tradesman__c == tradesman1.Name;
			if(valuesFound) break;
		}
		System.assert(valuesFound, 'Not found confirmation with proper values.');
	}

        @isTest static void testSAPCreateConfirmationsCallout() {
                //Set Mock
                Test.setMock(WebServiceMock.class, new HOG_SAPConfirmationsServiceMockImpl());

                //Generate test data
                List<HOG_SAPConfirmationsService.DT_SFDC_Confirmation> apiConfirmationList = new List<HOG_SAPConfirmationsService.DT_SFDC_Confirmation>();
                HOG_SAPConfirmationsService.DT_SFDC_Confirmation apiConfirmation = new HOG_SAPConfirmationsService.DT_SFDC_Confirmation();
                apiConfirmation.Order_Number = '123456789';
                apiConfirmation.Operation_Number = '0010';
                apiConfirmationList.add(apiConfirmation);

                //Call Service
                HOG_SAPConfirmationsService.HTTPS_Port stub = new HOG_SAPConfirmationsService.HTTPS_Port();
                HOG_SAPConfirmationsService.Confirmation_Resp_element[] confirmationResonseList = stub.SI_SFDC_Confirmation_Sync_OB(apiConfirmationList);
                System.assertEquals(confirmationResonseList.size(), 1);
                System.assertEquals(confirmationResonseList[0].Conf_No, apiConfirmation.Order_Number + '-' + apiConfirmation.Operation_Number + 1);
                System.assertEquals(confirmationResonseList[0].Status, 'E -' + ' ' + apiConfirmation.Order_Number + ' not found.');
        }

        @isTest static void testSAPCancelConfirmationCallout() {
                //Set Mock
                Test.setMock(WebServiceMock.class, new HOG_SAPCancelConfirmationServiceMockImpl());

                //Generate test data
                List<HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation> apiConfirmationList = new List<HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation>();
                HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation apiConfirmation = new HOG_SAPCancelConfirmationsService.DT_SFDC_Cancel_Confirmation();
                apiConfirmation.Order_Number = '123456789';
                apiConfirmation.Operation_Number = '0010';
                apiConfirmationList.add(apiConfirmation);

                //Call Service
                HOG_SAPCancelConfirmationsService.HTTPS_Port stub = new HOG_SAPCancelConfirmationsService.HTTPS_Port();
                HOG_SAPCancelConfirmationsService.Confirmation_Resp_element[] confirmationResonseList = stub.SI_SFDC_CancelConfirmation_Sync_OB(apiConfirmationList);
                System.assertEquals(confirmationResonseList.size(), 1);
                System.assertEquals(confirmationResonseList[0].Conf_No, apiConfirmation.Order_Number + '-' + apiConfirmation.Operation_Number + 0);
                System.assertEquals(confirmationResonseList[0].Status, 'E -' + ' ' + apiConfirmation.Order_Number + ' not found.');
        }
	
}