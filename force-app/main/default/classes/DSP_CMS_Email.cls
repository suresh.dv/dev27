/*
 * Called by DSP_CMS_FeedbackService. Sends a "feedback" message to Downstream Portal admins.
 */
global class DSP_CMS_Email
{
    @future
    public static void sendEmail(String toAddress, String feedbackMessage)
    {
        // Dont send email if test is running
        if(!Test.isRunningTest())
        {
            Messaging.reserveSingleEmailCapacity(1);
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        mail.setToAddresses(new String[] {toAddress});
        
        mail.setSenderDisplayName('Downstream Portal Feedback');
        
        mail.setSubject('Downstream Portal Feedback');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setPlainTextBody('Downstream Portal Feedback has been submitted:\n'+feedbackMessage);
        
        // Dont send email if test is running
        if(!Test.isRunningTest())
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}