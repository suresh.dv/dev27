@isTest
public class VrmSNowSyncBatchTest
{
    @isTest
    public static void unitTest()
    {
        Test.setMock(HttpCalloutMock.class, new VrmSNowIntegrationMock());
        
        Profile p = [Select id from Profile where Name = 'System Administrator' Limit 1];
        
        User u = new User(FirstName = 'Test', LastName= 'User', Alias = 'UserT', ProfileId = p.Id, Email = 'abc@abc.abc',
                            Username = 'abc@abc.abc'+System.currentTimeMillis(),CompanyName = 'TEST',Title = 'title',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US');
        insert u;
        
        User u1 = new User(FirstName = 'Test', LastName= 'User1', Alias = 'UserT1', ProfileId = p.Id, Email = 'abc1@abc1.abc1',
                            Username = 'abc1@abc1.abc1'+System.currentTimeMillis(),CompanyName = 'TEST',Title = 'title',TimeZoneSidKey = 'America/Los_Angeles',EmailEncodingKey = 'UTF-8',
                            LanguageLocaleKey = 'en_US',LocaleSidKey = 'en_US');
        insert u1;
        
        jbrvrm__Vendor_Subscription_Title__c vst = new jbrvrm__Vendor_Subscription_Title__c(Name = 'Test Vendor Subscription Title', jbrvrm__ServiceNow_SYS_ID__c = '0021295fdd2916446b6dbf97591750cc',
                                            jbrvrm__Contract_End_Date__c = System.today(), jbrvrm__Vendor_Analyst__c = u1.Id, jbrvrm__Status__c = 'Active');
        insert vst;
        
        SNow_Credentials__c sNow = new SNow_Credentials__c();
        sNow.Name = 'SNow_Credentials';
        sNow.Username__c = 'abc';
        sNow.Password__c = 'abc';
        sNow.API_URL__c = 'https://abc@abc.abc1';
        insert sNow;
        
        Test.startTest();
        
        SNowResultWrapper.Results re = new SNowResultWrapper.Results();
        re.sys_id = '1234';
        re.u_service_delivery_specialist = 'Test User (UserT)';
        re.name = 'Test';
        
        List<SNowResultWrapper.Results> reList = new List<SNowResultWrapper.Results>();
        reList.add(re);
        
        SNowResultWrapper srw = new SNowResultWrapper();
        srw.result = reList;
        
        
        //Database.executeBatch(new VrmSNowSyncBatch());
        
        VrmSNowSyncBatchScheduler scheduler = new VrmSNowSyncBatchScheduler();
        String exp = '0 0 23 * * ?  ';
        System.schedule('ServiceNow-Salesforce Sync',exp,scheduler);
        
        Test.stopTest();
    }
}