@isTest
public class TestUtilities {

    public static User createTestUser() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        
        return createTestUser(p);
    }
    public static User createOperationsUser()
    {
        PermissionSet ps = [select Id from PermissionSet where Name ='Operations_Log_Admin'];
        UserRole role = [select Id from UserRole where DeveloperName = 'Sunrise_Operations'];
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard Oilsands User'];
        User u = createTestUser(p);
        u.UserRoleId = role.Id;
        insert u;
        PermissionSetAssignment assignUser = new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = ps.Id);
        insert assignUser;
        return u;
    }
    public static User createTestUser(Profile p) {
        User u = new User(
            Alias = 'ut', 
            Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', 
            LastName='SF', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName='husky@test.com');

        return u;
    }
    
    public static Business_Unit__c createBusinessUnit(String name) {
        Business_Unit__c businessUnit = new Business_Unit__c(Name = name);
        
        return businessUnit;
    }
    
    public static Operating_District__c createOperatingDistrict(Id businessUnitId, Id businessDepartmentId, String name) {
        Operating_District__c operatingDistrict = new Operating_District__c(
            Business_Unit__c = businessUnitId,
            // rbo 12.03.13
            Business_Department__c = businessDepartmentId,
            // rbo 12.03.13
            Name = name
        );
        
        return operatingDistrict;
    }

    public static Account createTestAccount() {
        return new Account(name='test account');
    }
    
    public static Contact createTestContact(Id operatingDistrictId, Id accountId) {     
        Contact ct = new Contact(
            AccountId = accountId,
            /*Operating_District__c = operatingDistrictId,*/
            lastname='testing',
            firstname='apex'
        );
        
        return ct;
    }

    // OPS Log objects.
    public static Shift_Configuration__c createShiftConfiguration(Id operatingDistrictId, String name, Boolean isEnabled, 
                        String startTime, String endTime, Id rtId) {
        Shift_Configuration__c shift = new Shift_Configuration__c(
            Name = name,
            Enabled__c = isEnabled,
            Start_Time__c = startTime,
            End_Time__c = endTime,
            Operating_District__c = operatingDistrictId,
            Sunday__c = True,
            Monday__c = True,
            Tuesday__c = True,
            Wednesday__c = True,
            Thursday__c = True,
            Friday__c = True,
            Saturday__c = True,
            RecordTypeId = rtId
        );
        
        return shift;
    }
    
    // This method disables all shift configuration in the system. This is called by the test classes.
    public static void disableAllShiftConfiguration() {
        List<Shift_Configuration__c> shiftConfList = new List<Shift_Configuration__c>();
        for(Shift_Configuration__c config : [SELECT Id, Enabled__c FROM Shift_Configuration__c]) {
            config.Enabled__c = false;
            shiftConfList.add(config);
        }
        update shiftConfList;
    }
    
    public static Team_Allocation__c createTeamAllocation(Id operatingDistrictId, Date taDate, Id shiftId, String status, Id rtId) {
        Team_Allocation__c ta = new Team_Allocation__c(
            Date__c = taDate,
            Shift__c = shiftId,
            Status__c = status,
            Operating_District__c = operatingDistrictId,
            RecordTypeId = rtId
        );
        return ta;
    }
    
    public static Shift_Operator__c createShiftOperator(Id operatingDistrictId, Id operatorId, Id teamAllocationId, Id plantId, Id roleId) 
    { 
        Shift_Operator__c shiftOp = new Shift_Operator__c(
            Operating_District__c = operatingDistrictId,
            Operator_Contact__c = operatorId,
            Team_Allocation__c = teamAllocationId,
            Plant__c = plantId,
            Role__c = roleId
        );
        
        return shiftOp;
    }
    public static Shift_Operator__c createShiftOperator(Id teamId, Id operatorId, Id plantId, Id roleId) 
    { 
        Shift_Operator__c shiftOp = new Shift_Operator__c(
            
            Operator_Contact__c = operatorId,
            Team__c = teamId,
            Plant__c = plantId,
            Role__c = roleId
        );
        
        return shiftOp;
    }
    public static Unit__c createUnit(String name, Id plantId) {
        Unit__c unit = new Unit__c(Name = name);
        unit.Plant__c = plantId;
        
        return unit;
    }
    
    public static Process_Area__c createProcessArea(String name, Id rtId) {
        Process_Area__c area = new Process_Area__c(Name = name, RecordTypeId = rtId);
        
        return area;
    }
    
    public static Operational_Event__c createOperationalEvent(Id operatingDistrictId, Id operatorId, Id teamAllocationId, 
                                    Id unitId, Id areaId, Id plantId, Id rtId) {
        Operational_Event__c oEvent = new Operational_Event__c(
            Time__c = Datetime.now().format('HH:mm'),
            Operating_District__c = operatingDistrictId,
            Operator_Contact__c = operatorId,
            Team_Allocation__c = teamAllocationId,
            Unit__c = unitId,
            Area__c = areaId,
            Plant__c = plantId,
            RecordTypeId = rtId
        );
        
        return oEvent;  
    }
    
    public static Plant__c createPlant(String name) {
        Plant__c plant = new Plant__c(
            Name = name
        );
        
        return plant;
    }
    public static Team_Role__c createTeamRole(String name, boolean isLeader, integer maxOccur, Id rtId)
    {
        Team_Role__c role = new Team_Role__c(
           Name = name,
           Leader_Ind__c = isLeader,
           Max_Occurence__c = maxOccur,
           RecordTypeId = rtId);
        return role;   
    }
    public static Team__c createTeam(String name, Id operatingDistrictId, Id rtId)
    {
        Team__c team = new Team__c(
            Name = name,
            Operating_District__c = operatingDistrictId, RecordTypeId = rtId);
        return team;    
    }
    public static DOI__c createDOI(Id plantId, Id rtId)
    {
        DOI__c doi = new DOI__c();
        doi.Date__c = date.today();
        doi.Plant__c = plantId;
        doi.RecordTypeId = rtId;
        doi.Instructions__c = 'testing';
        doi.Completion_Date__c = date.today() + 7;
        return doi;
    }
}