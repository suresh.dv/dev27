/// controller classs for reports displayed in customer portals
public with sharing class LOM_Generic_Report_Controller {

/// Gets the corect base URL regardless if the page is called from platform 
/// or from a Force.com site

    public LOM_Generic_Report_Controller (){
    
        dateFrom = Date.Today()-1;
        dateTo = Date.Today()-1;
        ShowReport = true;
    }

public string ReportName {get;set;}
public string ReportDescription {get;set;}
Public String ReportID {get;set;}
Public Boolean ShowReport {get;set;}

public Date dateFrom {get;set;}
public Date dateTo {get;set;}  
    public String dateFromStr 
    {
        get
        {
            Date d = dateFrom;
            return d.format() ;
        }
        set
        {
            dateFrom = convertDate(value);    //Date.valueOf(value);
        }
    }
    public String dateToStr 
    {
        get
        {
            Date d = dateTo ;
            return d.format() ;
        }
        set
        {
            dateTo =  convertDate(value); //Date.valueOf(value);
        }
    }  


public string baseURL {
    get{
        return Site.getCurrentSiteUrl() == null ? URL.getSalesforceBaseUrl().toExternalForm() +'/' : Site.getCurrentSiteUrl();
    }
}

public string ReportURL {
    get{
        return baseURL  +  ReportID ;
    }
}


public string ReportURLwithParameters {get;set;}


    public PageReference RefreshData()
    {
        retrieveReportInfo();  
        RefreshReportUrl();     
        return null;
    }   
    
    public PageReference RefreshReportUrl()
    {
        /* Example */
        /*?pv0=06%2F13%2F2013&pv1=06%2F13%2F2013&isdtp=vw*/
        ReportURLwithParameters =  baseURL  +  ReportID + 
        '?pv0=' + dateFrom.month() + '%2F' +  dateFrom.day() + '%2F' +  dateFrom.year() +
        '&pv1=' + dateTo.month() + '%2F' +  dateTo.day() + '%2F' +  dateTo.year() +
        '&isdtp=vw';
        ShowReport = true;
        return null;
    }  




    public void retrieveReportInfo() {
        ReportID = System.currentPagereference().getParameters().get('ReportID');
        
        LIST<Report> AgR;

        AgR = [select Name, Description from Report where id = :ReportID]; 

        // Loop through the list and update the Name field
        for(Report s : AgR){
            ReportName = String.valueOf(s.get('Name'));
            ReportDescription = String.valueOf(s.get('Description'));
         }
        
    }

    //convert string dates in dd/mm/yyyy format into Date
    private Date convertDate(String inDate) {
           String[] dateParts = inDate.split('/');
           Integer day = Integer.valueOf(dateParts[0]);
           Integer month= Integer.valueOf(dateParts[1]);           
           Integer year = Integer.valueOf(dateParts[2]);                      
          

           Date dt = Date.newInstance(year , month, day);
              
           return dt ;    
  }   

}