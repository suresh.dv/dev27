/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of HOG_VG_AlertStrategyProvider for High Vent Alerts.
                This class contains logic for deciding whether High Vent Alert should be
                created.
Test Class:     HOG_VG_HighVentAlertStrategyProviderTest
History:        jschn 19/10/2018 - Created. - W-001276
*************************************************************************************************/
public with sharing class HOG_VG_HighVentAlertStrategyProvider extends HOG_VG_AlertStrategyProvider {


    public HOG_VG_HighVentAlertStrategyProvider(List<Location__c> locations,
            Map<Id, Location__c> oldLocationsMap,
            Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId,
            Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType) {
        super(locations, oldLocationsMap, ventGasExemptionsByLocationId, activeAlertsByLocationAndType);
    }

    /**
     * Implementation of decision logic for creating High Vent Alert.
     * Checks whether there is another active alert as well as if there is an active exemption on requested Location.
     * Last, it will check if there are all necessary conditions met.
     *
     * @param location
     *
     * @return
     */
    public override Boolean shouldCreate(Location__c location) {
        return !super.ventGasExemptionsByLocationId.containsKey(location.Id)
                && !super.activeAlertsByLocationAndType.containsKey(location.Id + HOG_VentGas_Utilities.ALERT_TYPE_HIGH_VENT)
                && highVentTriggered(location);
    }

    /**
     * Checks if there are all necessary conditions met for creating requested Alert.
     *
     * @param location
     *
     * @return
     */
    private Boolean highVentTriggered(Location__c location) {
        return location.High_Vent__c == 'YES'
                && super.isCorrectLocation(location)
                && correctDateToCreate();
    }

    /**
     * Compares today's day number with value from Vent Gas settings.
     *
     * @return
     */
    protected override Boolean correctDateToCreate() {
        Date today = Date.today();
        return today.day() == super.settings.Alerts_Create_Day_of_month__c;
    }

}