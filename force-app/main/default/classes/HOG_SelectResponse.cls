public abstract class HOG_SelectResponse extends HOG_RequestResult {
	
	@AuraEnabled public List<SObject> resultObjects {get;set;}

	abstract void addResult(List<SObject> results);

}