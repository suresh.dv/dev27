/**
 * Created by MartinBilicka on 25. 3. 2019.
 */
@isTest
public with sharing class WS_WellOptimizationControllerXTest {

    @testSetup
    static void testSetup(){
        WS_TestDataFactory.createWSUser();
    }

    /*test class that checks that it is not possible to save record on the Visualforce page
    as the Control Room mandatory field is not autocompleted*/
    @isTest
    static void testAutocompleteFieldsNoHistory(){
        //Prepare sObjects for testing
        Location__c wellLocation = HOG_TestDataFactory.createLocation('Test Well Location', true);

        Well_Optimization__c wellOptimization_1 = WS_TestDataFactory.createWellOptimization(wellLocation.Id, NULL, false);
        //System.debug('wellOptimization_1 ' + wellOptimization_1);

        PageReference pageRef = Page.WS_WellOptimizationCreate;
        Test.startTest();{
            Test.setCurrentPage(pageRef);

            User testUser = getTestUser();
            System.runAs(testUser){
                ApexPages.StandardController stdCtrl = new ApexPages.StandardController(wellOptimization_1);
                WS_WellOptimizationControllerX ctrlExt = new WS_WellOptimizationControllerX(stdCtrl);
                ctrlExt.autocompleteFields();
                stdCtrl.save();
                Well_Optimization__c wellOptFromCtrl = ctrlExt.wellOpt;
                //System.debug('wellOptFromCtrl ' + wellOptFromCtrl);
                System.assertEquals(NULL,wellOptFromCtrl.Control_Room_Centre__c, 'Control Room Center is populated but it should be empty (NULL)');
            }
        }
        Test.stopTest();
    }

    @isTest
    static void testAutocompleteFieldsWithHistory(){
        Location__c wellLocation = HOG_TestDataFactory.createLocation('Test Well Location',true);
        Field__c opField = HOG_TestDataFactory.createAMU('Test Control Room',true);
        Field__c opField2 = HOG_TestDataFactory.createAMU('Test New Control Room',true);

        Well_Optimization__c wellOptimization_1 = WS_TestDataFactory.createWellOptimization(wellLocation.Id, opField.Id, 5, 5.55, true);
        Test.setCreatedDate(wellOptimization_1.Id, Datetime.now().addDays(-1));
        Well_Optimization__c wellOptimization_2 = WS_TestDataFactory.createWellOptimization(wellLocation.Id, opField2.Id, 6, 6.66, true);
        Well_Optimization__c wellOptimization_new = WS_TestDataFactory.createWellOptimization(wellLocation.Id, NULL, false);

        /*System.debug('wellOptimization_1 = ' + wellOptimization_1);
        System.debug('wellOptimization_2 = ' + wellOptimization_2);
        System.debug('QUERY = ' + [SELECT CreatedDate, Control_Room_Centre__c, Pump_Rate_m_RPM_Day__c, Final_Speed__c
                                    FROM Well_Optimization__c
                                    WHERE Well_Location__c = :wellLocation.Id
                                    ORDER BY CreatedDate DESC
                                    ]);
        */
        PageReference pageRef = Page.WS_WellOptimizationCreate;
        Test.startTest();{
            Test.setCurrentPage(pageRef);

            User testUser = getTestUser();
            System.runAs(testUser){
                ApexPages.StandardController stdCtrl = new ApexPages.StandardController(wellOptimization_new);
                WS_WellOptimizationControllerX ctrlExt = new WS_WellOptimizationControllerX(stdCtrl);
                ctrlExt.autocompleteFields();
                stdCtrl.save();
                Well_Optimization__c wellOptFromCtrl = ctrlExt.wellOpt;
                //System.debug('wellOptFromCtrl = ' + wellOptFromCtrl);

                System.assertEquals(opField2.Id,wellOptFromCtrl.Control_Room_Centre__c, 'Control Room is not autocompleted from the last created Well Optimization with the same Well Location');
                System.assertEquals(6,wellOptFromCtrl.Initial_Speed__c,'Initial Speed is not autocompleted byt Final Speed field from the last created Well Optimization with the same Well Location');
                System.assertEquals(6.66,wellOptFromCtrl.Pump_Rate_m_RPM_Day__c,'Pump Rate is not autocompleted from the last created Well Optimization with the same Well Location');
            }
        }
        Test.stopTest();
    }

    @isTest
    static void testSaveAndNewButton(){

        Well_Optimization__c wellOptimization = WS_TestDataFactory.createWellOptimization(true);

        Test.startTest();
        {
            PageReference pageRef = Page.WS_WellOptimizationCreate;
            Test.setCurrentPage(pageRef);

            User testUser = getTestUser();
            System.runAs(testUser) {
                ApexPages.StandardController stdCtrl = new ApexPages.StandardController(wellOptimization);
                WS_WellOptimizationControllerX ctrlExt = new WS_WellOptimizationControllerX(stdCtrl);
                PageReference nextPage = ctrlExt.saveAndNew();
                System.assertEquals(Page.WS_WellOptimizationCreate.getUrl(), nextPage.getUrl(), 'Rediraction has not been done to WS_WellOptimizationCreate');
            }
        }
        Test.stopTest();
    }

    private static User getTestUser(){
        return [
                SELECT Id
                FROM User
                WHERE Alias = 'testuser'
                LIMIT 1
        ];
    }
}