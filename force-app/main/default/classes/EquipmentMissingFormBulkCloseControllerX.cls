/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentMissingFormBulkClose page to close Status of multiple Add Equipment Form records

Test Class:    EquipmentMissingFormTest

History:       26-Mar-15	Gangha Kaliyan	To include facilities in Equipment Inventory
               28-Feb-17    Miro Zelina - included Well Events in Equipment Inventory
               03-Jul-17    Miro Zelina - included System & Sub-System in Equipment Inventory
               27-Jul-17    Miro Zelina - added role check for SAP Support Lead
               04-Aug-17    Miro Zelina - added profile & PS check for process button
               16-Nov-18 	Maros Grajcar - Sprint - Equipment Transfer Release 1
							- added Rejected button to page
               				- added popup window on Rejected button
               				- added new field to the object
               				- added logic to get email from Planner Group
 				08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude - Email Service Enhancement R
				12.09.19 	Maros Grajcar - W-001635 - sorting list for Process list view button

------------------------------------------------------------*/

public with sharing class EquipmentMissingFormBulkCloseControllerX {

	public List<Equipment_Missing_Form__c> selectedList { get; private set; }
	private EquipmentFormService addEquipmentService;
	private String closePopupReason;
	public Boolean displayPopup { get; set; }
	public Integer CurrentIndex { get; private set; }
	public Equipment_Missing_Form__c CurrentForm { get; private set; }
	public EquipmentUtilities.UserPermissions userPermissions;
	private String retUrlID;

	public EquipmentMissingFormBulkCloseControllerX(ApexPages.StandardSetController controller) {
		userPermissions = new EquipmentUtilities.UserPermissions();
		addEquipmentService = new EquipmentFormService();

		if (!Test.isRunningTest()) {
			//jschn 08-Jul-19 - added Functional_Location__c
			controller.addFields(new List<String>{'Name', 'Status__c', 'Equipment_Status__c', 'Comments__c', 'P_ID__c', 'Description__c', 'Part_of_Package__c','Manufacturer__c', 'Model_Number__c', 'Manufacturer_Serial_No__c', 'Tag_Number__c','Location__c', 'Serial_plate_of_asset__c', 'Attachment__c','Location__r.Route__c', 'Location__r.Planner_Group__c', 'Location__r.Functional_Location__c','Well_Event__c', 'Well_Event__r.Route__c', 'Well_Event__r.Planner_Group__c', 'Well_Event__r.Functional_Location__c','System__c', 'System__r.Route__c', 'System__r.Planner_Group__c', 'System__r.Functional_Location__c','Sub_System__c', 'Sub_System__r.Route__c', 'Sub_System__r.Planner_Group__c', 'Sub_System__r.Functional_Location__c','Functional_Equipment_Level__c', 'Functional_Equipment_Level__r.Route__r.Route_Number__c', 'Functional_Equipment_Level__r.Planner_Group__c', 'Functional_Equipment_Level__r.Functional_Location__c','Yard__c', 'Yard__r.Route__r.Route_Number__c', 'Yard__r.Planner_Group__c', 'Yard__r.Functional_Location__c','CreatedById', 'CreatedDate', 'LastModifiedDate', 'LastModifiedById', 'Facility__c','Facility__r.Plant_Section__c', 'Facility__r.Planner_Group__c', 'Facility__r.Functional_Location__c', 'Reason__c','Expiration_Date__c', 'Regulatory_Equipment__c', 'Safety_Critical_Equipment__c', 'Send_Email_Notification__c','CreatedById', 'Location_Label__c'});
		}

		// This will set ID from first equipment missing CurrentForm for when users wants go back
		// they will be redirected to correct FLOC
		figureOutReturnURL(controller);


		if (!isUserAdmin) {

			selectedList = new List<Equipment_Missing_Form__c>();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
			return;
		}

		else if (controller.getSelected().size() == 0) {
			selectedList = new List<Equipment_Missing_Form__c>();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Add Equipment Form'));
			return;
		}


		selectedList = controller.getSelected();
		selectedList.sort();
		CurrentIndex = 1;
		CurrentForm = selectedList[CurrentIndex - 1];
	}

	public Attachment attachment {
		get {
			if (attachment == null) {
				List<Attachment> att = [
						SELECT Id, Name, Body, ContentType
						FROM Attachment
						WHERE ParentId = :CurrentForm.Id AND Id = :CurrentForm.Attachment__c
				];
				if (att.size() > 0)
					attachment = att[0];

			}
			return attachment;
		}
		set;
	}

	public ContentDocumentLink attachmentFile {
		get {
			if (attachmentFile == null) {
				List<ContentDocumentLink> file = [SELECT Id, ContentDocument.Title, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :CurrentForm.Id];
				if (file.size() > 0)
					attachmentFile = file[0];

			}
			return attachmentFile;
		}
		set;
	}

	public Attachment serialPlate {
		get {
			if (serialPlate == null) {
				List<Attachment> att = [
						SELECT Id, Name, Body, ContentType
						FROM Attachment
						WHERE ParentId = :CurrentForm.Id AND Id = :CurrentForm.Serial_plate_of_asset__c
				];
				if (att.size() > 0)
					serialPlate = att[0];

			}
			return serialPlate;
		}
		set;
	}

	public ContentDocumentLink serialPlateFile {
		get {
			if (serialPlateFile == null) {
				List<ContentDocumentLink> file = [
						SELECT Id, ContentDocument.Title, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :CurrentForm.Id];
				if (file.size() > 0)
					serialPlateFile = file[0];

			}
			return serialPlateFile;
		}
		set;
	}
	public Boolean getHasPreviousForm() {
		return (CurrentIndex > 1);
	}

	public Boolean getHasNextForm() {
		return (CurrentIndex < selectedList.size());
	}

	public PageReference NextForm() {
		if (getHasNextForm()) {
			CurrentIndex = CurrentIndex + 1 ;
			CurrentForm = selectedList[CurrentIndex - 1];
			attachment = null;
			serialPlate = null;
		}
		return null;
	}

	public PageReference PreviousForm() {
		if (getHasPreviousForm()) {
			CurrentIndex = CurrentIndex - 1 ;
			CurrentForm = selectedList[CurrentIndex - 1];
			attachment = null;
			serialPlate = null;
		}
		return null;
	}

	public PageReference CloseForm() {
		Equipment_Missing_Form__c obj = CurrentForm;

		if (obj.Status__c != EquipmentUtilities.REQUEST_STATUS_PROCESSED && obj.Status__c != EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED && obj.Status__c != EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED) {
			try {
				obj.Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED;
				update obj;
				EquipmentFormService.formProcessedTemplateEmail(CurrentForm.Id, new Set<String>{
						addEquipmentService.addEquipmentPlannerGroup(CurrentForm)
								+ EquipmentFormService.COMBINATION_DELIMITER
								+ addEquipmentService.getFunctionalLocationForDestination(CurrentForm)
				});

				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully PROCESSED and notification email has been sent.'));
				return null;

			} catch (Exception ex) {
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
				return null;
			}
		}

		return null;

	}

	public void figureOutReturnURL(ApexPages.StandardSetController controller) {
		if (controller.getSelected().size() > 0) {
			Equipment_Missing_Form__c eq = (Equipment_Missing_Form__c) controller.getSelected()[0];

			if (eq.Well_Event__c != null) {
				retUrlID = eq.Well_Event__c;
			} else if (eq.Yard__c != null) {
				retUrlID = eq.Yard__c;
			} else if (eq.Functional_Equipment_Level__c != null) {
				retUrlID = eq.Functional_Equipment_Level__c;
			} else if (eq.Sub_System__c != null) {
				retUrlID = eq.Sub_System__c;
			} else if (eq.System__c != null) {
				retUrlID = eq.System__c;
			} else if (eq.Location__c != null) {
				retUrlID = eq.Location__c;
			} else if (eq.Facility__c != null) {
				retUrlID = eq.Facility__c;
			} else {
				retUrlID = '';
			}
		} else {
			retUrlID = '';
		}
	}

	public Boolean isUserAdmin {
		get {
			return userPermissions.isSystemAdmin
					|| userPermissions.isSapSupportLead
					|| userPermissions.isHogAdmin;
		}
		private set;
	}

	/**
	* functions for popup window, to prevent displaying the reason on salesforce side on cancel button,
	* we are using this variable to keep the old reason
	*/
	public void showPopup() {
		displayPopup = true;
		closePopupReason = CurrentForm.Reason__c;
	}
	public void closePopup() {
		displayPopup = false;
	}

	public void closePopupOnPage() {
		displayPopup = false;
		CurrentForm.Reason__c = closePopupReason;
	}

	public void saveOnPopup() {
		System.Savepoint sp = Database.setSavepoint();
		String originalStatus = CurrentForm.Status__c;

		CurrentForm.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;
		try {
			update CurrentForm;
			//08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude
			EquipmentFormService.requestRejectionTemplateEmail(CurrentForm.Id, CurrentForm.CreatedById, new Set<String>{
					addEquipmentService.addEquipmentPlannerGroup(CurrentForm)
							+ EquipmentFormService.COMBINATION_DELIMITER
							+ addEquipmentService.getFunctionalLocationForDestination(CurrentForm)
			});

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully ' +
					'REJECTED and notification email ' +
					'has been sent.'));
		} catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			Database.rollback(sp);
			CurrentForm.Status__c = originalStatus;
		}
		closePopup();
	}

	public PageReference rejectAndNotify() {
		showPopup();
		return null;
	}

	public PageReference exit() {
		PageReference nextPage;
		if (ApexPages.currentPage().getParameters().get('retURL') != null) {
			System.debug('retURL =' + ApexPages.currentPage().getParameters().get('retURL'));
			nextPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
			nextPage.setRedirect(true);
		} else if (ApexPages.currentPage().getParameters().get('retURL').contains('%2F')) {
			String redirectExitButton = ApexPages.currentPage().getParameters().get('retURL').replace('%2F', '');
			nextPage = new PageReference('/' + redirectExitButton);
			nextPage.setRedirect(true);
		} else nextPage = new PageReference('/');
		return nextPage;
	}

}