/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Part__c
-------------------------------------------------------------------------------------------------*/
@isTest 
public class HOGPartTestData 
{
    public static HOG_Part__c createHOGPart
    (
    	String name,
    	String part_Code, 
    	String part_Description,
    	String catalogue_Code,
    	Boolean active
    )
    {                
        HOG_Part__c results = new HOG_Part__c
            (           
                Name = name,
                Part_Code__c = part_Code, 
                Part_Description__c = part_Description,
                Catalogue_Code__c = catalogue_Code,
                Active__c = active
            ); 

        return results;
    }
}