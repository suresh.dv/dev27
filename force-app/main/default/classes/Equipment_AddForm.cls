/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_AddForm {
	@AuraEnabled
	public Equipment_Missing_Form__c addForm;
	@AuraEnabled
	public List<ContentDocumentLink> addFormFiles;
	@AuraEnabled
	public List<Attachment> addFormAttachments;

	public Equipment_AddForm() {
		this.addForm = new Equipment_Missing_Form__c();
		this.addFormFiles = new List<ContentDocumentLink>();
		this.addFormAttachments = new List<Attachment>();
	}

}