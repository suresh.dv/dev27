/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of DAO interface serving as access point for retrieval of Permission
                related records.
Test Class:     HOG_PermissionDAOImplTest TODO
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
public inherited sharing class HOG_PermissionDAOImpl implements HOG_PermissionDAO {

    /**
     * Retrieves List of Permission Sets based on provided set of Names
     *
     * @param names
     *
     * @return List<PermissionSet>
     */
    public List<PermissionSet> getPermissionSetsByName(Set<String> names) {
        return [
                SELECT Id
                FROM PermissionSet
                WHERE Name IN :names
        ];
    }

    /**
     * Gets count of assignments based on user ID and set of Permission Set IDs
     *
     * @param userId
     * @param permissionIds
     *
     * @return Integer
     */
    public Integer getAssignmentCountForPermissions(Id userId, Set<Id> permissionIds) {
        return [
                SELECT COUNT()
                FROM PermissionSetAssignment
                WHERE AssigneeId = :userId
                AND PermissionSetId IN :permissionIds
        ];
    }

}