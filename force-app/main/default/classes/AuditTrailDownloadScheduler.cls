/*--------------------------------------------------------------------------------------
Author     : Yen Le
Company    : Thirdwave Consulting
Description: A scheduler to invoke AuditTrailDownloadManager apex class run every 10 minutes to download audit trail
Test Class : AuditTrailUnitTest

----------------------------------------------------------------------------------------*/ 
global class AuditTrailDownloadScheduler implements Schedulable {
	
	/* Copy the following script to schedule Audit Trail download jobs 
	//schedule download jobs run every 10 min
	
	System.schedule('Download Audit Trail Job 1', '0 0 * * * ?', new AuditTrailDownloadScheduler());
    System.schedule('Download Audit Trail Job 2', '0 10 * * * ?', new AuditTrailDownloadScheduler());
	System.schedule('Download Audit Trail Job 3', '0 20 * * * ?', new AuditTrailDownloadScheduler());
	System.schedule('Download Audit Trail Job 4', '0 30 * * * ?', new AuditTrailDownloadScheduler());
	System.schedule('Download Audit Trail Job 5', '0 40 * * * ?', new AuditTrailDownloadScheduler());
	System.schedule('Download Audit Trail Job 6', '0 50 * * * ?', new AuditTrailDownloadScheduler());

	
	*/
    global void execute(SchedulableContext sc) {
        AuditTrailDownloadManager auditTrail = new AuditTrailDownloadManager();
        ID batchprocessid = Database.executeBatch(auditTrail);   
    }
}