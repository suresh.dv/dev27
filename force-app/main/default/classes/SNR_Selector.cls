/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database queries for Service Request Notification
Test Class:     SNR_SelectorTest
History:        mbrimus 2020-01-08 - Created.
*************************************************************************************************/
public inherited sharing class SNR_Selector implements SNR_DAO {

    public List<AggregateResult> getServiceCategories() {
        return [
                SELECT
                        HOG_Notification_Type__r.HOG_Service_Category__c Id,
                        HOG_Notification_Type__r.HOG_Service_Category__r.Name Name,
                        Active__c
                FROM HOG_Work_Order_Type__c
                GROUP BY
                                HOG_Notification_Type__r.HOG_Service_Category__c,
                                HOG_Notification_Type__r.HOG_Service_Category__r.Name,
                                Active__c
                HAVING Active__c = TRUE
                ORDER BY HOG_Notification_Type__r.HOG_Service_Category__r.Name
                LIMIT 1000  // max limit of Collection type
        ];
    }

    public List<AggregateResult> getServiceTypesNoServiceRigProgram(String serviceCategoryId, Boolean isCommunityUser) {
        List<HOG_Notification_Type__c> notificationTypes = new List<HOG_Notification_Type__c>();
        List<Id> notificationTypeIds = new List<Id>();

        // Fix for W-001646 - Restricting access for community users to that they are not able to create work orders
        // And Rig Programs
        if (isCommunityUser != null && isCommunityUser) {
            notificationTypes = [
                    SELECT
                            Id,
                            UserRecordAccess.HasReadAccess,
                            UserRecordAccess.HasEditAccess
                    FROM HOG_Notification_Type__c
                    WHERE Generate_Rig_Program_Record__c = FALSE
                    AND Generate_Work_Order_Record__c = FALSE
            ];
        } else {
            notificationTypes = [
                    SELECT
                            Id,
                            UserRecordAccess.HasReadAccess,
                            UserRecordAccess.HasEditAccess
                    FROM HOG_Notification_Type__c
                    WHERE Generate_Rig_Program_Record__c = FALSE
            ];
        }

        for (HOG_Notification_Type__c notificationType : notificationTypes) {
            // Line 1496 in ServiceRequestNotification - edit logic has been taken out of migration
            if (notificationType.UserRecordAccess.HasEditAccess) {
                notificationTypeIds.add(notificationType.Id);
            }
        }

        return [
                SELECT
                        HOG_Notification_Type__c Id,
                        HOG_Notification_Type__r.HOG_Service_Category__c,
                        HOG_Notification_Type__r.Name Name,
                        Active__c
                FROM HOG_Work_Order_Type__c
                GROUP BY
                                HOG_Notification_Type__c,
                                HOG_Notification_Type__r.HOG_Service_Category__c,
                                HOG_Notification_Type__r.Name,
                                Active__c
                HAVING
                HOG_Notification_Type__r.HOG_Service_Category__c = :serviceCategoryId AND
                Active__c = TRUE AND
                HOG_Notification_Type__c IN :notificationTypeIds
                ORDER BY HOG_Notification_Type__r.Name
                LIMIT 1000  // max limit of Collection type
        ];
    }

    public List<HOG_Notification_Type__c> getNotificationTypeById(String Id) {
        return [
                SELECT
                        Auto_Generate_Work_Order_Number__c,
                        Auto_generate_Notification_Number__c,
                        Generate_Numbers_From_SAP__c,
                        Generate_Work_Order_Record__c,
                        Generate_Rig_Program_Record__c,
                        Allow_Manual_Update_Of_Work_Order_Number__c,
                        Allow_Event_Selection__c,
                        Allow_Work_Order_Linking__c,
                        Notification_Type__c,
                        Order_Type__c,
                        HOG_User_Status__r.Code__c,
                        Main_Work_Centre__c
                FROM HOG_Notification_Type__c
                WHERE Id = :Id
                LIMIT 1
        ];
    }

    public List<AggregateResult> getServiceActivityNames(String serviceCategoryId, String serviceType) {
        return [
                SELECT
                        HOG_User_Status__c Id,
                        HOG_Notification_Type__r.HOG_Service_Category__c,
                        HOG_Notification_Type__c,
                        HOG_User_Status__r.Name Name,
                        Active__c
                FROM HOG_Work_Order_Type__c
                GROUP BY
                                HOG_User_Status__c,
                                HOG_Notification_Type__r.HOG_Service_Category__c,
                                HOG_Notification_Type__c,
                                HOG_User_Status__r.Name,
                                Active__c
                HAVING
                HOG_Notification_Type__r.HOG_Service_Category__c = :serviceCategoryId AND
                HOG_Notification_Type__c = :serviceType AND
                Active__c = TRUE
                ORDER BY HOG_User_Status__r.Name
                LIMIT 1000  // max limit of Collection type
        ];
    }

    public List<HOG_Notification_Type_Priority__c> getServicePriorities(String serviceType) {
        return [
                SELECT
                        Id,
                        HOG_Notification_Type__c,
                        HOG_Service_Priority__r.Priority_Description__c,
                        HOG_Service_Priority__r.Priority_Number__c,
                        HOG_Service_Priority__r.Priority_Code__c
                FROM HOG_Notification_Type_Priority__c
                WHERE
                HOG_Notification_Type__c = :serviceType
                AND Work_Order_Priority__c = FALSE
                AND HOG_Service_Priority__r.Priority_Code__c != NULL
                AND Active__c = TRUE
                ORDER BY HOG_Service_Priority__r.Priority_Number__c
                LIMIT 1000  // max limit of Collection type
        ];
    }

    public List<AggregateResult> getServiceRequired(
            String serviceCategoryId,
            String serviceType,
            String serviceActivity,
            Boolean wellLocationStatus) {
        return [
                SELECT
                        HOG_Service_Required__c Id,
                        HOG_Notification_Type__r.HOG_Service_Category__c,
                        HOG_Notification_Type__c,
                        HOG_User_Status__c,
                        HOG_Service_Required__r.Name Name,
                        Production_Service__c,
                        Active__c
                FROM HOG_Work_Order_Type__c
                GROUP BY
                                HOG_Service_Required__c,
                                HOG_Notification_Type__r.HOG_Service_Category__c,
                                HOG_Notification_Type__c,
                                HOG_User_Status__c,
                                HOG_Service_Required__r.Name,
                                Production_Service__c,
                                Active__c
                HAVING
                HOG_Notification_Type__r.HOG_Service_Category__c = :serviceCategoryId AND
                HOG_Notification_Type__c = :serviceType AND
                HOG_User_Status__c = :serviceActivity AND
                Production_Service__c = :wellLocationStatus AND
                Active__c = TRUE
                ORDER BY HOG_Service_Required__r.Name
                LIMIT 1000  // max limit of Collection type
        ];
    }

    public List<HOG_Service_Required__c> getOneServiceRequired(String serviceRequiredId) {
        return [
                SELECT
                        Supervised__c,
                        Non_Recurring__c,
                        Vendor_Company_Is_Not_Required__c,
                        Vendor_Invoicing_Personnel_Is_Required__c,
                        Service_Time_Is_Required__c,
                        HOG_Service_Code_Group__r.Code_Group__c
                FROM HOG_Service_Required__c
                WHERE
                        Id = :serviceRequiredId
        ];
    }

    public List<AggregateResult> getServiceSpecifics(
            String serviceCategory,
            String serviceTypeId,
            String serviceActivityId,
            String serviceRequiredId,
            Boolean wellLocationStatusIsProducing) {

        return [
                SELECT
                        Id,
                        HOG_Notification_Type__r.HOG_Service_Category__c,
                        HOG_Notification_Type__c,
                        HOG_User_Status__c,
                        HOG_Service_Required__c,
                        HOG_Service_Code_MAT__r.Name Name,
                        HOG_Service_Code_MAT__r.MAT_Code__c,
                        Production_Service__c,
                        Active__c
                FROM HOG_Work_Order_Type__c
                GROUP BY
                                Id,
                                HOG_Notification_Type__r.HOG_Service_Category__c,
                                HOG_Notification_Type__c,
                                HOG_User_Status__c,
                                HOG_Service_Required__c,
                                HOG_Service_Code_MAT__r.Name,
                                HOG_Service_Code_MAT__r.MAT_Code__c,
                                Production_Service__c,
                                Active__c
                HAVING
                HOG_Notification_Type__r.HOG_Service_Category__c = :serviceCategory AND
                HOG_Notification_Type__c = :serviceTypeId AND
                HOG_User_Status__c = :serviceActivityId AND
                HOG_Service_Required__c = :serviceRequiredId AND
                Production_Service__c = :wellLocationStatusIsProducing AND
                Active__c = TRUE
                ORDER BY HOG_Service_Code_MAT__r.Name
                LIMIT 1000  // max limit of Collection type
        ];
    }

    public List<Location__c> getOneWellLocation(String locationId) {
        return [
                SELECT
                        Name,
                        Route__c,
                        Operating_Field_AMU__c,
                        Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Operating_Field_AMU__r.Operating_District__c,
                        Facility__c,
                        Current_Well_Status__c,
                        SAP_Object_ID__c,
                        Maintenance_Plant__c,
                        DLFL__c,
                        PVR_AVGVOL_5D_GAS__c,
                        PVR_AVGVOL_5D_OIL__c,
                        PVR_AVGVOL_5D_COND__c,
                        PVR_AVGVOL_5D_SAND__c,
                        PVR_AVGVOL_5D_WATER__c,
                        PVR_AVGVOL_UPDATED__c,
                        PVR_AVGVOL_5D_NO_OF_MEASURES__c,
                        PVR_AVGVOL_5D_LAST_MEASURED__c,
                        Surface_Location__c,
                        Well_Type__c,
                        Status__c
                FROM Location__c
                WHERE Id = :locationId
        ];
    }

    public List<Facility__c> getOneFacility(String facilityId) {
        return [
                SELECT
                        Name,
                        Plant_Section__c,
                        Operating_Field_AMU__c,
                        Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Operating_Field_AMU__r.Operating_District__c,
                        Current_Facility_Status__c,
                        SAP_Object_ID__c,
                        Maintenance_Plant__c,
                        Facility_Type__c,
                        Surface_Location__c,
                        DLFL__c,
                        Status__c
                FROM Facility__c
                WHERE Id = :facilityId
        ];
    }

    public List<Equipment__c> getOneEquipment(String equipmentId) {
        return [
                SELECT
                        Name,
                        Equipment_Number__c,
                        Equipment_Category__c,
                        Catalogue_Code__c,
                        Manufacturer__c,
                        Manufacturer_Serial_No__c,
                        Maintenance_Plant__c,
                        Location__c,
                        Facility__c,
                        Status__c,
                        DLFL__c,

                        // Facility
                        Facility__r.Name,
                        Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Facility__r.Operating_Field_AMU__r.Operating_District__c,
                        Facility__r.Plant_Section__c,
                        Facility__r.Operating_Field_AMU__c,
                        Facility__r.Current_Facility_Status__c,
                        Facility__r.SAP_Object_ID__c,

                        // Well ID
                        Location__r.Name,
                        Location__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Location__r.Operating_Field_AMU__r.Operating_District__c,
                        Location__r.Route__c,
                        Location__r.Operating_Field_AMU__c,
                        Location__r.Facility__c,
                        Location__r.PVR_AVGVOL_5D_GAS__c,
                        Location__r.PVR_AVGVOL_5D_OIL__c,
                        Location__r.PVR_AVGVOL_5D_COND__c,
                        Location__r.PVR_AVGVOL_5D_SAND__c,
                        Location__r.PVR_AVGVOL_5D_WATER__c,
                        Location__r.PVR_AVGVOL_UPDATED__c,
                        Location__r.PVR_AVGVOL_5D_NO_OF_MEASURES__c,
                        Location__r.PVR_AVGVOL_5D_LAST_MEASURED__c,
                        Location__r.Current_Well_Status__c,
                        Location__r.RecordType.Name,
                        Location__r.SAP_Object_ID__c,

                        // System
                        System__c,
                        System__r.Name,
                        System__r.SAP_Object_ID__c,
                        System__r.Operating_Field_AMU__c,
                        System__r.Operating_Field_AMU__r.Operating_District__c,
                        System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        System__r.Facility__c,
                        System__r.Route__c,

                        // SubSystem
                        Sub_System__c,
                        Sub_System__r.Name,
                        Sub_System__r.SAP_Object_ID__c,
                        Sub_System__r.Operating_Field_AMU__c,
                        Sub_System__r.Operating_Field_AMU__r.Operating_District__c,
                        Sub_System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Sub_System__r.Facility__c,
                        Sub_System__r.System__c,
                        Sub_System__r.Well_ID__c,
                        Sub_System__r.Route__c,

                        // FEL
                        Functional_Equipment_Level__c,
                        Functional_Equipment_Level__r.Name,
                        Functional_Equipment_Level__r.Route__c,
                        Functional_Equipment_Level__r.SAP_Object_ID__c,
                        Functional_Equipment_Level__r.Operating_Field_AMU__c,
                        Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__c,
                        Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Functional_Equipment_Level__r.System__c,
                        Functional_Equipment_Level__r.System__r.Facility__c,
                        Functional_Equipment_Level__r.System__r.Operating_Field_AMU__c,
                        Functional_Equipment_Level__r.System__r.Operating_Field_AMU__r.Operating_District__c,
                        Functional_Equipment_Level__r.System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Functional_Equipment_Level__r.Sub_System__c,
                        Functional_Equipment_Level__r.Sub_System__r.Facility__c,
                        Functional_Equipment_Level__r.Sub_System__r.Well_ID__c,
                        Functional_Equipment_Level__r.Sub_System__r.System__c,
                        Functional_Equipment_Level__r.Sub_System__r.Operating_Field_AMU__c,
                        Functional_Equipment_Level__r.Sub_System__r.Operating_Field_AMU__r.Operating_District__c,
                        Functional_Equipment_Level__r.Sub_System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Functional_Equipment_Level__r.Facility__c,
                        Functional_Equipment_Level__r.Facility__r.Operating_Field_AMU__c,
                        Functional_Equipment_Level__r.Facility__r.Operating_Field_AMU__r.Operating_District__c,
                        Functional_Equipment_Level__r.Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__r.Facility__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__r.Sub_System__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__r.Sub_System__r.Well_ID__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__r.System__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__r.Operating_Field_AMU__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__c,
                        Functional_Equipment_Level__r.Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,

                        // Yard
                        Yard__c,
                        Yard__r.Name,
                        Yard__r.Route__c,
                        Yard__r.SAP_Object_ID__c,
                        Yard__r.Well_ID__c,
                        Yard__r.Well_ID__r.Facility__c,
                        Yard__r.Well_ID__r.Operating_Field_AMU__c,
                        Yard__r.Well_ID__r.Operating_Field_AMU__r.Operating_District__c,
                        Yard__r.Well_ID__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Yard__r.Facility__c,
                        Yard__r.Facility__r.Operating_Field_AMU__c,
                        Yard__r.Facility__r.Operating_Field_AMU__r.Operating_District__c,
                        Yard__r.Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Yard__r.Operating_Field_AMU__c,
                        Yard__r.Operating_Field_AMU__r.Operating_District__c,
                        Yard__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c

                FROM Equipment__c
                WHERE Id = :equipmentId
        ];
    }

    public List<Well_Event__c> getOneWellEvent(String wellEventId) {
        return [
                SELECT
                        Name,
                        Well_ID__r.Route__c,
                        Well_ID__r.Operating_Field_AMU__c,
                        Well_ID__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Well_ID__r.Operating_Field_AMU__r.Operating_District__c,
                        Well_ID__r.Facility__c,
                        Current_Well_Status__c,
                        SAP_Object_ID__c,
                        Well_ID__r.Maintenance_Plant__c,
                        DLFL__c,
                        PVR_AVGVOL_5D_GAS__c,
                        PVR_AVGVOL_5D_OIL__c,
                        PVR_AVGVOL_5D_COND__c,
                        PVR_AVGVOL_5D_SAND__c,
                        PVR_AVGVOL_5D_WATER__c,
                        PVR_AVGVOL_UPDATED__c,
                        PVR_AVGVOL_5D_NO_OF_MEASURES__c,
                        PVR_AVGVOL_5D_LAST_MEASURED__c,
                        Surface_Location__c,
                        Well_Type__c,
                        Status__c
                FROM Well_Event__c
                WHERE Id = :wellEventId
        ];
    }

    public List<System__c> getOneSystem(String systemId) {
        return [
                SELECT
                        Name,
                        Operating_Field_AMU__c,
                        Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Operating_Field_AMU__r.Operating_District__c,
                        SAP_Object_ID__c,
                        Maintenance_Plant__c,
                        Route__c,
                        Surface_Location__c,
                        DLFL__c
                FROM System__c
                WHERE Id = :systemId
        ];
    }

    public List<Sub_System__c> getOneSubSystem(String subSystemId) {
        return [
                SELECT
                        Name,
                        Well_ID__c,
                        Well_ID__r.Operating_Field_AMU__c,
                        Well_ID__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Well_ID__r.Operating_Field_AMU__r.Operating_District__c,
                        System__c,
                        System__r.Operating_Field_AMU__c,
                        System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        System__r.Operating_Field_AMU__r.Operating_District__c,
                        Facility__c,
                        Facility__r.Operating_Field_AMU__c,
                        Facility__r.Operating_Field_AMU__r.Operating_District__c,
                        Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        SAP_Object_ID__c,
                        Maintenance_Plant__c,
                        Route__c,
                        Surface_Location__c,
                        DLFL__c
//Status__c
                FROM Sub_System__c
                WHERE Id = :subSystemId
        ];
    }

    public List<Yard__c> getOneYard(String yardId) {
        return [
                SELECT
                        Name,
                        Id,
                        Well_ID__c,
                        Well_ID__r.Facility__c,
                        Well_ID__r.Operating_Field_AMU__c,
                        Well_ID__r.Operating_Field_AMU__r.Operating_District__c,
                        Well_ID__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Facility__c,
                        Facility__r.Operating_Field_AMU__c,
                        Facility__r.Operating_Field_AMU__r.Operating_District__c,
                        Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Operating_Field_AMU__c,
                        Operating_Field_AMU__r.Operating_District__c,
                        Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Functional_Location_Description_SAP__c,
                        SAP_Object_ID__c,
                        Maintenance_Plant__c,
                        Route__c,
                        Surface_Location__c,
                        DLFL__c
                //Status__c
                FROM Yard__c
                WHERE Id = :yardId
        ];
    }

    public List<Functional_Equipment_Level__c> getFEL(String felId) {
        return [
                SELECT
                        Id,
                        Name,
                        System__c,
                        System__r.Facility__c,
                        System__r.Operating_Field_AMU__c,
                        System__r.Operating_Field_AMU__r.Operating_District__c,
                        System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Sub_System__c,
                        Sub_System__r.Facility__c,
                        Sub_System__r.Operating_Field_AMU__c,
                        Sub_System__r.Operating_Field_AMU__r.Operating_District__c,
                        Sub_System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Sub_System__r.Well_ID__c,
                        Facility__c,
                        Facility__r.Operating_Field_AMU__c,
                        Facility__r.Operating_Field_AMU__r.Operating_District__c,
                        Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Functional_Equipment_Level__c,
                        Functional_Equipment_Level__r.Facility__c,
                        Functional_Equipment_Level__r.Operating_Field_AMU__c,
                        Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__c,
                        Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Operating_Field_AMU__c,
                        Operating_Field_AMU__r.Operating_District__c,
                        Operating_Field_AMU__r.Operating_District__r.Business_Unit__c,
                        Functional_Location_Description_SAP__c,
                        SAP_Object_ID__c,
                        Maintenance_Plant__c,
                        Route__c,
                        Surface_Location__c,
                        DLFL__c
                FROM Functional_Equipment_Level__c
                WHERE Id = :felId
        ];
    }

}