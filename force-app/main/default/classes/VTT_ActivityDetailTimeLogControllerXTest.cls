/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class VTT_ActivityDetailTimeLogControllerXTest {    
    @isTest static void testAutoCompleteActivities() {

        User runningUser = VTT_TestData.createVTTUser();

        System.runAs(runningUser) {        
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);


            System.AssertNotEquals(runningUser.Id, Null);

            Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
            Contact tradesman1 = VTT_TestData.createTradesmanContact('Tom', 'Cruise',  vendor1.id, runningUser.id);
            Contact tradesman2 = VTT_TestData.createTradesmanContact('Brad', 'Pitt',  vendor1.id);              

            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);       
            HOG_Maintenance_Servicing_Form__c workOrder1 = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            workOrder1.User_Status_Code__c = '5X';
            workOrder1.Order_Type__c = 'WP01';
            workOrder1.Plant_Section__c  = '200';       
            workOrder1.Work_Order_Priority_Number__c  = '1';    
            workOrder1.Equipment__c = VTT_TestData.equipment.Id;
            workOrder1.Location__c = VTT_TestData.location.Id;
            workOrder1.Operating_Field_AMU_Lookup__c = VTT_TestData.field.Id;  
            update workOrder1;

            //Turn on triggers again
            MaintenanceServicingUtilities.executeTriggerCode = true;

            List<Work_Order_Activity__c> activityList1 =  VTT_TestData.createWorkOrderActivitiesWithAssignments(workOrder1.ID, tradesman1.ID, 3);
            VTT_Utilities.executeTriggerCode = false;
            //fine tuning of the one activity  to be able to filter it
            Work_Order_Activity__c woActivity = activityList1[0];
            woActivity.Work_Center__c ='100';
            woActivity.Scheduled_Start_Date__c = System.now();
            woActivity.is_Mass_Complete__c = false;
            update woActivity;

            woActivity = activityList1[1];
            woActivity.Work_Center__c ='100';
            woActivity.Scheduled_Start_Date__c = System.now();
            update woActivity;

            woActivity = activityList1[2];
            woActivity.Work_Center__c ='100';
            woActivity.Scheduled_Start_Date__c = System.now();
            update woActivity; 

            

            //////////////////
            //* START TEST *//
            //////////////////
            Test.startTest();
                
            System.runAs(runningUser) {
                

                Work_Order_Activity__c woActivityCopy = VTT_TestData.reloadWorkOrderActivity(woActivity.id);
                PageReference pageRef = Page.VTT_ActivityDetailTimeLog;
                Test.setCurrentPageReference(pageRef);  
                ApexPages.StandardController stdController = new ApexPages.StandardController(woActivityCopy);
                pageRef.getParameters().put('id', woActivityCopy.Id);
                VTT_ActivityDetailTimeLogControllerX controller = new VTT_ActivityDetailTimeLogControllerX(stdController);

                // Start completing activities   
                controller.workflowEngine.JobComplete_Start();
                System.AssertEquals(2, controller.workflowEngine.AvailableActivitiesToComplete.size());

                // Amount of time available for spending on selected activities
                controller.workflowEngine.runningTally = 20;
                controller.availableTimeSnapShot = 20;

                // Set all available activities to marked
                for(VTT_WorkFlowEngine.AvailableActivity ac: controller.workflowEngine.AvailableActivitiesToComplete){
                    ac.MarkToComplete = true;
                    ac.userDurationInput = 2;

                     // Call runningtally function simulating checking checkbox
                    controller.selectActivityToAutoComplete();

                    // Each time recount available time
                    controller.recountAvailableTime();

                }
                controller.selectAllActivitiesToAutoComplete();

                for(VTT_WorkFlowEngine.AvailableActivity ac: controller.workflowEngine.AvailableActivitiesToComplete){
                    ac.MarkToComplete = false;
                }
                controller.checkedAll = true;
                controller.selectAllActivitiesToAutoComplete();

                for(VTT_WorkFlowEngine.AvailableActivity ac: controller.workflowEngine.AvailableActivitiesToComplete){
                    ac.MarkToComplete = true;
                    ac.userDurationInput = 0;
                }
                controller.ValidateRunningTally();
                controller.distributeAvailableTime();
                controller.ValidateRunningTally();

                for(VTT_WorkFlowEngine.AvailableActivity ac: controller.workflowEngine.AvailableActivitiesToComplete){
                    ac.MarkToComplete = true;
                    ac.userDurationInput = -1;
                }
                controller.ValidateRunningTally();

                for(VTT_WorkFlowEngine.AvailableActivity ac: controller.workflowEngine.AvailableActivitiesToComplete){
                    ac.MarkToComplete = true;
                    ac.userDurationInput = 30;
                }
                controller.ValidateRunningTally();

                controller.checkedAll = false;
                controller.selectAllActivitiesToAutoComplete();
                // validate running tally
                ApexPages.currentPage().getParameters().put('requestReschedule', 'true');
                controller.JobComplete();
                System.assertNotEquals(controller.executeWSUpdateNotification(), null);
                
                //Test File Attachments for Coverage
                controller.attachmentFile.Name = 'Test Attachment File';
                controller.attachmentFile.Body = Blob.valueOf('Unit Test Attachment Body.');
                controller.Upload();
                List<Attachment> attachments = controller.getAttachments();
                //List<Attachment> woattachments = controller.getWorkOrderAttachments();
                System.assertEquals(1, attachments.size());
                //System.assertEquals(1, woattachments.size());

                System.AssertEquals(false, VTT_Utilities.IsAdminUser());
                System.assertEquals(3, activityList1.size());    

                System.AssertEquals(true, controller.EquipmentWorkOrder); 
                System.AssertEquals(vendor1.id, controller.assignedVendor); 

            
                System.AssertEquals(1, controller.vendorAccounts.size());  
                System.AssertEquals(2, controller.allTradesmen.size());              

                System.AssertNotEquals(null, controller.getOperatorOnCall());


                System.AssertNotEquals(null, controller.VTT_Assignment());             
                System.AssertNotEquals(null, controller.UpdateAssignment()); 
                System.AssertNotEquals(null, controller.VTT_Cancel());  
                System.AssertNotEquals(null, controller.ExitAssignment());
                //System.AssertEquals(null, controller.JobComplete());  

                System.AssertNotEquals(0, controller.getPartsOptions().size());  
                System.AssertNotEquals(0, controller.getDamagesOptions().size());              
                System.AssertNotEquals(0, controller.getCausesOptions().size());                          
                System.AssertNotEquals(1, controller.getUsersOptions().size());
            }

            /////////////////
            //* STOP TEST *//
            /////////////////
            Test.stopTest();
        }           
    }

    @isTest static void testPopulateFlocIdsYardFEL(){
        User runningUser = VTT_TestData.createVTTUser();
        
        System.runAs(runningUser) { 
            MaintenanceServicingUtilities.executeTriggerCode = false;

            // Setup data
            Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
            Field__c amu = HOG_TestDataFactory.createAMU('Test Amu', district.Id, true);
            HOG_Maintenance_Servicing_Form__c wo1 = VTT_TestDataFactory.createWorkOrder(true);

            //Yard
            Yard__c yard = HOG_TestDataFactory.createYard(amu.Id, 'H1-01-001-00002-900001', true);
            Work_Order_Activity__c woaOnYardPassing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'H1-01-001-00002-900001','Yard', yard.Id, true);
            Work_Order_Activity__c woaOnYardFailing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'Wrong Floc','Yard', yard.Id, true);

            //FEL
            Functional_Equipment_Level__c fel = HOG_TestDataFactory.createFunctionalEquipmentLevel(amu.Id, 'H1-06-006-00080-000091-000003', true);
            Work_Order_Activity__c woaOnFELpassing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'H1-06-006-00080-000091-000003','FEL', fel.Id, true);
            Work_Order_Activity__c woaOnFELfailing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'Wrong Floc','FEL', fel.Id, true);
            
            Test.startTest();
                PageReference pageRef = Page.VTT_ActivityDetailTimeLog;
                Test.setCurrentPageReference(pageRef);  
                ApexPages.StandardController stdController = new ApexPages.StandardController(woaOnYardPassing);
                pageRef.getParameters().put('id', woaOnYardPassing.Id);
                VTT_ActivityDetailTimeLogControllerX controller = new VTT_ActivityDetailTimeLogControllerX(stdController);
                //Yard
                System.assertEquals(woaOnYardPassing.Yard__c,yard.Id);
                controller.populateFlocId(woaOnYardFailing);
                //System.assertEquals(woaOnYardFailing.Yard__c,null);

                //FEL
                controller.populateFlocId(woaOnFELpassing);
                System.assertEquals(woaOnFELpassing.Functional_Equipment_Level__c,fel.Id);
                controller.populateFlocId(woaOnFELfailing);
                //System.assertEquals(woaOnFELfailing.Functional_Equipment_Level__c,null);
        }
    }

    @isTest static void testPopulateFlocIdsSystemSubSystem(){
        User runningUser = VTT_TestData.createVTTUser();
        
        System.runAs(runningUser) { 
            MaintenanceServicingUtilities.executeTriggerCode = false;

            // Setup data
            Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
            Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
            Field__c amu = HOG_TestDataFactory.createAMU('Test Amu', district.Id, true);
            HOG_Maintenance_Servicing_Form__c wo1 = VTT_TestDataFactory.createWorkOrder(true);

            //Sub System
            Sub_System__c subSystem = HOG_TestDataFactory.createSubSystem('H1-06-006-00030-000071', true);
            Work_Order_Activity__c woaOnSubSystemPassing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'H1-06-006-00030-000071','Sub System', subSystem.Id, true);
            Work_Order_Activity__c woaOnSubSystemFailing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'Wrong Floc','Sub System', subSystem.Id, true);
            
            //System
            System__c testSystemFloc = HOG_TestDataFactory.createSystem(amu.Id, 'H1-05-008-00020', true);
            Work_Order_Activity__c woaOnSystemPassing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'H1-05-008-00020','System', testSystemFloc.Id, true);
            Work_Order_Activity__c woaOnSystemFailing = VTT_TestDataFactory.createWorkOrderActivityWithSpecificFloc(wo1.Id, amu.Id, 'Wrong Floc','System', testSystemFloc.Id, true);
            
            //Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', amu.Id, 'H1-01-004-10007', null, true);
            //Well_Event__c wellEvent = HOG_TestDataFactory.createWellEvent(wellLocation.Id, 'H1-01-004-10007-100001', null, true);
            
            Test.startTest();
                PageReference pageRef = Page.VTT_ActivityDetailTimeLog;
                Test.setCurrentPageReference(pageRef);  
                ApexPages.StandardController stdController = new ApexPages.StandardController(woaOnSubSystemPassing);
                pageRef.getParameters().put('id', woaOnSubSystemPassing.Id);
                VTT_ActivityDetailTimeLogControllerX controller = new VTT_ActivityDetailTimeLogControllerX(stdController);
                //Sub System
                System.assertEquals(woaOnSubSystemPassing.Sub_System__c,subSystem.Id);
                controller.populateFlocId(woaOnSubSystemFailing);
                //System.assertEquals(woaOnSubSystemFailing.Sub_System__c,null);

                //System
                controller.populateFlocId(woaOnSystemPassing);
                System.assertEquals(woaOnSystemPassing.System__c,testSystemFloc.Id);
                controller.populateFlocId(woaOnSystemFailing);
                //System.assertEquals(woaOnSystemFailing.System__c,null);
        }
    }          
}