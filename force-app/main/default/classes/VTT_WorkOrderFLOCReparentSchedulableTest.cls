/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VTT_WorkOrderFLOCReparentSchedulable class
History:        jschn 2018-12-14 - Created. (US - 001323)
*************************************************************************************************/
@IsTest
private class VTT_WorkOrderFLOCReparentSchedulableTest {

    @IsTest
    static void testRun() {
        String jobName = 'TestScheduleJob';
        String cronString = '0 0 3 * * ?';
        Test.startTest();
        Id jobId = System.schedule(jobName, cronString, new VTT_WorkOrderFLOCReparentSchedulable());
        Test.stopTest();

        CronTrigger job = [SELECT CronExpression, CronJobDetail.Name
        FROM CronTrigger
        WHERE Id = :jobId];
        System.assertEquals(cronString, job.CronExpression);
        System.assertEquals(jobName, job.CronJobDetail.Name);
    }

}