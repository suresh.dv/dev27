/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database access interface for Lightning Community Management

History:        mbrimus 2019-09-24 - Created.
*************************************************************************************************/
public interface VTT_LTNG_PortalManagementDAO {

    /**
     * Returns list of account that are portal enabled and can be managed
     *
     * @return
     */
    List<Account> getAccounts();

    /**
    * Returns list of account that are portal enabled and can be managed
    *
    * @return
    */
    Account getAccountById(String accountId);

    /**
    * Returns list of contacts assigned to user
    *
    * @return
    */
    List<Contact> getUsersContact(Id userId);

    /**
     * Returns list of contacts
     *
     * @param accountId
     * @param enabled - portal enabled or disabled
     *
     * @return
     */
    List<Contact> getContactsForAccount(Id accountId, Boolean enabled);

    /**
     *  Returns User record for given User ID
     *
     * @param userId
     *
     * @return
     */
    List<User> getUserById(Id userId);

}