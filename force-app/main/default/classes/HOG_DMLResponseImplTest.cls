/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_DMLResponseImpl
History:        jschn 2019-01-30 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_DMLResponseImplTest {

    @IsTest
    static void testConstructorWithObject() {
        Account acc = new Account(Name = 'test account');

        Test.startTest();
        HOG_DMLResponse response = new HOG_DMLResponseImpl(acc);
        Test.stopTest();

        System.assert(response.success);
        System.assertNotEquals(null, response.resultObject);
        System.assertEquals(null, response.errors);
        System.assertEquals(acc.Name, ((Account)response.resultObject).Name);
    }

    @IsTest
    static void testConstructorWithErrorString() {
        String error = 'Test error';

        Test.startTest();
        HOG_DMLResponse response = new HOG_DMLResponseImpl(error);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObject);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(error, response.errors.get(0));
    }

    @IsTest
    static void testConstructorWithException() {
        String error = 'Test error';
        TestException ex = new TestException(error);

        Test.startTest();
        HOG_DMLResponse response = new HOG_DMLResponseImpl(ex);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObject);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(error, response.errors.get(0));
    }

    @IsTest
    static void testAddErrorWithException() {
        String error1 = 'Test error1';
        String error2 = 'Test error2';
        TestException ex1 = new TestException(error1);
        TestException ex2 = new TestException(error2);

        Test.startTest();
        HOG_DMLResponseImpl response = new HOG_DMLResponseImpl(ex1);
        response.addError(ex2);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObject);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(2, response.errors.size());
        System.assertEquals(error1, response.errors.get(0));
        System.assertEquals(error2, response.errors.get(1));
    }

    @IsTest
    static void testAddErrorWithErrorMsg() {
        String error1 = 'Test error1';
        String error2 = 'Test error2';

        Test.startTest();
        HOG_DMLResponseImpl response = new HOG_DMLResponseImpl(error1);
        response.addError(error2);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObject);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(2, response.errors.size());
        System.assertEquals(error1, response.errors.get(0));
        System.assertEquals(error2, response.errors.get(1));
    }

    @IsTest
    static void testAddErrorWithErrorMsgWithPrettify() {
        String error1 = 'Test error1';
        String error2 = 'Test error2';
        String uglyError1 = HOG_DMLResponseImpl.VALIDATION_ERROR_PREFIX + error1 + HOG_DMLResponseImpl.VALIDATION_ERROR_POSTFIX;
        String uglyError2 = HOG_DMLResponseImpl.VALIDATION_ERROR_PREFIX + error2 + HOG_DMLResponseImpl.VALIDATION_ERROR_POSTFIX;

        Test.startTest();
        HOG_DMLResponseImpl response = new HOG_DMLResponseImpl(uglyError1);
        response.addError(uglyError2);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObject);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(2, response.errors.size());
        System.assertEquals(error1, response.errors.get(0));
        System.assertEquals(error2, response.errors.get(1));
    }

    @IsTest
    static void testAddErrorWithExceptionWithPrettify() {
        String error1 = 'Test error1';
        String error2 = 'Test error2';
        String uglyError1 = HOG_DMLResponseImpl.VALIDATION_ERROR_PREFIX + error1 + HOG_DMLResponseImpl.VALIDATION_ERROR_POSTFIX;
        String uglyError2 = HOG_DMLResponseImpl.VALIDATION_ERROR_PREFIX + error2 + HOG_DMLResponseImpl.VALIDATION_ERROR_POSTFIX;

        TestException ex1 = new TestException(uglyError1);
        TestException ex2 = new TestException(uglyError2);

        Test.startTest();
        HOG_DMLResponseImpl response = new HOG_DMLResponseImpl(ex1);
        response.addError(ex2);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObject);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(2, response.errors.size());
        System.assertEquals(error1, response.errors.get(0));
        System.assertEquals(error2, response.errors.get(1));
    }

    public class TestException extends Exception {}

}