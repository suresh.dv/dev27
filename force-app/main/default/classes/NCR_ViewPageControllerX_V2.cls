public class NCR_ViewPageControllerX_V2{
    String recordId;
    ID contactId;
    ID AccID;
    ID userID;
  
  
    public NCR_WorkFlowEngine workflowEngine {get; private set;} 
  
    private final ApexPages.StandardController stdcontroller;
    private final NCR_Case__c ncr_record;
    
    
    public Boolean SubmitConfirmMode {get; private set;}

    //used to get a hold of the attachment record selected for deletion
    public String selectedAttachmentId { get; set; }
    public List<Document> documentList {get; private set;}     
    public Document documentFile {get; set;}        
    
    public String BusinessUnitName {get;set;}
    public String ProjectName {get;set;}

    public string currentBU { get; set; }
    public string currentProj { get; set; }
     
    public string selectedSupplier { get; set; }
    public string selectedAssignee { get; set; }
    
    //public Map<String, Schema.FieldSet> fieldSetMap {get; private set;}
    public Map<String, string> fieldSetLabelsMap {get; private set;}
    
    public String getCurrentProj() {return currentProj;} 
    public void setCurrentProj(string currentProj) {this.currentProj = currentProj;} 
    
      public PageReference NCR_CasesClone()
    {
        PageReference secondPage = Page.NCR_Case_ClonePage;
        //secondPage.setRedirect(true);
        return secondPage;     
    }
    
    public PageReference NCR_AdminEdit()
    {
        PageReference secondPage = Page.NCR_Case_AdminEditPage;
        //secondPage.setRedirect(true);
        return secondPage;     
    }
    
    public PageReference NCR_NotifyQALead()
    {
        PageReference secondPage = Page.NCR_Case_NotifyQALead;
        //secondPage.setRedirect(true);
        return secondPage;     
    }    
    
    public PageReference NCR_Pdf(){
        return Page.NCR_PDFPage;
    }

    public PageReference NCR_Action1()
    {
        return null;
    }
    public PageReference NCR_Action2()
    {
        return null;
    }    
    public PageReference NCR_Action3()
    {
        return null;
    } 
    
    public PageReference saveClonedCase(){
        this.ncr_record.Business_Unit_Lookup__c = currentBU;
        this.ncr_record.Project__c = currentProj; 
        
       //pg.getParameters().put('id', ncr_record.id);        
        
       NCR_Case__c n = new NCR_Case__c();
       
       n = this.ncr_record;
       
       n.id=null;
       n.NCR_Case_State__c = 'Created';
       n.Originator__c =  UserInfo.getUserId();
       
       insert n;
       PageReference pg = new PageReference('/'+n.id);
       //pg.getParameters().put('id', n.id);
        
       return pg;
             
    }
    
    public PageReference save(){
        this.ncr_record.Business_Unit_Lookup__c = currentBU;
        this.ncr_record.Project__c = currentProj;
        
         this.ncr_record.Vendor__c = selectedSupplier;
        this.ncr_record.Assignee__c = selectedAssignee; 
        
       PageReference pg = new PageReference('/apex/NCR_ViewPage');
       pg.getParameters().put('id', ncr_record.id);        
        
        update this.ncr_record;
        
        return pg;     
    }
                  
    public PageReference NCR_Save()
    {
    
        PageReference pageRef;
        SubmitConfirmMode = false;
        
        if(this.ncr_record.NCR_Case_State__c == 'Submitted') {
            this.ncr_record.QA_Lead__c = UserInfo.getUserId();
        }
        
        this.ncr_record.Business_Unit_Lookup__c = currentBU;
        this.ncr_record.Project__c = currentProj;
        
        this.ncr_record.Vendor__c = selectedSupplier;
        this.ncr_record.Assignee__c = selectedAssignee;        
        
        String oldState = this.ncr_record.NCR_Case_State__c;    
        pageRef = this.workflowEngine.NCR_Save();
        String newState = this.ncr_record.NCR_Case_State__c;    
        
        SaveAttachments();
        
        if(oldState<>'Submitted' && newState =='Submitted')
        {
            SubmitConfirmMode = true;
           //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'NCR Record successfully submitted')); 
           //return null; //stay on the same page
            
           PageReference pg = new PageReference('/apex/NCR_ViewPage');
           pg.getParameters().put('id', ncr_record.id);
           pg.getParameters().put('message', 'NCR Record successfully submitted');
           pg.setRedirect(true);
           return pg;            
        }
        else
        {
            return pageRef;
        }    
    }       
    public PageReference NCR_CancelEdit()
    {
        //delete uploaded attachments
        delete documentList;
        documentList = new List<Document>(); 
        
        return this.workflowEngine.NCR_CancelEdit();
    }
    public PageReference NCR_Exit()
    {
        return this.workflowEngine.exit();
    }
       
    public NCR_ViewPageControllerX_V2(ApexPages.StandardController stdController) {
        recordId = stdController.getId();
        userID =   UserInfo.getUserid();

       List<string> fields = new List<String>{'Name','NCR_Case_State__c', 'Project__r.Name','Business_Unit_Lookup__r.Name','Vendor__c','Assignee__c' };
        if(!Test.isRunningTest()) {
            stdController.addFields(fields);
        }    

        //add fields from fieldset
        List<String> fieldNames = new List<String>();
        fieldSetLabelsMap = new Map<string, String>();
        
        fieldSetLabelsMap.put('NCR_Case_Detail', 'NCR Case Details');
        
        
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.NCR_Case__c.FieldSets.getMap();
        for (String fieldSetName : fieldSetMap.keySet()){
            
            //System.debug('field name is ' + fieldSetName );
            
            fieldSetLabelsMap.put(fieldSetName, SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getLabel()) ;
            
            for(Schema.FieldSetMember f : SObjectType.NCR_Case__c.FieldSets.getMap().get(fieldSetName).getFields()){
                        fieldNames.add(f.getFieldPath());
                      }
                     
        }
         

        if(!Test.isRunningTest()) {
            stdController.addFields(fieldNames);   
        }


        this.stdcontroller = stdController;
        this.ncr_record = (NCR_Case__c)stdController.getRecord();          
        
        //TODO we need to find record type name based on record ID
        workflowEngine = new NCR_WorkFlowEngine(stdController );
        SubmitConfirmMode = false;
        
        String message = '' + ApexPages.CurrentPage().GetParameters().Get('message');
        String pageHeaderReferer = ApexPages.currentPage().getHeaders().get('Referer'); 

        // Use the referrer parameter to only show the message when coming from Page 1
        //if(pageHeaderReferer != null && pageHeaderReferer.containsIgnoreCase('NCR_ViewPage') && message != 'null')
        if(message != 'null')
        {
               ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, message)); 
        }        
        
        
        documentList = new List<Document>() ; 
        documentFile = new Document();

        this.ProjectName = ncr_record.Project__r.Name;
        this.BusinessUnitNAme = this.ncr_record.Business_Unit_Lookup__r.Name;
        
        //Assign selected business unit and project
        currentBU = this.ncr_record.Business_Unit_Lookup__r.Id;
        currentProj =  this.ncr_record.Project__c;  
        
        selectedSupplier = this.ncr_record.Vendor__c;
        selectedAssignee =  this.ncr_record.Assignee__c;
    }

    private void SaveAttachments()
    {
        
        List<Attachment> attachmentList = new List<Attachment>();
        
        documentList = [select id, Name, Body from Document where id in :documentList];
        for(Document doc: documentList)
        {
        
            Attachment newAtt = new Attachment();
             
            newAtt.ParentId = ncr_record.id;
            newAtt.OwnerId = UserInfo.getUserId();            
            newAtt.Body= doc.body;                        
            newAtt.Name= doc.Name;              
            
            attachmentList.add(newAtt); 
        }
        insert attachmentList;
       
        delete documentList;
    }
    
    public PageReference removeFile()
    {
        // if for any reason we are missing the reference
        if (selectedAttachmentId == null)
            return null;

        List<Document> selectedDocument = [Select Id, Name From Document Where Id = :selectedAttachmentId ];
         
        if (selectedDocument.size() > 0)
        {
                try
                {
                    delete selectedDocument ;
                    
                    
                    documentList = [select id, name from Document where id in :documentList];
                    /*                    
                    Integer i = 0;
                    for(Document doc: documentList)
                    {
                        if(doc.id == selectedAttachmentId)
                        {
                            documentList.remove(i);
                        }
                        i++;
                    }
                    */
                } 
                catch (DMLException e) 
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting file'));
                    return null;
                }
                finally 
                {
                    //maintenanceAttachments = getMaintenanceAttachments();
                    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File deleted successfully')); 
                }
        }
            
        return null;
    }    
    public PageReference uploadFile()  
    {  
    
        try 
        {    
        if(documentFile.Name == null || documentFile.Body == null)
        {
            return null;
        }
    
    /*
        Document doc = new Document();
        doc.folderid = UserInfo.getUserId();
        doc.name = document.Name;
        doc.body = document.body;
        insert doc;
      */
        documentFile.folderid = UserInfo.getUserId(); 
        insert documentFile;
        documentList.add(documentFile);                    

        System.debug('\n*****************************************\n'
            + 'METHOD: NCR_CreatePageControllerX.uploadFile'
            + '\NCR ID: ' + this.ncr_record.id
            + '\ndocument: ' + documentFile
            + '\ndocumentBody: ' + documentFile.Body
            + '\ndocumentName: ' + documentFile.Name
            + '\n************************************************\n');    

            //insert document;
            //attachmentList.add(document);
        } 
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        }
        finally 
        {
            //document.body = null; // clears the viewstate
            //document = new Attachment();
            
            
            documentFile.body = null;
            documentFile = new Document();
        }    

        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'File uploaded successfully')); 
        
       
  
                     
        return null; 
    }    
    

    /*populate business unit picklist*/
    public List<SelectOption> getBusinessUnits() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('none','--None--') };
        
        for(Business_Unit__c b:[SELECT id, name from Business_Unit__c where RecordType.name = 'SAP Profit Center Level 4']){
            options.add(new SelectOption(b.id,b.name));
        }
        
        return options;
    }
    
    /*populate project picklist based on business unit*/
    public List<SelectOption> getProjects() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('none','--None--') };
        
        for(Milestone1_Project__c p:[SELECT id, name from Milestone1_Project__c where RecordType.name = 'SAP Project' and Business_Unit__c = :currentBU]){
            options.add(new SelectOption(p.id,p.name));
        }
        
        return options;
    }  
    
    /*populate suppliers picklist*/
    public List<SelectOption> getNCRSuppliers() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(Account acc:[SELECT id, name from Account where RecordType.name = 'SAP Vendor' ORDER BY name ASC]){
            options.add(new SelectOption(acc.id,acc.name));
        }
        
        return options;
    }
    
    /*populate Supplier specific Assignees picklist based on Supplier*/
    public List<SelectOption> getNCRSupplierAssignees() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(User u:[select ID, ContactId, Contact.Firstname, Contact.Lastname from User Where ContactId IN (SELECT Id FROM Contact where Account.Id =: selectedSupplier) ORDER BY Contact.LastName ASC]){
            String fullName=u.Contact.Lastname+' '+u.Contact.Firstname;
            options.add(new SelectOption(u.id,fullName));
        }
        
        return options;
    }

}