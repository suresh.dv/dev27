/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_Utility_Endpoint
History:        jschn 25/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_Utility_EndpointTest {

    @IsTest
    static void getTradesmanInfo_admin() {
        VTT_TradesmanFlags result;
        User runningUser = VTT_TestData.createVTTAdminUser();
        Boolean expectedIsAdmin = true;
        Boolean expectedIsVendorTradesman = false;

        System.runAs(runningUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, runningUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            tradesman.Current_Work_Order_Activity__c = activity.Id;
            update tradesman;

            Test.startTest();
            result = VTT_Utility_Endpoint.getTradesmanInfo();
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Should always return value');
        System.assertEquals(expectedIsAdmin, result.isAdmin, 'As user has proper PS/Profile, isAdmin flag should be true');
        System.assertEquals(expectedIsVendorTradesman, result.isVendorSupervisor, 'As user has proper PS/Profile, isVendorSupervisor flag should be false');
    }

    @IsTest
    static void getTradesmanInfo_supervisor() {
        VTT_TradesmanFlags result;
        User runningUser = VTT_TestData.createVendorSupervisorUser();
        Boolean expectedIsAdmin = false;
        Boolean expectedIsVendorTradesman = true;

        System.runAs(runningUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Doctor', 'Strange', vendor.Id, runningUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            tradesman.Current_Work_Order_Activity__c = activity.Id;
            update tradesman;

            Test.startTest();
            result = VTT_Utility_Endpoint.getTradesmanInfo();
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Should always return value');
        System.assertEquals(expectedIsAdmin, result.isAdmin, 'As user has proper PS/Profile, isAdmin flag should be false');
        System.assertEquals(expectedIsVendorTradesman, result.isVendorSupervisor, 'As user has proper PS/Profile, isVendorSupervisor flag should be true');
    }

    @IsTest
    static void getTradesmanInfo_tradesman() {
        VTT_TradesmanFlags result;
        User runningUser = VTT_TestData.createVTTUser();
        Boolean expectedIsAdmin = false;
        Boolean expectedIsVendorTradesman = false;

        System.runAs(runningUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Agent', 'Colson', vendor.Id, runningUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            tradesman.Current_Work_Order_Activity__c = activity.Id;
            update tradesman;

            Test.startTest();
            result = VTT_Utility_Endpoint.getTradesmanInfo();
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Should always return value');
        System.assertEquals(expectedIsAdmin, result.isAdmin, 'As user has proper PS/Profile, isAdmin flag should be false');
        System.assertEquals(expectedIsVendorTradesman, result.isVendorSupervisor, 'As user has proper PS/Profile, isVendorSupervisor flag should be false');
    }

}