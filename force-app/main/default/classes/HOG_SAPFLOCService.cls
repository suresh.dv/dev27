global with sharing class HOG_SAPFLOCService {

	//Floc to be inserted
	private static List<Business_Unit__c> businessUnitList;
	private static List<Operating_District__c> operatingDistrictList;
	private static List<Field__c> operatingFieldList;
	private static List<Route__c> operatorRouteList;
	private static List<Facility__c> superiorFacilityList;
	private static List<Facility__c> inferiorFacilityList;
	private static List<Location__c> superiorWellIdList;
	private static List<Location__c> inferiorWellIdList;
	private static List<Well_Event__c> wellEventList;
	private static List<System__c> systemList;
	private static List<Sub_System__c> subSystemList;
	private static List<Yard__c> yardList;
	private static List<Functional_Equipment_Level__c> inferiorFelList;
	private static List<Functional_Equipment_Level__c> superiorFelList;
	private static Set<String> routeNumbers;

	//tracking parents to avoid unusual floc
	//private static Set<String> facilitySAPObjectIds;
	private static Set<String> invalidSubSystemFunctionalLocations;

	// Existing System and Facilities FunctionalLoaction String for deciding on SubSystems Parent
	private static Set<String> existingFacilities;
	private static Set<String> existingSubSystems;
	private static Set<String> existingSystems;

	/************************* SOAP Interface ************************************/
	webservice static HOG_SAPApiUtils.HOG_SAPFLOCServiceResponse processFLOC (
		HOG_SAPApiUtils.DT_SAP_Functional_Location MT_SAP_FLOC) {

		initialize();
		HOG_SAPApiUtils.HOG_SAPFLOCServiceResponse response = processPayload(MT_SAP_FLOC.FLOC);

		return response;
	}
	/*****************************************************************************/

	private static void initialize() {
		businessUnitList = new List<Business_Unit__c>();
		operatingDistrictList = new List<Operating_District__c>();
		operatingFieldList = new List<Field__c>();
		operatorRouteList = new List<Route__c>();
		superiorFacilityList = new List<Facility__c>();
		inferiorFacilityList = new List<Facility__c>();
		superiorWellIdList = new List<Location__c>();
		inferiorWellIdList = new List<Location__c>();
		wellEventList = new List<Well_Event__c>();
		systemList = new List<System__c>();
		subSystemList = new List<Sub_System__c>();
		yardList = new List<Yard__c>();
		inferiorFelList = new List<Functional_Equipment_Level__c>();
		superiorFelList = new List<Functional_Equipment_Level__c>();
		routeNumbers = new Set<String>();

		invalidSubSystemFunctionalLocations = new Set<String>();

		existingSystems = getExistingSystems();
		existingSubSystems = getExistingSubSystems();
		existingFacilities = getExistingFacilities();
	}

	private static HOG_SAPApiUtils.HOG_SAPFLOCServiceResponse 
		processPayload(List<HOG_SAPApiUtils.DT_SAP_FUNC_LOC> flocPayload) {
		HOG_SAPApiUtils.HOG_SAPFLOCServiceResponse response = new HOG_SAPApiUtils.HOG_SAPFLOCServiceResponse();
		response.addResults(parseFlocIntoSObjects(flocPayload));
		response.addResults(upsertSObjects());
		return response;
	}

	private static List<HOG_SAPApiUtils.Result> parseFlocIntoSObjects(List<HOG_SAPApiUtils.DT_SAP_FUNC_LOC> flocPayload) {
		HOG_FLOCFactoryUtils.FunctionalLocationFactory flocFactory = 
			new HOG_FLOCFactoryUtils.FunctionalLocationFactory();
		List<HOG_SAPApiUtils.Result> results = new List<HOG_SAPApiUtils.Result>();

		//Parse into appropriate SObjects
		for(HOG_SAPApiUtils.DT_SAP_FUNC_LOC flocPayloadEntry : flocPayload) {
			
			//Handle non-hog floc and pipelines
			if(!flocPayloadEntry.RIHIFLO_LST.TPLNR.startsWithIgnoreCase('H1') || !flocPayloadEntry.RIHIFLO_LST.FLTYP.isNumeric()) {
				HOG_SAPApiUtils.Result errorResult = new HOG_SAPApiUtils.Result(false, 
					flocPayloadEntry.RIHIFLO_LST.TPLNR.toUpperCase(), false, HOG_SAPApiUtils.STATUS_ERROR_INVALID_RECORD);
				errorResult.addError(new HOG_SAPApiUtils.Error(null, 'Functional location not part of the H1 Hierarchy', 
					null));
				results.add(errorResult);
			    continue;//Skip to the next iteration, to process the payload entry into floc
			}

			try {
				// We will also add Set of Function_Location strings from existing Facility and also Systems so we can decide on 
				// SubSystems Parent
				// 
				// mbrimus fix 3.8.2017 to set System and Subsystem on Location but for FEL that have Sys or SubSystem parents TODO will have to change in 
				// floc phase 3
				HOG_FLOCFactoryUtils.FunctionalLocation funcLoc = flocFactory.getFloc(flocPayloadEntry, existingSystems, existingSubSystems, existingFacilities);

				if(funcLoc instanceof HOG_FLOCFactoryUtils.BusinessUnit) {
					System.debug('BusinessUnit');
					businessUnitList.add((Business_Unit__c)((HOG_FLOCFactoryUtils.BusinessUnit) funcLoc).getRecord());

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.OperatingDistrict) {
					System.debug('OperatingDistrict');
					operatingDistrictList.add((Operating_District__c)
						((HOG_FLOCFactoryUtils.OperatingDistrict) funcLoc).getRecord());

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.OperatingField) {
					System.debug('OperatingField');
					operatingFieldList.add((Field__c)((HOG_FLOCFactoryUtils.OperatingField) funcLoc).getRecord());

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.Facility) {
					System.debug('Facility');
					Facility__c facility = (Facility__c)((HOG_FLOCFactoryUtils.Facility) funcLoc).getRecord();
					if(facility.Superior_Facility__r == null) superiorFacilityList.add(facility);
					else inferiorFacilityList.add(facility);

					// Add new Facility objects to set
					existingFacilities.add(facility.Functional_Location__c);

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.WellId) {
					System.debug('WellId');
					Location__c wellId = ((Location__c)((HOG_FLOCFactoryUtils.WellId) funcLoc).getRecord());
					if(wellId.Well_ID__r == null) superiorWellIdList.add(wellId);
					else inferiorWellIdList.add(wellId);

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.WellEvent) {
					System.debug('WellEvent');
					wellEventList.add((Well_Event__c)((HOG_FLOCFactoryUtils.WellEvent) funcLoc).getRecord());

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.SystemFLOC) {
					System.debug('System');
					System__c newSystem = (System__c)((HOG_FLOCFactoryUtils.SystemFLOC) funcLoc).getRecord();

					systemList.add(newSystem);
					
					// Add new System objects to set
					existingSystems.add(newSystem.Functional_Location__c);

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.SubSystem) {
					System.debug('Sub-System');
					Sub_System__c newSubSystem = (Sub_System__c)((HOG_FLOCFactoryUtils.SubSystem) funcLoc).getRecord();
					subSystemList.add(newSubSystem);

					// Add new SubSystem objects to set
					existingSubSystems.add(newSubSystem.Functional_Location__c);

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.Yard) {
					System.debug('Yard');
					yardList.add((Yard__c)((HOG_FLOCFactoryUtils.Yard) funcLoc).getRecord());

				} else if(funcLoc instanceof HOG_FLOCFactoryUtils.FuntionalEquipmentLevel) {
					System.debug('FEL');
				
					Functional_Equipment_Level__c fel  = ((Functional_Equipment_Level__c)((HOG_FLOCFactoryUtils.FuntionalEquipmentLevel) funcLoc).getRecord());
					if(fel.Functional_Equipment_Level__c == null) superiorFelList.add(fel);
					else inferiorFelList.add(fel);


				} else {
					HOG_SAPApiUtils.Result errorResult = new HOG_SAPApiUtils.Result(false, flocPayloadEntry.RIHIFLO_LST.TPLNR,
						false, HOG_SAPApiUtils.STATUS_ERROR_PARSE);
					errorResult.addError(new HOG_SAPApiUtils.Error(null, 
						'The payload does not match a known functional location object', null));
					results.add(errorResult);
					continue; //Skip to the next iteration, to prevent adding routes for wrong flocs
				}

				//Add Route
				if(funcLoc.getRouteNumber() != null) routeNumbers.add(funcLoc.getRouteNumber());

				//Success case - not adding parsing successes
				//HOG_SAPApiUtils.Result successResult = new HOG_SAPApiUtils.Result(false, funcLoc.getFlocString(),
				//	true, HOG_SAPApiUtils.STATUS_SUCCESS_PARSE);
				//results.add(successResult);
			} catch (Exception ex) {
				HOG_SAPApiUtils.Result errorResult = new HOG_SAPApiUtils.Result(false, 
					flocPayloadEntry.RIHIFLO_LST.TPLNR.toUpperCase(), false, HOG_SAPApiUtils.STATUS_ERROR_PARSE);
				errorResult.addError(new HOG_SAPApiUtils.Error(null, ex.getMessage() + '\n' + ex.getStackTraceString(), null));
				results.add(errorResult);
			    System.Debug('errorResult: ' + errorResult);
			}
		}

		//Create Routes
		for(String routeNum : routeNumbers) {
			operatorRouteList.add(new Route__c(Name = routeNum, Route_Number__c = routeNum));
		}

		return results;
	}

	private static List<HOG_SAPApiUtils.Result> upsertSObjects() {
		List<SObject> flocRecords = new List<SObject>();
		List<HOG_SAPApiUtils.Result> results = new List<HOG_SAPApiUtils.Result>();
		List<Database.UpsertResult> upsertResults = new List<Database.UpsertResult>();
		
		System.debug('routeList: ' + operatorRouteList);
		System.debug('superiorFacilityList: ' + superiorFacilityList);
		System.debug('inferiorFacilityList: ' + inferiorFacilityList);
		System.debug('superiorWellIdList: ' + superiorWellIdList);
		System.debug('inferiorWellIdList: ' + inferiorWellIdList);
		System.debug('businessUnitList: ' + businessUnitList);
		System.debug('operatingDistrictList: ' + operatingDistrictList);
		System.debug('systemList: ' + systemList);
		System.debug('subSystemList: ' + subSystemList);
		System.debug('yardList: ' + yardList);
		 
		upsertResults.addAll(Database.upsert(businessUnitList, Business_Unit__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) businessUnitList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(operatingDistrictList, Operating_District__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) operatingDistrictList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(operatingFieldList, Field__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) operatingFieldList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(operatorRouteList, Route__c.fields.Route_Number__c, false));
		flocRecords.addAll((List<SObject>) operatorRouteList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(superiorFacilityList, Facility__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) superiorFacilityList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(inferiorFacilityList, Facility__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) inferiorFacilityList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(superiorWellIdList, Location__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) superiorWellIdList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(inferiorWellIdList, Location__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) inferiorWellIdList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(wellEventList, Well_Event__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) wellEventList); //to provide elevant information when consolidating results in the response

		// Marcel B changes 6.7.2017 Floc phase 2
		upsertResults.addAll(Database.upsert(systemList, System__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) systemList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(subSystemList, Sub_System__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) subSystemList); //to provide elevant information when consolidating results in the response

		// Marcel B changes 23.08.2017 Floc phase 3
		upsertResults.addAll(Database.upsert(yardList, Yard__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) yardList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(superiorFelList, Functional_Equipment_Level__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) superiorFelList); //to provide elevant information when consolidating results in the response
		upsertResults.addAll(Database.upsert(inferiorFelList, Functional_Equipment_Level__c.fields.SAP_Object_ID__c, false));
		flocRecords.addAll((List<SObject>) inferiorFelList); //to provide elevant information when consolidating results in the response

		//Consolidate into api resuls for response
		for(Integer i=0; i < upsertResults.size(); i++) {
			Database.UpsertResult upsertResult = upsertResults[i];
			String statusCode = upsertResult.isSuccess() ? HOG_SAPApiUtils.STATUS_SUCCESS_UPSERT : 
				HOG_SAPApiUtils.STATUS_ERROR_UPSERT;
			String idString = (String) ((flocRecords[i] instanceof Route__c) ? flocRecords[i].get('Route_Number__c') :
				flocRecords[i].get('Functional_Location__c'));
			HOG_SAPApiUtils.Result result = new HOG_SAPApiUtils.Result(upsertResult.isCreated(), idString, 
				upsertResult.isSuccess(), statusCode);
			for(Database.Error err : upsertResult.getErrors()) {
				result.addError(new HOG_SAPApiUtils.Error(err.getFields(), err.getMessage(), err.getStatusCode()));
			}
			results.add(result);
		}
		return results;
	}

	private static Set<String> getExistingFacilities() {
		Set<String> funlocations = new Set<String>();
		for(Facility__c facility : [Select Functional_Location__c From Facility__c]) {
			funlocations.add(facility.Functional_Location__c);
		}
		return funlocations;
	}

	private static Set<String> getExistingSystems() {
		Set<String> funlocations = new Set<String>();
		for(System__c  systems: [Select Functional_Location__c From System__c]) {
			funlocations.add(systems.Functional_Location__c);
		}
		return funlocations;
	}

	private static Set<String> getExistingSubSystems() {
		Set<String> funlocations = new Set<String>();
		for(Sub_System__c  subSystems: [Select Functional_Location__c From Sub_System__c]) {
			funlocations.add(subSystems.Functional_Location__c);
		}
		return funlocations;
	}
}