/**
 * Created by MarcelBrimus on 03/09/2019.
 */

public inherited sharing class VTT_TotalsModel {
    @AuraEnabled
    String TotalHoursText { get; set; }

    @AuraEnabled
    String TotalHoursOnEquipmentText { get; set; }

    @AuraEnabled
    String TotalHoursOffEquipmentText { get; set; }

    public VTT_TotalsModel() {
    }

    public VTT_TotalsModel(String TotalHoursText, String TotalHoursOnEquipmentText, String TotalHoursOffEquipmentText) {
        this.TotalHoursText = TotalHoursText;
        this.TotalHoursOnEquipmentText = TotalHoursOnEquipmentText;
        this.TotalHoursOffEquipmentText = TotalHoursOffEquipmentText;
    }
}