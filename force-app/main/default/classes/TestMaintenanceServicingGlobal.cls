@isTest
private class TestMaintenanceServicingGlobal
{
    private static HOG_Service_Code_MAT__c serviceCodeMAT;
    private static HOG_Work_Order_Type__c workOrderType;
    private static HOG_Notification_Type_Priority__c notificationTypePriority;
    private static HOG_Service_Category__c serviceCategory;
    private static HOG_Service_Code_Group__c serviceCodeGroup;
    private static HOG_Notification_Type__c notificationType;
    private static HOG_Service_Required__c serviceRequiredList;
	private static HOG_User_Status__c userStatus;
	private static HOG_Service_Priority__c servicePriorityList;
	private static Business_Unit__c businessUnit;
	private static Business_Department__c businessDepartment;
	private static Operating_District__c operatingDistrict;
	private static Field__c field;
	private static Route__c route;
	private static Location__c location;		
	private static Facility__c facility;		
	private static Equipment__c equipment;
	private static HOG_Part__c equipmentPart;
	private static HOG_Damage__c equipmentPartDamage;
	private static HOG_Cause__c equipmentPartDamageCause;
	private static Account account;
	private static Contact contact;
	private static HOG_Maintenance_Servicing_Form__c[] maintenanceRecords;
	private static HOG_Maintenance_Servicing_Vendor__c[] vendorRecords;
    private static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};


    @isTest    
    static void MaintenanceServicingGlobal_Test()
    {            
        MaintenanceServicingUtilities.executeTriggerCode = false;   // do not to execute trigger      

		SetupTestData();
                        
        Test.startTest();
   
        String returnMessage;

        // This causes a fake response to be generated        
        Test.setMock(WebServiceMock.class, new SAPHOGGetWorkOrderResponseMock());    
        returnMessage = MaintenanceServicingGlobal.UpdateWorkOrderUserStatus(maintenanceRecords[0].Work_Order_Number__c, '8FC');
        System.assertNotEquals(null, returnMessage);        
        
        // This causes a fake response to be generated        
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());        
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(maintenanceRecords[0].Id);
        System.assertNotEquals(null, returnMessage);        
 
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());                    
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(maintenanceRecords[0].Id);
        System.assertNotEquals(null, returnMessage);
        
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());                    
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(maintenanceRecords[1].Id);
        System.assertNotEquals(null, returnMessage);

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());                    
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(maintenanceRecords[2].Id);
        System.assertNotEquals(null, returnMessage);

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());                                    
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(maintenanceRecords[3].Id);
        System.assertNotEquals(null, returnMessage);

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());                                    
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(maintenanceRecords[4].Id);
        System.assertNotEquals(null, returnMessage);
        
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());                            
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(null);
        System.assertNotEquals(null, returnMessage);
        
        delete vendorRecords;
        
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());                            
        returnMessage = MaintenanceServicingGlobal.updateALTConfirmed(maintenanceRecords[0].Id);
        System.assertNotEquals(null, returnMessage);
        
		final HOG_Cause__c cause = MaintenanceServicingGlobal.GetCauseCodeRecord('TESTCODE');  
		System.assertEquals(true, String.isBlank(cause.Cause_Code__c));
        
        Test.stopTest();                
    }           
    
 	private static void SetupTestData()
	{
		String serviceCategoryCode = 'MNT';  // Maintenance service category code
		String catalogueCode = 'CATCODE';
		String partCode = 'PARTCODE';
	    String notificationTypeCode = 'WP';
	    String orderTypeCode = 'WP01';
	    String matCode = 'TXN';

		Id recordTypeId;

        //-- Begin setup of data needed for Service Request Notification --//                
        //-- Setup Service Category
        serviceCategory = new HOG_Service_Category__c
            (
                Name = 'Test Category',
                Service_Category_Code__c = serviceCategoryCode                                
            );
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;        
        //--

        //-- Setup Notification Type
        notificationType = new HOG_Notification_Type__c
            (
                HOG_Service_Category__c = serviceCategory.Id,
                Notification_Type__c = notificationTypeCode,
                Order_Type__c = orderTypeCode
            );
        insert notificationType;
        //--
  
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', matCode, 'Master'); 	    
        insert serviceCodeMAT;        
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', false, false, false);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
        notificationTypePriority = NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriorityList.Id, false);
        insert notificationTypePriority;        
        //--

        //-- Setup Work Order Type
 	    workOrderType = WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true);
        insert workOrderType;
        //--

        //-- Setup objects for Maintenance Work Order
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

		businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

		operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

		field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

		route = RouteTestData.createRoute('999');
        insert route;                  

		location = LocationTestData.createLocation('Test Location', route.Id, field.Id);		
        insert location;

		facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        

		account = AccountTestData.createAccount('Test Account', null);			
        insert account;

        contact = new Contact
            (
                AccountId = account.Id,
                LastName = 'Lastname',
                User__c = UserInfo.getUserId()
            );
        insert contact;    

        equipment = new Equipment__c
        	(
        		Name = 'Equipment Test',
        		Location__c = location.Id,     		
		        Catalogue_Code__c = catalogueCode
			);                
        insert equipment;

		equipmentPart = HOGPartTestData.createHOGPart('Test Part', partCode, 'Test Part', catalogueCode, true);
		insert equipmentPart;		

		equipmentPartDamage = HOGDamageTestData.createHOGDamage('Test Damage', 'DMGECODE', 'Test Damage', partCode, true);
		insert equipmentPartDamage;		

		equipmentPartDamageCause = HOGCauseTestData.createHOGCause('Test Cause', 'DMGECODE', 'Test Cause', 'CDEGROUP', true);
		insert equipmentPartDamageCause;
        //----

        //-- Setup HOG_Maintenance_Servicing_Form__c
        maintenanceRecords = new List<HOG_Maintenance_Servicing_Form__c>();
            
        final HOG_Maintenance_Servicing_Form__c record1 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name 1',
            Functional_Location__c = 'Test FLOC1',
            Work_Order_Number__c = 'W1111111',
            Notification_Number__c = 'N1111111',
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            Priority_Number__c = 1,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS1',
            Equipment__c = equipment.Id,
            Part__c = equipmentPart.id,
            Damage__c = equipmentPartDamage.id,
            Cause__c = equipmentPartDamageCause.id,
            Part_Key__c = '0001',
            HOG_Notification_Type__c = notificationType.Id
        );
        
        maintenanceRecords.add(record1);

        final HOG_Maintenance_Servicing_Form__c record2 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name 2',
            Functional_Location__c = 'Test FLOC2',
            Work_Order_Number__c = 'W2222222',
            Notification_Number__c = 'N2222222',
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            Priority_Number__c = 1,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = true,
            Plant_Section__c = 'PS2',
            Equipment__c = equipment.Id,
            Part__c = equipmentPart.id,
            Damage__c = equipmentPartDamage.id,
            Cause__c = equipmentPartDamageCause.id,
            Part_Key__c = '0002',
            HOG_Notification_Type__c = notificationType.Id
        );

        maintenanceRecords.add(record2);

        final HOG_Maintenance_Servicing_Form__c record3 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name 3',
            Functional_Location__c = 'Test FLOC3',
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            Priority_Number__c = 1,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS3',
            Equipment__c = equipment.Id,
            Part__c = equipmentPart.id,
            Damage__c = equipmentPartDamage.id,
            Cause__c = equipmentPartDamageCause.id,
            Part_Key__c = '0003',
            HOG_Notification_Type__c = notificationType.Id
        );

        maintenanceRecords.add(record3);

        final HOG_Maintenance_Servicing_Form__c record4 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name 4',
            Functional_Location__c = 'Test FLOC4',
            Work_Order_Number__c = 'W4444444',
            Notification_Number__c = 'N4444444',            
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            Priority_Number__c = 1,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS4',
            Equipment__c = equipment.Id,
            Part_Key__c = '0004',
            HOG_Notification_Type__c = notificationType.Id            
        );

        maintenanceRecords.add(record4);

        final HOG_Maintenance_Servicing_Form__c record5 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name 5',
            Functional_Location__c = 'Test FLOC5',
            Work_Order_Number__c = 'W5555555',
            Notification_Number__c = 'N5555555',            
            Notification_Type__c = notificationTypeCode,
            Order_Type__c = orderTypeCode,
            Priority_Number__c = 1,
            MAT_Code__c = matCode,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS5',
            Equipment__c = equipment.Id,
            Part_Key__c = '0005',
            HOG_Notification_Type__c = notificationType.Id            
        );

        maintenanceRecords.add(record5);
        
        insert maintenanceRecords;

        vendorRecords = new List<HOG_Maintenance_Servicing_Vendor__c>();

        final HOG_Maintenance_Servicing_Vendor__c vendorRecord1 = new HOG_Maintenance_Servicing_Vendor__c
        (
            Name = 'Test Name 1',
            Maintenance_Servicing_Form__c = maintenanceRecords[0].Id,
            Activity_Status__c = 'Complete',
            Total_Travel_Times_Hrs__c = 1,
            Total_Personnel__c = 1,
            Start_Time__c = System.now(),
            Stop_Time__c = System.now(),
            Vendor__c = account.Id,
            Completed_By_Contact__c = contact.Id,
            Comments__c = 'Test Comment1'            
        );

        vendorRecords.add(vendorRecord1);

        final HOG_Maintenance_Servicing_Vendor__c vendorRecord2 = new HOG_Maintenance_Servicing_Vendor__c
        (
            Name = 'Test Name 2',
            Maintenance_Servicing_Form__c = maintenanceRecords[1].Id,
            Activity_Status__c = 'Complete',
            Total_Travel_Times_Hrs__c = 1,
            Total_Personnel__c = 1,
            Start_Time__c = System.now(),
            Stop_Time__c = System.now(),
            Vendor__c = account.Id,
            Completed_By_Contact__c = contact.Id,
            Comments__c = 'Test Comment2'
        );

        vendorRecords.add(vendorRecord2);

        final HOG_Maintenance_Servicing_Vendor__c vendorRecord3 = new HOG_Maintenance_Servicing_Vendor__c
        (
            Name = 'Test Name 3',
            Maintenance_Servicing_Form__c = maintenanceRecords[2].Id,
            Activity_Status__c = 'Complete',
            Total_Travel_Times_Hrs__c = 1,
            Total_Personnel__c = 1,
            Start_Time__c = System.now(),
            Stop_Time__c = System.now(),
            Vendor__c = account.Id,
            Completed_By_Contact__c = contact.Id,
            Comments__c = 'Test Comment3'
        );

        vendorRecords.add(vendorRecord3);

        final HOG_Maintenance_Servicing_Vendor__c vendorRecord4 = new HOG_Maintenance_Servicing_Vendor__c
        (
            Name = 'Test Name 4',
            Maintenance_Servicing_Form__c = maintenanceRecords[3].Id,
            Activity_Status__c = 'Awaiting on parts',
            Total_Travel_Times_Hrs__c = 1,
            Total_Personnel__c = 1,
            Start_Time__c = System.now(),
            Stop_Time__c = System.now(),
            Vendor__c = account.Id,
            Completed_By_Contact__c = contact.Id,
            Comments__c = 'Test Comment4'
        );

        vendorRecords.add(vendorRecord4);

        insert vendorRecords;
		//---
	}
}