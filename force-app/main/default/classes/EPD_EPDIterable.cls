/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    EPD Iterable for EPD_EmailBatch
Test Class:     EPD_EPDIterableTest
History:        jschn 2019-07-19 - Created. - EPD R1
*************************************************************************************************/
global inherited sharing class EPD_EPDIterable implements Iterable<EPD_Engine_Performance_Data__c> {

    private List<EPD_Engine_Performance_Data__c> epdRecords;

    global EPD_EPDIterable(List<EPD_Engine_Performance_Data__c> epdRecords) {
        this.epdRecords = epdRecords;
    }

    global Iterator<EPD_Engine_Performance_Data__c> iterator(){
        return new EPD_EPDIterator(epdRecords);
    }

}