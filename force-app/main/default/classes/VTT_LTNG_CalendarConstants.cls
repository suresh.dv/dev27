/**
 * Created by MarcelBrimus on 03/09/2019.
 */

public with sharing class VTT_LTNG_CalendarConstants {
    public static final String DTFORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
}