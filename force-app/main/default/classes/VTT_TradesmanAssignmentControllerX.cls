public without sharing class VTT_TradesmanAssignmentControllerX {

	private final Work_Order_Activity__c woActivity;
    public SelectOption[] selectedTradesmen { get; set; }
    public SelectOption[] allTradesmen { get; set; }    
    public SelectOption[] vendorAccounts { get; set; }
    public ID assignedVendor {get; set;}
    public Boolean assignedVendorDisabled {get; private set;}
    public Contact Tradesman {get; private set;}
    
    //Check for SF1 app context
    public Boolean isSalesforce1 {get; set;}
    public ID woId {get; set;}
    
	public VTT_TradesmanAssignmentControllerX(ApexPages.StandardController stdController) {
        this.woActivity = (Work_Order_Activity__c)stdController.getRecord();
        
        isSalesforce1 = VTT_Utilities.IsSalesforce1User();
        woId = this.woActivity.Id; 
        
        VendorAccountsSetup();
        assignedVendor = this.woActivity.Assigned_Vendor__c; 
        TradesmanAssignmentSetup();        		

		Tradesman = VTT_Utilities.LoadTradesmanInfo();
		assignedVendorDisabled = false;
		if(VTT_Utilities.IsVendorSupervisor() 
            && Tradesman.accountid <> null 
            && assignedVendor == Tradesman.accountid)
		{
			assignedVendorDisabled = true;
		} 

        if(this.woActivity.Status__c == VTT_Utilities.ACTIVITY_STATUS_COMPLETED) 
        {
            assignedVendorDisabled = true;  
            String message = 'You can not change assignment for completed activity.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, message));              
        }      
	}

    public PageReference UpdateAssignment() {

        //first delete everything from assignment table

        List<Work_Order_Activity_Assignment__c> currentAssignments =  [
                SELECT Tradesman__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Rejected__c <> TRUE
                AND Work_Order_Activity__c = :woActivity.Id
        ];

        List<Id> currentlyAssignedTradesmenIds = HOG_GeneralUtilities.getListOfParentIds(currentAssignments, 'Tradesman__c');
        List<Id> tradesmenSelectedAgain = new List<Id>();

        List<Work_Order_Activity_Assignment__c> assignmentListToCreate = new List<Work_Order_Activity_Assignment__c>();

        for ( SelectOption so : selectedTradesmen ) {
            Work_Order_Activity_Assignment__c assignmentRecord = new Work_Order_Activity_Assignment__c();
            assignmentRecord.Tradesman__c = so.getValue();
            assignmentRecord.Work_Order_Activity__c = this.woActivity.Id;
            if(!currentlyAssignedTradesmenIds.contains(assignmentRecord.Tradesman__c)) {
                assignmentListToCreate.add(assignmentRecord);
            } else {
                tradesmenSelectedAgain.add(assignmentRecord.Tradesman__c);
            }
        }
        List<Work_Order_Activity_Assignment__c> filteredAssignmentListToDelete = new List<Work_Order_Activity_Assignment__c>();

        for(Work_Order_Activity_Assignment__c assignment : currentAssignments) {
            if(!tradesmenSelectedAgain.contains(assignment.Tradesman__c)) {
                filteredAssignmentListToDelete.add(assignment);
            }
        }

        this.woActivity.Assigned_Vendor__c = this.assignedVendor;

        final Savepoint savePoint = Database.setSavepoint();
        try
        {
            delete filteredAssignmentListToDelete;
            insert assignmentListToCreate;
            upsert this.woActivity;
        }
        catch (Exception e)
        {
            Database.rollback(savePoint);
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            return null;
        }

        PageReference pageRef = new PageReference('/apex/VTT_ActivityDetailTimeLog?id=' + this.woActivity.id);
        pageRef.setRedirect(true);

        //Check if user is using sf1 web app if so then we will use different navigation on frontent
        //and return null
        if(isSalesforce1){
            return null;
        }else{
            return pageRef;
        }
    }

    public PageReference ExitAssignment()
    {

        PageReference pageRef = new PageReference('/apex/VTT_ActivityDetailTimeLog?id=' + this.woActivity.id);
        pageRef.setRedirect(true);
        return pageRef;  
    }

    private void  VendorAccountsSetup()
    {

        List<Account> accountList =VTT_Utilities.GetVendorAccountList();

        vendorAccounts = new List<SelectOption>();
        for(Account acc: accountList)
        {
            SelectOption accOption = new SelectOption(acc.id, acc.Name);
            vendorAccounts.add(accOption);
        }
    }

    public void  TradesmanAssignmentSetup()
    {
        selectedTradesmen = new List<SelectOption>();
        allTradesmen  = new List<SelectOption>();

        //assignedVendor; 
		Set<ID> assignedContactSet = new Set<ID>();


        if(assignedVendor ==this.woActivity.Assigned_Vendor__c)
        {

	        for(Work_Order_Activity_Assignment__c woaa : [select id, Tradesman__c from Work_Order_Activity_Assignment__c where Rejected__c <> true and Work_Order_Activity__c = :woActivity.id])
	        {
	            assignedContactSet.add(woaa.Tradesman__c);
	        }

	        for(Contact tradesmancontact : [select id, name, Phone, email, Tradesman_Status__c, Current_Work_Order_Activity__c, Tradesman_Status_Date__c, User__c 
	                                        from Contact where id in :assignedContactSet order by name])
	        {
	            SelectOption accOption = new SelectOption(tradesmancontact.id, tradesmancontact.Name);
	            selectedTradesmen.add(accOption);
	        }        	
        }


        List<Contact> contactList = [select id, name from Contact 
        where AccountID = :assignedVendor 
        and user__c <> null
        order by name limit 1000];

        for(Contact contactRec: contactList)
        {

        	if(!assignedContactSet.contains(contactRec.id))
        	{
	            SelectOption accOption = new SelectOption(contactRec.id, contactRec.Name);
	            allTradesmen.add(accOption);        		
        	}

        }  




    }

}