/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:
 *  Test Class		ELG_ListView_EndpointTest
 *  History:        Created on 8/27/2019
 */

public class ELG_ListView_Endpoint {

	//All AMUs
	@AuraEnabled
	public static Field__c getUserDefaultAMU() {
		return ELG_ListView_Service.getDefaultAMU();
	}

	//All Posts based on selected AMU
	@AuraEnabled
	public static List<ELG_ListView_Service.TemporaryClassToBuildObject> getAMUsPosts(String currentAMU) {
		return ELG_ListView_Service.getAvailablePosts(currentAMU);
	}

	//
	@AuraEnabled
	public static List<ELG_ListView_Service.DataTableWrapper> getSearchResults(
			Object filterSelection,
			Integer offsetSize,
			String fieldSort,
			String sortDirection,
			Boolean notReviewedBySE,
			Boolean notReviewedBySteamChief,
			Boolean notReviewedByShiftLead,
			Boolean notReviewedBySupervisor) {
		return ELG_ListView_Service.searchResults(
				filterSelection,
				offsetSize,
				fieldSort,
				sortDirection,
				notReviewedBySE,
				notReviewedBySteamChief,
				notReviewedByShiftLead,
				notReviewedBySupervisor);
	}

	@AuraEnabled
	public static List<ELG_ListView_Service.DataTableShiftSummaryWrapper> getShiftSummarySearchResults(
			Object filterSelection,
			Integer offsetSize,
			String fieldSort,
			String sortDirection) {

		return ELG_ListView_Service.searchShiftSummaryResults(filterSelection, offsetSize, fieldSort, sortDirection);

	}

	@AuraEnabled
	public static HOG_CustomResponseImpl massHandoverReviewersUpdate(
			List<String> listOfIds,
			String currentUser,
			Boolean supervisorReviews,
			Boolean steamChiefReviews,
			Boolean shiftEngineerReviews,
			Boolean shiftLeadReviews) {

		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response.addResult(ELG_ListView_Service.massHandoverReviewersUpdate(
					listOfIds,
					currentUser,
					supervisorReviews,
					steamChiefReviews,
					shiftEngineerReviews,
					shiftLeadReviews));

		} catch (Exception ex) {
			response.addError(ex);
		}
		return response;

	}
}