global class UpdateStausCancel implements database.batchable<sobject>, database.stateful
{
    String recordsUpdated = '';
    
    // start method of batch which collecting data 
    global database.querylocator start( database.batchablecontext bc ){
        DateTime sixWeeksDate = DateTime.now().addDays( -42 );
        //DateTime sixWeeksDate = DateTime.now();

        // fetching all header list whre created date is greater than 6 weeks and change status is new
        String chglist  = 'Select Id, Name, Request_Created_Date__c, Request_Title__c, Change_Status__c, Object_Name__c, '
                            + 'Change_Type__c from PREP_Change_Header__c where Change_Status__c = \'New\' AND CreatedDate <=: sixWeeksDate';
        system.debug('** '+database.getquerylocator(chglist ));

        return database.getquerylocator(chglist );
    }

    // execute method of batch which will update the header list change status
    global void  execute( database.batchablecontext bc, list<PREP_Change_Header__c> scope ){
        list<PREP_Change_Header__c> headerlist = new list<PREP_Change_Header__c>();   
        
        // iterating header lis and update the  change status field logic
        for( PREP_Change_Header__c ch : scope ){
            ch.Change_Status__c ='Cancelled'; 
            System.debug('aaaa'+ch.Change_Status__c);
            
            recordsUpdated += '<tr><td>' + ch.Name + '</td><td>' + ch.Object_Name__c + '</td><td>'
                                + ch.Request_Title__c + '</td><td>' + ch.Change_Type__c
                                + '</td><td>' + ch.Request_Created_Date__c + '</td></tr>';

            headerlist.add(ch);
        }

        // update scope;
        update headerlist;
    }

    global void finish( database.batchablecontext bc ){
        String tableBody = 'Following records are 6 weeks earlier and updated successfully: <br/>';
        tableBody += '<table border="1" style="width:100%"><tr><td><b>Name</b></td><td><b>Object Name</b></td>'
                        + '<td><b>Request Title<b></td><td><b>Change Type</b></td><td>'
                        + '<b>Request Created Date</b></td></tr>';
        tableBody += recordsUpdated;
        tableBody += '</table>';
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses( new List<String>{ 'Daniel.Lord@huskyenergy.com' });
        //mail.setToAddresses( new List<String>{ 'neetu.a.bansal@accenture.com' });
        mail.setSubject( 'Request Cancel on ' + Date.today() );
        
        if( recordsUpdated != '' )
            mail.setHtmlBody( tableBody );
        else
            mail.setPlainTextBody( 'There are no records which are 6 weeks earlier from today!!!' );
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}