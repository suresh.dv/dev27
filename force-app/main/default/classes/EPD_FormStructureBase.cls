/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Base class for Form Structures. It's just abstract class that pre-defines 2 methods
Test Class:     Tested with any extension test case
History:        jschn 2019-06-25 - Created. - EPD R1
*************************************************************************************************/
public abstract class EPD_FormStructureBase {

    public abstract SObject getRecord();
    public abstract EPD_FormStructureBase setRecord(SObject obj);

}