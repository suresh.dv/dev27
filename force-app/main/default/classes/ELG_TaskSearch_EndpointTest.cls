/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 11/6/2019   
 */

@IsTest
public class ELG_TaskSearch_EndpointTest {

	/*POSITIVE TESTS*/
	@IsTest
	static void getAMUPostsTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);

		Test.startTest();
		List<ELG_TaskSearch_EndpointService.TemporaryClassToBuildObject> result = ELG_TaskSearch_Endpoint.getAMUsPosts(post.Operating_Field_AMU__c);
		System.assert(!result.isEmpty(), 'Creating object or query failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(post.Id, result[0].value, 'Value pass to Object failed (constructor)');
		System.assertEquals(post.Post_Name__c, result[0].label, 'Value pass to Object failed (constructor)');
		Test.stopTest();
	}

	@IsTest
	static void searchOnlyByAMUTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Task__c task = ELG_TestDataFactory.createPostTask(post.Id, 'Test1', 'User text', true);
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];
		task.Operating_Field_AMU__c = post.Operating_Field_AMU__c;
		update task;

		Test.startTest();
		ELG_TaskSearch_EndpointService.FilterWrapper taskSearchParams = new ELG_TaskSearch_EndpointService.FilterWrapper();
		taskSearchParams.amuId = post.Operating_Field_AMU__c;


		List<ELG_TaskSearch_EndpointService.DataTableWrapper> result = ELG_TaskSearch_Endpoint.getSearchResults(
				taskSearchParams,
				0,
				'CreatedDate',
				'DESC',
				true,
				false,
				false);
		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(task.Id, result[0].taskId, 'Value pass to object failure');
		System.assertEquals(task.Short_Description__c, result[0].shortDescription, 'Value pass to object failure');
		Test.stopTest();
	}

	@IsTest
	static void searchByMoreParametersTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Task__c task = ELG_TestDataFactory.createPostTask(post.Id, 'Test1', 'User text', true);

		Test.startTest();
		ELG_TaskSearch_EndpointService.FilterWrapper taskSearchParams = new ELG_TaskSearch_EndpointService.FilterWrapper();
		String startDate = String.valueOf(Date.today() - 10);
		String endDate = String.valueOf(Date.today());


		taskSearchParams.postId = post.Id;
		taskSearchParams.taskStatus = ELG_Constants.TASK_ACTIVE;
		taskSearchParams.startDate = startDate;
		taskSearchParams.endDate = endDate;

		List<ELG_TaskSearch_EndpointService.DataTableWrapper> result = ELG_TaskSearch_Endpoint.getSearchResults(
				taskSearchParams,
				0,
				'CreatedDate',
				'DESC',
				true,
				false,
				false);

		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(task.Id, result[0].taskId, 'Value pass to object failure');
		System.assertEquals(ELG_Constants.TASK_ACTIVE, result[0].taskStatus, 'Value pass to object failure');
		Test.stopTest();

	}

	@IsTest
	static void tooManyRecordsForOffsetTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createPostTask(post.Id, 'Test1', 'User text', true);
		List<ELG_Task__c> taskList = [SELECT Id FROM ELG_Task__c];

		Test.startTest();
		Integer numberOfTasks = 2501;
		List<ELG_TaskSearch_EndpointService.DataTableWrapper> result = ELG_TaskSearch_EndpointService.objectBuilder(taskList, numberOfTasks);
		System.assertEquals(true, result[0].hasError, 'Value pass to object failure');
		System.assertEquals(ELG_Constants.maximumQueryLimit('Tasks', numberOfTasks), result[0].errorMsg, 'Value pass to object failure');
		Test.stopTest();
	}

	@IsTest
	static void searchAllTasksWithDifferentStatusesTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		ELG_TestDataFactory.createTasksWithDifferentStatuses(
				post.Operating_Field_AMU__c,
				post.Id,
				'Test',
				'Task Description',
				3,
				3,
				3,
				usr.Id,
				Datetime.now().addDays(1),
				true);

		Test.startTest();
		ELG_TaskSearch_EndpointService.FilterWrapper taskSearchParams = new ELG_TaskSearch_EndpointService.FilterWrapper();
		taskSearchParams.amuId = post.Operating_Field_AMU__c;

		List<ELG_TaskSearch_EndpointService.DataTableWrapper> result = ELG_TaskSearch_Endpoint.getSearchResults(
				taskSearchParams,
				0,
				'CreatedDate',
				'DESC',
				true,
				false,
				false);

		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(9, result.size(), 'Creating object or query failure');
		Test.stopTest();
	}

	@IsTest
	static void updateTaskToCompleteAlsoTriggerTest() {
		ELG_Utilities.executeTrigger.ELG_Task_Trigger__c = true;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Task__c task = ELG_TestDataFactory.createPostTask(post.Id, 'Test1', 'User text', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];

		task.Operating_Field_AMU__c = post.Operating_Field_AMU__c;
		task.Shift_Assignement__c = shift.Id;
		task.Finished_By__c = usr.Id;
		task.Finished_At__c = Datetime.now().addDays(1);
		task.Status__c = ELG_Constants.TASK_COMPLETED;
		task.Comment__c = 'I am a comment';
		update task;

		Test.startTest();

		ELG_TaskSearch_EndpointService.FilterWrapper taskSearchParams = new ELG_TaskSearch_EndpointService.FilterWrapper();
		taskSearchParams.amuId = post.Operating_Field_AMU__c;
		taskSearchParams.taskStatus = ELG_Constants.TASK_COMPLETED;

		List<ELG_TaskSearch_EndpointService.DataTableWrapper> result = ELG_TaskSearch_Endpoint.getSearchResults(
				taskSearchParams,
				0,
				'CreatedDate',
				'DESC',
				false,
				true,
				false);
		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');

		ELG_Log_Entry__c createdLogByTrigger = [
				SELECT
						Id,
						User__c,
						Post__c,
						Shift_Assignement__c,
						User_Log_Entry__c,
						Operating_Field_AMU__c
				FROM ELG_Log_Entry__c
		];

		task = [
				SELECT
						Id,
						Name,
						Post_Name__c,
						Short_Description__c,
						Task_Description__c,
						Comment__c,
						Log_Entry__c,
						Operating_Field_AMU__c
				FROM ELG_Task__c
				WHERE Status__c = :ELG_Constants.TASK_COMPLETED
		];

		String userLogEntry = '<a href="/'
				+ task.Id
				+ '" target="_blank">'
				+ task.Name
				+ ' - '
				+ task.Post_Name__c
				+ '</a>'
				+ ' has been Completed.'
				+ '\n\nShort Description:\n'
				+ task.Short_Description__c
				+ '\n\nTask Description:\n'
				+ task.Task_Description__c +
				'\n\nTask Comment: ' + task.Comment__c;

		System.assertNotEquals(null, createdLogByTrigger, 'Trigger failed to insert log');
		System.assertEquals(usr.Id, createdLogByTrigger.User__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(post.Id, createdLogByTrigger.Post__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(post.Operating_Field_AMU__c, createdLogByTrigger.Operating_Field_AMU__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(shift.Id, createdLogByTrigger.Shift_Assignement__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(userLogEntry, createdLogByTrigger.User_Log_Entry__c, 'Trigger failed to build user entry.');
		Test.stopTest();
	}

	@IsTest
	static void updateTaskToCancelAlsoTriggerTest() {
		ELG_Utilities.executeTrigger.ELG_Task_Trigger__c = true;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Task__c task = ELG_TestDataFactory.createPostTask(post.Id, 'Test1', 'User text', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];

		task.Operating_Field_AMU__c = post.Operating_Field_AMU__c;
		task.Shift_Assignement__c = shift.Id;
		task.Finished_By__c = usr.Id;
		task.Finished_At__c = Datetime.now().addDays(1);
		task.Status__c = ELG_Constants.TASK_CANCELLED;
		task.Reason__c = 'I am a reason';
		update task;

		Test.startTest();

		ELG_TaskSearch_EndpointService.FilterWrapper taskSearchParams = new ELG_TaskSearch_EndpointService.FilterWrapper();
		taskSearchParams.amuId = post.Operating_Field_AMU__c;
		taskSearchParams.taskStatus = ELG_Constants.TASK_CANCELLED;

		List<ELG_TaskSearch_EndpointService.DataTableWrapper> result = ELG_TaskSearch_Endpoint.getSearchResults(
				taskSearchParams,
				0,
				'CreatedDate',
				'DESC',
				false,
				false,
				true);
		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');

		ELG_Log_Entry__c createdLogByTrigger = [
				SELECT
						Id,
						User__c,
						Post__c,
						Shift_Assignement__c,
						User_Log_Entry__c,
						Operating_Field_AMU__c
				FROM ELG_Log_Entry__c
		];

		task = [
				SELECT
						Id,
						Name,
						Post_Name__c,
						Short_Description__c,
						Task_Description__c,
						Reason__c,
						Log_Entry__c,
						Operating_Field_AMU__c
				FROM ELG_Task__c
				WHERE Status__c = :ELG_Constants.TASK_CANCELLED
		];

		String userLogEntry = '<a href="/'
				+ task.Id
				+ '" target="_blank">'
				+ task.Name
				+ ' - '
				+ task.Post_Name__c
				+ '</a>'
				+ ' has been Cancelled.'
				+ '\n\nShort Description:\n'
				+ task.Short_Description__c
				+ '\n\nTask Description:\n'
				+ task.Task_Description__c
				+ '\n\nReason for Cancellation is: '
				+ task.Reason__c;

		System.assertNotEquals(null, createdLogByTrigger, 'Trigger failed to insert log');
		System.assertEquals(usr.Id, createdLogByTrigger.User__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(post.Id, createdLogByTrigger.Post__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(post.Operating_Field_AMU__c, createdLogByTrigger.Operating_Field_AMU__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(shift.Id, createdLogByTrigger.Shift_Assignement__c, 'Trigger failed to pass value to Log.');
		System.assertEquals(userLogEntry, createdLogByTrigger.User_Log_Entry__c, 'Trigger failed to build user entry.');
		Test.stopTest();
	}


	/*NEGATIVE TESTS*/

	@IsTest
	static void findNoTaskTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createPostTask(post.Id, 'Test1', 'User text', true);
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

		Test.startTest();
		ELG_TaskSearch_EndpointService.FilterWrapper taskSearchParams = new ELG_TaskSearch_EndpointService.FilterWrapper();
		taskSearchParams.amuId = amu.Id;


		List<ELG_TaskSearch_EndpointService.DataTableWrapper> result = ELG_TaskSearch_Endpoint.getSearchResults(
				taskSearchParams,
				0,
				'CreatedDate',
				'DESC',
				true,
				false,
				false);
		System.assert(!result.isEmpty(), 'Creating object, query or string builder failure. List is Empty');
		System.assertEquals(1, result.size(), 'Creating object or query failure');
		System.assertEquals(true, result[0].hasError, 'Value pass to object failure');
		System.assertEquals(ELG_Constants.NO_TASK_FOUND, result[0].errorMsg, 'Value pass to object failure');
		Test.stopTest();
	}

	//TODO negative test to find nothing, completed and cancelled tasks search

	/*Test Data*/
	@TestSetup
	static void testData() {
		ELG_TestDataFactory.createReviewerAndLogEntryUsers();

	}


}