@isTest
private class SPCC_POControllerXTest {


    @testSetup 
    static void setup() {

            //Create a SPCC Data
            User epcUser = SPCC_TestData.createEPCUser();
            User pcUser = SPCC_TestData.createPCUser();

            Test.startTest();

                System.runAs(pcUser){

                    Account spccAccount = SPCC_TestData.createSPCCVendor('SPCC Vendor');
                    insert spccAccount;

                    Account epcAccount = SPCC_TestData.createEPCVendor('EPC Vendor');
                    insert epcAccount;

                    Contact epcContact = SPCC_TestData.createContact(epcAccount.Id, 'EPC', 'Acc', epcUser.Id);
                    insert epcContact;

                    SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('Test Project', '12345', '', 'Facilities');
                    insert ewr;

                    SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('12-123A56-12-AB-12', '12-123A56-12-AB-12', 'Test AFE', ewr.Id);
                    insert afe;

                    SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, null, 'Test GL Code', '0123456', 'Service', 30, 200);
                    insert costElement;

                    SPCC_Cost_Code__c costCode = SPCC_TestData.createCostCode(costElement.Id, '9999', 'Test Codt Code', 300);
                    insert costCode;

                    SPCC_Vendor_Account_Assignment__c vaa2 = SPCC_TestData.createVAA(ewr.Id, spccAccount.Id);
                    insert vaa2;

                    SPCC_Purchase_Order__c po2 = SPCC_TestData.createPO('0000000001', ewr.ID, afe.Id, spccAccount.Id, 
                                                                        'test EPC PO', 'In Progress');
                    insert po2;
                    SPCC_Purchase_Order__c po3 = SPCC_TestData.createPO('0000000002', ewr.ID, afe.Id, spccAccount.Id, 
                                                                        'test EPC PO 2', 'In Progress');
                    insert po3;

            Test.stopTest();
        }
    }


    @isTest 
    static void testSaveWithError() {

        Test.startTest();

            //Set Page
            PageReference pref = Page.SPCC_POCreate;
            Test.setCurrentPage(pref);
            System.currentPageReference().getParameters();

            //Create Controller     
            ApexPages.StandardController sController = new ApexPages.StandardController(GetPOData()[0]);
            SPCC_POControllerX poController = new SPCC_POControllerX(sController);
            //Try to save po without gl & cost code to check validation is working (testing Save button)
            poController.savePO();
            System.assertEquals(null, poController.savePO());

            Set<String> errMsgSet = new Set<String>();
            for(ApexPages.Message msg :  ApexPages.getMessages()) {

                errMsgSet.add(msg.getSummary());
            }
            System.assertEquals(true, errMsgSet.contains('No line items added to the Purchase Order. At least one line item is required.'));

        Test.stopTest();
    }


    @isTest 
    static void testSaveAndNewWithError() {

        Test.startTest();

            //Set Page
            PageReference pref = Page.SPCC_POCreate;
            Test.setCurrentPage(pref);
            System.currentPageReference().getParameters();

            //Create Controller     
            ApexPages.StandardController sController = new ApexPages.StandardController(GetPOData()[0]);
            SPCC_POControllerX poController = new SPCC_POControllerX(sController);
            //Try to save po without gl & cost code to check validation is working (testing Save & New button)
            poController.saveAndNew();
            System.assertEquals(null, poController.savePO());

            Set<String> errMsgSet = new Set<String>();
            for(ApexPages.Message msg :  ApexPages.getMessages()) {

                errMsgSet.add(msg.getSummary());
            }
            System.assertEquals(true, errMsgSet.contains('No line items added to the Purchase Order. At least one line item is required.'));

        Test.stopTest();
    }


    @isTest 
    static void testSaveWithoutError() {

        Test.startTest();

            //Set Page
            PageReference pref = Page.SPCC_POCreate;
            Test.setCurrentPage(pref);
            System.currentPageReference().getParameters();
            SPCC_Purchase_Order__c newPO = new SPCC_Purchase_Order__c();

            //Create Controller     
            ApexPages.StandardController sController = new ApexPages.StandardController(newPO);
            SPCC_POControllerX poController = new SPCC_POControllerX(sController);

            SPCC_Purchase_Order__c poRetrieved = [SELECT Id, Name, Engineering_Work_Request__c, 
                                                         Authorization_for_Expenditure__c, Vendor_Account__c
                                                  FROM SPCC_Purchase_Order__c 
                                                  WHERE Name = '0000000002' 
                                                  LIMIT 1];

            SPCC_Cost_Element__c costElement = [Select Id, Name
                                                From SPCC_Cost_Element__c 
                                                Limit 1];

            //Add PO Information
            poController.po.Name = '0000000003';
            poController.po.Engineering_Work_Request__c = poRetrieved.Engineering_Work_Request__c;
            poController.po.Authorization_for_Expenditure__c = poRetrieved.Authorization_for_Expenditure__c;
            poController.po.Vendor_Account__c = poRetrieved.Vendor_Account__c;
            poController.po.Description__c = 'Test PO 3';
            poController.po.Status__c = 'In Progress';

            //Add Line Item
            System.assertEquals(null, poController.addLineItem());
            System.assertEquals(poController.lineItems[0].lineItem.Item__c, null);
            poController.lineItems[0].lineItem.Item__c = '0010';
            poController.lineItems[0].lineItem.Cost_Element__c = costElement.Id;
            poController.lineItems[0].lineItem.Short_Text__c = 'Test Line Item 1';
            poController.lineItems[0].lineItem.Committed_Amount__c = 100;

            System.assertEquals(2,GetPOData().Size());
            //Try to save valid PO (testing Save button)
            poController.savePO();

            System.assertEquals(poController.po.Name, GetPOData()[2].Name);
            System.assertEquals(3,GetPOData().Size());                                

        Test.stopTest();
    }


    @isTest 
    static void testSaveAndNewWithoutError() {

        Test.startTest();

            //Set Page
            PageReference pref = Page.SPCC_POCreate;
            Test.setCurrentPage(pref);
            SPCC_Purchase_Order__c newPO = new SPCC_Purchase_Order__c();

            //Create Controller     
            ApexPages.StandardController sController = new ApexPages.StandardController(newPO);
            SPCC_POControllerX poController = new SPCC_POControllerX(sController);

            SPCC_Purchase_Order__c poRetrieved = [SELECT Id, Name, Engineering_Work_Request__c, 
                                                         Authorization_for_Expenditure__c, Vendor_Account__c
                                                  FROM SPCC_Purchase_Order__c 
                                                  WHERE Name = '0000000002' 
                                                  LIMIT 1];

            SPCC_Cost_Element__c costElement = [Select Id, Name
                                                From SPCC_Cost_Element__c 
                                                Limit 1];

            //Add PO Information
            poController.po.Name = '0000000003';
            poController.po.Engineering_Work_Request__c = poRetrieved.Engineering_Work_Request__c;
            poController.po.Authorization_for_Expenditure__c = poRetrieved.Authorization_for_Expenditure__c;
            poController.po.Vendor_Account__c = poRetrieved.Vendor_Account__c;
            poController.po.Description__c = 'Test PO 3';
            poController.po.Status__c = 'In Progress';

            //Add Line Item
            System.assertEquals(null, poController.addLineItem());
            System.assertEquals(poController.lineItems[0].lineItem.Item__c, null);
            poController.lineItems[0].lineItem.Item__c = '0010';
            poController.lineItems[0].lineItem.Cost_Element__c = costElement.Id;
            poController.lineItems[0].lineItem.Short_Text__c = 'Test Line Item 1';
            System.assertEquals(2, poController.glOptions.size());
            System.assertEquals(costElement.Id, poController.glOptions[1].getValue());
            poController.lineItems[0].lineItem.Committed_Amount__c = 100;

            //Remove line Item
            System.currentPageReference().getParameters().put('newLIIndex', '0');
            System.assertEquals(null, poController.removeLineItem());
            System.assert(poController.lineItems.isEmpty());

            //Add Line Item again, because can't create PO without line items
            System.assertEquals(null, poController.addLineItem());
            System.assertEquals(poController.lineItems[0].lineItem.Item__c, null);
            poController.lineItems[0].lineItem.Item__c = '0010';
            poController.lineItems[0].lineItem.Cost_Element__c = costElement.Id;
            poController.lineItems[0].lineItem.Short_Text__c = 'Test Line Item 1';
            System.assertEquals(2, poController.glOptions.size());
            System.assertEquals(costElement.Id, poController.glOptions[1].getValue());
            poController.lineItems[0].lineItem.Committed_Amount__c = 100;

            System.assertEquals(2,GetPOData().Size());
            //Try to save valid PO (testing Save & New button)
            poController.saveAndNew();

            System.assertEquals(poController.po.Name, GetPOData()[2].Name);
            System.assertEquals(3,GetPOData().Size());                                

        Test.stopTest();
    }


    @isTest
    static void testPicklistsFunction(){

        List<SPCC_Purchase_Order__c> poList = GetPOData();
        List<SPCC_Authorization_for_Expenditure__c> afeList = GetAFEData();
        List<SPCC_Engineering_Work_Request__c> ewrlist = GetEWRData();
        SPCC_Purchase_Order__c po = poList[1];
        User testPCUser = GetPCUser();

        Test.startTest();

            System.runAs(testPCUser){

                PageReference pref = Page.SPCC_POCreate;
                Test.setCurrentPage(pref);
                System.currentPageReference().getParameters().put('id', po.Id);

                //Create controller
                ApexPages.StandardController sController = new ApexPages.StandardController(po);
                SPCC_POControllerX poExtension = new SPCC_POControllerX(sController);

                //Testing load of related picklist values for EWR / AFE / GLs / Cost Codes and Vendor Accounts
                System.assert(poExtension.getEWRs() != null );
                System.assert(poExtension.getAFEs() != null );
                System.assert(poExtension.getVendorAccounts() != null );

                System.assertEquals(poExtension.getIsEPCOnly(), false);

                //Test for null-ing dependent picklist values / fields when controlled picklist is nulled 
                poExtension.updateDropDowns();
                System.assertEquals(poExtension.po.Engineering_Work_Request__c, ewrlist[0].Id);
                System.assertEquals(poExtension.po.Authorization_for_Expenditure__c, afelist[0].Id);

                poExtension.updateDropDowns();
                System.assert(poExtension.po.Cost_Code__c == null);

                poExtension.po.Authorization_for_Expenditure__c = null;
                poExtension.updateDropDowns();

                poExtension.po.Engineering_Work_Request__c = null;
                poExtension.updateDropDowns();
                System.assert(poExtension.po.Authorization_for_Expenditure__c == null);

        Test.stopTest();
        }
    }


    @isTest
    static void testPoOpenFromAfePage(){

        List<SPCC_Purchase_Order__c> poList = GetPOData();
        List<SPCC_Authorization_for_Expenditure__c> afeList = GetAFEData();
        List<SPCC_Engineering_Work_Request__c> ewrlist = GetEWRData();
        SPCC_Purchase_Order__c po = poList[0];
        User testPCUser = GetPCUser();

            Test.startTest();

                System.runAs(testPCUser){

                //Set page
                //test of setting url parameters with related parent's Ids
                PageReference pref = Page.SPCC_POCreate;
                Test.setCurrentPage(pref);

                //Create controller
                ApexPages.StandardController sController = new ApexPages.StandardController(po);
                SPCC_POControllerX poExtension = new SPCC_POControllerX(sController);

                System.assert(poExtension.createdFromAFE);
                System.assertEquals(poExtension.getIsEPCOnly(), false);

            Test.stopTest();
            }
        }


    @isTest 
    static void testViewPOasEpcUser(){

        List<SPCC_Purchase_Order__c> poList = GetPOData();
        List<SPCC_Authorization_for_Expenditure__c> afeList = GetAFEData();
        List<SPCC_Engineering_Work_Request__c> ewrlist = GetEWRData();
        SPCC_Purchase_Order__c po = poList[0];
        User testEPCUser = GetEPCUser();

        Test.startTest();

            System.runAs(testEPCUser){

                PageReference pref = Page.SPCC_POCreate;
                Test.setCurrentPage(pref);
                System.currentPageReference().getParameters().put('id', po.Id);

        	    ApexPages.StandardController sController = new ApexPages.StandardController(po);
                SPCC_POControllerX poExtension = new SPCC_POControllerX(sController);

                System.assertEquals(poExtension.getIsEPCOnly(), true);

                System.assert(poExtension.getVendorAccounts() != null );
            }
        Test.stopTest();
    }


	//////////////////////////////////////////////
    //* DATA RETRIEVAL FUNCTIONS FOR TEST DATA *//
    //////////////////////////////////////////////

    private static List<SPCC_Purchase_Order__c> GetPOData(){

    	return [SELECT Name, Engineering_Work_Request__c, Authorization_for_Expenditure__c, Vendor_Account__c,
    				   Cost_Code__c, Purchase_Order_Number__c, Description__c, Status__c
    			FROM SPCC_Purchase_Order__c];
    }


    private static List<SPCC_Engineering_Work_Request__c> GetEWRData(){

    	return [SELECT Project_Name__c, Name, EWR_Number__c 
    			FROM SPCC_Engineering_Work_Request__c];
    }

    private static List<SPCC_Authorization_for_Expenditure__c> GetAFEData(){

    	return [SELECT Name, AFE_Number__c, Description__c, Engineering_Work_Request__c 
    			FROM SPCC_Authorization_for_Expenditure__c];
    }

    private static User GetPCUser(){

    	return [SELECT UserName, Email 
    			FROM User 
    			WHERE LastName =: 'User_PC' LIMIT 1];
    }

    private static User GetEPCUser(){

    	return [SELECT UserName, Email 
    			FROM User 
    			WHERE LastName = 'User_EPC' LIMIT 1];
    }
}