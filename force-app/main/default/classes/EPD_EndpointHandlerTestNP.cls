/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non parallel unit test for EPD_EndpointHandler
History:        jschn 2019-07-26 - Created.
*************************************************************************************************/
@IsTest
private class EPD_EndpointHandlerTestNP {

    @IsTest
    static void handleSave_save() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();

        Test.startTest();
        //TODO after LTNG migration replace with JSON.serialize(formStructure)
        response = new EPD_EndpointHandler().handleSave(formStructure.toString(), EPD_SaveMode.SAVE);
        Test.stopTest();

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    @IsTest
    static void handleSave_submit() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        Integer initialCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer initialCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer initialCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer initialCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer initialCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer initialCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        EPD_FormStructure formStructure = EPD_TestData.prepareStructure();
        EPD_DAOProvider.plannerGroupNotificationTypeDAO = new HPNTMock();

        Test.startTest();
        //TODO after LTNG migration replace with JSON.serialize(formStructure)
        response = new EPD_EndpointHandler().handleSave(formStructure.toString(), EPD_SaveMode.SUBMIT);
        Test.stopTest();

        Integer finalCountEPD = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer finalCountEOC = [SELECT COUNT() FROM EPD_Engine_Operating_Condition__c];
        Integer finalCountCOC = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer finalCountBI = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer finalCountCI = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        Integer finalCountPart = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
        System.assertEquals(initialCountEPD+1, finalCountEPD);
        System.assertEquals(initialCountEOC+1, finalCountEOC);
        System.assertEquals(initialCountCOC+1, finalCountCOC);
        System.assertEquals(initialCountBI+1, finalCountBI);
        System.assertEquals(initialCountCI+1, finalCountCI);
        System.assertEquals(initialCountPart+1, finalCountPart);
    }

    public class HPNTMock implements HOG_PlannerGroupNotificationTypeDAO {
        Contact contact = (Contact) HOG_SObjectFactory.createSObject(new Contact(), true);
        public List<HOG_Planner_Group_Notification_Type__c> getNotificationTypesForRoleTypes(List<String> roleTypes) {
            return new List<HOG_Planner_Group_Notification_Type__c> {
                    new HOG_Planner_Group_Notification_Type__c(
                            HOG_Planner_Group__r = new HOG_Planner_Group__c(Name = '100'),
                            Role_Type__c = 'Tradesman Lead',
                            Work_Center__c = 'Test',
                            Contact__c = contact.Id
                    )
            };
        }
    }

    @IsTest
    static void handleLoad_direct() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(epd.Id, EPD_LoadMode.DIRECT_LOAD);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void handleLoad_direct_withoutRecord() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(UserInfo.getUserId(), EPD_LoadMode.DIRECT_LOAD);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assert(response.errors.get(0).contains(EPD_Constants.UNEXPECTED_NUM_OF_RECS_BY_LOAD_MODE.get(EPD_LoadMode.DIRECT_LOAD)));
        System.assertEquals(null, response.formStructure);
    }

    @IsTest
    static void handleLoad_indirect() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        epd.Submitted__c = false;
        update epd;

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(epd.Work_Order_Activity__c, EPD_LoadMode.INDIRECT_LOAD);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

    @IsTest
    static void handleLoad_indirect_submitted() {
        Boolean expectedSuccessFlag = false;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(epd.Work_Order_Activity__c, EPD_LoadMode.INDIRECT_LOAD);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assert(response.errors.get(0).contains('was already submitted'));
        System.assertEquals(null, response.formStructure);
    }

    @IsTest
    static void handleLoad_indirect_withoutEPDRec() {
        Boolean expectedSuccessFlag = true;
        EPD_Response response;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        Id woaId = epd.Work_Order_Activity__c;
        delete epd;

        Test.startTest();
        response = new EPD_EndpointHandler().handleLoad(woaId, EPD_LoadMode.INDIRECT_LOAD);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(null, response.errors);
        System.assertNotEquals(null, response.formStructure);
    }

}