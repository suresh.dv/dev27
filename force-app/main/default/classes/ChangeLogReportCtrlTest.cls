@isTest
private class ChangeLogReportCtrlTest {
    private testmethod static void defaultMethods(){
        CR_Impacted_Functions__c impact = new CR_Impacted_Functions__c( Name = 'Test' );
        insert impact;
        
        Id uid = UserInfo.getUserId();
        
        Project_Area__c proj = new Project_Area__c( Name = 'Test', SPA__c = uid, Project_Manager__c = uid,
                                                    Change_Coordinator__c = uid );
        insert proj;
        
        Change_Request__c chq = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id, Contractor__c = uid, PCN_Number__c = '123456' );
        insert chq;
        
        test.startTest();
            ChangeLogReportCtrl controller = new ChangeLogReportCtrl();
            controller.getController();
            String selectedArea = controller.selectedArea;
            String selectedChangeStatus = controller.selectedChangeStatus;
            Boolean bolPrintableView = controller.bolPrintableView;
            controller.init();
            List<SelectOption> opt1 = controller.programOptions;
            List<SelectOption> opt2 = controller.ProjectOptions;
            List<SelectOption> opt3 = controller.AreaOptions;
            List<SelectOption> opt4 = controller.ContractorOptions;
            List<SelectOption> opt5 = controller.PONumberOptions;
            
            controller.programOptions = new List<SelectOption>();
            controller.ProjectOptions = new List<SelectOption>();
            controller.AreaOptions = new List<SelectOption>();
            controller.ContractorOptions = new List<SelectOption>();
            controller.PONumberOptions = new List<SelectOption>();
            String xmlHeader = controller.xlsHeader;
            controller.exportDetail();
            controller.RunReport();
            controller.SortField = null;
            controller.SortDirection = null;
            controller.SortToggle();
            controller.savecR();
            //controller.showExport = true;
            controller.preExport(true);
            controller.exportHeader();
            controller.exportDetails();
            controller.exportAll();
            
        test.stopTest();
    }
}