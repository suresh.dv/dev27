/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/25/2019   
 */

public class ELG_Task_EndpointService {

	public HOG_CustomResponseImpl getTask(String taskId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		ELG_Task__c task = ELG_DAOProvider.taskDAO.getTask(taskId);

		response.addResult(task);
		return response;
	}

	public HOG_CustomResponseImpl getPostTasks(String postId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<ELG_Task__c> tasks = ELG_DAOProvider.taskDAO.getActiveTasks(postId);

		response.addResult(tasks);
		return response;
	}

	public HOG_CustomResponseImpl getAMUPosts(String amuId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<TemporaryPostPayload> payload = new List<TemporaryPostPayload>();
		List<ELG_Post__c> posts = new List<ELG_Post__c>();

		if (!String.isBlank(amuId)) {
			posts = ELG_DAOProvider.taskDAO.getAMUPosts(amuId);
			if (!posts.isEmpty()) {
				for (ELG_Post__c post : posts) {
					if (post.Shift_Assignements__r.size() > 0) {
						payload.add(new TemporaryPostPayload(post.Post_Name__c, post.Id));
					}
				}
				response.addResult(payload);
			} else {
				response.addError('There is no Post setup for this Facility, please contact HOGLloydSF@huskyenergy.com.');
			}
		} else {
			response.addError('Please click on Set Default AMU button and select Thermal AMU to Create Task');
		}


		return response;
	}

	public HOG_CustomResponseImpl insertTask(String postId, String shortDescription, String taskDescription) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		List<ELG_User_Setting__c> userSettings = new ELG_ShiftHandoverSelector().getUsersSettings();
		ELG_Task__c task = new ELG_Task__c(
				Post__c = postId,
				Status__c = ELG_Constants.TASK_ACTIVE,
				Short_Description__c = shortDescription,
				Task_Description__c = taskDescription,
				Operating_Field_AMU__c = userSettings[0].Operating_Field_AMU__c);

		insert task;

		response.addResult(task);
		return response;
	}

	public HOG_CustomResponseImpl updateTask(ELG_Task__c newValueTask) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Task__c task = ELG_DAOProvider.taskDAO.getTask(newValueTask.Id);

		if (task.Status__c == ELG_Constants.TASK_ACTIVE) {
			update newValueTask;
			response.addResult(newValueTask);
		} else {
			response.addError('Update Failed. Task already has been Closed.');
		}

		return response;
	}

	public HOG_CustomResponseImpl updateTaskComplete(String taskId, String userId, String userComment) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		ELG_Task__c task = ELG_DAOProvider.taskDAO.getTask(taskId);
		ELG_Shift_Assignement__c shift = ELG_DAOProvider.taskDAO.getActiveShift(task.Post__c);

		if (task.Status__c == ELG_Constants.TASK_ACTIVE) {
			task.Shift_Assignement__c = shift.Id;
			task.Comment__c = userComment;
			task.Status__c = ELG_Constants.TASK_COMPLETED;
			task.Finished_By__c = userId;
			task.Finished_At__c = Datetime.now();
			update task;
			response.addResult(task);
		} else {
			response.addError('Update Failed. Task already has been Closed.');
		}

		return response;
	}

	public HOG_CustomResponseImpl updateTaskCancelled(String taskId, String userId, String cancellationReason) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		ELG_Task__c task = ELG_DAOProvider.taskDAO.getTask(taskId);
		ELG_Shift_Assignement__c shift = ELG_DAOProvider.taskDAO.getActiveShift(task.Post__c);

		if (task.Status__c == ELG_Constants.TASK_ACTIVE) {
			task.Shift_Assignement__c = shift.Id;
			task.Reason__c = cancellationReason;
			task.Status__c = ELG_Constants.TASK_CANCELLED;
			task.Finished_By__c = userId;
			task.Finished_At__c = Datetime.now();
			update task;
			response.addResult(task);
		} else {
			response.addError('Update Failed. Task already has been Closed.');
		}

		return response;
	}

	public class TemporaryPostPayload {
		@AuraEnabled
		public String label;
		@AuraEnabled
		public String value;

		public TemporaryPostPayload(String postName, String postId) {
			this.label = postName;
			this.value = postId;
		}
	}

}