/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Service Request Notification Endpoint
Test Class:     SNR_EndpointTest
History:        mbrimus 2020-01-08 - Created.
TODO missing large portions of WELL TRACKER, RECCURING JOB AND CREATING WORK ORDERS logic
 that will have to be implement in second rounds of migration when well trackers are being migrated
*************************************************************************************************/
public with sharing class SNR_Endpoint {
    private static final String CLASS_NAME = String.valueOf(SNR_Endpoint.class);

    /**
     * Endpoint for loading initial state of picklists for creating SNR
     *
     * @param recordId      - Floc ID (Well Location, Facility, Equipment, Well Event, System, SubSystem, Yard)
     * @param objectName    - API Name of object where SNR is being created
     *
     * @return              - SNR_Response
     */
    @AuraEnabled
    public static SNR_Response loadSNRData(String recordId, String objectName) {
        SNR_Response response = new SNR_Response();
        try {
            response = new SNR_Service().loadInitialValues(recordId, objectName);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' loadSNRData exception ' + ex.getMessage());
            response.addError(ex);
        }
        System.debug(CLASS_NAME + ' loadSNRData Response ' + response);
        System.debug(CLASS_NAME + ' loadSNRData recordId ' + recordId);
        System.debug(CLASS_NAME + ' loadSNRData objectName ' + objectName);
        return response;
    }

    /**
     * Endpoint for reloading data when user selects specific Service Type
     *
     * @param serviceCategoryId     - Selected Service Category (Grouped Active HOG_Notification_Type__c on HOG_Service_Category__c,
     *                                serviceCategoryId is Id of HOG_Service_Category__c)
     *
     * @param serviceTypeId         - Selected Service Type (Grouped Active HOG_Work_Order_Type__c on HOG_Notification_Type__c
     *                                having HOG_Notification_Type__r.HOG_Service_Category__c = selected serviceCategoryId,
     *                                serviceTypeId is Id of HOG_Notification_Type__c)
     *
     * @param serviceActivityId     - Selected Service Activity (Grouped Active HOG_Work_Order_Type__c on HOG_User_Status__c
     *                                having HOG_Notification_Type__r.HOG_Service_Category__c = selected serviceCategoryId AND
     *                                HOG_Notification_Type__c = serviceTypeId
     *                                serviceActivityId is Id of HOG_User_Status__c
     *                                (As of 22.01.2020 there is only 2 activities in use, one for each Service Category)
     *
     * @param serviceRequiredId     - Selected Service Required (Grouped Active HOG_Work_Order_Type__c on  HOG_Service_Required__c
     *                                having HOG_Notification_Type__r.HOG_Service_Category__c = selected serviceCategoryId AND
                                      HOG_Notification_Type__c = serviceTypeId
                                      serviceRequiredId is ID of HOG_Service_Required__c

     * @param recordId              - Floc ID (Well Location, Facility, Equipment, Well Event, System, SubSystem, Yard)
     *
     * @param objectName            - API Name of object where SNR is being created
     *
     * @return                      - SNR_Response
     */
    @AuraEnabled
    public static SNR_Response loadServiceSpecifics(
            String serviceCategoryId,
            String serviceTypeId,
            String serviceActivityId,
            String serviceRequiredId,
            String recordId,
            String objectName) {
        SNR_Response response = new SNR_Response();

        try {
            response = new SNR_Service().loadServiceSpecifics(
                    serviceCategoryId,
                    serviceTypeId,
                    serviceActivityId,
                    serviceRequiredId,
                    recordId,
                    objectName);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' loadSNRData exception ' + ex.getMessage());
            response.addError(ex);
        }
        System.debug(CLASS_NAME + ' loadServiceSpecifics Response ' + response);
        System.debug(CLASS_NAME + ' loadServiceSpecifics recordId ' + recordId);
        System.debug(CLASS_NAME + ' loadServiceSpecifics objectName ' + objectName);
        return response;
    }

    /**
     * Endpoint for saving SNR
     * Used to pass raw data to handler which takes care of the logic
     *
     * @param objectName    - API Name of object where SNR is being created
     * @param snrRequest    - Input data from User that is mapped to new Service Request notification
     * @param flocModel     - Model data that is passed to view on Init from loadSNRData endpoint
     *
     * @return
     */
    @AuraEnabled
    public static SNR_Response save(
            String objectName,
            String snrRequest,
            String flocModel) {
        return new SNR_EndpointHandler().handleSave(objectName, snrRequest, flocModel);
    }
}