/*  ENHC0019414  
    This should be run on system context so that taxonomy admin can see all taxonomy records and intiatives and update them
*/
public with sharing class PREPTaxonomyChangeController{
    public PREPChangeWrapper prepChange = new PREPChangeWrapper();
    
    public PREPTaxonomyChangeController(ApexPages.StandardController controller){
    
    }
    
    public PREPChangeWrapper getPrepChange(){
        //get the URL parameter
        String changeNum = ApexPages.currentPage().getParameters().get('change');
        
        //get change header information
        PREP_Change_Header__c h = [SELECT Id, Name, Approval_Status__c, Change_Status__c, Change_Type__c, Object_Name__c, Reason__c FROM PREP_Change_Header__c 
                WHERE Name = :changeNum];
        
        //get change details
         List<PREP_Change_Detail__c> d = [ SELECT Id, Name,PREP_Change_Header__r.ID,Field_Name__c,From_Field_value__c,
                                             Record_Id__c,To_Field_value__c,Update_Record_Name__c, PREP_Change_Header__c,
                                             Update_Record_Description__c, From_Number__c, To_Number__c, From_Field__c, To_Field__c, 
                                             To_Field_Id__c FROM PREP_Change_Detail__c
                                             WHERE PREP_Change_Header__c = :h.Id ORDER BY Update_Record_Name__c ];   
        
        prepChange.userJeff = false;
        if( h != null && h.Change_Type__c == 'Taxonomy Created' && d.size() > 0 ){
            List<Material_Service_Group_Clone__c> matClone = [ Select Id, Name, Material_Service_Group_Name__c, SAP_Short_Text_Name__c, Sub_Category__c,
                                                Active__c, Category_Link__c, Type__c, Discipline__c, Category_Manager__c, Category_Specialist__c,
                                                GL_Account__c, GL_Account__r.Name, GL_Account_Number__c, Includes__c, Does_Not_Include__c, Additional_Comments__c from Material_Service_Group_Clone__c
                                                where Id = :d[0].To_Field_Id__c ];
        
            if( matClone.size() > 0 ){
                prepChange.taxonomyClone = matClone[0];
                
                Set<String> userEmails = new Set<String>{ 'jeff.sardinha@huskyenergy.com' };
                User u = [ Select Id, Name, Email from User where Id = :UserInfo.getUserId() ];
                
                if( u != null && userEmails.contains( u.Email ))
                    prepChange.userJeff = true;
            }
        }                
        
        prepChange.header = h;     
        prepChange.detail = d;        
        
        return prepChange;
    }
    
    public PageReference saveGLChanges(){
        update prepChange.taxonomyClone;
        
        //Redirect page to Category Taxonomy Request page
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect(true);
                
        return rageRef;
    }
    
    public PageReference cancelRequest(){
        //get the URL parameter
        String changeNum = ApexPages.currentPage().getParameters().get('change');

        //get change header information
        PREP_Change_Header__c h = [SELECT Id, Name, Change_Status__c FROM PREP_Change_Header__c WHERE Name = :changeNum];        
        h.Change_Status__c = 'Cancelled';
        
        //update status of the header record
        update h;
        
        //Redirect page to Category Taxonomy Request page
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect(true);
                
        return rageRef;
    }
    
    public PageReference submitForApproval(){
        String recordId = ApexPages.currentPage().getParameters().get('Id');
        User user1 = [SELECT Id FROM User WHERE Alias='lordda'];
        System.debug('User#####'+user1.id);
        //create an approval request
        Approval.ProcessSubmitRequest request = new Approval.ProcessSubmitRequest();
        request.setObjectId(recordId);
        request.setSubmitterId(user1.Id); 
        request.setSubmitterId(UserInfo.getUserId());
        request.setProcessDefinitionNameOrId( 'PREP_Taxonomy_Change_Approval' );
        request.setSkipEntryCriteria(true);
        if(!test.isRunningTest()){
        Approval.ProcessResult result = Approval.process(request);
        
         System.assert(result.isSuccess());
        }
         //System.debug('Submitted for approval successfully: '+result.isSuccess());
        //Redirect page to Category Taxonomy Request page
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect(true);
                
        return rageRef;
    }
    
    public PageReference commitChanges(){
        String recordId = ApexPages.currentPage().getParameters().get('id');
        PREP_Change_Header__c h = [SELECT Id, Name, Change_Status__c,Change_Type__c,Object_Name__c FROM PREP_Change_Header__c WHERE Id = :recordId];
        if(h.Change_Status__c == 'Approved'){
            List<PREP_Change_Detail__c> details = [ SELECT Id,Name,Record_Id__c,To_Field_Id__c,Update_Record_Name__c,Field_Name__c, Related_Field_Id__c, From_Field__c, To_Field__c, To_Field_value__c
                                                     FROM PREP_Change_Detail__c WHERE PREP_Change_Header__c = :h.Id];

            if( details.size() > 0 ){
                if(h.Object_Name__c == 'PREP_Initiative__c'){
                    commitIniatiativeChanges(h, details);
                }
                
                if(h.Object_Name__c == 'PREP_prjinitiative__c'){
                    commitprjIniatiativeChanges(h, details);
                }
                
                if(h.Object_Name__c == 'PREP_Material_Service_Group__c'){
                    commitTaxonomyChanges(h, details);
                }
            }
        
        }     
        
        h.Change_Status__c = 'Completed';
        update h;    
        
    
        //Redirect page to Category Taxonomy Request page
        PageReference rageRef = Page.Taxonomy_Requests;
        rageRef.setRedirect(true);
                
        return rageRef;    
    }
    
    private void commitIniatiativeChanges(PREP_Change_Header__c header, List<PREP_Change_Detail__c> details){
        List<PREP_Initiative__c> updatedList = new List<PREP_Initiative__c>();
        Map<String,PREP_Initiative__c> updateMap = new Map<String,PREP_Initiative__c>();
        
        //get alll PREP_Initiative__c records to avoid govener limits
        List<PREP_Initiative__c> iniList = [SELECT Id,Name FROM PREP_Initiative__c];
        
        //This is not the best logic and need to re-visit to this logic again
        for(PREP_Change_Detail__c d : details){
            for(PREP_Initiative__c i : iniList){
                if(i.Name == d.Update_Record_Name__c){
                
                    if(updateMap.containsKey(d.Update_Record_Name__c)){
                        i = updateMap.get(d.Update_Record_Name__c);
                        updateMap.remove(d.Update_Record_Name__c);
                    }
                    if(d.Field_Name__c == 'Owner' ){
                        i.OwnerId= d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'CategoryManager'){
                        i.Category_Manager__c = d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'CategorySpecialist'){
                        i.Category_Specialist__c = d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'SCMManager'){
                        i.SCM_Manager__c = d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'BusinessAnalyst'){
                        i.Business_Analyst__c = d.To_Field_Id__c;
                    } 
                    if(d.Field_Name__c == 'CategoryTechnician'){
                        i.Technician__c = d.To_Field_Id__c;
                    }   
                    if(d.Field_Name__c == 'GSO'){
                        i.GSO__c = d.To_Field_Id__c;
                    }   
                    if( d.Field_Name__c == 'Category' ){
                        i.Category__c = d.To_Field_Id__c;
                        i.Sub_Category__c = d.Related_Field_Id__c;
                    }
                    /*if( d.Field_Name__c == 'Sub Category' ){
                        i.Category__c = d.Related_Field_Id__c;
                        i.Sub_Category__c = d.To_Field_Id__c;
                    }*/
                    if( d.Field_Name__c == 'Discipline' ){
                        i.Discipline__c = d.To_Field_Id__c;
                    }
                    
                    //updatedList.add(i);                        
                    updateMap.put(d.Update_Record_Name__c,i);
                }
            }
        }
        
        updatedList = updateMap.values();

        
        update updatedList;        
    }
    private void commitprjIniatiativeChanges(PREP_Change_Header__c header, List<PREP_Change_Detail__c> details){
        List<PREP_Initiative__c> updatedList = new List<PREP_Initiative__c>();
        Map<String,PREP_Initiative__c> updateMap = new Map<String,PREP_Initiative__c>();
        
        //get alll PREP_Initiative__c records to avoid govener limits
        List<PREP_Initiative__c> iniList = [SELECT Id,Name FROM PREP_Initiative__c];
        
        //This is not the best logic and need to re-visit to this logic again
        for(PREP_Change_Detail__c d : details){
            for(PREP_Initiative__c i : iniList){
                if(i.Name == d.Update_Record_Name__c){
                
                    if(updateMap.containsKey(d.Update_Record_Name__c)){
                        i = updateMap.get(d.Update_Record_Name__c);
                        updateMap.remove(d.Update_Record_Name__c);
                    }
                    if(d.Field_Name__c == 'Owner'){
                        i.OwnerId= d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'ProcurementRepresentative'){
                        i.Procurement_Representative__c = d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'CommercialRepresentative'){
                        i.Commercial_Representative__c = d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'SCMManager'){
                        i.SCM_Manager__c = d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'BusinessAnalyst'){
                        i.Business_Analyst__c = d.To_Field_Id__c;
                    } 
                    if(d.Field_Name__c == 'ProjectLead'){
                        i.Project_Lead__c = d.To_Field_Id__c;
                    } 
                    if( d.Field_Name__c == 'Category' ){
                        i.Category__c = d.To_Field_Id__c;
                        i.Sub_Category__c = d.Related_Field_Id__c;
                    }
                    /*if( d.Field_Name__c == 'Sub Category' ){
                        i.Category__c = d.Related_Field_Id__c;
                        i.Sub_Category__c = d.To_Field_Id__c;
                    }*/
                    if( d.Field_Name__c == 'Discipline' ){
                        i.Discipline__c = d.To_Field_Id__c;
                    }                   
                                                                 
                    
                    //updatedList.add(i);                        
                    updateMap.put(d.Update_Record_Name__c,i);
                }
            }
        }
        
        updatedList = updateMap.values();

         update updatedList; 
             
    }

    
    private void commitTaxonomyChanges(PREP_Change_Header__c header, List<PREP_Change_Detail__c> details){
        List<PREP_Material_Service_Group__c> updatedList = new List<PREP_Material_Service_Group__c>();
        Map<String,PREP_Material_Service_Group__c> updateMap = new Map<String,PREP_Material_Service_Group__c>();
        
        //get alll PREP_Initiative__c records to avoid govener limits
        List<PREP_Material_Service_Group__c> iniList = [SELECT Id,Name FROM PREP_Material_Service_Group__c];
        
        List<PREP_Material_Service_Group__c> materialGroupList = new List<PREP_Material_Service_Group__c>();
        
        //This is not the best logic and need to re-visit to this logic again
        for(PREP_Change_Detail__c d : details){
            for(PREP_Material_Service_Group__c i : iniList){
                if(i.Name == d.Update_Record_Name__c){
                
                    if(updateMap.containsKey(d.Update_Record_Name__c)){
                        i = updateMap.get(d.Update_Record_Name__c);
                        updateMap.remove(d.Update_Record_Name__c);
                    }
                    if(d.Field_Name__c == 'Owner'){
                        i.OwnerId= d.To_Field_Id__c;
                    }
                    
                    if(d.Field_Name__c == 'CategoryManager'){
                        i.Category_Manager__c = d.To_Field_Id__c;
                    }
                    if(d.Field_Name__c == 'CategorySpecialist'){
                        i.Category_Specialist__c = d.To_Field_Id__c;
                    }
                    if( d.Field_Name__c == 'Category' ){
                        i.Category_Link__c = d.To_Field_Id__c;
                    }
                    if( d.Field_Name__c == 'Sub Category' ){
                        i.Sub_Category__c = d.To_Field_Id__c;
                    }
                    if( d.Field_Name__c == 'Discipline' ){
                        i.Discipline_Link__c = d.To_Field_Id__c;
                    }
                    if( d.Field_Name__c == 'Short Text' ){
                        i.SAP_Short_Text_Name__c = d.To_Field_Id__c.toUpperCase();
                    }
                    if( d.Field_Name__c == 'Long Text' ){
                        i.Material_Service_Group_Name__c = d.To_Field_Id__c;
                    }
                    if( d.Field_Name__c == 'GL Account' ){
                        i.GL_Account__c = d.To_Field_Id__c;
                    }
                    if( d.Field_Name__c == 'Include' ){
                        i.Includes__c = d.To_Field__c;
                    }
                    if( d.Field_Name__c == 'Does not Include' ){
                        i.Does_Not_Include__c = d.To_Field__c;
                    }
                    if( d.Field_Name__c == 'Active' ){
                        i.Active__c = false;
                        i.Retire_Reason__c = d.To_Field_value__c;
                        sendRetireEmail( i );
                    }
                    
                    //updatedList.add(i);                        
                    updateMap.put(d.Update_Record_Name__c,i);
                }
            }
            
            if( d.Field_Name__c == 'Taxonomy' ){
                PREP_Material_Service_Group__c mat = createTaxonomyRecord( d );
                if( mat != null )
                    materialGroupList.add( mat );
            }
        }
        
        updatedList = updateMap.values();

        if( materialGroupList.size() > 0 )
            insert materialGroupList;
            
        update updatedList;        
    }
    
    private void sendRetireEmail( PREP_Material_Service_Group__c taxo ) {
        List<String> toAddreses = new List<String>{ 'daniel.lord@huskyenergy.com', 'Victor.lam@huskyenergy.com', 'Jeff.Sardinha@huskyenergy.com', 'Raman.dhillon@huskyenergy.com',  'mm.governance@huskyenergy.com', 'Stephanie.Forbes@huskyenergy.com' };
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.toAddresses = toAddreses;
        email.subject = taxo.Name + ' Material/Service Group is retire!';
        
        String mailBody = '<html>Hello Team,<br/><br/>This is to inform you that Material Service Group '
                          + taxo.Name + ' is retired and it is no longer available.<br/><br/>Thanks!</html>';
        email.setHtmlBody( mailBody );
        
        Messaging.SingleEmailMessage[] emails = new List<Messaging.SingleEmailMessage>{ email };
        Messaging.SendEmailResult[] results = Messaging.sendEmail( emails );
    }
    
    private PREP_Material_Service_Group__c createTaxonomyRecord( PREP_Change_Detail__c detailRec )
    {
        if( detailRec.To_Field_Id__c != null ){
            Material_Service_Group_Clone__c matClone = [ Select Id, Name, Material_Service_Group_Name__c, SAP_Short_Text_Name__c, Sub_Category__c,
                                                        Active__c, Category_Link__c, Type__c, Discipline__c, Category_Manager__c, Category_Specialist__c,
                                                        GL_Account__c, Includes__c, Does_Not_Include__c from Material_Service_Group_Clone__c
                                                        where Id = :detailRec.To_Field_Id__c ];
            
            List<PREP_Material_Service_Group__c> taxonomyList = [ Select Id, Name, Type__c from PREP_Material_Service_Group__c
                                                            where Type__c =: matClone.Type__c order By Name DESC ];
            
            String matNameVal = '';
            if( taxonomyList.size() > 0 ){
                String codeNumber = taxonomyList[0].Name;
                Integer codeVal = Integer.valueOf( codeNumber.subString( 1, codeNumber.length() ));
                codeVal++;
                
                matNameVal = ( matClone.Type__c == 'Material' ? 'M' + codeVal : 'S' + codeVal );
            }
            
            PREP_Material_Service_Group__c mat = new PREP_Material_Service_Group__c( Name = matNameVal, Material_Service_Group_Name__c = matClone.Material_Service_Group_Name__c,
                                                SAP_Short_Text_Name__c = matClone.SAP_Short_Text_Name__c, Sub_Category__c = matClone.Sub_Category__c,
                                                Active__c = matClone.Active__c, Category_Link__c = matClone.Category_Link__c, Type__c = matClone.Type__c,
                                                Discipline_Link__c = matClone.Discipline__c, Category_Manager__c = matClone.Category_Manager__c,
                                                Category_Specialist__c = matClone.Category_Specialist__c, GL_Account__c = matClone.GL_Account__c,
                                                Includes__c = matClone.Includes__c, Does_Not_Include__c = matClone.Does_Not_Include__c );
            return mat;
        }
        
        return null;
    }

    /*Wrapper class for the change requests*/
    public class PREPChangeWrapper{
        public PREP_Change_Header__c header {get; set;}
        public List<PREP_Change_Detail__c> detail {get; set;}
        public Material_Service_Group_Clone__c taxonomyClone { get; set; }
        public Boolean userJeff { get; set; }
    }
}