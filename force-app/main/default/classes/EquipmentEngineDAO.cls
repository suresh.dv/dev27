/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Abstract DAO class for Equipment Engine SObject
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public interface EquipmentEngineDAO {

    List<Equipment_Engine__c> getEngineByEquipmentId(Id equipmentId);
    List<Equipment_Engine__c> getEnginesByEquipmentIds(Set<Id> equipmentIds);

}