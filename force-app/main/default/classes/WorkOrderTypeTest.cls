@isTest
private class WorkOrderTypeTest 
{
    static List<HOG_Notification_Type__c> notificationType;
    static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
    static HOG_Work_Order_Type__c workOrderType;
    static HOG_Service_Code_MAT__c serviceCodeMAT;
    static HOG_Service_Category__c serviceCategory;
    static HOG_Service_Code_Group__c serviceCodeGroup;
    static HOG_Service_Required__c serviceRequiredList;
	static HOG_User_Status__c userStatus;
	static HOG_Service_Priority__c servicePriorityList;
	static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};

    static testMethod void controller_WorkOrderType()
    {                                        
		SetupTestData();

        Test.startTest();
        
        PageReference pageRef = Page.WorkOrderType;

        Test.setCurrentPage(pageRef);
 
        ApexPages.StandardController controller = new ApexPages.StandardController(workOrderType);
     
        workOrderType workOrderTypeController = new WorkOrderType(controller);        
        System.assertNotEquals(null, workOrderType);
        
        workOrderTypeController.workOrderType = workOrderType;        
        
        //workOrderTypeController.getRecordTypes();
        
        workOrderTypeController.getRecordTypeOptions();
        
	    //List<SelectOption> recordTypeOptions = new List<SelectOption>();
        
                
        workOrderTypeController.cancel();
                
        workOrderTypeController.save();
        
        workOrderTypeController.saveAndNew();
                        
        Test.stopTest();                     
    }
    
    private static void SetupTestData()
	{        
        //-- Begin setup of data needed to test the Service Request Notification controller --//
                
        //-- Setup Service Category
        serviceCategory = ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--

        //-- Setup Notification Type
	    notificationType = new List<HOG_Notification_Type__c>();        
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, true, true, true));
        insert notificationType;
        //--
 
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]);
        insert serviceCodeMAT;
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
     	notificationTypePriority = new List<HOG_Notification_Type_Priority__c>();
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, true));                
        insert notificationTypePriority;        
        //--
                
        //-- Setup Work Order Type
 	    workOrderType = WorkOrderTypeTestData.createWorkOrderType(notificationType[0].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true);
        insert workOrderType;
        //--
	}
}