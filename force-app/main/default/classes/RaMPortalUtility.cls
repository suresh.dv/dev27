public without sharing class RaMPortalUtility {

    public static List<Case> getRaMOpenCasesByVendor(String vendor){
        List<Case> caseList = [SELECT Id, CaseNumber, LocationNumber__c, LocationNumber__r.Name, Ticket_Number_R_M__c, Priority, Subject, Equipment_Type__c,
                                    Equipment_Class__c, Status, CreatedDate
                                FROM Case
                                WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')
                                    AND Status IN ('Assigned', 'Work in Progress', 'Pending Parts', 'Pending Approval', 'Resolved', 'Completed', 'Vendor Completed', 'Disputed')
                                    AND Vendor_Assignment__c IN (SELECT Id 
                                                                FROM RaM_VendorAssignment__c 
                                                                WHERE Account__c = :vendor)
                                ORDER BY Priority DESC, CreatedDate ASC];

        return caseList;
    }  
    
    public static Case getRaMCaseByCaseNumber(String caseNumber){
        Case kase = null;

        try{
            kase = [SELECT Id, CaseNumber, LocationNumber__c, LocationNumber__r.Name, LocationNumber__r.Physical_Address_2__c, Ticket_Number_R_M__c, Priority, Subject, Equipment_Type__c,
                            Equipment_Class__c, Status, CreatedDate, Vendor_Status__c, Description, EquipmentTag__c, EquipmentName__r.Name, LocationNumber__r.LocationName__c,
                            LocationNumber__r.Phone__c, LocationNumber__r.Physical_Address_1__c, LocationNumber__r.City__c, LocationNumber__r.Province__c, Vendor_Name_VA__c,
                            VendorEmail__c, Physical_Address_1__c, Physical_Address_2__c, City__c, Province__c, Postal_Code__c, LocationPhone__c, Retailer__c, District_Manager__c,
                            Location_Type__c, Location_Classification__c, Service_Area__c, Maintenance_Tech_User__r.Name
                        FROM Case
                        WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')
                                AND CaseNumber = :caseNumber
                        LIMIT 1];

        }catch(Exception exp) {
            kase = null;
        }

        return kase;
    }     

    public static void createPortalUser(Contact cont, String userType) {
        Profile prof = getProfileByName(RaM_ConstantUtility.RAM_VENDOR_PROFILE_NAME);

        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.EmailHeader.triggerUserEmail = true;  

        User usr = new User();
        usr.ContactId = cont.Id;
        usr.username = getRaMUserName(cont.Email);
        usr.Email = cont.Email;
        usr.FirstName = cont.FirstName;
        usr.LastName = cont.LastName;
        usr.Alias = string.valueof(cont.FirstName.substring(0,1) + cont.LastName.substring(0,1));
        usr.ProfileId = prof.Id;
        usr.emailencodingkey='UTF-8';
        usr.languagelocalekey='en_US';
        usr.localesidkey='en_CA';
        usr.timezonesidkey='America/Denver';
        usr.Vendor_Portal_User_Type__c = userType;
        usr.CommunityNickname = cont.FirstName + ' ' + cont.LastName;

        try {
            insert usr;

            assignPermissionSet(usr, userType);

            if(!Test.isRunningTest()) {
                System.resetPassword(usr.Id, true);  
            }              

        } catch (Exception exp) {
            throw exp;
        }

    }

    public static void activateUser(String userId, Boolean needPasswordReset) {
        User usr = new User();
        usr.Id = userId;
        usr.IsActive = true;

        try{
            update usr;

            if(needPasswordReset && !Test.isRunningTest()) {
                System.resetPassword(usr.Id, true);  
            }            
        } catch(Exception exp) {
            throw exp;
        }        
    }

    public static void deactivateUser(String userId) {
        User usr = new User();
        usr.Id = userId;
        usr.IsActive = false;

        try{
            update usr;

        } catch(Exception exp) {
            throw exp;
        }

    }

    public static void updateContact(Contact cont) {
        try{
            update cont;
        } catch(Exception exp) {
            throw exp;
        }
    }

    public static void createContact(Contact cont) {
        Account acct = getAccountById(cont.AccountId);
        cont.OwnerId = acct.OwnerId;

        try{
            insert cont;
        } catch(Exception exp) {
            throw exp;
        }
    }    

    public static void createAndAssignCase(Case kase, List<User> techUsers) {
        List<RaM_Case_Assignment__c> assignments = new List<RaM_Case_Assignment__c>();
        Map<String, RaM_Case_Assignment__c> assignmentMap = getAssignments(kase.Id);

        for(User techUser : techUsers) {
            if(!assignmentMap.containsKey(techUser.Id)) {
                RaM_Case_Assignment__c caseAssignment = new RaM_Case_Assignment__c();
                caseAssignment.Assign_To__c = techUser.Id;
                caseAssignment.Parent_Case__c = kase.Id;
                caseAssignment.Subject__c = kase.Subject;
                caseAssignment.Ticket_Number__c = kase.Ticket_Number_R_M__c;
    
                assignments.add(caseAssignment);
            }
        }

        try{
            if(!assignments.isEmpty()) {
                //set the Primary technician
                assignments = checkPrimaryTechnicianAssignment(assignments, assignmentMap);

                insert assignments;
            }

        } catch(Exception exp) {
            throw exp;
        }
    }

    public static List<RaM_Case_Assignment__c> getAssignmentsByCase(String caseNumber){
        try{
            return [SELECT Id, Assign_To__r.Name, Parent_Case__r.Status, Work_Start__c, Work_Completed__c, Work_History__c, Assign_To__r.ContactId, Is_Primary_Assignment__c 
                    FROM RaM_Case_Assignment__c
                    WHERE Parent_Case__r.CaseNumber = :caseNumber
                    ORDER BY CreatedDate DESC];

        }catch(Exception exp) {
            throw exp;
        }
    }   
    
    public static Map<String, RaM_Case_Assignment__c> getAssignments(String caseRecordId){
        Map<String, RaM_Case_Assignment__c> assignmentMap = new Map<String, RaM_Case_Assignment__c>();

        try{
            List<RaM_Case_Assignment__c> assignmentList = [SELECT Id, Assign_To__c, Assign_To__r.Name, Parent_Case__r.Status, Work_Start__c, Work_Completed__c, Work_History__c, Is_Primary_Assignment__c
                                                            FROM RaM_Case_Assignment__c
                                                            WHERE Parent_Case__r.Id = :caseRecordId
                                                                AND Parent_Case__r.RecordType.Name IN ('RaM Case', 'RaM Child Case')
                                                            ORDER BY CreatedDate DESC];

            for(RaM_Case_Assignment__c assignment : assignmentList) {
                if(!assignmentMap.containsKey(assignment.Assign_To__c)) {
                    assignmentMap.put(assignment.Assign_To__c, assignment);
                }
            }
        }catch(Exception exp) {
            throw exp;
        }

        return assignmentMap;
    }  
    
    public static List<RaM_Case_Assignment__c> getAssignmentByContact(String contactId) {
        try{
            return [SELECT Id, Parent_Case__r.Status, Work_Start__c, Work_Completed__c, Work_History__c, Parent_Case__r.Id, Parent_Case__r.Ticket_Number_R_M__c, 
                        Parent_Case__r.Subject, Parent_Case__r.CaseNumber, Is_Primary_Assignment__c
                    FROM RaM_Case_Assignment__c
                    WHERE Assign_To__c IN (SELECT Id FROM User WHERE ContactId = :contactId)
                        AND Parent_Case__r.RecordType.Name IN ('RaM Case', 'RaM Child Case')
                        AND Parent_Case__r.Status IN ('Assigned', 'Work in Progress', 'Pending Parts', 'Pending Approval', 'Resolved')
                    ORDER BY CreatedDate DESC];

        }catch(Exception exp) {
            throw exp;
        }
    }

    public static List<RaM_Case_Assignment__c> getOpenActivityList(String technicianId) {
        try{
            return [SELECT Id, Parent_Case__r.Status, Work_Start__c, Work_Completed__c, Work_History__c, Parent_Case__r.Id, Parent_Case__r.Ticket_Number_R_M__c, 
                            Subject__c, Parent_Case__r.LocationNumber__r.Name, Parent_Case__r.LocationNumber__r.LocationName__c, CreatedDate, Is_Primary_Assignment__c 
                    FROM RaM_Case_Assignment__c
                    WHERE Assign_To__r.id = :technicianId
                        AND Parent_Case__r.RecordType.Name IN ('RaM Case', 'RaM Child Case')
                        AND Parent_Case__r.Status IN ('Assigned', 'Work in Progress', 'Pending Parts', 'Pending Approval')];
        }catch(Exception exp) {
            throw exp;
        }
    }  
    
    public static RaM_Case_Assignment__c getActivityById(String activityId, String userId){
        try{
            return [SELECT Id, Parent_Case__r.CaseNumber, Parent_Case__r.LocationNumber__c, Parent_Case__r.LocationNumber__r.Name, 
                            Parent_Case__r.LocationNumber__r.Physical_Address_2__c, Ticket_Number__c, Parent_Case__r.Priority, 
                            Subject__c, Parent_Case__r.Equipment_Type__c, Parent_Case__r.Equipment_Class__c, Parent_Case__r.Description, 
                            Parent_Case__r.EquipmentTag__c, Parent_Case__r.EquipmentName__r.Name, Parent_Case__r.LocationNumber__r.LocationName__c,
                            Parent_Case__r.LocationNumber__r.Phone__c, Parent_Case__r.LocationNumber__r.Physical_Address_1__c, Parent_Case__r.LocationNumber__r.City__c, 
                            Parent_Case__r.LocationNumber__r.Province__c, Parent_Case__r.Vendor_Name_VA__c, Parent_Case__r.VendorEmail__c, 
                            Parent_Case__r.Physical_Address_1__c, Parent_Case__r.Physical_Address_2__c, Parent_Case__r.City__c, Parent_Case__r.Province__c, 
                            Parent_Case__r.Postal_Code__c, Parent_Case__r.LocationPhone__c, Parent_Case__r.Retailer__c, Parent_Case__r.District_Manager__c,
                            Parent_Case__r.Location_Type__c, Parent_Case__r.Location_Classification__c, Parent_Case__r.Service_Area__c, Parent_Case__r.Maintenance_Tech_User__r.Name,
                            Parent_Case__r.Status, Work_Start__c, Parent_Case__r.Id, CreatedDate, Work_Completed__c, Is_Primary_Assignment__c
                        FROM RaM_Case_Assignment__c
                        WHERE Parent_Case__r.RecordType.Name IN ('RaM Case', 'RaM Child Case')
                                AND Assign_To__r.id = :userId
                                AND Id = :activityId  ];
        }catch(Exception exp) {
            throw exp;
        }
    }  
    
    public static void updateActivity(RaM_Case_Assignment__c assignment, RaM_Case_Assignment_Activity__c activity, Case kase, RaM_WorkInformation__c workInfo) {
        Savepoint sp = Database.setSavepoint();

        if(kase != null) {
            try{
                if(!Test.isRunningTest()) {
                    update kase;
                }
                
            }catch(Exception exp) {
                Database.rollback(sp);
                throw exp;
            }
        }

        if(workInfo != null) {
            try{
                insert workInfo;
            }catch(Exception exp) {
                Database.rollback(sp);
                throw exp;
            }
        }

        if(assignment != null) {
            try{
                update assignment;
            }catch(Exception exp) {
                Database.rollback(sp);
                throw exp;
            }
        }  
                
        if(activity != null) {
            try{
                insert activity;
            }catch(Exception exp) {
                Database.rollback(sp);
                throw exp;
            }
        } 
    }

    public static void updatePrimaryTechnician(String caseId, String newTechnicianId) {
        List<RaM_Case_Assignment__c> assignments = getAssignmentsById(caseId);

        for(RaM_Case_Assignment__c assignment : assignments) {
            assignment.Is_Primary_Assignment__c = false;

            if(assignment.Assign_To__r.ContactId == newTechnicianId) {
                assignment.Is_Primary_Assignment__c = true;
            }
        }

        if(!assignments.isEmpty()) {
            try{
                update assignments;

            }catch(Exception exp) {
                throw exp;
            }
        }
    }

    public static void unassignTechnician(String assignmentRecordId) {
        RaM_Case_Assignment__c assignment = new RaM_Case_Assignment__c();
        assignment.Id = assignmentRecordId;
        assignment.Assign_To__c = null;

        try{
            update assignment;
        }catch(Exception exp) {
            throw exp;
        }
    }

    public static void reassignNewTechnician(String assignmentRecordId, String newTechnicianId) {
        RaM_Case_Assignment__c assignment = new RaM_Case_Assignment__c();
        assignment.Id = assignmentRecordId;
        assignment.Assign_To__c = newTechnicianId;

        try{
            update assignment;
        }catch(Exception exp) {
            throw exp;
        }
    }

    public static List<RaM_Case_Assignment_Activity__c> getTechnicianAcivities(String caseRecordId) {
        return [SELECT Id, Name, Activity_Location__Latitude__s, Activity_Location__Longitude__s, CreatedDate, RaM_Case_Assignment__c, 
                    Reason__c, Activity_Date__c, Location_Error__c, To_Status__c 
                FROM RaM_Case_Assignment_Activity__c
                WHERE RaM_Case_Assignment__r.Parent_Case__c  = :caseRecordId
                ORDER BY CreatedDate DESC
                LIMIT 999];
    }

    public static List<RaM_Case_Assignment__c> getAssignmentsById(String caseId){
        try{
            return [SELECT Id, Assign_To__r.Name, Parent_Case__r.Status, Work_Start__c, Work_Completed__c, Work_History__c, Assign_To__r.ContactId, Is_Primary_Assignment__c 
                    FROM RaM_Case_Assignment__c
                    WHERE Parent_Case__r.Id = :caseId
                        AND Parent_Case__r.RecordType.Name IN ('RaM Case', 'RaM Child Case')
                    ORDER BY CreatedDate DESC];

        }catch(Exception exp) {
            throw exp;
        }
    } 
    
    public static Case getCaseLocation(String caseRecordId){
        try{
            return [SELECT Id, CaseNumber, LocationNumber__r.Name, LocationNumber__r.Physical_Address_2__c, Ticket_Number_R_M__c, Priority, Subject, Status, 
                        CreatedDate, LocationNumber__r.LocationName__c, LocationNumber__r.Phone__c, LocationNumber__r.Physical_Address_1__c, Vendor_Name_VA__c,
                        LocationNumber__r.City__c, LocationNumber__r.Province__c, Postal_Code__c, District_Manager__c, Location_Type__c, Location_Classification__c, Maintenance_Tech_User__r.Name 
                    FROM Case
                    WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')
                            AND Id = :caseRecordId ];
        }catch(Exception exp) {
            throw exp;
        }
    }  
    
    public static List<Attachment> getAttachmentsByCase(String caseNumber) {
        return [SELECT Id, ParentId, Name, CreatedDate 
                FROM Attachment
                WHERE ParentId IN (SELECT Id FROM Case WHERE CaseNumber = :caseNumber)
                ORDER BY CreatedDate DESC];
    }

    public static List<ContentDocument> getFilesByCase(String caseNumber) {
        List<String> documentIds = new List<String>();
        
        List<ContentDocumentLink> contentDocLinks = [SELECT Id, LinkedEntityId, ContentDocumentId 
                                                    FROM ContentDocumentLink 
                                                    WHERE LinkedEntityId in  (SELECT Id FROM Case WHERE CaseNumber = :caseNumber)];

        for(ContentDocumentLink contentDocLink : contentDocLinks) {
            documentIds.add(contentDocLink.ContentDocumentId);
        }
        
        return [SELECT Id, CreatedDate, Title, ParentId 
                FROM ContentDocument
                WHERE Id IN :documentIds
                ORDER BY CreatedDate DESC];
    } 
    
    public static List<RaM_Case_Assignment_Activity__c> getActivityCommentsByCase(String caseRecordId) {
        return [SELECT Id, Reason__c, Activity_Date__c, CreatedBy.Name 
                FROM RaM_Case_Assignment_Activity__c
                WHERE RaM_Case_Assignment__r.Parent_Case__c = :caseRecordId
                ORDER BY CreatedDate DESC];
    }

    private static List<RaM_Case_Assignment__c> checkPrimaryTechnicianAssignment(List<RaM_Case_Assignment__c> assignments, Map<String, RaM_Case_Assignment__c> assignmentMap) {
        Boolean isPrimaryExists = false;

        if(!assignmentMap.isEmpty()) {
            for(String key : assignmentMap.keySet()) {
                RaM_Case_Assignment__c assignment = assignmentMap.get(key);

                if(assignment.Is_Primary_Assignment__c) {
                    isPrimaryExists = true;
                    break;
                }
            }
        }

        if(!isPrimaryExists) {
            RaM_Case_Assignment__c primaryAssignment = assignments[0];
            primaryAssignment.Is_Primary_Assignment__c = true;
        }

        return assignments;
    }    

    private static Account getAccountById(String accountId) {
        return [SELECT Id, OwnerId FROM Account WHERE Id = :accountId];
    }


    private static void assignPermissionSet(User usr, String userType) {
        PermissionSetAssignment psa = null;
        
        if(userType.equalsIgnoreCase(RaM_ConstantUtility.RAM_DISPATCHER_PERMISSION)) {
            PermissionSet permSet = gePermissionSetByName(RaM_ConstantUtility.RAM_DISPATCHER_PERMISSION);

            psa = new PermissionSetAssignment();
            psa.PermissionSetId = permSet.Id;
            psa.AssigneeId = usr.Id;  

        } else if(userType.equalsIgnoreCase(RaM_ConstantUtility.RAM_TECHNICIAN_PERMISSION)) {
            PermissionSet permSet = gePermissionSetByName(RaM_ConstantUtility.RAM_TECHNICIAN_PERMISSION);

            psa = new PermissionSetAssignment();
            psa.PermissionSetId = permSet.Id;
            psa.AssigneeId = usr.Id;  
        }

        if(psa != null) {
            try{
                insert psa;

            }catch(Exception exp) {
                throw exp;
            }
        }
    }

    private static Profile getProfileByName(String profileName) {
        return [SELECT Id, Name 
                FROM Profile 
                WHERE Name = :profileName];
    }

    private static PermissionSet gePermissionSetByName(String permissionSetName) {
        return [SELECT Id, Name, Label 
                FROM PermissionSet 
                WHERE Label = :permissionSetName];
    }  
    
    private static String getRaMUserName(String emailAddress) {
        String ramUserName = emailAddress;

        List<String> emailParts = emailAddress.split('@');
        List<String> domainParts = emailParts[1].split('\\.');

        Organization org = getInstance();

        if(org.IsSandbox) {
            ramUserName = emailParts[0] + '@' + domainParts[0] + RaM_ConstantUtility.RAM_DOMAIN_NAME + '.' + org.InstanceName;
        } else {
            ramUserName = emailParts[0] + '@' + domainParts[0] + RaM_ConstantUtility.RAM_DOMAIN_NAME;
        }

        return ramUserName;
    }

    private static Organization getInstance() {
        return [SELECT Id, IsSandbox, InstanceName FROM Organization];
    }

    // private static String getCaseRecordType(String recTypeName) {
    //     return  Schema.SObjectType.Case.getRecordTypeInfosByName().get(recTypeName).getRecordTypeId();
    // }

}