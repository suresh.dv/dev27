/**
 * Created by mzelina@sk.ibm.com on 24/01/2019.
 */
//include SPCC_MassServiceEntrySheetEditCtrlXTest to test this

public without sharing class SPCC_DMLServiceWithoutSharing {

    /* working but not using - commented to increase coverage
    public static List<SObject>  dynamicUpsert(List<SObject> sObjectList){

        upsert sObjectList;
        return sObjectList;
    }
    */

    public static List<Database.DeleteResult>  dynamicDelete(List<SObject> sObjectList){

        return Database.delete(sObjectList, false);
}
/*
    public static List<Database.UpsertResult> dynamicUpsertOnExtId(List<SObject> sObjectList, String objectName, Schema.SObjectField externalId){

        String listType = 'List<' + objectName + '>';
        List<SObject> castRecords = (List<SObject>)Type.forName(listType).newInstance();
        castRecords.addAll(sObjectList);

        return Database.upsert(castRecords, externalId);
    }
*/


    public static List<Database.UpsertResult> upsertCostElementOnExtId(List<SPCC_Cost_Element__c> costElementList, Schema.SObjectField externalId){

        return Database.upsert(costElementList, externalId, false);
    }
}