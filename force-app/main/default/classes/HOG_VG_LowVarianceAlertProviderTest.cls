/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Test class for HOG_VG_LowVarianceAlertProvider
History:        jschn 24/10/2018 - Created.
*************************************************************************************************/
@IsTest
private class HOG_VG_LowVarianceAlertProviderTest {

    @IsTest
    static void testBehavior() {
        Date today = Date.today();
        User usr = HOG_VentGas_TestData.createUser();
        Location__c location = new Location__c(
                PVR_GOR_Factor__c = 1,
                GOR_Test_Date__c = today,
                PVR_Fuel_Consumption__c = 1,
                GOR_Effective_Date__c = today,
                Measured_Vent_Rate__c = 1,
                Monthly_Trucked_Oil__c = 1,
                PVR_Hours_On_Prod__c = 1,
                Name = 'TestLocation',
                Operating_Field_AMU__r = new Field__c(Production_Engineer_User__c = usr.Id)
        );
        HOG_VG_AlertProvider provider = new HOG_VG_LowVarianceAlertProvider();

        Test.startTest();
        HOG_Vent_Gas_Alert__c alert = provider.createAlert(location);
        Datetime now = Datetime.now();
        Test.stopTest();

        System.assertNotEquals(null, alert);
        System.assertEquals(1, alert.PVR_GOR_Factor__c);
        System.assertEquals(today, alert.GOR_Test_Date__c);
        System.assertEquals(1, alert.PVR_Fuel_Consumption__c);
        System.assertEquals(today, alert.GOR_Effective_Date__c);
        System.assertEquals(null, alert.PVR_Fuel_Consumption_Rate__c);
        System.assertEquals(1, alert.Measured_Vent_Rate__c);
        System.assertEquals(1, alert.Monthly_Trucked_Oil__c);
        System.assertEquals(null, alert.Total_Gas_Production__c);
        System.assertEquals(null, alert.Predicted_Reported_Vent_Rate__c);
        System.assertEquals(usr.Id, alert.Production_Engineer__c);
        System.assertEquals(now, alert.Start_Date__c);
        System.assertEquals(HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, alert.Status__c);
        System.assertEquals(null, alert.Well_Location__c);
        System.assertEquals('TestLocation - '
                + HOG_VentGas_Utilities.ALERT_TYPE_ABV_LOW_VARIANCE + ' - '
                + String.valueOf(today),
                alert.Name);
        System.assertEquals(HOG_VentGas_Utilities.ALERT_PRIORITY_LOW, alert.Priority__c);
        System.assertEquals(HOG_VentGas_Utilities.ALERT_TYPE_LOW_VARIANCE, alert.Type__c);
    }

}