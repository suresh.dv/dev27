/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Exception extension class used for purpose of generic errors for HOG application.
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class HOG_Exception extends Exception { }