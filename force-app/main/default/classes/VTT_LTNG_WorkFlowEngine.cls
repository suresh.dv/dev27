/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_LTNG_WorkFlowEngine for Vendor time tracking
Test Class:     VTT_LTNG_WorkFlowEngineTest, VTT_LTNG_WorkFlowEngineServiceTest
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_WorkFlowEngine {
    private static final String CLASS_NAME = String.valueOf(VTT_LTNG_WorkFlowEngine.class);

    @AuraEnabled
    public List<VTT_LTNG_EngineMessage> messages = new List<VTT_LTNG_EngineMessage>();

    @AuraEnabled
    public List<VTT_LTNG_EngineAction> availableActions = new List<VTT_LTNG_EngineAction>();

    @AuraEnabled
    public Work_Order_Activity__c activity;

    @AuraEnabled
    public VTT_TradesmanFlags tradesmanInfo;

    @AuraEnabled
    public Contact tradesman;

    @AuraEnabled
    public Boolean canManageAssignments = false;

    @AuraEnabled
    public Boolean isAdmin = false;

    @AuraEnabled
    public Boolean hasEPDSubmitted = false;

    @AuraEnabled
    public Boolean isEPDFormRequired = false;

    @AuraEnabled
    public Boolean isEPDFormAvailable = false;

    public class EngineBuilder {
        private Contact tradesman;
        private Work_Order_Activity__c workOrderActivity;
        private Work_Order_Activity_Log_Entry__c lastEntry;
        private VTT_LTNG_WorkFlowEngine engine;
        private Boolean hasEPDSubmitted;
        private Boolean isEPDFormRequired;

        public EngineBuilder() {
            this.engine = new VTT_LTNG_WorkFlowEngine();
        }

        public EngineBuilder addTradesman(Contact tradesman) {
            this.engine.tradesman = tradesman;
            this.tradesman = tradesman;
            return this;
        }

        public EngineBuilder addActivity(Work_Order_Activity__c workOrderActivity) {
            this.engine.activity = workOrderActivity;
            this.workOrderActivity = workOrderActivity;
            return this;
        }

        public EngineBuilder addLastEntry(Work_Order_Activity_Log_Entry__c lastEntry) {
            this.lastEntry = lastEntry;
            return this;
        }

        public EngineBuilder addTradesmanInfo(VTT_TradesmanFlags info) {
            this.engine.tradesmanInfo = info;
            return this;
        }

        // Call after setting activity
        public EngineBuilder checkForEPD() {
            this.hasEPDSubmitted = VTT_LTNG_WorkFlowEngineService.hasEPDSubmitted(this.engine.activity);
            this.isEPDFormRequired = VTT_LTNG_WorkFlowEngineService.isEPDFormRequired(this.engine.activity);
            return this;
        }

        public VTT_LTNG_WorkFlowEngine build() {
            System.debug(CLASS_NAME + ' build start');

            this.engine.isAdmin = VTT_Utilities.IsAdminUser();
            this.engine.canManageAssignments = (workOrderActivity.Status__c <> VTT_Utilities.ACTIVITY_STATUS_COMPLETED)
                    && (this.engine.isAdmin || (VTT_Utilities.IsVendorSupervisor() && tradesman.AccountId <> null &
                    workOrderActivity.Assigned_Vendor__c == tradesman.AccountId));

            if (isCompletedActivity()) {
                this.engine.messages.add(new VTT_LTNG_EngineMessage('Activity is completed', VTT_LTNG_WorkFlowEngineConstants.SEVERITY_WARNING));
                return this.engine;
            }

            if (isActivityDeleted()) {
                this.engine.canManageAssignments = false;
                this.engine.messages.add(new VTT_LTNG_EngineMessage('Activity has been deleted in SAP', VTT_LTNG_WorkFlowEngineConstants.SEVERITY_WARNING));
                return this.engine;
            }

            if (isMissingAMU()) {
                this.engine.messages.add(new VTT_LTNG_EngineMessage('Activity and Work Order are missing AMU - This activity will use cold workflow', VTT_LTNG_WorkFlowEngineConstants.SEVERITY_WARNING));
            }

            if (isNonCompletedActivity()) {
                // Check if user can work on this activity
                System.debug(CLASS_NAME + ' -> build. 1 ');
                VTT_Rule_TradesmanWorkStatusValidation validation = new VTT_Rule_TradesmanWorkStatusValidation(
                        workOrderActivity,
                        tradesman
                );
                validation.processRule();
                this.engine.messages.addAll(validation.getMessages());
                this.engine.availableActions.addAll(validation.getAvailableActions());
                this.engine.availableActions.sort();
                if (!validation.passedRule()) {
                    System.debug(CLASS_NAME + ' -> build. 2 ');
                    System.debug(CLASS_NAME + ' isNonCompletedActivity() and !validation.passedRule() return '
                            + this.engine);
                    return this.engine;
                }
            }

            if (isTradesmanNotWorkingButIsAssigned()) {
                System.debug(CLASS_NAME + ' -> build. 3 ');
                VTT_Rule_AssignedButNotWorking validation = new VTT_Rule_AssignedButNotWorking(
                        workOrderActivity,
                        tradesman,
                        hasEPDSubmitted,
                        isEPDFormRequired
                );
                validation.processRule();
                this.engine.messages.addAll(validation.getMessages());
                this.engine.availableActions.addAll(validation.getAvailableActions());
                this.engine.availableActions.sort();
                // EPD
                this.engine.isEPDFormRequired = validation.isEPDFormRequired;
                this.engine.hasEPDSubmitted = validation.hasEPDSubmitted;
                this.engine.isEPDFormAvailable = validation.isEPDFormAvailable;

                System.debug(CLASS_NAME + ' isTradesmanNotWorkingButIsAssigned return  ' + this.engine);
                return this.engine;
            }

            if (isTradesmanWorkingAndIsAssigned()) {
                VTT_Rule_AssignedAndWorking validation = new VTT_Rule_AssignedAndWorking(
                        workOrderActivity,
                        tradesman,
                        lastEntry,
                        hasEPDSubmitted,
                        isEPDFormRequired
                );
                validation.processRule();
                this.engine.messages.addAll(validation.getMessages());
                this.engine.availableActions.addAll(validation.getAvailableActions());
                this.engine.availableActions.sort();
                // EPD
                this.engine.isEPDFormRequired = validation.isEPDFormRequired;
                this.engine.hasEPDSubmitted = validation.hasEPDSubmitted;
                this.engine.isEPDFormAvailable = validation.isEPDFormAvailable;
                System.debug(CLASS_NAME + ' isTradesmanWorkingAndIsAssigned return  ' + this.engine);
                return this.engine;
            }


            return new VTT_LTNG_WorkFlowEngine();
        }

        // HELPERS
        /**
         * Check if tradesman is assigned to this activity
         * @return
         */
        private Boolean isTradesmanWorkingAndIsAssigned() {
            return lastEntry.Id <> null && isCurrentUserAssignedTradesman(workOrderActivity);
        }

        /**
         * Check if tradesman is working on this activity and is also assigned
         * @return
         */
        private Boolean isTradesmanNotWorkingButIsAssigned() {
            return lastEntry.Id == null && isCurrentUserAssignedTradesman(workOrderActivity);
        }

        /**
         * Check if activity is in completed or canceled status
         * @return
         */
        private Boolean isNonCompletedActivity() {
            System.debug(CLASS_NAME + ' -> isNonCompletedActivity. 1 ');
            return this.workOrderActivity.Status__c <> VTT_Utilities.ACTIVITY_STATUS_COMPLETED
                    && this.workOrderActivity.Status__c <> VTT_Utilities.ACTIVITY_STATUS_CANCELLED;
        }

        private Boolean isCompletedActivity() {
            System.debug(CLASS_NAME + ' -> isCompletedActivity. 1 ');
            return this.workOrderActivity.Status__c == VTT_Utilities.ACTIVITY_STATUS_COMPLETED
                    || this.workOrderActivity.Status__c == VTT_Utilities.ACTIVITY_STATUS_CANCELLED;
        }

        private Boolean isActivityDeleted() {
            return this.workOrderActivity.SAP_Deleted__c;
        }

        /**
         * Check if activity and work order are having AMU filled in
         * @return
         */
        private Boolean isMissingAMU() {
            return this.workOrderActivity.Operating_Field_AMU__c == null
                    && this.workOrderActivity.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c == null;
        }
    }

    /**
     * Check If Tradesmen is assigned on this activity
     * @param activityId - Activity with all related Work_Order_Activity_Assignments__r
     * @return
     */
    private static Boolean isCurrentUserAssignedTradesman(Work_Order_Activity__c activityId) {
        return activityId.Work_Order_Activity_Assignments__r.size() > 0;
    }

    public void addMessage(VTT_LTNG_EngineMessage message) {
        this.messages.add(message);
    }
}