/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class containing all queries for EPD_Engine_mdt Custom Metadata Type
Test Class:     EPD_EngineConfigSelectorTest
History:        jschn 2019-05-26 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_EngineConfigSelector implements EPD_EngineConfigDAO {

    private static final String CLASS_NAME = String.valueOf(EPD_EngineConfigSelector.class);

    private static String QUERY_BASE = '' +
            'SELECT Label, Model__c, Manufacturer__c, Required_Exhaust_Measurements__c, Max_Valve_Seat_Recession__c, ' +
                    'Recommended_Cranking_Speed__c, Min_Cylinder_Compression__c, Max_Cylinder_Compression__c, ' +
                    'Min_Intake_Manifold_Pressure__c, Max_Intake_Manifold_Pressure__c, Min_RPM__c, Max_RPM__c, ' +
                    'Measurement_Type__c, Number_of_Cylinders__c, Engine_Configuration__c, Number_of_COC_Stages__c, ' +
                    'Advanced_EOC__c, EOC_Fuel_Pressure_Per_Block__c, Valve_Seat_Warning_Limit__c, ' +
            '( ' +
                    'SELECT Part__r.Label, Display_Order__c ' +
                    'FROM Engine_Parts__r ' +
                    'ORDER BY Display_Order__c ASC ' +
            ') ' +
            'FROM EPD_Engine__mdt';

    /**
     * Method for querying list of Engine Configuration records based on manufacturer and model.
     *
     * @param manufacturer
     * @param model
     *
     * @return List<EPD_Engine__mdt>
     */
    public List<EPD_Engine__mdt> getEngineConfig(String manufacturer, String model) {
        System.debug(CLASS_NAME + ' -> getEngineConfig. Params: \nManufacturer: ' + manufacturer + '\nModel: ' + model);
        return Database.query(
                QUERY_BASE +
                        ' WHERE Manufacturer__c =: manufacturer ' +
                        'AND Model__c =: model'
        );
    }

    /**
     * Method for querying list of Engine Configuration records.
     *
     * @return List<EPD_Engine__mdt>
     */
    public List<EPD_Engine__mdt> getEngineConfigs() {
        return Database.query(QUERY_BASE);
    }

}