/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint class that handles requests from List View filter on Work Order Activities
Test Class:     VTT_WOA_SearchCriteria_EndpointTest
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public without sharing class VTT_WOA_SearchCriteria_Endpoint {

    /**
     * Gets filter form options with all necessary flags for current user.
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    @AuraEnabled
    public static VTT_WOA_SearchCriteria_Options getFilterFormOptionsForCurrentUser() {
        return new VTT_WOA_SearchCriteria_Handler()
                .getFilterFormOptions(UserInfo.getUserId());
    }

    /**
     * Load List for Combobox containing Tradesmen(Contacts) based on Account Id.
     *
     * @param accountId
     *
     * @return List<HOG_PicklistItem_Simple>
     */
    @AuraEnabled(Cacheable = true)
    public static List<HOG_PicklistItem_Simple> loadTradesmanForAccount(String accountId) {
        return new VTT_WOA_SearchCriteria_Handler()
                .getTradesmanForAccount(accountId);
    }

    /**
     * Retrieves List of all stored Search Filters for Current User.
     *
     * @return List<Work_Order_Activity_Search_Criteria__c>
     */
    @AuraEnabled
    public static List<Work_Order_Activity_Search_Criteria__c> getStoredFiltersForCurrentUser() {
        return new VTT_WOA_SearchCriteria_Handler()
                .getStoredFilters(UserInfo.getUserId());
    }

    /**
     * Saves Search Filter for current User.
     *
     * @param filterString
     * @param filterName
     *
     * @return Work_Order_Activity_Search_Criteria__c
     */
    @AuraEnabled
    public static Work_Order_Activity_Search_Criteria__c saveFilter(String filterString, String filterName) {
        return new VTT_WOA_SearchCriteria_Handler()
                .saveFilter(
                        filterString,
                        filterName,
                        UserInfo.getUserId()
                );
    }

    /**
     * Set Last Selected Filter flag on Work Order Activity Search Criteria based on provided
     * Filter Id. Every other filter flag would be set to false.
     *
     * @param filterId
     *
     * @return List<Work_Order_Activity_Search_Criteria__c>
     */
    @AuraEnabled
    public static List<Work_Order_Activity_Search_Criteria__c> setLastSelectedFilter(Id filterId) {
        return new VTT_WOA_SearchCriteria_Handler()
                .setLastSelectedFilter(
                        filterId,
                        UserInfo.getUserId()
                );
    }

    /**
     * Select filter from provided filters based on provided Filter Id.
     * Every other filter flag would be set to false.
     *
     * @param filters
     * @param filterId
     *
     * @return Work_Order_Activity_Search_Criteria__c
     */
    @AuraEnabled
    public static Work_Order_Activity_Search_Criteria__c selectFilter(List<Work_Order_Activity_Search_Criteria__c> filters, Id filterId) {
        return VTT_Utilities.UpdateLastSelectedUserActivitySearchFilter(filters, filterId);
    }

    /**
     * Delete filter based on provided Filter Id.
     *
     * @param filterId
     *
     * @return String
     */
    @AuraEnabled
    public static String deleteFilter(Id filterId) {
        return new VTT_WOA_SearchCriteria_Handler()
                .deleteFilter(filterId);
    }

}