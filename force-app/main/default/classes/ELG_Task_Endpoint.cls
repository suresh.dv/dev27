/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/17/2019   
 */

public class ELG_Task_Endpoint {
	private static final String CLASS_NAME = String.valueOf(ELG_Task_Endpoint.class);

	@AuraEnabled
	public static HOG_CustomResponseImpl getUserSettings() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {

			List<ELG_User_Setting__c> userSettings = new ELG_ShiftHandoverSelector().getUsersSettings();

			if (!userSettings.isEmpty()) {
				response.addResult(userSettings[0]);
			} else {
				response.addError('No default AMU');
			}

			//response = new ELG_Task_EndpointService().getTask(taskId);
		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' user settings response: ' + response);
		return response;

	}

	@AuraEnabled
	public static HOG_CustomResponseImpl getTask(String taskId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_Task_EndpointService().getTask(taskId);
		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' task query response: ' + response);
		return response;

	}

	@AuraEnabled
	public static HOG_CustomResponseImpl getPostTasks(String postId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_Task_EndpointService().getPostTasks(postId);

		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' loading tasks response: ' + response);
		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl getAMUPosts(String amuId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_Task_EndpointService().getAMUPosts(amuId);
		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' loading posts response: ' + response);
		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl createPostTask(String postId, String shortDescription, String taskDescription) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_Task_EndpointService().insertTask(postId, shortDescription, taskDescription);
		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' task insert response: ' + response);
		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl saveEditedTask(ELG_Task__c newTaskValues) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_Task_EndpointService().updateTask(newTaskValues);
		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' task update response: ' + response);
		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl taskUpdateComplete(String taskId, String userId, String userComment) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_Task_EndpointService().updateTaskComplete(taskId, userId, userComment);
		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' task update completed status response: ' + response);
		return response;

	}

	@AuraEnabled
	public static HOG_CustomResponseImpl taskUpdateCancelled(String taskId, String userId, String cancellationReason) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_Task_EndpointService().updateTaskCancelled(taskId, userId, cancellationReason);
		} catch (Exception ex) {
			response.addError(ex.getMessage());
		}

		System.debug(CLASS_NAME + ' task update cancelled status response: ' + response);
		return response;

	}

/*	@AuraEnabled
	public static HOG_CustomResponseImpl getAMUActiveTasks() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		List<ELG_User_Setting__c> userSettings = new ELG_ShiftHandoverSelector().getUsersSettings();

		if (userSettings[0].Operating_Field_AMU__c == null) {
			response.addResult(null);
		} else {

			//response.addResult(getAMUPosts());

			List<ELG_Task__c> activeTasks = [
					SELECT Id,
							Name,
							Post_Name__c,
							CreatedBy.Name,
							CreatedDate
					FROM ELG_Task__c
					WHERE Operating_Field_AMU__c = :userSettings[0].Operating_Field_AMU__c
					AND Status__c = :ELG_Constants.TASK_ACTIVE
					ORDER BY Post_Name__c ASC
			];


		}

		return response;
	}*/

}