/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint class that handles requests from VG Alert components
Test Class:     VG_AlertEndpointTest
History:        jschn 21/01/2020 - Created.
*************************************************************************************************/
public with sharing class VG_AlertEndpoint {

    /**
     * Retrieves Id of Production Engineer based on Location Id.
     *
     * @param wellId
     *
     * @return Id
     */
    @AuraEnabled
    public static Id getProductionEngineer(Id wellId) {
        return new VG_AlertService().getProductionEngineer(wellId);
    }

    /**
     * Runs validation on Well Location if it is Valid for creating Engineering Notification Alert.
     *
     * @param wellId
     *
     * @return Boolean
     */
    @AuraEnabled
    public static Boolean isValidLocation(Id wellId) {
        return new VG_AlertService().isValidLocation(wellId);
    }

    /**
     * Validates current user if he can create Engineering Notification Alert
     *
     * @return Boolean
     */
    @AuraEnabled
    public static Boolean isValidUser() {
        return new VG_AlertService().isValidUserForEngNotifAlert();
    }

    /**
     * Saves files (serialized in JSON)
     *
     * @param files
     * @param alertId
     *
     * @return Boolean
     */
    @AuraEnabled
    public static Boolean saveFiles(String files, Id alertId) {
        return new VG_AlertFileService().saveFiles(files, alertId);
    }

}