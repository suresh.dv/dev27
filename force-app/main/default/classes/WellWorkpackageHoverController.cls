public class WellWorkpackageHoverController
{
    public Id workPackageId {get;set;}
    
    public String hoverPosition {get;set;}
    
    public Milestone1_Project__c workPackage {get;set;}
    
    public List<NetworkActivityWrapper> networkAndActivities {get;set;}
    
    public class NetworkActivityWrapper
    {
        public String Network {get;set;}
        public String Activity {get;set;}
        
        public NetworkActivityWrapper(String Network, String Activity)
        {
            this.Network = Network;
            this.Activity = Activity;
        }
    }
    
    public WellWorkpackageHoverController()
    {
        // Get project id
        workPackageId = ApexPages.CurrentPage().getParameters().get('id');
        
system.debug('mikep workPackageId '+workPackageId);
        
        // Get vertical position
        hoverPosition = ApexPages.CurrentPage().getParameters().get('hoverPosition');
        
system.debug('mikep hoverPosition '+hoverPosition);
        
        workPackage = [SELECT Id,
                              Network_Activity_Code__c
                       FROM Milestone1_Project__c
                       WHERE Id =: workPackageId];
        
system.debug('mikep workPackage '+workPackage);
        
        networkAndActivities = new List<NetworkActivityWrapper>();

        for(String datum : workpackage.Network_Activity_Code__c.split(';'))
        {
system.debug('mikep datum '+datum);
        
            List<String> pairs = datum.split('-');
            
system.debug('mikep pairs '+pairs);
            
            networkAndActivities.add(new NetworkActivityWrapper(pairs[0],pairs[1]));
        }
    }
}