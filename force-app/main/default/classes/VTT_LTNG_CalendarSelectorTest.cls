/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_CalendarSelector class
History:        mbrimus 14/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_CalendarSelectorTest {

    @IsTest
    static void getLogEntriesForTradesmanOnDate_withoutParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity_Log_Entry__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getLogEntriesForTradesmanOnDate(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getLogEntriesForTradesmanOnDate_withParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity_Log_Entry__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Starting Job');
        }

        Test.startTest();
        result = selector.getLogEntriesForTradesmanOnDate(tradesmanContact.Id, System.today());
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getVendorAccounts_noAccounts() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Account> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getVendorAccounts();
        Test.stopTest();

        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getVendorAccounts_someAccounts() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Account> result;
        Integer expectedCount = 2;

        Account vendor1 = VTT_TestData.createVendorAccount('Vendor1');
        Account vendor2 = VTT_TestData.createVendorAccount('Vendor2');

        Test.startTest();
        result = selector.getVendorAccounts();
        Test.stopTest();

        System.assertNotEquals(vendor1.Id, null);
        System.assertNotEquals(vendor2.Id, null);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getTradesmanForAccount_withoutParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Contact> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getTradesmanForAccount(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getTradesmanForAccount_withParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Contact> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Account vendor;
        System.runAs(otherUser) {
            vendor = VTT_TestData.createVendorAccount('Test');
            VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
        }

        Test.startTest();
        result = selector.getTradesmanForAccount(vendor.Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getActivityLogsForTradesman_withParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity_Log__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Starting Job');
        }

        Test.startTest();
        result = selector.getActivityLogsForTradesman(tradesmanContact.Id, System.today().addDays(-1), System.today());
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getActivityLogsForTradesman_withoutParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity_Log__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getActivityLogsForTradesman(null, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAutoCompletedActivities_withoutParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity_Log__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getAutoCompletedActivities(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAutoCompletedActivities_withParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity_Log__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_Utilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_AUTOCOMPLETE,
                    0,
                    0,
                    null,
                    'Starting Job');
        }
        List<Work_Order_Activity_Log__c> existingLogs = [SELECT Id FROM Work_Order_Activity_Log__c];
        System.assertNotEquals(null, existingLogs);
        System.assertEquals(1, existingLogs.size());

        Test.startTest();
        result = selector.getAutoCompletedActivities(new Set<Id>{
                existingLogs.get(0).Id
        });
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
        System.assertEquals(expectedCount, result.get(0).Work_Order_Activity_Log_Entries2__r.size());
    }

    @IsTest
    static void getScheduledActivities_withoutParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getScheduledActivities(null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getScheduledActivities_withParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            activity.Scheduled_Start_Date__c = System.today();
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesmanContact.Id);
            update activity;
        }

        Test.startTest();
        result = selector.getScheduledActivities(null, tradesmanContact.Id, System.today().addDays(-1), System.today().addDays(1));
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOnHoldActivities_withoutParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOnHoldActivities(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOnHoldActivities_withParam() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<Work_Order_Activity__c> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            activity.Status__c = VTT_Utilities.ACTIVITY_STATUS_ONHOLD;
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesmanContact.Id);
            update activity;
        }

        Test.startTest();
        result = selector.getOnHoldActivities(tradesmanContact.Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getTradesmanInfo_noData() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        Contact result;

        Test.startTest();
        result = selector.getTradesmanInfo();
        Test.stopTest();

        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getTradesmanInfo_withData() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        Contact result;

        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            result = selector.getTradesmanInfo();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(result.Id, tradesmanContact.Id);
    }

    @IsTest
    static void getSupervisorPermissionSets() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        Map<Id, PermissionSet> result;
        Integer expectedCount = 3;

        Test.startTest();
        result = selector.getSupervisorPermissionSets();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.keySet().size());
    }

    @IsTest
    static void getPermissionSetAssignmentsOfCurrentUser_nonSupervisor() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<PermissionSetAssignment> result;
        Integer expectedCount = 0;

        User otherUser = VTT_TestData.createVTTUser();
        System.runAs(otherUser) {
            result = selector.getPermissionSetAssignmentsOfCurrentUser(selector.getSupervisorPermissionSets().keySet());
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getPermissionSetAssignmentsOfCurrentUser_supervisor() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        List<PermissionSetAssignment> result;
        Integer expectedCount = 1;

        User otherUser = VTT_TestData.createVendorSupervisorUser();
        System.runAs(otherUser) {
            result = selector.getPermissionSetAssignmentsOfCurrentUser(selector.getSupervisorPermissionSets().keySet());
        }

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void isUserSystemAdmin() {
        VTT_LTNG_CalendarDAO selector = new VTT_LTNG_CalendarSelector();
        Boolean result;

        User otherUser = VTT_TestData.createVTTUser();
        System.runAs(otherUser) {
            result = selector.isUserSystemAdmin();
        }

        System.assertNotEquals(null, result);
        System.assertEquals(result, false);
    }
}