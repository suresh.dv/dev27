/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for ELG_ShiftHandoverService
History:        mbrimus 2019-07-15 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
public class ELG_ShiftHandoverServiceTest {

    @IsTest
    static void getUsersSettingsAvailablePostsAndCurrentShift_returnState() {

        // GIVEN
        HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

        // WHEN
        Test.startTest();

        ELG_DAOProvider.shiftHandoverDao = new ELG_ShiftHandoverSelectorMock();
        result = new ELG_ShiftHandoverService().getBaseShiftHandoverData();

        Test.stopTest();

        // THEN
        Map<String, Object> resultsMap = (Map<String, Object>) result.resultObjects.get(0);

        System.assertNotEquals(null, resultsMap.get(ELG_Constants.USER_SETTINGS_KEY));
        System.assertNotEquals(null, resultsMap.get(ELG_Constants.AVAILABLE_POSTS_KEY));

        ELG_User_Setting__c userSetting = (ELG_User_Setting__c)resultsMap.get(ELG_Constants.USER_SETTINGS_KEY);
        System.assertNotEquals(null, userSetting);

        List<ELG_Post__c> availablePosts = (List<ELG_Post__c>)resultsMap.get(ELG_Constants.AVAILABLE_POSTS_KEY);
        System.assertNotEquals(null, availablePosts);
        System.assertEquals(5, availablePosts.size());

        System.assertEquals(1, result.resultObjects.size());
    }

    @IsTest
    static void getAllLogsForActiveShiftAndPost_returnListOfLogs() {

        // GIVEN
        HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

        // WHEN
        Test.startTest();

        ELG_DAOProvider.shiftHandoverDao = new ELG_ShiftHandoverSelectorMock();
        result = new ELG_ShiftHandoverService().getAllLogsForActiveShiftAndPost(null, null);

        Test.stopTest();

        // THEN
        List<ELG_Log_Entry__c> entries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(1, result.resultObjects.size());
        System.assertEquals(10, entries.size());
    }

    @IsTest
    static void getHandoverDocumentByShift_returnHandoverDocument() {

        // GIVEN
        HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

        // WHEN
        Test.startTest();

        ELG_DAOProvider.shiftHandoverDao = new ELG_ShiftHandoverSelectorMock();
        result = new ELG_ShiftHandoverService().getHandoverDocumentByShift(null);

        Test.stopTest();

        // THEN
        ELG_Shift_Handover__c document = (ELG_Shift_Handover__c) result.resultObjects.get(0);

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(1, result.resultObjects.size());
        System.assertNotEquals(null, document);
    }

    @IsTest
    static void getHandoverForIncomingOperator_returnHandoverDocument() {

        // GIVEN
        HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

        // WHEN
        Test.startTest();

        ELG_DAOProvider.shiftHandoverDao = new ELG_ShiftHandoverSelectorMock();
        result = new ELG_ShiftHandoverService().getHandoverForTakePost();

        Test.stopTest();

        // THEN
        List<ELG_Shift_Handover__c> document = (List<ELG_Shift_Handover__c>) result.resultObjects.get(0);

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(1, result.resultObjects.size());
        System.assertNotEquals(null, document);
    }

    @IsTest
    static void getAllQuestionCategories_returnListOfAllCategories() {

        // GIVEN
        HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

        // WHEN
        Test.startTest();

        ELG_DAOProvider.shiftHandoverDao = new ELG_ShiftHandoverSelectorMock();
        result = new ELG_ShiftHandoverService().getAllQuestionCategories();

        Test.stopTest();

        // THEN
        List<ELG_Handover_Category__c> entries = (List<ELG_Handover_Category__c>) result.resultObjects.get(0);

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(1, result.resultObjects.size());
        System.assertEquals(10, entries.size());
    }

    @IsTest
    static void getAllActiveQuestions_returnListOfAllQuestions() {

        // GIVEN
        List<ELG_Handover_Category_Question__c> result;

        // WHEN
        Test.startTest();

        ELG_DAOProvider.shiftHandoverDao = new ELG_ShiftHandoverSelectorMock();
        result = new ELG_ShiftHandoverService().getAllActiveQuestions();

        Test.stopTest();

        // THEN
        System.assertNotEquals(null, result);
        System.assertEquals(10, result.size());
    }

    @IsTest
    static void getShiftWithMissingShiftEngineer_returnShiftWithMissingEngineer() {

        // GIVEN
        HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

        // WHEN
        Test.startTest();

        ELG_DAOProvider.shiftHandoverDao = new ELG_ShiftHandoverSelectorMock();
        result = new ELG_ShiftHandoverService().getShiftWithMissingShiftEngineer(null);

        Test.stopTest();

        // THEN
        List<ELG_Shift_Assignement__c> entry = (List<ELG_Shift_Assignement__c>) result.resultObjects.get(0);

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(2, result.resultObjects.size());
        System.assertNotEquals(null, entry);

    }

    // Mockup for Selector
    public class ELG_ShiftHandoverSelectorMock implements ELG_ShiftHandoverDAO {

        public List<ELG_User_Setting__c> getUsersSettings() {
            return (List<ELG_User_Setting__c>) HOG_SObjectFactory.createSObjectList(new ELG_User_Setting__c(),
                    1,
                    ELG_FieldDefaults.CLASS_NAME,
                    false);
        }

        public List<ELG_Post__c> getAvailablePostsForAMU(String amuId) {
            return (List<ELG_Post__c>) HOG_SObjectFactory.createSObjectList(new ELG_Post__c(),
                    5,
                    ELG_FieldDefaults.CLASS_NAME,
                    false);
        }

        public List<ELG_Shift_Assignement__c> getCurrentShiftAssignmentForUser(String amuId) {
            return (List<ELG_Shift_Assignement__c>) HOG_SObjectFactory.createSObjectList(new ELG_Shift_Assignement__c(),
                    1,
                    ELG_FieldDefaults.CLASS_NAME,
                    false);
        }

        public List<ELG_Log_Entry__c> getAllLogsForActiveShiftAndPost(String postId, String facilityId) {
            return (List<ELG_Log_Entry__c>) HOG_SObjectFactory.createSObjectList(new ELG_Log_Entry__c(),
                    10,
                    ELG_FieldDefaults.CLASS_NAME,
                    false);
        }

        public ELG_Shift_Handover__c getHandoverByShift(String shiftId) {
            return (ELG_Shift_Handover__c) HOG_SObjectFactory.createSObject(new ELG_Shift_Handover__c(),
                    ELG_FieldDefaults.CLASS_NAME, false);
        }

        public List<ELG_Shift_Handover__c> getHandoverForTakePost(String amu) {
            return (List<ELG_Shift_Handover__c>) HOG_SObjectFactory.createSObjectList(new ELG_Shift_Handover__c(),
                    10,
                    ELG_FieldDefaults.CLASS_NAME, false);
        }

        public List<ELG_Handover_Category__c> getAllQuestionCategories() {
            return (List<ELG_Handover_Category__c>) HOG_SObjectFactory.createSObjectList(new ELG_Handover_Category__c(),
                    10,
                    ELG_FieldDefaults.CLASS_NAME,
                    false);
        }

        public List<ELG_Handover_Category_Question__c> getAllActiveQuestions() {
            return (List<ELG_Handover_Category_Question__c>) HOG_SObjectFactory.createSObjectList(new ELG_Handover_Category_Question__c(),
                    10,
                    ELG_FieldDefaults.CLASS_NAME,
                    false);
        }

        public List<ELG_Shift_Assignement__c> getSteamShiftWithMissingShiftEngineer(String amuId) {
            return (List<ELG_Shift_Assignement__c>) HOG_SObjectFactory.createSObjectList(new ELG_Shift_Assignement__c(),
                    1,
                    ELG_FieldDefaults.CLASS_NAME,
                    false);
        }
    }
}