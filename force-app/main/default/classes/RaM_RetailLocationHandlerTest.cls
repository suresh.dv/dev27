/**************************************************************************************************************
  CreatedBy : Accenture
  Organization:Accenture
  Purpose   :  Test class For Trigger RaM_InsertSameRecordInLocation & RaM_RetailLocationHandler
  Version:1.0.0.1
*****************************************************************************************************************/
@isTest
Private class RaM_RetailLocationHandlerTest
{
  @testSetUp static void setup()
  {
     UserRole role1 = new UserRole(Name = 'Maintenance Tech');
     insert role1;
     
     
     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
     User u = new User(Alias = 'newUser', Email='newuser@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='testnewuser@testorg.com', UserRoleId = role1.Id);
      insert u;

      
      System.runAs(u){
        
      // Insert account
      Account accountRecord = new Account(Name = 'Husky Account',VendorGroup__c='BUNN-O-MATIC');
      accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
      insert accountRecord;
      
      //Insert Contact
      Contact contactRecord = new Contact(LastName = 'Glenn Mundi', ContactLogin__c='PRQ1234');
      contactRecord.AccountId = accountRecord.id;
      insert contactRecord;    
    }      
   
   }
    //  Method for Positive Scenario of P1 
   static testMethod void PositiveCases(){
      //Search internal husky useruser
      User u2 = [select id from user where UserName='testnewuser@testorg.com'];
    Test.startTest();  
      System.runAs(u2) {
            // Queried all records those inserted in test settup process.

            account accIns = [select id,Name  from account limit 1];
            System.assertEquals(accIns.Name,'Husky Account');   
            
            Retail_Location__c retailLoc=new Retail_Location__c();
            retailLoc.Account__c= accIns.id;
            retailLoc.Name='HUSKY01';
            retailLoc.Location_Number__c='HUSKY01';
            insert retailLoc; //insert retail location record
            
            retailLoc.Name='HUSKY01 UPDATE';
            update  retailLoc;//update retail location record
            RaM_moveRetailLocationRecordToLocation  obj = new RaM_moveRetailLocationRecordToLocation ();
            DataBase.executeBatch(obj); 

        }   
 Test.stopTest();
     }
}