/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    This class restrict deletion of records
Test Class:     any of following:
                EPD_EPDTriggerTest
                EPD_BlockInformationTriggerTest
                EPD_CylinderInformationTriggerTest
                EPD_PartReplacementTriggerTest
                EPD_EOCTriggerTest
                EPD_COCTriggerTest
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
public with sharing class EPD_DeletionRestrictor {

    /**
     * If enabled in Custom Settings, records are restricted to be deleted. Error is added to every record provided
     * through param.
     *
     * @param recordsToRestrict
     */
    public void restrictDeletion(List<SObject> recordsToRestrict) {
        if(EPD_Constants.SETTINGS != null && EPD_Constants.SETTINGS.Restrict_Record_Deletion__c) {
            if(recordsToRestrict != null) {
                for(SObject record : recordsToRestrict) {
                    record.addError(EPD_Constants.DELETE_RESTRICTION_MESSAGE);
                }
            }
        }
    }

}