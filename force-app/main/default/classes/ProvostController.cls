public class ProvostController
{
    public Boolean hasChatterAccess {get;set;}
    
    public String spotfireURL {get;set;}
    
    public String ProvostTeamChatterGroup {get;set;}
    
    public List<AssetWrapper> assets {get;set;}
    
    public class AssetWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String icon {get;set;}
        public Boolean isActive {get;set;}
        
        public AssetWrapper(String URL, String name, String icon, Boolean active)
        {
            this.name = name;
            this.URL = URL;
            this.icon = icon;
            isActive = active;
        }
    }
    
    public List<NavWrapper> navigation {get;set;}
    
    public class NavWrapper
    {
        public String  URL    {get;set;}
        public String  name   {get;set;}
        public Decimal indent {get;set;}
        
        public NavWrapper(String URL, String name, Decimal indent)
        {
            this.name = name;
            this.URL = URL;
            this.indent = indent;
        }
    }
    
    public List<TeamWrapper> teams {get;set;}
    
    public class TeamWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String pic  {get;set;}
        
        public TeamWrapper(String URL, String name, String pic)
        {
            this.name = name;
            this.URL = URL;
            this.pic = pic;
        }
    }
    
    public ProvostController()
    {
        AURASpotfireURLs__c spotfireURLs = AURASpotfireURLs__c.getOrgDefaults();
        
        spotfireURL = spotfireURLs.ProvostSpotfireSession__c;
        
        List<String> chatterNames = new List<String>{'Provost/Dodsland','Provost Reservoir','Provost Development','Provost D&C','Provost Production Ops','Provost Finance','Provost G&G','Provost BPI'};
        
        Map<String,Id> chatterName2Id = new Map<String,Id>();
        
        for(CollaborationGroup cg : [SELECT Id,Name FROM CollaborationGroup WHERE Name IN: chatterNames])
        {
            chatterName2Id.put(cg.Name,cg.Id);
        }
        
        ProvostTeamChatterGroup = '';
        if(chatterName2Id.containsKey('Provost/Dodsland'))
            ProvostTeamChatterGroup = chatterName2Id.get('Provost/Dodsland');
        
        // Does the current user have access to the Provost Chatter Group?
        hasChatterAccess = false;
        List<CollaborationGroupMember> ProvostGroupMembers = [SELECT Id FROM CollaborationGroupMember WHERE CollaborationGroupId =: ProvostTeamChatterGroup AND MemberId =: UserInfo.getUserId()];
        if(ProvostGroupMembers.size() > 0)
            hasChatterAccess = true;
        
        assets = new List<AssetWrapper>();
        assets.add(new AssetWrapper('/apex/WCPWells?show=Western Canada Production', 'Wells',  'wells.png', true));
        assets.add(new AssetWrapper('#','Routes / Pipelines', 'pipelines.png', false));
        assets.add(new AssetWrapper('#','Facilities', 'facilities.png', false));
        
        teams = new List<TeamWrapper>();
        
        if(chatterName2Id.containsKey('Provost Reservoir'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Provost Reservoir'),'Reservoir','Reservoir.png'));
        else
            teams.add(new TeamWrapper('#','Reservoir','Reservoir.png'));
        
        if(chatterName2Id.containsKey('Provost G&G'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Provost G&G'),'G & G','GandG.jpg'));
        else
            teams.add(new TeamWrapper('#','G & G','GandG.jpg'));
        
        if(chatterName2Id.containsKey('Provost Development'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Provost Development'),'Development','Development.jpg'));
        else
            teams.add(new TeamWrapper('#','Development','Development.jpg'));
        
        if(chatterName2Id.containsKey('Provost Production Ops'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Provost Production Ops'),'Production Ops','Production.jpg'));
        else
            teams.add(new TeamWrapper('#','Production Ops','Production.jpg'));
        
        if(chatterName2Id.containsKey('Provost BPI'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Provost BPI'),'BPI','BPI.jpg'));
        else
            teams.add(new TeamWrapper('#','BPI','BPI.jpg'));
        
        if(chatterName2Id.containsKey('Provost Finance'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Provost Finance'),'Finance','Finance.jpg'));
        else
            teams.add(new TeamWrapper('#','Finance','Finance.jpg'));
        
        if(chatterName2Id.containsKey('Provost D&C'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Provost D&C'),'D & C','DandC.png'));
        else
            teams.add(new TeamWrapper('#','D & C','DandC.png'));
    }
}