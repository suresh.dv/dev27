/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:
 *  Test Class		ELG_TaskSearch_EndpointTest
 *  History:        Created on 10/28/2019   
 */

public class ELG_TaskSearch_Endpoint {

	//All Posts based on selected AMU
	@AuraEnabled
	public static List<ELG_TaskSearch_EndpointService.TemporaryClassToBuildObject> getAMUsPosts(String currentAMU) {
		return ELG_TaskSearch_EndpointService.getAvailablePosts(currentAMU);
	}

	//
	@AuraEnabled
	public static List<ELG_TaskSearch_EndpointService.DataTableWrapper> getSearchResults(
			Object filterSelection,
			Integer offsetSize,
			String fieldSort,
			String sortDirection,
			Boolean defaultView,
			Boolean completedView,
			Boolean cancelledView) {
		return ELG_TaskSearch_EndpointService.searchResults(
				filterSelection,
				offsetSize,
				fieldSort,
				sortDirection,
				defaultView,
				completedView,
				cancelledView);
	}


}