@isTest
private class SFSecurityToOracle_Test
{
    static testMethod void test()
    {
        // Create a user

        Profile p = [SELECT Id
                     FROM Profile
                     WHERE Name = 'WCP No Access']; 

/* OLD STATEMENT REMOVED BY GUI MANDERS ON 2015-12-09
        UserRole r = [SELECT Id
                      FROM UserRole
                      WHERE DeveloperName = 'GRD_Manager'];
*/

        List<UserRole> lRoles = [SELECT Id FROM UserRole WHERE DeveloperName = 'GRD_Manager' LIMIT 1];
        UserRole r = (lRoles.size() == 1) ? lRoles[0] : new UserRole(Name = 'Test Role');

        User u = new User(Alias = 'test',
                          Email='SFSecurityToOracletestuser@huskyenergy.com', 
                          EmailEncodingKey='UTF-8',
                          LastName='Test',
                          LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US',
                          ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles',
                          UserName='SFSecurityToOracletestuser@huskyenergy.com');
        
        insert u;
        
        PermissionSet ps = [SELECT Id
                            FROM PermissionSet
                            WHERE Name = 'WCP_Editor'];
        
        PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = u.Id,
                                                                  PermissionSetId = ps.Id);
        
        test.startTest();
        
        SFSecurityToOracle sfsto = new SFSecurityToOracle();
        
        // Generate the XML
        
        DOM.Document doc = sfsto.fetchSecurity();
        
        // Send it
        
        SFSecurityToOracle.sendXMLtoMule(doc.toXmlString());
        
        test.stopTest();
    }
}