/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for EquipmentEngineEndpoint
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EquipmentEngineEndpointTest {

    @IsTest
    static void refreshEngineData_withoutParam() {
        HOG_SimpleResponse response;
        Boolean expectedSuccessFlag = false;

        Test.startTest();
        response = EquipmentEngineEndpoint.refreshEngineData(null);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
    }

    @IsTest
    static void refreshEngineData_withEmptyParam() {
        HOG_SimpleResponse response;
        Boolean expectedSuccessFlag = true;

        Test.startTest();
        response = EquipmentEngineEndpoint.refreshEngineData(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
        System.assertEquals(Label.EPD_Refresh_Engine_Data_Success_Message, response.successMessage);
    }

    @IsTest
    static void refreshEngineData_mockedFail() {
        HOG_SimpleResponse response;
        Boolean expectedSuccessFlag = false;

        Test.startTest();
        EPD_DAOProvider.equipmentDAO = new DAOMockFail();
        response = EquipmentEngineEndpoint.refreshEngineData(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
    }

    @IsTest
    static void refreshEngineData_mockedFailOnInsert() {
        HOG_SimpleResponse response;
        Boolean expectedSuccessFlag = false;

        Test.startTest();
        EPD_DAOProvider.equipmentDAO = new DAOMock();
        response = EquipmentEngineEndpoint.refreshEngineData(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(expectedSuccessFlag, response.success);
    }

    public class DAOMock implements EquipmentDAO {
        public List<Equipment__c> getEquipments(Set<Id> equipmentIds) {
            return new List<Equipment__c>{
                    (Equipment__c) HOG_SObjectFactory.createSObject(new Equipment__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false)
            };
        }
    }

    public class DAOMockFail implements EquipmentDAO {
        public List<Equipment__c> getEquipments(Set<Id> equipmentIds) {
            Equipment__c eq = (Equipment__c) HOG_SObjectFactory.createSObject(new Equipment__c(), EPD_FieldDefaultsGeneral.CLASS_NAME, false);
            eq.Object_Type__c = 'WrongObjectType';
            return new List<Equipment__c>{
                    eq
            };
        }
    }

}