/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Utility class for Truck Trip object
Testing Class:  FM_CancelTruckTripControllerXTest
                FM_TruckTripExportControllerTest
                FM_TruckTripRenewConfirmCtrlTest
                FM_TruckTrip_UtilitiesTest
History:        W-001431
				mg - 03.04.2019
					- added new method to get field picklist values
**************************************************************************************************/
public with sharing class FM_TruckTrip_Utilities {

	@testVisible private static Boolean skipInTest = true;

	public static final String CLASS_NAME = 'FM_TruckTrip_Utilities';

	public static final String PARAM_ID = 'Id';

	public static final String STATUS_NEW = 'New';
	public static final String STATUS_CARRYOVER = 'Carryover';
	public static final String STATUS_REDISPATCH = 'Redispatch';
	public static final String STATUS_BOOKED = 'Booked';
	public static final String STATUS_GIVENBACK = 'Given Back';
	public static final String STATUS_DISPATCHED = 'Dispatched';
	public static final String STATUS_ADDON = 'Add On';
	public static final String STATUS_EXPORTED = 'Exported';
	public static final String STATUS_COMPLETED = 'Completed';
	public static final String STATUS_CANCELLED = 'Cancelled';
	public static final String STATUS_UNCANCELLED = 'Uncancelled';

	public static final String ERROR_WRONG_STATUS = Label.FM_TruckTrip_Err_Wrong_Status.replace('###', STATUS_CANCELLED);
	public static final String ERROR_WRONG_ID_PARAM = Label.FM_TruckTrip_Err_NotFound;
	public static final String ERROR_RENEW_NOTSUPPORTED = Label.FM_TruckTrip_Err_RenewNotSupported;
	public static final String MODE_TITLE = Label.FM_Renew_Title;
	public static final String LABEL_OLD_TRUCKTRIP = Label.FM_TruckTrip_OldTruckTrip;

	public static final String FIELD_STATUS = 'Truck_Trip_Status__c';
	public static final String FIELD_UNIT = 'Unit__c';
	public static final String FIELD_WELL = 'Well__c';
	public static final String FIELD_FACILITY = 'Facility_Lookup__c';
	public static final String FIELD_LOCATION = 'Location_Lookup__c';
	public static final String FIELD_FACILITY_TYPE = 'Facility_Type__c';

	public static final String UNCANCEL_MODE_RENEW = 'Renew';
	public static final String UNCANCEL_MODE_CONFIRM = 'Confirm';

	public static final Set<String> historyIdFieldsToCast = new Set<String>(new List<String>{
			FIELD_FACILITY
			, FIELD_LOCATION
			, FIELD_WELL
	});

	public static final Set<String> historyFieldsConfirm = new Set<String>(new List<String>{
			FIELD_UNIT
			, FIELD_WELL
			, FIELD_FACILITY
			, FIELD_LOCATION
			, FIELD_FACILITY_TYPE
	});

	public static final Set<String> historyFieldsRenew = new Set<String>(new List<String>{
			FIELD_UNIT
			, FIELD_WELL
			, FIELD_FACILITY
			, FIELD_LOCATION
			, FIELD_STATUS
			, FIELD_FACILITY_TYPE
	});

	public static final Set<String> confirmSupportedStatuses = new Set<String>(new List<String>{
			STATUS_DISPATCHED
			, STATUS_ADDON
			, STATUS_EXPORTED
	});

	public static final Set<String> renewSupportedStatuses = new Set<String>(new List<String>{
			STATUS_NEW
			, STATUS_CARRYOVER
			, STATUS_REDISPATCH
			, STATUS_BOOKED
			, STATUS_EXPORTED
	});
	public static final Set<String> renewNotSupportedStatuses = new Set<String>(new List<String>{
			STATUS_DISPATCHED
			, STATUS_ADDON
	});

	public static final Set<String> renewConfirmUnsupportedStatuses = new Set<String>(new List<String>{
			STATUS_COMPLETED
			, STATUS_CANCELLED
			, STATUS_GIVENBACK
			, STATUS_UNCANCELLED
	});

	public static final List<SelectOption> uncancelModes = new List<SelectOption>{
			new SelectOption(UNCANCEL_MODE_RENEW, UNCANCEL_MODE_RENEW)
			, new SelectOption(UNCANCEL_MODE_CONFIRM, UNCANCEL_MODE_CONFIRM)
	};

	public static List<SelectOption> getUncancelModes(Boolean isTruckTripConfirmable, Boolean isTruckTripRenewable) {
		List<SelectOption> modes = new List<SelectOption>();
		if (isTruckTripRenewable) modes.add(new SelectOption(UNCANCEL_MODE_RENEW, UNCANCEL_MODE_RENEW));
		if (isTruckTripConfirmable) modes.add(new SelectOption(UNCANCEL_MODE_CONFIRM, UNCANCEL_MODE_CONFIRM));
		return modes;
	}

	public static final String FACILITY_TYPE_LOCATION = 'Location';
	public static final String FACILITY_TYPE_FACILITY = 'Facility';

	public static String EXPORT_HEADER = 'Truck Trip, Route, Date, Well Name, Tank Size'
			+ ', Tank Low Level, Act Tank Level, Well Flow Rate, Shift, Product'
			+ ', Load Type, Standing Comments, Unit Configuration, Sour'
			+ ', Surface Location, Additional TKs (RS = From Runsheet; LR = From Load Request; SCADA = From SCADA)';

	public static String TRUCKTRIP_FIELDS = 'Id'
			+ ';Load_Request_Cancel_Reason__c'
			+ ';Load_Request_Cancel_Comments__c'
			+ ';Carrier__c'
			+ ';Unit__c'
			+ ';Load_Request__r.Source_Location__c'
			+ ';Load_Request__r.Source_Facility__c';

	/**
	* Future call to mark truckrips as exported after export
	*/
	public static void markTruckTripsAsExported(String idString) {
		Set<String> truckTripIds = new Set<String>();
		truckTripIds.addAll(idString.split(','));

		List<FM_Truck_Trip__c> truckTripsToUpdate = [
				SELECT Id, Name, Truck_Trip_Status__c
				FROM FM_Truck_Trip__c
				WHERE Id In :truckTripIds
		];
		for (FM_Truck_Trip__c trip : truckTripsToUpdate)
			trip.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_EXPORTED;

		update truckTripsToUpdate;
	}

	/**
	 * Get Well name based on Source Location/Facility and Tank label
	 * @param  truckTrip for which you want to get well name
	 * @return           well name
	 */
	public static String getWellName(FM_Truck_Trip__c truckTrip) {
		String wellName = '';
		if (truckTrip != null) {
			if (truckTrip.Load_Request__c != null) {
				if (truckTrip.Load_Request__r.Source_Location__c != null)
					wellName = truckTrip.Load_Request__r.Source_Location__r.Name;
				if (truckTrip.Load_Request__r.Source_Facility__c != null)
					wellName = truckTrip.Load_Request__r.Source_Facility__r.Name;
			}
			if (truckTrip.Tank__c != null)
				wellName += ' / ' + truckTrip.Tank__c;
		}
		return wellName;
	}

	/**
	 * Retrieve Truck Trip record based on Id.
	 * @param  truckTripId to filter by
	 * @return             Truck Trip record or null.
	 */
	public static FM_Truck_Trip__c getTruckTripById(Id truckTripId) {
		if (String.isNotBlank(truckTripId)) {
			List<FM_Truck_Trip__c> truckTrips = [
					SELECT Id
							, Name
							, Axle__c
							, Carrier__c
							, Destination_Id__c
							, Facility__c
							, Facility_Lookup__c
							, Facility_Lookup_Original__c
							, Facility_String_Hidden__c
							, Facility_Type__c
							, Is_Reroute__c
							, Load_Request__c
							, Load_Request__r.Source_Facility__c
							, Load_Request__r.Source_Location__c
							, Load_Request__r.Cancel_Comments__c
							, Load_Request__r.Cancel_Reason__c
							, Load_Request__r.Load_Weight__c
							, Load_Request__r.Route__c
							, Load_Request__r.Status__c
							, Load_Request_Cancel_Comments__c
							, Load_Request_Cancel_Reason__c
							, Load_Type__c
							, Location_Lookup__c
							, Order__c
							, Planned_Dispatch_Date__c
							, Priority__c
							, Product__c
							, Product_Change_Comments__c
							, Product_Change_Reason__c
							, Rebook_Comments__c
							, Rebook_Reason__c
							, Run_Sheet_Date__c
							, Run_Sheet_Lookup__c
							, Shift__c
							, Shift_Day__c
							, Source__c
							, Standing_Comments__c
							, Tank__c
							, TL_Dispatched_By__c
							, TL_Dispatched_On__c
							, TL_First_Booked_By__c
							, TL_Phone_Lookup__c
							, Truck_Trip_Status__c
							, Unit__c
							, Unit_Change_Other_Reason__c
							, Unit_Change_Reason__c
							, Well__c
							, Well_Lookup_Original__c
					FROM FM_Truck_Trip__c
					WHERE Id = :truckTripId
			];
			if (truckTrips != null && truckTrips.size() > 0)
				return truckTrips.get(0);
		}
		return null;
	}

	/**
	 * Get Truck Trip history data based on truck trip id.
	 * @param  truckTripId to filter by.
	 * @return             List of Truck Trip history.
	 */
	public static List<FM_Truck_Trip__History> getTruckTripHistoryData(Id truckTripId) {
		if (String.isNotBlank(truckTripId)) {
			return [
					SELECT Field
							, OldValue
					FROM FM_Truck_Trip__History
					WHERE ParentId = :truckTripId
					ORDER BY CreatedDate DESC
			];
		}
		return null;
	}


/*******************
*HISTORY DATA RENEW*
*******************/
	/**
	 * Renew Truck Trip values from History Item based on renew mode.
	 * Renew mode serves just to get validation if field can be renewed.
	 * WARNING: This method works only for fields that are ID or String.
	 *          Needed to change it to this state bcs of Unit Tests.
	 * @param  truckTrip    to restore
	 * @param  historyItem  value to restore
	 * @param  uncancelMode mode for renew
	 * @return              renewed Truck Trip
	 */
	public static FM_Truck_Trip__c setValueFromHistoryItem(FM_Truck_Trip__c truckTrip,
			String fieldName,
			String fieldValue,
			String uncancelMode) {
		if ((UNCANCEL_MODE_CONFIRM.equals(uncancelMode)
				&& ableRestoreHistoryValueConfirm(truckTrip, fieldName, fieldValue))
				|| (UNCANCEL_MODE_RENEW.equals(uncancelMode)
				&& ableRestoreHistoryValueRenew(truckTrip, fieldName, fieldValue))) {
			assignHistoricalValue(truckTrip, fieldName, fieldValue);
		}
		return truckTrip;
	}

	/**
	 * Assign Historical Value from history item.
	 * @param truckTrip   to restore
	 * @param historyItem value to restore
	 */
	private static void assignHistoricalValue(FM_Truck_Trip__c truckTrip,
			String fieldName,
			String fieldValue) {
		if (historyIdFieldsToCast.contains(fieldName)) {
			try {
				Id lookupId = (Id) fieldValue;
				truckTrip.put(fieldName, lookupId);
			} catch (Exception ex) {
				System.debug(CLASS_NAME + '-> Handled exception (Casting into ID).');
			}
		} else truckTrip.put(fieldName, fieldValue);
		System.debug(CLASS_NAME + '-> historical value assignment.');
		System.debug(CLASS_NAME + '-> history Field: ' + fieldName);
		System.debug(CLASS_NAME + '-> history Value: ' + fieldValue);
		System.debug(CLASS_NAME + '-> after assign truck trip state: ' + truckTrip);
	}

	/**
	 * Validation if renew can be done for this field and value for Confirm mode.
	 * @param  truckTrip   to restore
	 * @param  historyItem value to restore
	 * @return             if history is restorable
	 */
	private static Boolean ableRestoreHistoryValueConfirm(FM_Truck_Trip__c truckTrip,
			String fieldName,
			String fieldValue) {
		return historyFieldsConfirm.contains(fieldName)
				&& ableRestoreHistoryValue(truckTrip, fieldName, fieldValue);
	}

	/**
	 * Validation if renew can be done for this field and value for Renew mode.
	 * @param  truckTrip   to restore
	 * @param  historyItem value to restore
	 * @return             if history is restorable
	 */
	private static Boolean ableRestoreHistoryValueRenew(FM_Truck_Trip__c truckTrip,
			String fieldName,
			String fieldValue) {
		return historyFieldsRenew.contains(fieldName)
				&& ableRestoreHistoryValue(truckTrip, fieldName, fieldValue);
	}

	/**
	 * Validation if renew can be done for this field and value.
	 * There is check if
	 * HistoryItem has field populated
	 * Truck Trip has that field unpopulated (exception for Truck Trip Status when it is in Cancelled status)
	 * HistoryItem has OldValue populated.
	 * @param  truckTrip   to restore
	 * @param  historyItem value to restore
	 * @return             if history is restorable
	 */
	private static Boolean ableRestoreHistoryValue(FM_Truck_Trip__c truckTrip,
			String fieldName,
			String fieldValue) {
		return String.isNotBlank(fieldName)
				&& (truckTrip.get(fieldName) == null
				|| String.isBlank(String.valueOf(truckTrip.get(fieldName)))
				|| STATUS_CANCELLED.equals(String.valueOf(truckTrip.get(fieldName))))
				&& String.isNotBlank(String.valueOf(fieldValue));
	}

	/**
	 * Checks if Truck Trip had correct status in history for confirm.
	 * Supported statuses are different for external and internal dispatchers
	 * @param  truckTripHistory     history of truck trip to check
	 * @param  isExternalDispatcher User has ext dispatcher PS.
	 * @return                      if history contains supported status
	 */
	public static Boolean isTruckTripConfirmable(List<FM_Truck_Trip__History> truckTripHistory
			, Boolean isExternalDispatcher) {
		if (isExternalDispatcher)
			return isTruckTripConfirmableForExternalUser(truckTripHistory);
		return isTruckTripConfirmableForInternalUser(truckTripHistory);
	}

	/**
	 * Checks if Truck Trip had correct status in history for confirm.
	 * Correct statuses is Exported.
	 * @param  truckTripHistory history of truck trip to check
	 * @return                  if history contains supported status
	 */
	private static Boolean isTruckTripConfirmableForExternalUser(List<FM_Truck_Trip__History> truckTripHistory) {
		if (Test.isRunningTest() && skipInTest) return true;
		for (FM_Truck_Trip__History historyItem : truckTripHistory)
			if (FIELD_STATUS.equals(historyItem.Field)
					&& STATUS_EXPORTED.contains(String.valueOf(historyItem.OldValue)))
				return true;
		return false;
	}

	/**
	 * Checks if Truck Trip had correct status in history for confirm.
	 * Correct statuses are stored inside confirmSupportedStatuses set.
	 * @param  truckTripHistory history of truck trip to check
	 * @return                  if history contains supported status
	 */
	private static Boolean isTruckTripConfirmableForInternalUser(List<FM_Truck_Trip__History> truckTripHistory) {
		if (Test.isRunningTest() && skipInTest) return true;
		for (FM_Truck_Trip__History historyItem : truckTripHistory)
			if (FIELD_STATUS.equals(historyItem.Field)
					&& confirmSupportedStatuses.contains(String.valueOf(historyItem.OldValue)))
				return true;
		return false;
	}

	/**
	 * Checks if Truck Trip had correct status in history for renew.
	 * Correct statuses are stored inside renewSupportedStatuses set.
	 * If external dispatcher return false.
	 * @param  truckTripHistory     history of truck trip to check
	 * @param  isExternalDispatcher User has ext dispatcher PS.
	 * @return                      if history contains supported status
	 */
	public static Boolean isTruckTripRenewable(List<FM_Truck_Trip__History> truckTripHistory
			, Boolean isExternalDispatcher) {
		if (Test.isRunningTest() && skipInTest) return true;
		if (isExternalDispatcher) return false;
		for (FM_Truck_Trip__History historyItem : truckTripHistory)
			if (FIELD_STATUS.equals(historyItem.Field)
					&& renewNotSupportedStatuses.contains(String.valueOf(historyItem.OldValue)))
				return false;
		return truckTripHistory != null
				&& truckTripHistory.size() > 0;
	}

	public static Boolean wasTruckTripExported(List<FM_Truck_Trip__History> truckTripHistory) {
		if (Test.isRunningTest() && skipInTest) return true;
		for (FM_Truck_Trip__History historyItem : truckTripHistory) {
			if (FIELD_STATUS.equals(historyItem.Field)
					&& STATUS_EXPORTED.equals(historyItem.OldValue)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Picklist values from Salesforce
	 *
	 * @return List of picklist values
 	*/
	public static List<SelectOption> getUnitChangeReason() {
		List<SelectOption> unitChangeReasonOptions = new List<SelectOption>();
		for (Schema.PicklistEntry ple : FM_Truck_Trip__c.Unit_Configuration_Change_Reason__c.getDescribe().getPicklistValues()) {
			unitChangeReasonOptions.add(new SelectOption(ple.getLabel(), ple.getValue()));
		}
		return unitChangeReasonOptions;
	}
}