public class CarsealControllerExt
{
    private ApexPages.standardController std;
    private CarSeal__c carseal;
    public Boolean isClone {get; private set;}
    
    // object to encapsulate the entered value and any error message associated
    public ValueAndError plantId {get; set;}
    public ValueAndError unitId {get; set;}
    
    public CarsealControllerExt(ApexPages.standardController pstd)
    {
        std = pstd;
        carseal = (Carseal__c)std.getRecord();
        plantId = new ValueAndError();
        plantId.value = carseal.Plant__c;
        unitId = new ValueAndError();
        unitId.value = carseal.Unit__c;
        
        // check to see if it is a clone operation.  If so, we don't want to show the current Carseal Id field.
        isClone = (ApexPages.currentPage().getParameters().get('clone') == '1');
    }
    
    public List<SelectOption> getPlants()
    {
        List<SelectOption> result = new List<SelectOption>();
        result.add(new SelectOption('*', '-- Choose A Plant --'));
        for (List<Plant__c> plantList : [SELECT Name FROM Plant__c ORDER BY Name])
        {
            for (Plant__c p : plantList)
            {
                result.add(new SelectOption(p.Id, p.Name));
            }
        }
        return result;
    }
    
    public PageReference clearUnit()
    {
        system.debug('clear unit');
        unitId.value = '*';
        return null;
    }
    
    public List<SelectOption> getUnits()
    {
        system.debug('getUnits');
        List<SelectOption> result = new List<SelectOption>();
        result.add(new SelectOption('*', '-- Choose A Unit --'));

        if ('*' != plantId.value)
        {
            for (List<Unit__c> unitList : [SELECT Name FROM Unit__c WHERE Plant__c = :plantId.value])
            {
                for (Unit__c u : unitList)
                {
                    result.add(new SelectOption(u.Id, u.Name));
                }
            }
        }
        return result;
    }
    
    public PageReference save()
    {
        plantId.error = '';
        unitId.error = '';
        Boolean hasError = false;
        
        if ('*' == plantId.value)
        {
            plantId.error = 'You must enter a value';
            hasError = true;
        }
        if ('*' == unitId.value)
        {
            unitId.error = 'You must enter a value';
            hasError = true;
        }
        if (hasError)
        {
            return null;
        }
        else
        {
            carseal.Plant__c = plantId.value;
            carseal.Unit__c = unitId.value;
            return std.save();          
        }
    }
    
    // custom class to encapsulate value and error
    public class ValueAndError
    {
        public String value {get; set;}
        public String error {get; set;}
        
        public ValueAndError()
        {
                value = '';
                error = '';
        }
        
        public Boolean getHasError()
        {
            return (!String.isEmpty(error));
        }
    }
    
    //method to retrieve most recent CarSeal ID
    public String getHelpText() {
        String hText;
        CarSeal__c cSeal = [SELECT Id, Name FROM CarSeal__c ORDER BY Name DESC LIMIT 1];
        hText = 'The most recent saved record Car Seal ID is: '+cSeal.Name+'. Please enter next immediate Car Seal ID Number';
        return hText;
    } 
    
}