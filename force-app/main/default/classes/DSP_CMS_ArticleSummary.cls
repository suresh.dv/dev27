global virtual with sharing class DSP_CMS_ArticleSummary extends DSP_CMS_ArticleController
{
    global DSP_CMS_ArticleSummary(cms.GenerateContent cc)
    {
        super(cc);
    }
    
    global DSP_CMS_ArticleSummary()
    {
        super();
    }
    
    global override String getHTML()
    {
        return articleSummaryHTML();
    }
}