/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VG_AlertService class
History:        jschn 24/01/2020 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class VG_AlertServiceTest {

    @IsTest
    static void isValidLocation_mocked_noLocation() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_NoLocation();
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        Boolean result;
        Boolean expectedResult;
        String errorMsg;
        String expectedErrorMsg = Label.VG_Invalid_Location;

        try {
            result = service.isValidLocation(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedErrorMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidLocation_mocked_multipleLocations() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_MultipleLocation();
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        Boolean result;
        Boolean expectedResult;
        String errorMsg;
        String expectedErrorMsg = Label.VG_Invalid_Location;

        try {
            result = service.isValidLocation(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedErrorMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidLocation_mocked_singleLocationInvalid() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_SingleLocation();
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        Boolean result;
        Boolean expectedResult;
        String errorMsg;
        String expectedErrorMsg = Label.VG_Invalid_Location;

        try {
            result = service.isValidLocation(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedErrorMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidLocation_mocked_singleLocationValid() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_SingleLocationValid();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Boolean result;
        Boolean expectedResult = true;
        String errorMsg;
        String expectedErrorMsg;

        try {
            result = service.isValidLocation(UserInfo.getUserId());
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedErrorMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    class LocationMock_NoLocation implements LocationDAO {
        public List<Location__c> getLocationById(Id wellId) {
            return new List<Location__c>();
        }
    }
    class LocationMock_SingleLocation implements LocationDAO {
        public List<Location__c> getLocationById(Id wellId) {
            return new List<Location__c>{
                    new Location__c(Name = 'Test')
            };
        }
    }
    class LocationMock_SingleLocationValid implements LocationDAO {
        public List<Location__c> getLocationById(Id wellId) {
            Field__c amu = new Field__c(
                    Planner_Group__c = '100'
            );
            amu.recalculateFormulas();
            Location__c loc = new Location__c(
                    Name = 'Test',
                    Functional_Location_Category__c = 4,
                    Well_Type__c='OIL',
                    Operating_Field_AMU__r = amu
            );
            return new List<Location__c>{loc};
        }
    }
    class LocationMock_MultipleLocation implements LocationDAO {
        public List<Location__c> getLocationById(Id wellId) {
            return new List<Location__c>{
                    new Location__c(Name = 'Test1'),
                    new Location__c(Name = 'Test2')
            };
        }
    }


    @IsTest
    static void getProductionEngineer_mocked_noLocation() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_NoLocation();
        Id result;
        Id expectedResult;

        result = service.getProductionEngineer(UserInfo.getUserId());

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getProductionEngineer_mocked_multipleLocation() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_MultipleLocation();
        Id result;
        Id expectedResult;

        result = service.getProductionEngineer(UserInfo.getUserId());

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getProductionEngineer_mocked_AMUWithoutPE() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_withoutPE();
        Id result;
        Id expectedResult;

        result = service.getProductionEngineer(UserInfo.getUserId());

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void getProductionEngineer_mocked_AMUWithPE() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.locationSelector = new LocationMock_withPE();
        Id result;
        Id expectedResult = UserInfo.getUserId();

        result = service.getProductionEngineer(UserInfo.getUserId());

        System.assertEquals(expectedResult, result);
    }

    class LocationMock_withPE implements LocationDAO {
        public List<Location__c> getLocationById(Id wellId) {
            Location__c loc = new Location__c(
                    Operating_Field_AMU__r = new Field__c(
                            Production_Engineer_User__c = UserInfo.getUserId()
                    )
            );
            return new List<Location__c>{loc};
        }
    }
    class LocationMock_withoutPE implements LocationDAO {
        public List<Location__c> getLocationById(Id wellId) {
            Location__c loc = new Location__c(
                    Operating_Field_AMU__r = new Field__c()
            );
            return new List<Location__c>{loc};
        }
    }

}