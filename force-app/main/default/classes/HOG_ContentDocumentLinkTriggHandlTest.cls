/**
 * Created by MarcelBrimus on 21/02/2019.
 */
@IsTest
private class HOG_ContentDocumentLinkTriggHandlTest {

    @IsTest static void whenFileIsAttachedOnWorkOrder_itsSharedWithCommunity() {
        User runningUser = VTT_TestData.createVTTAdminUser();

        System.runAs(runningUser) {

            // Given
            HOG_Maintenance_Servicing_Form__c workOrder = [SELECT Id FROM HOG_Maintenance_Servicing_Form__c LIMIT 1];

            // Insert File
            ContentVersion cv = new ContentVersion();
            cv.Title = 'ABC';
            cv.PathOnClient = 'test';
            cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            insert cv;

            ContentVersion filesWithDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];

            ContentDocumentLink link = new ContentDocumentLink();
            link.ContentDocumentId = filesWithDocId.ContentDocumentId;
            link.LinkedEntityId = workOrder.Id;
            link.Visibility = 'InternalUsers';
            link.ShareType = 'C';
            insert link;

            // When
            ContentDocumentLink linkToTest = [SELECT ShareType, Visibility FROM ContentDocumentLink WHERE Id = :link.Id LIMIT 1];

            // Then
            System.assertEquals('I', linkToTest.ShareType);
            System.assertEquals('AllUsers', linkToTest.Visibility);
        }
    }

    @IsTest static void whenFileIsAttachedOnWorkOrderActivity_itsSharedWithCommunity() {
        User runningUser = VTT_TestData.createVTTAdminUser();

        System.runAs(runningUser) {

            // Given
            Work_Order_Activity__c workOrderActivity = [SELECT Id FROM Work_Order_Activity__c LIMIT 1];

            // Insert File
            ContentVersion cv = new ContentVersion();
            cv.Title = 'ABC';
            cv.PathOnClient = 'test';
            cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            insert cv;

            ContentVersion filesWithDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];

            ContentDocumentLink link = new ContentDocumentLink();
            link.ContentDocumentId = filesWithDocId.ContentDocumentId;
            link.LinkedEntityId = workOrderActivity.Id;
            link.Visibility = 'InternalUsers';
            link.ShareType = 'C';
            insert link;

            // When
            ContentDocumentLink linkToTest = [SELECT ShareType, Visibility FROM ContentDocumentLink WHERE Id = :link.Id LIMIT 1];

            // Then
            System.assertEquals('I', linkToTest.ShareType);
            System.assertEquals('AllUsers', linkToTest.Visibility);
        }
    }

    @IsTest static void whenFileIsAttachedOnAMU_itsNOTSharedWithCommunity() {
        User runningUser = VTT_TestData.createVTTAdminUser();

        System.runAs(runningUser) {

            // Given
            Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

            // Insert File
            ContentVersion cv = new ContentVersion();
            cv.Title = 'ABC';
            cv.PathOnClient = 'test';
            cv.VersionData = EncodingUtil.base64Decode('Unit Test Attachment Body');
            insert cv;

            ContentVersion filesWithDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id LIMIT 1];

            ContentDocumentLink link = new ContentDocumentLink();
            link.ContentDocumentId = filesWithDocId.ContentDocumentId;
            link.LinkedEntityId = amu.Id;
            link.Visibility = 'InternalUsers';
            link.ShareType = 'V';
            insert link;

            // When
            ContentDocumentLink linkToTest = [SELECT ShareType, Visibility FROM ContentDocumentLink WHERE Id = :link.Id LIMIT 1];

            // Then
            System.assertEquals('V', linkToTest.ShareType);
            System.assertEquals('InternalUsers', linkToTest.Visibility);
            System.assertNotEquals('I', linkToTest.ShareType);
            System.assertNotEquals('AllUsers', linkToTest.Visibility);
        }
    }


    @TestSetup
    private static void CreateTestData() {
        HOG_Maintenance_Servicing_Form__c wo = VTT_TestDataFactory.createWorkOrder(true);
        Field__c amu = HOG_TestDataFactory.createAMU('test amu', true);
        VTT_TestDataFactory.createWorkOrderActivity('New', 'Test woa', wo.Id, amu.Id, true);
    }
}