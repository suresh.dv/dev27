public with sharing class PrintableChangeRequestFormCtrl {
    private Change_Request__c req;
    public Impact_Assessor__c impactAss { get; set; }
    public class CustomWrapper
    {
        public boolean isSelected {get;set;}
        public String name {get;set;}
        public CustomWrapper(String strName)
        {
            isSelected = false;
            name = strName;
        }
    }
    public PrintableChangeRequestFormCtrl(ApexPages.StandardController std)
    {
        req = (Change_Request__c)std.getRecord();   
        impactAss = new Impact_Assessor__c();
        List<Impact_Assessor__c> impactAssList = [ Select Assessor__c, Assessor__r.Name, Impact_Assessment_Description__c,
                                                    OPEX_Cost__c, TIC_Cost__c, Risk_Description__c,
                                                    Impact_Assessment_Complete__c, Implementation_Plan__c,
                                                    Required_Resources__c, Scheduled_Impact_Comments__c,
                                                    Contract_Commercial_Impact__c, Regulatory_Implications__c,
                                                    Change_Impact_Hours__c, TIC_Schedule_Impact__c, Cost_Impact__c,
                                                    Requested_By__c from Impact_Assessor__c
                                                    where Change_Request__c =: req.id limit 1 ];
        if( impactAssList.size() > 0 )
            impactAss = impactAssList[0];
    }
    public integer getCostImpactListSize()
    {
        if (req.CR_Cost_Impacts__r != null)
           return req.CR_Cost_Impacts__r.size();
        return 0;   
    }
    
    /*public String getLogoURL()
    {
        String url = '/servlet/servlet.FileDownload?file=';
        List<Document> docs = [select Id from Document where DeveloperName = 'Husky_Oil_Logo'];
        if (docs.size() > 0)
           url += docs[0].Id;
        return url;   
    }*/
  
    public List<String> getJustificationOptions()
    {
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = Change_Request__c.Justification__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        integer index = 0;    
        for( Schema.PicklistEntry f : ple)
        {
           options.add(f.getValue());
           index ++;
           if (index == 7)
               break;
        }
        return options;
    } 
    
    public List<CustomWrapper> getProjectAreas()
    {
        List<CustomWrapper> options = new List<CustomWrapper>();
        
        for(Project__c prog : [select Id, Name from Project__c])
        {
           CustomWrapper obj = new CustomWrapper(prog.Name);
           for(CR_Affected_Program_Area__c eprog : [select Id, Project__c from CR_Affected_Program_Area__c where Change_Request__c =:req.Id])
               if (eprog.Project__c == prog.Id)
                   obj.isSelected = true;
           options.add(obj);    
        }
              
        return options;   
  
    }    
    
    public List<CustomWrapper> getProgramAreas()
    {
        List<CustomWrapper> options = new List<CustomWrapper>();
        
        for(CR_Program_Area__c prog : [select Id, Name from CR_Program_Area__c])
        {
           CustomWrapper obj = new CustomWrapper(prog.Name);
           for(CR_Affected_Program_Area__c eprog : [select Id, CR_Program_Area__c from CR_Affected_Program_Area__c where Change_Request__c =:req.Id])
               if (eprog.CR_Program_Area__c == prog.Id)
                   obj.isSelected = true;
           options.add(obj);    
        }
              
        return options;   
  
    }
    public List<CustomWrapper> getImpactedFunctions()
    {
        List<CustomWrapper> options = new List<CustomWrapper>();
        
        for(CR_Impacted_Functions__c prog : [select Id, Name from CR_Impacted_Functions__c])
        {
           CustomWrapper obj = new CustomWrapper(prog.Name);
           for(CR_Affected_Impacted_Function__c eprog : [select Id, CR_Impacted_Function__c from CR_Affected_Impacted_Function__c where Change_Request__c =:req.Id])
               if (eprog.CR_Impacted_Function__c == prog.Id)
                   obj.isSelected = true;
           options.add(obj);    
        }
              
        return options;  
    }
    public List<CR_Approver__c> getApprovers()
    {
        return [select Id, Name, Approver__r.Title, Order__c from CR_Approver__c where Change_Request__c =: req.Id order by Order__c];
    }
    
    public ProcessInstance getSPAApprovalPro()
    {
        List<ProcessDefinition> process = [select Id from ProcessDefinition where DeveloperName = 'SPA_Review'];
        if (process != null && process.size() > 0)
        {
            List<ProcessInstance> approvalStep = [select Id, LastModifiedDate, Status from ProcessInstance 
                  where TargetObjectId =: req.Id and ProcessDefinitionId =: process[0].Id and Status = 'Approved'];
            if (approvalStep != null && approvalStep.size() > 0)
              return approvalStep[0];
        }
        return null;        
    
    }
    public Datetime getPMApprovedDate()
    {
        List<Change_Request__History> history = [select CreatedDate, NewValue from Change_Request__History 
                   where ParentId =: req.Id and Field = 'Recommended__c' ];
        if (history != null && history.size() > 0)
        {
            if (history[0].NewValue == 'Recommend')
               return history[0].CreatedDate;
        }
        return null;                 
    }
}