@isTest
public class UtilitiesTestSuite{
    @isTest static void testStringUtilities() {
        try {
            String original = 'Test Method';
            String expectedResult = 'TestMethod';
            
            String actualResult = StringUtilities.stripWhiteSpaces(original);
            
            System.assert((actualResult == expectedResult));
        } catch(Exception ex) {
            System.assert(false, 'Error testing String utilities. ' + ex.getMessage());
        }
    }
    
    @isTest static void testDateUtilities() {
        try {
        	// Test toDateTime function.
        	DateTime resultDateTime = DateUtilities.toDateTime(Date.today());
        	System.assert(resultDateTime != null);
        	
        	// Test date parser.
        	DateTime resultDate = DateUtilities.toMDYYYDate('12/31/2013', '/', null);
        	System.assert(resultDate != null);
        } catch(Exception ex) {
            System.assert(false, 'Error testing Date utilities. ' + ex.getMessage());
        }    	
    }
    
    @isTest static void testSortableSunriseUnit() {
        List<SortableSunriseUnit> units = new List<SortableSunriseUnit>();
        
        units.add(new SortableSunriseUnit('50', 'CR 1A'));
        units.add(new SortableSunriseUnit('CR 1A', null));
        units.add(new SortableSunriseUnit('10', 'CR 1A'));
        units.add(new SortableSunriseUnit('00', 'CR 1A'));
        units.add(new SortableSunriseUnit('01', 'CR 1B'));
        units.add(new SortableSunriseUnit('CR 1B', null));
        units.add(new SortableSunriseUnit('81', 'CR 1B'));
        units.add(new SortableSunriseUnit('11', 'CR 1B'));
        units.add(new SortableSunriseUnit('OF', 'CR FF'));
        units.add(new SortableSunriseUnit('OC', 'CR FF'));
        units.add(new SortableSunriseUnit('OA', 'CR FF'));
        units.add(new SortableSunriseUnit('CR FF', null));
        
        units.sort();
        
        String logStr = '\n*******************************\n'
            + 'METHOD: testSortableSunriseUnit';
        
        for (Integer i = 0; i < units.size(); i++) {
            SortableSunriseUnit curUnit = units[i];
            
            logStr += '\n'
                + 'units[' + i + '].unitName = ' + curUnit.unitName
                + '\t\t'
                + 'units[' + i + '].parentName = ' + curUnit.parentName;        
        }
        
        logStr += '\n********************************\n';
        System.debug(logStr);
            
        System.assert((units[0].unitName == 'CR 1A' && units[0].parentName == null), 'Sort result is either wrong or there was an error.');
        
    }
    
    @isTest static void testSortableSunriseArea() {
        List<SortableSunriseArea> areas = new List<SortableSunriseArea>();
        
        // areas.add(new SortableSunriseArea(null));
        areas.add(new SortableSunriseArea('Control Room 1B'));
        areas.add(new SortableSunriseArea('Field'));
        areas.add(new SortableSunriseArea('Control Room 1A'));
        areas.add(new SortableSunriseArea('Field Panel'));
        areas.add(new SortableSunriseArea('Process 1A'));
        areas.add(new SortableSunriseArea('Process 1B'));
        areas.add(new SortableSunriseArea('Unknown-Area-Name'));
        areas.add(new SortableSunriseArea('Steam 1B'));
        areas.add(new SortableSunriseArea('Steam 1A'));
        areas.add(new SortableSunriseArea('Swing Operator'));
         areas.add(new SortableSunriseArea('Water 1B'));
        areas.add(new SortableSunriseArea('Water 1A'));

        areas.sort();
        
        String logStr = '\n*******************************\n'
            + 'METHOD: testSortableSunriseArea';
        
        for (Integer i = 0; i < areas.size(); i++) {
            SortableSunriseArea curArea = areas[i];
            
            logStr += '\n'
                + 'areas[' + i + '].areaName = ' + curArea.areaName;        
        }
        
        logStr += '\n********************************\n';
        System.debug(logStr);
            
        System.assert((areas[0].areaName == 'Control Room 1A'), 'Sort result is either wrong or there was an error.');
        
    }
}