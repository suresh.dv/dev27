/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Controller Extension for Canceling Truck Trip page.
Test Class:     FM_CancelTruckTripControllerXTest
History:        jschn 06.08.2018    - Added ability to create stuck truck for external dispatchers 
                                    - US W-001151
**************************************************************************************************/
public with sharing class FM_CancelTruckTripControllerX {

    public static final String CLASS_NAME = 'FM_CancelTruckTripControllerX';

    private ApexPages.StandardController stdController;
	public FM_Truck_Trip__c trucktripRecord {get; private set;}

    public Boolean                  isStuckTruck       {get;set;}
    public FM_Stuck_Truck__c        stuckTruck         {get;set;}
    public Boolean isExternalDispatcher     {
        get {
            if (isExternalDispatcher == null) {
                isExternalDispatcher = FM_LoadConfirmation_Utilities.userHasExtDispatcherPS();
            }
            return isExternalDispatcher;
        } 
        private set;
    }

    public Boolean isLightningUser {
        get {
            return HOG_GeneralUtilities.isUserUsingLightning();
        }
        private set;
    }

    public Boolean successSave {get;set;}

    public static final String ERROR_MSG_MISSING_CARRIER    = 'Carrier is required field.';
    public static final String ERROR_MSG_MISSING_UNIT       = 'Carrier Unit is required field.';
    public static final String ERROR_WRONG_STATUS_FOR_VENDOR = 'Only exported truck trip can be cancelled';

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public FM_CancelTruckTripControllerX(ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()) {
            stdController.addFields(new List<String>(FM_TruckTrip_Utilities.TRUCKTRIP_FIELDS.split(';')));
        }
        this.trucktripRecord = (FM_Truck_Trip__c) stdController.getRecord();
        this.stdController = stdController;
        if (isExternalDispatcher) {
            initStuckTruck();
        }
    }

    /****************
    *INITIALIZATIONS*
    ****************/
    /**
     * [initStuckTruck description]
     */
    private void initStuckTruck() {
        this.stuckTruck = FM_LoadConfirmation_Utilities.getExternalStuckTruck();
        this.isStuckTruck = false;
        Datetime now = Datetime.now();
        stuckTruck.Carrier_Unit__c = trucktripRecord.Unit__c;
        stuckTruck.Truck_Trip__c = trucktripRecord.Id;
        stuckTruck.Stuck_From__c = now;
        stuckTruck.Location__c = trucktripRecord.Load_Request__r.Source_Location__c;
        stuckTruck.Facility__c = trucktripRecord.Load_Request__r.Source_Facility__c;
        stuckTruck.Stuck_To__c = now;
        stuckTruck.Carrier__c = trucktripRecord.Carrier__c;
        stuckTruck.Status__c = FM_LoadConfirmation_Utilities.STUCK_TRUCK_STATUS_EXTERNAL_DEFAULT;
    }

    /********
    *GETTERS*
    ********/
    /**
     * [getLoadRequestCancelReasons description]
     * @return [description]
     */
    public List<SelectOption> getLoadRequestCancelReasons() {
        return FM_LoadRequest_Utilities.getCancelReasons();
    }

    /*************
    *PAGE ACTIONS*
    *************/
    /**
     * [handleCancelReasonChange description]
     */
    public void handleCancelReasonChange() {
        if (trucktripRecord != null
            && trucktripRecord.Load_Request_Cancel_Reason__c != 'Other') {
            trucktripRecord.Load_Request_Cancel_Comments__c = '';
        }
    }

    public void submitLtng() {
        successSave = (submit() != null);
    }

    /**
     * [submit description]
     * @return [description]
     */
    public PageReference submit() {
        Savepoint sp = Database.setSavepoint();
        try {
            if (doStuckTruckInsert() && isValidStuckTruck()) {
                insert stuckTruck;
            }
            if(!ApexPages.hasMessages()) {
                trucktripRecord.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_CANCELLED;
                return stdController.save();
            }
        } catch(Exception ex) {
            System.debug(CLASS_NAME + ' Exception: ' + ex.getMessage());
            HOG_GeneralUtilities.addPageError(ex.getMessage());
            Database.rollback(sp);
            if(stuckTruck != null) stuckTruck.Id = null;
        }
        return null;
    }

    /**
     * [doStuckTruckInsert description]
     * @return [description]
     */
    private Boolean doStuckTruckInsert() {
        return isExternalDispatcher 
            && isStuckTruck;
    }

    /************
    *VALIDATIONS*
    ************/

    /**
     * Validate if Stuck Truck is valid.
     * Only external dispatcher can create Stuck Truck from this place.
     * Validation on Stuck Truck fields is runned only whe isStucktruck is true.
     */
    private Boolean isValidStuckTruck() {
        return isValidStuckTruckCarrier()
            && isValidStuckTruckCarrierUnit();
    }

    /**
     * Validate if Stuck Truck has Carrier assigned.
     */
    private Boolean isValidStuckTruckCarrier() {
        Boolean isValid = String.isNotBlank(stuckTruck.Carrier__c);
        if (!isValid) {
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_CARRIER);
        }
        return isValid;
    }

    /**
     * Validate if Stuck Truck has Carrier Unit assigned.
     */
    private Boolean isValidStuckTruckCarrierUnit() {
        Boolean isValid = String.isNotBlank(stuckTruck.Carrier_Unit__c);
        if (!isValid) {
            HOG_GeneralUtilities.addPageError(ERROR_MSG_MISSING_UNIT);
        }
        return isValid;
    }

}