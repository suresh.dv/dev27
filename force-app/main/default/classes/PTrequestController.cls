public class PTrequestController{
      public String lsd  {get;set;}
      public String amu {get;set;}
      public String siteName {get;set;}
      public String meternum {get;set;}
      public String site {get;set;}
      public String utilityReferenceNum {get;set;}
      public boolean currentRequests {get;set;}
      public boolean search1 {get;set;}
      public boolean search2 {get;set;}
      public boolean objectBuilded{get;set;}
      public List<wrapper> wrapperList {get;set;}         
      public List<PT_RequestController_Settings__c> statusRequestsList = PT_RequestController_Settings__c.getall().values();
      public Integer firstTimeLoad = Integer.valueOf(returnControllerSettingValue('ControllerFirsTimeLoad'));
      public Integer filterTimeLoad = Integer.valueOf(returnControllerSettingValue('ControllerFilterTimeLoad'));
    
    public string returnControllerSettingValue(String name){
        String controllerSettingValue;
        if (PT_RequestController_Settings__c.getInstance(name) != null){
        	controllerSettingValue = PT_RequestController_Settings__c.getInstance(name).requestStatusController__c;
            }
        else{
            controllerSettingValue = '0';
        }
        return controllerSettingValue;
    }
    
    public void GetRequestTable()
    {
        wrapperList = new List<wrapper>();
        Integer insertCount;
        list<Service__c> serviceList = new list<Service__c>();
        Set<Service__c> finalServiceList = new set<Service__c>();
        Set<Service__c> refinedServiceList = new set<Service__c>();
        set<Service__c> initialServiceList = new set<Service__c>();
        initialServiceList = new set<Service__c>([select id,name,lsd_id__c,amu_id__c,site_id__c,site_name__c,amu_id__r.name, (select name from Utility_Meters__r  ),(select name,owner.name,utility_ref_num__c,utility_contact__c,field_contact__c,request_status__c from utility_requests__r),amu_id__r.operating_district__r.name from service__c ]);
        Service__C serviceDummy = new Service__c(site_name__c = 'serviceDummy');//This service is needed to make the finalServiceList not empty so the validations can work correctly
        objectBuilded = false;
        if(search1 == true || search1 == null)//Search 1 controller
        {
            site = null;
            utilityReferenceNum = null;
            if (currentRequests == true)
            {
                for(PT_RequestController_Settings__c statusRequest : statusRequestsList)
                {
                    for(Service__c service : initialServiceList)
                    {
                        if(service.utility_requests__r.size() > 0)
                        {
                            for(utility_requests__c request : service.utility_requests__r)
                            {
                                if(request.Request_Status__c == statusRequest.requestStatusController__c)
                                {
                                    refinedServiceList.add(service);
                                }
                            }
                        }
                    }
                }
                
            }
            else
            {
                refinedServiceList = initialServiceList.clone();           
            } 
            
            if(siteName == null && amu == null && lsd == null && meternum == null || siteName == '' && amu == '' && lsd == '' && meternum == '')
            {
                insertCount = 0;
                for(service__c ser : refinedServiceList)
                {
                    if(insertCount <= firstTimeLoad)
                    {
                        BuildObject(ser);
                        insertCount++;
                    }
                    else
                    {
                        break;
                    }
                }
                objectBuilded = true;
            }
            if(objectBuilded != true)
            {
                if(lsd != null && lsd != '')
                {
                    for (service__c ser : refinedServiceList){
                        if(ser.LSD_id__c != null && ((ser.LSD_id__c).toUpperCase()).contains(lsd.toUpperCase()) == true)
                        {
                            finalServiceList.add(ser);    
                        }
                    }
                    finalServiceList.add(serviceDummy);
                }
                if(siteName !=null && siteName != '')
                {
                    if(finalServiceList.isEmpty() != true)
                    {
                        refinedServiceList.clear();
                        refinedServiceList = finalServiceList.clone();
                        finalServiceList.clear();                    
                    }
                    for (service__c ser : refinedServiceList){
                        if(ser.Site_Name__c != null && ((ser.site_name__c).toUpperCase()).contains(siteName.toUpperCase()) == true)
                        {
                            finalServiceList.add(ser);    
                        }
                    }
                    finalServiceList.add(serviceDummy);                    
                }
                if(amu != null && amu != '')
                {
                    if(finalServiceList.isEmpty() != true)
                    {
                        refinedServiceList.clear();
                        refinedServiceList = finalServiceList.clone();
                        finalServiceList.clear();                    
                    }
                    for (service__c ser : refinedServiceList){
                        if(ser.amu_id__r !=null && ((ser.amu_id__r.name).toUpperCase()).contains(amu.toUpperCase()) == true)
                        {
                            finalServiceList.add(ser);    
                        }
                    }
                    finalServiceList.add(serviceDummy);                 
                }
                if(meterNum !=null && meterNum != '')
                {
                    if(finalServiceList.isEmpty() != true)
                    {
                        refinedServiceList.clear();
                        refinedServiceList = finalServiceList.clone();
                        finalServiceList.clear();                    
                    }
                    for (service__c ser : refinedServiceList)
                    {
                        for(utility_meter__c meter : ser.utility_meters__r)
                        {
                            if(meter.name != null && ((meter.name).toUpperCase()).contains(meternum.toUpperCase()) == true)
                            {
                                finalServiceList.add(ser);
                            }
                        }
                    }
                    finalServiceList.add(serviceDummy);                 
                }                
            }            
        }
        if(search2 == true)//Search 2 controller
        {
            lsd = null;
            amu = null;
            siteName = null;
            meternum = null;
            refinedServiceList = initialServiceList.clone();
            insertCount = 0;
            if(site == null && utilityReferenceNum == null || site == '' && utilityReferenceNum == '')
            {
                for(service__c ser : refinedServiceList)
                {
                    if(insertCount <= firstTimeLoad)
                    {
                        BuildObject(ser);
                        insertCount++;
                    }
                    else
                    {
                        break;
                    }
                }
                objectBuilded = true;                
            }
            if(objectBuilded != true)
            {
                if(site != null && site != '')
                {
                    for(Service__c service : refinedServiceList)
                    {
                        if(service.Site_Id__c != null && ((service.Site_Id__c).toUpperCase()).contains(site.toUpperCase()) == true) 
                        {
                            finalServiceList.add(service);
                            insertCount++;
                        }
                    }
                    finalServiceList.add(serviceDummy);                     
                }
                
                if (utilityReferenceNum != null && utilityReferenceNum != '')
                {
                    if(finalServiceList.isEmpty() != true)
                    {
                        refinedServiceList.clear();
                        refinedServiceList = finalServiceList.clone();
                        finalServiceList.clear();
                    }
                    for(Service__c service : refinedServiceList)
                    {
                        for(utility_requests__c requests : service.utility_requests__r)
                            if(requests.utility_ref_num__c != null && ((requests.utility_ref_num__c).toUpperCase()).contains(utilityReferenceNum.toUpperCase()) == true)
                        {
                            finalServiceList.add(service);
                        }
                    }
                    finalServiceList.add(serviceDummy);                    
                }   
            }
        }
        for (service__c ser : finalServiceList)
        {
            serviceList.add(ser);
        }
        serviceList.sort();
        insertCount = 0;
        for(Service__c service : serviceList)
        {
            if (insertCount < filterTimeLoad){
                if(service.Site_Name__c != 'serviceDummy')
                {
                    BuildObject(service);
                    insertCount++;
                }
            }
            else
            {
                break;
            }
        } 
    }
    
    public void BuildObject(Service__c info){
        Integer insertCount;
        if(info.utility_requests__r.size() > 0)//If there are requests attached to the service, this if is used to validate each one of the conditions inputed by the user (currentRequests or UtilityReferenceNum)
        {
            Field__c amu = new Field__c();
            amu.Name = info.amu_id__r.name;
            
            Operating_District__c operatingDistrict = new Operating_District__c();
            operatingDistrict.Name = info.amu_id__r.operating_district__r.name;
            
            Utility_Requests__c utilityRequest = new Utility_Requests__c();
            for(Utility_Requests__c crequests : info.utility_requests__r){
                if(utilityReferenceNum != null && utilityReferenceNum != '')//This condition is to insert into the final list only the request that meet the condition of the utilityReferenceNum
                {
                    if(crequests.Utility_Ref_Num__c != null && crequests.Utility_Ref_Num__c.contains(utilityReferenceNum) == true)
                    {
                        utilityRequest = crequests;
                        wrapperList.add(new wrapper(info,amu,utilityRequest,operatingDistrict));
                    }
                }
                else if(currentRequests == true)//This condition is to insert into the final list only the request that meet the condition of the currentRequest
                {
                    for(PT_RequestController_Settings__c statusRequest : statusRequestsList){
                        if(statusRequest.name == crequests.Request_Status__c )
                        {
                            utilityRequest = crequests;
                            wrapperList.add(new wrapper(info,amu,utilityRequest,operatingDistrict));
                        }
                    }
                }
                else//If there is no condition to satisfy, all the requests are inserted into the final list
                {
                    utilityRequest = crequests;
                    wrapperList.add(new wrapper(info,amu,utilityRequest,operatingDistrict));
                }
            }
        }
        else//If the service does not have requests attached to it, the service is inserted with a blank request
        {
            Field__c amu = new Field__c();
            amu.Name = info.amu_id__r.name;
            
            Operating_District__c operatingDistrict = new Operating_District__c();
            operatingDistrict.Name = info.amu_id__r.operating_district__r.name;
            
            Utility_Requests__c utilityRequest = new Utility_Requests__c();            
            wrapperList.add(new wrapper(info,amu,utilityRequest,operatingDistrict));           
        } 
    }
    
    public class wrapper{
        public List<wrapper> wrapper {get;set;}
        public Field__c amu {get;set;}
        public Utility_Requests__c utilityRequests {get;set;}
        public Service__c service {get;set;} 
        public Operating_District__c operatingDistrict {get;set;}
        public wrapper(Service__c serv,Field__c amuv,Utility_Requests__c urv,Operating_District__c opv)
        {
            wrapper = new List<wrapper>();
            utilityRequests = new Utility_Requests__c();
            amu = new Field__c();
            service = new Service__c();
            operatingDistrict = new Operating_District__c();
            amu = amuv;
            utilityRequests = urv;
            service = serv;
            operatingDistrict = opv;       
        }
        
    }
}