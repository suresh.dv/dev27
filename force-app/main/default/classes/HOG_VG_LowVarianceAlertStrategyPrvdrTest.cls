/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    HOG_VG_LowVarianceAlertStrategyProvider
History:        jschn 24/10/2018 - Created.
                jschn 19/03/2019 - Coverage for US-001449 changes
*************************************************************************************************/
@IsTest
private class HOG_VG_LowVarianceAlertStrategyPrvdrTest {

    static List<Location__c> locations;
    static Map<Id, Location__c> oldLocationsMap;
    static Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId;
    static Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType;

    @IsTest
    static void testStrategy_falseBcsActiveExemption() {
        prepareData(false);
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_LowVarianceAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs there is active Vent Gas Exemption');

    }

    @IsTest
    static void testStrategy_falseBcsActiveAlert() {
        prepareData(false);
        ventGasExemptionsByLocationId.clear();
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_LowVarianceAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs there is active Vent Gas Alert');
    }

    @IsTest
    static void testStrategy_falseBcsFlagNotYes() {
        prepareData(false);
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_LowVarianceAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs High Variance flag on location was not YES');
    }

    @IsTest
    static void testStrategy_falseBcsNotCorrectLocation() {
        prepareData(false);
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        Location__c location = locations.get(0);
        location.PVR_Hours_On_Prod__c = 96;
        location.recalculateFormulas();
        Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c WHERE Id = :location.Operating_Field_AMU__c];
        amu.Planner_Group__c = '110';
        amu.Name = 'THERMAL';
        amu.recalculateFormulas();
        location.Operating_Field_AMU__r = amu;
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_LowVarianceAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs Operating Field name was THERMAL');
    }

    @IsTest
    static void testStrategy_falseNotCorrectDate() {
        prepareData(false);
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        Location__c location = locations.get(0);
        location.PVR_Hours_On_Prod__c = 96;
        location.recalculateFormulas();
        Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c WHERE Id = :location.Operating_Field_AMU__c];
        amu.Planner_Group__c = '100';
        amu.recalculateFormulas();
        location.Operating_Field_AMU__r = amu;
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_LowVarianceAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(!strategyResult, 'Should be false, bcs Custom Settings are set to other day of month');
    }

    @IsTest
    static void testStrategy_true() {
        prepareData(true);
        ventGasExemptionsByLocationId.clear();
        activeAlertsByLocationAndType.clear();
        Location__c location = locations.get(0);
        location.recalculateFormulas();
        Field__c amu = [SELECT Name, Is_Thermal__c, Planner_Group__c FROM Field__c WHERE Id = :location.Operating_Field_AMU__c];
        amu.Planner_Group__c = '100';
        amu.recalculateFormulas();
        location.Operating_Field_AMU__r = amu;
        HOG_VG_AlertStrategyProvider provider = new HOG_VG_LowVarianceAlertStrategyProvider(locations,
                oldLocationsMap,
                ventGasExemptionsByLocationId,
                activeAlertsByLocationAndType);

        Test.startTest();
        Boolean strategyResult = provider.shouldCreate(locations.get(0));
        Test.stopTest();

        System.assert(strategyResult, 'Should be true. Strategy for Low Variance was fulfilled');
    }

    private static void prepareData(Boolean createCorrectData) {
        Integer alertsCreateDayOfMonth;
        if (createCorrectData) {
            alertsCreateDayOfMonth = Date.today().day();
        } else {
            alertsCreateDayOfMonth = Date.today().day() + 1;
        }
        HOG_Vent_Gas_Alert_Configuration__c config = new HOG_Vent_Gas_Alert_Configuration__c(
                Alerts_Create_Day_of_month__c = alertsCreateDayOfMonth,
                Alberta_Large_Low_Variance__c = 200,
                Low_Variance_Production_Hrs_Threshold__c = 300
        );
        insert config;


        locations = new List<Location__c>();
        oldLocationsMap = new Map<Id, Location__c>();
        ventGasExemptionsByLocationId = new Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c>();
        activeAlertsByLocationAndType = new Map<String, HOG_Vent_Gas_Alert__c>();

        Location__c location = HOG_VentGas_TestData.createLocationForAlert(false);
        location.Name += ' W4';
        location.GOR_Effective_Date__c = Date.today();
        location.Measured_Vent_Rate__c = 500;
        location.PVR_Hours_On_Prod__c = 300;
        location.PVR_Fuel_Consumption__c = 1;
        location.Monthly_Trucked_Oil__c = 1;
        location.PVR_GOR_Factor__c = 2;
        insert location;

        locations.add(location);
        oldLocationsMap.put(location.Id, location.clone());
        ventGasExemptionsByLocationId.put(location.Id, new HOG_Vent_Gas_Alert_Exemption_Request__c());
        activeAlertsByLocationAndType.put(location.Id + HOG_VentGas_Utilities.ALERT_TYPE_LOW_VARIANCE, new HOG_Vent_Gas_Alert__c());
    }

}