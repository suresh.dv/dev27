/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class WTDCForgotPasswordController {
    public String username {get; set;}   
       
    public WTDCForgotPasswordController() {}
    
    public PageReference forgotPassword() {
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.WTDCForgotPasswordConfirm;
        pr.setRedirect(true);
        
        if (success) {              
            return pr;
        }
        return null;    
    }
}