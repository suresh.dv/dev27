/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    HOG Service class for lightning containing endpoints for Custom Lookup component.
History:        jschn 07.20.2018 - Created.
**************************************************************************************************/
@IsTest private class VG_TaskLtngTest {
	
    public static final String CLASS_NAME = 'VG_TaskLtngTest';
	
	@IsTest static void getTask_withoutParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getTask_v1(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void getTask_withWrongParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getTask_v1('abc');
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void getTask_withParam() {
		prepareData();
		HOG_Vent_Gas_Alert_Task__c task = [
				SELECT Id, Name, Status__c, Vent_Gas_Alert__c
				FROM HOG_Vent_Gas_Alert_Task__c
				LIMIT 1
		];

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getTask_v1(task.Id);
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(1, response.resultObjects.size());
		HOG_Vent_Gas_Alert_Task__c taskResponse = (HOG_Vent_Gas_Alert_Task__c) response.resultObjects.get(0);
		System.assertEquals(task.Status__c, taskResponse.Status__c);
		System.assertEquals(task.Name, taskResponse.Name);
	}
	
	@IsTest static void getNonCompletedTasks_v1_withoutParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getNonCompletedTasks_v1(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(0, response.resultObjects.size());
	}

	@IsTest static void getNonCompletedTasks_v1_withWrongParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getNonCompletedTasks_v1('abc');
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(0, response.resultObjects.size());
	}

	@IsTest static void getNonCompletedTasks_v1_withParam() {
		prepareData();
		HOG_Vent_Gas_Alert__c alert = [SELECT Id FROM HOG_Vent_Gas_Alert__c LIMIT 1];

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getNonCompletedTasks_v1(alert.Id);
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(2, response.resultObjects.size());
	}

	@IsTest static void getCompletedTasks_v1_withoutParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getCompletedTasks_v1(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(0, response.resultObjects.size());
	}

	@IsTest static void getCompletedTasks_v1_withWrongParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getCompletedTasks_v1('abc');
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(0, response.resultObjects.size());
	}

	@IsTest static void getCompletedTasks_v1_withParam() {
		prepareData();
		HOG_Vent_Gas_Alert__c alert = [SELECT Id FROM HOG_Vent_Gas_Alert__c LIMIT 1];

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponseImpl) VG_TaskLtng.getCompletedTasks_v1(alert.Id);
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(2, response.resultObjects.size());
	}

	@IsTest static void getRouteOperator_v1_withoutParam() {

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponse) VG_TaskLtng.getRouteOperator_v1(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void getRouteOperator_v1_withWrongParam() {
		prepareData();
		Location__c loc = [SELECT Id FROM Location__c WHERE Name = 'loc2' LIMIT 1];
		delete loc;

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponse) VG_TaskLtng.getRouteOperator_v1(loc.Id);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void getRouteOperator_v1_locationWithoutOperator() {
		prepareData();
		Location__c loc = [SELECT Id FROM Location__c WHERE Name = 'loc2' LIMIT 1];

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponse) VG_TaskLtng.getRouteOperator_v1(loc.Id);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObjects == null);
		System.assertEquals(1, response.errors.size());
		System.debug(response.errors);
	}

	@IsTest static void getRouteOperator_v1_locationWithOperator() {
		prepareData();
		Location__c loc = [SELECT Id FROM Location__c WHERE Name = 'Beach House' LIMIT 1];

		Test.startTest();
		HOG_SelectResponse response = (HOG_SelectResponse) VG_TaskLtng.getRouteOperator_v1(loc.Id);
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObjects != null);
		System.assertEquals(1, response.resultObjects.size());
	}

	@IsTest static void testUpsertTask_v1_withoutParam() {

		Test.startTest();
		HOG_DMLResponse response = (HOG_DMLResponse) VG_TaskLtng.upsertTask_v1(null);
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObject == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testUpsertTask_v1_withWrongParam() {

		Test.startTest();
		HOG_DMLResponse response = (HOG_DMLResponse) VG_TaskLtng.upsertTask_v1('{wrongJSON}');
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObject == null);
		System.assertEquals(1, response.errors.size());
	}

	@IsTest static void testUpsertTask_v1_withoutId() {
		prepareData();
		HOG_Vent_Gas_Alert_Task__c task = [
				SELECT Id, Name, Status__c, Vent_Gas_Alert__c, Assignee1__c, Comments__c, Assignee_Type__c
				FROM HOG_Vent_Gas_Alert_Task__c
				WHERE Status__c =: HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED
				LIMIT 1
		];
		delete task;
		task.Id = null;

		Test.startTest();
		//TODO run as users.
		HOG_DMLResponse response = (HOG_DMLResponse) VG_TaskLtng.upsertTask_v1(JSON.serialize(task));
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObject != null);
		System.assert(response.resultObject.Id != null);
		System.assert([SELECT Name FROM HOG_Vent_Gas_Alert_Task__c WHERE Id = : response.resultObject.Id] != null);
	}

	@IsTest static void testUpsertTask_v1_withWrongUser() {
		prepareData();
		HOG_Vent_Gas_Alert_Task__c task = [
				SELECT Id, Name, Status__c, Vent_Gas_Alert__c, Assignee1__c, Comments__c
				FROM HOG_Vent_Gas_Alert_Task__c
				WHERE Status__c =: HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED
				LIMIT 1
		];
		task.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS;
		User runningUser = [SELECT Id FROM User WHERE Alias = 'dude'];
		HOG_DMLResponse response;

		Test.startTest();
		System.runAs(runningUser) {
			response = (HOG_DMLResponse) VG_TaskLtng.upsertTask_v1(JSON.serialize(task));
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(!response.success);
		System.assert(response.errors != null);
		System.assert(response.resultObject == null);
		System.assertEquals(1, response.errors.size());
		System.assertNotEquals(task.Status__c, [SELECT Status__c FROM HOG_Vent_Gas_Alert_Task__c WHERE Id =: task.Id].Status__c);
	}

	@IsTest static void testUpsertTask_v1_withCorrectUser() {
		prepareData();
		HOG_Vent_Gas_Alert_Task__c task = [
				SELECT Id, Name, Status__c, Vent_Gas_Alert__c, Assignee1__c, Comments__c
				FROM HOG_Vent_Gas_Alert_Task__c
				WHERE Status__c =: HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED
				LIMIT 1
		];
		task.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS;

		User runningUser = [SELECT Id FROM User WHERE Alias = 'IronMan'];
		HOG_DMLResponse response;

		Test.startTest();
		System.runAs(runningUser) {
			response = (HOG_DMLResponse) VG_TaskLtng.upsertTask_v1(JSON.serialize(task));
		}
		Test.stopTest();

		System.assert(response != null);
		System.assert(response.success);
		System.assert(response.errors == null);
		System.assert(response.resultObject != null);
		System.assertEquals(task.Status__c, [SELECT Status__c FROM HOG_Vent_Gas_Alert_Task__c WHERE Id =: task.Id].Status__c);
	}

	@TestSetup
	private static void createUser() {
		User usr = HOG_VentGas_TestData.createUser();
		usr = HOG_VentGas_TestData.assignPermissionSet(usr, 'HOG_Production_Engineer');
		HOG_VentGas_TestData.createUser('testUser', 'forVG', 'dude');
	}

	static void prepareData() {
		Location__c loc = HOG_VentGas_TestData.createLocation();
		Route__c route = HOG_VentGas_TestData.createRoute('woOper');
		insert route;
		HOG_VentGas_TestData.createLocation('loc2', route.Id, loc.Operating_Field_AMU__c, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'];
		HOG_VentGas_TestData.createOperatorOnCall(loc.Route__c, usr.Id);
		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST,'TestDescription',
				loc.Id,
				usr.Id,
				HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
				HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		HOG_VentGas_TestData.createTask(alert.Id,
				usr.Id,
				null,
				'NotStartedTask',
				alert.Priority__c,
				true,
				HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);

		HOG_Vent_Gas_Alert_Task__c task2 = HOG_VentGas_TestData.createTask(alert.Id,
				usr.Id,
				null,
				'InProgressTask',
				alert.Priority__c,
				true,
				HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
		task2.Status__c = HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS;
		update task2;

		HOG_Vent_Gas_Alert_Task__c task3 = HOG_VentGas_TestData.createTask(alert.Id,
				usr.Id,
				null,
				'CompletedTask',
				alert.Priority__c,
				true,
				HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
		task3.Status__c = HOG_VentGas_Utilities.TASK_STATUS_COMPLETED;
		update task3;


		HOG_Vent_Gas_Alert_Task__c task4 = HOG_VentGas_TestData.createTask(alert.Id,
				usr.Id,
				null,
				'CancelledTask',
				alert.Priority__c,
				true,
				HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED,
				HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
		task4.Status__c = HOG_VentGas_Utilities.TASK_STATUS_CANCELLED;
		update task4;
	}
	
}