/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_Calendar_Endpoint class
History:        mbrimus 19/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_Calendar_EndpointTest {

    @IsTest
    static void loadCurrentUserData_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadCurrentUserData();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result.resultObjects);
    }

    @IsTest
    static void loadCurrentUserData_withUserData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            try {
                result = VTT_LTNG_Calendar_Endpoint.loadCurrentUserData();
            } catch (Exception ex) {
                failFlag = true;
            }
        }

        Test.stopTest();

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(true, result.success);
        System.assertEquals(null, result.errors);
        System.assertEquals(1, result.resultObjects.size());
        System.assertEquals(expectedFailFlag, failFlag);

        Map<String, Object> resultsMap = (Map<String, Object>) result.resultObjects.get(0);
        Contact contact = (Contact) resultsMap.get('userData');
        System.assertNotEquals(null, contact);
    }

    @IsTest
    static void loadEventDataForDate_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadEventDataForDate(null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, result.success);
        System.assertEquals(1, result.errors.size());
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void loadEventDataForDate_withData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Starting Job');
        }
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadEventDataForDate(String.valueOf(System.today()), tradesmanContact.Id);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
        System.assertEquals(1, result.resultObjects.size());
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void loadVendorAccounts_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadVendorAccounts();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
    }

    @IsTest
    static void loadVendorAccounts_withData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        VTT_TestData.createVendorAccount('Vendor1');
        VTT_TestData.createVendorAccount('Vendor2');

        try {
            result = VTT_LTNG_Calendar_Endpoint.loadVendorAccounts();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
        System.assertEquals(1, result.resultObjects.size());

        List<Account> acs = (List<Account>) result.resultObjects.get(0);
        System.assertEquals(2, acs.size());

    }

    @IsTest
    static void loadTradesman_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadTradesman(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
    }

    @IsTest
    static void loadTradesman_withData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        User otherUser = VTT_TestData.createVTTUser();
        Account vendor;
        System.runAs(otherUser) {
            vendor = VTT_TestData.createVendorAccount('Test');
            VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
        }

        try {
            result = VTT_LTNG_Calendar_Endpoint.loadTradesman(vendor.Id);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
        System.assertEquals(1, result.resultObjects.size());
    }

    @IsTest
    static void loadEventsForTradesman_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadEventsForTradesman(null, null, null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(false, result.success);
    }

    @IsTest
    static void loadEventsForTradesman_withData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            insert VTT_Utilities.CreateLogEntry(tradesmanContact,
                    activity,
                    VTT_Utilities.LOGENTRY_STARTJOB,
                    0,
                    0,
                    null,
                    'Starting Job');
        }
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadEventsForTradesman(tradesmanContact.Id, System.today().addDays(-1), System.today());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
        System.assertEquals(1, result.resultObjects.size());
    }

    @IsTest
    static void loadOnHoldActivities_noData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadOnHoldActivities(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
        System.assertEquals(1, result.resultObjects.size());
        List<Work_Order_Activity__c> ac = (List<Work_Order_Activity__c>) result.resultObjects.get(0);
        System.assertEquals(0, ac.size());
    }

    @IsTest
    static void loadOnHoldActivities_withData() {
        HOG_CustomResponseImpl result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;

        Test.startTest();
        User otherUser = VTT_TestData.createVTTUser();
        Contact tradesmanContact;
        Work_Order_Activity__c activity;
        System.runAs(otherUser) {
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Test');
            tradesmanContact = VTT_TestData.createTradesmanContact('Fero', 'Man', vendor.Id, otherUser.Id);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            activity.Status__c = VTT_Utilities.ACTIVITY_STATUS_ONHOLD;
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesmanContact.Id);
            update activity;
        }
        try {
            result = VTT_LTNG_Calendar_Endpoint.loadOnHoldActivities(tradesmanContact.Id);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(true, result.success);
        System.assertEquals(1, result.resultObjects.size());
    }
}