/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Form structure for Engine Operating Condition SObject
Test Class:     EPD_FormStructureEOCTest
History:        jschn 2019-07-24 - Created. - EPD R1
*************************************************************************************************/
public with sharing class EPD_FormStructureEOC extends EPD_FormStructureBase {

    @AuraEnabled
    public Id recordId;

    @AuraEnabled
    public Id epdId;

    @AuraEnabled
    public Id recordTypeId;

    @AuraEnabled
    public String blockName {get;set;}

    @AuraEnabled
    public String inputLabelPostfix {get;set;}

    @AuraEnabled
    public Boolean showPreChamberFuelPressure {get;set;}

    @AuraEnabled
    public Decimal preChamberFuelPressure;
    //TODO remove after LTNG migration
    public String preChamberFuelPressureClassic {
        get {
            return String.valueOf(preChamberFuelPressure);
        }
        set {
            preChamberFuelPressureClassic = value;
            if(String.isNotBlank(value)) {
                preChamberFuelPressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal regulatedFuelPressure;
    //TODO remove after LTNG migration
    public String regulatedFuelPressureClassic {
        get {
            return String.valueOf(regulatedFuelPressure);
        }
        set {
            regulatedFuelPressureClassic = value;
            if(String.isNotBlank(value)) {
                regulatedFuelPressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal suppliedFuelPressure;
    //TODO remove after LTNG migration
    public String suppliedFuelPressureClassic {
        get {
            return String.valueOf(suppliedFuelPressure);
        }
        set {
            suppliedFuelPressureClassic = value;
            if(String.isNotBlank(value)) {
                suppliedFuelPressure = Decimal.valueOf(value);
            }
        }
    }

    /**
     * This method maps local values into Engine Operating Condition SObject.
     *
     * @return SObject (EPD_Engine_Operating_Condition__c)
     */
    public override SObject getRecord() {
        return new EPD_Engine_Operating_Condition__c(
                Id = recordId,
                Engine_Performance_Data__c = epdId,
                Block_Name__c = blockName,
                Pre_Chamber_Fuel_Pressure__c = preChamberFuelPressure,
                Regulated_Fuel_Pressure__c = regulatedFuelPressure,
                Supplied_Fuel_Pressure__c = suppliedFuelPressure,
                RecordTypeId = this.recordTypeId
        );
    }

    /**
     * Maps Engine Operating Condition record into local variables.
     *
     * @param obj
     *
     * @return EPD_FormStructureBase
     */
    public override EPD_FormStructureBase setRecord(SObject obj) {
        EPD_Engine_Operating_Condition__c record = (EPD_Engine_Operating_Condition__c) obj;

        inputLabelPostfix = EPD_Constants.EOC_BLOCK_NAME_TO_INPUT_LABEL_MAP.get(record.Block_Name__c);

        recordId = record.Id;
        epdId = record.Engine_Performance_Data__c;
        blockName = record.Block_Name__c;
        preChamberFuelPressure = record.Pre_Chamber_Fuel_Pressure__c;
        regulatedFuelPressure = record.Regulated_Fuel_Pressure__c;
        suppliedFuelPressure = record.Supplied_Fuel_Pressure__c;
        recordTypeId = record.RecordTypeId;

        System.debug(JSON.serialize(this));

        return this;
    }

    /**
     * Sets Show Pre Chamber Fuel Pressure flag based on engine config.
     *
     * @param engineConfig
     *
     * @return EPD_FormStructureEOC
     */
    public EPD_FormStructureEOC setFlag(EPD_Engine__mdt engineConfig) {
        showPreChamberFuelPressure = engineConfig.Advanced_EOC__c;
        return this;
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuildingString = '';
        contentBuildingString += '"recordId":' + (String.isNotBlank(recordId) ? ('"' + recordId + '"') : 'null') + ',';
        contentBuildingString += '"epdId":' + (String.isNotBlank(epdId) ? ('"' + epdId + '"') : 'null') + ',';
        contentBuildingString += '"recordTypeId":' + (String.isNotBlank(recordTypeId) ? ('"' + recordTypeId + '"') : 'null') + ',';
        contentBuildingString += '"blockName":' + (String.isNotBlank(blockName) ? ('"' + blockName + '"') : 'null') + ',';
        contentBuildingString += '"inputLabelPostfix":' + (String.isNotBlank(inputLabelPostfix) ? ('"' + inputLabelPostfix + '"') : 'null') + ',';
        contentBuildingString += '"showPreChamberFuelPressure":' + showPreChamberFuelPressure + ',';
        contentBuildingString += '"preChamberFuelPressure":' + preChamberFuelPressure + ',';
        contentBuildingString += '"regulatedFuelPressure":' + regulatedFuelPressure + ',';
        contentBuildingString += '"suppliedFuelPressure":' + suppliedFuelPressure;
        return '{' + contentBuildingString + '}';
    }

}