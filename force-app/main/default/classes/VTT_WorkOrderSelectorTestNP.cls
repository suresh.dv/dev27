/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non parallel unit test for VTT_WorkOrderSelector class
History:        jschn 04/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WorkOrderSelectorTestNP {

    @IsTest
    static void getRecordsByFilter_success() {
        VTT_WOA_SearchCriteria filter = new VTT_WOA_SearchCriteria();
        String sorting = 'Name ASC';
        Integer offset = 0;
        Integer queryLimit = 1;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        List<HOG_Maintenance_Servicing_Form__c> result;
        VTT_WOA_ListViewDAO selector = new VTT_WorkOrderSelector();
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTAdminUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            RecordType rt = [SELECT Id FROM RecordType WHERE SobjectType = 'HOG_Maintenance_Servicing_Form__c' AND DeveloperName = 'Maintenance_Servicing'];

            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrderWithRecordType(serviceRequest.Id, VTT_TestData.notificationType.Id, rt.Id);
            workOrder.User_Status_Code__c = '5X';
            workOrder.Order_Type__c = 'WP01';
            workOrder.Plant_Section__c  = '200';
            workOrder.Work_Order_Priority_Number__c  = '1';
            update workOrder;
            VTT_TestData.createWorkOrderActivity(workOrder.Id);
            System.debug(JSON.serialize([SELECT (SELECT User_Status__c, SAP_Deleted__c, Name, Status__c, Operating_Field_AMU__c, Operating_Field_AMU__r.Is_Thermal__c FROM Work_Order_Activities__r), RecordType.DeveloperName FROM HOG_Maintenance_Servicing_Form__c]));

            Test.startTest();
            try {
                result = selector.getRecordsByFilter(filter, sorting, offset, queryLimit);
            } catch (Exception ex) {
                System.debug(ex.getMessage());
                System.debug(ex.getStackTraceString());
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

}