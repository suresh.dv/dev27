@isTest
private class SAPHOGWorkOrderServicesTest {

    static TestMethod void CreateWorkOrderTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGWorkOrderResponseMock());

		String functionalLocationObjectId = '?0100000000000034667';
		String maintenancePlanningPlant = 'PP01';
		String mainDescription = 'Test';
		String mainLongDescription = 'Test';
		String priority = '1';
		String orderType = 'RL01';
		String mainWorkCenter = 'SCHED3';
		String maintenanceActivityType = 'BRK';
		String activityNumber = '0010';
		String controlKey = 'INT1';
		String userStatus = '4S';
		String referenceNotificationNumber;
		String referenceId;        
 
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();

        SAPHOGWorkOrderServices.CreateWorkOrderResponse response = workOrder.CreateWorkOrder(
																			functionalLocationObjectId,
																			maintenancePlanningPlant,
																			mainDescription,
																			mainLongDescription,
																			priority,
																			orderType,
																			mainWorkCenter,
																			maintenanceActivityType,
																			activityNumber,
																			controlKey,
																			userStatus,
																			referenceNotificationNumber,
																			referenceId,
																			null);
                                                                           
        System.assertEquals(response.WorkOrder.WorkOrderNumber, '24173534');
    }
    
    static TestMethod void CreateWorkOrdersTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGWorkOrdersResponseMock());
        
	    SAPHOGWorkOrderServices.CreateWorkOrderRequest[] workOrdersRequest = new List<SAPHOGWorkOrderServices.CreateWorkOrderRequest>();
	    SAPHOGWorkOrderServices.CreateWorkOrderRequest workOrderRequest;

        workOrderRequest = new SAPHOGWorkOrderServices.CreateWorkOrderRequest();
        workOrderRequest.FunctionalLocationObjectId = '?0100000000000034667';
        workOrderRequest.MaintenancePlanningPlant = 'PP01';
        workOrderRequest.MainDescription = 'Test';
        workOrderRequest.MainLongDescription = 'Test';
        workOrderRequest.Priority = '1';
        workOrderRequest.OrderType = 'RL01';
        workOrderRequest.MainWorkCenter = 'SCHED3';
        workOrderRequest.MaintenanceActivityType = 'BRK';
        workOrderRequest.ActivityNumber = '0010';
        workOrderRequest.ControlKey = 'INT1';
        workOrderRequest.UserStatus = '4S';                                    
        workOrderRequest.ReferenceId = null;
        workOrdersRequest.add(workOrderRequest);                                
 
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();
        SAPHOGWorkOrderServices.CreateWorkOrdersResponse response = workOrder.CreateWorkOrders(workOrdersRequest);
                                                                                    
        System.assertEquals(response.Message, 'SAPHOGWorkOrdersResponseMock.CreateWorkOrdersResponse Test Message');
        System.assertEquals(response.type_x, True);
     }


    static TestMethod void ATLConfirmWorkOrderTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGATLConfirmWorkOrderResponseMock());
        
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();
 
        String workOrderNumber = '000005506735';
        String orderType = 'WP01';
        Date startDateTime = System.today();
        Date StopDateTime = System.today();
         
        SAPHOGWorkOrderServices.ATLConfirmWorkOrderResponse response = workOrder.ATLConfirmWorkOrder(workOrderNumber, orderType, startDateTime, stopDateTime);
        
        System.assertEquals(response.Message, 'ATLConfirmWorkOrderResponse Test Message');
        System.assertEquals(response.WorkOrderNumber, 'WorkOrderNumber Test');
        System.assertEquals(response.type_x, True);
    }
    

    static TestMethod void UpdateWorkOrderTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGUpdateWorkOrderResponseMock());
                
		String workOrderNumber = '7521256';
		String mainDescription = 'Test';
		String mainLongDescription = 'Test';
		String priority = '1';
		String mainWorkCenter = 'OPER';
		String maintenanceActivityType = 'BRK';
		String activityNumber = '0010';
		String userStatus = '4S';
		Date lastChangedDate;
		String lastChangedBy;

        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();
        
        SAPHOGWorkOrderServices.UpdateWorkOrderResponse response = workOrder.UpdateWorkOrder(
																					workOrderNumber,
																					mainDescription,
																					mainLongDescription,
																					priority,
																					mainWorkCenter,
																					maintenanceActivityType,
																					activityNumber,
																					userStatus,
																					lastChangedDate,
																					lastChangedBy,
																					null);
        
        System.assertEquals(response.Message, 'SAPHOGWorkOrderServices.UpdateWorkOrderResponse Test Message');
        System.assertEquals(response.WorkOrder.WorkOrderNumber, '7521256');
        System.assertEquals(response.type_x, True);
        
    }
    
    static TestMethod void IsAliveTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGWorkOrderIsAliveResponseMock());   
        
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();
        
        String response = workOrder.IsAlive();
        
        System.assertEquals(response, 'SAPHOGWorkOrderServices.IsAliveResponse Test Message');
    }
    
    static TestMethod void GetWorkOrderTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGGetWorkOrderResponseMock());
        
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();
        
        String WorkOrderNumber = '7521256';
        
        SAPHOGWorkOrderServices.GetWorkOrderResponse response = workOrder.GetWorkOrder(WorkOrderNumber);
        
        System.assertEquals(response.Message, 'SAPHOGWorkOrderServices.GetWorkOrderResponse Test Message');
        System.assertEquals(response.WorkOrder.WorkOrderNumber, '24173534');
        System.assertEquals(response.type_x, True);
    }

    static TestMethod void WorkOrderOperationsTest() {
        SAPHOGWorkOrderServices.WorkOrderOperations workOrderOperations = new SAPHOGWorkOrderServices.WorkOrderOperations();
        SAPHOGWorkOrderServices.WorkOrderOperation workOrderOperation = new SAPHOGWorkOrderServices.WorkOrderOperation();
    }    
    
    static testMethod void RefreshSalesforceWorkOrdersTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGRefreshWorkOrdersResponseMock());

        DateTime dateToRefresh = System.now();
        
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();
        String response = workOrder.RefreshSalesforceWorkOrders(dateToRefresh, dateToRefresh);
                                                                                    
        System.assertEquals(response, 'SAPHOGWorkOrderServices.RefreshSalesforceWorkOrdersResponse Test Message');
    }


    static TestMethod void UpdateWorkOrderActivitiesTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGResponseMock.UpdateWorkOrderActivitiesResponseMock());
     
 
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();

        List<SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest> WorkOrderActivityRequestList = new List<SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest>();

        WorkOrderActivityRequestList.add(new SAPHOGWorkOrderServices.UpdateWorkOrderActivityRequest());


        SAPHOGWorkOrderServices.UpdateWorkOrderActivitiesResponse  response = workOrder.UpdateWorkOrderActivities(
                                                                            WorkOrderActivityRequestList
                                                                            );
                                                                           
        System.assertEquals('1234567', response.WorkOrderActivityResponseList[0].ActivityNumber);
    }
    
    static TestMethod void ConfirmWorkOrderActivitiesTest() {
        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGResponseMock.ConfirmWorkOrderActivitiesResponseMock());
 
        SAPHOGWorkOrderServices.WorkOrderPort workOrder = new SAPHOGWorkOrderServices.WorkOrderPort();

        List<SAPHOGWorkOrderServices.ConfirmWorkOrderActivityRequest> WorkOrderActivityRequestList = new List<SAPHOGWorkOrderServices.ConfirmWorkOrderActivityRequest>();

        WorkOrderActivityRequestList.add(new SAPHOGWorkOrderServices.ConfirmWorkOrderActivityRequest());


        SAPHOGWorkOrderServices.ConfirmWorkOrderActivitiesResponse  response = workOrder.ConfirmWorkOrderActivities(
                                                                            WorkOrderActivityRequestList
                                                                            );
                                                                           
        System.assertEquals('1234567', response.WorkOrderActivityConfirmResponseList[0].ActivityNumber);
    }
    
    static TestMethod void ReceiveWorkOrderFromSAPResponseTest() {
    	SAPHOGWorkOrderServices.ReceiveWorkOrderFromSAPResponse workOrder = new SAPHOGWorkOrderServices.ReceiveWorkOrderFromSAPResponse();
    }
    
    static TestMethod void WorkOrderOperationAssignmentTest() {
    	SAPHOGWorkOrderServices.WorkOrderOperationAssignment workOrderAssignment = new SAPHOGWorkOrderServices.WorkOrderOperationAssignment();
    	SAPHOGWorkOrderServices.WorkOrderOperationAssignments workOrderAssignments = new SAPHOGWorkOrderServices.WorkOrderOperationAssignments();
    }
}