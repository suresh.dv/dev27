/**************************************************************************************************************
  CreatedBy     :   Accenture
  Organization  :   Accenture
  Purpose       :   Test class For Trigger RaM_updateEquipmentNewValue and its helping class RaM_EquipmentTriggerHandler
  Version:1.0.0.1
*****************************************************************************************************************/
@isTest
public class RaM_EquipmentTriggerHandlerTest {
  
    @testSetUp static void setup()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        User u = new User(Alias = 'newUser', Email='newuser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles', UserName='testnewuser@testorg.com');
        insert u;
        
        System.runAs(u){
          // Insert account
          Account accountRecord = new Account(Name = 'Husky Account',VendorGroup__c='BUNN-O-MATIC');
          accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
          insert accountRecord;
          system.debug('account'+accountRecord.Id);
          
          System.assertEquals(accountRecord.Name,'Husky Account');
          
          //Insert Contact
          Contact contactRecord = new Contact(LastName = 'Glenn Mundi');
          contactRecord.AccountId = accountRecord.id;
          contactRecord .ContactLogin__c='ABC';
          insert contactRecord;  
          
          System.assertEquals(contactRecord.LastName,'Glenn Mundi');
          
          // Insert Location
          RaM_Location__c locationRecord = new RaM_Location__c(Name = '10001',Service_Area__c = 'Island',SAPFuncLOC__c = 'C1-02-002-01006', Location_Classification__c ='urban');
          insert locationRecord;
          
          System.assertEquals(locationRecord.Name,'10001');
        }
    }
    static testMethod void testClass(){
    
        
        User u2 = [select id from user where UserName='testnewuser@testorg.com'];

        System.runAs(u2) {
            // Queried all records those inserted in test settup process.
            Account accountRecord = [select id,Name from Account limit 1];  
            Contact contactRecord = [select id,AccountId from contact limit 1]; 
            Ram_Location__c locationRecord = [select id,Name,SAPFuncLOC__c  from Ram_Location__c limit 1];
            Test.startTest() ;

            //Insert Equipment
            RaM_Equipment__c equipmentRecord = new RaM_Equipment__c(Name = 'Test1');
            equipmentRecord.Equipment_Type__c = 'ELECTRICAL NEW';
            equipmentRecord.Status__c = 'Active';
            equipmentRecord.Equipment_Class__c = 'ELECTRICAL PANEL NEW';
            equipmentRecord.EquipmentTag__c = 'RP1006ASTINF0001';
            equipmentRecord.SerialNumber__c = '1234';
            equipmentRecord.Manufacturer__c = 'Air-Serv';
            equipmentRecord.Model__c = 'Stand alone unit';
            equipmentRecord.Model__c = 'Stand alone unit';
            equipmentRecord.FunctionalLocation__c='C1-02-002-01006';
            try{
                insert equipmentRecord; //Insert Equipment record
            }
            catch(Exception e){} 
            System.assertEquals(equipmentRecord.SerialNumber__c,'1234');
            //Insert Equipment
            RaM_Equipment__c equipmentRecord1 = new RaM_Equipment__c(Name = 'Test1');
            equipmentRecord1.Equipment_Type__c = 'ELECTRICAL NEW';
            equipmentRecord1.Status__c = 'Active';
            equipmentRecord1.Equipment_Class__c = 'ELECTRICAL PANEL NEW';
            equipmentRecord1.EquipmentTag__c = 'RP1006ASTINF0001';
            equipmentRecord1.SerialNumber__c = '1234';
            equipmentRecord1.Manufacturer__c = 'Air-Serv';
            equipmentRecord1.Model__c = 'Stand alone unit';
            equipmentRecord1.Model__c = 'Stand alone unit';
            equipmentRecord1.FunctionalLocation__c='C1-02-002-01006';
            try{
                insert equipmentRecord1; //Insert Equipment record
            }
            catch(Exception e){} 
            System.assertEquals(equipmentRecord1.SerialNumber__c,'1234');
            system.debug('equipment+'+equipmentRecord1.Id);
            Test.stopTest();
    }
    }


}