/**********************************************************************************
  CreatedBy :   Accenture
  Organization: Accenture
  Purpose   :  Batch apex class to move all existing Retail Location record to Location object
  Test Class:  RaM_moveRetailLocationRecordToLocation
  Version:1.0.0.1
**********************************************************************************/

global class RaM_moveRetailLocationRecordToLocation implements Database.Batchable<sObject>,Database.Stateful  {
global string query ; //A global variable required which will get all fields from retail location object to query
global List<String> exception_List;//A global variable required which will maintained failed record.

global database.querylocator start(Database.BatchableContext BC){
exception_List=new List<String>();
    query=RaM_RetailLocationHandler.getAllRetailLocationFields();
    return Database.getQueryLocator(query);    
}

global void execute(Database.BatchableContext BC, List<Retail_Location__c> scope){   
      list<RaM_Location__c> locList=new list<RaM_Location__c>();  
      system.debug('----scope-----'+ scope);
      locList=RaM_RetailLocationHandler.returnRaMLocationRecord(scope, true);
     
        Database.SaveResult[] SaveResultList = Database.insert(locList, false);
        system.debug('--------SaveResultList-----------'+SaveResultList);
        //store all the failed record to email in the finish method
        for(integer i =0; i<locList.size();i++){
            String msg='';
            If(!SaveResultList[i].isSuccess()){ msg += locList.get(i).LocationExternalId__c + '--'+'Error: "'; 
                    system.debug('--------msg-----1------'+msg);
                    for(Database.Error err: SaveResultList[i].getErrors()){   msg += err.getmessage()+'",'; } 
            }
            if(msg!='') exception_List.add(msg);
        } 
     system.debug('--------exception_List-----------'+exception_List);
      
}
global void finish(Database.BatchableContext BC){
system.debug('--------exception_List-----finish------'+exception_List);
    string errorLog='';
    for(string str: exception_List){ errorLog+=str;
    }
    if(!exception_List.IsEmpty()){ RaM_UtilityHolder.errormethod(RaM_ConstantUtility.Retail_Location_OBJECT,RaM_ConstantUtility.BATCH_CLASS_RETAIL_LOCATION_CLASSNAME ,RaM_ConstantUtility.BATCH_CLASS_RETAIL_LOCATION_CLASSNAME , RaM_ConstantUtility.FATAL,errorLog);
    }
       
}  
 
}