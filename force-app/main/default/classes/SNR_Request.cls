/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Request data for saving SNR
Test Class:     SNR_EndpointTest
History:        mbrimus 2020-01-14 - Created.
*************************************************************************************************/
public inherited sharing class SNR_Request {

    @AuraEnabled
    public String workDetails;

    @AuraEnabled
    public String title;

    @AuraEnabled
    public Datetime malfunctionStartDate;

    @AuraEnabled
    public String serviceTypeId;

    @AuraEnabled
    public String serviceCategoryId;

    @AuraEnabled
    public String serviceActivityId;

    @AuraEnabled
    public String servicePriorityId;

    @AuraEnabled
    public String serviceRequiredId;

    @AuraEnabled
    public String serviceSpecificsId;

    @AuraEnabled
    public String vendorCompanyId;

    @AuraEnabled
    public Boolean equipmentDown;

    @AuraEnabled
    public Boolean productionImpacted;

}