/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 3/11/2020   
 */

public class ELG_ShiftSummary_Service {

	public static ELG_Shift_Summary__c getShiftSummaryOrCreateNew(String shiftId) {
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c();
		List<ELG_Shift_Summary__c> shiftSummaryList = new List<ELG_Shift_Summary__c>();

		shiftSummaryList = ELG_ShiftSummary_Selector.getShiftSummaryBasedOnShift(shiftId);

		if (shiftSummaryList.isEmpty()) {
			List<ELG_Shift_Assignement__c> summaryShift = ELG_ShiftSummary_Selector.getShiftToCreateFirstSummary(shiftId);
			if (summaryShift.isEmpty()) {
				throw new HOG_Exception('The Shift Summary cannot be created for current shift. Please, contact HOG Administrator.');
			} else {
				summary.Shift_Assignement__c = summaryShift[0].Id;
				insert summary;
			}
		} else {
			summary = shiftSummaryList[0];
		}

		return summary;
	}

	public static ELG_Shift_Summary__c signOffShiftSummary(String shiftId) {
		Id currentUser = UserInfo.getUserId();
		ELG_Shift_Summary__c summary = ELG_ShiftSummary_Selector.getShiftSummaryBasedOnShift(shiftId)[0];

		if (summary.Shift_Assignement__r.Primary__c == currentUser) {
			if (summary.Status__c != ELG_Constants.SHIFT_SUMMARY_IN_PROGRESS) {
				throw new HOG_Exception(ELG_Constants.SHIFT_SUMMARY_CANNOT_BE_UPDATED);
			} else {
				summary.Status__c = ELG_Constants.SHIFT_SUMMARY_AWAITING_ACCEPT;
				summary.Signed_Off__c = Datetime.now();
				update summary;
			}
		} else {
			throw new HOG_Exception(ELG_Constants.ONLY_SHIFT_LEAD_PRIMARY_CAN_SIGN_OFF);
		}

		return summary;
	}

	public static ELG_Shift_Summary__c acceptSummary(String shiftId, String currentUser) {
		ELG_Shift_Summary__c summary = ELG_ShiftSummary_Selector.getShiftSummaryBasedOnShift(shiftId)[0];

		if (summary.Status__c != ELG_Constants.SHIFT_SUMMARY_AWAITING_ACCEPT) {
			throw new HOG_Exception(ELG_Constants.SHIFT_SUMMARY_CANNOT_BE_UPDATED);
		} else {
			summary.Status__c = ELG_Constants.SHIFT_SUMMARY_ACCEPTED;
			summary.Signed_In__c = Datetime.now();
			summary.Next_Shift_Lead__c = currentUser;
			update summary;
		}

		return summary;
	}

}