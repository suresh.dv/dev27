/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Save mode to decide if action should fail or not during unsuccessful persist action.
History:        jschn 2019-07-18 - Created. - EPD R1
*************************************************************************************************/
public enum EquipmentEngineSaveMode {

    ALL_OR_NOTHING_ENABLED,
    ALL_OR_NOTHING_DISABLED

}