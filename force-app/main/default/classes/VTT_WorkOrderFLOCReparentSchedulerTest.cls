/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VTT_WorkOrderFLOCReparentScheduler class
History:        jschn 2018-12-14 - Created. (US - 001323)
*************************************************************************************************/
@IsTest
private class VTT_WorkOrderFLOCReparentSchedulerTest {

    @IsTest
    public static void scheduleWithDefaultCron() {

        Test.startTest();
        Id jobId = VTT_WorkOrderFLOCReparentScheduler.schedule();
        Test.stopTest();

        CronTrigger job = [SELECT CronExpression, CronJobDetail.Name
        FROM CronTrigger
        WHERE Id = :jobId];
        System.assertEquals(VTT_WorkOrderFLOCReparentScheduler.CRON, job.CronExpression);
        System.assertEquals(VTT_WorkOrderFLOCReparentScheduler.JOB_NAME, job.CronJobDetail.Name);
    }

    @IsTest
    public static void scheduleWithCustomCron() {
        String customCronString = '0 0 3 ? * MON';

        Test.startTest();
        Id jobId = VTT_WorkOrderFLOCReparentScheduler.schedule(customCronString);
        Test.stopTest();

        CronTrigger job = [SELECT CronExpression, CronJobDetail.Name
        FROM CronTrigger
        WHERE Id = :jobId];
        System.assertEquals(customCronString, job.CronExpression);
        System.assertEquals(VTT_WorkOrderFLOCReparentScheduler.JOB_NAME, job.CronJobDetail.Name);
    }

}