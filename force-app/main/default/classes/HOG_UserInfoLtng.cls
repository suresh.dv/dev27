/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class which allows you to retrieve User specific settings for Lightning
Test Class:     HOG_UserInfoLtngTest
History:        jschn 2019-01-25 - Created.
*************************************************************************************************/
public with sharing class HOG_UserInfoLtng {

    private static final String CLASSNAME = String.valueOf(HOG_UserInfoLtng.class);
    public static final String ERROR_USER_WITHOUT_ROLE = 'User doesn\'t have assigned role.';

    @AuraEnabled
    public static UserRole getCurrentUserRole() {
        Id roleId = UserInfo.getUserRoleId();
        System.debug(CLASSNAME + ' -> getCurrentUserRole - User Role ID: ' + roleId);
        try {
            return [
                    SELECT Name, DeveloperName
                    FROM UserRole
                    WHERE Id = :roleId
            ];
        } catch(Exception ex) {
            System.debug(CLASSNAME + ' -> getCurrentUserRole - User role with Id "' + roleId + '" doesn\'t exist.');
            return null;
        }
    }

}