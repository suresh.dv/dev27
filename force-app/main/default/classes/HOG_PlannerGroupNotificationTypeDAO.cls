/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Abstract DAO class for Planner Group Notification Type SObject
History:        jschn 2019-07-03 - Created. - EPD R1
*************************************************************************************************/
public interface HOG_PlannerGroupNotificationTypeDAO {

    List<HOG_Planner_Group_Notification_Type__c> getNotificationTypesForRoleTypes(List<String> roleTypes);

}