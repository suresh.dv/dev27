/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Account DAO Layer implementation
Test Class:     HOG_AccountDAOImplTest
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public inherited sharing class HOG_AccountDAOImpl implements HOG_AccountDAO, VTT_SCAccountDAO {

    /**
     * Returns result of the query that selects Account records based on provided record type Id.
     * Only accounts that has proper Record Type Id or correct Husky AD CompanyName are selected
     * Results are ordered by Name (ASC)
     *
     * @param recordTypeId
     *
     * @return List<Account>
     */
    public List<Account> getHuskyAccountsByRecordTypeId(Id recordTypeId) {
        return [
                SELECT Name
                FROM Account
                WHERE RecordTypeId =: recordTypeId
                    OR Husky_AD_Companyname__c =: HOG_Constants.HUSKY_AD_COMPANY_NAME
                ORDER BY Name
        ];
    }

    /**
     * Returns result of the query that selects Account records with it's child contacts based on provided record type Id.
     * Only accounts that has proper Record Type Id or correct Husky AD CompanyName are selected
     * Results and child records (Contacts) are ordered by Name (ASC)
     * and number of child records (Contacts) is limited by 1000
     *
     * @param recordTypeId
     *
     * @return List<Account>
     */
    public List<Account> getAccountsWithContactsByRecordType(Id recordTypeId) {
        return [
                SELECT Id, Name,
                (
                        SELECT Id, Name
                        FROM Contacts
                        ORDER BY Name
                        LIMIT 1000
                )
                FROM Account
                WHERE RecordTypeId =: recordTypeId
                    OR Husky_AD_Companyname__c =: HOG_Constants.HUSKY_AD_COMPANY_NAME
                ORDER BY Name
        ];
    }

}