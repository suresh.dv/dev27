/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    DAO class for HOG_LookupService
History:        mbrimus 2020-01-22 - Created.
*************************************************************************************************/
public inherited sharing class HOG_LookupServiceDAO {
    private static final String CLASS_NAME = String.valueOf(HOG_LookupServiceDAO.class);

    public List<SObject> getData(String queryString) {
        try {
            List<SObject> returnList = (List<SObject>) Database.query(queryString);
            System.debug(CLASS_NAME + '-> Search result: ' + returnList);
            return returnList;
        } catch (Exception ex) {
            System.debug(CLASS_NAME + '-> Exception: ' + ex.getMessage());
            return null;
        }
    }
}