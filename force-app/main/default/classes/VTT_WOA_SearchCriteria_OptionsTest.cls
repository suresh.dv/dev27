/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WOA_SearchCriteria_Options
History:        jschn 25/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_SearchCriteria_OptionsTest {

    @IsTest
    static void buildCleanCriteria_withoutTradesman() {
        VTT_WOA_SearchCriteria_Options options;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().buildCleanCriteria();
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertEquals(true, options.cleanCriteria.hideCompletedFilter, 'Hide Completed filter should be defaulted to true');
        System.assertEquals(null, options.cleanCriteria.vendorFilter, 'Value should be null as tradesman was not provided');
        System.assertEquals(null, options.cleanCriteria.tradesmanFilter, 'Value should be null as tradesman was not provided');
    }

    @IsTest
    static void buildCleanCriteria_withTradesman() {
        VTT_WOA_SearchCriteria_Options options;
        Account acc = VTT_TestData.createVendorAccount('Avengers');
        Contact ct = VTT_TestData.createTradesmanContact('Black','Widow',acc.Id, UserInfo.getUserId());

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options()
                .setTradesman(ct)
                .buildCleanCriteria();
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertEquals(true, options.cleanCriteria.hideCompletedFilter, 'Hide Completed filter should be defaulted to true');
        System.assertEquals(ct.AccountId, options.cleanCriteria.vendorFilter, 'Value should be populated as tradesman was provided');
        System.assertEquals(ct.Id, options.cleanCriteria.tradesmanFilter, 'Value should be populated as tradesman was provided');
    }

    @IsTest
    static void setFlags_true() {
        VTT_WOA_SearchCriteria_Options options;
        Boolean expectedIsAdmin = true;
        Boolean expectedIsVendorSupervisor = true;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setFlags(expectedIsAdmin, expectedIsVendorSupervisor);
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertEquals(expectedIsAdmin, options.isAdmin, 'Is Admin flag should be set to true');
        System.assertEquals(expectedIsVendorSupervisor, options.isVendorSupervisor, 'Os Vendor Supervisor flag should be set to true');
    }

    @IsTest
    static void setFlags_false() {
        VTT_WOA_SearchCriteria_Options options;
        Boolean expectedIsAdmin = false;
        Boolean expectedIsVendorSupervisor = false;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setFlags(expectedIsAdmin, expectedIsVendorSupervisor);
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertEquals(expectedIsAdmin, options.isAdmin, 'Is Admin flag should be set to false');
        System.assertEquals(expectedIsVendorSupervisor, options.isVendorSupervisor, 'Os Vendor Supervisor flag should be set to false');
    }

    @IsTest
    static void setFlags_null() {
        VTT_WOA_SearchCriteria_Options options;
        Boolean expectedIsAdmin;
        Boolean expectedIsVendorSupervisor;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setFlags(expectedIsAdmin, expectedIsVendorSupervisor);
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertEquals(expectedIsAdmin, options.isAdmin, 'Is Admin flag should be set to null');
        System.assertEquals(expectedIsVendorSupervisor, options.isVendorSupervisor, 'Os Vendor Supervisor flag should be set to null');
    }

    @IsTest
    static void setTradesman() {
        VTT_WOA_SearchCriteria_Options options;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setTradesman(new Contact(User__c = UserInfo.getUserId()));
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.tradesman, 'Tradesman should be populated with provided record');
        System.assertEquals(UserInfo.getUserId(), options.tradesman.User__c, 'Id should be set');
    }

    @IsTest
    static void setVendors_nullFail() {
        VTT_WOA_SearchCriteria_Options options;
        Boolean failFLag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            options = new VTT_WOA_SearchCriteria_Options().setVendors(null);
        } catch (Exception ex) {
            failFLag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFLag, 'This action should fail as there is forloop without null check');
        System.assertEquals(null, options, 'Action failed so value should be null');
    }

    @IsTest
    static void setVendors_empty() {
        VTT_WOA_SearchCriteria_Options options;
        Integer expectedCount = 2;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setVendors(new List<Account>());
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.vendors, 'Vendors list should be initialised.');
        System.assertEquals(expectedCount, options.vendors.size(), 'Size of vendors list should be 2 as we didn\'t provide any record but there are 2 items by default');
    }

    @IsTest
    static void setVendors_withRecord() {
        VTT_WOA_SearchCriteria_Options options;
        Account acc = VTT_TestData.createVendorAccount('Avengers');
        Integer expectedCount = 3;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setVendors(new List<Account>{acc});
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.vendors, 'Vendors list should be initialised.');
        System.assertEquals(expectedCount, options.vendors.size(), 'Size of vendors list should be 3 as we did provide 1 record and there are 2 items by default');
    }

    @IsTest
    static void setTradesmans_nullFail() {
        VTT_WOA_SearchCriteria_Options options;
        Boolean failFLag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            options = new VTT_WOA_SearchCriteria_Options().setTradesmans(null);
        } catch (Exception ex) {
            failFLag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFLag, 'This action should fail as there is forloop without null check');
        System.assertEquals(null, options, 'Action failed so value should be null');
    }

    @IsTest
    static void setTradesmans_empty() {
        VTT_WOA_SearchCriteria_Options options;
        Integer expectedCount = 2;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setTradesmans(new List<Contact>());
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.tradesmans, 'Tradesmen list should be initialised.');
        System.assertEquals(expectedCount, options.tradesmans.size(), 'Size of tradesmans list should be 2 as we didn\'t provide any record, but there are 2 default values');
    }

    @IsTest
    static void setTradesmans_withRecord() {
        VTT_WOA_SearchCriteria_Options options;
        Account acc = VTT_TestData.createVendorAccount('Avengers');
        Contact contact = VTT_TestData.createTradesmanContact('Black','Widow',acc.Id, UserInfo.getUserId());
        Integer expectedCount = 3;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setTradesmans(new List<Contact>{contact});
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.tradesmans, 'Tradesmen list should be initialised.');
        System.assertEquals(expectedCount, options.tradesmans.size(), 'Size of tradesmans list should be 3 as we did provide 1 record and there are 2 default values');
    }

    @IsTest
    static void setAMUs_nullFail() {
        VTT_WOA_SearchCriteria_Options options;
        Boolean failFLag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            options = new VTT_WOA_SearchCriteria_Options().setAMUs(null);
        } catch (Exception ex) {
            failFLag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFLag, 'This action should fail as there is forloop without null check');
        System.assertEquals(null, options, 'Action failed so value should be null');
    }

    @IsTest
    static void setAMUs_empty() {
        VTT_WOA_SearchCriteria_Options options;
        Integer expectedCount = 1;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setAMUs(new List<Field__c>());
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.AMUs, 'AMUs list should be initialised.');
        System.assertEquals(expectedCount, options.AMUs.size(), 'Size of AMUs list should be 1 as we didn\'t provide any record, but there are 1 default value');
    }

    @IsTest
    static void setAMUs_withRecord() {
        VTT_WOA_SearchCriteria_Options options;
        Integer expectedCount = 2;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setAMUs(new List<Field__c>{new Field__c(Name = 'abc')});
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.AMUs, 'AMUs list should be initialised.');
        System.assertEquals(expectedCount, options.AMUs.size(), 'Size of AMUs list should be 2 as we did provide 1 record and there are 1 default value');
    }

    @IsTest
    static void setOrderTypes_nullFail() {
        VTT_WOA_SearchCriteria_Options options;
        Boolean failFLag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            options = new VTT_WOA_SearchCriteria_Options().setOrderTypes(null);
        } catch (Exception ex) {
            failFLag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFLag, 'This action should fail as there is forloop without null check');
        System.assertEquals(null, options, 'Action failed so value should be null');
    }

    @IsTest
    static void setOrderTypes_empty() {
        VTT_WOA_SearchCriteria_Options options;
        Integer expectedCount = 1;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setOrderTypes(new List<String>());
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.orderTypes, 'Order Type list should be initialised.');
        System.assertEquals(expectedCount, options.orderTypes.size(), 'Size of Order Type list should be 1 as we didn\'t provide any record, but there are 1 default value');
    }

    @IsTest
    static void setOrderTypes_withRecord() {
        VTT_WOA_SearchCriteria_Options options;
        Integer expectedCount = 2;

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setOrderTypes(new List<String>{'abc'});
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.orderTypes, 'Order Type list should be initialised.');
        System.assertEquals(expectedCount, options.orderTypes.size(), 'Size of Order Type list should be 2 as we did provide 1 record and there are 1 default value');
    }

    @IsTest
    static void setActivityStatuses_withRecord() {
        VTT_WOA_SearchCriteria_Options options;
        Integer expectedCount = VTT_Constants.FILTERABLE_WOA_STATUSES.size();

        Test.startTest();
        options = new VTT_WOA_SearchCriteria_Options().setActivityStatuses();
        Test.stopTest();

        System.assertNotEquals(null, options, 'Shouldn\'t fail so value shouldn\'t be null');
        System.assertNotEquals(null, options.activityStatuses, 'Activity Status list should be initialised.');
        System.assertEquals(expectedCount, options.activityStatuses.size(), 'Size of Activity Status list is based on VTT_Constants.FILTERABLE_WOA_STATUSES.');
    }

}