/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_ActiveForms {
	@AuraEnabled
	public List<Equipment_Missing_Form__c> addForms;
	@AuraEnabled
	public List<Equipment_Transfer_Form__c> transferToForms;
	@AuraEnabled
	public List<Equipment_Transfer_Form__c> transferFromForms;

	public Equipment_ActiveForms() {
		this.addForms = new List<Equipment_Missing_Form__c>();
		this.transferToForms = new List<Equipment_Transfer_Form__c>();
		this.transferFromForms = new List<Equipment_Transfer_Form__c>();
	}
}