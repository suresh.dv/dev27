/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector for SObject Work_Order_Activity_Assignment__c
Test Class:     VTT_WOA_AssignmentDAOImplTest
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_AssignmentDAOImpl implements VTT_WOA_AssignmentDAO {

    /**
     * Retrieves Work Order Activity Assignments based on activity Ids
     *
     * @param activityIdsForDeletion
     *
     * @return List<Work_Order_Activity_Assignment__c>
     */
    public List<Work_Order_Activity_Assignment__c> getAssignments(Set<Id> activityIdsForDeletion) {
        return [
                SELECT Work_Order_Activity__c, Tradesman__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Work_Order_Activity__c IN :activityIdsForDeletion
        ];
    }

    /**
     * Retrieves Work Order Activity Assignments based on Activity Ids, List of Activities and List of tradesman Ids.
     * Using activityIds AND (activities OR tradesmanIds) logic
     *
     * @param activityIdsForDeletion
     * @param activities
     * @param tradesmenIds
     *
     * @return List<Work_Order_Activity_Assignment__c>
     */
    public List<Work_Order_Activity_Assignment__c> getAssignments(Set<Id> activityIdsForDeletion, List<Work_Order_Activity__c> activities, List<Id> tradesmenIds) {
        return [
                SELECT Work_Order_Activity__c,Tradesman__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Work_Order_Activity__c IN :activityIdsForDeletion
                OR (
                        Work_Order_Activity__c IN :activities
                        AND Tradesman__c IN :tradesmenIds
                )
        ];
    }

    /**
     * Retrieves Work Order Activity Assignments based on Activities and tradesman Ids.
     *
     * @param activities
     * @param tradesmenIds
     *
     * @return List<Work_Order_Activity_Assignment__c>
     */
    public List<Work_Order_Activity_Assignment__c> getAssignments(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds) {
        return [
                SELECT Work_Order_Activity__c, Tradesman__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Work_Order_Activity__c IN :activities
                AND Tradesman__c IN :tradesmenIds
        ];
    }

    /**
     * Retrieves Non Rejected (Rejected != true) Work Order Activities Assignments based on tradesman (ID)
     *
     * @param tradesman
     *
     * @return List<Work_Order_Activity_Assignment__c>
     */
    public List<Work_Order_Activity_Assignment__c> getNonRejectedAssignmentsByTradesman(String tradesman) {
        return [
                SELECT Work_Order_Activity__c, Tradesman__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Rejected__c <> TRUE
                AND Tradesman__c = :tradesman
        ];
    }

    /**
     * Retrieves Non Rejected (Rejected != true) Work Order Activities Assignments based on activity Id
     *
     * @param activityId
     *
     * @return List<Work_Order_Activity_Assignment__c>
     */
    public List<Work_Order_Activity_Assignment__c> getNonRejectedAssignmentsByActivity(Id activityId) {
        return [
                SELECT Work_Order_Activity__c, Tradesman__c
                FROM Work_Order_Activity_Assignment__c
                WHERE Rejected__c <> TRUE
                AND Work_Order_Activity__c = :activityId
        ];
    }

}