public with sharing class CarsealStatusReport {
    public String reportRunTime {get; set;}
    public Map<String, List<Carseal__c>> carsealReport {get; set;}
    public String selectedUnits {get;set;} 

    public CarsealStatusReport() {
        setReportRunTime();
        
        if (ApexPages.currentPage().getParameters().get('unit') != null)   
            selectedUnits = ApexPages.currentPage().getParameters().get('unit');          
        
        carsealReport = buildReport(new List<String>());
    }
    
    public void runReport() {
        List<String> unitList = new List<String>();
        
        if (ApexPages.currentPage().getParameters().get('unit') != null)   
            selectedUnits = ApexPages.currentPage().getParameters().get('unit');          

        if (selectedUnits != null && selectedUnits != '')
        {
            unitList = selectedUnits.split(',');
        } 
        
        carsealReport = buildReport(unitList);       
    }
    
    public PageReference exportToPdf() {
        String url = '/apex/CarsealStatusReportExport?unit=' + selectedUnits;   
        PageReference pdfPage = new PageReference(url);
        
        return pdfPage;
    }
    
    public Map<String, List<Carseal__c>> getCarsealReport() {
        return this.carsealReport;
    }            

    private void setReportRunTime() {
        reportRunTime = System.Now().format('MM/dd/yyyy HH:mm a');
    }
    
    private Map<String, List<Carseal__c>> buildReport(List<String> unitList) {
        Map<String, List<Carseal__c>> carSelsByUnit = new Map<String, List<Carseal__c>>();

        String soql = 'SELECT Id, Name, Status_Updated_On__c, Current_Status_Text__c, P_ID_Status__c, Reference_Equipment__c, Equipment_Protected__c, Line__c,' +
                        'Size__c, Justification__c, Description__c, Special_Consideration__c, P_ID__c, Unit__r.Name ' +
                        'FROM Carseal__c ';

        if(!unitList.isEmpty()) {
            soql = soql + 'WHERE Unit__r.Name IN :unitList ';
        }

        soql = soql + 'ORDER BY Name ';

        for(Carseal__c carseal : (List<Carseal__c>)Database.query(soql)) {            

            if(carSelsByUnit.containsKey(carseal.Unit__r.Name)) {
                List<Carseal__c> carSeals = carSelsByUnit.get(carseal.Unit__r.Name);
                carSeals.add(carseal);
                carSelsByUnit.put(carseal.Unit__r.Name, carSeals);

            } else {
                List<Carseal__c> carSeals = new List<Carseal__c>();
                carSeals.add(carseal);
                carSelsByUnit.put(carseal.Unit__r.Name, carSeals);
            }
        }

        return carSelsByUnit;

    }

}