/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Simple response for Apex request.
Test Class:     HOG_SimpleResponseTest
History:        jschn 2019-07-01 - Created. - EPD R1
*************************************************************************************************/
public with sharing class HOG_SimpleResponse extends HOG_RequestResult {

    @AuraEnabled public String successMessage {get;set;}

    public HOG_SimpleResponse addSuccessMessage(String message) {
        success = true;
        successMessage = message;
        return this;
    }

    public List<String> addError(Exception ex) {
        return addError(HOG_ErrorPrettifyService.prettifyErrorMessage(ex.getMessage()));
    }

    public List<String> addError(String errorMsg) {
        if(this.errors == null) {
            this.errors = new List<String>();
        }
        this.errors.add(errorMsg);
        this.success = false;
        return this.errors;
    }

}