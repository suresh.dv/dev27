/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Related Class:  HOG_SObjectFactory
Description:    Vendor Time Tracking SObject field defaults for Testing purposes. 
History:        mbrimus 05.31.2018 - Created.
****************************************************************************************************
WARNING.. WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..
****************************************************************************************************
DO NOT CHANGE THIS CLASS, you can extend but dont change any values these, if you have to then rerun these tests
VTT_UtilitiesTest
**************************************************************************************************/
@isTest
public class VTT_FieldDefaults {

	public static final String CLASS_NAME = 'VTT_FieldDefaults';

	public class HOG_CauseDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
					HOG_Cause__c.Active__c => true,
					HOG_Cause__c.Name => 'TestCause',
					HOG_Cause__c.Cause_Description__c => 'TestDescription',
					HOG_Cause__c.Cause_Code__c => 'cause1',
					HOG_Cause__c.Code_Group__c => 'group1'
			};
		}
	}

	public class HOG_DamageDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
					HOG_Damage__c.Active__c => true,
					HOG_Damage__c.Name => 'TestDamage',
					HOG_Damage__c.Damage_Description__c => 'TestDescription',
					HOG_Damage__c.Damage_Code__c => 'damage1',
					HOG_Damage__c.Part_Code__c => 'partCd1'
			};
		}
	}

	public class HOG_PartDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
					HOG_Part__c.Active__c => true,
					HOG_Part__c.Name => 'TestPart',
					HOG_Part__c.Part_Description__c => 'TestDescription',
					HOG_Part__c.Part_Code__c => 'part1',
					HOG_Part__c.Catalogue_Code__c => 'partCd1'
			};
		}
	}

	public class Work_Order_Activity_Assignment_Rule_ItemDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
			};
		}
	}

	public class Work_Order_Activity_Assignment_RuleDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Work_Order_Activity_Assignment_Rule__c.Name => 'Rule 1',
		    	Work_Order_Activity_Assignment_Rule__c.Active__c => true,
		    	Work_Order_Activity_Assignment_Rule__c.MAT_Code__c => 'TXN',
		    	Work_Order_Activity_Assignment_Rule__c.Order_Type__c => 'WP01',
		    	Work_Order_Activity_Assignment_Rule__c.Planner_Group__c => null,
		    	Work_Order_Activity_Assignment_Rule__c.Plant_Section__c => null,
		    	Work_Order_Activity_Assignment_Rule__c.Recipient__c => 'Recipient1',
		    	Work_Order_Activity_Assignment_Rule__c.Unloading_Point__c => 'UnloadingPoint1',
		    	Work_Order_Activity_Assignment_Rule__c.Work_Center__c => '400'
			};
		}
	}

	public class HOG_Service_CategoryDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Service_Category__c.Name => 'Test Category New1',
				HOG_Service_Category__c.Service_Category_Code__c => 'WSR'
		    };
		}
	}

	public class HOG_Notification_TypeDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Notification_Type__c.Name => 'Test Notification New1',
				HOG_Notification_Type__c.Notification_Type__c => 'WP',
				HOG_Notification_Type__c.Order_Type__c => 'WP01'
		    };
		}
	}

	public class HOG_Service_Code_MATDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Service_Code_MAT__c.Name => 'Test MAT New1',
				HOG_Service_Code_MAT__c.MAT_Code__c => 'TXN',
				HOG_Service_Code_MAT__c.Record_Type_Name__c => 'Master'
		    };
		}
	}

	public class HOG_Service_RequiredDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Service_Required__c.Name => 'Test Service Required',
                HOG_Service_Required__c.Supervised__c => true,
                HOG_Service_Required__c.Validate_Pressure_Test__c => true,
                HOG_Service_Required__c.Validate_Start_and_Stop_Time__c => true
			};
		}
	}

	public class HOG_User_StatusDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_User_Status__c.Name => 'Test User Status',
                HOG_User_Status__c.Description__c => 'Test Description'
            };
		}
	}

	public class HOG_Work_Order_TypeDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Work_Order_Type__c.Active__c => true,
                HOG_Work_Order_Type__c.Record_Type_Name__c => 'Flushby - Production Flush'
            };
		}
	}

	public class HOG_Service_Request_Notification_FormDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Service_Request_Notification_Form__c.Notification_Number__c => 'W87654324',
                HOG_Service_Request_Notification_Form__c.Work_Order_Number__c => 'W87654324',
                HOG_Service_Request_Notification_Form__c.Title__c => 'Test Title',
                HOG_Service_Request_Notification_Form__c.Work_Details__c => 'Test Detail'
            };
		}
	}

	public class HOG_Maintenance_Servicing_FormDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		
		Double random = Math.Random();

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Maintenance_Servicing_Form__c.Name => 'Test New Data ' + random,
				HOG_Maintenance_Servicing_Form__c.Functional_Location__c => 'Test Floc',
				HOG_Maintenance_Servicing_Form__c.Notification_Type__c => 'WP',
				HOG_Maintenance_Servicing_Form__c.Order_Type__c => 'WP01',
				HOG_Maintenance_Servicing_Form__c.MAT_Code__c => 'TXN',
				HOG_Maintenance_Servicing_Form__c.ALT_Confirmed__c => true,
				HOG_Maintenance_Servicing_Form__c.Work_Order_Number__c => (''+random).left(15)
            };
		}
	}

	public class Work_Order_ActivityDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				Work_Order_Activity__c.Name => 'Test Name',
				Work_Order_Activity__c.Status__c => 'New',
				Work_Order_Activity__c.User_Status__c => 'RTS',
				Work_Order_Activity__c.Work_Center__c => '500',
            	Work_Order_Activity__c.Recipient__c => 'Recipient1',
            	Work_Order_Activity__c.Unloading_Point__c => 'UnloadingPoint1'
            };
		}
	}

	public class Work_Order_Activity_Log_EntryDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
			};
		}
	}

    public class Work_Order_Activity_LogDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
        public Map<Schema.SObjectField, Object> getFieldDefaults() {
            return new Map<Schema.SObjectField, Object> {
                    Work_Order_Activity_Log__c.Started_New__c => System.today(),
                    Work_Order_Activity_Log__c.Finished_New__c => System.today().addDays(1)
            };
        }
    }

	public class HOG_Work_Execution_Close_Out_ChecklistDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {
		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object>{
			};
		}
	}
}