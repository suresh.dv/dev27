/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non parallel Unit Test for VTT_WOA_Assignment_Service class
History:        jschn 04/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_Assignment_ServiceTestNP {

    @IsTest
    static void runAssignmentWithoutTradesmen_success() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;
        Integer assignmentsCount = 0;
        Integer beforeDeletionCount;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Account vendor2 = VTT_TestData.createVendorAccount('Hydra');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            VTT_TestData.createWorkOrderActivityAssignment(activities.get(0).Id, tradesman.Id);
            vendorAccountId = vendor.Id;
            beforeDeletionCount = [SELECT COUNT() FROM Work_Order_Activity_Assignment__c];
            activities.get(0).Assigned_Vendor__c = vendor2.Id;
            update activities;
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runAssignmentWithoutTradesmen(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.SUCCESS, response);
        System.assertEquals(vendorAccountId, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
        System.assertNotEquals(beforeDeletionCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
    }

    @IsTest
    static void runAssignmentWithTradesmen_success() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;
        Integer assignmentsCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Account vendor2 = VTT_TestData.createVendorAccount('Hydra');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            vendorAccountId = vendor.Id;
            tradesmenIds.add(tradesman.Id);
            activities.get(0).Assigned_Vendor__c = vendor2.Id;
            update activities;
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runAssignmentWithTradesmen(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.SUCCESS, response);
        System.assertEquals(vendorAccountId, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
    }

    @IsTest
    static void runAssignmentWithTradesmen_skipDeletion() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;
        Integer assignmentsCount = 1;
        Id assignmentRecordId;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            Work_Order_Activity_Assignment__c assignment = VTT_TestData.createWorkOrderActivityAssignment(activities.get(0).Id, tradesman.Id);
            vendorAccountId = vendor.Id;
            tradesmenIds.add(tradesman.Id);
            assignmentRecordId = assignment.Id;
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runAssignmentWithTradesmen(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.SUCCESS, response);
        System.assertEquals(vendorAccountId, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
        System.assertEquals(assignmentRecordId, [SELECT Id FROM Work_Order_Activity_Assignment__c LIMIT 1].get(0).Id);
    }

    @IsTest
    static void runUnAssignmentWithoutTradesmen_notAdmin() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;

        System.runAs(VTT_TestData.createVTTUser()) {
            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runUnAssignmentWithoutTradesmen(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runUnAssignmentWithoutTradesmen_withoutActivities() {
        List<Work_Order_Activity__c> activities;
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runUnAssignmentWithoutTradesmen(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runUnAssignmentWithoutTradesmen_withTradesman() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id> {UserInfo.getUserId()};
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runUnAssignmentWithoutTradesmen(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runUnAssignmentWithoutTradesmen_success() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;
        Integer assignmentsCount = 0;
        Integer beforeDeletionCount;

        System.runAs(VTT_TestData.createVTTAdminUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Account vendor2 = VTT_TestData.createVendorAccount('Hydra');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            VTT_TestData.createWorkOrderActivityAssignment(activities.get(0).Id, tradesman.Id);
            vendorAccountId = vendor.Id;
            beforeDeletionCount = [SELECT COUNT() FROM Work_Order_Activity_Assignment__c];
            activities.get(0).Assigned_Vendor__c = vendor2.Id;
            update activities;
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runUnAssignmentWithoutTradesmen(activities, tradesmenIds, vendorAccountId);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.SUCCESS, response);
        System.assertEquals(null, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
        System.assertNotEquals(beforeDeletionCount, assignmentsCount);
    }

    @IsTest
    static void runUnAssignmentWithTradesmen_success() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id>();
        VTT_WOA_Assignment_Response response;
        Integer assignmentsCount = 0;
        Integer beforeDeletionCount;

        System.runAs(VTT_TestData.createVTTAdminUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            activities.add(VTT_TestData.createWorkOrderActivity(workOrder.Id));
            VTT_TestData.createWorkOrderActivityAssignment(activities.get(0).Id, tradesman.Id);
            beforeDeletionCount = [SELECT COUNT() FROM Work_Order_Activity_Assignment__c];
            tradesmenIds.add(tradesman.Id);
            MaintenanceServicingUtilities.executeTriggerCode = true;

            Test.startTest();
            response = new VTT_WOA_Assignment_Service().runUnAssignmentWithTradesmen(activities, tradesmenIds);
            Test.stopTest();
        }

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.SUCCESS, response);
        System.assertEquals(null, activities.get(0).Assigned_Vendor__c);
        System.assert(String.isBlank(activities.get(0).Assigned_Text__c));
        System.assertEquals(assignmentsCount, [SELECT COUNT() FROM Work_Order_Activity_Assignment__c]);
        System.assertNotEquals(beforeDeletionCount, assignmentsCount);
    }

}