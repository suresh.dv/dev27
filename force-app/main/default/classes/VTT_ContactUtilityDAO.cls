/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO interface that is used to query Contact records for purpose of VTT Utility classes
History:        jschn 03/12/2019 - Created.
*************************************************************************************************/
public interface VTT_ContactUtilityDAO {

    List<Contact> getTradesmanInfo();

}