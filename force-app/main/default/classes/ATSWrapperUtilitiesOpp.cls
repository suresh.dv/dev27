public class ATSWrapperUtilitiesOpp {
 public class ATSQuoteWrapper {
  public Id accountId {
   get;
   set;
  }
  public String accountName {
   get;
   set;
  }
  public String accountNumber {
   get;
   set;
  }
  public String accountAddressCity {
   get;
   set;
  }
  public String accountAddressPostal {
   get;
   set;
  }
  public String accountAddressState {
   get;
   set;
  }
  public String accountAddressStreet {
   get;
   set;
  }
  public String customerName {
   get;
   set;
  }
  public String CustomerComments {
   get;
   set;
  }
  public Opportunity tender {
   get;
   set;
  }
  public String productFamily {
   get;
   set;
  }
  public String opportunityMarketer {
   get;
   set;
  }

  public String FobShipping {
   get;
   set;
  }

  public String oppComments {
   get;
   set;
  }
  public String opportunityType {
   get;
   set;
  }
  public decimal FreightCredits {
   get;
   set;
  }
  public Id contactId {
   get;
   set;
  }
  public String contactName {
   get;
   set;
  }
  public Date quoteDate {
   get;
   set;
  }
  public String contactFaxNumber {
   get;
   set;
  }
  public String contactComments {
   get;
   set;
  }
  public String contactEmailAddress {
   get;
   set;
  }

  public String tenderHeader {
   get;
   set;
  }
  public String autoNumber {
   get;
   set;
  }
  public Id tenderId {
   get;
   set;
  }
  public String tenderNumber {
   get;
   set;
  }
  public String tenderName {
   get;
   set;
  }
  public String description {
   get;
   set;
  }
  public String AsphaltNumber {
   get;
   set;
  }

  public Decimal freightCredit {
   get;
   set;
  }
  public String PO {
   get;
   set;
  }
  public String project {
   get;
   set;
  }
  public String fob {
   get;
   set;
  }
  public String priceValid {
   get;
   set;
  }
  public String priceValidto {
   get;
   set;
  }
  public DateTime priceValidFromOpp {
   get;
   set;
  }
  public DateTime priceValidToOpp {
   get;
   set;
  }

  public DateTime acceptanceDeadline {
   get;
   set;
  }
  public DateTime offerEndDate {
   get;
   set;
  }
  public String offerEndDateString {
   get;
   set;
  }
  public String additionalComments {
   get;
   set;
  }

  public String Broker {
   get;
   set;
  }
  public String Broker2 {
   get;
   set;
  }
  public String OppResultCat {
   get;
   set;
  }
  public String OppResultCat2 {
   get;
   set;
  }
  public String TenderNum {
   get;
   set;
  }
  public String TaxExempt {
   get;
   set;
  }
  public String TaxExempt2 {
   get;
   set;
  }
  public String PayOut {
   get;
   set;
  }
  public String PayOut2 {
   get;
   set;
  }
  public String SMSCon {
   get;
   set;
  }
  public String SMSCon2 {
   get;
   set;
  }
  public String POquote {
   get;
   set;
  }
  public String PO2quote {
   get;
   set;
  }
  public String SalesComments {
   get;
   set;
  }
  public String SalesComments2 {
   get;
   set;
  }
  public String TaxExemptPick {
   get;
   set;
  }
  public String TaxExemptPick2 {
   get;
   set;
  }
  public String ForSeason {
   get;
   set;
  }
  public String TradeClass {
   get;
   set;
  }
  public String Destination {
   get;
   set;
  }
  public String signatureName {
   get;
   set;
  }
  public String signatureImageURL {
   get;
   set;
  }
  public String marketerName {
   get;
   set;
  }
  public String marketerFax {
   get;
   set;
  }
  public String marketerEmail {
   get;
   set;
  }
  List < ATSOpportunityWrapper > opportunities;

  public List < ATSOpportunityWrapper > getOpportunities() {
   return opportunities;
  }
  
  public void setOpportunities(List < Opportunity > oppList, String CustomerType) {

   opportunities = new List < ATSOpportunityWrapper > ();
   for (Opportunity opp: oppList) {
    String opportynityType = opp.RecordType.Name;
    opportynityType = opportynityType.Replace('ATS:', '');
    opportynityType = opportynityType.Replace('Product Category', '').trim();

    //use in generate SMS report                
    //-->                if(CustomerType != null && CustomerType.containsIgnoreCase(opportynityType)) {
    if (CustomerType != null) {
     opportunities.add(new ATSOpportunityWrapper(opp));
    } else if (CustomerType == null) //use in genereate Quote
     opportunities.add(new ATSOpportunityWrapper(opp));
   }
  }
 }

 public class ATSOpportunityWrapper {
  public Opportunity obj {
   get;
   set;
  }
  public String RecordType {
   get;
   set;
  }
  public String ProductCategoryComments {
   get;
   set;
  }
  // public List<OpportunityLineItem> Products{get; set;}
  public List < ATSOpportunityProductWrapper > Products {
   get;
   set;
  }
  public ATS_Freight__c Freight {
   get;
   set;
  }
  public Boolean IncludeAxle5 {
   get;
   set;
  }
  public Boolean IncludeAxle6 {
   get;
   set;
  }
  public Boolean IncludeAxle7 {
   get;
   set;
  }
  public Boolean IncludeAxle8 {
   get;
   set;
  }
  public Boolean AsphaltIncludeAxle5 {
   get;
   set;
  }
  public Boolean AsphaltIncludeAxle6 {
   get;
   set;
  }
  public Boolean AsphaltIncludeAxle7 {
   get;
   set;
  }
  public Boolean AsphaltIncludeAxle8 {
   get;
   set;
  }
  public Boolean EmulsionIncludeAxle5 {
   get;
   set;
  }
  public Boolean EmulsionIncludeAxle6 {
   get;
   set;
  }
  public Boolean EmulsionIncludeAxle7 {
   get;
   set;
  }
  public Boolean EmulsionIncludeAxle8 {
   get;
   set;
  }
  public Boolean ResidualIncludeAxle5 {
   get;
   set;
  }
  public Boolean ResidualIncludeAxle6 {
   get;
   set;
  }
  public Boolean ResidualIncludeAxle7 {
   get;
   set;
  }
  public Boolean ResidualIncludeAxle8 {
   get;
   set;
  }

  public ATSOpportunityWrapper(Opportunity opp) {

   obj = opp;
   // Products = opp.OpportunityLineItems;

   IncludeAxle5 = False;
   IncludeAxle6 = False;
   IncludeAxle7 = False;
   IncludeAxle8 = False;
   AsphaltIncludeAxle5 = False;
   AsphaltIncludeAxle6 = False;
   AsphaltIncludeAxle7 = False;
   AsphaltIncludeAxle8 = False;
   EmulsionIncludeAxle5 = False;
   EmulsionIncludeAxle6 = False;
   EmulsionIncludeAxle7 = False;
   EmulsionIncludeAxle8 = False;
   ResidualIncludeAxle5 = False;
   ResidualIncludeAxle6 = False;
   ResidualIncludeAxle7 = False;
   ResidualIncludeAxle8 = False;

   System.debug('opp.OpportunityLineItems : ' + opp.OpportunityLineItems);
   for (OpportunityLineItem oli: opp.OpportunityLineItems) {
    IncludeAxle5 = IncludeAxle5 || oli.Include_Axle_5__c;
    IncludeAxle6 = IncludeAxle6 || oli.Include_Axle_6__c;
    IncludeAxle7 = IncludeAxle7 || oli.Include_Axle_7__c;
    IncludeAxle8 = IncludeAxle8 || oli.Include_Axle_8__c;
    if (oli.cpm_Product_Family__c == 'Asphalt') {
     AsphaltIncludeAxle5 = AsphaltIncludeAxle5 || oli.Include_Axle_5__c;
     AsphaltIncludeAxle6 = AsphaltIncludeAxle6 || oli.Include_Axle_6__c;
     AsphaltIncludeAxle7 = AsphaltIncludeAxle7 || oli.Include_Axle_7__c;
     AsphaltIncludeAxle8 = AsphaltIncludeAxle8 || oli.Include_Axle_8__c;
    }
    if (oli.cpm_Product_Family__c == 'Emulsion') {
     EmulsionIncludeAxle5 = EmulsionIncludeAxle5 || oli.Include_Axle_5__c;
     EmulsionIncludeAxle6 = EmulsionIncludeAxle6 || oli.Include_Axle_6__c;
     EmulsionIncludeAxle7 = EmulsionIncludeAxle7 || oli.Include_Axle_7__c;
     EmulsionIncludeAxle8 = EmulsionIncludeAxle8 || oli.Include_Axle_8__c;
    }
    if (oli.cpm_Product_Family__c == 'Residual') {
     ResidualIncludeAxle5 = ResidualIncludeAxle5 || oli.Include_Axle_5__c;
     ResidualIncludeAxle6 = ResidualIncludeAxle6 || oli.Include_Axle_6__c;
     ResidualIncludeAxle7 = ResidualIncludeAxle7 || oli.Include_Axle_7__c;
     ResidualIncludeAxle8 = ResidualIncludeAxle8 || oli.Include_Axle_8__c;
    }
   }

   if (opp.Freight_Ats__r != null && opp.Freight_Ats__r.size() > 0)
    Freight = opp.Freight_Ats__r[0];

   Products = new List < ATSOpportunityProductWrapper > ();
   for (OpportunityLineItem product: opp.OpportunityLineItems) {
    Products.add(new ATSOpportunityProductWrapper(product, Freight, opp));
   }
  }
 }
 public class ATSOpportunityProductWrapper {
  public OpportunityLineItem obj {
   get;
   set;
  }
  public ATS_Freight__c Freight {
   get;
   set;
  }
  public Opportunity myOpportunity {
   get;
   set;
  }
  public Decimal SalesPrice {
   get;
   set;
  }
  public Decimal PickupPrice {
   get;
   set;
  }
  public Decimal Axle5Price {
   get;
   set;
  }
  public Decimal Axle6Price {
   get;
   set;
  }
  public Decimal Axle7Price {
   get;
   set;
  }
  public Decimal Axle8Price {
   get;
   set;
  }

  private Decimal productDensity;

  public ATSOpportunityProductWrapper(OpportunityLineItem product, ATS_Freight__c fr, Opportunity parentOpp) {
   System.debug('parentOpp = ' + parentOpp);
   obj = product;
   Freight = fr;
   myOpportunity = parentOpp;
   productDensity = product.PricebookEntry.Product2.Density__c;


   System.debug('myOpportunity.RecordType.Name = ' + myOpportunity.RecordType.Name);


   /*   if( (myOpportunity.RecordType.Name != 'ATS: Asphalt Product Category' && 
            myOpportunity.RecordType.Name != 'ATS: Emulsion Product Category') ||
           Freight == null ||
           (Freight.Prices_F_O_B__c == 'Destination' || 
            Freight.Prices_F_O_B__c == 'Origin'))    */


   if ((myOpportunity.RecordType.Name != 'Asphalt') || Freight == null || (Freight.Prices_F_O_B__c == 'Destination' || Freight.Prices_F_O_B__c == 'Origin')) {
    System.debug('Not Asphalt and Not Emulsion Or Freight == null Or Destination or Origin');
    SalesPrice = obj.Sales_Price__c;
    PickupPrice = obj.cpm_Pickup_price__c;
    Axle5Price = obj.Axle_5_Price__c;
    Axle6Price = obj.Axle_6_Price__c;
    Axle7Price = obj.Axle_7_Price__c;
    Axle8Price = obj.Axle_8_Price__c;
   } else { //if Freight.Prices_F_O_B__c == 'Origin + Freight'
    if (Freight.Husky_Supplier_1_Selected__c == true && Freight.cpm_Product_Type__c == 'Asphalt') {
     System.debug('Input Husky 1');
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_1_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier1__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier_1__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier1__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier1__c, conversionRatio);
     System.debug('Output Husky 1');
    } else if (Freight.Husky_Supplier_2_Selected__c == true && Freight.cpm_Product_Type__c == 'Asphalt') {
     System.debug('Input Husky 2');
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_2_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_2_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_2_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier2__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier2__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier2__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier2__c, conversionRatio);
     System.debug('Uputput Husky 2');
    } else if (Freight.Husky_Supplier_3_Selected__c == true && Freight.cpm_Product_Type__c == 'Asphalt') {
     System.debug('Input Husky 3');
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_3_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_3_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_3_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier3__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier3__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier3__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier3__c, conversionRatio);
     System.debug('Uoutput Husky 3');
    } else {
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_1_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier1__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier_1__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier1__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier1__c, conversionRatio);
    }

   }
   if ((myOpportunity.RecordType.Name != 'Asphalt') || Freight == null || (Freight.Prices_F_O_B__c == 'Destination' || Freight.Prices_F_O_B__c == 'Origin')) {
    System.debug('Not Asphalt and Not Emulsion Or Freight == null Or Destination or Origin');
    SalesPrice = obj.Sales_Price__c;
    PickupPrice = obj.cpm_Pickup_price__c;
    Axle5Price = obj.Axle_5_Price__c;
    Axle6Price = obj.Axle_6_Price__c;
    Axle7Price = obj.Axle_7_Price__c;
    Axle8Price = obj.Axle_8_Price__c;
   } else { //if Freight.Prices_F_O_B__c == 'Origin + Freight'
    if (Freight.Husky_Supplier_1_Selected__c == true && Freight.cpm_Product_Type__c == 'Emulsion') {
     System.debug('Input Husky 1');
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_1_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier1__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier_1__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier1__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier1__c, conversionRatio);
     System.debug('Output Husky 1');
    } else if (Freight.Husky_Supplier_2_Selected__c == true && Freight.cpm_Product_Type__c == 'Emulsion') {
     System.debug('Input Husky 2');
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_2_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_2_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_2_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier2__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier2__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier2__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier2__c, conversionRatio);
     System.debug('Uputput Husky 2');
    } else if (Freight.Husky_Supplier_3_Selected__c == true && Freight.cpm_Product_Type__c == 'Emulsion') {
     System.debug('Input Husky 3');
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_3_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_3_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_3_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier3__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier3__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier3__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier3__c, conversionRatio);
     System.debug('Uoutput Husky 3');
    } else {
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_1_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier1__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier_1__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier1__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier1__c, conversionRatio);
    }

   }
   if ((myOpportunity.RecordType.Name != 'Asphalt') || Freight == null || (Freight.Prices_F_O_B__c == 'Destination' || Freight.Prices_F_O_B__c == 'Origin')) {
    System.debug('Not Asphalt and Not Emulsion Or Freight == null Or Destination or Origin');
    SalesPrice = obj.Sales_Price__c;
    PickupPrice = obj.cpm_Pickup_price__c;
    Axle5Price = obj.Axle_5_Price__c;
    Axle6Price = obj.Axle_6_Price__c;
    Axle7Price = obj.Axle_7_Price__c;
    Axle8Price = obj.Axle_8_Price__c;
   } else { //if Freight.Prices_F_O_B__c == 'Origin + Freight'
    if (Freight.Husky_Supplier_1_Selected__c == true && Freight.cpm_Product_Type__c == 'Residual') {

     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_1_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier1__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier_1__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier1__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier1__c, conversionRatio);

    } else if (Freight.Husky_Supplier_2_Selected__c == true && Freight.cpm_Product_Type__c == 'Residual') {

     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_2_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_2_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_2_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier2__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier2__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier2__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier2__c, conversionRatio);

    } else if (Freight.Husky_Supplier_3_Selected__c == true && Freight.cpm_Product_Type__c == 'Residual') {
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_3_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_3_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_3_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier3__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier3__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier3__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier3__c, conversionRatio);
    } else {
     Decimal conversionRatio = UnitConversions.getConversionRatio(obj.Unit__c, Freight.Supplier_1_Unit__c, productDensity);
     SalesPrice = getConvertedPrice(obj.Sales_Price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     PickupPrice = getConvertedPrice(obj.cpm_Pickup_price__c, Freight.Supplier_1_Rate__c, conversionRatio);
     Axle5Price = getConvertedPrice(obj.Axle_5_Price__c, Freight.Emulsion_Rate5_Supplier1__c, conversionRatio);
     Axle6Price = getConvertedPrice(obj.Axle_6_Price__c, Freight.Emulsion_Rate6_Supplier_1__c, conversionRatio);
     Axle7Price = getConvertedPrice(obj.Axle_7_Price__c, Freight.Emulsion_Rate7_Supplier1__c, conversionRatio);
     Axle8Price = getConvertedPrice(obj.Axle_8_Price__c, Freight.Emulsion_Rate8_Supplier1__c, conversionRatio);
    }
   }
  }

  private Decimal getConvertedPrice(Decimal salePrice, Decimal freightPrice, Decimal convRatio) {
   return ((salePrice == null) ? 0 : salePrice) +
    ((freightPrice == null) ? 0 : freightPrice) / convRatio;
  }
 }

}