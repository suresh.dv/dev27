/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_AddForm_File {

	/* Serial Plate File */
	public String originalSerialPlateFileId;
	public String serialPlateBase64Data;
	public String serialPlateName;

	/* Serial Plate Attachment */
	public String originalSerialPlateAttachmentId;

	/* Attachment File */
	public String originalAttachmentFileId;
	public String attachmentBase64Data;
	public String attachmentFileName;

	/* Attachment */
	public String originalAttachmentId;
	public Boolean insertSerialPlate;
	public Boolean insertAttachment;

}