/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_Rule_TradesmanWorkStatusValidation validation that is processing non completed activity to
                see if tradesman can work on it.
Test Class:     VTT_LTNG_WorkFlowEngineTest
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public inherited sharing class VTT_Rule_TradesmanWorkStatusValidation implements VTT_LTNG_EngineRule {
    Work_Order_Activity__c activity;
    Contact tradesman;

    Set<VTT_LTNG_EngineMessage> messages = new Set<VTT_LTNG_EngineMessage>();
    Set<VTT_LTNG_EngineAction> availableActions = new Set<VTT_LTNG_EngineAction>();
    Boolean passed = false;

    public VTT_Rule_TradesmanWorkStatusValidation(Work_Order_Activity__c activity, Contact tradesman) {
        this.activity = activity;
        this.tradesman = tradesman;
    }

    public Boolean passedRule() {
        return this.passed;
    }

    public void processRule() {
        Boolean isAssigned = activity.Work_Order_Activity_Assignments__r.size() > 0;

        if (!isAssigned) {
            this.messages.add(new VTT_LTNG_EngineMessage('You are not assigned Tradesman for this Activity.',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_WARNING));
            this.passed = false;
            return;
        }

        if (tradesman.Current_Work_Order_Activity__c <> null
                && tradesman.Current_Work_Order_Activity__c <> activity.Id
                && isAssigned) {
            // Message with link
            this.messages.add(new VTT_LTNG_EngineMessage(
                    'You are currently working on a different Activity. Activity Id: ',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_WARNING,
                    'Work_Order_Activity__c',
                    tradesman.Current_Work_Order_Activity__r.Name,
                    tradesman.Current_Work_Order_Activity__c));

            this.availableActions.add(
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get('Reject Activity'));
            this.passed = false;
            return;
        }

        if (tradesman.Current_Work_Order_Activity__c == null
                && activity.Maintenance_Work_Order__r.On_Hold__c
                && isAssigned) {
            this.messages.add(new VTT_LTNG_EngineMessage('This Work Order is currently ON HOLD',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_WARNING));
            this.passed = false;
            return;
        }

        this.passed = true;
    }

    public Set<VTT_LTNG_EngineMessage> getMessages() {
        return messages;
    }

    public Set<VTT_LTNG_EngineAction> getAvailableActions() {
        return availableActions;
    }
}