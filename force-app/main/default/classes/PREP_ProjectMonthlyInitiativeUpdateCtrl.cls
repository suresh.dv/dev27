public with sharing class PREP_ProjectMonthlyInitiativeUpdateCtrl {
    public String projId {get;set;}
    public String selectedYear {get;set;}
    public List<SelectOption> awardYears {get;set;}
    public String searchTerm {get;set;}
    
    public PREP_Baseline__c baseline {get;set;}
    public integer noOneOffPackages {get;set;}
    public integer noMSAPackages {get;set;}
    public integer noFullPackages {get;set;}
    public integer noPartialPackages {get;set;}
    public integer noLocalPackages {get;set;}
    public integer noGlobalPackages {get;set;}
    public List<PREP_Mat_Serv_Group_Update__c> scheduleStatusList {get;set;}
   
    public decimal totalAwardedSpend {get;set;}
    public decimal totalTarget {get;set;}
    public decimal totalSpendToMarket {get;set;}

    public PREP_ProjectMonthlyInitiativeUpdateCtrl()
    {
    
    }
    public void loadAwardYears()
    {
        awardYears = new List<SelectOption>();
        if (projId != null)
        {
            awardYears.add(new SelectOption('All', 'All'));
            for(PREP_Baseline__c baseline : [select Id, Planned_Award_Year__c from PREP_Baseline__c where Initiative_Id__c =: projId order by Planned_Award_Year__c])
            {
                awardYears.add(new SelectOption(baseline.Planned_Award_Year__c, baseline.Planned_Award_Year__c));
            }
        }
    }
    @RemoteAction
    public static List<PREP_Initiative__c> searchProject(String searchTerm) {
        System.debug('Movie Name is: '+searchTerm );
        String query = 'Select Id, Initiative_Name__c from PREP_Initiative__c '+
                        'where Initiative_Name__c like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' ' +
                        'and RecordType.DeveloperName = \'Projects_Initiative\'';
        List<PREP_Initiative__c> projectList = Database.query(query);
        return projectList;
    }
    public PageReference runSearch()
    {   
        System.debug(projId);
        System.debug(selectedYear);
        baseline = null;
        //find baseline
        Map<Id, PREP_Baseline__c> baselineMap = new Map<Id, PREP_Baseline__c>();
        if (selectedYear == 'All')
            baselineMap = new Map<Id, PREP_Baseline__c>([select Id, Initiative_Id__r.Initiative_Name__c, Initiative_Id__r.Discipline__c, Initiative_Id__r.Business_Unit__c, 
                                Initiative_Id__r.TIC__c, Initiative_Id__r.Phase_4_Start_Date__c, Initiative_Id__r.Phase_4_Completion_Date__c, 
                                Initiative_Id__r.FCM_Level__c, Initiative_Id__r.Procurement_Representative__c, Initiative_Id__r.Project_Lead__c, 
                                P_Baseline_Spend_Dollar_To_Be_Awarded__c, MSA_Baseline_Spend_Dollar__c
                            from PREP_Baseline__c 
                            where Initiative_Id__c =: projId ]);
        else
            baselineMap = new Map<Id, PREP_Baseline__c>([select Id, Initiative_Id__r.Initiative_Name__c, Initiative_Id__r.Discipline__c, Initiative_Id__r.Business_Unit__c, 
                                Initiative_Id__r.TIC__c, Initiative_Id__r.Phase_4_Start_Date__c, Initiative_Id__r.Phase_4_Completion_Date__c, 
                                Initiative_Id__r.FCM_Level__c, Initiative_Id__r.Procurement_Representative__c, Initiative_Id__r.Project_Lead__c, 
                                P_Baseline_Spend_Dollar_To_Be_Awarded__c, MSA_Baseline_Spend_Dollar__c
                            from PREP_Baseline__c 
                            where Calendar_Year(XCAC2_CAC_Award_Approval__c) = : integer.valueOf(selectedYear) and
                                    Initiative_Id__c =: projId]);                                           
        if (baselineMap.size() > 0)
        {   
            baseline = baselineMap.values()[0];
            
            noOneOffPackages = 0;
            noMSAPackages = 0;
            noFullPackages = 0;
            noPartialPackages = 0;
            noLocalPackages = 0;
            noGlobalPackages = 0;
            totalAwardedSpend = 0;
            totalSpendToMarket = 0;
            totalTarget = 0;
            for(PREP_Baseline__c b :  baselineMap.values())
            {
                totalSpendToMarket += b.P_Baseline_Spend_Dollar_To_Be_Awarded__c == null 
                                        ? 0 : b.P_Baseline_Spend_Dollar_To_Be_Awarded__c;
                totalTarget += b.MSA_Baseline_Spend_Dollar__c == null ? 0 : b.MSA_Baseline_Spend_Dollar__c;             
            }
            
            for(PREP_Mat_Serv_Group_Baseline_Allocation__c pack : [select Id, Contract_Type__c, Rate_Validation__c, Global_Sourcing__c,
                                                                    Baseline_Id__c, Baseline_Id__r.MSA_Baseline_Spend_Dollar__c
                                                                    from PREP_Mat_Serv_Group_Baseline_Allocation__c 
                                                                    where Baseline_Id__c in: baselineMap.keySet()])
            {
                
                if (pack.Contract_Type__c == 'MSA')
                {
                    noMSAPackages ++;
                }
                else if (pack.Contract_Type__c == 'One Off')    
                {
                    noOneOffPackages ++;
                    if (pack.Rate_Validation__c == 'Yes')
                        noFullPackages ++;
                    else if (pack.Rate_Validation__c == 'Partial')  
                        noPartialPackages++;
                    if (pack.Global_Sourcing__c == 'Yes')
                        noGlobalPackages++;
                    else if (pack.Global_Sourcing__c == 'No')
                        noLocalPackages ++; 
                }
            }
            
            scheduleStatusList = new List<PREP_Mat_Serv_Group_Update__c>();
            
            for(PREP_Mat_Serv_Group_Planned_Schedule__c sche : [select Id, RecordType.DeveloperName, Mat_Serv_Group_Baseline_Allocation_Id__r.Contract_Type__c,
                                                                    (select Id, Equipment_Service__c, Status_Delivery_Variance_Days_Serv__c, Actual_Spend_Allocation_Dollar__c,
                                                                        Status_Delivery_Variance_Days_Mat__c, Status_Contract_Execution__c, RecordType.DeveloperName 
                                                                    FROM Mat_Serv_Group_Updates__r WHERE Completed__c = true ORDER BY LastModifiedDate DESC LIMIT 1) 
                                                                 from PREP_Mat_Serv_Group_Planned_Schedule__c
                                                                 where Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c in: baselineMap.keySet()])
            {
                if (sche.Mat_Serv_Group_Updates__r.size() > 0)
                {
                    PREP_Mat_Serv_Group_Update__c item = sche.Mat_Serv_Group_Updates__r[0];
                    System.debug(item);
                    if (sche.Mat_Serv_Group_Baseline_Allocation_Id__r.Contract_Type__c == 'MSA')
                    	totalAwardedSpend += item.Actual_Spend_Allocation_Dollar__c == null ? 0 : item.Actual_Spend_Allocation_Dollar__c;
                    
                    if (sche.RecordType.DeveloperName.contains('Mat_Group_Baseline_Schedule'))
                    {
                        if (item.Status_Delivery_Variance_Days_Mat__c.contains('red') || item.Status_Contract_Execution__c.contains('red'))
                        {
                            scheduleStatusList.add(item);
                            System.debug('add to list = ' + item);  
                        }
                        
                    }
                    else if (sche.RecordType.DeveloperName.contains('Serv_Group_Baseline_Schedule'))
                    {
                        if (item.Status_Delivery_Variance_Days_Serv__c.contains('red') || item.Status_Contract_Execution__c.contains('red'))
                        {
                            scheduleStatusList.add(item);
                            System.debug('add to list = ' + item);  
                        }
                    }
                }
            }
            System.debug(scheduleStatusList);   
        
        }
        return null;        
    }
    @RemoteAction 
    public static List<PREP_ChartData> getSpendToMarketChartData(String initId, String selectedYear, String runReportDate)
    {
        return getChartData(initId, selectedYear, runReportDate, new List<String>{'One Off', 'MSA'}, 'Spend');                  
    }
    //exclude packages where contract type ='MSA'
    @RemoteAction 
    public static List<PREP_ChartData> getSavingsChartData(String initId, String selectedYear, String runReportDate)
    {
        return getChartData(initId, selectedYear, runReportDate, new List<String>{'One Off'}, 'Saving');
    }
    private static List<PREP_ChartData> getChartData(String initId, String selectedYear, String runReportDate, 
    												List<String> contractTypes, String strType)
    {
         integer reportMonth = Integer.valueOf(runReportDate.split('/')[0]);
         integer reportYear = Integer.valueOf(runReportDate.split('/')[1]);
         System.debug('reportMonth = ' + reportMonth);
         System.debug('reportYear =' + reportYear);
         //key = Month/Year, rollup planned/forecast/actual amt by Month/Year
         Map<String, PREP_ChartData> dataMap = new Map<String, PREP_ChartData>();
         String defaultKey = runReportDate;
         System.debug('defaultKey =' + defaultKey);
         dataMap.put(defaultKey, new PREP_ChartData(defaultKey, reportMonth, reportYear));
         
         System.debug(contractTypes);
         Map<Id, PREP_Baseline__c> baselineMap = new Map<Id, PREP_Baseline__c>();
         if (selectedYear == 'All')
            baselineMap = new Map<Id, PREP_Baseline__c>([select Id from PREP_Baseline__c where Initiative_Id__c =: initId ]);
         else
         {
            baselineMap = new Map<Id, PREP_Baseline__c>([select Id from PREP_Baseline__c 
                                                        where Calendar_Year(XCAC2_CAC_Award_Approval__c) =: integer.valueOf(selectedYear) and
                                                            Initiative_Id__c =: initId ]);
         }                                              
        if (baselineMap.size() > 0)
        { 
            PREP_Baseline__c baseline = baselineMap.values()[0];
            for(PREP_Mat_Serv_Group_Planned_Schedule__c sche : [select Id, RecordType.DeveloperName,
                                                                    (select Id, RecordType.DeveloperName, Planned_Decision_Summary_Approved__c,
                                                                            Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Savings_Allocation_Dollar__c,
                                                                            Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Spend_Allocation_Dollar__c,
                                                                            Forecast_Decision_Summary_Approved_Mat__c, Forecast_Decision_Summary_Approved_Serv__c 
                                                                    FROM Mat_Serv_Group_Updates__r WHERE Completed__c = true ORDER BY LastModifiedDate DESC LIMIT 1) 
                                                                 from PREP_Mat_Serv_Group_Planned_Schedule__c
                                                                 where Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c in: baselineMap.keySet()
                                                                        and Mat_Serv_Group_Baseline_Allocation_Id__r.Contract_Type__c in: contractTypes])
            {
                System.debug(sche);
                if (sche.Mat_Serv_Group_Updates__r.size() > 0)
                {
                    PREP_Mat_Serv_Group_Update__c item = sche.Mat_Serv_Group_Updates__r[0];
                    decimal amt = 0;
                    if (strType == 'Spend')
                    {
                        amt = item.Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Spend_Allocation_Dollar__c == null
                            ? 0 : item.Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Spend_Allocation_Dollar__c;
                    }
                    else
                        amt = item.Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Savings_Allocation_Dollar__c == null
                            ? 0 : item.Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Savings_Allocation_Dollar__c;
                     //calcuate planned amt
                     System.debug('calculate planned amt');
                    if (item.Planned_Decision_Summary_Approved__c != null)
                    {
                        integer month = item.Planned_Decision_Summary_Approved__c.month();
                        integer year = item.Planned_Decision_Summary_Approved__c.year();
                        String key = month + '/' + year;
                        
                        if (dataMap.containsKey(key))
                        {
                            PREP_ChartData obj = dataMap.get(key);
                            obj.plannedAmt = (obj.plannedAmt == null ? 0 : obj.plannedAmt) + amt;
                            System.debug(obj);
                        }
                        else
                            dataMap.put(key, new PREP_ChartData(key, month, year, amt));
                    }
                    //calculate forecast amt after the run report date
                    if (item.Forecast_Decision_Summary_Approved_Mat__c != null || item.Forecast_Decision_Summary_Approved_Serv__c != null)
                    {
                        String key;
                        integer month, year;
                        if (item.RecordType.DeveloperName.contains('Mat_Group_Update') && 
                            item.Forecast_Decision_Summary_Approved_Mat__c != null &&
                            ((item.Forecast_Decision_Summary_Approved_Mat__c.month() >= reportMonth && item.Forecast_Decision_Summary_Approved_Mat__c.year() == reportYear) ||
                            item.Forecast_Decision_Summary_Approved_Mat__c.year() > reportYear))
                        {
                            key = item.Forecast_Decision_Summary_Approved_Mat__c.month() + '/' + item.Forecast_Decision_Summary_Approved_Mat__c.year();
                            month = item.Forecast_Decision_Summary_Approved_Mat__c.month();
                            year = item.Forecast_Decision_Summary_Approved_Mat__c.year();
                        }
                        else if (item.RecordType.DeveloperName.contains('Serv_Group_Update') && 
                                item.Forecast_Decision_Summary_Approved_Serv__c != null &&
                                ((item.Forecast_Decision_Summary_Approved_Serv__c.month() >= reportMonth && item.Forecast_Decision_Summary_Approved_Serv__c.year() == reportYear) ||
                                item.Forecast_Decision_Summary_Approved_Serv__c.year() > reportYear))
                        {
                            key = item.Forecast_Decision_Summary_Approved_Serv__c.month() + '/' + item.Forecast_Decision_Summary_Approved_Serv__c.year();
                            month = item.Forecast_Decision_Summary_Approved_Serv__c.month();
                            year = item.Forecast_Decision_Summary_Approved_Serv__c.year();
                        }
                        if (key != null)
                        {
                            System.debug('key =' + key);
                            if (dataMap.containsKey(key))
                            {
                                System.debug('contains');
                                PREP_ChartData obj = dataMap.get(key);
                                obj.forecastAmt = (obj.forecastAmt == null ? 0 : obj.forecastAmt) + amt;
                                System.debug(obj);
                            }
                            else
                            {
                                System.debug('first');
                                PREP_ChartData obj = new PREP_ChartData(key, month, year);
                                obj.forecastAmt = amt;
                                dataMap.put(key, obj);
                                System.debug(obj);
                            }
                            
                        }
                    }
                }
            }
            //calculate actual amt before the run report date
            for(AggregateResult item : [SELECT Calendar_Month(Contract_Purchase_Approve_Date__c) Month, Calendar_Year(Contract_Purchase_Approve_Date__c) Year, 
                                                sum(Total_Negotiated_Savings__c) TotalSaving, sum(Addressable_Spend__c) TotalSpend  
                                        FROM PREP_Business_Value_Submission__c 
                                        where ((Calendar_Month(Contract_Purchase_Approve_Date__c) <=: reportMonth and  Calendar_Year(Contract_Purchase_Approve_Date__c) =: reportYear)
                                                or Calendar_Year(Contract_Purchase_Approve_Date__c) <:  reportYear) and
                                            Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c in: baselineMap.keySet() and
                                            Mat_Serv_Group_Baseline_Allocation_Id__r.Contract_Type__c in: contractTypes and 
                                            Status__c = 'Approved'
                                        group by Calendar_Month(Contract_Purchase_Approve_Date__c), Calendar_Year(Contract_Purchase_Approve_Date__c)            
                                        order by Calendar_Year(Contract_Purchase_Approve_Date__c) desc, Calendar_Month(Contract_Purchase_Approve_Date__c) asc])
                                                                
            {
                integer month = integer.valueOf(item.get('Month'));
                integer year = integer.valueOf(item.get('Year'));
                String key = item.get('Month') + '/' + item.get('Year');
                decimal amt = 0;
                if (strType == 'Spend')
                    amt = item.get('TotalSpend') == null ? 0 : System.Decimal.valueOf(String.valueOf(item.get('TotalSpend')));
                else
                    amt = item.get('TotalSaving') == null ? 0 : System.Decimal.valueOf(String.valueOf(item.get('TotalSaving')));
                if (dataMap.containsKey(key))
                {
                    PREP_ChartData obj = dataMap.get(key);
                    obj.actualAmt = (obj.actualAmt == null ? 0 : obj.actualAmt) + amt;
                    dataMap.put(key, obj);
                }
                else
                {
                    PREP_ChartData obj = new PREP_ChartData(key, month, year);
                    obj.actualAmt = amt;
                    dataMap.put(key, obj);
                }
            }
        }
        
        
        //sort the data by month/year
        List<PREP_ChartData> dataList = new List<PREP_ChartData>();
        dataList = dataMap.values();
        dataList.sort();              
        //find missing month
        List<PREP_ChartData> missing = new List<PREP_ChartData>();
		Date d1 = date.newInstance(dataList[0].year, dataList[0].month, 1);
    	
        for(PREP_ChartData item : dataList)
        {
            Date d2 = date.newInstance(item.year, item.month, 1);
            while (d1 < d2) {
	            PREP_ChartData expect = new PREP_ChartData(d1.month() + '/' + d1.year(), d1.month(), d1.year());
	            missing.add(expect);
	            d1 = d1.addMonths(1);
	        }
	        d1 = d1.addMonths(1);
        }
        System.debug(dataList);
        System.debug('missing list');
        System.debug(missing);
        List<PREP_ChartData> result = new List<PREP_ChartData>();
        result.addAll(dataList);
        
        result.addAll(missing);
        result.sort(); 
        decimal sumPlannedAmt = 0;
        decimal sumForecastAmt = 0;
        decimal sumActualAmt = 0;
     
        for(PREP_ChartData item : result)
        {
            sumPlannedAmt += item.plannedAmt == null ? 0 : item.plannedAmt;
            item.plannedAmt = sumPlannedAmt;
            
            if ((item.month <= reportMonth && item.year == reportYear) ||
                    item.year < reportYear)
            {   
                sumActualAmt += item.actualAmt == null ? 0 : item.actualAmt;
                item.actualAmt = sumActualAmt;
            }
            if (item.month == reportMonth && item.year == reportYear)
            {
            	sumForecastAmt += item.forecastAmt == null ? 0 : item.forecastAmt;
            	item.forecastAmt = item.actualAmt;
            }
            else if ((item.month > reportMonth && item.year == reportYear) || 
                    item.year > reportYear)
            {
                sumForecastAmt += item.forecastAmt == null ? 0 : item.forecastAmt;
                item.forecastAmt = sumForecastAmt;
            }
        }
        
        System.debug('final list =' + result);
        return result;                   
    }
    @RemoteAction 
    public static List<PREP_ChartData> getAwardChartData(String initId, String selectedYear, String runReportDate)
    {
          integer reportMonth = Integer.valueOf(runReportDate.split('/')[0]);
         integer reportYear = Integer.valueOf(runReportDate.split('/')[1]);
         //key = Month/Year, rollup planned/forecast/actual amt by Month/Year
         Map<String, PREP_ChartData> dataMap = new Map<String, PREP_ChartData>();
         String defaultKey = runReportDate;
         dataMap.put(defaultKey, new PREP_ChartData(defaultKey, reportMonth, reportYear));
         

         Map<Id, PREP_Baseline__c> baselineMap = new Map<Id, PREP_Baseline__c>();
         if (selectedYear == 'All')
            baselineMap = new Map<Id, PREP_Baseline__c>([select Id from PREP_Baseline__c where Initiative_Id__c =: initId ]);
         else
         {
            baselineMap = new Map<Id, PREP_Baseline__c>([select Id from PREP_Baseline__c 
                                                        where Calendar_Year(XCAC2_CAC_Award_Approval__c) =: integer.valueOf(selectedYear) and
                                                            Initiative_Id__c =: initId ]);
         }  
         if (baselineMap.size() > 0)
         { 
            PREP_Baseline__c baseline = baselineMap.values()[0];
            for(PREP_Mat_Serv_Group_Planned_Schedule__c sche : [select Id, RecordType.DeveloperName,
                                                                    (select Id, RecordType.DeveloperName, Planned_Decision_Summary_Approved__c, Actual_Decision_Summary_Approved__c,
                                                                            Mat_Serv_Group_Planned_Schedule_Id__r.Mat_Serv_Group_Baseline_Allocation_Id__r.Planned_Savings_Allocation_Dollar__c,
                                                                            Forecast_Decision_Summary_Approved_Mat__c, Forecast_Decision_Summary_Approved_Serv__c 
                                                                    FROM Mat_Serv_Group_Updates__r WHERE Completed__c = true ORDER BY LastModifiedDate DESC LIMIT 1) 
                                                                 from PREP_Mat_Serv_Group_Planned_Schedule__c
                                                                 where Mat_Serv_Group_Baseline_Allocation_Id__r.Baseline_Id__c in: baselineMap.values()])
            {
                System.debug(sche);
                if (sche.Mat_Serv_Group_Updates__r.size() > 0)
                {
                    PREP_Mat_Serv_Group_Update__c item = sche.Mat_Serv_Group_Updates__r[0];
                    
                     //calcuate planned amt
                    if (item.Planned_Decision_Summary_Approved__c != null)
                    {
                        integer month = item.Planned_Decision_Summary_Approved__c.month();
                        integer year = item.Planned_Decision_Summary_Approved__c.year();
                        String key = month + '/' + year;
                        
                        if (dataMap.containsKey(key))
                        {
                            PREP_ChartData obj = dataMap.get(key);
                            obj.plannedAmt = (obj.plannedAmt == null ? 0 : obj.plannedAmt) + 1;
                            System.debug(obj);
                        }
                        else
                            dataMap.put(key, new PREP_ChartData(key, month, year, 1));
                    }
                    //calculate forecast amt after the run report date
                    if (item.Forecast_Decision_Summary_Approved_Mat__c != null || item.Forecast_Decision_Summary_Approved_Serv__c != null)
                    {
                        String key;
                        integer month, year;
                        if (item.RecordType.DeveloperName.contains('Mat_Group_Update') && 
                            item.Forecast_Decision_Summary_Approved_Mat__c != null &&
                            ((item.Forecast_Decision_Summary_Approved_Mat__c.month() >= reportMonth && item.Forecast_Decision_Summary_Approved_Mat__c.year() == reportYear) ||
                            item.Forecast_Decision_Summary_Approved_Mat__c.year() > reportYear))
                        {
                            key = item.Forecast_Decision_Summary_Approved_Mat__c.month() + '/' + item.Forecast_Decision_Summary_Approved_Mat__c.year();
                            month = item.Forecast_Decision_Summary_Approved_Mat__c.month();
                            year = item.Forecast_Decision_Summary_Approved_Mat__c.year();
                        }
                        else if (item.RecordType.DeveloperName.contains('Serv_Group_Update') && 
                                item.Forecast_Decision_Summary_Approved_Serv__c != null &&
                                ((item.Forecast_Decision_Summary_Approved_Serv__c.month() >= reportMonth && item.Forecast_Decision_Summary_Approved_Serv__c.year() == reportYear) ||
                                item.Forecast_Decision_Summary_Approved_Serv__c.year() > reportYear))
                        {
                            key = item.Forecast_Decision_Summary_Approved_Serv__c.month() + '/' + item.Forecast_Decision_Summary_Approved_Serv__c.year();
                            month = item.Forecast_Decision_Summary_Approved_Serv__c.month();
                            year = item.Forecast_Decision_Summary_Approved_Serv__c.year();
                        }
                        if (key != null)
                        {
                           
                            if (dataMap.containsKey(key))
                            {
                               
                                PREP_ChartData obj = dataMap.get(key);
                                obj.forecastAmt = (obj.forecastAmt == null ? 0 : obj.forecastAmt) + 1;
                               
                            }
                            else
                            {
                                
                                PREP_ChartData obj = new PREP_ChartData(key, month, year);
                                obj.forecastAmt = 1;
                                dataMap.put(key, obj);
                                
                            }
                            
                        }
                    }
                    System.debug('calculate actual');
                     //calcuate actual amt before the run report date
                    if (item.Actual_Decision_Summary_Approved__c != null &&
                        ((item.Actual_Decision_Summary_Approved__c.month() <= reportMonth && item.Actual_Decision_Summary_Approved__c.year() == reportYear) 
                            || item.Actual_Decision_Summary_Approved__c.year() < reportYear))
                    {
                        integer month = item.Actual_Decision_Summary_Approved__c.month();
                        integer year = item.Actual_Decision_Summary_Approved__c.year();
                        String key = month + '/' + year;
                        System.debug(' key =' + key);
                        if (dataMap.containsKey(key))
                        {
                            PREP_ChartData obj = dataMap.get(key);
                            obj.actualAmt = (obj.actualAmt == null ? 0 : obj.actualAmt) + 1;
                            system.debug('1 actualAmt =' + obj.actualAmt);
                        }
                        else
                        {   
                        	PREP_ChartData obj =  new PREP_ChartData(key, month, year);
                        	obj.actualAmt = 1;
                        	dataMap.put(key, obj);
                        	 system.debug('2 actualAmt =' + 1);
                        }
                    }
                }
            }
        }
        
        
        //sort the data by month/year
        List<PREP_ChartData> dataList = new List<PREP_ChartData>();
        dataList = dataMap.values();
        dataList.sort();              
        //find missing month
        List<PREP_ChartData> missing = new List<PREP_ChartData>();
		Date d1 = date.newInstance(dataList[0].year, dataList[0].month, 1);
    	
        for(PREP_ChartData item : dataList)
        {
            Date d2 = date.newInstance(item.year, item.month, 1);
            while (d1 < d2) {
	            PREP_ChartData expect = new PREP_ChartData(d1.month() + '/' + d1.year(), d1.month(), d1.year());
	            missing.add(expect);
	            d1 = d1.addMonths(1);
	        }
	        d1 = d1.addMonths(1);
        }
       
        List<PREP_ChartData> result = new List<PREP_ChartData>();
        result.addAll(dataList);
        result.addAll(missing);
        result.sort(); 
        decimal sumPlannedAmt = 0;
        decimal sumForecastAmt = 0;
        decimal sumActualAmt = 0;
     
        for(PREP_ChartData item : result)
        {
            sumPlannedAmt += item.plannedAmt == null ? 0 : item.plannedAmt;
            item.plannedAmt = sumPlannedAmt;
            
            if ((item.month <= reportMonth && item.year == reportYear) ||
                    item.year < reportYear)
            {   
                sumActualAmt += item.actualAmt == null ? 0 : item.actualAmt;
                item.actualAmt = sumActualAmt;
            }
            if (item.month == reportMonth && item.year == reportYear)
            {
            	sumForecastAmt += item.forecastAmt == null ? 0 : item.forecastAmt;
            	item.forecastAmt = item.actualAmt;
            }
            else if ((item.month > reportMonth && item.year == reportYear) || 
                    item.year > reportYear)
            {
                sumForecastAmt += item.forecastAmt == null ? 0 : item.forecastAmt;
                item.forecastAmt = sumForecastAmt;
            }
        }
        System.debug('----------' + result);
        return result;                   
    }
}