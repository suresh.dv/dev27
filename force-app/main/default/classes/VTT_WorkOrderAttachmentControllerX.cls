/**
 * VTT_WorkOrderAttachmentControllerX
 *
 * Used for displaying all attachments for WOAs displayed on parent WO
 * 
 * Test: VTT_WorkOrderAttachmentControllerXTest
 *
 * Created: 2018/05/03
 * Author: MBrimus
 * 			20.02.2019 mb - W-001367 - check for user context
 */
public without sharing class VTT_WorkOrderAttachmentControllerX {

	public static String NO_ATTACHMENTS_AND_NOTE_MESSAGE = 'There are no attachments on any of the Work Order or Activities';
	public static String NOTE_TYPE = 'Note';
	public static String ATTACHMENT_TYPE = 'Attachment';
	public static String FILE_TYPE = 'File';

	public HOG_Maintenance_Servicing_Form__c record;
	public String clickedId { get;set; }
	public String clickedType { public get; set; }

	// Used for buttons on vf page since we cannot add 18 digit id to link on New Note or New Attachment
	public String recordId {get;set;}
	
	public VTT_WorkOrderAttachmentControllerX(ApexPages.StandardController stdController) {
		record = (HOG_Maintenance_Servicing_Form__c) stdController.getRecord();
		record = [SELECT Id,
					(SELECT Id FROM Work_Order_Activities__r)
					FROM HOG_Maintenance_Servicing_Form__c 
					WHERE Id =: record.Id];

		recordId = String.valueOf(record.Id).substring(0, 15);
	}

	// W-001367 - check users context and then render buttons accordingly
	public Boolean getIsLightningUser(){
		return HOG_GeneralUtilities.isUserUsingLightning();
	}

	public List<AttachmentWrapper> getAttachments() {
		// Based on business requirements we are also adding Work Order notes and attachments
		Set<Id> recordIds = new Set<Id>{ record.Id };
		
		for(Work_Order_Activity__c woa: record.Work_Order_Activities__r){
			recordIds.add(woa.Id);
		}

		List<Attachment> attachments = [SELECT Id, Name, ParentId, Parent.Name, SystemModstamp, LastModifiedDate, Owner.Name
										FROM Attachment 
										WHERE ParentId IN :recordIds];

		List<Note> notes = [SELECT Id, Title, ParentId, Parent.Name, SystemModstamp, LastModifiedDate, Owner.Name
							FROM Note
							WHERE ParentId IN :recordIds];

		// Get All files
		List<ContentDocumentLink> allFiles = getAllFilesForWorkOrder(recordIds);

		if (attachments.size() <= 0 && notes.size() <= 0 && allFiles.size() <= 0) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, NO_ATTACHMENTS_AND_NOTE_MESSAGE));
		}

		return getAttachmentWrapper(attachments, notes, allFiles);
	}

	public List<ContentDocumentLink> getAllFilesForWorkOrder(Set<Id> recordIds) {

		List<ContentDocumentLink> files  = [
					SELECT ContentDocument.Title,
							ContentDocument.Id,
							ContentDocument.LastModifiedDate,
							ContentDocument.Owner.Name,
							LinkedEntityId,
							LinkedEntity.Name
					FROM ContentDocumentLink
					WHERE LinkedEntityId IN:recordIds
			];
		return files;
	}

	public List<AttachmentWrapper> getAttachmentWrapper(List<Attachment> attachments, List<Note> notes, List<ContentDocumentLink> files) {
		List<AttachmentWrapper> attachmentWrapperList = new List<AttachmentWrapper>();

		for (Attachment at : attachments) {
			attachmentWrapperList.add(new AttachmentWrapper(at, null, null, ATTACHMENT_TYPE));
		}

		for (Note note : notes) {
			attachmentWrapperList.add(new AttachmentWrapper(null, note, null, NOTE_TYPE));
		}

		for (ContentDocumentLink file : files) {
			attachmentWrapperList.add(new AttachmentWrapper(null, null, file, FILE_TYPE));
		}

		return attachmentWrapperList;
	}

	public void deleteAttachment() {
		try {
			if (clickedType == NOTE_TYPE) {
				delete new Note(Id = clickedId);
			} else if (clickedType == FILE_TYPE) {
				delete new ContentDocument(Id = clickedId);
			} else {
				delete new Attachment(Id = clickedId);
			}
		} catch (Exception e) {
			System.debug('Exception ' + e.getMessage());
		}
	}

	public class AttachmentWrapper {
		public Attachment attachment {get;set;}
		public Note note {get;set;}
		public ContentDocumentLink file { get; set; }
		public String sourceType {get;set;}

		public AttachmentWrapper(Attachment attachment, Note note, ContentDocumentLink file, String sourceType) {
			this.attachment = attachment;
			this.note = note;
			this.sourceType = sourceType;
			this.file = file;
		}

		public String getOutputLink() {
			if (attachment != null) {
				return attachment.Id;
			}
			if (note != null) {
				return note.Id;
			}
			if (file != null) {
				return file.ContentDocument.Id;
			}
			return '';
		}

		public String getOutputLinkLabel() {
			if (attachment != null) {
				return attachment.Name;
			}
			if (note != null) {
				return note.Title;
			}
			if (file != null) {
				return file.ContentDocument.Title;
			}
			return '';
		}

		public Datetime getLastMofiedDate() {
			if (attachment != null) {
				return attachment.LastModifiedDate;
			}
			if (note != null) {
				return note.LastModifiedDate;
			}
			if (file != null) {
				return file.ContentDocument.LastModifiedDate;
			}
			return null;
		}

		public String getOwnerName() {
			if (attachment != null) {
				return attachment.Owner.Name;
			}
			if (note != null) {
				return note.Owner.Name;
			}
			if (file != null) {
				return file.ContentDocument.Owner.Name;
			}
			return '';
		}
	}
}