/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Utility class for E-Log application
Test class:		ELG_UtilitiesTest
History:        mbrimus 01.07.2019 - Created.
**************************************************************************************************/


public with sharing class ELG_Utilities {
	private static final String CLASS_NAME = String.valueOf(ELG_Utilities.class);

	public static Boolean executeTriggerCode = true;
	public static HOG_Trigger_Control__c executeTrigger = HOG_Trigger_Control__c.getInstance();

	/**
	 * Before insert Handler for Shift Handovers
	 *
	 * @param handovers
	 */
	public static void shiftHandoverBeforeInsert(List<ELG_Shift_Handover__c> handovers) {
		if (executeTriggerCode) {
			takeSnapShotOfActiveQuestionUUIDs(handovers);
		}
	}

	/**
	* Before update Handler for Task
	*
	* @param task
	*/
	public static void taskBeforeUpdate(List<ELG_Task__c> task) {
		if (executeTrigger.ELG_Task_Trigger__c) {
			insertLogEntryAndUpdateTaskLookup(task);
		}
	}

	/**
	 * This will create a snapshot of all active questions and creates a key for later use
	 * Key is a ; delimited list of UUIDs of each question
	 *
	 * @param handovers
	 */
	public static void takeSnapShotOfActiveQuestionUUIDs(List<ELG_Shift_Handover__c> handovers) {
		List<ELG_Handover_Category_Question__c> activeQuestions =
				new ELG_ShiftHandoverService().getAllActiveQuestions();
		System.debug(CLASS_NAME + ' active questions ' + activeQuestions);
		String activeQuestionsString = '';

		for (ELG_Handover_Category_Question__c question : activeQuestions) {
			activeQuestionsString = activeQuestionsString + question.Question_UUID__c + ';';
			System.debug(CLASS_NAME + ' active activeQuestionsString ' + activeQuestionsString);
		}

		for (ELG_Shift_Handover__c handover : handovers) {
			handover.Question_UUIDs__c = activeQuestionsString;
		}
	}

	public static void insertLogEntryAndUpdateTaskLookup(List<ELG_Task__c> taskList) {
		Id recordType = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();

		List<ELG_Log_Entry__c> logEntries = new List<ELG_Log_Entry__c>();

		for (ELG_Task__c task : taskList) {
			ELG_Log_Entry__c logEntry = new ELG_Log_Entry__c();
			logEntry.Shift_Assignement__c = task.Shift_Assignement__c;
			logEntry.Post__c = task.Post__c;
			logEntry.Operating_Field_AMU__c = task.Operating_Field_AMU__c;
			logEntry.User__c = task.Finished_By__c;
			logEntry.Correctable__c = false;
			logEntry.RecordTypeId = recordType;

			if (task.Status__c == 'Completed') {
				logEntry.User_Log_Entry__c = '<a href="/'
						+ task.Id
						+ '" target="_blank">'
						+ task.Name
						+ ' - '
						+ task.Post_Name__c
						+ '</a>'
						+ ' has been Completed.'
						+ '\n\nShort Description:\n'
						+ task.Short_Description__c
						+ '\n\nTask Description:\n'
						+ task.Task_Description__c;
				if (task.Comment__c != null) {
					logEntry.User_Log_Entry__c += '\n\nTask Comment: ' + task.Comment__c;
				}
			} else if (task.Status__c == 'Cancelled') {
				logEntry.User_Log_Entry__c = '<a href="/'
						+ task.Id
						+ '" target="_blank">'
						+ task.Name
						+ ' - '
						+ task.Post_Name__c
						+ '</a>'
						+ ' has been Cancelled.'
						+ '\n\nShort Description:\n'
						+ task.Short_Description__c
						+ '\n\nTask Description:\n'
						+ task.Task_Description__c
						+ '\n\nReason for Cancellation is: '
						+ task.Reason__c;
			}

			logEntries.add(logEntry);
		}
		insert logEntries;

		for (Integer i = 0; i < taskList.size(); i++) {
			taskList[i].Log_Entry__c = logEntries[i].Id;
		}

	}

	/**
	 * Flag to see if user can become shift engineer
	 *
	 * @return true if user can edit this field
	 */
	public static Boolean hasEditAccessForReviewShiftEngineer() {
		return Schema.sObjectType.ELG_Shift_Handover__c.fields.Shift_Engineer__c.isUpdateable();
	}

	/**
	 * Flag to see if user can do reviews
	 *
	 * @return true if user can edit these fields
	 */
	public static Boolean hasEditAccessForReview() {
		return Schema.sObjectType.ELG_Shift_Handover__c.fields.Sr_Supervisor__c.isUpdateable() &&
				Schema.sObjectType.ELG_Shift_Handover__c.fields.Steam_Chief__c.isUpdateable() &&
				Schema.sObjectType.ELG_Shift_Handover__c.fields.Shift_Lead__c.isUpdateable();
	}

	/**
	* Flag to see if user can do fill Incoming Operator on Handover object
	*
	* @return true if user can edit this field
	*/
	public static Boolean hasEditAccessToSignOffAndStartNewShift() {
		return Schema.sObjectType.ELG_Shift_Handover__c.fields.Signed_Off__c.isUpdateable();
	}

	/**
   * Flag to see if user can do edit Sign In field, that is necessary to Accept Passed Handover
   *
   * @return true if user can edit this field
   */
	public static Boolean hasEditAccessToAcceptHandover() {
		return Schema.sObjectType.ELG_Shift_Handover__c.fields.Signed_In__c.isUpdateable();
	}

	/**
	* Before insert Handler for Post
	*
	* @param posts
	*/
	public static void postBeforeInsert(List<ELG_Post__c> posts) {
		if (executeTrigger.ELG_Post_Trigger__c) {
			validatePost(posts);
		}
	}

	public static void validatePost(List<ELG_Post__c> posts) {
		Set<Id> amuIds = new Set<Id>();

		for (ELG_Post__c post : posts) {
			amuIds.add(post.Operating_Field_AMU__c);
		}

		List<ELG_Post__c> allPostsForAmusWithShiftLead = [
				SELECT
						Id,
						Operating_Field_AMU__c
				FROM ELG_Post__c
				WHERE Operating_Field_AMU__c IN :amuIds
				AND Shift_Lead_Post__c = TRUE
				LIMIT 10000
		];

		Set<Id> amusWithPostThatNeedShiftLead = new Set<Id>();
		for (ELG_Post__c post : allPostsForAmusWithShiftLead) {
			amusWithPostThatNeedShiftLead.add(post.Operating_Field_AMU__c);
		}

		for (ELG_Post__c post : posts) {
			if (post.Shift_Lead_Post__c && amusWithPostThatNeedShiftLead.contains(post.Operating_Field_AMU__c)) {
				post.addError('This AMU already has Shift Lead Post assigned');
			}
		}
	}

}