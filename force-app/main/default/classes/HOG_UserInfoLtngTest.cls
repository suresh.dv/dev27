/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for HOG_UserInfoLtngTest.
History:        jschn 2019-01-30 - Created.
*************************************************************************************************/
@IsTest
private class HOG_UserInfoLtngTest {

    @IsTest
    static void testAsSpecificUserWithRole() {
        User runningUser = createUser(true);
        UserRole runningUserRole = [SELECT Name FROM UserRole WHERE Id =: runningUser.UserRoleId];

        UserRole role;
        System.runAs(runningUser) {
            role = HOG_UserInfoLtng.getCurrentUserRole();
        }

        System.assert(role != null, 'User role should not be empty.');
        System.assertEquals(runningUserRole.Name, role.Name, 'Role name should be same after retrieving.');
    }

    @IsTest
    static void testAsSpecificUserWithoutRole() {
        User runningUser = createUser(false);

        UserRole role;
        System.runAs(runningUser) {
            role = HOG_UserInfoLtng.getCurrentUserRole();
        }

        System.assert(role == null, 'User role should be empty.');
    }

    @IsTest
    static void testAsAdmin() {
        UserRole role = HOG_UserInfoLtng.getCurrentUserRole();

        System.assert(role != null, 'User role should not be empty.');
        System.assert(String.isNotBlank(role.Name));
    }

    private static User createUser(Boolean withRole) {
        User usr = HOG_TestDataFactory.createUser('First', 'second', 'nck');
        if(withRole) {
            usr = HOG_TestDataFactory.assignRole(usr, 'Heavy Oil & Gas');
        }
        return usr;
    }

}