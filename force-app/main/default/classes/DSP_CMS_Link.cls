global virtual with sharing class DSP_CMS_Link extends DSP_CMS_LinkController
{
    global DSP_CMS_Link(cms.GenerateContent cc)
    {
        super(cc);
    }
    
    global DSP_CMS_Link()
    {
        super();
    }
    
    global override String getHTML()
    {
        return linkHTML();
    }
}