/**********************************************************************************
  CreatedBy : Accenture
  Organization:Accenture
  Purpose   :  Handler class For  Retail_Location__c object functions
  Test Class:  RaM_RetailLocationHandlerTest
  Version:1.0.0.1
**********************************************************************************/
public without Sharing Class RaM_RetailLocationHandler{ 
/**************************************
CreatedBy:      Accenture
Method name:    insertRetailLocationRecord
Param:          List<Retail_Location__c>, Map<id,Retail_Location__c>, boolean
Return:         void
Purpose:        Retail Location all fields return     
*************************************/
public static void insertRetailLocationRecord(List<Retail_Location__c> retailLocList,Map<id,Retail_Location__c> oldMap,boolean isInsert){
     list<RaM_Location__c> locListInsert=new list<RaM_Location__c>();
     list<RaM_Location__c> locListUpdate=new list<RaM_Location__c>();
     Schema.SObjectField externalIdIns = RaM_Location__c.Fields.LocationExternalId__c;
    if(isInsert){
        locListInsert=returnRaMLocationRecord(retailLocList,true);
    }
    if(!isInsert){
        locListUpdate=returnRaMLocationRecord(retailLocList,false);
    }
    system.debug('------locListInsert-----1-------'+locListInsert);
    system.debug('------locListUpdate-----1-------'+locListUpdate);
    try{
        if(!locListInsert.IsEmpty())
            insert locListInsert ; // Insert new record;

        if(!locListUpdate.IsEmpty())
            upsert locListUpdate LocationExternalId__c;
            
            system.debug('------locListInsert-----2-------'+locListInsert); 
            system.debug('------locListUpdate-----2-------'+locListUpdate); 

    }catch(DmlException qExcp){
            System.debug('An exception occurred: ' + qExcp.getMessage());
            RaM_UtilityHolder.errormethod(RaM_ConstantUtility.Retail_Location_OBJECT,RaM_ConstantUtility.RETAIL_LOCATION_TRIGGER_HANDLER_CLASSNAME ,RaM_ConstantUtility.RETAIL_LOCATION_TRIGGER_HANDLER_METHODNAME , RaM_ConstantUtility.FATAL,qExcp.getMessage());
      
    }   
        
}
/**************************************
CreatedBy:      Accenture
Method name:    getAllRetailLocationFields
Param:          null
Return:         string
Purpose:        Retail Location all fields return     
*************************************/
    public static string getAllRetailLocationFields(){
        Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.Retail_Location__c.fields.getMap();
        List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();

        String theQuery = 'SELECT ';
        for(Schema.SObjectField s : fldObjMapValues){
           String theName = s.getDescribe().getName();

           // Continue building your dynamic query string
           theQuery += theName + ',';
        }

        // Trim last comma
        theQuery = theQuery.subString(0, theQuery.length() - 1);

        // Finalize query string
        theQuery += ' FROM Retail_Location__c';

        system.debug('----theQuery-----'+ theQuery);
        return theQuery;
    }
    /**************************************
    CreatedBy:      Accenture
    Method name:    returnRaMLocationRecord
    Param:          List<Retail_Location__c> 
    Return:         list<RaM_Location__c >
    Purpose:        return list of RaM_Location__c to insert     
    *************************************/
    public static list<RaM_Location__c > returnRaMLocationRecord(List<Retail_Location__c> retailLocationList, boolean isInsert) {
      list<RaM_Location__c> locList=new list<RaM_Location__c>();
      for(Retail_Location__c retailLoc: retailLocationList){
      
        RaM_Location__c locIns=new RaM_Location__c();
        locIns.Name=retailLoc.Name;
        locIns.Is24Hours__c=retailLoc.X24_Hours__c;
        locIns.Active__c= (retailLoc.Active__c==RaM_ConstantUtility.YES_TEXT)?true:false; 
        locIns.ATMBanking__c=retailLoc.ATM_Banking__c;
        locIns.Brand_Code__c=retailLoc.Brand_Code__c;
        locIns.Card_Lock_Type__c=retailLoc.Cardlock_Type__c;
        locIns.IsCarwash__c=retailLoc.Carwash__c;
        locIns.Carwash_Type_1__c=retailLoc.Carwash_Type__c;
        locIns.Carwash_Type_2__c=retailLoc.Carwash_Type__c;
        locIns.City__c=retailLoc.City__c;
        locIns.Closing_Time__c=retailLoc.Closes__c;  
        locIns.CMSL_Number__c=retailLoc.CMSL__c;
        
        locIns.IsDiesel__c=retailLoc.Diesel__c;
        locIns.District__c=retailLoc.District__c;
        locIns.District_Manager__c=retailLoc.District_Manager__c;
        //locIns.Email__c=retailLoc.Location_Email__c;
        locIns.IsEthanol__c=retailLoc.Ethanol__c;
        locIns.Fax__c=retailLoc.Fax__c;
        locIns.IsFoodStore__c=retailLoc.Food_Store__c;
        locIns.IsGasoline__c=retailLoc.Gasoline__c;
        locIns.IsLaundry__c=retailLoc.Laundry__c;
       
        locIns.LocationName__c=retailLoc.Location_Name__c;
        locIns.Location_Type__c=retailLoc.Location_Type__c;
        locIns.IsLottery__c=retailLoc.Lottery__c;
        locIns.IsNGV__c=retailLoc.NGV__c;
        locIns.No_of_Bays__c=retailLoc.No_Of_Bays__c;
        locIns.Opening_Time__c=retailLoc.Opens__c;
        locIns.Phone__c=retailLoc.Phone__c;
        
        locIns.Physical_Address_1__c=retailLoc.Location_Name__c; // Remedy R&M uses Location Name as "Physical Address 1"
        locIns.Physical_Address_2__c=retailLoc.Address__c;
        locIns.Postal_Code__c=retailLoc.Postal_Code__c;
        locIns.IsPropane__c=retailLoc.Propane__c;
        locIns.Province__c=retailLoc.Province__c;
        locIns.IsRestaurant__c=retailLoc.Restaurant__c;
        locIns.IsSaniDump__c=retailLoc.SaniDump__c;
        locIns.IsServiceStation__c=retailLoc.Service_Station__c;
        locIns.IsShowers__c=retailLoc.Showers__c;
        locIns.IsTireRepair__c=retailLoc.Tire_Repair__c;
        locIns.Trade_Class__c=retailLoc.Trade_Class__c;
        locIns.IsTravelStop__c=retailLoc.Travel_Stop__c;
        locIns.IsTruckerLounge__c=retailLoc.Trucker_Lounge__c;
        locIns.IsTruckWash__c=retailLoc.Truckwash__c;
        locIns.IsUsedOilDepot__c=retailLoc.Used_Oil_Depot__c;
        locIns.IsWeighScale__c=retailLoc.Weigh_Scale__c;
        locIns.LocationExternalId__c=retailLoc.id; 
        if(isInsert){  
            locIns.Macs__c=false;
            //locIns.Manager__c=RaM_ConstantUtility.BLANK;
            //locIns.Physical_Address_2__c=RaM_ConstantUtility.BLANK;
            locIns.SAPFuncLOC__c=null;
            locIns.Service_Area__c=null;
            //locIns.Login_ID__c= RaM_ConstantUtility.BLANK;
            locIns.Location_Classification__c=null;
            locIns.Contact__c=null;
        }
        if((retailLoc.Name).length()<=4)// As per MARK, if location# length size ig greater then 4 character then dont insert that record
        locList.add(locIns);
        system.debug('--------locList-----------'+locList);
      }
        return locList;
    }
}