/*-----------------------------------------------------------------------------------------------
Author     : Miro Zelina
Company    : Husky Energy
Description: A utility class creating test data for the HOG_SAPEquipmentServiceTest class
History    : 20-Mar-2017 - Created
-------------------------------------------------------------------------------------------------*/
@isTest 


public class HOG_SAPEquipmentServiceTestData
{
    
    /*****************************     DT_SAP_RIHEQUI_LIST     ********************************/
    public static HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST createDT_SAP_RIHEQUI_LIST
    (
        
    	String EQKTX,
    	String EQUNR,
    	String TYPBZ,
    	String TPLNR,
    	String TPLNR_INT,
    	String HEQUI
    	
    )
    {
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST equipment = new HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST();
        
        equipment.EQKTX = EQKTX;
        equipment.EQUNR = EQUNR;
        equipment.TYPBZ = TYPBZ;
        equipment.TPLNR = TPLNR;
        equipment.TPLNR_INT = TPLNR_INT;
        equipment.HEQUI = HEQUI;
        
        equipment.SPRAS = 'X';
        equipment.EQTYP = 'X';
        equipment.IWERK = 'X';
        equipment.SWERK = 'X';
        equipment.STORT = 'X';
        equipment.MSGRP = 'X';
        equipment.BEBER = 'X';
        equipment.ABCKZ = 'X';
        equipment.EQFNR = 'X';
        equipment.BUKRS = 'X';
        equipment.ANLNR = 'X';
        equipment.ANLUN = 'X';
        equipment.GSBER = 'X';
        equipment.KOSTL = '0080040082';
        equipment.DAUFN = 'X';
        equipment.AUFNR = 'X';
        equipment.TIDNR = 'X';
        equipment.HEQNR = 'X';
        equipment.SUBMT = 'X';
        equipment.SERNR = 'X';
        equipment.MAPAR = 'X';
        equipment.INGRP = 'X';
        equipment.ELIEF = 'X';
        equipment.ANSWT = 'X';
        equipment.KRFKZ = 'X';
        equipment.WAERS = 'X';
        equipment.INVNR = 'X';
        equipment.GROES = 'X';
        equipment.BRGEW = 'X';
        equipment.BAUJJ = 'X';
        equipment.BAUMM = 'X';
        equipment.HERST = 'X';
        equipment.HERLD = 'X';
        equipment.HZEIN = 'X';
        equipment.SERGE = 'X';
        equipment.KUND1 = 'X';
        equipment.KUND2 = 'X';
        equipment.KUND3 = 'X';
        equipment.LIZNR = 'X';
        equipment.ARBPL = 'X';
        equipment.GEWRK = 'X';
        equipment.ERDAT = 'X';
        equipment.ERNAM = 'X';
        equipment.AEDAZ = 'X';
        equipment.AENAZ = 'X';
        equipment.DATAB = 'X';
        equipment.BEGRU = 'X';
        equipment.KMATN = 'X';
        equipment.MATNR = 'X';
        equipment.WERK = 'X';
        equipment.LAGER = 'X';
        equipment.CHARGE = 'X';
        equipment.KUNDE = 'X';
        equipment.DATBI = 'X';
        equipment.STTXT = 'X';
        equipment.ILOAN = 'X';
        equipment.USTXT = 'X';
        equipment.OBJNR = 'X';
        equipment.RBNR = 'X';
        equipment.PROID = 'X';
        equipment.EQART = 'X';
        equipment.ANSDT = 'X';
        equipment.VKORG = 'X';
        equipment.VTWEG = 'X';
        equipment.SPART = 'X';
        equipment.ADRNR = 'X';
        equipment.MANDT = 'X';
        equipment.NAME_LIST = 'X';
        equipment.TEL_NUMBER = 'X';
        equipment.POST_CODE1 = 'X';
        equipment.CITY1 = 'X';
        equipment.CITY2 = 'X';
        equipment.COUNTRY = 'X';
        equipment.REGION = 'X';
        equipment.STREET = 'X';
        equipment.RKEOBJNR = 'X';
        equipment.CUOBJ = 'X';
        equipment.GEWEI = 'X';
        equipment.KOKRS = 'X';
        equipment.GWLDT_K = 'X';
        equipment.GWLEN_K = 'X';
        equipment.MGANR_K = 'X';
        equipment.GARTX_K = 'X';
        equipment.GWLDT_L = 'X';
        equipment.GWLEN_L = 'X';
        equipment.MGANR_L = 'X';
        equipment.GARTX_L = 'X';
        equipment.AULDT = 'X';
        equipment.INBDT = 'X';
        equipment.EQLFN = 'X';
        equipment.SUBMTKTX = 'X';
        equipment.EQUZ_ERDAT = 'X';
        equipment.EQUZ_ERNAM = 'X';
        equipment.EQUZ_AEDAT = 'X';
        equipment.EQUZ_AENAM = 'X';
        equipment.EQUZN = 'X';
        equipment.TIMBI = 'X';
        equipment.LBBSA = 'X';
        equipment.SOBKZ = 'X';
        equipment.KUNNR = 'X';
        equipment.LIFNR = 'X';
        equipment.KDAUF = 'X';
        equipment.KDPOS = 'X';
        equipment.PS_PSP_PNR = 'X';
        equipment.B_CHARGE = 'X';
        equipment.MAKTX = 'X';
        equipment.PLTXT = 'X';
        equipment.KZLTX = 'X';
        equipment.S_EQUI = 'X';
        equipment.DFPS_CP = 'X';
        equipment.DFPS_CP_SYS = 'X';
        equipment.DFPS_CP_SYST = 'X';
        equipment.ZZ_MARKET_VALUE = 'X';
        equipment.ZZ_REPLACE_COST = 'X';
        equipment.ZZ_OWNING_FLOCN = 'X';
        equipment.ZZ_SASK_PST_IND = 'X';
        equipment.ZZ_BC_PST_IND = 'X';
        equipment.ZZ_MAN_PST_IND = 'X';
        equipment.ZZ_ON_PST_IND = 'X';
        equipment.ZZ_HIST_EQUIP = 'X';
        equipment.ZZ_CONDITION = 'X';
        equipment.ZZ_MKT_VAL_IND = 'X';
        equipment.ZZ_OWNING_KOSTL = 'X';
        equipment.ZZ_AB_REG = 'X';
        equipment.ZZ_BC_REG = 'X';
        equipment.ZZ_SK_REG = 'X';
        equipment.ZZ_MB_REG = 'X';
        equipment.ZZ_ON_REG = 'X';
        equipment.ZZ_NO_SCHED = 'X';
        equipment.ZZ_ABSA = 'X';
        equipment.ZZ_TRANSFER = 'X';
        equipment.ZZ_HAZARD = 'X';
        equipment.ZZ_MAINT = 'X';
        equipment.ZZ_EQUIP_AREA = 'X';
        equipment.ZZ_INGRESS = 'X';
        equipment.ZZ_PROTECT = 'X';
        equipment.ZZ_GAS_GROUP = 'X';
        equipment.ZZ_TEMP_CLASS = 'X';
        equipment.ZZ_NON_CODE = 'X';
        equipment.REC_TYPE = 'X';
        equipment.TPLNR_1 = 'X';
        equipment.TPLNR_2 = 'X';
        equipment.TPLNR_3 = 'X';
        equipment.FLTYP = 'X';
        equipment.ZZ_LSD = 'X';
        equipment.ZZ_RECTYP = 'X';
        equipment.ZZEQHEIR = 'X';
        equipment.RBNRX = 'X';
        equipment.TYPTX = 'X';
        equipment.SYSID_MANDT = 'X';
        equipment.ABCTX = 'X';
        equipment.EARTX = 'X';
        equipment.NAME1 = 'X';
        equipment.INNAM = 'X';
        equipment.KSCHL = 'X';
        equipment.TXT04 = 'X';
        equipment.POST1 = 'X';
    
    return equipment;
    
    }
    
    
    
    
    
    /*****************************     DT_SAP_ZIPM_CHAR_DATA     ********************************/
    public static HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA createDT_SAP_ZIPM_CHAR_DATA()
    {
        HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA result = new HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA();
        
        result.EQUNR = 'X';
  		//result.CLASS = 'X';
  		result.ATNAM = 'X';
  		result.ZVAL = 'X';
  		result.MSEHI = 'X';
  		result.MSEHL = 'X';
     
    return result;    
    }
    
    
    
    
    
    /*****************************       DT_SAP_EQUIP_ITEM       ********************************/
    public static HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM createDT_SAP_EQUIP_ITEM
    (
        
        HOG_SAPApiUtils.DT_SAP_RIHEQUI_LIST RIHEQUI_LIST,
        List<HOG_SAPApiUtils.DT_SAP_ZIPM_CHAR_DATA> EQUIP_CHAR

    )
    {
        HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM result = new HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM();
        
        result.RIHEQUI_LIST = RIHEQUI_LIST;
        result.EQUIP_CHAR = EQUIP_CHAR;

        return result;
    }
    
}