//wrapper class to store chart data
global class PREP_ChartData implements Comparable {
	public String xAxis { get; set; }
	public integer month {get;set;}
	public integer year {get;set;}
    public decimal plannedAmt { get; set; }
	public decimal forecastAmt {get;set;}
	public decimal actualAmt {get;set;}
		
	public PREP_ChartData(String key, integer m, integer y) {
       xAxis = key;
       month = m;
       year = y; 
    	
    }
    public PREP_ChartData(String key, integer m, integer y, decimal amt) {
       xAxis = key;
       month = m;
       year = y; 
     
       plannedAmt = amt;
      
    }
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        PREP_ChartData compareToObj = (PREP_ChartData)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        
        if (year > compareToObj.year) {
           returnValue = 1;
        } else if (year < compareToObj.year) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        else
        {
        	 // Set return value to a positive value. 
            if (month > compareToObj.month)
            	returnValue = 1;
            else if (month < compareToObj.month)
            	returnValue = -1;	
        }
        
        return returnValue;       
    }
}