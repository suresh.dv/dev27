/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentCorrectionFormEdit/EquipmentCorrectionFormView page
Test Class:    EquipmentCorrectionFormTest

History:       03-Jul-17    Miro Zelina - included System & Sub-System into Correction Form
               04.09.17    Marcel Brimus - added FEL and Yard
               03-Aug-2018 Miro Zelina - added check for not populated FLOC id (isEmptyFloc) [W-001180]
               16-Nov-18 	Maros Grajcar - Sprint - Equipment Transfer Release 1
							- added Rejected button to page
               				- added popup window on Rejected button
               				- added new field to the object
               				- added logic to get email from Planner Group
				08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude - Email Service Enhancement R
               	11.09.19 Maros Grajcar - W-001579
               							- added new methods
               							- update of Equipment Utilities
------------------------------------------------------------*/
public with sharing class EquipmentCorrectionFormControllerX {
	public Equipment__c equip { get; set; }
	private EquipmentFormService equipmentUpdate;
	public Boolean displayPopup { get; set; }
	public Equipment_Correction_Form__c form { get; set; }
	private String equipId;
	private ApexPages.StandardController controller;
	public String formId { get; set; }
	public Attachment photo { get; set; }
	public ContentDocumentLink photoFile { get; set; }
	public EquipmentUtilities.UserPermissions userPermissions;
	public Equipment_Correction_Form__c temporaryForm;


	public EquipmentCorrectionFormControllerX(ApexPages.StandardController std) {
		if (!Test.isRunningTest()) {
			//jschn 08-Jul-19 - added Functional_Location__c
			std.addFields(new List<String>{
					'Name', 'Status__c', 'Equipment_Status__c', 'Description_old__c', 'Description_new__c',
					'Manufacturer_old__c', 'Manufacturer_new__c', 'Model_Number_old__c', 'Model_Number_new__c',
					'Manufacturer_Serial_No_old__c', 'Manufacturer_Serial_No_new__c', 'Tag_Number_old__c', 'Tag_Number_new__c',
					'Equipment__c', 'Equipment__r.Location__c', 'Equipment__r.Well_Event__c', 'Equipment__r.System__c', 'Equipment__r.Sub_System__c',
					'Equipment__r.Functional_Equipment_Level__c', 'Equipment__r.Yard__c', 'Equipment__r.Functional_Location__c',
					'Equipment__r.Location__r.Route__c', 'Equipment__r.Planner_Group__c',
					'Equipment__r.Well_Event__r.Route__c', 'Equipment__r.System__r.Route__c', 'Equipment__r.Sub_System__r.Route__c',
					'Equipment__r.Functional_Equipment_Level__r.Route__c', 'Equipment__r.Yard__r.Route__c',
					'CreatedById', 'CreatedDate', 'LastModifiedDate', 'LastModifiedById', 'Comments__c',
					'Equipment__r.Facility__r.Plant_Section__c', 'Equipment__r.Facility__c', 'Expiration_Date__c',
					'Reason__c', 'Regulatory_Equipment__c', 'Safety_Critical_Equipment__c', 'Send_Email_Notification__c',
					'Location_Label__c'
			});
		}
		controller = std;
		form = (Equipment_Correction_Form__c) std.getRecord();
		photo = new Attachment();
		photoFile = new ContentDocumentLink();
		equipId = ApexPages.currentPage().getParameters().get('equipId');
		userPermissions = new EquipmentUtilities.UserPermissions();
		equipmentUpdate = new EquipmentFormService();
		temporaryForm = form.clone(false, false, false, false);

		if (form.Id == null) {
			HOG_Equipment_Request_Configuration__c settings = HOG_Equipment_Request_Configuration__c.getInstance();
			Date dT = Date.today();
			Date expiredDate = (Date.newInstance(dT.year(), dT.month(), dT.day())) + Integer.valueOf(settings.Equipment_Request_Expiration_Date_Days__c);
			form.Expiration_Date__c = expiredDate;
		}
		//User have to new a record from Equipment detail view
		if (form.Id == null && equipId != null) //creating a new form
			setDefault(); else {
			photo = getAttachmentPhoto();
			photoFile = getFilePhoto();
			System.debug('Photo file:' + photoFile);
		}

	}

	private Boolean checkNewForm() {
		return form.Equipment_Status__c != null
				|| form.Description_new__c != null
				|| form.Manufacturer_new__c != null
				|| form.Model_Number_new__c != null
				|| form.Manufacturer_Serial_No_new__c != null
				|| form.Tag_Number_new__c != null
				|| form.Comments__c != null
				|| form.Regulatory_Equipment__c != false
				|| form.Safety_Critical_Equipment__c != false;
	}

	private Boolean fieldChangedBeforeSave() {
		List<String> dataFormFields = EquipmentUtilities.equipmentDataUpdateFields;
		Boolean hasChanged = false;

		if (form.Id == null) {
			hasChanged = checkNewForm();
		} else {
			for (String fieldToCheck : dataFormFields) {
				hasChanged |= (form.get(fieldToCheck) != temporaryForm.get(fieldToCheck));
				if (hasChanged) break;
			}
		}

		return hasChanged;
	}

	private Attachment getAttachmentPhoto() {
		List<Attachment> att = [SELECT Id, Name, Body, ContentType FROM Attachment WHERE ParentId = :form.Id];
		if (att.size() > 0)
			return att[0];

		return new Attachment();
	}

	private ContentDocumentLink getFilePhoto() {

		if (form.Id != null) {
			List<ContentDocumentLink> file = [SELECT Id, ContentDocument.Title, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :form.Id];

			if (file.size() > 0) {
				return file[0];
			}

		}
		return new ContentDocumentLink();
	}
	private void setDefault() {
		//jschn 08-Jul-19 - added Functional_Location__c
		equip = [
				SELECT Id, Description_of_Equipment__c, Manufacturer__c, Model_Number__c,
						Manufacturer_Serial_No__c, Location__c, Location__r.Route__r.Route_Number__c,
						Well_Event__c, Well_Event__r.Route__r.Route_Number__c,
						System__c, System__r.Route__r.Route_Number__c, Functional_Location__c,
						Sub_System__c, Sub_System__r.Route__r.Route_Number__c,
						Functional_Equipment_Level__c, Functional_Equipment_Level__r.Route__r.Route_Number__c,
						Yard__c, Yard__r.Route__r.Route_Number__c,
						Planner_Group__c, Tag_Number__c, Facility__c, Facility__r.Plant_Section__c, Facility__r.Planner_Group__c,
						Location__r.Operating_Field_AMU__r.Name, Facility__r.Operating_Field_AMU__r.Name, Yard__r.Operating_Field_AMU__r.Name,
						Functional_Equipment_Level__r.Operating_Field_AMU__r.Name, System__r.Operating_Field_AMU__r.Name, Sub_System__r.Operating_Field_AMU__r.Name,
						Well_Event__r.Well_ID__r.Operating_Field_AMU__r.Name
				FROM Equipment__c
				WHERE Id = :equipId
		];
		form.Description_old__c = equip.Description_of_Equipment__c;
		form.Manufacturer_old__c = equip.Manufacturer__c;
		form.Model_Number_old__c = equip.Model_Number__c;
		form.Manufacturer_Serial_No_old__c = equip.Manufacturer_Serial_No__c;
		form.Tag_Number_old__c = equip.Tag_Number__c;
		form.Equipment__c = equipId;
		form.Equipment__r = equip;

		if (equip.Location__c != null) {
			form.AMU_Name__c = equip.Location__r.Operating_Field_AMU__r.Name;
		} else if (equip.Facility__c != null) {
			form.AMU_Name__c = equip.Facility__r.Operating_Field_AMU__r.Name;
		} else if (equip.Yard__c != null) {
			form.AMU_Name__c = equip.Yard__r.Operating_Field_AMU__r.Name;
		} else if (equip.Functional_Equipment_Level__c != null) {
			form.AMU_Name__c = equip.Functional_Equipment_Level__r.Operating_Field_AMU__r.Name;
		} else if (equip.System__c != null) {
			form.AMU_Name__c = equip.System__r.Operating_Field_AMU__r.Name;
		} else if (equip.Sub_System__c != null) {
			form.AMU_Name__c = equip.Sub_System__r.Operating_Field_AMU__r.Name;
		} else if (equip.Well_Event__c != null) {
			form.AMU_Name__c = equip.Well_Event__r.Well_ID__r.Operating_Field_AMU__r.Name;
		}

	}
	private void clearViewState() {
		photo.Body = null;
		photo = new Attachment();
		photoFile = new ContentDocumentLink();
	}
	public PageReference save() {
		if (fieldChangedBeforeSave()) {
			try {
				if (form.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED) {
					form.Status__c = EquipmentUtilities.REQUEST_STATUS_OPEN;
				}
				PageReference nextPage = controller.save();
				if (photo.Id == null && photo.Body != null) {
					//Create new instance of uploaded file
					Attachment a = photo.clone(false, true, false, false);
					a.ParentId = controller.getId();
					//clear body of uploaded file to remove from view state limit error
					clearViewState();
					insert a;
				}
				if (ApexPages.currentPage().getParameters().get('retURL') != null) {
					System.debug('retURL =' + ApexPages.currentPage().getParameters().get('retURL'));
					nextPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
					nextPage.setRedirect(true);
				}
				return nextPage;
			} catch (Exception e) {
				System.debug('save error');
				clearViewState();
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
				return null;
			}
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill at least one field before Save.'));
			return null;
		}
	}

	public void DeletePhoto() {
		try {

			if (photoFile.ContentDocumentId != null) {
				ContentDocument document = [SELECT Id FROM ContentDocument WHERE Id = :photoFile.ContentDocumentId];
				delete document;
			}

			if (photo.Id != null) {
				delete photo;
			}
		} catch (DmlException e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error deleting file'));

		} finally {
			clearViewState();
		}

		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'File deleted successfully'));

	}

	public PageReference CloseRequest() {

		System.Savepoint sp = Database.setSavepoint();

		try {
			if (!isUserAdmin) {

				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
				return null;
			} else {

				form.Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED;
				update form;
				EquipmentFormService.formProcessedTemplateEmail(
						form.Id,
						new Set<String>{
								form.Equipment__r.Planner_Group__c +
										EquipmentFormService.COMBINATION_DELIMITER +
										form.Equipment__r.Functional_Location__c
						}
				);
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully PROCESSED and notification email has been sent.'));
				return null;
			}
		} catch (Exception ex) {
			Database.rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			return null;
		}

	}

	public Boolean hasEquipment {
		get {
			if (form.Equipment__c != null) {
				return true;
			} else {
				return false;
			}
		}
		set;
	}

	public Boolean isUserAdmin {
		get {
			return userPermissions.isSystemAdmin
					|| userPermissions.isSapSupportLead
					|| userPermissions.isHogAdmin;
		}
		private set;
	}

	/**
	* functions for popup window
	*/
	public void showPopup() {
		displayPopup = true;
		//closePopupReason = form.Reason__c;
	}
	public void closePopup() {
		displayPopup = false;
		//form.Reason__c = closePopupReason;
	}

	public Boolean isEditable {
		get {
			return form.Status__c != EquipmentUtilities.REQUEST_STATUS_PROCESSED
					&& form.Status__c != EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED
					&& form.Status__c != EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED;
		}
		private set;
	}

	public Boolean isClosed {
		get {
			return form.Status__c == EquipmentUtilities.REQUEST_STATUS_PROCESSED
					|| form.Status__c == EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED
					|| form.Status__c == EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED;
		}
	}

	public void saveOnPopup() {
		System.Savepoint sp = Database.setSavepoint();
		String originalStatus = form.Status__c;
		form.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;

		try {
			update form;
			//08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude
			EquipmentFormService.requestRejectionTemplateEmail(
					form.Id,
					form.CreatedById,
					new Set<String>{
							form.Equipment__r.Planner_Group__c +
									EquipmentFormService.COMBINATION_DELIMITER +
									form.Equipment__r.Functional_Location__c
					}
			);

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully ' +
					'REJECTED and notification email ' +
					'has been sent.'));
		} catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			Database.rollback(sp);
			form.Status__c = originalStatus;
		}
		closePopup();
	}

	public PageReference rejectAndNotify() {
		showPopup();
		return null;
	}
}