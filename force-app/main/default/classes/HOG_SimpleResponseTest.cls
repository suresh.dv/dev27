/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_SimpleResponse
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_SimpleResponseTest {

    @IsTest
    static void addSuccessMessage_withoutParam() {
        HOG_SimpleResponse result;
        Boolean expectedSuccessFlag = true;
        String message = null;
        String expectedMessage = message;

        Test.startTest();
        result = new HOG_SimpleResponse().addSuccessMessage(message);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(expectedMessage, result.successMessage);
    }

    @IsTest
    static void addSuccessMessage_withParam() {
        HOG_SimpleResponse result;
        Boolean expectedSuccessFlag = true;
        String message = 'TestMessage';
        String expectedMessage = message;

        Test.startTest();
        result = new HOG_SimpleResponse().addSuccessMessage(message);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(expectedMessage, result.successMessage);
    }

    @IsTest
    static void addError_exception() {
        HOG_SimpleResponse result;
        Boolean expectedSuccessFlag = false;
        String message = 'TestMessage';
        HOG_Exception ex = new HOG_Exception(message);

        Test.startTest();
        result = new HOG_SimpleResponse();
        result.addError(ex);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(1, result.errors.size());
        System.assert(result.errors.get(0).contains(message));
    }

    @IsTest
    static void addError_string() {
        HOG_SimpleResponse result;
        Boolean expectedSuccessFlag = false;
        String message = 'TestMessage';
        String expectedMessage = message;

        Test.startTest();
        result = new HOG_SimpleResponse();
        result.addError(message);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(1, result.errors.size());
        System.assertEquals(expectedMessage, result.errors.get(0));
    }

}