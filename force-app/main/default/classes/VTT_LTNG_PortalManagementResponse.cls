/**
 * Created by MarcelBrimus on 28/11/2019.
 */

public class VTT_LTNG_PortalManagementResponse {
    @AuraEnabled
    public Boolean isCommunityUser;

    @AuraEnabled
    public List<Contact> enabledContacts;

    @AuraEnabled
    public List<Contact> disabledContacts;

    @AuraEnabled
    public List<Account> vendorAccounts;

    @AuraEnabled
    public Account vendorAccount;

    @AuraEnabled
    public Id hogContactRecordType;
}