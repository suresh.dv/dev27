/**
 * Created by MarcelBrimus on 03/09/2019.
 */

public inherited sharing class VTT_WorkOrderSummaryModel implements Comparable {
    @AuraEnabled
    public String WorkOrderName { get; set; }

    @AuraEnabled
    public String WorkOrderId { get; set; }

    @AuraEnabled
    public String LocationName { get; set; }

    @AuraEnabled
    public Decimal Hours { get; set; }

    // TODO this is definitelly wrong an might not be needed at all...., but it was in original calendar
    public Integer compareTo(Object compareTo) {

        VTT_WorkOrderSummaryModel WorkOrderSummaryItem = (VTT_WorkOrderSummaryModel) compareTo;
        if (this.WorkOrderName == WorkOrderSummaryItem.WorkOrderName) return 0;
        if (this.WorkOrderName > WorkOrderSummaryItem.WorkOrderName) return 1;
        return -1;
    }
}