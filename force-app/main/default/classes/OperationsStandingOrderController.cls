public with sharing class OperationsStandingOrderController {
    private final Standing_Order_Acknowledgement__c acknowledgement;
    public List<OperationsStandingOrderWrapper> standingOrderWrappers {get; set;}
    public Map<String, List<OperationsStandingOrderWrapper>> completedStandingOrderWrapperMap {get; set;}
    public Map<String, List<OperationsStandingOrderWrapper>> pendingStandingOrderWrapperMap {get; set;}    
    public String selectedStandingOrderId {get;set;}
    private String selectedTabName;

    public OperationsStandingOrderController() {
        buildStandingOrderWrapper();
    }    

    public OperationsStandingOrderController(ApexPages.StandardController stdController) {
        this.acknowledgement = (Standing_Order_Acknowledgement__c)stdController.getRecord();
        selectedTabName = ApexPages.currentPage().getParameters().get('tabName');
        buildStandingOrderWrapper();
    }

    public PageReference confirmReadAcknowledgement() {
        if(this.selectedStandingOrderId == null || this.selectedStandingOrderId == '') {
            return null;  
        }

        Standing_Order_Acknowledgement__c acknowledgement = getAvailableReadAcknowledgement(this.selectedStandingOrderId);

        if(acknowledgement == null) {
            acknowledgement = new Standing_Order_Acknowledgement__c();
            acknowledgement.Standing_Order__c = this.selectedStandingOrderId;
        } 
        
        acknowledgement.Read_Acknowledged__c = true;
        acknowledgement.Read_Acknowledged_Date__c = System.Now();
        acknowledgement.Acknowledged_User__c = UserInfo.getUserId();

        upsert acknowledgement;   
        
        PageReference currPage = new PageReference('/apex/OperationsStandingOrder?sfdc.tabName=');   
        currPage.setRedirect(true);
        return currPage;            

    }

    public void resetReadAcknowledgement(List<Standing_Order__c> updatedStandingOrders, Map<Id, Standing_Order__c> oldStandingOrders) {
        List<Standing_Order_Acknowledgement__c> acknowledgements = getAcknowledgementsByStandingOrder(updatedStandingOrders);
        

        if(acknowledgements.size() > 0) {
            for(Standing_Order_Acknowledgement__c acknowledgement : acknowledgements) {
                Standing_Order__c oldStandingOrder = oldStandingOrders.get(acknowledgement.Standing_Order__c);

                if(acknowledgement.Standing_Order__r.Details__c != oldStandingOrder.Details__c ||
                    acknowledgement.Standing_Order__r.Completion_Date__c != oldStandingOrder.Completion_Date__c ||
                    acknowledgement.Standing_Order__r.Start_Date__c != oldStandingOrder.Start_Date__c ){

                    acknowledgement.Read_Acknowledged__c = false;
                    acknowledgement.Read_Acknowledged_Date__c = null;                
                }
            }

            update acknowledgements;
        }
    }

    private Standing_Order_Acknowledgement__c getAvailableReadAcknowledgement(String selectedStandingOrder) {

        for(OperationsStandingOrderWrapper standingOrderWrapper : standingOrderWrappers) {
            if(standingOrderWrapper.acknowledgement.Standing_Order__c == selectedStandingOrder && 
                standingOrderWrapper.acknowledgement.Acknowledged_User__c == UserInfo.getUserId()) {

                return standingOrderWrapper.acknowledgement;
            }
        }

        return null;
    }

    private void buildStandingOrderWrapper() {
        standingOrderWrappers = new List<OperationsStandingOrderWrapper>();
        completedStandingOrderWrapperMap = new Map<String, List<OperationsStandingOrderWrapper>>(); 
        pendingStandingOrderWrapperMap = new Map<String, List<OperationsStandingOrderWrapper>>(); 

        List<Standing_Order__c> standingOrders = getStandingOrderList();
        List<Standing_Order_Acknowledgement__c> acknowledgements = getCurrentUserAcknowledgements();

        for(Standing_Order__c standingOrder : standingOrders) {
            OperationsStandingOrderWrapper sOrderWrapper = new OperationsStandingOrderWrapper(standingOrder);

            for(Standing_Order_Acknowledgement__c acknowledgement : acknowledgements) {
                if(acknowledgement.Standing_Order__c == standingOrder.Id) {
                    sOrderWrapper.setAcknowledgement(acknowledgement);
                    sOrderWrapper.setReadAcknowledge(acknowledgement.Read_Acknowledged__c);
                    sOrderWrapper.setReadAcknowledgementDate(acknowledgement.Read_Acknowledged_Date__c);

                    break;
                }
            }

            standingOrderWrappers.add(sOrderWrapper);

            if(sOrderWrapper.isReadAcknowledged) {
                if(completedStandingOrderWrapperMap.containsKey(standingOrder.Plant__c)) {
                    List<OperationsStandingOrderWrapper> stdOrderList = completedStandingOrderWrapperMap.get(standingOrder.Plant__c);
                    stdOrderList.add(sOrderWrapper);
                    completedStandingOrderWrapperMap.put(standingOrder.Plant__c, stdOrderList);
    
                } else {
                    List<OperationsStandingOrderWrapper> stdOrderList = new List<OperationsStandingOrderWrapper>();
                    stdOrderList.add(sOrderWrapper);
                    completedStandingOrderWrapperMap.put(standingOrder.Plant__c, stdOrderList);
                }  
                          
            } else {
                if(pendingStandingOrderWrapperMap.containsKey(standingOrder.Plant__c)) {
                    List<OperationsStandingOrderWrapper> stdOrderList = pendingStandingOrderWrapperMap.get(standingOrder.Plant__c);
                    stdOrderList.add(sOrderWrapper);
                    pendingStandingOrderWrapperMap.put(standingOrder.Plant__c, stdOrderList);
    
                } else {
                    List<OperationsStandingOrderWrapper> stdOrderList = new List<OperationsStandingOrderWrapper>();
                    stdOrderList.add(sOrderWrapper);
                    pendingStandingOrderWrapperMap.put(standingOrder.Plant__c, stdOrderList);
                }            
            }

        }
    }

    private List<Standing_Order__c> getStandingOrderList() {
        return [SELECT Id, Name, Completion_Date__c, Completion_Date_Status__c, Details__c, Engineering_Investigation__c, Equipment__r.Name, Plant__c, Process_Area__r.Name, Start_Date__c, Unit__r.Name, Work_notification__c
                FROM Standing_Order__c
                WHERE Completion_Date__c = null OR Completion_Date__c >= TODAY
                ORDER BY Plant__c DESC NULLS LAST];

    }

    private List<Standing_Order_Acknowledgement__c> getCurrentUserAcknowledgements() {
        return [SELECT Id, Read_Acknowledged__c, Read_Acknowledged_Date__c, Shift_Resource__c, Standing_Order__c, Acknowledged_User__c 
                FROM Standing_Order_Acknowledgement__c
                WHERE Acknowledged_User__c = :UserInfo.getUserId()
                ORDER BY Standing_Order__r.Plant__c NULLS LAST];
    }
    
    private List<Standing_Order_Acknowledgement__c> getAcknowledgementsByStandingOrder(List<Standing_Order__c> updatedStandingOrders) {
        return [SELECT Id, Read_Acknowledged__c, Read_Acknowledged_Date__c, Standing_Order__r.Details__c, Standing_Order__r.Start_Date__c, Standing_Order__r.Completion_Date__c, Standing_Order__c 
                FROM Standing_Order_Acknowledgement__c
                WHERE Standing_Order__c IN :updatedStandingOrders];
                                                                    
                                                                        
    }   

    public class OperationsStandingOrderWrapper{
        public Standing_Order__c standingOrder {get; set;}
        public Standing_Order_Acknowledgement__c acknowledgement {get; set;}
        public Boolean isReadAcknowledged {get; set;}
        public String readAcknowledgedDate {get; set;}

        public OperationsStandingOrderWrapper(Standing_Order__c standingOrder) {
            this.standingOrder = standingOrder;
            this.isReadAcknowledged = false;
        }

        public void setAcknowledgement(Standing_Order_Acknowledgement__c acknowledgement) {
            this.acknowledgement = acknowledgement;
        }

        public void setReadAcknowledge(Boolean isReadAcknowledged) {
            this.isReadAcknowledged = isReadAcknowledged;
        }

        public void setReadAcknowledgementDate(DateTime readAcknowledgedDate) {
            //format the Date / Time to align with start and end date format
            if(readAcknowledgedDate != null) {
                this.readAcknowledgedDate = readAcknowledgedDate.format('M/d/YYYY HH:mm');
            }
        }        
    }
}