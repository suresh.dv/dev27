/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Enum that is used for control actions for Assigned Text during Assign actions
                Currently there are 2 supported modes:
                    - CLEAR - Clears value of Assigned Text
                    - PRESERVE - Preserves value of Assigned Text
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public enum VTT_WOA_Assignment_AssignedTextAction {
    CLEAR,
    PRESERVE
}