/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Utility class for Load Confirmation purposes.
Test Class:     FM_LoadConfirmationEditCtrlTest
History:        jschn 05.02.2018 - Created.
                jschn 05.31.2018 - added getConfirmation using trucktripId and confirmationId as 
                                    parameters(excluding confirmations with confirmationId)
                                    US W-001114
                jschn 03/21/2019 - updated getLoadConfirmationOnFacilityWithScaleTicketNumber (added days condition)
                                 - US W-001418
                jschn 03/21/2019 - Added getLoadConfirmationWithTicketNumber see FM_LoadConfirmationEditCtrl for more info
                                 - US W-001417
**************************************************************************************************/
public class FM_LoadConfirmation_Utilities {
    @SuppressWarnings('ApexUnusedDeclaration')
    private static final String DEBUG_PREFIX = 'FM_LoadConfirmation_Utilities: ';

    public static final String ERROR_STN_DUPLICATE = Label.FM_LoadConfirmation_Err_STN_Duplicate;
    @SuppressWarnings('ApexUnusedDeclaration')
    public static final String ERROR_ALREADY_CONFIRMED = Label.FM_LoadConfirmation_TTAlreadyConfirmed;
    public static final String ERROR_DESTINATION_MISSING = Label.FM_LoadConfirmation_Err_DestinationMissing;
    public static final String ERROR_UNIT_CHANGE_REASON_MISSING = Label.FM_LoadConfirmation_Err_UnitChangeReasonMissing;
    public static final String ERROR_UNIT_CHANGE_REASON_POPULATED = Label.FM_LoadConfirmation_Err_UnitChangeReasonPopulated;
    public static final String ERROR_UNIT_CHANGE_COMMENT_MISSING = Label.FM_LoadConfirmation_Err_UnitChangeCommentMissing;
    public static final String ERROR_CONFIRMATION_NOTSUPPORTED = Label.FM_LoadConfirmation_Err_ConfirmationNotSupported;

    public static final String ERROR_MISSING_TICKETNUMBER = Label.FM_LoadConfirmation_Err_TicketNumberMissing;
    public static final String ERROR_MISSING_VOLUME = Label.FM_LoadConfirmation_Err_VolumeMissing;
    public static final String ERROR_STUCKTRUCK_MISSING_CARRIER = Label.FM_StuckTruck_Err_CarrierMissing;
    public static final String ERROR_STUCKTRUCK_MISSING_STUCK_FROM = Label.FM_StuckTruck_Err_StuckFromMissing;
    public static final String ERROR_STUCKTRUCK_WRONG_STUCK_TO = Label.FM_StuckTruck_Err_WrongStuckTo;
    public static final String ERROR_STUCKTRUCK_MISSING_TOWING_COMPANY = Label.FM_StuckTruck_Err_TowingCompanyMissing;

    public static final String NOTE_STUCKTRUCK_UNIT_FROM_CONFIRMATION = Label.FM_StuckTruck_Note_UnitFromConfirmation;

    public static final String RETURN_URL_PARAM_KEY = 'retURL';
    public static final String SAVERETURN_URL_PARAM_KEY = 'saveURL';
    public static final String TRUCKTRIP_ID_PARAM_KEY = 'ttid';
    public static final String CONFIRMATION_ID_PARAM_KEY = 'id';

    public static final String UNIT_CHANGE_REASON_OTHER = 'Other';

    public static final String EXTERNAL_DISPATCHER_PS_NAME = 'HOG_FM_Dispatcher_Vendor';

    public static final String STUCK_TRUCK_RECORD_TYPE_EXTERNAL = 'External Stuck Truck';
    public static final String STUCK_TRUCK_RECORD_TYPE_INTERNAL = 'Internal Stuck Truck';

    public static final String STUCK_TRUCK_STATUS_STUCK = 'Stuck';
    public static final String STUCK_TRUCK_STATUS_UNSTUCK = 'Unstuck';
    public static final String STUCK_TRUCK_STATUS_EXTERNAL_DEFAULT = STUCK_TRUCK_STATUS_UNSTUCK;

    public static final Integer DEFAULT_STN_LIFESPAN_DAYS = 365;

    /**
     * @return Id for External Stuck Truck Record Type
     */
    public static Id getExternalStuckTruckRecordTypeId() {
        return Schema.SObjectType.FM_Stuck_Truck__c.getRecordTypeInfosByName().get(STUCK_TRUCK_RECORD_TYPE_EXTERNAL).getRecordTypeId();
    }

    /**
     * @return Id for Internal Stuck Truck Record Type
     */
    public static Id getInternalStuckTruckRecordTypeId() {
        return Schema.SObjectType.FM_Stuck_Truck__c.getRecordTypeInfosByName().get(STUCK_TRUCK_RECORD_TYPE_INTERNAL).getRecordTypeId();
    }

    /**
    * Checks if user has HOG_FM_Dispatcher_Vendor PS assigned.
    */
    public static Boolean userHasExtDispatcherPS() {
        String userId = UserInfo.getUserId();
        return 1 == [
                SELECT COUNT()
                FROM PermissionSetAssignment
                WHERE PermissionSet.Name = :EXTERNAL_DISPATCHER_PS_NAME
                AND AssigneeId = :userId
        ];
    }

    /**
     * Get General Carrier Unit for selected carrer.
     * @param  carrierId Carrier Id for which General Unit would be retrieved
     * @return           General Unit Id
     */
    public static Id getGeneralCarrierUnit(String carrierId) {
        Id unitId;
        if (String.isNotBlank(carrierId)) {
            List<Carrier_Unit__c> units = [
                    SELECT Id
                    FROM Carrier_Unit__c
                    WHERE Carrier__c = :carrierId
                    AND Name LIKE '%General%'
                    AND Enabled__c = TRUE
            ];
            if (units != null && units.size() > 0) {
                unitId = units.get(0).Id;
            }
        }
        return unitId;
    }

    /**
     * Return Last Stuck Truck on selected Truck Trip or null
     * @param  truckTripId Truck Trip Id for which last Stuck Truck should be querried 
     * @return             Stuck Truck record
     */
    public static FM_Stuck_Truck__c getLastStuckTruckByTruckTripId(String truckTripId) {
        FM_Stuck_Truck__c stuckTruck;
        if (String.isNotBlank(truckTripId)) {
            List<FM_Stuck_Truck__c> stuckTrucks = [
                    SELECT Id
                            , Carrier__c
                            , Carrier_Unit__c
                            , Comments__c
                            , On_Lease__c
                            , Towing_Company_Lookup__c
                            , Stuck_From__c
                            , Stuck_To__c
                            , Status__c
                            , Location__c
                            , Facility__c
                            , Fluid_Loaded__c
                    FROM FM_Stuck_Truck__c
                    WHERE Truck_Trip__c = :truckTripId
                    ORDER BY CreatedDate DESC
            ];
            if (stuckTrucks != null && stuckTrucks.size() > 0) {
                stuckTruck = stuckTrucks.get(0);
            }
        }
        return stuckTruck;
    }

    /**
     * Get Load Confirmation record by Id
     * @param  confirmationId Id of Load Confirmation
     * @return                Load Confirmation record
     */
    public static FM_Load_Confirmation__c getConfirmation(String confirmationId) {
        FM_Load_Confirmation__c confirmation;
        if (String.isNotBlank(confirmationId)) {
            List<FM_Load_Confirmation__c> confirmations = [
                    SELECT Id
                            , Destination_Facility__c
                            , Destination_Location__c
                            , Product__c
                            , Unit__c
                            , Scale_Ticket_Number__c
                            , Source__c
                            , Ticket_Number__c
                            , Truck_Trip__c
                            , Unit_Configuration__c
                            , Volume__c
                            , Name
                            , LoadRequestId__c
                            , Truck_Trip__r.Facility__c
                            , Truck_Trip__r.Destination_Id__c
                            , Truck_Trip__r.Facility_Type__c
                            , Truck_Trip__r.Facility_Lookup__c
                            , Truck_Trip__r.Unit__c
                            , Truck_Trip__r.Source__c
                            , Truck_Trip__r.Tank__c
                            , Truck_Trip__r.Location_Lookup__c
                            , Truck_Trip__r.Unit_Change_Reason__c
                            , Truck_Trip__r.Unit_Change_Other_Reason__c
                            , Truck_Trip__r.Truck_Trip_Status__c
                            , Truck_Trip__r.Carrier__c
                            , Truck_Trip__r.Load_Request__r.Source_Location__c
                            , Truck_Trip__r.Load_Request__r.Source_Facility__c
                    FROM FM_Load_Confirmation__c
                    WHERE Id = :confirmationId
            ];
            if (confirmations != null && confirmations.size() > 0) {
                confirmation = confirmations.get(0);
            }
        }
        return confirmation;
    }

    /**
     * Get first load confirmation for Truck Trip except the one which ID was provided as parameter.
     * @param  truckTripId    ID for which Truck Trip query will be filtering Confirmations
     * @param  confirmationId ID of Confirmation which should be excluded from search
     * @return                First Load Confirmation retrieved from query.
     */
    public static FM_Load_Confirmation__c getLoadConfirmation(Id truckTripId, Id confirmationId) {
        List<FM_Load_Confirmation__c> confirmations = [
                SELECT Id
                        , Name
                FROM FM_Load_Confirmation__c
                WHERE Truck_Trip__c = :truckTripId
                AND Id != :confirmationId
        ];
        return (confirmations != null && confirmations.size() > 0)
                ? confirmations.get(0)
                : null;
    }

    /**
     * Get Truck Trip record based on Truck Trip Id
     * @param  truckTripId Id of Truck Trip
     * @return             Truck Trip record
     */
    public static FM_Truck_Trip__c getTruckTripData(String truckTripId) {
        FM_Truck_Trip__c truckTrip;
        if (String.isNotBlank(truckTripId)) {
            List<FM_Truck_Trip__c> truckTrips = [
                    SELECT Id
                            , Carrier__c
                            , Source__c
                            , Facility__c
                            , Destination_Id__c
                            , Facility_Type__c
                            , Facility_Lookup__c
                            , Unit__c
                            , Product__c
                            , Location_Lookup__c
                            , Unit_Change_Reason__c
                            , Unit_Change_Other_Reason__c
                            , Truck_Trip_Status__c
                            , Tank__c
                            , Load_Request__r.Source_Location__c
                            , Load_Request__r.Source_Facility__c
                    FROM FM_Truck_Trip__c
                    WHERE Id = :truckTripId
            ];
            if (truckTrips != null && truckTrips.size() > 0) {
                truckTrip = truckTrips.get(0);
            }
        }
        return truckTrip;
    }

    /**
     * Gets load confirmation on facility with same Scale Ticket Number (ingoring Confirmation with presented ID)
     * @param  facilityId        For which you want to query
     * @param  scaleTicketNumber Scale Ticket number which is entered
     * @param  confirmationId    Which Confirmation should be ignored
     * @return                   Load Confirmation record or null
     */
    public static List<FM_Load_Confirmation__c> getLoadConfirmationOnFacilityWithScaleTicketNumber(
            Id facilityId,
            String scaleTicketNumber,
            Id confirmationId) {

        HOG_Settings__c settings = HOG_Settings__c.getInstance();
        Integer inDays = DEFAULT_STN_LIFESPAN_DAYS;
        if(settings != null
                && settings.Scale_Ticket_Number_Days_Behind_Check__c != null
                && settings.Scale_Ticket_Number_Days_Behind_Check__c > 0) {
            inDays = Integer.valueOf(settings.Scale_Ticket_Number_Days_Behind_Check__c);
        }
        Date limitDate = Date.today().addDays(-inDays);

        return [
                SELECT Truck_Trip__r.Name
                FROM FM_Load_Confirmation__c
                WHERE Destination_Facility__c = :facilityId
                AND Scale_Ticket_Number__c = :scaleTicketNumber
                AND Id != :confirmationId
                AND CreatedDate >= :limitDate
        ];
    }


    /**
     * Gets load confirmation on facility with same Ticket Number (ignoring Confirmation with presented ID)
     * @param  carrierId         For which you want to query
     * @param  ticketNumber      Ticket number which is entered
     * @param  confirmationId    Which Confirmation should be ignored
     * @return                   Load Confirmation record or null
     */
    public static List<FM_Load_Confirmation__c> getLoadConfirmationWithTicketNumber(
            Id carrierId,
            String ticketNumber,
            Id confirmationId) {
        return [
                SELECT Truck_Trip__r.Name
                FROM FM_Load_Confirmation__c
                WHERE Truck_Trip__r.Carrier__c = :carrierId
                AND Ticket_Number__c = :ticketNumber
                AND Id != :confirmationId
        ];
    }

    /**
     * Get Destination map to assign target destination (location/facility) based on Ids and facility type.
     * @param  truckTrip Truck trip for which you want to know Destination
     * @return           Map containing destination behind key(Location/Facility)
     */
    public static Map<String, Id> getDestinationIdMap(FM_Truck_Trip__c truckTrip) {
        Map<String, Id> destinationIdMap = new Map<String, Id>();
        Id destinationId = String.isNotBlank(truckTrip.Destination_Id__c)
                ? truckTrip.Destination_Id__c
                : String.isNotBlank(truckTrip.Facility_Lookup__c)
                        ? truckTrip.Facility_Lookup__c
                        : truckTrip.Location_Lookup__c;
        String destinationType = String.isNotBlank(truckTrip.Facility_Type__c) ? truckTrip.Facility_Type__c : 'Location';
        destinationIdMap.put(destinationType, destinationId);
        return destinationIdMap;
    }

    /**
     * Prepare new Stuck Truck record with External Stuck Truck Record Type.
     * @return Stuck Truck record
     */
    public static FM_Stuck_Truck__c getExternalStuckTruck() {
        return new FM_Stuck_Truck__c(RecordTypeId = getExternalStuckTruckRecordTypeId());
    }

    /**
     * Prepare new Stuck Truck record with Internal Stuck Truck Record Type.
     * @return Stuck Truck record
     */
    public static FM_Stuck_Truck__c getInternalStuckTruck() {
        return new FM_Stuck_Truck__c(RecordTypeId = getInternalStuckTruckRecordTypeId());
    }

    /**
     * Adds error msg on page.
     * @param msg Content of error msg.
     */
    public static void addPageError(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }

}