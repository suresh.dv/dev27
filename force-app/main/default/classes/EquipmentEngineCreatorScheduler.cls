/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Scheduler class for Equipment Engine update/create job.
Test Class:     EquipmentEngineCreatorSchedulerTest
History:        jschn 2019-06-13 - Created. - EPD R1
*************************************************************************************************/
public with sharing class EquipmentEngineCreatorScheduler {

    private static final String CLASS_NAME = String.valueOf(EquipmentEngineCreatorScheduler.class);

    public static final String DEFAULT_CRON_STRING = '0 30 6 * * ?';

    /**
     * Schedule job with default cron string.
     *
     * @return Id
     */
    public static Id schedule() {
        return schedule(DEFAULT_CRON_STRING);
    }

    /**
     * Schedule job with provided cron string.
     *
     * @param cronString
     *
     * @return Id
     */
    public static Id schedule(String cronString) {
        Id jobId = System.schedule(
                EquipmentEngineCreatorSchedulable.JOB_NAME,
                cronString,
                new EquipmentEngineCreatorSchedulable()
        );

        System.debug(CLASS_NAME + ' -> Job was successfully scheduled.\n' +
                'Job Name: ' + EquipmentEngineCreatorSchedulable.JOB_NAME + '\n' +
                'Job ID: ' + jobId);

        return jobId;
    }

}