/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VTT_WOA_SearchCriteria_Handler class
History:        jschn 28/10/2019 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class VTT_WOA_SearchCriteria_HandlerTest {

    @IsTest
    static void getFilterFormOptions_validationError() {
        VTT_WOA_SearchCriteria_Options result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().getFilterFormOptions(null);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateUserIdParam is run, it should throw error');
        System.assertEquals(null, result, 'As validateUserIdParam is run, it should throw error so result should be null');
    }

    @IsTest
    static void getTradesmanForAccount_null() {
        List<HOG_PicklistItem_Simple> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 2;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().getTradesmanForAccount(null);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As simple query is run, can\'t fail');
        System.assertNotEquals(null, result, 'As action didn\'t fail, result is not null');
        System.assertEquals(expectedCount, result.size(), 'By default there should be 2 options in the list even without any Contact record.');
    }

    @IsTest
    static void getTradesmanForAccount_mockSingleRec() {
        List<HOG_PicklistItem_Simple> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 3;
        VTT_WOA_SCDAOProvider.contactDAO = new SignalMock_getTradesmanForAccount();

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().getTradesmanForAccount(UserInfo.getUserId());
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As simple query is run, can\'t fail');
        System.assertNotEquals(null, result, 'As action didn\'t fail, result is not null');
        System.assertEquals(expectedCount, result.size(), 'By default there should be 2 options in the list + 1 with queried Contact record.');
    }

    class SignalMock_getTradesmanForAccount implements VTT_SCContactDAO {
        public List<Contact> getContactsByAccount(Id accId) {
            return new List<Contact>{
                    new Contact()
            };
        }
    }

    @IsTest
    static void getStoredFilters_validationError() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().getStoredFilters(null);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateUserIdParam is run, it should throw error');
        System.assertEquals(null, result, 'As validateUserIdParam is run, it should throw error so result should be null');
    }

    @IsTest
    static void getStoredFilters_emptyListMock() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;
        VTT_WOA_SCDAOProvider.searchCriteriaDAO = new EmptyListMock_getStoredFilters();

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().getStoredFilters(UserInfo.getUserId());
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As simple query is run, can\'t fail');
        System.assertNotEquals(null, result, 'As action didn\'t fail, result is not null');
        System.assertEquals(expectedCount, result.size(), 'As mock returns empty list, size of the list should be 0.');
    }

    class EmptyListMock_getStoredFilters implements VTT_WOA_SearchCriteriaDAO {
        public List<Work_Order_Activity_Search_Criteria__c> getUserActivitySearchFilters(Id pUserId) {
            return new List<Work_Order_Activity_Search_Criteria__c>();
        }
    }

    @IsTest
    static void getStoredFilters_singleItemListMock() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;
        VTT_WOA_SCDAOProvider.searchCriteriaDAO = new SingleItemListMock_getStoredFilters();

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().getStoredFilters(UserInfo.getUserId());
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As simple query is run, can\'t fail');
        System.assertNotEquals(null, result, 'As action didn\'t fail, result is not null');
        System.assertEquals(expectedCount, result.size(), 'As mock returns list with one record, size of the list should be 1.');
    }

    class SingleItemListMock_getStoredFilters implements VTT_WOA_SearchCriteriaDAO {
        public List<Work_Order_Activity_Search_Criteria__c> getUserActivitySearchFilters(Id pUserId) {
            return new List<Work_Order_Activity_Search_Criteria__c> {
                    new Work_Order_Activity_Search_Criteria__c()
            };
        }
    }

    @IsTest
    static void setLastSelectedFilter_validationErrorFilterId() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().setLastSelectedFilter(null, UserInfo.getUserId());
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateFilterId is run, it should throw error');
        System.assertEquals(null, result, 'As validateFilterId is run, it should throw error so result should be null');
    }

    @IsTest
    static void setLastSelectedFilter_validationErrorUserId() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().setLastSelectedFilter(UserInfo.getUserId(), null);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateUserIdParam is run, it should throw error');
        System.assertEquals(null, result, 'As validateUserIdParam is run, it should throw error so result should be null');
    }

    @IsTest
    static void setLastSelectedFilter_mockEmpty() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;
        VTT_WOA_SCDAOProvider.searchCriteriaDAO = new EmptyListMock_getStoredFilters();

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().setLastSelectedFilter(UserInfo.getUserId(), UserInfo.getUserId());
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As simple query is run, can\'t fail');
        System.assertNotEquals(null, result, 'As action didn\'t fail, result is not null');
        System.assertEquals(expectedCount, result.size(), 'As mock returns list with 0 records, size of the list should be 0.');
    }

    @IsTest
    static void saveFilter_validationErrorUserId() {
        Work_Order_Activity_Search_Criteria__c result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().saveFilter('Test1', 'Test2', null);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateUserIdParam is run, it should throw error');
        System.assertEquals(null, result, 'As validateUserIdParam is run, it should throw error so result should be null');
    }

    @IsTest
    static void saveFilter_validationErrorFilterString() {
        Work_Order_Activity_Search_Criteria__c result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().saveFilter('', 'Test2', UserInfo.getUserId());
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateFilterParams is run, it should throw error');
        System.assertEquals(null, result, 'As validateFilterParams is run, it should throw error so result should be null');
    }

    @IsTest
    static void saveFilter_validationErrorFilterName() {
        Work_Order_Activity_Search_Criteria__c result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().saveFilter('Test1', '', UserInfo.getUserId());
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateFilterParams is run, it should throw error');
        System.assertEquals(null, result, 'As validateFilterParams is run, it should throw error so result should be null');
    }

    @IsTest
    static void deleteFilter_validationError() {
        String result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        Test.startTest();
        try {
            result = new VTT_WOA_SearchCriteria_Handler().deleteFilter(null);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As validateFilterId is run, it should throw error');
        System.assertEquals(null, result, 'As validateFilterId is run, it should throw error so result should be null');
    }

}