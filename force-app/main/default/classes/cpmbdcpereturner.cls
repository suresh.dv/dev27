public class cpmbdcpereturner{
    
    public List<cpm_BD_PCE__c> lstBDPCE{get;set;}
    public Opportunity opp {get;set;}
    //public String myname {get;set;}
    
    public cpmbdcpereturner(ApexPages.StandardController mycontroller) {
       opp = (Opportunity)mycontroller.getRecord(); 
       lstBDPCE = [
           select Opportunity__c, name, cpm_Quantity__c, cpm_Unit__c, cpm_Type__c, cpm_Rate__c, cpm_Estimated_Cost__c, cpm_Comments__c, cpm_Category__c 
           from cpm_BD_PCE__c 
           where Opportunity__c =: opp.Id 
       ];
   }
    
    public PageReference save() {
        update lstBDPCE;
        PageReference pRef= new PageReference('/'+opp.Id);
		pRef.setRedirect(true);
		return pRef;
	}
}