/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WOA_Assignment_Service class
History:        jschn 31/10/2019 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class VTT_WOA_Assignment_ServiceTest {

    @IsTest
    static void runAssignmentWithoutTradesmen_invalidCase_withoutVendorId() {
        List<Work_Order_Activity__c> activities;
        List<Id> tradesmenIds;
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;

        Test.startTest();
        response = new VTT_WOA_Assignment_Service().runAssignmentWithoutTradesmen(activities, tradesmenIds, vendorAccountId);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runAssignmentWithoutTradesmen_invalidCase_withTradesmanId() {
        List<Work_Order_Activity__c> activities;
        List<Id> tradesmenIds = new List<Id> {UserInfo.getUserId()};
        Id vendorAccountId = UserInfo.getUserId();
        VTT_WOA_Assignment_Response response;

        Test.startTest();
        response = new VTT_WOA_Assignment_Service().runAssignmentWithoutTradesmen(activities, tradesmenIds, vendorAccountId);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runAssignmentWithTradesmen_invalidCase_withoutVendorAccount() {
        List<Work_Order_Activity__c> activities;
        List<Id> tradesmenIds = new List<Id> ();
        Id vendorAccountId;
        VTT_WOA_Assignment_Response response;

        Test.startTest();
        response = new VTT_WOA_Assignment_Service().runAssignmentWithTradesmen(activities, tradesmenIds, vendorAccountId);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runAssignmentWithTradesmen_invalidCase_withoutTradesman() {
        List<Work_Order_Activity__c> activities;
        List<Id> tradesmenIds = new List<Id> ();
        Id vendorAccountId = UserInfo.getUserId();
        VTT_WOA_Assignment_Response response;

        Test.startTest();
        response = new VTT_WOA_Assignment_Service().runAssignmentWithTradesmen(activities, tradesmenIds, vendorAccountId);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runUnAssignmentWithTradesmen_invalidCase_withoutActivities() {
        List<Work_Order_Activity__c> activities;
        List<Id> tradesmenIds = new List<Id> ();
        VTT_WOA_Assignment_Response response;

        Test.startTest();
        response = new VTT_WOA_Assignment_Service().runUnAssignmentWithTradesmen(activities, tradesmenIds);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runUnAssignmentWithTradesmen_invalidCase_withEmptyTradesmenList() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id> ();
        VTT_WOA_Assignment_Response response;

        Test.startTest();
        response = new VTT_WOA_Assignment_Service().runUnAssignmentWithTradesmen(activities, tradesmenIds);
        Test.stopTest();

        System.assertNotEquals(null, response);
        System.assertEquals(VTT_WOA_Assignment_Response.INVALID_CASE, response);
    }

    @IsTest
    static void runUnAssignmentWithTradesmen_mock_error() {
        List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();
        List<Id> tradesmenIds = new List<Id> {UserInfo.getUserId()};
        VTT_WOA_Assignment_Response response;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;
        VTT_WOA_Assignment_Service.WOAAssignmentDAO = new SingleAssignmentRecMock();

        Test.startTest();
        try{
            response = new VTT_WOA_Assignment_Service().runUnAssignmentWithTradesmen(activities, tradesmenIds);
        } catch (HOG_Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(null, response);
        System.assertEquals(expectedFailFlag, failFlag);
    }

    class SingleAssignmentRecMock implements VTT_WOA_AssignmentDAO {
        public List<Work_Order_Activity_Assignment__c> getAssignments(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds) {
            return new List<Work_Order_Activity_Assignment__c>{
                    new Work_Order_Activity_Assignment__c()
            };
        }
        public List<Work_Order_Activity_Assignment__c> getAssignments(Set<Id> activityIdsForDeletion) {return null;}
        public List<Work_Order_Activity_Assignment__c> getAssignments(Set<Id> activityIdsForDeletion, List<Work_Order_Activity__c> activities, List<Id> tradesmenIds) {return null;}
        public List<Work_Order_Activity_Assignment__c> getNonRejectedAssignmentsByTradesman(String tradesman) {return null;}
        public List<Work_Order_Activity_Assignment__c> getNonRejectedAssignmentsByActivity(Id activityId) {return null;}
    }

}