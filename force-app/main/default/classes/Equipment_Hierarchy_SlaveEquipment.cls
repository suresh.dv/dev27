/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_Hierarchy_SlaveEquipment {
	@AuraEnabled
	public Equipment__c equipment;

	@AuraEnabled
	public Boolean hasChild = false;
}