/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentTransferFormBulkClose page to close Status of multiple Equipment Transfer Request records

Test Class:    EquipmentTransferFormTest

History:       
			   26-Mar-15	Gangha Kaliyan	To include facilities in Equipment Inventory
			   27-Feb-17    Miro Zelina included reference to Well Event
			   29-Jun-17    Miro Zelina included reference to System and Sub-System
			   27-Jul-17    Miro Zelina - added role check for SAP Support Lead
			   04-Aug-17    Miro Zelina - added profile & PS check for process button
         		04.09.17    Marcel Brimus - added FEL and Yard
         		16-Nov-18 	Maros Grajcar - Sprint - Equipment Transfer Release 1
							- added Rejected button to page
               				- added popup window on Rejected button
               				- added new field to the object
               				- added logic to get email from Planner Group
				08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude - Email Service Enhancement R
               	12.09.19 	Maros Grajcar - W-001635 - sorting list for Process list view button
------------------------------------------------------------*/

public with sharing class EquipmentTransferFormBulkCloseController {

	public List<Equipment_Transfer_Form__c> selectedList { get; private set; }
	private String closePopupReason;
	public Boolean displayPopup { get; set; }
	public Integer CurrentIndex { get; private set; }
	private EquipmentFormService eqTransServ;
	public Equipment_Transfer_Form__c CurrentForm { get; private set; }
	public EquipmentUtilities.UserPermissions userPermissions;

	public EquipmentTransferFormBulkCloseController(ApexPages.StandardSetController controller) {
		userPermissions = new EquipmentUtilities.UserPermissions();

		if (!Test.isRunningTest()) {
			//08-Jul-19 - Jakub Schon - added Functional_Location__c
			controller.addFields(new List<String>{'From_Location__c', 'To_Location__c', 'Charge_No__c', 'Status__c', 'Date_of_Physical_Transfer__c', 'Authorized_By__c', 'Reason_for_Transfer_Additional_Details__c', 'Shipped_Via__c', 'Waybill_No__c', 'Maintenance_Work_Order__c', 'CreatedById', 'CreatedDate', 'LastModifiedById', 'LastModifiedDate', 'Name', 'Comments__c', 'From_Facility__c', 'To_Facility__c', 'From_Well_Event__c', 'To_Well_Event__c', 'From_System__c', 'To_System__c', 'From_Sub_System__c', 'To_Sub_System__c', 'From_Functional_Equipment_Level__c', 'To_Functional_Equipment_Level__c', 'From_Yard__c', 'To_Yard__c', 'Safety_Critical_Equipment__c', 'Regulatory_Equipment__c', 'Authorized_By__r.Email', 'Reason__c', 'Expiration_Date__c', 'Send_Email_Notification__c', 'Location_Label__c', 'FROM_Source__c', 'TO_Destination__c', 'Equipment_Description__c', 'FROM_Source__c', 'TO_Destination__c', 'From_Location__r.Planner_Group__c','From_Location__r.Functional_Location__c', 'To_Location__r.Planner_Group__c','To_Location__r.Functional_Location__c', 'From_Facility__r.Planner_Group__c','From_Facility__r.Functional_Location__c', 'To_Facility__r.Planner_Group__c','To_Facility__r.Functional_Location__c', 'From_Yard__r.Planner_Group__c','From_Yard__r.Functional_Location__c', 'To_Yard__r.Planner_Group__c','To_Yard__r.Functional_Location__c', 'From_Well_Event__r.Planner_Group__c','From_Well_Event__r.Functional_Location__c', 'To_Well_Event__r.Planner_Group__c','To_Well_Event__r.Functional_Location__c', 'From_System__r.Planner_Group__c','From_System__r.Functional_Location__c', 'To_System__r.Planner_Group__c','To_System__r.Functional_Location__c', 'From_Sub_System__r.Planner_Group__c','From_Sub_System__r.Functional_Location__c', 'To_Sub_System__r.Planner_Group__c','To_Sub_System__r.Functional_Location__c', 'From_Functional_Equipment_Level__r.Planner_Group__c','From_Functional_Equipment_Level__r.Functional_Location__c', 'To_Functional_Equipment_Level__r.Planner_Group__c','To_Functional_Equipment_Level__r.Functional_Location__c'});
		}

		if (!isUserAdmin) {

			selectedList = new List<Equipment_Transfer_Form__c>();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
			return;
		}

		else if (controller.getSelected().size() == 0) {
			selectedList = new List<Equipment_Transfer_Form__c>();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Equipment Transfer Form'));
			return;
		}

		selectedList = controller.getSelected();
		selectedList.sort();
		CurrentIndex = 1;

		CurrentForm = selectedList[CurrentIndex - 1];
		eqTransServ = new EquipmentFormService();

	}
	public List<EquipmentItemWrapper> equipmentList {
		get {
			if (equipmentList == null) {
				equipmentList = new List<EquipmentItemWrapper>();

				if (CurrentForm != null) {
					Map<Id, Equipment_Transfer_Item__c> transferItemMap = new Map<Id, Equipment_Transfer_Item__c>([
							SELECT Id, Equipment__r.Description_of_Equipment__c, Equipment__r.Manufacturer__c,
									Equipment__r.Model_Number__c, Equipment__r.Manufacturer_Serial_No__c, Equipment__r.Tag_Number__c,
									Equipment__c, Equipment__r.Equipment_Number__c, Tag_Colour__c,Equipment__r.Location__c, Equipment__r.Facility__c,
									Equipment__r.Well_Event__c, Equipment__r.System__c, Equipment__r.Sub_System__c, Equipment__r.Functional_Equipment_Level__c, Equipment__r.Yard__c
							FROM Equipment_Transfer_Item__c
							WHERE Equipment_Transfer_Form__c = :CurrentForm.Id
							ORDER BY Equipment__r.Description_of_Equipment__c
					]);

					/*for (Equipment_Transfer_Item__c item : itemList) {
						equipmentList.add(new EquipmentItemWrapper(item));
					}*/



					List<ContentDocumentLink> cdlList = [
							SELECT Id, ContentDocument.Title, ContentDocumentId, LinkedEntityId
							FROM ContentDocumentLink
							WHERE LinkedEntityId IN :transferItemMap.keySet()
					];

					List<Attachment> attachmentList = [
							SELECT Id, Name, ParentId, ContentType
							FROM Attachment
							WHERE ParentId IN :transferItemMap.keySet()
					];

					Set<Id> usedIds = new Set<Id>();


					for (ContentDocumentLink cld : cdlList) {
						if (transferItemMap.containsKey(cld.LinkedEntityId)) {
							Equipment_Transfer_Item__c item = transferItemMap.get(cld.LinkedEntityId);
							EquipmentItemWrapper wrapper = new EquipmentItemWrapper(item);
							wrapper.photoFile = cld;
							equipmentList.add(wrapper);
							usedIds.add(cld.LinkedEntityId);
						}
					}

					for (Attachment attachment : attachmentList) {
						if (transferItemMap.containsKey(attachment.ParentId)) {
							Equipment_Transfer_Item__c item = transferItemMap.get(attachment.ParentId);
							EquipmentItemWrapper wrapper = new EquipmentItemWrapper(item);
							wrapper.photo = attachment;
							equipmentList.add(wrapper);
							usedIds.add(attachment.ParentId);
						}
					}


					for (Equipment_Transfer_Item__c item : transferItemMap.values()) {
						if (!usedIds.contains(item.Id)) {
							EquipmentItemWrapper wrapper = new EquipmentItemWrapper(item);
							equipmentList.add(wrapper);
						}
					}


				}

			}
			return equipmentList;
		}
		set;
	}
	public Boolean getHasPreviousForm() {
		return (CurrentIndex > 1);
	}

	public Boolean getHasNextForm() {
		return (CurrentIndex < selectedList.size());
	}

	public PageReference NextForm() {
		if (getHasNextForm()) {
			CurrentIndex = CurrentIndex + 1 ;
			CurrentForm = selectedList[CurrentIndex - 1];
			equipmentList = null;
			System.debug(CurrentIndex);
		}
		return null;
	}

	public PageReference PreviousForm() {
		if (getHasPreviousForm()) {
			System.debug(CurrentIndex);
			CurrentIndex = CurrentIndex - 1 ;
			CurrentForm = selectedList[CurrentIndex - 1];
			equipmentList = null;
			System.debug(CurrentIndex);
		}
		return null;
	}

	public PageReference CloseForm() {
		Equipment_Transfer_Form__c obj = CurrentForm;

		if (obj.Status__c != EquipmentUtilities.REQUEST_STATUS_PROCESSED && obj.Status__c != EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED && obj.Status__c != EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED) {
			System.Savepoint sp = Database.setSavepoint();
			try {
				obj.Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED;
				update obj;
				String plannerGroupToDestination = eqTransServ.toDestinationPlannerGroup(CurrentForm);
				String plannerGroupFromSource = eqTransServ.fromSourcePlannerGroup(CurrentForm);
				//08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude
				String functionalLocationForSource = eqTransServ.getFunctionalLocationForFromSource(CurrentForm);
				String functionalLocationForTarget = eqTransServ.getFunctionalLocationForToDestination(CurrentForm);
				Set<String> plannerGroup = new Set<String>{
						plannerGroupToDestination + EquipmentFormService.COMBINATION_DELIMITER + functionalLocationForTarget,
						plannerGroupFromSource + EquipmentFormService.COMBINATION_DELIMITER + functionalLocationForSource
				};

				EquipmentFormService.formProcessedTemplateEmail(CurrentForm.Id, plannerGroup);
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully PROCESSED and notification email has been sent.'));
				return null;

			} catch (Exception ex) {
				Database.rollback(sp);
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
				return null;
			}
		}

		return null;

	}

	/**
	 * populate field with equipment from the list, to prevent duplication, field is set to blank and get values again
	 */
	private void populateEquipmentDescriptionField() {
		CurrentForm.Equipment_Description__c = '';
		for (EquipmentItemWrapper item : equipmentList) {
			if(String.isNotBlank(CurrentForm.Equipment_Description__c)) CurrentForm.Equipment_Description__c += '\n';
			CurrentForm.Equipment_Description__c += item.equipment.Equipment__r.Equipment_Number__c + ' - ' + item.equipment.Equipment__r.Description_of_Equipment__c;
		}
	}

	public Boolean isUserAdmin {
		get {
			return userPermissions.isSystemAdmin
					|| userPermissions.isSapSupportLead
					|| userPermissions.isHogAdmin;
		}
		private set;
	}

	/**
	* functions for popup window, to prevent displaying the reason on salesforce side on cancel button,
	* we are using this variable to keep the old reason
	*/
	public void showPopup() {
		displayPopup = true;
		closePopupReason = CurrentForm.Reason__c;
	}
	public void closePopup() {
		displayPopup = false;
	}

	public void closePopupOnPage() {
		displayPopup = false;
		CurrentForm.Reason__c = closePopupReason;
	}

	public void saveOnPopup() {
		System.Savepoint sp = Database.setSavepoint();
		String originalStatus = CurrentForm.Status__c;

		CurrentForm.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;
		populateEquipmentDescriptionField();
		try {
			update CurrentForm;
			String plannerGroupToDestination = eqTransServ.toDestinationPlannerGroup(CurrentForm);
			String plannerGroupFromSource = eqTransServ.fromSourcePlannerGroup(CurrentForm);
			//08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude
			String functionalLocationForSource = eqTransServ.getFunctionalLocationForFromSource(CurrentForm);
			String functionalLocationForTarget = eqTransServ.getFunctionalLocationForToDestination(CurrentForm);
			Set<String> plannerGroup = new Set<String>{
					plannerGroupToDestination + EquipmentFormService.COMBINATION_DELIMITER + functionalLocationForTarget,
					plannerGroupFromSource + EquipmentFormService.COMBINATION_DELIMITER + functionalLocationForSource
			};
			System.debug('Planner Group combination set: ' + plannerGroup);

			EquipmentFormService.requestRejectionTemplateEmail(CurrentForm.Id, CurrentForm.CreatedById, plannerGroup);

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully ' +
					'REJECTED and notification email ' +
					'has been sent.'));
		} catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			Database.rollback(sp);
			CurrentForm.Status__c = originalStatus;
		}
		closePopup();
	}

	public PageReference rejectAndNotify() {
		showPopup();
		return null;
	}

	public PageReference redirectPage() {
		PageReference nextPage;
		if (ApexPages.currentPage().getParameters().get('retURL') != null) {
			System.debug('retURL =' + ApexPages.currentPage().getParameters().get('retURL'));
			nextPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
			nextPage.setRedirect(true);
		} else if (ApexPages.currentPage().getParameters().get('retURL').contains('%2F')) {
			String redirectExitButton = ApexPages.currentPage().getParameters().get('retURL').replace('%2F', '');
			nextPage = new PageReference('/' + redirectExitButton);
			nextPage.setRedirect(true);
		} else nextPage = new PageReference('/');
		return nextPage;
	}

}