public class HOG_DMLResponseImpl extends HOG_DMLResponse {

	@TestVisible
    private static final String VALIDATION_ERROR_PREFIX 	= 'FIELD_CUSTOM_VALIDATION_EXCEPTION,';
	@TestVisible
    private static final String VALIDATION_ERROR_POSTFIX 	= ':';

    public HOG_DMLResponseImpl(SObject result){
		success = true;
		resultObject = result;
	}

	public HOG_DMLResponseImpl(String errorMsg) {
		success = false;
		addError(errorMsg);
	}

	public HOG_DMLResponseImpl(Exception ex) {
		success = false;
		addError(ex);
	}	

	public List<String> addError(String errorMsg) {
		if(errors == null) errors = new List<String>();
		errorMsg = prettifyValidationError(errorMsg);
		errors.add(errorMsg);
		return errors;
	}

	public List<String> addError(Exception ex) {
		addError(ex.getMessage());
		return errors;
	}
	
	private String prettifyValidationError(String errorMsg) {
		if(String.isNotBlank(errorMsg)) {
			if(errorMsg.contains(VALIDATION_ERROR_PREFIX)) {
				errorMsg = errorMsg.split(VALIDATION_ERROR_PREFIX).get(1);
				if(errorMsg.contains(VALIDATION_ERROR_POSTFIX)) {
					errorMsg = errorMsg.split(VALIDATION_ERROR_POSTFIX).get(0);
				}
			}
		}
		return errorMsg;
	}

}