@isTest
private class RaM_CaseStatusHandlerTest {

 @testSetUp static void setup() {
  User u;
  System.runAs(u = new user(ID = UserInfo.getUserID())) {

   // Insert account
   Account accountRecord = new Account(Name = 'Husky Account', VendorGroup__c = 'BUNN-O-MATIC');
   accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
   insert accountRecord;

   //Insert Contact
   Contact contactRecord = new Contact(LastName = 'Glenn Mundi', ContactLogin__c = 'PRQ1234');
   contactRecord.AccountId = accountRecord.id;
   insert contactRecord;

   // Insert Location
   RaM_Location__c locationRecord = new RaM_Location__c(Name = '10001', Service_Area__c = 'Island', SAPFuncLOC__c = 'C1-02-002-01006', Location_Classification__c = 'Urban', R_M_Location__c = true);
   insert locationRecord;

   //Insert Equipment
   RaM_Equipment__c equipmentRecord = new RaM_Equipment__c(Name = 'Test1');
   equipmentRecord.FunctionalLocation__c = 'C1-02-002-01006';
   equipmentRecord.Equipment_Type__c = 'ELECTRICAL';
   equipmentRecord.Status__c = 'Active';
   equipmentRecord.Equipment_Class__c = 'ELECTRICAL PANEL';
   insert equipmentRecord;

   // Insert Vendor Assignment
   RaM_VendorAssignment__c vaRecord = new RaM_VendorAssignment__c();
   vaRecord.Account__c = accountRecord.id;
   vaRecord.Equipment_Type_VA__c = 'ELECTRICAL';
   vaRecord.Equipment_Class_VA__c = 'ELECTRICAL PANEL';
   vaRecord.Priority__c = 1;
   vaRecord.ServiceArea__c = 'Island';
   vaRecord.Location_Classification_VA__c = 'Urban';
   insert vaRecord;

   //Insert Entitlement Record
   Entitlement entitlementRecord = new Entitlement(Name = 'SLA Husky Energy', AccountId = AccountRecord.id);
   insert entitlementRecord;

   // Insert Ram Setting 
   RaM_Setting__c cusSetting = new RaM_Setting__c();
   cusSetting.name = 'Case Next Counter';
   cusSetting.R_M_Ticket_Number_Next_Sequence__c = 1234567;
   insert cusSetting;

  }

 }

 testMethod static void testCaseStatusUpdate() {

  User u1;
  System.runAs(u1 = new user(ID = UserInfo.getUserID())) {

   Ram_Location__c locationRecord = [select id, Name from Ram_Location__c limit 1];
   RaM_VendorAssignment__c vaRecord = [select id, Account__c, Equipment_Type_VA__c, Equipment_Class_VA__c, ServiceArea__c, Location_Classification_VA__c, Priority__c from RaM_VendorAssignment__c limit 1];

   RaM_Equipment__c equipmentRecord = [select id, Equipment_Type__c, Equipment_Class__c from RaM_Equipment__c];

   List < Case > caseRecords = new List < Case > ();
   // To get case record type ids with out using query
   Id childTicketRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Child Case').getRecordTypeId();
   Id ramTicketRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();

   //insert cases  
   Case c1 = new Case(RecordTypeId = ramTicketRecordTypeId, Status = 'New', Priority = 'P1-Emergency', Origin = 'Maintenance Tech', LocationNumber__c = LocationRecord.id, EquipmentName__c = equipmentRecord.id, Location_Contact__c = 'Amit Singh', Equipment_Type__c = 'ELECTRICAL', Equipment_Class__c = 'ELECTRICAL PANEL', Location_Classification__c = 'Urban', Service_Area__c = 'Island', subject = 'test', description = 'test');
   System.assertEquals(c1.status, 'New');
   insert c1;
   System.debug('c1 inseted***' + c1);

   List < RaM_CaseStatusHandler.CaseStatus > cases = new List < RaM_CaseStatusHandler.CaseStatus > ();
   RaM_CaseStatusHandler.CaseStatus info1 = new RaM_CaseStatusHandler.CaseStatus();
   info1.ticketNumber_R_M = c1.Ticket_Number_R_M__c; //'R0177826';
   info1.caseStatus = 'New';
   cases.add(info1);
   System.debug('cases***' + cases);
   /*
   RaM_CaseStatusHandler5.CaseStatus info2 = new RaM_CaseStatusHandler5.CaseStatus();
   info2.caseNumber = csList[1].caseNumber;//'678910';
   info2.caseStatus = 'Pending Parts';
   cases.add(info2);
   */
   List < RaM_CaseStatusHandler.CaseStatusResponce > csrList = new List < RaM_CaseStatusHandler.CaseStatusResponce > ();

   csrList = RaM_CaseStatusHandler.updateCaseStatus(cases);
   RaM_CaseStatusHandler.CaseStatus cStatus = new RaM_CaseStatusHandler.CaseStatus();
   RaM_CaseStatusHandler.CaseStatusResponce cRes = new RaM_CaseStatusHandler.CaseStatusResponce();
   //System.assert(csrList != null);
  }
 }
}