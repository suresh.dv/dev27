public with sharing class SortableSunriseUnit implements Comparable {
	public String unitName {get; set;}
	public String parentName {get; set;}
	
	public SortableSunriseUnit(String unitName, String parentName) {
		this.unitName = unitName;
		this.parentName = parentName;
	}
	
	
	/****************************************************
	* The sorter will sort units in the order specified below.
	* Where "00 => CR 1A" implies unitName = "00" and parentName="CR 1A".
	*
	* CR 1A
	* 00 => CR 1A
	* 10 => CR 1A
	* 20 => CR 1A
	* 30 => CR 1A
	* 40 => CR 1A
	* 50 => CR 1A
	* 60 => CR 1A
	* 70 => CR 1A
	* 80 => CR 1A
	* CR 1B
	* 01 => CR 1B
	* 11 => CR 1B
	* 21 => CR 1B
	* 31 => CR 1B
	* 41 => CR 1B
	* 51 => CR 1B
	* 61 => CR 1B
	* 71 => CR 1B
	* 81 => CR 1B
	* CR FF
	* OB => CR FF
	* OC => CR FF
	* OD => CR FF
	* OE => CR FF
	* OF => CR FF
	* OG => CR FF
	* OH => CR FF
	* OI => CR FF
	* OJ => CR FF
	* OK => CR FF
	* OL => CR FF
	*****************************************************/
	public  Integer compareTo(Object obj) {
		SortableSunriseUnit other = (SortableSunriseUnit) obj;
		
		if (this.unitName == null) {
			return -1;
		} else if (this.parentName == null) {
			 // This unit is a parent.
			 return (other.parentName == null) 
			 	? this.unitName.compareTo(other.unitName) 
			 		: this.unitName.compareTo(other.parentName);
		} else {
			// Sub-unit with a parent.
			if (other.parentName == null) {
				// Other unit is a parent.
				Integer compareResult = this.parentName.compareTo(other.unitName);
				return (compareResult == 0) ? 1 : compareResult;
			} else {
				// Other unit is a sub-unit with a parent.
				Integer compareResult = this.parentName.compareTo(other.parentName);
				return (compareResult == 0) ? this.unitName.compareTo(other.unitName) : compareResult;
			}
		}
	}
}