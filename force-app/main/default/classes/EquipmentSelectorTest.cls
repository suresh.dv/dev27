/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EquipmentSelector
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
private class EquipmentSelectorTest {

    @IsTest
    static void getEquipments_withoutParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment__c> records = new EquipmentSelector().getEquipments(null);
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEquipments_withEmptyParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment__c> records = new EquipmentSelector().getEquipments(new Set<Id>());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEquipments_withWrongParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment__c> records = new EquipmentSelector().getEquipments( new Set<Id> { UserInfo.getUserId() } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEquipments_withProperParam() {
        Integer expectedRecordCount = 1;
        Id equipmentId = EPD_TestData.createEquipmentForBatch();

        Test.startTest();
        List<Equipment__c> records = new EquipmentSelector().getEquipments( new Set<Id> { equipmentId } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

}