public class ATSQuoteGeneratorControllerOpp {
 String[] categories = new String[] {};
 public String categoriesList {
  get;
  set;
 } // same content as categories but separated with , when used pdfPage.getParameters().put it accepts primitive types
 String[] customers = new String[] {};
 public String opportunityList {
  get;
  set;
 }
 public String idTZ {
  get;
  set;
 }
 public Boolean reportSuccess {
  get;
  set;
 }
 public Date quoteDate{
  get;
  set;
 } 

 //Map<String, String> categoriesMap  = new Map<String, String>();
 Map < String, String > customersMap = new Map < String, String > ();
 public List < SelectOption > CategoryOptions = new List < SelectOption > ();
 List < SelectOption > CustomerOptions = new List < SelectOption > ();

 private final Opportunity tender;

 // The extension constructor initializes the private member
 // variable acct by using the getRecord method from the standard
 // controller.
 public ATSQuoteGeneratorControllerOpp(ApexPages.StandardController Controller) {
  reportSuccess = false;
  this.tender = (Opportunity) Controller.getRecord();
  // select product categories
  // [From Opportunity] - items of product category added on Opportunity
  // split multi-select picklist 

  for (opportunity opp: [select id, cpm_Product_Category__c from Opportunity where id =: tender.Id]) {
   List < String > parsedCategories = new List < String > ();

   if (opp.cpm_Product_Category__c != null) {
    parsedCategories = opp.cpm_Product_Category__c.split(';');
   }

   List < Integer > tmp = new Integer[] {
    64
   };
   categoriesList = '';

   System.debug('parsedCategories: ' + parsedCategories.size());
   System.debug('parsedCategories: ' + opp.cpm_Product_Category__c);

   for (string ctg: parsedCategories) {
    tmp[0]++;
    String convertedChar = string.fromCharArray(tmp);
    categories.add(ctg);
    System.debug(ctg);
   }
  }

  // [From Picklist] - fill the checkboxes list directly with pick-list values 
  Schema.DescribeFieldResult fieldResult = Opportunity.cpm_Product_Category__c.getDescribe();
  List < Schema.PicklistEntry > ple = fieldResult.getPicklistValues();
  for (Schema.PicklistEntry f: ple) {
   CategoryOptions.add(new SelectOption(f.getLabel(), f.getValue()));
  }

  // select customers  
  for (Cpm_Customer_Opportunity__c objCustOpp: [select cpm_AccountLookup__r.id, cpm_AccountLookup__r.name from Cpm_Customer_Opportunity__c where cpm_OpportunityLookup__c =: tender.id]) //select Account__r.id, Acccount__r.name from Customer where id = :tender.id
  {
   CustomerOptions.add(new SelectOption(objCustOpp.cpm_AccountLookup__c, objCustOpp.cpm_AccountLookup__r.name));
   customersMap.put(objCustOpp.cpm_AccountLookup__c, objCustOpp.cpm_AccountLookup__r.name);
   customers.add(objCustOpp.cpm_AccountLookup__c);
  }
  
  System.debug('quoteDate###' +quoteDate);
  if(quoteDate == null){
   quoteDate = System.Today();
  }
 }

 public PageReference GenerateQuotes() {

  opportunityList = tender.id;
  this.reportSuccess = false;

  for (String accountID: customers) {

   PageReference pdfPage;

   pdfPage = new PageReference('/apex/ATSQuoteTemplateV2Opp');

   pdfPage.getParameters().put('tenderId', tender.id);
   pdfPage.getParameters().put('categoriesList', categoriesList);
   pdfPage.getParameters().put('accountId', accountID);
   pdfPage.getParameters().put('quoteDate', String.valueOf(quoteDate));
   // pdfPage.getParameters().put('opportunityList',opportunityList);             
   // pdfPage.setRedirect(false);
   // generate the pdf blob
   Blob pdfBlob;

   if (Test.IsRunningTest()) {
    pdfBlob = Blob.valueOf('UNIT.TEST'); // Casts the specified String to a Blob.   
   } else {
    try {
     pdfBlob = pdfPage.getContent();
    } catch (Exception e) {
     System.debug('This is Error' + e);
     ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,
      'There was an error generating quote for customer \'' + customersMap.get(accountID) + '\'.' +
      ' Please review Product and Freight data associated with the opportunity.'));

     return null;
    }
   }

   // create the attachment against the quote
   Attachment attachment = new Attachment();
   attachment.OwnerId = UserInfo.getUserId();
   attachment.ParentId = tender.id; // the record the file is attached to
   attachment.name = customersMap.get(accountID) + '.pdf';
   attachment.IsPrivate = false;
   attachment.body = pdfBlob;

   //insert the attachment
   try {
    insert attachment;
   } catch (DMLException e) {
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));
    return null;
   }

   reportSuccess = true;
   ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Attachment uploaded successfully. Click on Close button to get back to Opportunity record.'));
  }
  //new ApexPages.StandardController(tender).view();
  //a1mJ0000000NCtJ#a1mJ0000000NCtJ_RelatedNoteList_target
  /*Pagereference pg = new PageReference('/'+tender.id+'#' + tender.id + '_RelatedNoteList_target');
    pg.setRedirect(true);*/
  reportSuccess = true;
  return null;
 }

 public pagereference BackMethod() {
  Pagereference pg = new Pagereference('/' + tender.id);
  pg.setRedirect(true);
  
  return pg;
 }
 // property get categories for checkboxes
 public List < SelectOption > getCategoryItems() {
  return CategoryOptions;
 }
 // property return selected categories from checkboxes
 public String[] getSelectedCategories() {
  return categories;
 }
 // property set selected categories in checkboxes
 public void setSelectedCategories(String[] items) {
  this.categories = items;
  for (string s: items) {
   categoriesList += s + ',';
  }
 }
 // property get customers for checkboxes
 public List < SelectOption > getCustomers() {
  return CustomerOptions;
 }
 // property set selected customers in checkboxes    
 public String[] getSelectedCustomers() {
  return customers;
 }
 // property set selected customers from checkboxes
 public void setSelectedCustomers(String[] items) {
  this.customers = items;
 }
 
}