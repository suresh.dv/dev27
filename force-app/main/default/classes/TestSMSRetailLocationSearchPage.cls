// ********************************************************************
// Test class for SMS Retail Location Search page
//

@isTest
private class TestSMSRetailLocationSearchPage {
    

    // ================================================================
    // Search
    // Given that I have pre-existing list of retail locations
    // When I filter for following retail locations
    // | Active? | Cardlock Type         |
    // | Yes     | Husky Route Commander |
    // Then I should get following retail locations
    // | Active? | Cardlock Type         | Location Name |
    // | Yes     | Husky Route Commander | INSLEY Husky  |
    // | Yes     | Husky Route Commander | ...           | 
    @isTest static void testSearch() {

        Account acct = new Account(Name = 'TestAcct');
        insert acct;

        List<Retail_Location__c>testRecords = 
            new List<Retail_Location__c>{
                new Retail_Location__c(Account__c = acct.Id,
                    Active__c = 'Yes', 
                    Cardlock_Type__c = 'Husky Route Commander'),
                new Retail_Location__c(Account__c = acct.Id,
                    Active__c = 'Yes', 
                    Cardlock_Type__c = 'Husky Route Commander')
            };
        insert testRecords;

        Retail_Location__c dummyLocation = new Retail_Location__c(Account__c = acct.Id);
        insert dummyLocation;

        SMSRetailLocationSearchPageCtrl ctrl = 
            new SMSRetailLocationSearchPageCtrl();

        ctrl.filterLocation.Active__c = 'Yes';
        ctrl.filterLocation.Cardlock_Type__c = 
            'Husky Route Commander';
        ctrl.filterLocation.Address__c = '1234';
        ctrl.filterLocation.Cardlock__c = true;
        ctrl.filterLocation.Location_Number__c = '123213';
        ctrl.filterLocation.Carwash__c = true;
        ctrl.filterLocation.Carwash_Type__c = 'Brush/Softcloth, Touchless, Wand Wash';
        ctrl.filterLocation.Active__c = 'YES';
        ctrl.filterLocation.Trade_Class__c = 'SSERV';

        ctrl.search();
    }

    // ================================================================
    // Next Page
    // Given that I have pre-existing list of retail locations
    // When I am on the first page of the result,
    // Then I should be able to go to the next page
    @isTest static void testNextPage() {
        List<Retail_Location__c>testRecords = 
            new Retail_Location__c[20];

        Account acct = new Account(Name = 'TestAcct');
        insert acct;

        Retail_Location__c dummyLocation = new Retail_Location__c(Account__c = acct.Id, Active__c = 'YES');
        insert dummyLocation;

        SMSRetailLocationSearchPageCtrl ctrl = 
            new SMSRetailLocationSearchPageCtrl();


        System.assert(!ctrl.getHasNext());
        ctrl.next();
        System.assertEquals(1, ctrl.getLocations().size());

    }

    // ================================================================
    // Previous Page
    // Given that I have pre-existing list of retail locations
    // When I am on the first page of the result,
    // Then I should be able to go to the next page
    @isTest static void testPrevPage() {
        List<Retail_Location__c>testRecords = 
            new Retail_Location__c[20];

        Account acct = new Account(Name = 'TestAcct');
        insert acct;

        Retail_Location__c dummyLocation = new Retail_Location__c(Account__c = acct.Id, Active__c = 'YES');
        insert dummyLocation;

        SMSRetailLocationSearchPageCtrl ctrl = 
            new SMSRetailLocationSearchPageCtrl();


        System.assert(!ctrl.getHasPrevious());
        ctrl.previous();
        System.assertEquals(1, ctrl.getLocations().size());

    }

    // ================================================================
    // Test coverage for miscllaneous functions.
    @isTest static void testMisc() {
        Account acct = new Account(Name = 'TestAcct');
        insert acct;

        Retail_Location__c dummyLocation = new Retail_Location__c(Account__c = acct.Id);
        insert dummyLocation;

        SMSRetailLocationSearchPageCtrl ctrl = 
            new SMSRetailLocationSearchPageCtrl();

        ctrl.getFilters();
        ctrl.getResultFields();
        ctrl.Reset();
    }
}