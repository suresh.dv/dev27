/*
* Author: Kishore Chandolu
* Name: HCVC_TankMapControllerTest
* Purpose: This is the test class for HCVC_TankMapController.
* Created Date: 23rd September 2014
*/


/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class HCVC_TankMapControllerTest {
		
   public static Id crudeRecordTypeId {get {if(crudeRecordTypeId==null)crudeRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Crude Pipeline').getRecordTypeId(); return crudeRecordTypeId;}set;}
   public static Id overviewRecordTypeId {get {if(overviewRecordTypeId==null)overviewRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Overview').getRecordTypeId(); return overviewRecordTypeId;}set;}
   public static Id refineryTypeId {get {if(refineryTypeId==null)refineryTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery').getRecordTypeId(); return refineryTypeId;}set;}
   public static Id refineryUnitTypeId {get {if(refineryUnitTypeId==null)refineryUnitTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Unit').getRecordTypeId(); return refineryUnitTypeId;}set;}
   public static Id refineryRacksTypeId {get {if(refineryRacksTypeId==null)refineryRacksTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Racks').getRecordTypeId(); return refineryRacksTypeId;}set;}
   public static Id tankTypeId {get {if(tankTypeId==null)tankTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Tank').getRecordTypeId(); return tankTypeId;}set;}  
   public static Id geojsonTypeId {get {if(geojsonTypeId==null)geojsonTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Geojson').getRecordTypeId(); return geojsonTypeId;}set;}  
	   


       static testMethod void constructorTest() {

            Asset__c parentrefinery=new Asset__c(Name='Prince George Refinery',Lat__c=53.928333,Long__c=-122.696390,
                                                  RecordTypeId=refineryTypeId,Details__c='Prince George Refinery',Refinery_Overview__c='',
                                                  Refinery_History__c='');
            insert parentrefinery;
            Id parentId = parentrefinery.Id;
            
            // Make sure it was inserted properly
            parentrefinery = [SELECT Name, Details__c FROM Asset__c WHERE Id=: parentId];
            System.assertEquals(parentrefinery.Name, 'Prince George Refinery');

            Asset__c asset=new Asset__c(Name='Crude Unit (CRUD - UNIT #:10)',Parent_Refinery_Asset__c=parentrefinery.Id,Substance__c='',
                                        Tank_Number__c='1',order__c=1,Lat__c=53.927528,Long__c=-122.698284,RecordTypeId=refineryUnitTypeId,
                                        Details__c='Crude Unit (CRUD – UNIT #: 10)',Address__c='');

                insert asset;
				// Make sure it was inserted properly
                asset = [SELECT Name, Details__c FROM Asset__c WHERE Details__c='Crude Unit (CRUD – UNIT #: 10)'];
                System.assertEquals(asset.Name, 'Crude Unit (CRUD - UNIT #:10)');
                
           Asset__c tankasset= new Asset__c(Name='Tank1',Details__c='Tank1 details',Lat__c=53.927528,Long__c=-122.698284,Volume_BB__c='',
                    CEPA_Tank__c='',Material__c='',SHAPE_Area__c='',SHAPE_Length__c=0.0,Parent_Refinery_Asset__c=parentrefinery.Id,
                    Substance__c='',Tank_Number__c='1',order__c=1,Temperatur__c='',UN_CAS_Number__c='',
                    BBLS_FOOT__c=0.0, BBLS_INCH__c=0.0, Dimensions__c='', HEEL_BBLS__c=0.0,RecordTypeId=tankTypeId,
                    HEEL_LTRS__c=0.0, LTRS_FOOT__c=0.0, LTRS_INCH__c=0.0, MAXIMUM_FILL_FEET__c=0.0,
                    MAX_VOLUME_BBL__c=0.0, MAX_VOLUME_M3__c=0.0, Notes__c='', Product__c='', Sales_Tanks__c='');

                    insert tankasset;
                    
                    // Make sure it was inserted properly
                    tankasset = [SELECT Name, Details__c FROM Asset__c WHERE Details__c='Tank1 details'];
                    System.assertEquals(tankasset.Name, 'Tank1');

                    ApexPages.currentPage().getParameters().put('id',asset.Id);

                    HCVC_TankMapController tmc=new HCVC_TankMapController();
                    tmc.goBackToMap();
        }


}