/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for ELG_ShiftHandover_Endpoint
History:        mbrimus 2019-07-15 - Created.
*************************************************************************************************/
@IsTest
public class ELG_ShiftHandover_EndpointTest {

	@IsTest
	static void whenUserHasNoSettingForElog_WillNotReturnData() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			result = new ELG_ShiftHandoverService().getBaseShiftHandoverData();
		}
		Test.stopTest();

		// THEN
		System.assertEquals(1, result.resultObjects.size());
		Map<String, Object> resultsMap = (Map<String, Object>) result.resultObjects.get(0);

		System.assertEquals(null, resultsMap.get(ELG_Constants.USER_SETTINGS_KEY));
		System.assertNotEquals(null, resultsMap.get(ELG_Constants.AVAILABLE_POSTS_KEY));

		ELG_User_Setting__c userSetting = (ELG_User_Setting__c) resultsMap.get(ELG_Constants.USER_SETTINGS_KEY);
		System.assertEquals(null, userSetting);

		List<ELG_Post__c> availablePosts = (List<ELG_Post__c>) resultsMap.get(ELG_Constants.AVAILABLE_POSTS_KEY);
		System.assertEquals(0, availablePosts.size());

		Boolean isShiftEngineer = (Boolean) resultsMap.get(ELG_Constants.IS_SHIFTENGINEER_KEY);
		System.assertEquals(false, isShiftEngineer);

	}

	@IsTest
	static void whenUserHasSettingsForElogButNotWorkingTheShift_WillReturnData() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = [SELECT Id FROM Field__c LIMIT 1].Id;
			insert settings;

			result = ELG_ShiftHandover_Endpoint.getBaseShiftHandoverData();
		}
		Test.stopTest();

		// THEN
		System.assertEquals(1, result.resultObjects.size());
		Map<String, Object> resultsMap = (Map<String, Object>) result.resultObjects.get(0);

		System.assertNotEquals(null, resultsMap.get(ELG_Constants.USER_SETTINGS_KEY));
		System.assertNotEquals(null, resultsMap.get(ELG_Constants.AVAILABLE_POSTS_KEY));

		ELG_User_Setting__c userSetting = (ELG_User_Setting__c) resultsMap.get(ELG_Constants.USER_SETTINGS_KEY);
		System.assertNotEquals(null, userSetting);

		List<ELG_Post__c> availablePosts = (List<ELG_Post__c>) resultsMap.get(ELG_Constants.AVAILABLE_POSTS_KEY);
		System.assertEquals(5, availablePosts.size());
	}

	@IsTest
	static void whenUserHasSettingsForElogAndWorkingTheShift_WillReturnShiftData() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = [SELECT Id FROM Field__c LIMIT 1].Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = [SELECT Id FROM ELG_Post__c LIMIT 1].Id;
			insert assignement;

			result = ELG_ShiftHandover_Endpoint.getBaseShiftHandoverData();
		}
		Test.stopTest();

		// THEN
		System.assertEquals(1, result.resultObjects.size());
		Map<String, Object> resultsMap = (Map<String, Object>) result.resultObjects.get(0);

		System.assertNotEquals(null, resultsMap.get(ELG_Constants.USER_SETTINGS_KEY));
		System.assertNotEquals(null, resultsMap.get(ELG_Constants.AVAILABLE_POSTS_KEY));
	}

	@IsTest
	static void whenUserIsShiftEngineer_heWillBeAbleToSeeButton() {

		// GIVEN
		User reviewer = ELG_TestDataFactory.createReviewerUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(reviewer) {
			result = ELG_ShiftHandover_Endpoint.getBaseShiftHandoverData();
		}
		Test.stopTest();

		// THEN
		Map<String, Object> resultsMap = (Map<String, Object>) result.resultObjects.get(0);
		Boolean isShiftEngineer = (Boolean) resultsMap.get(ELG_Constants.IS_SHIFTENGINEER_KEY);
		System.assertEquals(true, isShiftEngineer);
	}

	@IsTest
	static void whenUserHasSettingsForElogAndIsIncomingOperator_WillReturnHandoverDocument() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = [SELECT Id FROM Field__c LIMIT 1].Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = [SELECT Id FROM ELG_Post__c LIMIT 1].Id;
			assignement.Shift_Status__c = ELG_Constants.SHIFT_HANDOVER_IN_PROGRESS;
			insert assignement;

			ELG_Shift_Handover__c handover = [
					SELECT Id, Incoming_Operator__c
					FROM ELG_Shift_Handover__c
					WHERE Shift_Assignement__c = :assignement.Id
			];
			handover.Status__c = ELG_Constants.HANDOVER_IN_PROGRESS;
			//handover.Incoming_Operator__c = operator.Id;
			update handover;

			result = ELG_ShiftHandover_Endpoint.findDataForTakePost();
		}
		Test.stopTest();

		// THEN
		System.assertEquals(1, result.resultObjects.size());
		List<ELG_Shift_Handover__c> results = (List<ELG_Shift_Handover__c>) result.resultObjects.get(0);
		System.assertNotEquals(null, results);
		System.assertEquals(1, results.size());
	}

	@IsTest
	static void whenIsNotIncomingOperator_noDocumentWillBeReturned() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = [SELECT Id FROM Field__c LIMIT 1].Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = [SELECT Id FROM ELG_Post__c LIMIT 1].Id;
			assignement.Shift_Status__c = ELG_Constants.SHIFT_HANDOVER_IN_PROGRESS;
			insert assignement;

			result = ELG_ShiftHandover_Endpoint.findDataForTakePost();
		}
		Test.stopTest();

		// THEN
		System.assertEquals(1, result.resultObjects.size());
		ELG_Shift_Handover__c handover = (ELG_Shift_Handover__c) result.resultObjects.get(0);

		System.assertEquals(null, handover);
	}

	@IsTest
	static void whenGettingAllLogEntriesForPostInFacility_logEntriesShouldBeReturned() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			insert assignement;

			ELG_TestDataFactory.createLogEntries(10,
					amu,
					post,
					assignement, true);

			result = ELG_ShiftHandover_Endpoint.getAllLogEntriesForPostInFacility(post.Id, amu.Id);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(1, result.resultObjects.size());
		List<ELG_Log_Entry__c> handover = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);

		System.assertEquals(10, handover.size());
	}

	@IsTest
	static void whenGettingAllQuestionCategories_allCategoriesAreReturned() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			result = ELG_ShiftHandover_Endpoint.getAllQuestionCategories();
		}
		Test.stopTest();

		// THEN
		System.assertEquals(1, result.resultObjects.size());
		List<ELG_Handover_Category__c> questionCategories = (List<ELG_Handover_Category__c>) result.resultObjects.get(0);

		System.assertEquals(10, questionCategories.size());
	}

	@IsTest
	static void whenUserSignOffHandover_statusIsUpdatedAndNewShiftCreated() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_RequestResult result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			assignement.Shift_Status__c = ELG_Constants.SHIFT_NEW;
			insert assignement;

			ELG_Shift_Handover__c handover = [
					SELECT Id,
							Incoming_Operator__c,
							Shift_Assignement__c,
							Shift_Assignement__r.Post__c
					FROM ELG_Shift_Handover__c
					WHERE Shift_Assignement__c = :assignement.Id
			];
			handover.Status__c = ELG_Constants.HANDOVER_IN_PROGRESS;
			handover.Signed_Off__c = Datetime.now();
			update handover;

		}
		Test.stopTest();

		// THEN
		List<ELG_Shift_Assignement__c> oldShift = [
				SELECT
						Id
				FROM ELG_Shift_Assignement__c
				WHERE Shift_Status__c = :ELG_Constants.SHIFT_HANDOVER_COMPLETE
		];
		List<ELG_Shift_Assignement__c> newShift = [
				SELECT Id,
						Primary__c
				FROM ELG_Shift_Assignement__c
				WHERE Shift_Status__c = :ELG_Constants.SHIFT_NEW
		];

		System.assertEquals(2, [SELECT Id FROM ELG_Shift_Assignement__c].size());
		System.assertEquals(1, oldShift.size());
		System.assertEquals(1, newShift.size());
		System.assertEquals(null, newShift.get(0).Primary__c);
	}

	@IsTest
	static void whenAddingLogEntryAndTextIsLarge_errorIsDisplayed() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_RequestResult result;

		ELG_Log_Entry__c entry;
		ELG_Shift_Assignement__c assignement;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			insert assignement;

			entry = ELG_TestDataFactory.createLogEntry(false);
			entry.Post__c = post.Id;
			entry.User_Log_Entry__c = 'A wonderful serenity has taken possession of my entire soul, like these sweet ' +
					'mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence ' +
					'in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so ' +
					'absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be ' +
					'incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater ' +
					'artist than now. When, while the lovely valley teems with vapour around me, and the meridian sun strikes' +
					' the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner ' +
					'sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth,' +
					' a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and' +
					' grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of ' +
					'the Almighty, who formed us in his own image, and the breath of that universal love which bears and sustains ' +
					'us, as it floats around us in an eternity of bliss; and then, my friend, when darkness overspreads my eyes, ' +
					'and heaven and earth seem to dwell in my soul and absorb its power, like the form of a beloved mistress, ' +
					'then I often think with longing, Oh, would I could describe these conceptions, could impress upon paper ' +
					'all that is living so full and warm within me, that it might be the mirror of my soul, as my soul is the ' +
					'mirror of the infinite God! O my friend -- but it is too much for my strength -- I sink under the weight ' +
					'of the splendour of these visions! A wonderful serenity has taken possession of my entire soul, like these ' +
					'sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in' +
					' this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed ' +
					'in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of ' +
					'drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now.' +
					' When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface' +
					' of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, ' +
					'I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, ' +
					'a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks,' +
					' and grow familiar with the countless indescribable forms of the insects and flies, then I feel the ' +
					'presence of the Almighty, who formed us in his own image, and the breath of that universal love which ' +
					'bears and sustains us, as it floats around us in an eternity of bliss; and then, my friend, when darkness' +
					' overspreads my eyes, and heaven and earth seem to dwell in my soul and absorb its power, like the form ' +
					'of a beloved mistress, then I often think with longing, Oh, would I could describe these conceptions, ' +
					'could impress upon paper all that is living so full and warm within me, that it might be the mirror of my' +
					' soul, as my soul is the mirror of the infinite God! O my friend -- but it is too much for my strength --' +
					' I sink under the weight of the splendour of these visions! A wonderful serenity has taken possession of my ' +
					'entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the ' +
					'charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear f' +
					'riend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be ' +
					'incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist ' +
					' now. When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper ' +
					'surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary' +
					', I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, a' +
					' thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks,' +
					' and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence' +
					' of the Almighty, who formed us in his own image, and the breath of that universal love which bears and sust' +
					'ains us, as it floats around us in an eternity of bliss; and then, my friend, when darkness overspreads my' +
					' eyes, and heaven and earth seem to dwell in my soul and absorb its power, like the form of a beloved mis' +
					'tress, then I often think with longing, Oh, would I could describe these conceptions, could impress upon ' +
					'paper all that is living so full and warm within me, that it might be the mirror of my soul, as my soul is ' +
					'the mirror of the infinite God! O my friend -- butch for my ' +
					'ch for my strch for my strch for my strch for my strch for my strstrch for my str it is too much for my strength -- I s';

			entry.Operating_Field_AMU__c = amu.Id;
			entry.Shift_Assignement__c = assignement.Id;

			result = ELG_ShiftHandover_Endpoint.saveLogEntry(entry);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(false, result.success);
		System.assertEquals(1, result.errors.size());
		System.assertEquals(true, result.errors.get(0).contains(System.Label.ELG_User_Log_Entry));
	}

	@IsTest
	static void whenAddingLogEntryAndPostIsNotSelected_errorIsDisplayed() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_RequestResult result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			insert assignement;

			ELG_Log_Entry__c entry = ELG_TestDataFactory.createLogEntry(false);

			result = ELG_ShiftHandover_Endpoint.saveLogEntry(entry);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(false, result.success);
		System.assertEquals(1, result.errors.size());
		System.assertEquals(true, result.errors.get(0).contains(System.Label.ELG_Log_Post));
	}

	@IsTest
	static void whenAddingLogEntryAndAMUIsNotSelected_errorIsDisplayed() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_RequestResult result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			insert assignement;

			ELG_Log_Entry__c entry = ELG_TestDataFactory.createLogEntry(false);
			entry.Post__c = post.Id;

			result = ELG_ShiftHandover_Endpoint.saveLogEntry(entry);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(false, result.success);
		System.assertEquals(1, result.errors.size());
		System.assertEquals(true, result.errors.get(0).contains(System.Label.ELG_Log_Entry_AMU));
	}

	@IsTest
	static void whenAddingLogEntryAndShiftIsNotActive_errorIsDisplayed() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_RequestResult result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			assignement.Shift_Status__c = ELG_Constants.SHIFT_HANDOVER_COMPLETE;
			insert assignement;

			ELG_Log_Entry__c entry = ELG_TestDataFactory.createLogEntry(false);
			entry.Post__c = post.Id;
			entry.Operating_Field_AMU__c = amu.Id;

			result = ELG_ShiftHandover_Endpoint.saveLogEntry(entry);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(false, result.success);
		System.assertEquals(1, result.errors.size());
		System.assertEquals(true, result.errors.get(0).contains(System.Label.ELG_Post_With_No_Active_Shift));
	}

	@IsTest
	static void whenAddingLogEntryForDifferentShiftThenActiveOne_shiftChangesToCorrectOne() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_RequestResult result;

		ELG_Log_Entry__c entry;
		ELG_Shift_Assignement__c assignement;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			insert assignement;

			entry = ELG_TestDataFactory.createLogEntry(false);
			entry.Post__c = post.Id;
			entry.Operating_Field_AMU__c = amu.Id;
			entry.Shift_Assignement__c =
					ELG_TestDataFactory.createShiftAssignement(ELG_TestDataFactory.createPost('Test', true), true).Id;

			result = ELG_ShiftHandover_Endpoint.saveLogEntry(entry);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(true, result.success);
		System.assertEquals(entry.Shift_Assignement__c, assignement.Id);
	}

	@IsTest
	static void whenAddingLogEntry_entrySuccessfullyInserted() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_RequestResult result;

		ELG_Log_Entry__c entry;
		ELG_Shift_Assignement__c assignement;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			insert assignement;

			entry = ELG_TestDataFactory.createLogEntry(false);
			entry.Post__c = post.Id;
			entry.Operating_Field_AMU__c = amu.Id;
			entry.Shift_Assignement__c = assignement.Id;

			result = ELG_ShiftHandover_Endpoint.saveLogEntry(entry);
		}
		Test.stopTest();

		Id recordType = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();

		// THEN
		System.assertEquals(true, result.success);
		System.assertEquals(entry.User__c, operator.Id);
		System.assertEquals(entry.RecordTypeId, recordType);
	}

	@IsTest
	static void whenFindHandoverDocumentByShift_handoverDocumentIsReturned() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			insert assignement;

			result = (HOG_CustomResponseImpl) ELG_ShiftHandover_Endpoint.findHandoverDocumentByShift(assignement.Id);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(true, result.success);
		System.assertEquals(1, result.resultObjects.size());
		ELG_Shift_Handover__c handover = (ELG_Shift_Handover__c) result.resultObjects.get(0);
		System.assertNotEquals(null, handover);
	}

	@IsTest
	static void whenGettingSteamShiftWithMissingShiftEngineer_ShiftIsReturned() {

		// GIVEN
		User operator = ELG_TestDataFactory.createLogEntryUser();
		HOG_CustomResponseImpl result;

		// WHEN
		Test.startTest();
		System.runAs(operator) {
			ELG_Post__c post = [SELECT Id FROM ELG_Post__c LIMIT 1];
			Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = operator.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = operator.Id;
			assignement.Post__c = post.Id;
			assignement.Shift_Status__c = ELG_Constants.SHIFT_HANDOVER_IN_PROGRESS;
			insert assignement;

			post.Operating_Field_AMU__c = amu.Id;
			post.Shift_Engineer_Review_Required__c = true;
			update post;

			result = (HOG_CustomResponseImpl) ELG_ShiftHandover_Endpoint.getSteamShiftWithMissingShiftEngineer(amu.Id);
		}
		Test.stopTest();

		// THEN
		System.assertEquals(true, result.success);
		System.assertEquals(2, result.resultObjects.size());
        List<ELG_Shift_Assignement__c> handover = (List<ELG_Shift_Assignement__c>) result.resultObjects[0];
		System.assertEquals(false, handover.isEmpty());
	}

	@IsTest
	private static void shiftEngineerOnMoreShifts() {
		HOG_CustomResponseImpl response;
		User shiftEngineer = ELG_TestDataFactory.createShiftEngineerUser();
		List<ELG_Post__c> post = [SELECT Id FROM ELG_Post__c LIMIT 2];
		Field__c amu = [SELECT Id FROM Field__c LIMIT 1];

		List<String> shiftIds = new List<String>();

		Test.startTest();
		System.runAs(shiftEngineer) {


			// User set his setting
			ELG_User_Setting__c settings = ELG_TestDataFactory.creteUserSetting(false);
			settings.User__c = shiftEngineer.Id;
			settings.Operating_Field_AMU__c = amu.Id;
			insert settings;

			// User sets him self on as primary on one shift
			ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
			assignement.Primary__c = shiftEngineer.Id;
			assignement.Post__c = post[0].Id;
			insert assignement;

			ELG_Shift_Assignement__c assignement2 = ELG_TestDataFactory.createShiftAssignement(false);
			assignement2.Primary__c = shiftEngineer.Id;
			assignement2.Post__c = post[1].Id;
			insert assignement2;

			post[0].Operating_Field_AMU__c = amu.Id;
			post[0].Shift_Engineer_Review_Required__c = true;
			post[1].Operating_Field_AMU__c = amu.Id;
			post[1].Shift_Engineer_Review_Required__c = true;
			update post;

			List<ELG_Shift_Assignement__c> shifts = [SELECT Id, Shift_Engineer__c FROM ELG_Shift_Assignement__c];
			shiftIds.add(shifts[0].Id);
			shiftIds.add(shifts[1].Id);

			response = ELG_ShiftHandover_Endpoint.saveShiftEngineer(shiftIds, shiftEngineer.Id);
            List<ELG_Shift_Assignement__c> shiftsAfterUpdate = [SELECT Id, Shift_Engineer__c FROM ELG_Shift_Assignement__c];
            System.assertNotEquals(null, response.resultObjects);
            System.assertEquals(1, response.resultObjects.size());
            System.assertEquals(true, response.success);
            System.assertEquals(shiftEngineer.Id, shiftsAfterUpdate[0].Shift_Engineer__c);
            System.assertEquals(shiftEngineer.Id, shiftsAfterUpdate[1].Shift_Engineer__c);

			try {
                shiftIds.add(post[0].Id);
                response = ELG_ShiftHandover_Endpoint.saveShiftEngineer(shiftIds, shiftEngineer.Id);
                System.assertEquals(null, response.resultObjects);
                System.assertEquals(false, response.success);
			} catch (Exception ex) {
                System.assertEquals(ex.getMessage(), response.errors[0]);
			}

		}
		Test.stopTest();
	}

	@TestSetup
	private static void testSetup() {
		ELG_TestDataFactory.createPosts('PostName', 5, true);
		List<ELG_Handover_Category__c> categories = ELG_TestDataFactory.createQuestionCategories(10, true);
		ELG_TestDataFactory.createQuestionsForCategory('test', 10, categories.get(0), true);
	}
}