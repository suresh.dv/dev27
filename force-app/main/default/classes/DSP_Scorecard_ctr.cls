public class DSP_Scorecard_ctr {
    
	public String strVal1 {get{return 'Timely Incident Investigation';}}
    public String strVal2 {get{return 'ROCE';}}
    public String strVal3 {get{return 'Total Crude Throughput';}}
    public String strVal4 {get{return 'Emergency Response Exercises Completed';}}
    public String strVal5 {get{return 'EBITDA';}}
    public String strVal6 {get{return 'Total Pipeline Throughput';}}
    public String strVal7 {get{return 'On-time Action Item Completion';}}
    public String strVal8 {get{return 'Net Earnings';}}
    public String strVal9 {get{return 'Total Recordable Injury Rate';}}
    public String strVal10 {get{return 'OPEX';}}
    public String strVal11 {get{return 'Lost Time Incidents';}}
    public String strVal12 {get{return 'CAPEX';}}
    public String strVal13 {get{return 'Motor Vehicle Accidents';}}
    public String strVal14 {get{return 'Operating Cash Flow';}}
    public String strVal15 {get{return 'Reportable Spill Volume';}}
    
    public boolean getmapDSP01_ScorecardNoEmpty() {return mapDSP01_Scorecard.size() > 0 ;}
    
    public Map<String,DSP01_Scorecard__c> mapDSP01_Scorecard {
        get{
            mapDSP01_Scorecard = new Map<String,DSP01_Scorecard__c>(); 
            try{
                List<DSP01_Scorecard__c> lstDSPscorecard = new List<DSP01_Scorecard__c>([SELECT DSP01_dt_CalDay__c,DSP01_nu_Status__c,DSP01_nu_Direction__c,DSP01_tx_KPI__c,DSP01_fx_Arrow__c,DSP01_fx_Symbol__c  FROM DSP01_Scorecard__c ]);
                for(DSP01_Scorecard__c DSP:lstDSPscorecard){
                    mapDSP01_Scorecard.put(DSP.DSP01_tx_KPI__c, DSP);
                }
                               
            }catch(Exception ex){
                system.debug('ERROR DSP_Scorecard_ctr.mapDSP01_Scorecard: '+ex);
            }
            return mapDSP01_Scorecard;
        }set;
    }
    
}