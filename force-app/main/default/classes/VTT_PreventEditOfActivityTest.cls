@isTest
private class VTT_PreventEditOfActivityTest {
	enum PortalType { CSPLiteUser, PowerPartner, PowerCustomerSuccess, CustomerSuccess }

    @isTest
    static void controller_Test()

    {                                        
    	UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
		Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
		
		User portalAccountOwner = new User(
			UserRoleId = portalRole.Id,
	    	ProfileId = profile1.Id,
			Username = System.now().millisecond() + 'test2@test.com.test',
	    	Alias = 'superman',
			Email='portal.owner@huskyenergy.com.test',
			EmailEncodingKey='UTF-8',
			Firstname='Portal',
			Lastname='Owner',
			LanguageLocaleKey='en_US',
			LocaleSidKey='en_US',
			TimeZoneSidKey='America/Chicago'
		);
		Database.insert(portalAccountOwner);

		System.runAs ( portalAccountOwner ) {
			User puTradesman = getPortalUser(PortalType.PowerPartner, null,'HOG_Vendor_Community_VTT_User', true);
			User puSupervisor = getPortalUser(PortalType.PowerPartner, null,'HOG_Vendor_Community_VTT_Vendor_Supervisor', true);

	        HOG_Maintenance_Servicing_Form__c wo = VTT_TestDataFactory.createWorkOrder(true);
	        Field__c amu = HOG_TestDataFactory.createAMU('test amu',true);
	        Work_Order_Activity__c woa = VTT_TestDataFactory.createWorkOrderActivity('New', 'Test woa', wo.Id , amu.Id, true);
	        
			User standardTestUser = HOG_TestDataFactory.createUser('NonVendor', 'NonCommunity', 'user');
			Profile standardHOGProfile = [Select Id from Profile where Name='Standard HOG – Maintenance Vendor User'];
	        standardTestUser.ProfileId = standardHOGProfile.Id;
			PermissionSet psVTTUser = [SELECT Id FROM PermissionSet WHERE Name = 'HOG_VTT_User'];
	        PermissionSetAssignment psaStandardUser = new PermissionSetAssignment();
		    psaStandardUser.PermissionSetId = psVTTUser.Id;
		    psaStandardUser.AssigneeId = standardTestUser.Id;
	    	Database.insert(psaStandardUser);
	        update standardTestUser;
			
		    //-- End setup of data - Start Test --//
		        
		    Test.startTest();

		    System.runAs( standardTestUser ) {
		    	PageReference pageRef = Page.VTT_PreventEditOfWorkOrder;
		        pageRef.getparameters().put('id', woa.Id);
		        pageRef.getParameters().put('retURL', '/a/a/a/a'+ woa.Id);		    
		        Test.setCurrentPage(pageRef);
		        ApexPages.StandardController stdController = new ApexPages.StandardController(woa);
		        VTT_PreventEditOfActivity ext = new  VTT_PreventEditOfActivity(stdController);
		        PageReference r = ext.redirect();
		        List<Apexpages.Message> errorMsgs = ApexPages.getMessages();
				System.assertEquals(0, errorMsgs.size());
		        Map<String,String> pageParams = r.getParameters();
		        String retURL = pageParams.get('retURL');
		        retURL = retURL.replace('/VendorCommunity', '');
		        r.getparameters().put('retURL', retURL);

			    PageReference expectedPageReference = new PageReference('/'+ woa.Id+'/e?retURL=%2Fa'+woa.Id+ '&nooverride=1'); 
			    System.assertEquals(expectedPageReference.getURL(), r.getURL());

		    }

		    System.runAs( puSupervisor ) {
				PageReference pageRef = Page.VTT_PreventEditOfActivity;
		        pageRef.getparameters().put('id', woa.Id);
		        pageRef.getParameters().put('retURL', '/a/a/a/a/a/a/a/a/a/a/a/a/VendorCommunity/'+ woa.Id);		    
		        Test.setCurrentPage(pageRef);	        
		        ApexPages.StandardController stdController = new ApexPages.StandardController(woa);
		        VTT_PreventEditOfActivity ext = new  VTT_PreventEditOfActivity(stdController);
				PageReference r = ext.redirect();
				List<Apexpages.Message> errorMsgs = ApexPages.getMessages();
				System.assertEquals(1, errorMsgs.size());
		        Try {
		        	System.assertEquals(null,r.getURL());
	        	}Catch (Exception e) {
	        		Boolean expectedExceptionThrown =  e.getMessage().contains('Attempt to de-reference a null object') ? true : false;
					System.AssertEquals(expectedExceptionThrown, true);
	        	}
		        
		    }

		    System.runAs( puTradesman ) {
				PageReference pageRef = Page.VTT_PreventEditOfActivity;
		        pageRef.getparameters().put('id', woa.Id);
		        pageRef.getParameters().put('retURL', '/a/a/a/a/a/a/a/a/a/a/a/a/VendorCommunity/'+ woa.Id +'/e?retURL=%2F'+'VendorCommunity/a'+woa.Id+'&nooverride=1');	    
		        Test.setCurrentPage(pageRef);
		        ApexPages.StandardController stdController = new ApexPages.StandardController(woa);
		        VTT_PreventEditOfActivity ext = new  VTT_PreventEditOfActivity(stdController);
		        PageReference r = ext.redirect();
		        List<Apexpages.Message> errorMsgs = ApexPages.getMessages();
				System.assertEquals(1, errorMsgs.size());
		        Try {
		        	System.assertEquals(null,r.getURL());
	        	}Catch (Exception e) {
	        		Boolean expectedExceptionThrown =  e.getMessage().contains('Attempt to de-reference a null object') ? true : false;
					System.AssertEquals(expectedExceptionThrown, true);
	        	}
		    }

		    Test.stopTest();            
    	}
    }
    
    public static User getPortalUser(PortalType portalType, User userWithRole, String permissionSetApiName, Boolean doInsert) {
    
        /* Make sure the running user has a role otherwise an exception 
           will be thrown. */
        if(userWithRole == null) {   
            
            if(UserInfo.getUserRoleId() == null) {

                UserRole r = new UserRole(name = System.now().millisecond() + 'TEST ROLE');
                Database.insert(r);
                
                userWithRole = new User(alias = 'hasrole', email='userwithrole@roletest1.com', userroleid = r.id,
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
                                    localesidkey='en_US', profileid = UserInfo.getProfileId(), 
                                    timezonesidkey='America/Los_Angeles', username='userwithrole@testorg.com');
            } else {
                userWithRole = new User(Id = UserInfo.getUserId(), UserRoleId = UserInfo.getUserRoleId());
            }
            
            System.assert(userWithRole.userRoleId != null, 'This test requires the target org to have at least one UserRole created. Please create a user role in this organization and try again.');
        }

        Account a;
        Contact c;
        System.runAs(userWithRole) {

            a = new Account(name = System.now().millisecond() + 'TEST ACCOUNT');
            Database.insert(a);
            
            c = new Contact(AccountId = a.id, lastname = 'lastname');
            Database.insert(c);

        }
        
        /* Get any profile for the given type.*/
        Profile p = [select id 
                      from profile 
                     where usertype =:portalType.name() 
                     limit 1];   
        
        String testemail = System.now().millisecond() + '.puserTest@huskyvttportal.com';
        User pu = new User(profileId = p.id, username = testemail, email = testemail, 
                           emailencodingkey = 'UTF-8', localesidkey = 'en_US', 
                           languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', 
                           alias='cspu', lastname=System.now().millisecond() + 'lastname', contactId = c.id);
        
        

        if(doInsert) {
            Database.insert(pu);
            Id vttCommunityPsId = [SELECT Id FROM PermissionSet WHERE Name = :permissionSetApiName].Id;
	        PermissionSetAssignment psa = new PermissionSetAssignment();
		    psa.PermissionSetId = vttCommunityPsId;
		    psa.AssigneeId = pu.Id;
		    Database.insert(psa);
        }

        return pu;
    }
 }