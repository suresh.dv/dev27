/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class containing all methods and business logic regarding EPD_Cylinder_Information__c SObject
Test Class:     EPD_CylinderInformationServiceTest
History:        jschn 2019-06-26 - Created. - EPD R1
*************************************************************************************************/
public with sharing class EPD_CylinderInformationService {

    private static final String CLASS_NAME = String.valueOf(EPD_CylinderInformationService.class);

    /**
     * Method that builds list of Cylinder Information structures based on provided List of Cylinder Information records.
     * Structures are required for proper functioning of EPD Forms and PDF.
     *
     * @param cylinders
     *
     * @return List<EPD_FormStructureCylinder>
     */
    public List<EPD_FormStructureCylinder> getCylindersWrapped(List<EPD_Cylinder_Information__c> cylinders) {
        List<EPD_FormStructureCylinder> cylindersWrapped = new List<EPD_FormStructureCylinder>();

        System.debug(CLASS_NAME + ' -> getCylindersWrapped. Cylinders to wrap: ' + JSON.serialize(cylinders));

        for(EPD_Cylinder_Information__c cylinder : cylinders) {
            cylindersWrapped.add(
                    (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(cylinder)
            );
        }

        System.debug(CLASS_NAME + ' -> getCylindersWrapped. Wrapped cylinders: ' + JSON.serialize(cylindersWrapped));

        return cylindersWrapped;
    }

}