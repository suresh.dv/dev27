/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class that contains supporting methods to easy work with Custom Permissions.
Test Class:     HOG_CustomPermissionServiceTest
History:        jschn 2019-02-01 - Created.
*************************************************************************************************/
public without sharing class HOG_CustomPermissionService {

    private static final String CLASS_NAME = String.valueOf(HOG_CustomPermissionService.class);

    public static Boolean isUserInCustomPermission(Set<String> customPermissionNames) {
        try {
            Map<Id, CustomPermission> customPermissionsById = new Map<Id, CustomPermission> ([
                    SELECT Id, DeveloperName
                    FROM CustomPermission
                    WHERE DeveloperName IN :customPermissionNames
            ]);

            List<SetupEntityAccess> setupEntities = [
                    SELECT SetupEntityId
                    FROM SetupEntityAccess
                    WHERE SetupEntityId IN :customPermissionsById.keySet()
                    AND ParentId IN (
                            SELECT PermissionSetId
                            FROM PermissionSetAssignment
                            WHERE AssigneeId = :UserInfo.getUserId()
                    )
            ];

            return setupEntities != null
                    && setupEntities.size() > 0;
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' -> isUserInCustomPermission: ' + ex.getMessage());
        }

        return false;
    }

}