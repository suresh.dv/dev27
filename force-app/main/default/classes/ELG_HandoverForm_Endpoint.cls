/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    Endpoint Class for Handover
 *  Test Class:		ELG_HandoverFrom_EndpointTest
 *  History:        Created on 5/28/2019   
 */

public with sharing class ELG_HandoverForm_Endpoint {
	private static final String CLASS_NAME = String.valueOf(ELG_HandoverForm_Endpoint.class);


	/**
	 * Returns responses to set component:
	 * for response[0] get queried Handover object,
	 * for response[1] get Wrapper of Log Entries (section ones with Questions), Handover Categories
	 * and Handover Category Questions,
	 * for response[2] Boolean value, if current User has Permission set for Review
	 *
	 * Used in: ELG_HandoverForm, ELG_ShiftHandover_HO_ViewForm
	 *
	 * @param handoverForm
	 * @param currentUserId
	 *
	 * @return handover object, wrapper of log entries, categories and questions, boolean
	 */
	@AuraEnabled
	public static HOG_CustomResponseImpl loadHandoverFromDatabase(String handoverForm, String currentUserId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			ELG_HandoverForm_ServiceHandler handler = new ELG_HandoverForm_ServiceHandler();
			response.addResult(handler.loadHandoverForm(handoverForm).resultObjects);
			response.addResult(handler.getAllHandoverLogEntries().resultObjects.get(0));
			response.addResult(handler.userCannotReview(currentUserId).resultObjects.get(0));
			System.debug(CLASS_NAME + ' DATA: ' + response);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' ex: ' + ex.getMessage());
			response.addError(ex);
		}
		return response;

	}

	/**
	 * First will get all active questions from Handover Category (passed from component based on what button was clicked)
	 *  and then prepare log entry for each question
	 *
	 * Used in: ELG_ShiftHandover_HO_GenericSection
	 *
	 * @param categoryName
	 *
	 * @return list of log entries
	 */
	@AuraEnabled
	public static HOG_CustomResponseImpl loadCategoryAndQuestions(String categoryName) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_HandoverForm_ServiceHandler().preparedLogEntries(categoryName);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' ex: ' + ex.getMessage());
			response.addError(ex);
		}

		return response;
	}

	/**
	 * Get log entries that were prepared and which are not null will get into the list of entries and get inserted
	 * into Salesforce with required Shift information
	 *
	 * Used in: ELG_ShiftHandover_HO_GenericSection
	 *
	 * @param listOfEntries
	 * @param shiftAssignment
	 *
	 * @return inserted log entries
	 */
	@AuraEnabled
	public static HOG_CustomResponseImpl logEntriesFromComponent(List<ELG_Log_Entry__c> listOfEntries, ELG_Shift_Assignement__c shiftAssignment) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_HandoverForm_ServiceHandler().logEntriesToInsert(listOfEntries, shiftAssignment);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' ex: ' + ex.getMessage());
			response.addError(ex);
		}
		System.debug(CLASS_NAME + ' server response: ' + response);

		return response;
	}

	/**
	 * Query Handover before updating to get most actual value due to Handover Status, get Id of Incoming Operator for update
	 * and current User for validation that only Primary can Sign off and Pass Handover
	 *
	 *Used in: ELG_HandoverForm_Status_New
	 *
	 * @param HOFormId
	 * @param incomingOperatorId
	 * @param currentUserId
	 *
	 * @return handover object
	 */
	@AuraEnabled
	public static HOG_RequestResult updateHandoverAndStatus(String HOFormId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_HandoverForm_ServiceHandler().updateHandoverIncOpAndStatus(HOFormId);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' ex: ' + ex.getMessage());
			response.addError(ex);
		}

		return response;
	}

	/**
	 * Accept of Handover form and change Status,
	 * current User Id is for validation that only Incoming Operator can accept passed Handover
	 *
	 * Used in: ELG_HandoverForm_Status_InProgress
	 *
	 * @param HOForm
	 * @param currentUserId
	 *
	 * @return handover object
	 */
	@AuraEnabled
	public static HOG_RequestResult acceptHandover(ELG_Shift_Handover__c HOForm, String currentUserId, String traineeId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			System.debug(CLASS_NAME + ' trainee: ' +traineeId);
			response = new ELG_HandoverForm_ServiceHandler().acceptHandoverAndChangeStatus(HOForm, currentUserId, traineeId);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' ex: ' + ex.getMessage());
			response.addError(ex);
		}

		return response;

	}

	/**
	 * Query all Log Entries for the Shift from record type of Generic
	 *
	 * Used in: ELG_HandoverForm
	 *
	 * @param shiftAssignmentId
	 *
	 * @return list of log entries
	 */
	@AuraEnabled
	public static HOG_CustomResponseImpl loadGenericLogEntries(String shiftAssignmentId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_HandoverForm_ServiceHandler().genericLogEntries(shiftAssignmentId);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' Log Entries ex: ' + ex.getMessage());
			response.addError(ex);
		}
		return response;
	}

	/**
	 * Updates Handover fields based on User selection
	 *
	 * Used in: ELG_HandoverForm_Status_Completed
	 *
	 * @param HOForm
	 * @param sectionsForUpdate
	 * @param currentUser
	 *
	 * @return handover object
	 */
	@AuraEnabled
	public static HOG_RequestResult updateHandoverReview(ELG_Shift_Handover__c HOForm,
			List<String> sectionsForUpdate,
			String currentUser) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_HandoverForm_ServiceHandler().updateReviewOnHandover(HOForm,
					sectionsForUpdate,
					currentUser);
			System.debug(CLASS_NAME + ' UPDATE DATA: ' + response);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' update ex: ' + ex.getMessage());
			response.addError(ex);
		}

		return response;
	}

	/**
	 * Updates Reviewed by Shift Handover, only after handover from was Pass and before Accepting
	 *
	 * Used in: ELG_HandoverForm_Status_InProgress
	 *
	 * @param HOForm
	 * @param currentUser
	 *
	 * @return handover object
	 */
	@AuraEnabled
	public static HOG_RequestResult updateHandoverReviewByShiftEngineer(ELG_Shift_Handover__c HOForm, String currentUser) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response = new ELG_HandoverForm_ServiceHandler().updateReviewOnHandover(HOForm, currentUser);
			System.debug(CLASS_NAME + ' UPDATE DATA: ' + response);
		} catch (Exception ex) {
			System.debug(CLASS_NAME + ' update ex: ' + ex.getMessage());
			response.addError(ex);
		}

		return response;
	}

}