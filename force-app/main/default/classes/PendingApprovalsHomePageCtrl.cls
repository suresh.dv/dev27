public with sharing class PendingApprovalsHomePageCtrl 
{
    @AuraEnabled    
    public static List<TableWrapper> getUserPendingProcess()
    {
        Id userId = userInfo.getUserId();
        
        String sSoql = '';
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        List<ProcessInstance> prcInstList = new List<ProcessInstance>();
        List<TableWrapper> twList = new List<TableWrapper>();
        
        Set<Id> targetObjIdSet = new Set<Id>();
        Set<Id> targetIdSet = new Set<Id>();
        
        Map<Id,ProcessInstance> targetIdProcessMap = new Map<Id,ProcessInstance>();
        Map<Id,String> sObjectIdNameMap = new Map<Id,String>();
        
        System.debug('Current User Id - ' + userId);
        
        
        prcInstList = [SELECT Id, TargetObjectId, TargetObject.Name, TargetObject.Type, Createddate, (SELECT Id, CreatedDate, OriginalActor.Name, Actor.Name FROM Workitems where ActorId =: userId or OriginalActorId =: userId)FROM ProcessInstance where status = 'Pending'];
        
        system.debug('prcInstList - ' + prcInstList);
        
        for(ProcessInstance prc : prcInstList)
        {
            if(prc.WorkItems.size()>0)
            {
                TableWrapper tw = new TableWrapper();
                tw.RelatedTo = prc.TargetObject.Name;
                tw.Name = prc.TargetObject.Name;
                
                Schema.SObjectType gd = Schema.getGlobalDescribe().get(prc.TargetObject.Type); 
                Schema.DescribeSobjectResult a11= gd.getDescribe();
                tw.Type = a11.getLabel();
                
                
                tw.AssignedTo = prc.WorkItems[0].OriginalActor.Name;
                tw.ActualApprover = prc.WorkItems[0].Actor.Name;
                tw.DateSubmitted = prc.WorkItems[0].CreatedDate;
                tw.RelatedToId = baseUrl+'/lightning/r/'+ prc.WorkItems[0].Id +'/view';
                tw.NameId = baseUrl+'/lightning/r/'+ prc.TargetObjectId +'/view';
                
                tw.AssignedToId = baseUrl+'/lightning/r/'+ prc.WorkItems[0].OriginalActorId +'/view';
                tw.ActualApproverId = baseUrl+'/lightning/r/'+ prc.WorkItems[0].ActorId +'/view';
                
                
                //system.debug(a11.getLabel());
                
                
                twList.add(tw);
            }
            
        }        
        
        return twList;
    }
    
    public class TableWrapper
    {
        @AuraEnabled public String RelatedTo;
        @AuraEnabled public String Name;
        @AuraEnabled public String Type;
        @AuraEnabled public String AssignedTo;
        @AuraEnabled public String ActualApprover;
        @AuraEnabled public DateTime DateSubmitted;
        @AuraEnabled public String RelatedToId;
        @AuraEnabled public String NameId;
        @AuraEnabled public String AssignedToId;
        @AuraEnabled public String ActualApproverId;
        
        public TableWrapper()
        {
            RelatedTo = '';
            Name = '';
            Type = '';
            AssignedTo = '';
            ActualApprover = '';
            DateSubmitted = System.now(); 
            RelatedToId = '';
            NameId = '';
            AssignedToId = '';
            ActualApproverId = '';
        }
        
    }
}