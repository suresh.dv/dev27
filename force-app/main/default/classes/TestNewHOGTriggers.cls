/*---------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Test class for HOG Triggers
----------------------------------------------------------------------------------------------------------*/  
@isTest
private class TestNewHOGTriggers 
{
    private static HOG_Service_Code_MAT__c serviceCodeMAT;
    private static HOG_Work_Order_Type__c workOrderType;
    private static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
    private static HOG_Service_Category__c serviceCategory;
    private static HOG_Service_Code_Group__c serviceCodeGroup;
    private static HOG_Notification_Type__c notificationType;
    private static HOG_Service_Required__c serviceRequiredList;
    private static HOG_User_Status__c userStatus;
    private static HOG_Service_Priority__c servicePriorityList;
    private static Business_Unit__c businessUnit;
    private static Business_Department__c businessDepartment;
    private static Operating_District__c operatingDistrict;
    private static Field__c field;
    private static Route__c route;
    private static Location__c location;        
    private static Facility__c facility;        
    private static Equipment__c equipment;
    private static Account account;
    private static Contact contact;
    private static Id recordTypeId;

    private static String SERVICECATEGORYCODE = 'MNT';  // Maintenance service category code
    private static String CATALOGUECODE = 'CATCODE';
    private static String NOTIFICATIONTYPECODE = 'WP';
    private static String ORDERTYPECODE = 'WP01';
    private static String MATCODE = 'TXN';
    private static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};
        
    @isTest
    static void RiglessServicing_Location_Test()
    {                
        //ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;         

        SetupTestData();

        final Well_Tracker__c wellTracker = WellTrackerTestData.createWellTracker(location.id, facility.Id, field.Id, 1, Datetime.valueOf('2013-05-20 12:00:00'), 'Producing', 'Engineering');
        insert wellTracker;
                
        final HOG_Service_Request_Notification_Form__c srnForm = new HOG_Service_Request_Notification_Form__c
            (
                Well_Tracker__c = wellTracker.Id,
                HOG_Work_Order_Type__c = workOrderType.Id,
                HOG_Notification_Type_Priority__c = notificationTypePriority[0].Id,
                Vendor_Company__c = account.Id,
                Work_Details__c = 'Test',
                Recurring_Start_Date__c = Date.today(),
                Recurring_End_Date__c = Date.today() + 10                
            );        
        insert srnForm;
        
        final HOG_Service_Time__c recordServiceTime = new HOG_Service_Time__c();
        final HOG_Vendor_Personnel__c recordVendorPersonnel = new HOG_Vendor_Personnel__c();
        //--

        System.debug('notificationType.Id:' + notificationType.Id);

        final HOG_Maintenance_Servicing_Form__c recordRiglessServicing = 
            new HOG_Maintenance_Servicing_Form__c 
                (
                    Name = 'Test Rigless', 
                    Service_Status__c = 'New Request',
                    HOG_Service_Request_Notification_Form__c = srnForm.id, 
                    Location__c = location.id,
                    Well_Tracker__c = wellTracker.id,
                    SAP_Generated_Work_Order_Number__c = true,
                    Unit_Number__c = 'Test',
                    HOG_Notification_Type__c = notificationType.Id
                );

        test.startTest();

        //--- Insert Work_Order__c record, testing RiglessServicingFormRecord trigger        
        insert recordRiglessServicing;
        
        //--- Insert Service_Time__c record, testing HOG_Rigless_Servicing_Form_Trigger trigger
        recordServiceTime.Work_Order__c = recordRiglessServicing.Id;
        recordServiceTime.Day__c = 'Day 1';
        recordServiceTime.Rig_On_Location__c = Datetime.valueOf('2013-06-19 12:00:00');
        recordServiceTime.Rig_Off_Location__c = Datetime.valueOf('2013-06-20 12:00:00');        
        insert recordServiceTime;
        
        //--- Insert Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        recordVendorPersonnel.Work_Order__c = recordRiglessServicing.Id;
        recordVendorPersonnel.Invoice_Category__c = 'Communication';
        recordVendorPersonnel.Vendor__c = account.Id;
        recordVendorPersonnel.On_Location_Time__c = Datetime.valueOf('2013-06-19 12:00:00');
        recordVendorPersonnel.Off_Location_Time__c = Datetime.valueOf('2013-06-20 12:00:00');        
        insert recordVendorPersonnel;
        //--

        //--- Update Work_Order__c record, testing RiglessServicingFormRecord and RiglessServicingFormUpdated triggers        
        recordRiglessServicing.Service_Status__c = 'Complete'; 
        recordRiglessServicing.Well_On_Production__c  = 'Yes';   
        recordRiglessServicing.Work_Details__c = 'Test1';
        update recordRiglessServicing;

        recordRiglessServicing.Service_Status__c = 'New Request'; 
        recordRiglessServicing.Work_Details__c = 'Test2';
        update recordRiglessServicing;
 
        serviceRequiredList.Validate_Pressure_Test__c = true;
        serviceRequiredList.Validate_Start_and_Stop_Time__c = true;
        update serviceRequiredList;
        
        recordRiglessServicing.Pressure_Test__c = 'test pressure';
        recordRiglessServicing.Start_Time__c = Datetime.valueOf('2013-06-19 11:40:00');
        recordRiglessServicing.Stop_Time__c = Datetime.valueOf('2013-06-19 12:30:00');
        recordRiglessServicing.Service_Status__c = 'Complete';   
        recordRiglessServicing.Well_On_Production__c  = 'No';               
        recordRiglessServicing.Supervised__c = false;

        recordRiglessServicing.Vendor_Company__c = account.id;        
        recordRiglessServicing.Work_Details__c = 'Test3';
        update recordRiglessServicing;
        
        //--- Update Work_Order__c record, testing RiglessServicingFormRecord and RiglessServicingFormUpdated triggers
        recordRiglessServicing.Service_Status__c = 'New Request';   
        recordRiglessServicing.Work_Details__c = 'Test4';
        update recordRiglessServicing;

        recordRiglessServicing.Work_Order_Number__c = '1234567A';
        recordRiglessServicing.Service_Status__c = 'Requires Reassignment';
        recordRiglessServicing.Work_Details__c = 'Test5';
        update recordRiglessServicing;        
        
        //--- Update  Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        update recordVendorPersonnel;

        //--- Update Service_Time__c record, testing ServiceTimeRecord and SetDayOfTheServiceAndDay1RigOnLocation triggers
        update recordServiceTime;

        //--- Delete Service_Time__c record, testing ServiceTimeRecord trigger
        delete recordServiceTime;

        //--- Delete  Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        delete recordVendorPersonnel;
        
        test.stopTest();
        
        final HOG_Maintenance_Servicing_Form__c insertedRecord = 
            [SELECT Name, Service_Status__c, Id FROM HOG_Maintenance_Servicing_Form__c WHERE Id = :recordRiglessServicing.Id];
                
        System.assertEquals(recordRiglessServicing.Id, insertedRecord.Id);                              
    }

    @isTest
    static void RiglessServicing_Facility_Test()
    {
        //ServiceRequestNotificationUtilities.executeTriggerCode = false;
        WellServicingUtilities.executeTriggerCode = false;
        MaintenanceServicingUtilities.executeTriggerCode = false;         

        SetupTestData();

        final Well_Tracker__c wellTracker = WellTrackerTestData.createWellTracker(location.id, facility.Id, field.Id, 1, Datetime.valueOf('2013-05-20 12:00:00'), 'Producing', 'Engineering');
        insert wellTracker;
        
        final HOG_Service_Request_Notification_Form__c srnForm = new HOG_Service_Request_Notification_Form__c
            (
                Well_Tracker__c = wellTracker.Id,
                HOG_Work_Order_Type__c = workOrderType.Id,
                HOG_Notification_Type_Priority__c = notificationTypePriority[0].Id,
                Vendor_Company__c = account.Id,
                Work_Details__c = 'Test',
                Recurring_Start_Date__c = Date.today(),
                Recurring_End_Date__c = Date.today() + 10                
            );        
        insert srnForm;

        System.debug('notificationType.Id:' + notificationType.Id);
        
        final HOG_Maintenance_Servicing_Form__c recordRiglessServicing = 
            new HOG_Maintenance_Servicing_Form__c 
                (
                    Name = 'Test Rigless', 
                    Service_Status__c = 'New Request',
                    HOG_Service_Request_Notification_Form__c = srnForm.id, 
                    Facility__c = facility.id,
                    Well_Tracker__c = wellTracker.id,
                    Unit_Number__c = 'Test',
                    HOG_Notification_Type__c = notificationType.Id
                );

        final HOG_Service_Time__c recordServiceTime = new HOG_Service_Time__c();
        final HOG_Vendor_Personnel__c recordVendorPersonnel = new HOG_Vendor_Personnel__c();
        //--

        test.startTest();

        //--- Insert Work_Order__c record, testing RiglessServicingFormRecord trigger        
        insert recordRiglessServicing;
        
        //--- Insert Service_Time__c record, testing ServiceTimeRecord and SetDayOfTheServiceAndDay1RigOnLocation triggers
        recordServiceTime.Work_Order__c = recordRiglessServicing.Id;
        recordServiceTime.Day__c = 'Day 1';
        recordServiceTime.Rig_On_Location__c = Datetime.valueOf('2013-06-19 12:00:00');
        recordServiceTime.Rig_Off_Location__c = Datetime.valueOf('2013-06-20 12:00:00');        
        insert recordServiceTime;        
        
        //--- Insert Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        recordVendorPersonnel.Work_Order__c = recordRiglessServicing.Id;
        recordVendorPersonnel.Invoice_Category__c = 'Communication';
        recordVendorPersonnel.Vendor__c = account.Id;
        recordVendorPersonnel.On_Location_Time__c = Datetime.valueOf('2013-06-19 12:00:00');
        recordVendorPersonnel.Off_Location_Time__c = Datetime.valueOf('2013-06-20 12:00:00');        
        insert recordVendorPersonnel;
        //--

        //--- Update Work_Order__c record, testing RiglessServicingFormRecord and RiglessServicingFormUpdated triggers        
        recordRiglessServicing.Service_Status__c = 'Complete'; 
        recordRiglessServicing.Well_On_Production__c  = 'Yes';
        update recordRiglessServicing;

        recordRiglessServicing.Service_Status__c = 'New Request'; 
        update recordRiglessServicing;
 
        serviceRequiredList.Validate_Pressure_Test__c = true;
        serviceRequiredList.Validate_Start_and_Stop_Time__c = true;
        update serviceRequiredList;
        
        recordRiglessServicing.Pressure_Test__c = 'test pressure';
        recordRiglessServicing.Start_Time__c = Datetime.valueOf('2013-06-19 11:40:00');
        recordRiglessServicing.Stop_Time__c = Datetime.valueOf('2013-06-19 12:30:00');
        recordRiglessServicing.Service_Status__c = 'Complete';   
        recordRiglessServicing.Well_On_Production__c  = 'No';               
        recordRiglessServicing.Supervised__c = false;
        
        recordRiglessServicing.Vendor_Company__c = account.id;        
        update recordRiglessServicing;
        
        //--- Update Work_Order__c record, testing HOG_Rigless_Servicing_Form_Trigger trigger
        recordRiglessServicing.Service_Status__c = 'New Request';   
        update recordRiglessServicing;

        recordRiglessServicing.Work_Order_Number__c = '1234567A';
        recordRiglessServicing.Service_Status__c = 'Requires Reassignment';
        update recordRiglessServicing;        
        
        //--- Update  Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        update recordVendorPersonnel;

        //--- Update Service_Time__c record, testing ServiceTimeRecord and SetDayOfTheServiceAndDay1RigOnLocation triggers
        update recordServiceTime;

        //--- Delete Service_Time__c record, testing ServiceTimeRecord trigger
        delete recordServiceTime;

        //--- Delete  Vendor_Personnel__c record, testing VendorPersonnelRecord trigger
        delete recordVendorPersonnel;

        delete srnForm;
        
        test.stopTest();
        
        final HOG_Maintenance_Servicing_Form__c insertedRecord = 
            [SELECT Name, Service_Status__c, Id FROM HOG_Maintenance_Servicing_Form__c WHERE Id = :recordRiglessServicing.Id];
                
        System.assertEquals(recordRiglessServicing.Id, insertedRecord.Id);                              
    }
    
    @isTest    
    static void MaintenanceServicing_Test()
    {       
        SetupTestData();
                                
        Test.startTest();

        // test HOG_Maintenance_Servicing_Form_Trigger 
        final HOG_Maintenance_Servicing_Form__c maintenanceRecord = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name',
            Functional_Location__c = 'Test FLOC',
            Work_Order_Number__c = '876543WA',
            Notification_Number__c = '876543NA',
            Notification_Type__c = NOTIFICATIONTYPECODE,
            Order_Type__c = ORDERTYPECODE,
            Priority_Number__c = 1,
            MAT_Code__c = MATCODE,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS',
            Equipment__c = equipment.Id,
            Notification_Priority_Number__c = '1',
            Work_Order_Priority_Number__c = '1',
            HOG_Notification_Type__c = notificationType.Id
        );

        // test before insert HOG_Maintenance_Servicing_Form_Trigger 
        insert maintenanceRecord;        
        final HOG_Maintenance_Servicing_Form__c insertedRecord = 
            [SELECT Id FROM HOG_Maintenance_Servicing_Form__c WHERE Id = :maintenanceRecord.Id];                    
        System.assertEquals(maintenanceRecord.Id, insertedRecord.Id);                              
       
        // test before update HOG_Maintenance_Servicing_Form_Trigger 
        maintenanceRecord.Plant_Section__c = 'SP';
        update maintenanceRecord;        
        final HOG_Maintenance_Servicing_Form__c updatedRecord = 
            [SELECT Plant_Section__c FROM HOG_Maintenance_Servicing_Form__c WHERE Id = :maintenanceRecord.Id];                    
        System.assertEquals('SP', updatedRecord.Plant_Section__c);                              
        
        // test before delete HOG_Maintenance_Servicing_Form_Trigger
        try 
        { 
            delete maintenanceRecord; 
        }
        catch(Exception e)
        {
            System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }        

        // test HOG_Maintenance_Servicing_Vendor__Trigger         
        final HOG_Maintenance_Servicing_Form__c maintenanceRecord2 = new HOG_Maintenance_Servicing_Form__c
        (
            Name = 'Test Name',
            Functional_Location__c = 'Test FLOC',
            Work_Order_Number__c = '776543WB',
            Notification_Number__c = '776543NB',
            Notification_Type__c = NOTIFICATIONTYPECODE,
            Order_Type__c = ORDERTYPECODE,
            Priority_Number__c = 1,
            MAT_Code__c = MATCODE,
            ALT_Confirmed__c = false,
            Plant_Section__c = 'PS',
            Equipment__c = equipment.Id,
            Notification_Priority_Number__c = '1',
            Work_Order_Priority_Number__c = '1',
            HOG_Notification_Type__c = notificationType.Id          
        );
        
        MaintenanceServicingUtilities.executeTriggerCode = false;   // do not to execute trigger         

        insert maintenanceRecord2;        
        final HOG_Maintenance_Servicing_Form__c insertedRecord2 = 
            [SELECT Id FROM HOG_Maintenance_Servicing_Form__c WHERE Id = :maintenanceRecord2.Id];                    
        System.assertEquals(maintenanceRecord2.Id, insertedRecord2.Id);                              
        
        final HOG_Maintenance_Servicing_Vendor__c vendorRecord = new HOG_Maintenance_Servicing_Vendor__c
        (
            Name = 'Test Name',
            Maintenance_Servicing_Form__c = maintenanceRecord2.Id,
            Activity_Status__c = 'Complete',
            Total_Travel_Times_Hrs__c = 1,
            Total_Personnel__c = 1,
            Start_Time__c = System.now(),
            Stop_Time__c = System.now(),
            Vendor__c = account.Id,
            Completed_By_Contact__c = contact.Id,
            Comments__c = 'Test Comment'
        );

        MaintenanceServicingUtilities.executeTriggerCode = true;
        // test before insert HOG_Maintenance_Servicing_Vendor__Trigger
        try 
        { 
            insert vendorRecord;        
        }
        catch(Exception e)
        {
            System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }

        Test.stopTest();                
    }


    @isTest
    static  void Location_Trigger_Test() {
        Field__c field = HOG_TestDataFactory.createAMU(true);
        Route__c route = HOG_TestDataFactory.createRoute(true);
        Location__c location = HOG_TestDataFactory.createLocation(route.Id, field.Id, true);
        Id oldLocationId = location.Id;
        Date dayToday = date.Today();
        location.recalculateFormulas();

        Test.startTest();
        location.GOR_Effective_Date__c = dayToday;
        update location;
        Id newLocationId = location.Id;
        Test.stopTest();

        System.assertEquals('N/A', location.Expired_GOR_Test_img__c, 'Flag status is different as it should be');
        System.assertEquals(newLocationId, oldLocationId, 'Just checking the ID, not much to check on update trigger in this test');

    }

    @isTest
    static  void Location_Trigger_Creation_Of_Expired_Gor_Test_Alert_Test() {

        HOG_Vent_Gas_Alert_Configuration__c customSettings= new HOG_Vent_Gas_Alert_Configuration__c();
        insert customSettings;
        Location__c location = HOG_VentGas_TestData.createLocationForAlert(true);
		location.recalculateFormulas();
        Date expiredGorTestDate = Date.today()-1100;

        Test.startTest();
        location.GOR_Effective_Date__c = expiredGorTestDate;
        update location;
		location.recalculateFormulas();
        Id alert = [SELECT Id FROM HOG_Vent_Gas_Alert__c].Id;
        Test.stopTest();

        System.assertEquals('YES', location.Expired_GOR_Test__c, 'Effective Date is not set right');
        System.assertEquals(4, location.Functional_Location_Category__c, 'Functional Location Category is not 4');
        System.assertEquals('OIL', location.Well_Type__c, 'Well type is not OIL');
        System.assert(String.isNotBlank(alert), 'Alert haven\t been created');
   }

	@isTest
	static void HOG_VG_Alert_Exemption_Trigger_Insert_And_Update_Test() {
		HOG_Vent_Gas_Alert_Configuration__c customSettings= new HOG_Vent_Gas_Alert_Configuration__c();
		insert customSettings;

		Date expiredExemptionDate = Date.Today()+365;

		User usr = HOG_VentGas_TestData.createUser('Mr', 'Mychal', 'TheMys');
		User usr2 = HOG_VentGas_TestData.createUser('Lucifer', 'Morningstar', 'Devil');
		Location__C location = HOG_VentGas_TestData.createLocation();
		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(location.Id, usr.Id, true);
		alert.Exemption_Approver__c = usr2.Id;
		update alert;

		Test.startTest();

		HOG_Vent_Gas_Alert_Exemption_Request__c exemption = new HOG_Vent_Gas_Alert_Exemption_Request__c();
		exemption.Vent_Gas_Alert__c = alert.Id;
		exemption.Well_Location__c = location.Id;
		exemption.Operations_Engineer__c = usr2.Id;
		exemption.Expiration_Date__c = expiredExemptionDate;
		exemption.Reason__c = 'Approve pls';
		insert exemption;
		System.assert(String.isNotBlank(exemption.Id), 'Record haven\t been inserted');

		HOG_Vent_Gas_Alert_Exemption_Request__c queryForExemption = [SELECT Id, Status__c FROM HOG_Vent_Gas_Alert_Exemption_Request__c];
		queryForExemption.Status__c = 'Approved';
		update  queryForExemption;

		HOG_Vent_Gas_Alert_Exemption_Request__c queryForExemptionAssert = [SELECT Id, Status__c
																			FROM HOG_Vent_Gas_Alert_Exemption_Request__c];
		System.assertEquals('Approved', queryForExemptionAssert.Status__c, 'Record haven\'t been updated.');
		Test.stopTest();



	}

    @isTest
    static void MaintenanceServicingTriggerDelete_Test()
    {
        SetupTestData();
        Date startTime = System.today();
        Date stopTime = System.today()+1;

        Test.startTest();

        // test HOG_Maintenance_Servicing_Form_Trigger
        final HOG_Maintenance_Servicing_Form__c maintenanceRecord = new HOG_Maintenance_Servicing_Form__c (
                Name = 'Test Name',
                Functional_Location__c = 'Test FLOC',
                Work_Order_Number__c = '876543WA',
                Notification_Number__c = '876543NA',
                Notification_Type__c = NOTIFICATIONTYPECODE,
                Order_Type__c = ORDERTYPECODE,
                Priority_Number__c = 1,
                MAT_Code__c = MATCODE,
                ALT_Confirmed__c = false,
                Plant_Section__c = 'PS',
                Equipment__c = equipment.Id,
                Notification_Priority_Number__c = '1',
                Work_Order_Priority_Number__c = '1',
                HOG_Notification_Type__c = notificationType.Id);

        insert maintenanceRecord;
        List<HOG_Maintenance_Servicing_Vendor__c> vendorList = new List<HOG_Maintenance_Servicing_Vendor__c>();
        vendorList.add(new HOG_Maintenance_Servicing_Vendor__c (
                Maintenance_Servicing_Form__c = maintenanceRecord.Id,
                Completed_By_Contact__c = contact.Id,
                Start_Time__c = startTime,
                Stop_Time__c = stopTime,
                Vendor__c = account.Id,
                Comments__c = 'Lets write something',
                Total_Personnel__c = 2,
                Activity_Status__c = 'Cancelled'));

        vendorList.add(new HOG_Maintenance_Servicing_Vendor__c (
                Maintenance_Servicing_Form__c = maintenanceRecord.Id,
                Completed_By_Contact__c = contact.Id,
                Start_Time__c = startTime,
                Stop_Time__c = stopTime,
                Vendor__c = account.Id,
                Comments__c = 'Lets write something',
                Total_Personnel__c = 2,
                Activity_Status__c = 'In Progress'));



        try {
            insert vendorList;
        } catch(DmlException ex) {
            System.assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
        MaintenanceServicingUtilities.executeTriggerCode = false;
        insert vendorList;
        MaintenanceServicingUtilities.executeTriggerCode = true;
        delete vendorList.get(0);

        try {
            delete vendorList.get(1);
        } catch(DmlException ex) {
            System.assert(ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
        Test.stopTest();
    }

    private static void SetupTestData()
    {
        //-- Begin setup of data needed for Service Request Notification --//                
        //-- Setup Service Category
        serviceCategory = new HOG_Service_Category__c
            (
                Name = 'Test Category',
                Service_Category_Code__c = SERVICECATEGORYCODE                                
            );
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;        
        //--

        //-- Setup Notification Type
        notificationType = new HOG_Notification_Type__c
            (
                HOG_Service_Category__c = serviceCategory.Id,
                Notification_Type__c = NOTIFICATIONTYPECODE,
                Order_Type__c = ORDERTYPECODE,
                Auto_Generate_Work_Order_Number__c = false                
            );
        insert notificationType;
        //--

        //-- Setup Service Code MAT
        serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', MATCODE, WORKORDERRECORDTYPE[0]);         
        insert serviceCodeMAT;        
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', false, false, false);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
        userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
        servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Service - Priority
        notificationTypePriority = new List<HOG_Notification_Type_Priority__c>
            {            
                (HOG_Notification_Type_Priority__c)
                    NotificationTypePriorityTestData.createNotificationTypePriority
                        (notificationType.Id, servicePriorityList.Id, false),
                (HOG_Notification_Type_Priority__c)
                    NotificationTypePriorityTestData.createNotificationTypePriority
                        (notificationType.Id, servicePriorityList.Id, true)
            };
        insert notificationTypePriority;
        //--

        //-- Setup Work Order Type
        workOrderType = WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true);
        insert workOrderType;
        //--

        //-- Setup data for objects
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

        businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

        operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

        field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;
        
        route = RouteTestData.createRoute('999');
        insert route;                  

        location = LocationTestData.createLocation('Test Location', route.Id, field.Id);        
        insert location;

        facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        

        account = AccountTestData.createAccount('Test Account', null);          
        insert account;

        contact = new Contact
            (
                AccountId = account.Id,
                LastName = 'Lastname',
                User__c = UserInfo.getUserId()
            );
        insert contact;    

        equipment = new Equipment__c
            (
                Name = 'Equipment Test',
                Location__c = location.Id,          
                Catalogue_Code__c = CATALOGUECODE
            );                
        insert equipment;        
        //--
    }    
}