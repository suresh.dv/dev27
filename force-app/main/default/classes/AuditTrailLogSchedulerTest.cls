@isTest 
public class AuditTrailLogSchedulerTest{
    static testMethod void testGenerateLogCsv(){
        List<SetupAuditTrail> audit = [SELECT CreatedDate, createdBy.username, Action, Section,  DelegateUser FROM SetupAuditTrail WHERE CreatedDate = LAST_N_DAYS:90];
        System.assertNotEquals(0, audit.size());
        
        Test.StartTest();
            Datetime d = Datetime.now().addMinutes(1);
            String sch = d.second()+' '+d.minute()+' '+d.hour()+' '+d.day()+' '+d.month()+' ? '+d.year();   
            
            String jobId = System.schedule('Download Audit Trail Unit test Job 1', sch, new AuditTrailLogScheduler());
            
            // Get the information from the CronTrigger API object
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                                FROM CronTrigger WHERE id = :jobId];          
                                
            // Verify the expressions are the same
            System.assertEquals(sch,  ct.CronExpression);
            
            SchedulableContext sc = null;
            AuditTrailLogScheduler s = new AuditTrailLogScheduler();
            s.execute(sc);         
            
            // Verify the job has not run
            System.assertEquals(0, ct.TimesTriggered);                                     
        
        Test.StopTest();  
    }
}