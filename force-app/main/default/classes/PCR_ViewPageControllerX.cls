public class PCR_ViewPageControllerX{

    public Change_Request__c pcr {get; set;}
    public Boolean viewMode {get; private set;}
    public Boolean viewAssessmentButton {get; private set;}
    public Boolean viewfuncaffct {get; private set;}
    public Boolean viewImpactAssessment {get; private set;}
    public Boolean viewSpaReview {get; private set;}
    public Boolean viewAddReviewers {get; private set;}
    public Boolean isAdmin {get; private set;}
    public Boolean isSPA {get; private set;}
    public Boolean isCC {get; private set;}
    public Boolean isPM {get; private set;}
    public Map<String,String> reviewFields {get; private set;}
    public Boolean showOth {get; set;}
    
    public PCR_ViewPageControllerX(ApexPages.StandardController controller) {

        List<string> fields = new List<String>();
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.Change_Request__c.FieldSets.getMap();    
        
        for (String fieldSetName : fieldSetMap.keySet()){
            for(Schema.FieldSetMember f : SObjectType.Change_Request__c.FieldSets.getMap().get(fieldSetName).getFields()){
                fields.add(f.getFieldPath());
            }        
        }                
                
        if(!Test.isRunningTest()) {
            controller.addFields(fields);   
        }  
        
        viewMode = true;        
        pcr = (Change_Request__c)controller.getRecord(); 
        
        //add CR impact assessment field to a map
        reviewFields = new Map<String,String>();

        try{
            for (Impact_Assessor__c a : [ Select Assessor__c, Impact_Assessment_Description__c, OPEX_Cost__c, TIC_Cost__c, Risk_Description__c,
                                            Impact_Assessment_Complete__c, Implementation_Plan__c, Required_Resources__c, Scheduled_Impact_Comments__c,
                                            Contract_Commercial_Impact__c, Regulatory_Implications__c, Change_Impact_Hours__c, TIC_Schedule_Impact__c,
                                            Cost_Impact__c, Requested_By__c from Impact_Assessor__c where Change_Request__c =: pcr.id limit 1 ]) {  
                User u = [select id,name from User where id =: a.Assessor__c];
                /*List<User> users = [select id,name from User where id =: a.Assessor__c];
                User u = new User();
                if( users.size() > 0 )
                    u = users[0];*/
                reviewFields.put('Requested By', a.Requested_By__c == null ? '' : a.Requested_By__c);                   
                reviewFields.put('Project Control Analyst', u.Name);
                reviewFields.put('Cost Impact', a.Cost_Impact__c == null ? '0' : '$' + String.valueOf(a.Cost_Impact__c));
                reviewFields.put('Change Impact Hours', a.Change_Impact_Hours__c == null ? '0' : String.valueOf(a.Change_Impact_Hours__c));
                //reviewFields.put('Contract Commercial Impact', a.Contract_Commercial_Impact__c == null ? '' : a.Contract_Commercial_Impact__c);                   
                reviewFields.put('Schedule Impact', a.Scheduled_Impact_Comments__c == null ? '' : a.Scheduled_Impact_Comments__c); 
                //reviewFields.put('Required Resources', a.Required_Resources__c == null ? '' : a.Required_Resources__c);                   
                //reviewFields.put('Regulatory Implications', a.Regulatory_Implications__c == null ? '' : a.Regulatory_Implications__c);   
                //reviewFields.put('Implementation Plan', a.Implementation_Plan__c == null ? '' : a.Implementation_Plan__c);               
                //reviewFields.put('Risk Description', a.Risk_Description__c == null ? '' : a.Risk_Description__c);            
                reviewFields.put('TIC Cost', a.TIC_Cost__c == null ? '0' : '$' + String.valueOf(a.TIC_Cost__c));
                reviewFields.put('TIC Schedule Impact', a.TIC_Schedule_Impact__c == null ? '0' : String.valueOf(a.TIC_Schedule_Impact__c));
                //reviewFields.put('OPEX Cost', a.OPEX_Cost__c == null ? '0' : '$' + String.valueOf(a.OPEX_Cost__c));  
            }     
        } catch(Exception exp) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error retrieving Impact assessment data '));            
        }          
         
        //track diffeent PCR roles
        isAdmin = isPCRAdmin(); 
        isSPA = isPCRSPA();
        isCC = isPCRCC();
        isPM = isPCRPM();         
         
        viewAssessmentButton =  pcr.Change_Status__c=='Verification' && !crHasAssessment() && (pcr.SPA__c == UserInfo.getUserId() || isAdmin) ? true:false;
        viewfuncaffct = pcr.Verification_Approved__c && pcr.Change_Status__c == 'Initiated' && (pcr.Change_Coordinator__c == UserInfo.getUserId() || isAdmin) ? true:false;
        //viewImpactAssessment = pcr.Change_Status__c=='Impact Assessment and Review' && crHasAssessment() && (pcr.SPA__c == UserInfo.getUserId() || isAdmin) ? true:false;    
        viewImpactAssessment = pcr.Change_Status__c=='Impact Assessment and Review' && crHasAssessment() &&  (pcr.SPA__c == UserInfo.getUserId() || isAdmin || canEditAssessment()) ? true:false;            
        viewSpaReview = pcr.Change_Status__c=='SPA Review' && !pcr.Add_Reviewers__c && (pcr.SPA__c == UserInfo.getUserId() || (isAdmin && !pcr.Add_Reviewers__c)) ? true:false;
        viewAddReviewers = pcr.Change_Status__c=='Review Recommendation' && pcr.Add_Reviewers__c && (pcr.SPA__c == UserInfo.getUserId() || (isAdmin && pcr.Add_Reviewers__c) ) ? true : false;                                      
    }
    
    public String[] getPcrFieldSets(){
        return new String[]{'Title_and_Description','Information'};        
    }  
    
    public String[] getPcrEditFieldSets(){
        return new String[]{'Title_and_Description','Information_Edit'};        
    }  
    
    /*dynamically display the change status image. File name should be equal to name of status*/
    public String getImageName(){
        
        String imgName = (( pcr.Change_Status__c != null && pcr.Change_Status__c != '' ) ? pcr.Change_Status__c.replace(' ', '') + '.png' : '' );
        
        return imgName;
    }
    
    public PageReference edit(){
        viewMode = false;
        return null; 
    }  
    
    public SelectOption[] getGroups(){
        SelectOption[] grps = new SelectOption[]{};
        grps.add(new SelectOption('','--None--'));
        
        try{
            for (Project_Area__c g : [select id, name from Project_Area__c order by name]) {  
                grps.add(new SelectOption(g.id, g.name));  
            }     
        } catch(Exception exp) {
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error retrieving Project area records '));            
        }  
        
        return grps;
    }
    
    public PageReference next() {
        PageReference ref = null;
        
        try{
            pcr.Add_Reviewers__c = true;
            
            update pcr;
            ApexPages.StandardController pcrController = new ApexPages.StandardController(pcr);
            pcrController.save();
            
        } catch(Exception exp) {
            System.debug('Exception : ' + exp.getMessage());
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error updating Change Request '));            
        }
        
        if(pcr.Bypass_Review__c) {
            try{
                pcr.Change_Status__c = 'Sent for Approval';
                update pcr;  
                             
            } catch(Exception exp) {                
                ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error updating Change Request ')); 
            }
            
            ref = new PageReference('/apex/Change_Request_View?id=' + pcr.id);
            
        } else {
            ref = new PageReference('/apex/PCR_Functional_Review?Change_Request=' + pcr.id);
        }
        
        return ref;
    }
    
    public PageReference assessment(){
        return new PageReference('/apex/PCR_Impact_Assessment?id=' + pcr.id);
    }
    
    public PageReference functional(){
        return new PageReference('/apex/PCR_Complete_Initiate?id=' + pcr.id);
    }
    
    public PageReference functional2(){
        return new PageReference('/apex/PCR_Verification?id=' + pcr.id);
    }    
    
    public PageReference impact(){
        return new PageReference('/apex/PCR_Impact_Assessment_View?Change_Request=' + pcr.id);
    }    
    
    public PageReference spaReview(){
        //return new PageReference('/apex/PCR_Bypass_Review?Change_Request=' + pcr.id);
        return new PageReference('/apex/PCR_Bypass_Review_View?Change_Request=' + pcr.id);
    }  
    
    public PageReference addReviewer(){
        return new PageReference('/apex/PCR_Functional_Review?Change_Request=' + pcr.id);
    }        
    
    private Boolean crHasAssessment() {
        List<Impact_Assessor__c> a = [select id from Impact_Assessor__c where Change_Request__c =: pcr.id];
        /*if(a.size() > 0){
            return true;
        }*/
        
        return true;
    }
    
    private Boolean isPCRAdmin(){
        Boolean pcrAdmin = false;
        
        //Check for Sys admin
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = PROFILE[0].Name;
        
        System.debug('MyProflieName ' + MyProflieName);
        
        if(MyProflieName  == 'System Administrator'){
            pcrAdmin = true;                
        }
        
        //Check for PCR admin
        PermissionSet ps = [select id, name from PermissionSet where name = 'Change_Management_Admin' limit 1];
        List<PermissionSetAssignment> psalist = [select id from PermissionSetAssignment where AssigneeId =:userinfo.getUserId() and PermissionSetId =:ps.id LIMIT 1];
        if(psalist.size()>0){
            pcrAdmin = true;
        }
        
        return pcrAdmin;
    }    
    
    //Look for SPA
    private Boolean isPCRSPA(){
        if(userinfo.getUserId() == pcr.SPA__c){
            return true;
        } else {
            return false;
        }        
    }
    
    //Look for Change coordinator
    private Boolean isPCRCC(){
        if(userinfo.getUserId() == pcr.Change_Coordinator__c){
            return true;
        } else {
            return false;
        }        
    }
    
    //Look for Project Manager
    private Boolean isPCRPM(){
        if(userinfo.getUserId() == pcr.Project_Manager__c){
            return true;
        } else {
            return false;
        }        
    }  
    
    //Grant edit access to impact assessor
    private Boolean canEditAssessment(){
        List<Impact_Assessor__c> assess = new List<Impact_Assessor__c>([select id, Assessor__c from Impact_Assessor__c where Change_Request__c =: pcr.id limit 1]);
        if(assess.size() > 0){
            assess.get(0).Assessor__c = userinfo.getUserId(); 
            return true;   
        } else {
            return false;
        }            
    }      
    
    //to Show Describe Other field
    
    public PageReference renderOther(){    
        if(pcr.Justification__c.contains('Other')){
            showOth = true;
        }else{
            showOth = false;
            pcr.Describe_Other__c = '';                  
        }      
        return null;  
    }    
}