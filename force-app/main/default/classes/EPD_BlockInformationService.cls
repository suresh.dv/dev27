/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class containing all methods and business logic regarding EPD_Block_Information__c SObject
Test Class:     EPD_BlockInformationServiceTest
History:        jschn 2019-05-25 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_BlockInformationService {

    private static final String CLASS_NAME = String.valueOf(EPD_BlockInformationService.class);

    /**
     * Method that builds structural classes for Block Information required for EPD Form and PDF functionality.
     * Works in bulk.
     *
     * @param blockInformation
     * @param engineConfig
     *
     * @return List<EPD_FormStructureBlock>
     */
    public List<EPD_FormStructureBlock> buildEngineBlocksForEPD(List<EPD_Block_Information__c> blockInformation,
            EPD_Engine__mdt engineConfig) {
        System.debug(CLASS_NAME + ' -> buildEngineBlocksForEPD. START.');

        List<EPD_FormStructureBlock> blockStructureRecords = new List<EPD_FormStructureBlock>();

        if(isBlockInfoListCountInRange(blockInformation)) {

            for(EPD_Block_Information__c blockInfo : blockInformation) {
                blockStructureRecords.add(
                        buildBlockStructureFrom(blockInfo, engineConfig)
                );
            }

        } else {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Unexpected_Record_Count,
                    new List<String> {
                            'Block Information',
                            ''
                    }
            );
        }

        System.debug(CLASS_NAME + ' -> buildEngineBlocksForEPD. END. blockStructureRecords: '  + JSON.serialize(blockStructureRecords));
        return blockStructureRecords;
    }

    /**
     * Method that builds structural classes for Block Information required for EPD Form and PDF Functionality.
     *
     * @param blockInfo
     * @param engineConfig
     *
     * @return EPD_FormStructureBlock
     */
    private EPD_FormStructureBlock buildBlockStructureFrom(EPD_Block_Information__c blockInfo,
            EPD_Engine__mdt engineConfig) {
        System.debug(CLASS_NAME + ' -> buildBlockStructureFrom. START.');

        EPD_FormStructureBlock blockStructure = (
                (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo)).setConfig(engineConfig);

        blockStructure.cylinders = new EPD_CylinderInformationService().getCylindersWrapped(blockInfo.Cylinder_Information__r);

        System.debug(CLASS_NAME + ' -> buildBlockStructureFrom. END.');
        return blockStructure;
    }

    /**
     * Method that calls selector to get Block Information records and map them by parent EPD record ID.
     *
     * @param ids
     *
     * @return Map<Id, List<EPD_Block_Information__c>>
     */
    public Map<Id, List<EPD_Block_Information__c>> getBlockInformationByEPDId(Set<Id> ids) {
        System.debug(CLASS_NAME + ' -> getBlockInformationByEPDId. START. Ids: ' + ids);

        List<EPD_Block_Information__c> blockInformation = EPD_DAOProvider.blockInformationDAO.getBlockInformationByEPDs(ids);

        Map<Id, List<EPD_Block_Information__c>> blockInformationByEPDId = new Map<Id, List<EPD_Block_Information__c>>();

        for(EPD_Block_Information__c blockInfo : blockInformation) {

            if(!blockInformationByEPDId.containsKey(blockInfo.Engine_Performance_Data__c)) {
                blockInformationByEPDId.put(blockInfo.Engine_Performance_Data__c, new List<EPD_Block_Information__c>());
            }
            blockInformationByEPDId.get(blockInfo.Engine_Performance_Data__c).add(blockInfo);

        }

        System.debug(CLASS_NAME + ' -> getBlockInformationByEPDId. END.');
        return blockInformationByEPDId;
    }

    /**
     * Method that checks whether number of Block Information records on provided list is within range.
     * Expected 1 or 2 records.
     *
     * @param blockInformation
     *
     * @return Boolean
     */
    private Boolean isBlockInfoListCountInRange(List<EPD_Block_Information__c> blockInformation) {
        return blockInformation != null
                && blockInformation.size() > 0
                && blockInformation.size() <= 2;
    }

}