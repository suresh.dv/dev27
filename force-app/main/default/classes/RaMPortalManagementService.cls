public inherited sharing class RaMPortalManagementService {

    public RaMPortalManagementService() {

    }

    public List<Case> getRaMOpenCasesByVendor(String vendor){
        return RaMPortalUtility.getRaMOpenCasesByVendor(vendor);
    }

    public Case getRaMCaseByCaseNumber(String caseNumber) {
        return RaMPortalUtility.getRaMCaseByCaseNumber(caseNumber);
    }

    public List<PortalUserWrapper> getPortalUsersByAccount(String userId) {
        List<PortalUserWrapper> portalUsers = new List<PortalUserWrapper>();

        Contact cont = getAccountByUser(userId);
        List<Contact> contacts = getContactsByAccount(cont.AccountId);
        Map<String, User> contactUsers = getContactUser(contacts);

        for(Contact con : contacts) {
            if(contactUsers.containsKey(con.Id)) {
                PortalUserWrapper portalUser = new PortalUserWrapper(con, contactUsers.get(con.Id));

                if(portalUser.userId != userId) {
                    portalUsers.add(portalUser);
                }
                
            } else {
                PortalUserWrapper portalUser = new PortalUserWrapper(con, null);
                portalUsers.add(portalUser);
            }
        }

        return portalUsers;
    }

    public List<CurrentAssignment> getAssignmentUsersByCase(String caseRecordId, String userId) {
        List<CurrentAssignment> assignments = new List<CurrentAssignment>();

        Map<String, RaM_Case_Assignment__c> assignmentMap = RaMPortalUtility.getAssignments(caseRecordId);

        Contact cont = getAccountByUser(userId);
        List<Contact> contacts = getContactsByAccount(cont.AccountId);
        Map<String, User> contactUsers = getContactUser(contacts);
        
        for(Contact con : contacts) {
            if(contactUsers.containsKey(con.Id)) {
                User usr = contactUsers.get(con.Id);

                if(usr.IsActive && assignmentMap.containsKey(usr.Id)) {
                    CurrentAssignment ca = new CurrentAssignment(usr.Name, usr.Id, true);
                    assignments.add(ca);
                } else if(usr.IsActive) {
                    CurrentAssignment ca = new CurrentAssignment(usr.Name, usr.Id, false);
                    assignments.add(ca);
                }
            }
        }

        return assignments;
    }

    public void activateOrCreateUser(String contactId, String isConverted, String userId, Boolean needPasswordReset, String userType) {
        if(Boolean.valueOf(isConverted)) {
            RaMPortalUtility.activateUser(userId, needPasswordReset);

        } else {
            Contact cont = [SELECT Id, FirstName, LastName, Email 
                            FROM Contact 
                            WHERE Id =:contactId];

            RaMPortalUtility.createPortalUser(cont, userType);            
        }
        
    }

    public void deactivatePortalUser(String userId) {

        if(UserInfo.getUserId() != userId) {
            RaMPortalUtility.deactivateUser(userId);
        }
        
    }

    public void updateContact(Contact cont) {
        RaMPortalUtility.updateContact(cont);
    }

    public void createContact(Contact cont) {
        RaMPortalUtility.createContact(cont);
    }    

    public void assignCaseToTechnician(String caseRecordId, List<String> technicians, String subject, String ticketNumber){
        List<User> techUsers = new List<User>();

        for(String technician : technicians) {
            User u = new User();
            u.Id = technician;

            techUsers.add(u);
        }

        Case kase = new Case();
        kase.Id = caseRecordId;
        kase.Subject = subject;
        kase.Ticket_Number_R_M__c = ticketNumber;

        RaMPortalUtility.createAndAssignCase(kase, techUsers);
    } 
    
    public List<CaseAssignment> getAssignmentsByCase(String caseNumber){
        List<CaseAssignment> caseAssignments = new List<CaseAssignment>();

        List<RaM_Case_Assignment__c> assignments = RaMPortalUtility.getAssignmentsByCase(caseNumber);

        for(RaM_Case_Assignment__c assignment : assignments) {
            CaseAssignment caseAssignment = new CaseAssignment(
                assignment.Assign_To__r.Name,
                assignment.Parent_Case__r.Status,
                assignment.Work_Start__c,
                assignment.Work_Completed__c,
                assignment.Assign_To__r.ContactId,
                assignment.Is_Primary_Assignment__c,
                assignment.Id );

            caseAssignments.add(caseAssignment);
        }

        return caseAssignments;
    }

    public List<CaseAssignment> getAssignmentByContact(String contactId) {
        List<CaseAssignment> assignments = new List<CaseAssignment>();

        List<RaM_Case_Assignment__c> caseAssignments = RaMPortalUtility.getAssignmentByContact(contactId);

        for(RaM_Case_Assignment__c caseAssignment : caseAssignments) {
            CaseAssignment assignment = new CaseAssignment(
                caseAssignment.Parent_Case__r.Id,
                caseAssignment.Parent_Case__r.Ticket_Number_R_M__c,
                caseAssignment.Parent_Case__r.Subject,
                caseAssignment.Parent_Case__r.Status,
                caseAssignment.Work_Start__c,
                caseAssignment.Work_Completed__c,
                caseAssignment.Parent_Case__r.CaseNumber,
                caseAssignment.Is_Primary_Assignment__c,
                caseAssignment.Id
            );

            assignments.add(assignment);
        }

        return assignments;
    }

    public List<OpenActivity> getOpenActivityList(String technicianId) {
        List<OpenActivity> activities = new List<OpenActivity>();

        List<RaM_Case_Assignment__c> assignments = RaMPortalUtility.getOpenActivityList(technicianId);

        for(RaM_Case_Assignment__c assignment : assignments) {
            SiteLocation site = new SiteLocation(assignment.Parent_Case__r.LocationNumber__r.Name, 
                                        assignment.Parent_Case__r.LocationNumber__r.LocationName__c, 
                                        null, null, null, null, null, null, null, null);
            Ticket ticket = new Ticket(assignment.Parent_Case__r.Id, 
                                        assignment.Parent_Case__r.Ticket_Number_R_M__c, 
                                        assignment.Parent_Case__r.Status, 
                                        assignment.Subject__c, 
                                        assignment.CreatedDate, 
                                        null, null, null, null, null, 
                                        assignment.Is_Primary_Assignment__c);
            OpenActivity act = new OpenActivity(assignment.Id, site, null, ticket, null);

            activities.add(act);
        }
        
        return activities;
    }

    public OpenActivity getActivityDetails(String activityId, String userId) {
        RaM_Case_Assignment__c ramCase = RaMPortalUtility.getActivityById(activityId, userId);
        
        SiteLocation site = new SiteLocation(ramCase.Parent_Case__r.LocationNumber__r.Name, 
                                    ramCase.Parent_Case__r.LocationNumber__r.LocationName__c, 
                                    ramCase.Parent_Case__r.LocationNumber__r.Physical_Address_2__c, 
                                    ramCase.Parent_Case__r.LocationNumber__r.City__c, 
                                    ramCase.Parent_Case__r.Province__c, 
                                    ramCase.Parent_Case__r.Postal_Code__c,
                                    ramCase.Parent_Case__r.District_Manager__c,
                                    ramCase.Parent_Case__r.Location_Type__c,
                                    ramCase.Parent_Case__r.Location_Classification__c,
                                    ramCase.Parent_Case__r.Maintenance_Tech_User__r.Name);  
                                    
        SiteContact sContact = new SiteContact(ramCase.Parent_Case__r.Retailer__c,
                                                ramCase.Parent_Case__r.VendorEmail__c,
                                                ramCase.Parent_Case__r.LocationNumber__r.Phone__c);

        Boolean isCompleted = isTechnicianActivitiesCompleted(ramCase);

        Ticket ticket = new Ticket(ramCase.Parent_Case__r.Id,
                                    ramCase.Ticket_Number__c,
                                    ramCase.Parent_Case__r.Status,
                                    ramCase.Subject__c,
                                    ramCase.CreatedDate,
                                    ramCase.Work_Start__c,
                                    ramCase.Work_Completed__c,
                                    ramCase.Parent_Case__r.Description,
                                    ramCase.Parent_Case__r.Priority,
                                    isCompleted,
                                    ramCase.Is_Primary_Assignment__c);

        Equipment eqip = new Equipment(ramCase.Parent_Case__r.EquipmentTag__c, 
                                        ramCase.Parent_Case__r.Equipment_Class__c, 
                                        ramCase.Parent_Case__r.Equipment_Type__c, 
                                        ramCase.Parent_Case__r.EquipmentName__r.Name);

        OpenActivity act = new OpenActivity(ramCase.Id, site, sContact, ticket, eqip);

        return act;
    }

    public void updateActivity(String recordId, String lat, String lon, String comments, String currentStatus, String nextStatus, String errorCode, String caseRecordId) {
        Case kase = new Case();
        kase.Id = caseRecordId;
        kase.Status = statusMap.get(nextStatus);

        RaM_WorkInformation__c workInfo = new RaM_WorkInformation__c();
        workInfo.Ticket__c = caseRecordId;
        workInfo.Worklog__c = comments + ' ::: From status ' +  currentStatus + ' => ' + nextStatus;


        if(recordId != null) {
            RaM_Case_Assignment_Activity__c activity = new RaM_Case_Assignment_Activity__c();
            activity.RaM_Case_Assignment__c = recordId;
            activity.Activity_Location__latitude__s = String.isEmpty(lat) ? 0 : Decimal.valueOf(lat);
            activity.Activity_Location__longitude__s = String.isEmpty(lon) ? 0 :  Decimal.valueOf(lon);
            activity.Activity_Date__c = System.now();
            activity.From_Status__c = currentStatus;
            activity.To_Status__c = nextStatus;
            activity.Reason__c = comments;
            activity.Location_Error__c = errorCode;
    
            RaM_Case_Assignment__c assignment = new RaM_Case_Assignment__c();
            assignment.Id = recordId;
            assignment.Work_History__c = assignment.Work_History__c + '<br/>' + System.now() + '<br/>' + comments;
    
            if(statusMap.get(nextStatus).equalsIgnoreCase('Work in Progress')) {
                assignment.Work_Start__c = System.now();
            }
    
            if(statusMap.get(nextStatus).equalsIgnoreCase('Resolved')) {
                assignment.Work_Completed__c = System.now();
            }

            RaMPortalUtility.updateActivity(assignment, activity, kase, workInfo);

        } else {
            RaMPortalUtility.updateActivity(null, null, kase, workInfo);
        }
        
    }

    public void updatePrimaryTechnician(String caseId, String newTechnicianId){
        RaMPortalUtility.updatePrimaryTechnician(caseId, newTechnicianId);
    }  
    
    public void unassignTechnician(String assignmentRecordId) {
        RaMPortalUtility.unassignTechnician(assignmentRecordId);
    }   
    
    public void reassignNewTechnician(String assignmentRecordId, String newTechnicianId) {
        RaMPortalUtility.reassignNewTechnician(assignmentRecordId, newTechnicianId);
    }  
    
    public LocationMarker getActivityLocations(String caseRecordId) {
        List<RaM_Case_Assignment_Activity__c> activities = RaMPortalUtility.getTechnicianAcivities(caseRecordId);
        Case kase = RaMPortalUtility.getCaseLocation(caseRecordId);

        SiteLocation sLocation = new SiteLocation(
            kase.LocationNumber__r.Name,
            kase.LocationNumber__r.LocationName__c,
            kase.LocationNumber__r.Physical_Address_2__c,
            kase.LocationNumber__r.City__c, 
            kase.LocationNumber__r.Province__c,
            kase.Postal_Code__c, 
            kase.District_Manager__c,
            kase.Location_Type__c, 
            kase.Location_Classification__c, 
            kase.Maintenance_Tech_User__r.Name 
        );

        return new LocationMarker(sLocation, activities);
    }

    public List<CombineAttachment> getCombinedAttachments(String caseNumber) {
        List<CombineAttachment> combineAttachments = new List<CombineAttachment>();

        List<Attachment> attachments = RaMPortalUtility.getAttachmentsByCase(caseNumber);
        List<ContentDocument> files = RaMPortalUtility.getFilesByCase(caseNumber);

        for(ContentDocument f : files) {           
            CombineAttachment combine = new CombineAttachment('File', f.Id, f.Title, f.ParentId, f.CreatedDate);
            combineAttachments.add(combine);
        }

        for(Attachment att : attachments) {
            CombineAttachment combine = new CombineAttachment('Attachment', att.Id, att.Name, att.ParentId, att.CreatedDate);
            combineAttachments.add(combine);
        }

        return combineAttachments;
    }

    public List<ActivityComments> getActivityCommentsByCase(String caseRecordId) {
        List<ActivityComments> activityComments = new List<ActivityComments>();

        List<RaM_Case_Assignment_Activity__c> activities = RaMPortalUtility.getActivityCommentsByCase(caseRecordId);
        for(RaM_Case_Assignment_Activity__c activity : activities) {
            ActivityComments activityComment = new ActivityComments(activity.CreatedBy.Name, activity.Reason__c, activity.Activity_Date__c);
            activityComments.add(activityComment);
        }

        return activityComments;
    }    

    private Map<String, String> statusMap = new Map<String, String> {
        'Work In Progress' => 'Work in Progress',
        'Pending Parts' => 'Pending Parts',
        'Pending Approval' => 'Pending Approval',
        'Resolved' => 'Resolved',
        'Vendor Completed' => 'Vendor Completed'
    };

    private List<Contact> getContactsByAccount(String userId) {
        return [SELECT Id, Name, Email, Phone, HomePhone, OtherPhone, AssistantPhone, Title, Fax, AccountId, MobilePhone
                FROM Contact
                WHERE RecordType.Name = 'RaM Vendor'
                    AND AccountId = :userId
                ORDER BY CreatedDate DESC];
    }

    private Map<String, User> getContactUser(List<Contact> contacts) {
        Map<String, User> contactUsers = new Map<String, User>();
        List<String> contactIds = new List<String>();

        for(Contact con : contacts) {
            contactIds.add(con.Id);
        }

        List<User> users = [SELECT Id, UserName, Name, IsActive, LastLoginDate, IsPortalEnabled, ContactId 
                            FROM User
                            WHERE ContactId IN :contactIds
                                AND IsPortalEnabled = true];

        for(User usr : users) {
            if(!contactUsers.containsKey(usr.ContactId)) {
                contactUsers.put(usr.ContactId, usr);
            }
        }
        
        return contactUsers;
    }  

    private Contact getAccountByUser(String userId) {
        return [SELECT Id, AccountId, Account.Name
                FROM Contact
                WHERE Id IN (SELECT ContactId FROM User WHERE Id = :userId)
                    AND RecordType.Name = 'RaM Vendor'];
    }

    private Boolean isDispatcher(UserInfo userInfo) {
        Boolean isDispatcher = false;

        return isDispatcher;
    }

    private Boolean isTechnician(UserInfo userInfo) {
        Boolean isTechnician = false;

        return isTechnician;
    }

    private Boolean isTechnicianActivitiesCompleted(RaM_Case_Assignment__c ramCase) {
        Boolean isCompleted = false;

        if(!ramCase.Parent_Case__r.Status.equalsIgnoreCase('Assigned') || 
            !ramCase.Parent_Case__r.Status.equalsIgnoreCase('Work in Progress') ||
            !ramCase.Parent_Case__r.Status.equalsIgnoreCase('Pending Parts') ||
            !ramCase.Parent_Case__r.Status.equalsIgnoreCase('Pending Approval')) {
        
            isCompleted = true;
        }

        return isCompleted;
    } 

    public class ActivityComments {
        @AuraEnabled public String name {get; private set;}
        @AuraEnabled public String comments {get; private set;}
        @AuraEnabled public DateTime createdDate {get; private set;}

        public ActivityComments(String name, String comments, DateTime createdDate) {
            this.name = name;
            this.comments = comments;
            this.createdDate = createdDate;
        }
    }

    public class CombineAttachment {
        @AuraEnabled public String recordId {get; private set;}
        @AuraEnabled public String name {get; private set;}
        @AuraEnabled public String parentId {get; private set;}
        @AuraEnabled public String downloadUrl {get; private set;}
        @AuraEnabled public DateTime uploadDate {get; private set;}

        public CombineAttachment(String source, String recordId, String name, String parentId, DateTime uploadDate) {
            this.recordId = recordId;
            this.name = name;
            this.parentId = parentId;
            this.uploadDate = uploadDate;

            if(source.equalsIgnoreCase('File')) {
                this.downloadUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/HuskyRaM/sfc/servlet.shepherd/document/download/' + recordId + '?operationContext=S1';
            }

            if(source.equalsIgnoreCase('Attachment')) {
                this.downloadUrl = URL.getSalesforceBaseUrl().toExternalForm() + '/HuskyRaM/servlet/servlet.FileDownload?file=' + recordId;
            }            
        }
    }
    
    public class LocationMarker {
        @AuraEnabled public List<RaM_Case_Assignment_Activity__c> activities {get; private set;}
        @AuraEnabled public SiteLocation siteLoc {get; private set;}

        public LocationMarker(SiteLocation siteLoc, List<RaM_Case_Assignment_Activity__c> activities) {
            this.activities = activities;
            this.siteLoc = siteLoc;
        }
    }

    public class Equipment{
        @AuraEnabled public String tag {get; private set;}
        @AuraEnabled public String quipClass {get; private set;}
        @AuraEnabled public String type {get; private set;}
        @AuraEnabled public String name {get; private set;}

        public Equipment(String tag, String quipClass, String type, String name) {
            this.tag = tag;
            this.quipClass = quipClass;
            this.type = type;
            this.name = name;
        }        
    }

    public class SiteContact{
        @AuraEnabled public String contactName {get; private set;}
        @AuraEnabled public String contactEmail {get; private set;}
        @AuraEnabled public String contactPhone {get; private set;}

        public SiteContact(String contactName, String contactEmail, String contactPhone) {
            this.contactName = contactName;
            this.contactEmail = contactEmail;
            this.contactPhone = contactPhone;
        }
    }

    public class SiteLocation{
        @AuraEnabled public String locationNumber {get; private set;}
        @AuraEnabled public String locationName {get; private set;}
        @AuraEnabled public String locationAddress1 {get; private set;}
        @AuraEnabled public String city {get; private set;}
        @AuraEnabled public String province {get; private set;}
        @AuraEnabled public String postalCode {get; private set;}
        @AuraEnabled public String districtManager {get; private set;}
        @AuraEnabled public String type {get; private set;}
        @AuraEnabled public String classification {get; private set;}
        @AuraEnabled public String techUser {get; private set;}

        public SiteLocation(String locationNumber, String locationName, String locationAddress1, String city, String province, String postalCode,
                        String districtManager, String type, String classification, String techUser) {
            this.locationNumber = locationNumber;
            this.locationName = locationName;
            this.locationAddress1 = locationAddress1;
            this.city = city;
            this.province = province;
            this.postalCode = postalCode;
            this.districtManager = districtManager;
            this.type = type;
            this.classification = classification;
            this.techUser = techUser;
        }

    }

    public class Ticket{
        @AuraEnabled public String caseId {get; private set;}
        @AuraEnabled public String ticketNumber {get; private set;}
        @AuraEnabled public String status {get; private set;}
        @AuraEnabled public String subject {get; private set;}
        @AuraEnabled public String description {get; private set;}
        @AuraEnabled public DateTime assignedDate {get; private set;} 
        @AuraEnabled public DateTime workStart {get; private set;}
        @AuraEnabled public DateTime workEnd {get; private set;} 
        @AuraEnabled public String priority {get; private set;} 
        @AuraEnabled public Boolean isTicketClosed {get; private set;} 
        @AuraEnabled public Boolean isPrimaryTechnician {get; private set;} 

        public Ticket(String caseId, String ticketNumber, String status, String subject, DateTime assignedDate, DateTime workStart, 
                        DateTime workEnd, String description, String priority, Boolean isTicketClosed, Boolean isPrimaryTechnician) {
            this.caseId = caseId;
            this.ticketNumber = ticketNumber;
            this.status = status;
            this.subject = subject;
            this.assignedDate = assignedDate;
            this.workStart = workStart;
            this.workEnd = workEnd;
            this.description = description;
            this.priority = priority;
            this.isTicketClosed = isTicketClosed;
            this.isPrimaryTechnician = isPrimaryTechnician;
        }
    }

    public class OpenActivity{  
        @AuraEnabled public String recordId {get; private set;} 
        @AuraEnabled public SiteLocation loc {get; private set;} 
        @AuraEnabled public SiteContact contact {get; private set;} 
        @AuraEnabled public Ticket tkt {get; private set;} 
        @AuraEnabled public Equipment equip {get; private set;}

        public OpenActivity(String recordId, SiteLocation loc, SiteContact contact, Ticket tkt, Equipment equip) {
            this.recordId = recordId;
            this.loc = loc;
            this.contact = contact;
            this.tkt = tkt;
            this.equip = equip;
        }
    }

    public class CurrentAssignment {
        @AuraEnabled public String userName {get; private set;}
        @AuraEnabled public String userId {get; private set;}
        @AuraEnabled public Boolean hasAssignment {get; private set;}

        public CurrentAssignment(String userName, String userId, Boolean hasAssignment) {
            this.userName = userName;
            this.userId = userId;
            this.hasAssignment = hasAssignment;
        }
    }

    public class CaseAssignment {
        @AuraEnabled public String assignmentRecordId {get; private set;}
        @AuraEnabled public String technicianName {get; private set;}
        @AuraEnabled public String technicianContactId {get; private set;}
        @AuraEnabled public String status {get; private set;}
        @AuraEnabled public DateTime workStart {get; private set;}
        @AuraEnabled public DateTime workEnd {get; private set;}
        @AuraEnabled public String ticketNumber {get; private set;}
        @AuraEnabled public String subject {get; private set;}
        @AuraEnabled public String caseRecordId {get; private set;}
        @AuraEnabled public String caseNumber {get; private set;}
        @AuraEnabled public Boolean isPrimary {get; private set;}

        public CaseAssignment(String technicianName, String status, DateTime workStart, DateTime workEnd, String technicianContactId, Boolean isPrimary, String assignmentRecordId) {
            this.technicianName = technicianName == null ? 'Unassigned' : technicianName;
            this.status = status;
            this.workStart = workStart;
            this.workEnd = workEnd;
            this.technicianContactId = technicianContactId;
            this.isPrimary = isPrimary;
            this.assignmentRecordId = assignmentRecordId;
        }

        public CaseAssignment(String caseRecordId, String ticketNumber, String subject, String status, DateTime workStart, DateTime workEnd, 
                                    String caseNumber, Boolean isPrimary, String assignmentRecordId) {
            this.caseRecordId = caseRecordId;
            this.ticketNumber = ticketNumber;
            this.subject = subject;
            this.status = status;
            this.workStart = workStart;
            this.workEnd = workEnd;
            this.caseNumber = caseNumber;
            this.isPrimary = isPrimary;
            this.assignmentRecordId = assignmentRecordId;
        }        
    }


    public class PortalUserWrapper {
        @AuraEnabled public String accountId {get; private set;}
        @AuraEnabled public String contactId {get; private set;}
        @AuraEnabled public String userId {get; private set;}
        @AuraEnabled public String name {get; set;}
        @AuraEnabled public String email {get; set;}
        @AuraEnabled public String phone {get; set;}
        @AuraEnabled public String mobilePhone {get; set;}
        @AuraEnabled public String homePhone {get; set;}
        @AuraEnabled public String otherPhone {get; set;}
        @AuraEnabled public String assistantPhone {get; set;}
        @AuraEnabled public String fax {get; set;}
        @AuraEnabled public String title {get; set;}
        @AuraEnabled public String userName {get; set;}
        @AuraEnabled public Boolean isActive {get; set;}
        @AuraEnabled public DateTime lastLogin {get; private set;}
        @AuraEnabled public Boolean isDispatcher {get; set;}
        @AuraEnabled public Boolean hasUserAccount {get; set;}
        @AuraEnabled public Boolean hasAssignmentToCurrentCase {get; private set;}

        public PortalUserWrapper(Contact con, User usr) {
            this.accountId = con.AccountId;
            this.contactId = con.Id;
            this.name = con.Name;
            this.email = con.Email;
            this.phone = con.Phone;
            this.mobilePhone = con.MobilePhone;
            this.homePhone = con.HomePhone;
            this.otherPhone = con.OtherPhone;
            this.assistantPhone = con.AssistantPhone;
            this.fax = con.Fax;
            this.title = con.Title;
            this.isDispatcher = false;
            this.isActive = false;
            this.hasUserAccount = false;

            if(usr != null) {
                this.userId = usr.Id;
                this.userName = usr.UserName;
                this.isActive = usr.IsActive;
                this.lastLogin = usr.LastLoginDate;
                this.hasUserAccount = true;
            }
        }

    }
}