/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non parallel test for EquipmentEngineService
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest
private class EquipmentEngineServiceTestNP {

    @IsTest
    static void updateEngines_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Exception resultException;
        Id equipmentId = EPD_TestData.createEquipmentForBatch();
        Equipment__c eq = [
                SELECT Manufacturer__c,
                        Model_Number__c,
                        Tag_Number__c,
                        Manufacturer_Serial_No__c
                FROM Equipment__c
                WHERE Id =: equipmentId
                LIMIT 1
        ];
        insert new Equipment_Engine__c(
                Equipment__c = eq.Id,
                Manufacturer__c = 'something',
                Model__c = 'something',
                Tag__c = 'something',
                Serial_Number__c = 'something'
        );
        Integer expectedNumberOfDMLs = 1;

        Test.startTest();
        try {
            new EquipmentEngineService().updateEngines( new List<Equipment__c> { eq }, EquipmentEngineSaveMode.ALL_OR_NOTHING_DISABLED );
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Integer finalDmlCount = Limits.getDmlStatements();
        Test.stopTest();

        Equipment_Engine__c eqe = [
                SELECT Manufacturer__c, Model__c, Tag__c, Serial_Number__c
                FROM Equipment_Engine__c
                WHERE Equipment__c =: eq.Id
                LIMIT 1
        ];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, resultException);
        System.assertEquals(expectedNumberOfDMLs, finalDmlCount);
        System.assertEquals(eq.Manufacturer__c.toUpperCase(), eqe.Manufacturer__c);
        System.assertEquals(eq.Model_Number__c.toUpperCase(), eqe.Model__c);
        System.assertEquals(eq.Tag_Number__c, eqe.Tag__c);
        System.assertEquals(eq.Manufacturer_Serial_No__c, eqe.Serial_Number__c);
    }

    @IsTest
    static void createEngines_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Exception resultException;
        Id equipmentId = EPD_TestData.createEquipmentForBatch();
        Equipment__c eq = [
                SELECT Manufacturer__c,
                        Model_Number__c,
                        Tag_Number__c,
                        Manufacturer_Serial_No__c
                FROM Equipment__c
                WHERE Id =: equipmentId
                LIMIT 1
        ];
        Integer expectedNumberOfDMLs = 1;

        Test.startTest();
        try {
            new EquipmentEngineService().createEngines( new List<Equipment__c> { eq }, EquipmentEngineSaveMode.ALL_OR_NOTHING_DISABLED );
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Integer finalDmlCount = Limits.getDmlStatements();
        Test.stopTest();

        Equipment_Engine__c eqe = [
                SELECT Manufacturer__c, Model__c, Tag__c, Serial_Number__c
                FROM Equipment_Engine__c
                WHERE Equipment__c =: eq.Id
                LIMIT 1
        ];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, resultException);
        System.assertEquals(expectedNumberOfDMLs, finalDmlCount);
        System.assertEquals(eq.Manufacturer__c.toUpperCase(), eqe.Manufacturer__c);
        System.assertEquals(eq.Model_Number__c.toUpperCase(), eqe.Model__c);
        System.assertEquals(eq.Tag_Number__c, eqe.Tag__c);
        System.assertEquals(eq.Manufacturer_Serial_No__c, eqe.Serial_Number__c);
    }

    @IsTest
    static void updateEngines_withProperParam_ALL_OR_NOTHING() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Exception resultException;
        Id equipmentId = EPD_TestData.createEquipmentForBatch();
        Equipment__c eq = [
                SELECT Manufacturer__c,
                        Model_Number__c,
                        Tag_Number__c,
                        Manufacturer_Serial_No__c
                FROM Equipment__c
                WHERE Id =: equipmentId
                LIMIT 1
        ];
        insert new Equipment_Engine__c(
                Equipment__c = eq.Id,
                Manufacturer__c = 'something',
                Model__c = 'something',
                Tag__c = 'something',
                Serial_Number__c = 'something'
        );
        Integer expectedNumberOfDMLs = 1;

        Test.startTest();
        try {
            new EquipmentEngineService().updateEngines( new List<Equipment__c> { eq }, EquipmentEngineSaveMode.ALL_OR_NOTHING_ENABLED );
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Integer finalDmlCount = Limits.getDmlStatements();
        Test.stopTest();

        Equipment_Engine__c eqe = [
                SELECT Manufacturer__c, Model__c, Tag__c, Serial_Number__c
                FROM Equipment_Engine__c
                WHERE Equipment__c =: eq.Id
                LIMIT 1
        ];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, resultException);
        System.assertEquals(expectedNumberOfDMLs, finalDmlCount);
        System.assertEquals(eq.Manufacturer__c.toUpperCase(), eqe.Manufacturer__c);
        System.assertEquals(eq.Model_Number__c.toUpperCase(), eqe.Model__c);
        System.assertEquals(eq.Tag_Number__c, eqe.Tag__c);
        System.assertEquals(eq.Manufacturer_Serial_No__c, eqe.Serial_Number__c);
    }

    @IsTest
    static void createEngines_withProperParam_ALL_OR_NOTHING() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Exception resultException;
        Id equipmentId = EPD_TestData.createEquipmentForBatch();
        Equipment__c eq = [
                SELECT Manufacturer__c,
                        Model_Number__c,
                        Tag_Number__c,
                        Manufacturer_Serial_No__c
                FROM Equipment__c
                WHERE Id =: equipmentId
                LIMIT 1
        ];
        Integer expectedNumberOfDMLs = 1;

        Test.startTest();
        try {
            new EquipmentEngineService().createEngines( new List<Equipment__c> { eq }, EquipmentEngineSaveMode.ALL_OR_NOTHING_ENABLED );
        } catch (Exception ex) {
            failFlag = true;
            resultException = ex;
        }
        Integer finalDmlCount = Limits.getDmlStatements();
        Test.stopTest();

        Equipment_Engine__c eqe = [
                SELECT Manufacturer__c, Model__c, Tag__c, Serial_Number__c
                FROM Equipment_Engine__c
                WHERE Equipment__c =: eq.Id
                LIMIT 1
        ];
        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, resultException);
        System.assertEquals(expectedNumberOfDMLs, finalDmlCount);
        System.assertEquals(eq.Manufacturer__c.toUpperCase(), eqe.Manufacturer__c);
        System.assertEquals(eq.Model_Number__c.toUpperCase(), eqe.Model__c);
        System.assertEquals(eq.Tag_Number__c, eqe.Tag__c);
        System.assertEquals(eq.Manufacturer_Serial_No__c, eqe.Serial_Number__c);
    }

}