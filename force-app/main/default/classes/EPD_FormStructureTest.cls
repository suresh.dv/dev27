/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_FormStructure
History:        jschn 2019-07-15 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_FormStructureTest {

    @IsTest
    static void setEPDStructure_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setEPDStructure(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(null, result.epd);
    }

    @IsTest
    static void setEPDStructure_withParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setEPDStructure(new EPD_FormStructureEPD());
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.epd);
    }

    @IsTest
    static void setEngineBlocks_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setEngineBlocks(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(null, result.engineBlocks);
    }

    @IsTest
    static void setEngineBlocks_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructureBlock> blocks = new List<EPD_FormStructureBlock>();
        Integer expectedSize = blocks.size();
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setEngineBlocks(blocks);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.engineBlocks);
        System.assertEquals(expectedSize, result.engineBlocks.size());
    }

    @IsTest
    static void setEngineBlocks_withParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructureBlock> blocks = new List<EPD_FormStructureBlock>{
                new EPD_FormStructureBlock()
        };
        Integer expectedSize = blocks.size();
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setEngineBlocks(blocks);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.engineBlocks);
        System.assertEquals(expectedSize, result.engineBlocks.size());
    }

    @IsTest
    static void setParts_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructurePart> parts;
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setParts(parts);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(null, result.parts);
    }

    @IsTest
    static void setParts_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructurePart> parts = new List<EPD_FormStructurePart>();
        Integer expectedSize = parts.size();
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setParts(parts);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.parts);
        System.assertEquals(expectedSize, result.parts.size());
    }

    @IsTest
    static void setParts_withParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructurePart> parts = new List<EPD_FormStructurePart>{
                new EPD_FormStructurePart()
        };
        Integer expectedSize = parts.size();
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setParts(parts);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.parts);
        System.assertEquals(expectedSize, result.parts.size());
    }

    @IsTest
    static void setCOCStages_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructureCOCStage> cocStages;
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setCOCStages(cocStages);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(null, result.cocStages);
    }

    @IsTest
    static void setCOCStages_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructureCOCStage> cocStages = new List<EPD_FormStructureCOCStage>();
        Integer expectedSize = cocStages.size();
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setCOCStages(cocStages);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.cocStages);
        System.assertEquals(expectedSize, result.cocStages.size());
    }

    @IsTest
    static void setCOCStages_withParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        List<EPD_FormStructureCOCStage> cocStages = new List<EPD_FormStructureCOCStage>{
                new EPD_FormStructureCOCStage()
        };
        Integer expectedSize = cocStages.size();
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setCOCStages(cocStages);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.cocStages);
        System.assertEquals(expectedSize, result.cocStages.size());
    }

    @IsTest
    static void setEngineThresholds_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureThresholds thresholds;
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setEngineThresholds(thresholds);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(null, result.engineThresholds);
    }

    @IsTest
    static void setEngineThresholds_withParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_FormStructureThresholds thresholds = new EPD_FormStructureThresholds();
        EPD_FormStructure result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().setEngineThresholds(thresholds);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.engineThresholds);
    }

    @IsTest
    static void hasReachedThreshold_fail() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean expectedResult;
        Boolean result;

        Test.startTest();
        try {
            result = new EPD_FormStructure().hasReachedThreshold();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_false() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean expectedResult = false;
        Boolean result;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_RPM__c = 1500,
                Min_RPM__c = 1000
        );

        Test.startTest();
        try {
            result = new EPD_FormStructure()
                    .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)))
                    .setEngineBlocks(new List<EPD_FormStructureBlock>())
                    .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig))
                    .hasReachedThreshold();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_trueOnEPD() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean expectedResult = true;
        Boolean result;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_RPM__c = 1100,
                Min_RPM__c = 800
        );

        Test.startTest();
        try {
            result = new EPD_FormStructure()
                    .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)))
                    .setEngineBlocks(new List<EPD_FormStructureBlock>())
                    .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig))
                    .hasReachedThreshold();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_trueOnBlock() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean expectedResult = true;
        Boolean result;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_RPM__c = 1500,
                Min_RPM__c = 1000,
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 0,
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 200,
                Max_Cylinder_Compression__c = 250
        );

        Test.startTest();
        try {
            EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
            block.cylinders = new List<EPD_FormStructureCylinder> {
                    (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                            (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Cylinder_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    )
            };
            result = new EPD_FormStructure()
                    .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)))
                    .setEngineBlocks(new List<EPD_FormStructureBlock>())
                    .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig))
                    .setEngineBlocks(new List<EPD_FormStructureBlock> {block})
                    .hasReachedThreshold();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedThreshold_trueOnCylinder() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean expectedResult = true;
        Boolean result;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine__mdt engineConfig = new EPD_Engine__mdt(
                Max_RPM__c = 1500,
                Min_RPM__c = 1000,
                Min_Intake_Manifold_Pressure__c = -20,
                Max_Intake_Manifold_Pressure__c = 25,
                Max_Valve_Seat_Recession__c = 0.1,
                Valve_Seat_Warning_Limit__c = 0.1,
                Min_Cylinder_Compression__c = 240,
                Max_Cylinder_Compression__c = 250
        );

        Test.startTest();
        try {
            EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
            block.cylinders = new List<EPD_FormStructureCylinder> {
                    (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                            (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Cylinder_Information__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    )
            };
            result = new EPD_FormStructure()
                    .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)))
                    .setEngineBlocks(new List<EPD_FormStructureBlock>())
                    .setEngineThresholds(new EPD_FormStructureThresholds().setRecord(engineConfig))
                    .setEngineBlocks(new List<EPD_FormStructureBlock> {block})
                    .hasReachedThreshold();
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean expectedResult;
        Boolean result;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);

        Test.startTest();
        try {

            result = new EPD_FormStructure()
                    .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)))
                    .setEngineBlocks(new List<EPD_FormStructureBlock> {block})
                    .hasReachedRetrospectiveThreshold(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_false() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean expectedResult = false;
        Boolean result;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine_Performance_Data__c epd2 = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructureBlock block2 = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructure previousStructure = new EPD_FormStructure()
                .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd2)))
                .setEngineBlocks(new List<EPD_FormStructureBlock> {block2});
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;

        Test.startTest();
        try {

            result = new EPD_FormStructure()
                    .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)))
                    .setEngineBlocks(new List<EPD_FormStructureBlock> {block})
                    .hasReachedRetrospectiveThreshold(previousStructure);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void hasReachedRetrospectiveThreshold_true() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean expectedResult = true;
        Boolean result;
        EPD_Engine_Performance_Data__c epd = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_Engine_Performance_Data__c epd2 = (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                new EPD_Engine_Performance_Data__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock block = prepareBlock(EPD_FieldDefaultsGeneral.CLASS_NAME);
        EPD_FormStructureBlock block2 = prepareBlock(EPD_FieldDefaultsWrong.CLASS_NAME);
        EPD_FormStructure previousStructure = new EPD_FormStructure()
                .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd2)))
                .setEngineBlocks(new List<EPD_FormStructureBlock> {block2});
        EPD_Constants.SETTINGS.Compression_Minor_Threshold__c = 20;

        Test.startTest();
        try {

            result = new EPD_FormStructure()
                    .setEPDStructure(((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd)))
                    .setEngineBlocks(new List<EPD_FormStructureBlock> {block})
                    .hasReachedRetrospectiveThreshold(previousStructure);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedResult, result);
    }

    private static EPD_FormStructureBlock prepareBlock(String className) {
        EPD_Block_Information__c blockInfo = (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                new EPD_Block_Information__c(),
                EPD_FieldDefaultsGeneral.CLASS_NAME,
                false
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(blockInfo);
        block.cylinders = new List<EPD_FormStructureCylinder> {
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                        (EPD_Cylinder_Information__c) HOG_SObjectFactory.createSObject(
                                new EPD_Cylinder_Information__c(),
                                className,
                                false
                        )
                )
        };
        return block;
    }

    //TODO remove after LTNG migration
    @IsTest
    static void toStringTest() {
        EPD_FormStructure structure = new EPD_FormStructure()
                .setEPDStructure(new EPD_FormStructureEPD())
                .setEngineThresholds(new EPD_FormStructureThresholds())
                .setParts(new List<EPD_FormStructurePart>())
                .setCOCStages(new List<EPD_FormStructureCOCStage>())
                .setEOCs(new List<EPD_FormStructureEOC>())
                .setEngineBlocks(new List<EPD_FormStructureBlock>());
        String structureString = '';

        Test.startTest();
        structureString = structure.toString();
        Test.stopTest();

        System.assert(String.isNotBlank(structureString));
    }

}