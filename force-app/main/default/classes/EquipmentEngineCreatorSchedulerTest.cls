/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EquipmentEngineCreatorScheduler
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest
private class EquipmentEngineCreatorSchedulerTest {

    @IsTest
    public static void scheduleWithDefaultCron() {

        Test.startTest();
        Id jobId = EquipmentEngineCreatorScheduler.schedule();
        Test.stopTest();

        CronTrigger job = [SELECT CronExpression, CronJobDetail.Name
        FROM CronTrigger
        WHERE Id = :jobId];
        System.assertEquals(EquipmentEngineCreatorScheduler.DEFAULT_CRON_STRING, job.CronExpression);
        System.assertEquals(EquipmentEngineCreatorSchedulable.JOB_NAME, job.CronJobDetail.Name);
    }

    @IsTest
    public static void scheduleWithCustomCron() {
        String customCronString = '0 0 9 ? * MON';

        Test.startTest();
        Id jobId = EquipmentEngineCreatorScheduler.schedule(customCronString);
        Test.stopTest();

        CronTrigger job = [SELECT CronExpression, CronJobDetail.Name
        FROM CronTrigger
        WHERE Id = :jobId];
        System.assertEquals(customCronString, job.CronExpression);
        System.assertEquals(EquipmentEngineCreatorSchedulable.JOB_NAME, job.CronJobDetail.Name);
    }

}