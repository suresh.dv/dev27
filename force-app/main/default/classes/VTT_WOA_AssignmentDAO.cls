/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO Interface for VTT WOA Assignment Endpoint purposes
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public interface VTT_WOA_AssignmentDAO {

    List<Work_Order_Activity_Assignment__c> getAssignments(Set<Id> activityIdsForDeletion);
    List<Work_Order_Activity_Assignment__c> getAssignments(Set<Id> activityIdsForDeletion, List<Work_Order_Activity__c> activities, List<Id> tradesmenIds);
    List<Work_Order_Activity_Assignment__c> getAssignments(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds);
    List<Work_Order_Activity_Assignment__c> getNonRejectedAssignmentsByTradesman(String tradesman);
    List<Work_Order_Activity_Assignment__c> getNonRejectedAssignmentsByActivity(Id activityId);

}