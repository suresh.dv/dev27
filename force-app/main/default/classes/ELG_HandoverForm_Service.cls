/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    Class that taking care of logic
 *  Test Class:		ELG_HandoverFrom_EndpointTest
 *  History:        Created on 7/11/2019   
 */

public inherited sharing class ELG_HandoverForm_Service {
	private static final String CLASS_NAME = String.valueOf(ELG_HandoverForm_Service.class);

	public static List<ELG_Log_Entry__c> prepareLogEntries(String categoryName) {
		List<ELG_Log_Entry__c> sectionLogEntries = new List<ELG_Log_Entry__c>();

		List<ELG_Handover_Category__c> listOfQuestionsByCategory = ELG_DAOProvider.handoverDAO.getQuestionsBasedOnCategory(categoryName);

		if (listOfQuestionsByCategory.isEmpty()) {
			//do nothing
		} else {

			String nameOfRecordType = listOfQuestionsByCategory[0].RecordType_DeveloperName__c;

			//get record type based on queried handover category section
			Id idOfRecordType = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
					.get(nameOfRecordType).getRecordTypeId();

			//prepare Log Entries based on number of questions that will wait for an insert
			for (ELG_Handover_Category_Question__c question : listOfQuestionsByCategory[0].Handover_Category_Questions__r) {
				sectionLogEntries.add(new ELG_Log_Entry__c(Question__c = question.Question__c,
						Handover_Category_Question__c = question.Id, RecordTypeId = idOfRecordType,
						Question_Category_Name__c = question.Handover_Category__r.Name,
						Question_Category_Order__c = question.Handover_Category__r.Order__c,
						Question_Order__c = question.Order__c));
			}
		}

		return sectionLogEntries;
	}

	public static List<ELG_Log_Entry__c> insertLogs(List<ELG_Log_Entry__c> listOfEntries, ELG_Shift_Assignement__c shiftAssignment) {
		List<ELG_Log_Entry__c> logsToInsert = new List<ELG_Log_Entry__c>();
		for (ELG_Log_Entry__c logEntry : listOfEntries) {
			if (logEntry.User_Log_Entry__c != null) {
				logEntry.Shift_Assignement__c = shiftAssignment.Id;
				logEntry.Post__c = shiftAssignment.Post__c;
				logEntry.Operating_Field_AMU__c = shiftAssignment.Post__r.Operating_Field_AMU__c;
				logsToInsert.add(logEntry);
			}
		}

		if (!logsToInsert.isEmpty()) {
			insert logsToInsert;
		}

		return logsToInsert;
	}

	public static ELG_Shift_Handover__c acceptHandoverAndChangeStatus(ELG_Shift_Handover__c HOForm, String userId, ELG_Shift_Assignement__c nextShift, String traineeId) {
		Datetime dateToday = Datetime.now();

		System.Savepoint sp = Database.setSavepoint();

		try {
			HOForm.Incoming_Operator__c = userId;
			HOForm.Signed_In__c = dateToday;
			HOForm.Status__c = ELG_Constants.HANDOVER_ACCEPTED;
			update HOForm;

			System.debug(CLASS_NAME + ' trainee: ' +traineeId);
			nextShift.Trainee__c = traineeId;
			nextShift.Primary__c = userId;
			nextShift.Previous_Primary__c = userId;
			update nextShift;
		} catch (DmlException ex) {
			Database.rollback(sp);
			throw ex;
		}

		return HOForm;
	}

	public static ELG_Shift_Handover__c updateHandoverStatusAndSignOff(ELG_Shift_Handover__c HOForm) {

		Datetime dateToday = Datetime.now();

		if (HOForm.Shift_Status__c != ELG_Constants.HANDOVER_IN_PROGRESS) {
			HOForm.Status__c = ELG_Constants.HANDOVER_IN_PROGRESS;
			HOForm.Signed_Off__c = dateToday;
			update HOForm;
		}
		return HOForm;
	}

	public static ELG_Shift_Handover__c updateHandoverReview(ELG_Shift_Handover__c HOForm,
			List<String> sectionsForUpdate,
			String currentUser) {

		Datetime dateToday = Datetime.now();

		if (sectionsForUpdate.isEmpty()) {
			//do nothing
		} else {

			for (String sectionToUpdate : sectionsForUpdate) {
				if (sectionToUpdate.contains('supervisorReview')) {
					HOForm.Sr_Supervisor__c = currentUser;
					HOForm.Reviewed_by_Sr_Supervisor_Timestamp__c = dateToday;
				}

				if (sectionToUpdate.contains('steamChiefReview')) {
					HOForm.Steam_Chief__c = currentUser;
					HOForm.Reviewed_by_Steam_Chief_Timestamp__c = dateToday;
				}

				if (sectionToUpdate.contains('shiftLeadReview')) {
					HOForm.Shift_Lead__c = currentUser;
					HOForm.Reviewed_by_Shift_Lead_Timestamp__c = dateToday;
				}
			}
			update HOForm;
		}

		return HOForm;
	}

	public static ELG_Shift_Handover__c updateHandoverReview(ELG_Shift_Handover__c HOForm, String currentUser) {

		Datetime dateToday = Datetime.now();

		if (HOForm.Shift_Engineer__c == null) {
			HOForm.Shift_Engineer__c = currentUser;
			HOForm.Reviewed_by_Shift_Engineer_Timestamp__c = dateToday;
			update HOForm;
		}

		return HOForm;
	}


	public static List<HandoverQuestionCategoryWrapper> getAllHandoverLogEntries(String shiftAssignementId) {

		List<HandoverQuestionCategoryWrapper> results = new List<HandoverQuestionCategoryWrapper>();

		// Grouping log entries by Name of Question Category
		List<ELG_Log_Entry__c> logEntriesForHandover = ELG_DAOProvider.handoverDAO.getLogEntriesByCategories(shiftAssignementId);

		if (logEntriesForHandover.size() > 0) {
			results = new HandoverBuilder()
					.ofEntries(logEntriesForHandover)
					.groupByCategoriesAndQuestions()
					.buildAndSort();
		}

		return results;
	}

	@TestVisible
	private class HandoverBuilder {
		private List<ELG_Log_Entry__c> entries;

		// Results of builder
		@TestVisible
		private List<HandoverQuestionCategoryWrapper> results;

		// Helper to group entries by questions
		private Map<String, List<ELG_Log_Entry__c>> questionLogEntryMap;

		// Helper to group entries by question categories
		private Map<String, HandoverQuestionCategoryWrapper> groupByQuestionCategoryMap;

		public HandoverBuilder() {
		}

		public HandoverBuilder ofEntries(List<ELG_Log_Entry__c> entries) {
			this.entries = entries;
			return this;
		}

		public HandoverBuilder groupByCategoriesAndQuestions() {
			this.groupLogEntriesByQuestions();

			this.groupByQuestionCategoryMap = new Map<String, HandoverQuestionCategoryWrapper>();

			// Grouping based Question Categories
			for (String questionKey : this.questionLogEntryMap.keySet()) {

				// Taking first item for setting
				// Question Category name - Handover_Category_Question__r.Handover_Category__r.Name
				// And Question Name from snapshots Question__c field
				// Handover_Category_Question__r.Handover_Category__r.Name - Displayed as category
				// Question__c - Displayed as question
				ELG_Log_Entry__c firstEntry = this.questionLogEntryMap.get(questionKey).get(0);

				String categoryQuestionKey = firstEntry.Handover_Category_Question__r.Handover_Category__r.Name;

				if (this.groupByQuestionCategoryMap.containsKey(categoryQuestionKey)) {

					HandoverQuestionLogEntriesWrapper question = new HandoverQuestionLogEntriesWrapper(
							firstEntry.Question__c,
							this.questionLogEntryMap.get(questionKey)
					);

					HandoverQuestionCategoryWrapper wrapper = this.groupByQuestionCategoryMap.get(categoryQuestionKey);
					wrapper.questionsWithLogEntries.add(question);
					this.groupByQuestionCategoryMap.put(categoryQuestionKey, wrapper);
				} else {

					ELG_Handover_Category__c category = new ELG_Handover_Category__c(
							Name = firstEntry.Handover_Category_Question__r.Handover_Category__r.Name,
							Order__c = firstEntry.Handover_Category_Question__r.Handover_Category__r.Order__c);

					HandoverQuestionLogEntriesWrapper question = new HandoverQuestionLogEntriesWrapper(
							firstEntry.Handover_Category_Question__r.Question__c,
							this.questionLogEntryMap.get(questionKey)
					);

					HandoverQuestionCategoryWrapper wrapper = new HandoverQuestionCategoryWrapper(
							category, new List<HandoverQuestionLogEntriesWrapper>{
									question
							});
					this.groupByQuestionCategoryMap.put(categoryQuestionKey, wrapper);
				}

			}

			return this;
		}

		public List<HandoverQuestionCategoryWrapper> buildAndSort() {
			this.results = new List<HandoverQuestionCategoryWrapper>();
			// Build list of results
			for (String key : groupByQuestionCategoryMap.keySet()) {

				this.results.add(groupByQuestionCategoryMap.get(key));
			}

			// Sorting results by Handover_Category__r.Order__c
			this.results.sort();
			return this.results;
		}

		private void groupLogEntriesByQuestions() {
			this.questionLogEntryMap = new Map<String, List<ELG_Log_Entry__c>>();

			for (ELG_Log_Entry__c entry : this.entries) {

				String key = entry.Handover_Category_Question__r.Name;

				if (questionLogEntryMap.containsKey(key)) {
					List<ELG_Log_Entry__c> entries = questionLogEntryMap.get(key);
					entries.add(entry);
					questionLogEntryMap.put(key, entries);
				} else {
					questionLogEntryMap.put(key, new List<ELG_Log_Entry__c>{
							entry
					});
				}
			}
		}

	}

	@TestVisible
	private class HandoverQuestionCategoryWrapper implements Comparable {

		@AuraEnabled
		public ELG_Handover_Category__c questionCategory;

		@AuraEnabled
		public List<HandoverQuestionLogEntriesWrapper> questionsWithLogEntries;

		public HandoverQuestionCategoryWrapper(ELG_Handover_Category__c questionCategory, List<HandoverQuestionLogEntriesWrapper> questionWithLogEntries) {
			this.questionCategory = questionCategory;
			this.questionsWithLogEntries = questionWithLogEntries;
		}

		public Integer compareTo(Object compareToObject) {
			HandoverQuestionCategoryWrapper compareTo = (HandoverQuestionCategoryWrapper) compareToObject;
			if (questionCategory.Order__c > compareTo.questionCategory.Order__c) {
				return 1;
			} else if (questionCategory.Order__c < compareTo.questionCategory.Order__c) {
				return -1;
			} else {
				return 0;
			}
		}
	}

	@TestVisible
	private class HandoverQuestionLogEntriesWrapper {
		@AuraEnabled
		public String Question;

		@AuraEnabled
		public List<ELG_Log_Entry__c> Log_Entries;

		public HandoverQuestionLogEntriesWrapper(String Question, List<ELG_Log_Entry__c> Log_Entries) {
			this.Question = Question;
			this.Log_Entries = Log_Entries;
		}
	}


}