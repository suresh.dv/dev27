/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    HOG Service class for lightning containing endpoints for Custom Lookup component.
History:        jschn 07.20.2018 - Created.
**************************************************************************************************/
@IsTest private class HOG_PicklistServiceLtngTest {

	@IsTest static void picklistRetrieve_success() {
		List<HOG_PicklistServiceLtng.PicklistItem> picklistValues = HOG_PicklistServiceLtng.getPicklist('Account', 'Industry');
		System.assertNotEquals(null, picklistValues);
		System.assert(picklistValues.size() > 0);
	}
	
	@IsTest static void picklistRetrieve_fail() {
		List<HOG_PicklistServiceLtng.PicklistItem> picklistValues = HOG_PicklistServiceLtng.getPicklist('Account', 'Name');
		System.assertNotEquals(null, picklistValues);
		
		picklistValues = HOG_PicklistServiceLtng.getPicklist('', '');
		System.assertNotEquals(null, picklistValues);
		System.assertEquals(0, picklistValues.size());
	}
	
}