public class ATSSMSReportControllerOpp{    		
    public ATSWrapperUtilitiesOpp.ATSQuoteWrapper quote{get; set;}
    
    public Id tenderId {get; set;}
    public Id accountId {get; set;}
    public List<ATSWrapperUtilitiesOpp.ATSOpportunityWrapper> o{get;set;}
    public Decimal sequenceNumber{get;set;}
    public String CustomerType {get;set;}
    
    public ATS_Freight__c asphaltFreight {get; set;}
    public ATS_Freight__c emulsionFreight {get; set;}
    public ATS_Freight__c residualFreight {get; set;}
    
    
    
    // Copied from quote controller
    public List<ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper> asphaltLineItems {get;set;}
    public List<ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper> EmulsionLineItems {get;set;}
    public List<ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper> ResidualsLineItems {get;set;}  
    
    
    List<String> categoriesList = new List<String>();
    Set<String> categoriesSet = new Set<String>();

    List<String> opportunityList;
    
    List<ATSWrapperUtilitiesOpp.ATSQuoteWrapper> quoteWrapperList{get; set;}
    
    public ATSSMSReportControllerOpp() {
        tenderId = ApexPages.currentPage().getParameters().get('tenderId');
        accountId = ApexPages.currentPage().getParameters().get('accountId');
        //opportunityList =  ApexPages.currentPage().getParameters().get('opportunityList').split(',');
        asphaltLineItems = new List<ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper>(); 
        emulsionLineItems = new List<ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper>();
        residualsLineItems = new List<ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper>(); 
        
        categoriesList = ApexPages.currentPage().getParameters().get('categoriesList').split(',');
        for(string s : categoriesList)
        {    
            categoriesSet.add(s);
        }
        
        initializeQuoteWrapper();   
    }
    
    public void initializeQuoteWrapper() {
        quote = new ATSWrapperUtilitiesOpp.ATSQuoteWrapper();
        
        //system.debug('Sme v Generate SMS Report Controller');
        
        Opportunity tender = [SELECT 
                              Id, 
                              Name,
                              //Tender__c, 
                              cpm_Price_Valid_From__c, 
                              cpm_Price_Valid_To__c, 
                              cpm_Bid_Due_Date_Time__c, 
                              Description, 
                              Marketer__r.Name, 
                              Marketer__c,
                              cpm_For_Season__c,
                              cpm_Trade_Class__c,
                              cpm_Destination_City_Province__c,
                              cpm_Tender__c,
                              cpm_Tax_Exempt__c,
                              cpm_Tax_Exempt2__c,
                              cpm_Freight_Credit__c,
                              cpm_Broker__c,
                              cpm_Broker2__c,
                              Pay_out_Handling_Fees__c,
                              Pay_out_Handling_Fees2__c,
                              SMS_Contract__c,
                              SMS_Contract2__c,
                              PO__c,
                              PO2__c,
                              Sales_Comments__c,
                              Sales_Comments2__c,
                              cpm_Tax_Exempt_picklist__c,
                              cpm_Tax_Exempt_picklist2__c,
                              cpm_Opportunity_Results_Category__c,
                              cpm_Opportunity_Results_Category2__c,
                              Asphalt_Number__c
                              FROM 
                              Opportunity 
                              WHERE 
                              Id =:tenderId];
        
        
        Cpm_Customer_Opportunity__c customer = [SELECT 
                                                Id, 
                                                cpm_Comments__c,
                                                cpm_AccountLookup__c, 
                                                cpm_AccountLookup__r.Name, 
                                                cpm_AccountLookup__r.AccountNumber,
                                                cpm_OpportunityLookup__c, 
                                                cpm_Contact__c, 
                                                cpm_Contact__r.Name, 
                                                cpm_Contact__r.Fax,
                                                cpm_AccountLookup__r.ATS_Customer_Type__c 
                                                FROM 
                                                Cpm_Customer_Opportunity__c 
                                                WHERE 
                                                Cpm_Customer_Opportunity__c.cpm_AccountLookup__c =: accountId AND cpm_OpportunityLookup__c =: tenderId][0];
        
        CustomerType  = customer.cpm_AccountLookup__r.ATS_Customer_Type__c; 
        
        if(CustomerType == null) {
            
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unknown Customer Type for account ' + customer.cpm_AccountLookup__r.Name);
            ApexPages.addMessage(myMsg);
            return;
        }
        
        quote.tender = tender ;
        quote.accountId = this.accountId;
        quote.accountName = customer.cpm_AccountLookup__r.Name;
        quote.accountNumber = customer.cpm_AccountLookup__r.AccountNumber;
        quote.CustomerComments  = customer.cpm_Comments__c;        
        
        quote.offerEndDate = tender.cpm_Bid_Due_Date_Time__c;
        // need to do this to include proper time zone into the output
        quote.offerEndDateString = tender.cpm_Bid_Due_Date_Time__c.format('h:mm a (z) MM/dd/yyyy ');
        
        quote.contactId = customer.cpm_Contact__c;
        quote.contactName = customer.cpm_Contact__r.Name;
        quote.contactFaxNumber = customer.cpm_Contact__r.Fax;
        
        quote.tenderId = this.tenderId;
        //-->        quote.tenderNumber = tender.Tender__c;
        quote.tenderName = tender.Name;
        quote.AsphaltNumber = tender.Asphalt_Number__c;
        
        quote.priceValid = tender.cpm_Price_Valid_From__c.format() + ' - ' + tender.cpm_Price_Valid_To__c.format();
        quote.project = tender.Description; //Bid_Description__c;
        //-->        quote.additionalComments = tender.Bid_Description__c; // need to change this value to customer specific comments (from junction)
        quote.signatureName = tender.Marketer__r.Name;
        quote.ForSeason = tender.cpm_For_Season__c;
        quote.TradeClass = tender.cpm_Trade_Class__c;
        quote.Destination = tender.cpm_Destination_City_Province__c;
        quote.Broker = tender.cpm_Broker__c;
        quote.Broker2 = tender.cpm_Broker2__c;
        quote.OppResultCat = tender.cpm_Opportunity_Results_Category__c; 
        quote.OppResultCat2 = tender.cpm_Opportunity_Results_Category2__c;
        quote.FreightCredits = tender.cpm_Freight_Credit__c;
        quote.TenderNum = tender.cpm_Tender__c;
        quote.TaxExempt = tender.cpm_Tax_Exempt__c;
        quote.TaxExempt2 = tender.cpm_Tax_Exempt2__c;
        quote.PayOut = tender.Pay_out_Handling_Fees__c;
        quote.PayOut2 = tender.Pay_out_Handling_Fees2__c;
        quote.SMSCon = tender.SMS_Contract__c;
        quote.SMSCon2 = tender.SMS_Contract2__c; 
        quote.POQuote = tender.PO__c;
        quote.PO2Quote = tender.PO2__c; 
        quote.SalesComments = tender.Sales_Comments__c;
        quote.SalesComments2 = tender.Sales_Comments2__c;
        quote.TaxExemptPick = tender.cpm_Tax_Exempt_picklist__c;
        quote.TaxExemptPick2 = tender.cpm_Tax_Exempt_picklist2__c;
        
      
        
        
        
        
        // now lets get all opportunity and figure out what we need to filter out based on customer type
        // Account__r.ATS_Customer_Type__c 
        //System.debug('Tender - Opportunity ID1: ' + tender.id); 
        //System.debug('Tender - Opportunity ID2: ' + tenderid); 
                  
        quote.setOpportunities([SELECT Id, 
                                //Opportunity_ATS_Product_Category__c, 
                                cpm_Product_Category__c,
                                RecordType.Name,
                                PO__c,
                                SMS_Contract__c,
                                Pay_out_Handling_Fees__c,
                                Sales_Comments__c,
                                (SELECT 
                                 Id, 
                                 PricebookEntry.Name, 
                                 PricebookEntry.Id, 
                                 Price_Type__c, 
                                 UnitPrice, 
                                 Unit__c,
                                 cpm_Product_Family__c,
                                 Quantity, 
                                 TotalPrice, 
                                 PricebookEntryId, 
                                 OpportunityId, 
                                 ListPrice,
                                 Description, 
                                 Anti_Strip__c, 
                                 Additive__c,
                                 Currency__c,
                                 Sales_Price__c,
                                 Axle_5_Price__c,
                                 Axle_6_Price__c,
                                 Axle_7_Price__c,
                                 Axle_8_Price__c,
                                 Sales_Price_Formula__c,
                                 cpm_Pickup_Price_Formula__c, //new pickup
                                 cpm_Pickup_price__c, //new pickup
                                 Axle_5_Price_Formula__c,
                                 Axle_6_Price_Formula__c,
                                 Axle_7_Price_Formula__c,
                                 Axle_8_Price_Formula__c,
                                 Include_Sales_Price__c,
                                 cpm_Include_Pickup_Price__c, //new pickup
                                 Include_Axle_5__c,
                                 Include_Axle_6__c,
                                 Include_Axle_7__c,
                                 Include_Axle_8__c,
                                 PricebookEntry.Product2.Density__c,
                                 PricebookEntry.Product2.ProductCode,
                                 PricebookEntry.Product2.Product_AKA_SMS_Name__c
                                 FROM 
                                 OpportunityLineItems
                                 WHERE  cpm_Product_Won__c='Yes' AND PricebookEntry.Product2.Family IN : categoriesSet
                                 Order By PricebookEntry.Name),
                                (SELECT  
                                 Id,
                                 cpm_Product_Type__c,
                                 Husky_Supplier_1_Selected__c,
                                 Husky_Supplier_2_Selected__c,
                                 Husky_Supplier_3_Selected__c, //<-- new
                                 cpm_HuskySupplier1_ats_pricing__c, //<-- Husky_Supplier_1__c,
                                 cpm_HuskySupplier2_ats_pricing__c, //<-- Husky_Supplier_2__c,                                     	
                                 cpm_HuskySupplier3_ats_pricing__c, //<-- Husky_Supplier_3__c, 
                                 Supplier_1_Carrier__c,
                                 Supplier_2_Carrier__c,
                                 Supplier_3_Carrier__c, // <-- new
                                 Supplier_1_Min_Load__c,
                                 Supplier_2_Min_Load__c,                                     
                                 Supplier_3_Min_Load__c, //<-- new
                                 cpm_Supplier_1_Pickup_Rate__c, // new Pickp Rate 1
                                 cpm_Supplier_2_Pickup_Rate__c, // new Pickp Rate 2
                                 cpm_Supplier_3_Pickup_Rate__c, // new Pickp Rate 3
                                 Supplier_1_Rate__c,
                                 Supplier_2_Rate__c,
                                 Supplier_3_Rate__c, //<-- new
                                 Supplier_1_Unit__c,
                                 Supplier_2_Unit__c,  
                                 Supplier_3_Unit__c, //<-- new
                                 Emulsion_Rate5_Supplier1__c,
                                 Emulsion_Rate5_Supplier2__c,
                                 Emulsion_Rate5_Supplier3__c, //<-- new                                      
                                 Emulsion_Rate6_Supplier_1__c,
                                 Emulsion_Rate6_Supplier2__c,
                                 Emulsion_Rate6_Supplier3__c, //<-- new
                                 Emulsion_Rate7_Supplier1__c,
                                 Emulsion_Rate7_Supplier2__c,
                                 Emulsion_Rate7_Supplier3__c, //<-- new
                                 Emulsion_Rate8_Supplier1__c,
                                 Emulsion_Rate8_Supplier2__c,
                                 Emulsion_Rate8_Supplier3__c, //<-- new
                                 Emulsion_Min_Load5_Supplier1__c,
                                 Emulsion_Min_Load5_Supplier2__c,
                                 Emulsion_Min_Load5_Supplier3__c, //<-- new
                                 Emulsion_Min_Load6_Supplier1__c,    
                                 Emulsion_Min_Load6_Supplier2__c,
                                 Emulsion_Min_Load6_Supplier3__c, //<-- new
                                 Emulsion_Min_Load7_Supplier1__c,
                                 Emulsion_Min_Load7_Supplier2__c, 
                                 Emulsion_Min_Load7_Supplier3__c, //<-- new
                                 Emulsion_Min_Load8_Supplier1__c,
                                 Emulsion_Min_Load8_Supplier2__c,
                                 Emulsion_Min_Load8_Supplier3__c, //<-- new
                                 Emulsions_Carrier_Supplier1__c,
                                 Emulsions_Carrier_Supplier2__c,
                                 Emulsions_Carrier_Supplier3__c, //<-- new
                                 Prices_F_O_B__c,
                                 Prices_F_O_B2__c,  //<-- new
                                 Prices_F_O_B3__c,  //<-- new                                     
                                 Fleet_ID__c,
                                 Fleet_ID2__c, //<-- new
                                 Fleet_ID3__c, //<-- new
                                 Rail_Acknowledgment__c,
                                 Rail_Acknowledgment2__c, //<-- new
                                 Rail_Acknowledgment3__c, //<-- new
                                 RR_City__r.Name,
                                 RR_City2__r.Name, //<-- new
                                 RR_City3__r.Name, //<-- new
                                 RR_Address__r.Name,
                                 RR_Address2__r.Name, //<-- new
                                 RR_Address3__r.Name, //<-- new
                                 Invoicing_Bill_To__r.Sequence__c,
                                 Invoicing_Bill_To2__r.Sequence__c, //<-- new
                                 Invoicing_Bill_To3__r.Sequence__c, //<-- new
                                 Freight_to_COLLECT__c,
                                 Freight_to_COLLECT2__c, //<-- new
                                 Freight_to_COLLECT3__c, //<-- new                                     
                                 Rail_Car_Routing__c,
                                 Rail_Car_Routing2__c, //<-- new
                                 Rail_Car_Routing3__c, //<-- new
                                 Freight_to_Pay__c,
                                 Freight_to_Pay2__c, //<-- new
                                 Freight_to_Pay3__c, //<-- new
                                 Freight_Pay_Method__c,
                                 Freight_Pay_Method2__c, //<-- new
                                 Freight_Pay_Method3__c, //<-- new
                                 Emulsion_Rate7_Competitor1__c,
                                 Emulsion_Rate7_Competitor2__c,
                                 Emulsion_Rate7_Competitor3__c,
                                 Emulsion_Rate7_Competitor4__c,
                                 cpm_Consignee__c,
                                 cpm_Consignee2__c,
                                 cpm_Consignee3__c
                                 
                                 FROM 
                                 Freight_ATS__r) 
                                FROM 
                                Opportunity 
                                WHERE 
                                (Opportunity.Id =: tender.Id AND Opportunity.StageName = 'Bid Analysis') OR
                                (Opportunity.Id =: tender.Id AND Opportunity.StageName = 'Sales Contract') OR
                                (Opportunity.Id =: tender.Id AND Opportunity.StageName = 'Closed Won Partial') OR 
                                (Opportunity.Id =: tender.Id AND Opportunity.StageName = 'Closed Won')], CustomerType); //StageName = 'Won' AND 
      
        
        
        
        
        //FROM Opportunity WHERE id in :opportunityList];  
        o = quote.getOpportunities();
        
        //System.debug('quote object: '+ o.size()); 
        
        ATS_Freight__c  f;
        if(o!=null)
            f= quote.getOpportunities()[0].Freight ; 
        
        //System.debug('freight id: ' + f.Id);
        //System.debug('freight city: ' + f.RR_City__r.Name);
        //System.debug('freight fleet: ' + f.Fleet_ID__c);
        
        if(f!=null) sequenceNumber = [Select Invoicing_Bill_To__r.Sequence__c from ATS_Freight__c   where Id = :f.Id limit 1][0].Invoicing_Bill_To__r.Sequence__c;                       	
        
        
        
        
        
        
        
        
        
        for(ATSWrapperUtilitiesOpp.ATSOpportunityWrapper opp : quote.getOpportunities()) {
            for(ATS_Freight__c freight:opp.obj.Freight_ATS__r){
                if(freight.cpm_Product_Type__c== 'Asphalt'){
                    asphaltFreight=freight;
                } else if(freight.cpm_Product_Type__c== 'Residual'){
                    residualFreight=freight; 
                }
                else if(freight.cpm_Product_Type__c== 'Emulsion'){
                    emulsionFreight=freight;
                }
            }
            
            for(ATSWrapperUtilitiesOpp.ATSOpportunityProductWrapper product : opp.products) {
                if(product.obj.cpm_Product_Family__c == 'Asphalt') {
                    asphaltLineItems.add(product);
                } else if(product.obj.cpm_Product_Family__c == 'Residual') {
                    residualsLineItems.add(product);
                } else if(product.obj.cpm_Product_Family__c == 'Emulsion') {
                    emulsionLineItems.add(product);
                }
            }
        }
    }
    
    public Boolean getIsAsphaltLineItemsEmpty() {
        return asphaltLineItems == null || asphaltLineItems.size() == 0;
    }
    
    public Boolean getIsResidualsLineItemsEmpty() {
        return residualsLineItems == null || residualsLineItems.size() == 0;
    }
    
    public Boolean getIsEmulsionLineItemsEmpty() {
        return emulsionLineItems == null || emulsionLineItems.size() == 0;
    }       
    
    
    
   /* 
  // --- move to ATSWrapperUtilities.ATSOpportunityWrapper ----
IncludeAxle5 = False;
IncludeAxle6 = False;
IncludeAxle7 = False;
IncludeAxle8 = False;

for(Opportunity opp: opportunities) {
// lets extract opportunity type without 'ATS' or 'Product Category'
String opportynityType = opp.RecordType.Name;
opportynityType = opportynityType.Replace('ATS:','');
opportynityType = opportynityType.Replace('Product Category','').trim();   

if(CustomerType.containsIgnoreCase(opportynityType)) {
oppWrapperList.add(new ATSWrapperUtilities.ATSOpportunityWrapper(opp));
}

       if(opp.RecordType.Name == 'ATS: Emulsion Product Category') {
for(OpportunityLineItem oli : opp.OpportunityLineItems) {
IncludeAxle5 = IncludeAxle5 || oli.Include_Axle_5__c;
IncludeAxle6 = IncludeAxle6 || oli.Include_Axle_6__c;
IncludeAxle7 = IncludeAxle7 || oli.Include_Axle_7__c;
IncludeAxle8 = IncludeAxle8 || oli.Include_Axle_8__c;
}
       }
}
    
    } */
    /*
private static String GetResourceURL(String resourceName) {

List<StaticResource> resourceList = [SELECT 
Name, 
NamespacePrefix, 
SystemModStamp 
FROM 
StaticResource 
WHERE 
Name = :resourceName];

if (resourceList.size() == 1) {
String namespace = resourceList[0].NamespacePrefix;
return '/resource/' 
+ resourceList[0].SystemModStamp.getTime() + '/' 
+ (namespace != null && namespace != '' ? namespace + '__' : '') 
+ resourceName; 
} 
else 
return '';
}*/
}