/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Abstract DAO class for Engine Configuration (MDT) SObject
History:        jschn 2019-07-02 - Created. - EPD R1
*************************************************************************************************/
public interface EPD_EngineConfigDAO {

    List<EPD_Engine__mdt> getEngineConfig(String manufacturer, String model);
    List<EPD_Engine__mdt> getEngineConfigs();

}