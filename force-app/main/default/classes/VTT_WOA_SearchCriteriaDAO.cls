/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DOA layer interface for WOA Search Criteria SObject queries for VTT Search Criteria purposes
Test Class:     VTT_WOA_SearchCriteriaSelectorTest
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public interface VTT_WOA_SearchCriteriaDAO {

    List<Work_Order_Activity_Search_Criteria__c> getUserActivitySearchFilters(Id pUserId);

}