@isTest(SeeAllData=true)
public class PTCalculatePCSOTestClass {
    private static testmethod void PCSOCreateCustomSettings(){
		PTTestData.PCSOCreateCustomSetting('allPCSOs',1.00);
        PTTestData.PCSOCreateCustomSetting('powerFactorReviewValue',0.90);
        PTTestData.PCSOCreateCustomSetting('reviewContractMinimumValue',0.30);
    }
    private static testmethod void CalculateZMPCSOS(){        
        test.startTest();
        PCSOCreateCustomSettings();
        PTTestData.createPTTestRecords();
        PTTestData.createZMBillingTestRecords();
        PTCalculatePCSO calcpcso = new PTCalculatePCSO();
        calcpcso.CalculatePCSO();        
        PTTestData.CreateInvalidPCSOTestRecord('D32');        
        PTCalculatePCSOCacheQuery.pcsoInfoQueried = false;
        //PTCalculatePCSOBillingCacheQuery.billingMapQueried = false;
        calcpcso.CalculatePCSO();
        test.stopTest();
    }
    private static testmethod void CalculatePFPCSOS(){
        test.startTest();
        PCSOCreateCustomSettings();
        PTTestData.createPTTestRecords();
        PTTestData.createPFBillingTestRecords();
        PTCalculatePCSO calcpcso = new PTCalculatePCSO();
        calcpcso.CalculatePCSO();        
        PTTestData.CreateInvalidPCSOTestRecord('D33');        
        PTCalculatePCSOCacheQuery.pcsoInfoQueried = false;
        //PTCalculatePCSOBillingCacheQuery.billingMapQueried = false;        
        calcpcso.CalculatePCSO();
        test.stopTest();
    }
    private static testmethod void CalculateRCMPCSOS(){
        test.startTest();
        PCSOCreateCustomSettings();
        PTTestData.createPTTestRecords();
        PTTestData.createRCMBillingTestRecords();
        PTCalculatePCSO calcpcso = new PTCalculatePCSO();
        calcpcso.CalculatePCSO();        
        PTTestData.CreateInvalidPCSOTestRecord('D35');        
        PTCalculatePCSOCacheQuery.pcsoInfoQueried = false;
		//PTCalculatePCSOBillingCacheQuery.billingMapQueried = false;        
        calcpcso.CalculatePCSO();
        test.stopTest();
    }
    private static testmethod void testZMPCSOImplemented(){
         PCSOCreateCustomSettings();
         PTTestData.createPTTestRecords();
         PTTestData.testZMPCSOImplemented();
         PTCalculatePCSO calcpcso = new PTCalculatePCSO();
         calcpcso.CalculatePCSO();
    }
    private static testmethod void testRCMPCSOImplemented(){
         PCSOCreateCustomSettings();
         PTTestData.createPTTestRecords();
         PTTestData.testRCMPCSOImplemented();
         PTCalculatePCSO calcpcso = new PTCalculatePCSO();
         calcpcso.CalculatePCSO();
    }
    private static testmethod void testPFPCSOImplemented(){
         PCSOCreateCustomSettings();
         PTTestData.createPTTestRecords();
         PTTestData.testPFPCSOImplemented();
         PTCalculatePCSO calcpcso = new PTCalculatePCSO();
         calcpcso.CalculatePCSO();
    }    
}