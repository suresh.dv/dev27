/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    EPD Iterator for EPD_EmailBatch
Test Class:     EPD_EPDIteratorTest
History:        jschn 2019-07-19 - Created. - EPD R1
*************************************************************************************************/
global inherited sharing class EPD_EPDIterator implements Iterator<EPD_Engine_Performance_Data__c> {

    List<EPD_Engine_Performance_Data__c> epdRecords {get; set;}
    Integer i {get; set;}
    Integer max;

    global EPD_EPDIterator( List<EPD_Engine_Performance_Data__c> epdRecords){
        this.epdRecords = epdRecords;
        this.max = epdRecords.size();
        this.i = 0;
    }

    global Boolean hasNext() {
        return i < epdRecords.size();
    }

    global EPD_Engine_Performance_Data__c next() {
        if(i == max){ return null; }
        i++;
        return epdRecords.get(i - 1);
    }

}