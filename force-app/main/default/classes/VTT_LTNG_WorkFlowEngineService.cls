/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Service class containing Business Logic for VTT Workflow Engine
Test Class:     VTT_LTNG_WorkFlowEngineServiceTest, VTT_LTNG_WorkFlowEngineHandlerTest
History:        mbrimus 2019-09-20 - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_WorkFlowEngineService {
    private static final String CLASS_NAME = String.valueOf(VTT_LTNG_WorkFlowEngineService.class);

    public HOG_CustomResponseImpl getEngineState(String workOrderActivityId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        Work_Order_Activity__c workOrderActivity;
        Contact tradesman;
        VTT_TradesmanFlags tradesmanInfo = new VTT_Utility_EndpointHandler().getTradesmanInfo();
        Work_Order_Activity_Log_Entry__c lastEntry = new Work_Order_Activity_Log_Entry__c();

        // Validation and fail-fast if woa Id is not in place
        if (workOrderActivityId == null) {
            response.success = false;
            response.addResult(new VTT_LTNG_EngineMessage('Activity Id is missing in request,' +
                    ' please refresh the page or contact Administrator',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_ERROR));
            return response;
        }
        // Activity with assignment record of current user
        List<Work_Order_Activity__c> activityRecords =
                VTT_LTNG_DAOProvider.engineDAO.getActivitiesWithAssignment(workOrderActivityId);
        List<Contact> tradesmanContacts =
                VTT_LTNG_DAOProvider.engineDAO.getTradesmanContacts();

        if (activityRecords.size() < 1) {
            response.success = false;
            response.addResult(new VTT_LTNG_EngineMessage('Activity with this Id has not been found' +
                    ' please contact administrator',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_ERROR));
            return response;
        } else {
            workOrderActivity = activityRecords.get(0);
        }

        // User has to have Contact setup
        if (tradesmanContacts.size() < 1) {
            response.success = false;
            response.addResult(new VTT_LTNG_EngineMessage('You dont have Tradesman contact setup,' +
                    ' please contact administrator if you are a Vendor',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_ERROR));
            return response;
        } else {
            tradesman = tradesmanContacts.get(0);
        }

        // Set last log entry
        List<Work_Order_Activity_Log_Entry__c> lastLogEntry =
                VTT_LTNG_DAOProvider.engineDAO.getLastLogEntryForTradesmanAndActivity(workOrderActivityId, tradesman.Id);
        if (lastLogEntry.size() > 0) {
            lastEntry = lastLogEntry.get(0);
        }

        VTT_LTNG_WorkFlowEngine engine = new VTT_LTNG_WorkFlowEngine.EngineBuilder()
                .addTradesman(tradesman)
                .addTradesmanInfo(tradesmanInfo)
                .addActivity(workOrderActivity)
                .addLastEntry(lastEntry)
                .checkForEPD()
                .build();

        // Add a notification for user if he has more then one contact referenced, that
        // We are going to be using first one from query
        if (tradesmanContacts.size() > 1) {
            engine.addMessage(new VTT_LTNG_EngineMessage(
                    'You have more then one tradesman assigned to your User account.' +
                            'You are using working as this tradesman for this activity: ',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_WARNING,
                    'Contact',
                    tradesman.Name,
                    tradesman.Id
            ));
        }

        response.addResult(engine);
        return response;
    }

    public HOG_CustomResponseImpl suppressEPD(Boolean flag, String activityId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        // Activity with assignment record of current user
        List<Work_Order_Activity__c> activityRecords =
                VTT_LTNG_DAOProvider.engineDAO.getActivitiesWithAssignment(activityId);
        if (activityRecords.size() > 0) {
            Work_Order_Activity__c ac = activityRecords.get(0);
            ac.Maintenance_Work_Order__r.EPD_Suppressed__c = flag;
            update ac.Maintenance_Work_Order__r;

            if (flag) {
                EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = false;
                delete [SELECT Id FROM EPD_Engine_Performance_Data__c WHERE Work_Order__c = :ac.Maintenance_Work_Order__c];
                EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = true;
            }
        } else {
            response.addError('Work Order Activity was not found');
            return response;
        }

        return response;
    }

    public HOG_CustomResponseImpl hasActivityChanged(Work_Order_Activity__c originalActivity) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Boolean needRefresh = optimisticLockCheck(originalActivity);
        response.addResult(needRefresh);
        return response;
    }

    public HOG_CustomResponseImpl canActivityBePutOnHold(Work_Order_Activity__c originalActivity, Contact tradesman) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Boolean canActivityBePutOnHold = true;
        String message = '';

        Work_Order_Activity__c refreshedActivity =
                VTT_LTNG_DAOProvider.engineDAO.getLatestVersionOfActivity(originalActivity.Id);


        if (!optimisticLockCheck(originalActivity)) {
            canActivityBePutOnHold = false;

            // NOTE: this else may never happen... but since this logic was in place in original
            // Engine, we will migrate it.. Reason why it cannot happen is this will only happen
            // Is because onhold button is rendered if nobody else that this tradesman is working on it
            // One way it could happen is if this tradesman screen has stale data and se sees this button
            // But meanwhile somebody started work.. then once he tryies to do onhold it should enter this
            // But it wont, because guy who started earlier already changed lastmodif date (i think rollups are doing these)
            // ... but who knows...
        } else {
            for (Contact workingTradesman : refreshedActivity.Working_Tradesmen__r) {
                if (workingTradesman.Id != tradesman.Id) {
                    message = workingTradesman.Name + ' is currently working on this Activity. You can\'t put this Activity on hold.';
                    canActivityBePutOnHold = false;
                }
            }
        }
        // Dont change the order..
        response.addResult(canActivityBePutOnHold);
        response.addResult(message);
        return response;
    }

    public HOG_CustomResponseImpl getDataForJobComplete(Work_Order_Activity__c activity, Contact tradesman) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Map<String, Object> resultsMap = new Map<String, Object>();

        List<Work_Order_Activity__c> activityRecords =
                VTT_LTNG_DAOProvider.engineDAO.getActivitiesWithAssignment(activity.Id);
        if (activityRecords.size() < 1) {
            response.success = false;
            response.addResult(new VTT_LTNG_EngineMessage('Activity with this Id has not been found' +
                    ' please contact administrator',
                    VTT_LTNG_WorkFlowEngineConstants.SEVERITY_ERROR));
            return response;
        }

        resultsMap.put('otherWorkingTradesman', new List<Contact>());
        resultsMap.put('availableActivitiesToComplete', new List<VTT_LTNG_AvailableActivity>());
        resultsMap.put('runningTally', 0);
        resultsMap.put('showPartsDamageCause', false);

        // Sets flag to see if we need to show parts damage and cause
        Work_Order_Activity__c woa = activityRecords.get(0);
        if (isEquipmentWorkOrder(activity.Maintenance_Work_Order__r)) {
            Boolean showPartsDamageCause = !MaintenanceServicingUtilities.SkipPartsDamageCause(woa.Maintenance_Work_Order__r) &&
                    !MaintenanceServicingUtilities.SkipPartsDamageCauseSysAdmin(woa.Maintenance_Work_Order__r);
            resultsMap.put('showPartsDamageCause', showPartsDamageCause);
            if (showPartsDamageCause) {
                resultsMap.put('partsSelection', VTT_LTNG_DAOProvider.engineDAO.getPartsByCatalogueCode(woa.Maintenance_Work_Order__r.Equipment__r.Catalogue_Code__c));
                resultsMap.put('causesSelection', VTT_LTNG_DAOProvider.engineDAO.getCause());
            }
        }


        // Other working tradesman
        List<Contact> assignedTradesmenList =
                VTT_LTNG_DAOProvider.engineDAO.getAssignedTradesman(activity.Id, UserInfo.getUserId());
        List<Contact> otherWorkingTradesmen =
                VTT_LTNG_DAOProvider.engineDAO.getOtherWorkingTradesman(activity, assignedTradesmenList, tradesman);
        resultsMap.put('otherWorkingTradesman', otherWorkingTradesmen);

        // Available Activities for Complete
        List<VTT_LTNG_AvailableActivity> availableActivitiesToComplete =
                VTT_LTNG_DAOProvider.engineDAO.getAvailableActivitiesToComplete(tradesman, activity);
        resultsMap.put('availableActivitiesToComplete', availableActivitiesToComplete);

        // Running Tally Time
        List<Work_Order_Activity_Log__c> activityLogs = VTT_LTNG_DAOProvider.engineDAO.getActivityLogs(activity.Id, UserInfo.getUserId());
        Double runningTally = VTT_LTNG_DAOProvider.engineDAO.getRunningTallyTime(activity, activityLogs);
        resultsMap.put('runningTally', runningTally);

        // Work Execution and Close out Checklist
        resultsMap.put('closeOutChecklistId', null);
        resultsMap.put('isPermitHolder', false);
        List<HOG_Work_Execution_Close_Out_Checklist__c> permitHolderData =
                VTT_LTNG_DAOProvider.engineDAO.getCloseOutChecklistForToday(activity, tradesman);

        if (!permitHolderData.isEmpty()) {
            resultsMap.put('closeOutChecklistId', permitHolderData.get(0).Id);
            if (permitHolderData.get(0).Permit_Holder_Name__c == tradesman.Id) {
                resultsMap.put('isPermitHolder', true);
            }
        }

        response.addResult(resultsMap);
        return response;
    }

    public HOG_CustomResponseImpl getPermitHolderData(Work_Order_Activity__c activity, Contact tradesman) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Map<String, Object> resultsMap = new Map<String, Object>();
        resultsMap.put('checkListId', null);
        resultsMap.put('isPermitHolder', false);
        List<HOG_Work_Execution_Close_Out_Checklist__c> permitHolderData =
                VTT_LTNG_DAOProvider.engineDAO.getCloseOutChecklistForToday(activity, tradesman);

        if (!permitHolderData.isEmpty()) {
            resultsMap.put('checkListId', permitHolderData.get(0).Id);
            if (permitHolderData.get(0).Permit_Holder_Name__c == tradesman.Id) {
                resultsMap.put('isPermitHolder', true);
            }
        }

        response.addResult(resultsMap);
        return response;
    }

    public HOG_CustomResponseImpl getDamage(String partCode) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        response.addResult(VTT_LTNG_DAOProvider.engineDAO.getDamageByPartCode(partCode));
        return response;
    }

    public void handleJobOnHoldEnd(
            VTT_LTNG_Request request,
            VTT_LTNG_EngineAction action,
            Work_Order_Activity__c activity,
            Contact tradesman
    ) {
        List<Contact> tradesmanListForUpdate = VTT_LTNG_DAOProvider.engineDAO.getAssignedTradesman(activity.Id, UserInfo.getUserId());

        LogEntryAction(
                tradesman,
                activity,
                action.logEntryAction,
                request.currentLatitude,
                request.currentLongitude,
                request.actionComment,
                action.getNewTradesmanStatus(),
                action.getNewActivityStatus(),
                request.onHoldReason,
                tradesmanListForUpdate,
                null
        );
    }

    public void handleRejectActivity(
            VTT_LTNG_Request request,
            Work_Order_Activity__c activity,
            Contact tradesman
    ) {
        List<Work_Order_Activity_Assignment__c> nonRejectedAssignmentFor =
                VTT_LTNG_DAOProvider.engineDAO.getNonRejectedAssignment(activity.Id, tradesman.Id);

        if (nonRejectedAssignmentFor.size() > 0) {
            Work_Order_Activity_Assignment__c rec = nonRejectedAssignmentFor[0];
            rec.Rejected__c = true;
            rec.Reject_Reason__c = request.actionComment;
            update rec;
        }
    }

    public void handleGenericActivityEnd(
            VTT_LTNG_Request request,
            VTT_LTNG_EngineAction action,
            Work_Order_Activity__c activity,
            Contact tradesman) {

        //Get Prior tradesman status
        String priorTradesmanStatus = tradesman.Tradesman_Status__c;
        String newTradesmanStatus = action.getNewTradesmanStatus();

        System.debug(CLASS_NAME + ' newTradesmanStatus before ' + newTradesmanStatus);

        // Default Action
        LogEntryAction(
                tradesman,
                activity,
                action.logEntryAction,
                request.currentLatitude,
                request.currentLongitude,
                request.actionComment,
                newTradesmanStatus,
                action.getNewActivityStatus(),
                null,
                new List<Contact>(),
                null
        );

        System.debug(CLASS_NAME + ' newTradesmanStatus after ' + newTradesmanStatus);
        System.debug(CLASS_NAME + ' priorTradesmanStatus ' + newTradesmanStatus);
        System.debug(CLASS_NAME + ' logEntryAction ' + action.logEntryAction);

        if (newTradesmanStatus == VTT_Utilities.TRADESMAN_STATUS_PREWORK
                && priorTradesmanStatus == VTT_Utilities.TRADESMAN_STATUS_STARTEDWORK) {

            VTT_LTNG_EngineAction startEquipmentAction =
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTATSITE);
            LogEntryAction(
                    tradesman,
                    activity,
                    startEquipmentAction.logEntryAction,
                    request.currentLatitude,
                    request.currentLongitude,
                    request.actionComment,
                    startEquipmentAction.getNewTradesmanStatus(),
                    startEquipmentAction.getNewActivityStatus(),
                    null,
                    new List<Contact>(),
                    null
            );
        }
    }

    public void handleGenericActivityEndWithCheckList(
            VTT_LTNG_Request request,
            VTT_LTNG_EngineAction action,
            Work_Order_Activity__c activity,
            Contact tradesman,
            HOG_Work_Execution_Close_Out_Checklist__c checklist
    ) {
        System.debug('Saving ' + checklist);
        handleGenericActivityEnd(request, action, activity, tradesman);
        saveCheckList(activity, tradesman, checklist);
    }

    public void handleJobOnHoldEndWithCheckList(
            VTT_LTNG_Request request,
            VTT_LTNG_EngineAction action,
            Work_Order_Activity__c activity,
            Contact tradesman,
            HOG_Work_Execution_Close_Out_Checklist__c checklist
    ) {
        System.debug('Saving ' + checklist);
        handleJobOnHoldEnd(request, action, activity, tradesman);
        saveCheckList(activity, tradesman, checklist);
    }

    public void handleStartJobAtEquipmentAction(
            VTT_LTNG_Request request,
            VTT_LTNG_EngineAction action,
            Work_Order_Activity__c activity,
            Contact tradesman) {

        // Default Action = Start Job
        LogEntryAction(
                tradesman,
                activity,
                action.logEntryAction,
                request.currentLatitude,
                request.currentLongitude,
                request.actionComment,
                action.getNewTradesmanStatus(),
                action.getNewActivityStatus(),
                null,
                new List<Contact>(),
                null
        );

        // Default Action = Start Job At Equipment
        VTT_LTNG_EngineAction startEquipmentAction =
                VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTATSITE);
        LogEntryAction(
                tradesman,
                activity,
                startEquipmentAction.logEntryAction,
                request.currentLatitude,
                request.currentLongitude,
                request.actionComment,
                startEquipmentAction.getNewTradesmanStatus(),
                startEquipmentAction.getNewActivityStatus(),
                null,
                new List<Contact>(),
                null
        );
    }

    public void handleStartJobAtEquipmentActionWithCheckList(VTT_LTNG_Request request,
            VTT_LTNG_EngineAction action,
            Work_Order_Activity__c activity,
            Contact tradesman,
            HOG_Work_Execution_Close_Out_Checklist__c checklist) {
        handleStartJobAtEquipmentAction(request, action, activity, tradesman);
        saveCheckList(activity, tradesman, checklist);
    }

    public void handleJobComplete(VTT_LTNG_Request request,
            VTT_LTNG_EngineAction action,
            Work_Order_Activity__c activity,
            Contact tradesman) {
        System.debug('ACTION ' + action);
        System.debug('request.partItem ' + request.partItem);
        System.debug('request.damageItem ' + request.damageItem);
        System.debug('request.causeItem ' + request.causeItem);

        if (isEquipmentWorkOrder(activity.Maintenance_Work_Order__r)) {
            activity.Part__c = String.isEmpty(request.partItem) ? null : request.partItem;
            activity.Damage__c = String.isEmpty(request.damageItem) ? null : request.damageItem;
            activity.Cause__c = String.isEmpty(request.causeItem) ? null : request.causeItem;
            activity.Cause_Text__c = request.causeDescription;
            activity.Damage_Text__c = request.damageDescription;
        } else {
            List<HOG_Cause__c> result = [
                    SELECT
                            Id,
                            Cause_Code__c,
                            Cause_Description__c
                    FROM HOG_Cause__c
                    WHERE Cause_Description__c LIKE'Other%'
                    LIMIT 1
            ];
            activity.Cause__c = (result.size() > 0) ? result.get(0).Id : null;
        }

        VTT_Utilities.executeTriggerCode = false;
        upsert activity;
        VTT_Utilities.executeTriggerCode = true;

        if(request.requestReschedule){
            Work_Order_Activity_Reschedule_Request__c requestToSchedule = new Work_Order_Activity_Reschedule_Request__c (
                    Work_Order_Activity__c = activity.Id,
                    Tradesman__c = tradesman.Id,
                    Tech_s_Comments__c = request.actionComment
            );
            // mbrimus - this forces recalculate and then runs triggers so i have added flag to disable it
            VTT_Utilities.executeTriggerCode = false;
            insert requestToSchedule;
            VTT_Utilities.executeTriggerCode = true;
        }

        // Call JOB COMPLETE LOGIC NOW
        // If nobody else is working
        if(!request.isOtherTradesmanStillWorking){

            List<Contact> assignedTradesmenList = VTT_LTNG_DAOProvider.engineDAO.getAssignedTradesman(activity.Id, UserInfo.getUserId());
            List<Contact> TradesmanListforUpdate = new List<Contact>();
            for(Contact tradesmenRec : assignedTradesmenList) {
                //update statuses for assigned tradesmen only if they currently working on the same activity
                if(tradesmenRec.Current_Work_Order_Activity__c == activity.Id) {
                    TradesmanListforUpdate.add(tradesmenRec);
                }
            }

            if(tradesman.Tradesman_Status__c == VTT_Utilities.TRADESMAN_STATUS_WORKING &&
                    VTT_Utilities.IsThermalActivity(activity.Id)) {
                VTT_LTNG_EngineAction finishAtEquipmentAction = new VTT_LTNG_EngineAction(
                        VTT_Utilities.LOGENTRY_FINISHEDATSITE, null, null, 4
                );
                // Default Action
                LogEntryAction(
                        tradesman,
                        activity,
                        finishAtEquipmentAction.logEntryAction,
                        request.currentLatitude,
                        request.currentLongitude,
                        request.actionComment,
                        finishAtEquipmentAction.getNewTradesmanStatus(),
                        finishAtEquipmentAction.getNewActivityStatus(),
                        null,
                        TradesmanListforUpdate,
                        null
                );
            }

            /*Complete selected activities*/
            List<VTT_WorkFlowEngine.AvailableActivity> activitiesToComplete = new List<VTT_WorkFlowEngine.AvailableActivity>();
            for(VTT_LTNG_AvailableActivity obj :request.activitiesToComplete) {
                if(obj.markToComplete) {
                    VTT_WorkFlowEngine.AvailableActivity avtc = new VTT_WorkFlowEngine.AvailableActivity(obj.activityRecord);
                    avtc.MarkToComplete = obj.markToComplete;
                    avtc.userDurationInput = obj.userDurationInput;
                    activitiesToComplete.add(avtc);
                }
            }

            // Placeholder until business decides what to do
            // runningTally should have remainder of duration available for this activity
            // this will add log entry with status Auto-Complete but only if something is selected and running tally value is valid
            Decimal residualTimeDuration = 0;
            if(activitiesToComplete.size() > 0){
                if(request.runningTally < 1){
                    throw new HOG_Exception('Running Tally cannot be less then 1');
                }
                VTT_WorkFlowEngine.AvailableActivity activityThatStartedThis = new VTT_WorkFlowEngine.AvailableActivity(activity, true);
                activityThatStartedThis.userDurationInput = request.runningTally;
                residualTimeDuration = request.runningTally;
                activitiesToComplete.add(activityThatStartedThis);
            }

            //moved this log entry action here, to get value in residualTimeDuration and have it in the
            //Job Complete log Entry on the initial activity when using autocomplete
            //(It's needed to populate Work Start on final Job Complete confirmation, that is created in trigger after insert
            //Log Entry, and Auto Complete Log Entry for initial activity is not yet created, beceasue it's created after this method below)
            LogEntryAction(
                    tradesman,
                    activity,
                    action.logEntryAction,
                    request.currentLatitude,
                    request.currentLongitude,
                    request.actionComment,
                    action.getNewTradesmanStatus(),
                    action.getNewActivityStatus(),
                    null,
                    TradesmanListforUpdate,
                    residualTimeDuration
            );

            VTT_Utilities.AutoCompleteActivities(
                    tradesman,
                    activitiesToComplete,
                    request.currentLatitude,
                    request.currentLongitude,
                    'Autocompleted with activity: ' + activity.Name);

            // OnComplete update notif
            VTT_IntegrationUtilities.SAP_UpdateNotification(activity.Id);

        // If Somebody else is working on AC
        } else {
            if(tradesman.Tradesman_Status__c == VTT_Utilities.TRADESMAN_STATUS_WORKING) {
                VTT_LTNG_EngineAction finishAtEquipmentAction = new VTT_LTNG_EngineAction(
                        VTT_Utilities.LOGENTRY_FINISHEDATSITE, null, null, 4
                );
                LogEntryAction(
                        tradesman,
                        activity,
                        finishAtEquipmentAction.logEntryAction,
                        request.currentLatitude,
                        request.currentLongitude,
                        request.actionComment,
                        finishAtEquipmentAction.getNewTradesmanStatus(),
                        finishAtEquipmentAction.getNewActivityStatus(),
                        null,
                        new List<Contact>(),
                        null
                );
            }

            VTT_LTNG_EngineAction finishedForTheDayAction = new VTT_LTNG_EngineAction(
                    VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY, null, null, 4
            );

            LogEntryAction(
                    tradesman,
                    activity,
                    finishedForTheDayAction.logEntryAction,
                    request.currentLatitude,
                    request.currentLongitude,
                    request.actionComment,
                    finishedForTheDayAction.getNewTradesmanStatus(),
                    finishedForTheDayAction.getNewActivityStatus(),
                    null,
                    new List<Contact>(),
                    null
            );

            // OnComplete update notif
            VTT_IntegrationUtilities.SAP_UpdateNotification(activity.Id);

        }

    }

    private void LogEntryAction(
            Contact pTradesman,
            Work_Order_Activity__c pWoActivity,
            String pActionName,
            Decimal pCurrentLatitude,
            Decimal pCurrentLongitude,
            String pActionComment,
            String pNewTradesmanStatus,
            String pNewActivityStatus,
            String pNewActivityStatusReason,
            List<Contact> pTradesmanList,
            Decimal residualTimeDuration
    ) {
        system.debug('ActionComment');
        system.debug(pActionComment);

        system.debug('AssignedTradesmenList');
        system.debug(pTradesmanList);

        List<Work_Order_Activity_Log_Entry__c> logEntryList = new List<Work_Order_Activity_Log_Entry__c>();


        Work_Order_Activity_Log_Entry__c logEntry = VTT_Utilities.CreateLogEntry(pTradesman,
                pWoActivity, pActionName, pCurrentLatitude, pCurrentLongitude, pNewActivityStatusReason, pActionComment);

        if (pActionName == VTT_Utilities.LOGENTRY_JOBCOMPLETE && residualTimeDuration != 0) {
            logEntry.Initial_Activity_Residual_Time_Duration__c = residualTimeDuration;
        }

        logEntryList.add(logEntry);

        List<Contact> TradesmanListforUpdate = new List<Contact>();
        TradesmanListforUpdate.add(pTradesman);

        for (Contact tradesmenRec : pTradesmanList) {
            logEntry = VTT_Utilities.CreateLogEntry(tradesmenRec,
                    pWoActivity,
                    pActionName,
                    pCurrentLatitude,
                    pCurrentLongitude,
                    pNewActivityStatusReason,
                    null);
            system.debug('InLoop logEntry' + logEntry);
            logEntryList.add(logEntry);
            system.debug('InLoop logEntryList' + logEntryList);
            TradesmanListforUpdate.add(tradesmenRec);
            system.debug('InLoop TradesmanListforUpdate' + TradesmanListforUpdate);
        }
        system.debug('AfterLoopInserting logEntryList' + logEntryList);


        insert logEntryList;
        VTT_Utilities.RefreshActivityWorkDetails(pWoActivity);
        VTT_Utilities.UpdateTradesmanStatus(TradesmanListforUpdate, pNewTradesmanStatus, pWoActivity.Id);
        System.debug('UpdateTradesmanStatus ' + pNewTradesmanStatus);
        VTT_Utilities.UpdateActivityStatuses(new List<Work_Order_Activity__c>{
                pWoActivity
        },
                pNewActivityStatus, pNewActivityStatusReason, true /*isWorkDetailsUpdated*/);
        if (pActionName == VTT_Utilities.LOGENTRY_JOBONHOLD) {
            System.debug('HERE NotifyLeadsAboutOnHoldActivity');
            VTT_EmailNotifications notificationService = new VTT_EmailNotifications();
            notificationService.NotifyLeadsOnHoldActivity(pWoActivity, pTradesman);
        }
    }

    private void saveCheckList(Work_Order_Activity__c activity,
            Contact tradesman,
            HOG_Work_Execution_Close_Out_Checklist__c checklist) {
        checklist.Permit_Holder_Name__c = tradesman.Id;
        checklist.Company_Name_Lookup__c = tradesman.AccountId;
        checklist.Work_Order_Activity__c = activity.Id;
        upsert checklist;
    }

    public static Boolean optimisticLockCheck(Work_Order_Activity__c originalActivity) {
        Work_Order_Activity__c refreshedActivity =
                VTT_LTNG_DAOProvider.engineDAO.getLatestVersionOfActivity(originalActivity.Id);
        if (refreshedActivity.LastModifiedById == originalActivity.LastModifiedById &&
                refreshedActivity.LastModifiedDate == originalActivity.LastModifiedDate) {
            return true;
        } else {
            return false;
        }
    }

    //3 July 2019 - jschn - EPD R1 - W-001507
    public static Boolean hasEPDSubmitted(Work_Order_Activity__c activity) {
        return EPD_DAOProvider.EPDDAO.getCountOfSubmittedRecords(activity.Id) > 0;
    }

    public static Boolean isEPDFormRequired(Work_Order_Activity__c activity) {
        Boolean isEPDFormRequired = false;
        try {
            isEPDFormRequired = new EPD_EPDService().isEPDRequired(
                    String.isNotBlank(activity.Equipment__c) ? activity.Equipment__c : activity.Maintenance_Work_Order__r.Equipment__c,
                    activity.Maintenance_Work_Order__r
            );
        } catch (Exception ex) {
            System.debug(ex.getMessage());
            isEPDFormRequired = false;
        }

        return isEPDFormRequired;
    }

    private Boolean isEquipmentWorkOrder(HOG_Maintenance_Servicing_Form__c workOrder) {
        System.debug('equip ' + workOrder);
        return workOrder.Equipment__c <> null && workOrder.Equipment__r.Catalogue_Code__c <> null;
    }
}