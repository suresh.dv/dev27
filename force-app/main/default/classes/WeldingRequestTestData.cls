public class WeldingRequestTestData {
    
    public static Welding_Request__c createWeldingRequest() {
        Welding_Request__c request = new Welding_Request__c();
        request.Business_Unit__c = 'HLR';
        request.Area_Name__c = 'HLU';
        request.PWHT__c = 'Yes';
        request.Equipment_Piping_Pipeline_Tank__c = 'Tank (API Code)';
        request.Welding_Engineering_Specification__c = 'Husky Engineering Specification PS-MW-01, Rev 2';
        request.Status__c = 'Draft';
        request.MDMT__c = '50 Celcius';
        request.Material_Thickness__c = '3.2 mm';
        request.Item_Being_fabricated_or_repaired__c = 'Fabrication';
        request.Service__c = 'Service';
        request.Material_1__c = 'P. No. 1';
        request.Material_2__c = 'P. No. 1';
        request.Material_type_being_joined__c = 'ASME';
        request.Project_Name__c = 'Project Name';
        
        insert request;
        return request;
    }
}