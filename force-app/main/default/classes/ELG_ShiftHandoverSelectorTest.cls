/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for ELG_ShiftHandoverSelector
History:        mbrimus 2019-07-15 - Created.
*************************************************************************************************/
@IsTest
public class ELG_ShiftHandoverSelectorTest {

    @IsTest
    static void getUsersSettings_returnSettingsForUser() {

        // GIVEN
        ELG_TestDataFactory.creteUserSetting(true);
        List<ELG_User_Setting__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getUsersSettings();
        Test.stopTest();

        // THEN
        System.assertEquals(1, result.size());
    }

    @IsTest
    static void getAvailablePostsForAMU_noParams() {

        // GIVEN
        ELG_TestDataFactory.createPosts('PostName', 5, true);
        List<ELG_Post__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getAvailablePostsForAMU(null);
        Test.stopTest();

        // THEN
        System.assertEquals(0, result.size());
    }

    @IsTest
    static void getAvailablePostsForAMU_withParams() {

        // GIVEN
        List<ELG_Post__c> posts = ELG_TestDataFactory.createPosts('PostName', 5, true);
        List<ELG_Post__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getAvailablePostsForAMU(posts.get(0).Operating_Field_AMU__c);
        Test.stopTest();

        // THEN
        System.assertEquals(5, result.size());
    }

    @IsTest
    static void getCurrentShiftAssignmentForUser_returnActiveShift() {

        // GIVEN
        ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(false);
        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', true);
        assignement.Post__c = post.Id;
        insert assignement;
        List<ELG_Shift_Assignement__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getCurrentShiftAssignmentForUser(post.Operating_Field_AMU__c);
        Test.stopTest();

        // THEN
        System.assertEquals(1, result.size());
    }

    @IsTest
    static void getAllLogsForActiveShiftAndPost_noParams() {

        // GIVEN
        Field__c amu = HOG_TestDataFactory.createAMU(true);

        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', false);
        post.Operating_Field_AMU__c = amu.Id;
        insert post;

        ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(post, true);
        ELG_TestDataFactory.createLogEntries(10,
                amu,
                post,
                assignement, true);

        List<ELG_Log_Entry__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getAllLogsForActiveShiftAndPost(null, null);
        Test.stopTest();

        // THEN
        System.assertEquals(0, result.size());
    }

    @IsTest
    static void getAllLogsForActiveShiftAndPost_withParams() {

        // GIVEN
        Field__c amu = HOG_TestDataFactory.createAMU(true);

        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', false);
        post.Operating_Field_AMU__c = amu.Id;
        insert post;

        ELG_Shift_Assignement__c assignement = ELG_TestDataFactory.createShiftAssignement(post, true);
        ELG_TestDataFactory.createLogEntries(10,
                amu,
                post,
                assignement, true);

        List<ELG_Log_Entry__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getAllLogsForActiveShiftAndPost(post.Id, amu.Id);
        Test.stopTest();

        // THEN
        System.assertEquals(10, result.size());
    }

    @IsTest
    static void getHandoverByShift_withoutParams() {

        // GIVEN
        Field__c amu = HOG_TestDataFactory.createAMU(true);

        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', false);
        post.Operating_Field_AMU__c = amu.Id;
        insert post;

        // This will also create handover doc, from process builder
        ELG_TestDataFactory.createShiftAssignement(post, true);

        ELG_Shift_Handover__c result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getHandoverByShift(null);
        Test.stopTest();

        // THEN
        System.assertEquals(null, result);
    }

    @IsTest
    static void getHandoverByShift_withParams() {

        // GIVEN
        Field__c amu = HOG_TestDataFactory.createAMU(true);

        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', false);
        post.Operating_Field_AMU__c = amu.Id;
        insert post;

        // This will also create handover doc, from process builder
        ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);

        ELG_Shift_Handover__c result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getHandoverByShift(shift.Id);
        Test.stopTest();

        // THEN
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getHandoverForIncomingOperator_returnHandoverForIncomingOperator() {

        // GIVEN
        Field__c amu = HOG_TestDataFactory.createAMU(true);

        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', false);
        post.Operating_Field_AMU__c = amu.Id;
        insert post;

        // This will also create handover doc, from process builder
        ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, false);
        shift.Shift_Status__c = ELG_Constants.SHIFT_HANDOVER_IN_PROGRESS;
        insert shift;

        ELG_Shift_Handover__c handover = [
                SELECT Id,
                        Incoming_Operator__c
                FROM ELG_Shift_Handover__c
                WHERE Shift_Assignement__c =: shift.Id];
        handover.Incoming_Operator__c = UserInfo.getUserId();
        update handover;

        ELG_Shift_Handover__c result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getHandoverByShift(shift.Id);
        Test.stopTest();

        // THEN
        System.assertNotEquals(null, result);
    }

    @IsTest
    static void getAllQuestionCategories_returnAllQuestionCategories() {

        // GIVEN
        ELG_TestDataFactory.createQuestionCategories(10, true);
        List<ELG_Handover_Category__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getAllQuestionCategories();
        Test.stopTest();

        // THEN
        System.assertEquals(10, result.size());
    }

    @IsTest
    static void getAllActiveQuestions_returnAllActiveQuestions() {

        // GIVEN
        ELG_TestDataFactory.createQuestions('test', 10, true);
        List<ELG_Handover_Category_Question__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getAllActiveQuestions();
        Test.stopTest();

        // THEN
        System.assertEquals(10, result.size());
    }

    @IsTest
    static void getSteamShiftWithMissingShiftEngineer_withoutParams() {

        // GIVEN
        Field__c amu = HOG_TestDataFactory.createAMU(true);

        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', false);
        post.Operating_Field_AMU__c = amu.Id;
        post.Shift_Engineer_Review_Required__c = true;
        insert post;

        ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, false);
        shift.Shift_Status__c = ELG_Constants.SHIFT_HANDOVER_IN_PROGRESS;
        insert shift;

        List<ELG_Shift_Assignement__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getSteamShiftWithMissingShiftEngineer(null);
        Test.stopTest();

        // THEN
        System.assertEquals(0, result.size());
    }

    @IsTest
    static void getSteamShiftWithMissingShiftEngineer_withParams() {

        // GIVEN
        Field__c amu = HOG_TestDataFactory.createAMU(true);

        ELG_Post__c post = ELG_TestDataFactory.createPost('Post', false);
        post.Operating_Field_AMU__c = amu.Id;
        post.Shift_Engineer_Review_Required__c = true;
        insert post;

        ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, false);
        shift.Shift_Status__c = ELG_Constants.SHIFT_HANDOVER_IN_PROGRESS;
        insert shift;

        List<ELG_Shift_Assignement__c> result;

        // WHEN
        Test.startTest();
        result = new ELG_ShiftHandoverSelector().getSteamShiftWithMissingShiftEngineer(amu.Id);
        Test.stopTest();

        // THEN
        System.assertEquals(1, result.size());
    }

}