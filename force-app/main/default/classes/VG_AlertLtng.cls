/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Test Class: 	VG_AlertLtngTest
Description:    Vent Gas Alert endpoint service class.
History:        jschn 07.10.2018 - Created.
**************************************************************************************************/
public with sharing class VG_AlertLtng {
	
    public static final String CLASS_NAME = 'VG_AlertLtng';

    /**
     * Retrieve HOG Vent Gas Alert record based on record Id.
     * @param  alertId [Alert Id]
     * @return         [HOG_Vent_Gas_Alert__c record]
     */

	@AuraEnabled public static HOG_RequestResult getAlert_v1(String alertId) {
    	System.debug(CLASS_NAME + ' -> AlertID: ' + alertId);
    	HOG_RequestResult result;
    	try {
			HOG_Vent_Gas_Alert__c alert = [
					SELECT Id
							, Name
							, CreatedById
							, AMU__c
							, Description__c
							, Comments__c
							, End_Date__c
							, GOR_Effective_Date__c
							, GOR_Test_Date__c
							, Measured_Vent_Rate__c
							, Monthly_Trucked_Oil__c
							, Predicted_Reported_Vent_Rate__c
							, PVR_Fuel_Consumption__c
							, PVR_Fuel_Consumption_Rate__c
							, PVR_GOR_Factor__c
							, Total_Gas_Production__c
							, My_Alert__c
							, Priority__c
							, Production_Engineer__c
							, Production_Engineer__r.Name
							, Start_Date__c
							, Status__c
							, Type__c
							, Well_Location__c
							, Well_Location__r.Name
							, Well_Location__r.Operating_Field_AMU__c
							, Well_Location__r.Operating_Field_AMU__r.Name
					FROM HOG_Vent_Gas_Alert__c
					WHERE Id =: alertId
			];
    		result = new HOG_SelectResponseImpl(alert);
    	} catch(Exception ex) {
    		result = new HOG_SelectResponseImpl(ex);
    		System.debug(CLASS_NAME + ' -> getAlert_v1 exception: ' + ex.getMessage());
    	}
    	return result;
    }


	/**
	 * Updates VG alert record passed as parameter.
	 * @param  vgAlert [JSON serialized record of Vent Gas Alert]
	 * @return         [Upsert Wrapper with result]
	 */
	@AuraEnabled public static HOG_RequestResult upsertAlert_v1(String vgAlert) {
		HOG_RequestResult result;
		Savepoint sp = Database.setSavepoint();
        try {
        	HOG_Vent_Gas_Alert__c ventGasAlert = deserializeAlert(vgAlert);
        	update ventGasAlert;
        	result = new HOG_DMLResponseImpl(ventGasAlert);
        } catch(Exception ex) {
        	Database.rollback(sp);
			System.debug(CLASS_NAME + ' -> '+ ex.getMessage());
        	result = new HOG_DMLResponseImpl(ex);
        }
        return result;
	}

	/**
	 * Retrieve user permissions based on logged in user
	 * @param  alertId [Alert Id]
	 * @return  [UserPermissionsWrapper populated with results]
	 */
	@AuraEnabled public static HOG_RequestResult getUserPermissionsForAlert(String alertId) {
		System.debug(CLASS_NAME + ' -> getUserPermissionsForAlert alertId: ' + alertId);
    	HOG_RequestResult result;
		try {
			Boolean canEdit = HOG_VentGas_Utilities.validateAlertEditPrivileges(alertId);
			result = new HOG_CustomResponseImpl(new UserPermissionsWrapper(canEdit));
		} catch(Exception ex) {
			result = new HOG_CustomResponseImpl(ex);
			System.debug(CLASS_NAME + ' -> getAlert_v1 exception: ' + ex.getMessage());
		}
		return result;
	}

	/**
	 * Deserialize VG Alert record.
	 * @param  vgAlert [JSON of VG Alert record to deserialize]
	 * @return         [deserialized VG Alert record]
	 */
	private static HOG_Vent_Gas_Alert__c deserializeAlert(String vgAlert) {		
		return (HOG_Vent_Gas_Alert__c) JSON.deserialize(vgAlert, HOG_Vent_Gas_Alert__c.class);
	}

	/**
	 * Wrapper class for Vent Gas User permissions.
	 */
	 public class UserPermissionsWrapper {
	 	@AuraEnabled public Boolean canEditAlert {get; set;}

	 	UserPermissionsWrapper(Boolean canEditAlert) {
	 		this.canEditAlert = canEditAlert;
	 	}
	 }
}