/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for SNR_Selector class
History:        mbrimus 2020-01-29 - Created.
*************************************************************************************************/
@IsTest
private class SNR_SelectorTest {
    static List<String> WORKORDERRECORDTYPE = new List<String>{
            'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'
    };

    @IsTest
    static void getServiceCategories_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getServiceCategories();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceCategories_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 1;

        createTestData();

        Test.startTest();
        result = selector.getServiceCategories();
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceTypesNoServiceRigProgram_noParams() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getServiceTypesNoServiceRigProgram(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceTypesNoServiceRigProgram_withParamsAndData_isCommunityUser() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 1;

        createTestData();
        List<HOG_Service_Category__c> cat = [SELECT Id FROM HOG_Service_Category__c LIMIT 1];
        System.assertEquals(1, cat.size());

        Test.startTest();
        result = selector.getServiceTypesNoServiceRigProgram(cat.get(0).Id, true);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNotificationTypeById_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<HOG_Notification_Type__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getNotificationTypeById(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNotificationTypeById_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<HOG_Notification_Type__c> result;
        Integer expectedCount = 1;
        createTestData();
        List<HOG_Notification_Type__c> notification = [SELECT Id FROM HOG_Notification_Type__c LIMIT 1];
        System.assertEquals(1, notification.size());

        Test.startTest();
        result = selector.getNotificationTypeById(notification.get(0).Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceActivityNames_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getServiceActivityNames(null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceActivityNames_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 1;

        createTestData();
        List<HOG_Service_Category__c> serviceCategory = [SELECT Id FROM HOG_Service_Category__c LIMIT 1];
        List<HOG_Notification_Type__c> notificationType = [SELECT Id FROM HOG_Notification_Type__c LIMIT 1];
        System.assertEquals(1, serviceCategory.size());
        System.assertEquals(1, notificationType.size());

        Test.startTest();
        result = selector.getServiceActivityNames(serviceCategory.get(0).Id, notificationType.get(0).Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServicePriorities_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<HOG_Notification_Type_Priority__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getServicePriorities(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServicePriorities_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<HOG_Notification_Type_Priority__c> result;
        Integer expectedCount = 1;

        createTestData();
        List<HOG_Notification_Type__c> notificationType = [SELECT Id FROM HOG_Notification_Type__c LIMIT 1];
        System.assertEquals(1, notificationType.size());

        Test.startTest();
        result = selector.getServicePriorities(notificationType.get(0).Id);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceRequired_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getServiceRequired(null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceRequired_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 1;
        createTestData();
        List<HOG_Service_Category__c> serviceCategory = [SELECT Id FROM HOG_Service_Category__c LIMIT 1];
        List<HOG_Notification_Type__c> notificationType = [SELECT Id FROM HOG_Notification_Type__c LIMIT 1];
        List<HOG_User_Status__c> activity = [SELECT Id FROM HOG_User_Status__c LIMIT 1];
        System.assertEquals(1, serviceCategory.size());
        System.assertEquals(1, notificationType.size());
        System.assertEquals(1, activity.size());

        Test.startTest();
        result = selector.getServiceRequired(
                serviceCategory.get(0).Id,
                notificationType.get(0).Id,
                activity.get(0).Id,
                false
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneServiceRequired_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<HOG_Service_Required__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneServiceRequired(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneServiceRequired_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<HOG_Service_Required__c> result;
        Integer expectedCount = 1;

        createTestData();
        List<HOG_Service_Required__c> serviceRequired = [SELECT Id FROM HOG_Service_Required__c LIMIT 1];
        System.assertEquals(1, serviceRequired.size());

        Test.startTest();
        result = selector.getOneServiceRequired(
                serviceRequired.get(0).Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceSpecifics_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getServiceSpecifics(null, null, null, null, null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getServiceSpecifics_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<AggregateResult> result;
        Integer expectedCount = 2;
        createTestData();

        List<HOG_Service_Category__c> serviceCategory = [SELECT Id FROM HOG_Service_Category__c LIMIT 1];
        List<HOG_Notification_Type__c> notificationType = [SELECT Id FROM HOG_Notification_Type__c LIMIT 1];
        List<HOG_User_Status__c> activity = [SELECT Id FROM HOG_User_Status__c LIMIT 1];
        List<HOG_Service_Required__c> serviceRequired = [SELECT Id FROM HOG_Service_Required__c LIMIT 1];

        System.assertEquals(1, serviceCategory.size());
        System.assertEquals(1, notificationType.size());
        System.assertEquals(1, activity.size());
        System.assertEquals(1, serviceRequired.size());

        Test.startTest();
        result = selector.getServiceSpecifics(
                serviceCategory.get(0).Id,
                notificationType.get(0).Id,
                activity.get(0).Id,
                serviceRequired.get(0).Id,
                false
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneFacility_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<Facility__c> result;
        Integer expectedCount = 1;

        Facility__c facility = FacilityTestData.createFacility('Test Facility', null, null);
        facility.Functional_Location__c = 'H1-01-004-10006';
        facility.Functional_Location_Category__c = 3;
        insert facility;

        Test.startTest();
        result = selector.getOneFacility(
                facility.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneFacility_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<Facility__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneFacility(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneWellLocation_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<Location__c> result;
        Integer expectedCount = 1;

        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);

        Test.startTest();
        result = selector.getOneWellLocation(
                wellLocation.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneWellLocation_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<Location__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneWellLocation(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneEquipment_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<Equipment__c> result;
        Integer expectedCount = 1;

        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);

        Equipment__c equipment = new Equipment__c
                (
                        Name = 'Equipment Test',
                        Location__c = wellLocation.Id,
                        Catalogue_Code__c = 'CATCODE'
                );
        insert equipment;

        Test.startTest();
        result = selector.getOneEquipment(
                equipment.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneEquipment_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<Equipment__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneEquipment(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneWellEvent_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<Well_Event__c> result;
        Integer expectedCount = 1;

        Id recordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'].Id;
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);
        Well_Event__c wellEvent = HOG_TestDataFactory.createWellEvent(wellLocation.Id, 'H1-01-004-10007-100001', null, true);

        Test.startTest();
        result = selector.getOneWellEvent(
                wellEvent.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneWellEvent_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<Well_Event__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneWellEvent(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneSystem_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<System__c> result;
        Integer expectedCount = 1;

        System__c systemRecord = HOG_TestDataFactory.createSystem(true);

        Test.startTest();
        result = selector.getOneSystem(
                systemRecord.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneSystem_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<System__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneSystem(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneSubSystem_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<Sub_System__c> result;
        Integer expectedCount = 1;

        Sub_System__c subSystem = HOG_TestDataFactory.createSubSystem(true);

        Test.startTest();
        result = selector.getOneSubSystem(
                subSystem.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneSubSystem_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<Sub_System__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneSubSystem(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneYard_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<Yard__c> result;
        Integer expectedCount = 1;

        Yard__c yard = HOG_TestDataFactory.createYard(true);

        Test.startTest();
        result = selector.getOneYard(
                yard.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getOneYard_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<Yard__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getOneYard(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getFEL_withData() {
        SNR_Selector selector = new SNR_Selector();
        List<Functional_Equipment_Level__c> result;
        Integer expectedCount = 1;

        Functional_Equipment_Level__c fel = HOG_TestDataFactory.createFunctionalEquipmentLevel(true);

        Test.startTest();
        result = selector.getFEL(
                fel.Id
        );
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getFEL_withoutData() {
        SNR_Selector selector = new SNR_Selector();
        List<Functional_Equipment_Level__c> result;
        Integer expectedCount = 0;

        Test.startTest();
        result = selector.getFEL(null);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    private static void createTestData() {
        HOG_Service_Category__c serviceCategory =
                ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;

        HOG_Notification_Type__c notificationType =
                NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, false, false, true);
        insert notificationType;

        HOG_Service_Priority__c servicePriority =
                ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriority;

        HOG_Service_Required__c serviceRequired = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequired;

        HOG_User_Status__c userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;

        List<HOG_Notification_Type_Priority__c> notificationTypePriority = new List<HOG_Notification_Type_Priority__c>{
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, false),
                NotificationTypePriorityTestData.createNotificationTypePriority(notificationType.Id, servicePriority.Id, true)
        };
        insert notificationTypePriority;

        List<HOG_Service_Code_MAT__c> serviceCodeMAT = new List<HOG_Service_Code_MAT__c>{
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]),
                ServiceCodeMATTestData.createServiceCodeMAT('Test MAT2', 'TS2', WORKORDERRECORDTYPE[1])
        };
        insert serviceCodeMAT;

        List<HOG_Work_Order_Type__c> workOrderType = new List<HOG_Work_Order_Type__c>{
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[0].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true),
                WorkOrderTypeTestData.createWorkOrderType(notificationType.Id, serviceCodeMAT[1].Id, serviceRequired.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true)
        };
        insert workOrderType;
    }
}