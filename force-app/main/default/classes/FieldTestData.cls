/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the Field__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class FieldTestData
{
    public static Field__c createField(String name, Id operatingDistrictId, Id recordTypeId)
    {                
        Field__c results = new Field__c
            (           
                Name = name,
                Operating_District__c = operatingDistrictId,
                RecordTypeId = recordTypeId
            );            

        return results;
    }
}