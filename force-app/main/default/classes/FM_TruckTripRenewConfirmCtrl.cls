/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Extension for renewing and confirming Cancelled truck trips. It supports
                two modes. Renew nad Confirm. 
                For renew there is process which create clone from Truck Trip and inserts it into
                database and updates old truck trip status to uncancelled. Along with that 
                Load Request Status and CancelReasons are updated accordingly.
                For confirm there is similar process as for renew, with additional steps which are
                1. create stuck truck if necessary, 2. create load confirmation for new Truck Trip.
Test Class:     FM_TruckTripRenewConfirmCtrlTest
History:        jschn 06.15.2018 - Created.
                jschn 01.10.2020 - W-001627 - copied same validation for Load Confirmation as it is on Confirmation page
**************************************************************************************************/
public with sharing class FM_TruckTripRenewConfirmCtrl {

    public static final String CLASS_NAME = 'FM_TruckTripRenewConfirmCtrl';
    //jschn 01.10.2020 - W-001627
    public static final String INFO_MSG_DUPLICATE_STN = 'Scale Ticket Number is already used on Confirmation for Truck Trip(s) ';
    public static final String ERROR_MSG_DUPLICATE_TN = 'Ticket Number is already used on Confirmation for Truck Trip ';

    private static final String SUCCESS      = 'success';
    private static final String UNSUCCESSFUL = 'unsuccessful';

    //jschn 01.10.2020 - W-001627
    private String scaleTicketNumberPreviousAttempt;

    private String truckTripId;
    public FM_Truck_Trip__c            truckTrip      {get;set;}
    public FM_Truck_Trip__c            oldTruckTrip   {get;set;}
    private List<FM_Truck_Trip__History> truckTripHistory;

    public FM_Load_Confirmation__c  loadConfirmation    {get;set;}

    public Boolean                  isStuckTruck        {get;set;}
    public FM_Stuck_Truck__c        stuckTruck          {get;set;}

    @TestVisible private String                  oldUnit;
    private String                  oldUnitChangeReason;

    public Boolean isExternalDispatcher {
        get {
            if (isExternalDispatcher == null) {
                isExternalDispatcher = FM_LoadConfirmation_Utilities.userHasExtDispatcherPS();
            }
            return isExternalDispatcher;
        }
        private set;
    }

    public Boolean initError                {get;set;}
    public Boolean isTruckTripConfirmable   {get;set;}
    public Boolean isTruckTripRenewable     {get;set;}
    public Boolean isConfirmMode {
        get {
            return FM_TruckTrip_Utilities.UNCANCEL_MODE_CONFIRM.equals(selectedMode);
        }
        private set;
    }
    public Boolean isRenewMode   {
        get {
            return FM_TruckTrip_Utilities.UNCANCEL_MODE_RENEW.equals(selectedMode);
        }
        private set;
    }

    public String selectedMode {get;set;}
    public List<SelectOption> uncancelModeOptions {
        get {
            if (uncancelModeOptions == null) {
                uncancelModeOptions = FM_TruckTrip_Utilities.getUncancelModes(isTruckTripConfirmable, isTruckTripRenewable);
            }
            return uncancelModeOptions;
        }
        private set;
    }

    public String modeTitle {
        get{
            return FM_TruckTrip_Utilities.MODE_TITLE;
        }
        private set;
    }

    /**
     * Constructor which initialize basic variables, retrieve URL parameters.
     * Also it retriews original truck trip based on retrieved parameters, 
     * does some validation around statuses, loads history data, sets 
     * capabilities for trucktrips (turn on modes), sets initial mode based
     * on those capabilities and switch all variables to correspond with
     * chosen mode.
     * @param  stdController
     */
    @SuppressWarnings('ApexUnusedDeclaration')
    public FM_TruckTripRenewConfirmCtrl(ApexPages.StandardController stdController) {
        initVariables();
        getUrlParams();
        oldTruckTrip = getTruckTrip();
        if (isValidTruckTrip(oldTruckTrip)) {
            truckTripHistory = getHistoryDataForTruckTrip();
            setTruckTripCapabilities();
            setInitialMode();
            handleModeChange();
        }
    }

/****************
*INITIALIZATIONS*
****************/
    /**
     * Currently only variable which is initiated before data retrieve is initError
     */
    private void initVariables() {
        initError = false;
    }

    /**
     * Retrieves URL parameters into designated variables.
     * Currently only parameter is Id of trucktrip.
     */
    private void getUrlParams() {
        truckTripId = HOG_GeneralUtilities.getUrlParam(FM_TruckTrip_Utilities.PARAM_ID);
    }

    /**
     * Set's mode capabilities for Truck Trip based on historical statuses.
     */
    private void setTruckTripCapabilities() {
        isTruckTripConfirmable = FM_TruckTrip_Utilities.isTruckTripConfirmable(truckTripHistory, isExternalDispatcher);
        isTruckTripRenewable = FM_TruckTrip_Utilities.isTruckTripRenewable(truckTripHistory, isExternalDispatcher);
    }

    /**
     * Selects first mode which is available for Truck Trip.
     */
    private void setInitialMode() {
        System.debug(CLASS_NAME + '-> Initial mode selection.');
        System.debug(CLASS_NAME + '-> Truck Trip renewable: ' + isTruckTripRenewable);
        System.debug(CLASS_NAME + '-> Truck Trip confirmable: ' + isTruckTripConfirmable);
        if (isTruckTripRenewable) {
            selectedMode = FM_TruckTrip_Utilities.UNCANCEL_MODE_RENEW;
        } else if (isTruckTripConfirmable) {
            selectedMode = FM_TruckTrip_Utilities.UNCANCEL_MODE_CONFIRM;
        } else {
            selectedMode = FM_TruckTrip_Utilities.UNCANCEL_MODE_RENEW;
        }
        System.debug(CLASS_NAME + '-> Selected mode: ' + selectedMode);
    }

/********
*GETTERS*
********/
    /**
    * returns true if unit has been changed on Truck Trip.
    * Also false if confirmation or truck trip unpresent.
    */
    public Boolean getUnitChanged() {
        return loadConfirmation != null
            && loadConfirmation.Truck_Trip__r != null
            && String.isNotBlank(oldUnit)
            && loadConfirmation.Truck_Trip__r.Unit__c != oldUnit;
    }

/*
CUSTOM LABELS
 */
    /**
     * @return Label for displaying note for Stuck Truck Unit.
     */
    public String getStuckTruckUnitNoteText() {
        return FM_LoadConfirmation_Utilities.NOTE_STUCKTRUCK_UNIT_FROM_CONFIRMATION;
    }

    /**
     * @return Old Truck Trip label.
     */
    public String getOldTruckTripLabel() {
        return FM_TruckTrip_Utilities.LABEL_OLD_TRUCKTRIP;
    }

/**************
*DATA RETRIEVE*
**************/
    /**
     * @return Truck Trip record based on URL param.
     */
    private FM_Truck_Trip__c getTruckTrip() {
        return FM_TruckTrip_Utilities.getTruckTripById(truckTripId);
    } 

    /**
     * @return List of Truck Trip historical data.
     */
    private List<FM_Truck_Trip__History> getHistoryDataForTruckTrip() {
        return FM_TruckTrip_Utilities.getTruckTripHistoryData(truckTripId);
    }

    /**
    * Loads Load Confirmations based on selected Destination Facility for validation
    * of Scale Ticket Number (STN should be unique per Facility. Other facility can have
    * same STN).
    */
    private List<FM_Load_Confirmation__c> getLoadConfirmationOnFacilityWithScaleTicketNumber() {
        return FM_LoadConfirmation_Utilities.getLoadConfirmationOnFacilityWithScaleTicketNumber(
                                                                loadConfirmation.Destination_Facility__c,
                                                                loadConfirmation.Scale_Ticket_Number__c,
                                                                loadConfirmation.Id);
    }

/*************
*PAGE ACTIONS*
*************/
/*
Handlers
 */
    /**
     * Handles mode changes from page.
     */
    public void handleModeChange() {
        if (FM_TruckTrip_Utilities.UNCANCEL_MODE_CONFIRM.equals(selectedMode)) {
            handleConfirmModeChange();
        } else if (FM_TruckTrip_Utilities.UNCANCEL_MODE_RENEW.equals(selectedMode)) {
            handleRenewModeChange();
        }
    }

    /**
    * Handle action when unit change.
    * Reset Unit Change Reason when Unit changed to original value.
    */
    public void handleUnitChange() {
        if (!getUnitChanged()) {
            oldUnitChangeReason = loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c;
            loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c = null;
        } else if (String.isBlank(loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c)) {
            loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c = oldUnitChangeReason;
        }
        handleUnitChangeReason();
   }

    /**
    * Handle Change reason other. 
    * Reset Unit Change Other Reason if there is not Other reason selected.
    */
    public void handleUnitChangeReason() {
        if (String.isBlank(loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c)
                || loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c != FM_LoadConfirmation_Utilities.UNIT_CHANGE_REASON_OTHER) {
            loadConfirmation.Truck_Trip__r.Unit_Change_Other_Reason__c = '';
        }
    }

/*
Button actions
 */
    /**
     * @return Page Reference for detail page for original Truck Trip
     */
    public PageReference cancel() {
        if (oldTruckTrip != null) {
            return new ApexPages.StandardController(oldTruckTrip).view();
        }
        if (String.isNotBlank(truckTripId)) {
            return new ApexPages.StandardController(new FM_Truck_Trip__c(Id = truckTripId)).view();
        }
        return new PageReference('/');
    }

    /**
     * Tries to insert/update old Truck Trip, new Truck Trip, Load Request,
     * Load Confirmation and Stuck Truck.
     * If anything goes wrong, it falls to catch block which will handle exception
     * and rolls back database to previous state.
     * After successful dml operations it calls doSaveRedirect.
     * @return Page Reference according to settings
     */
    public PageReference confirm() {
        Savepoint sp = Database.setSavepoint();
        try{
            if (tryToSaveTruckTripsAndLoadRequest() == SUCCESS
                    && tryToSaveLoadConfirmationAndStuckTruck() == SUCCESS) {
                return doSaveRedirect();
            }
        } catch(Exception ex) {
            handleSaveException(ex, sp);
        }
        return null;
    }

    /**
     * Tries to insert/update old Truck Trip, new Truck Trip, Load Request.
     * If anything goes wrong, it falls to catch block which will handle exception
     * and rolls back database to previous state.
     * After successful dml operations it calls doSaveRedirect.
     * @return Page Reference according to settings
     */
    public PageReference renew() {
        Savepoint sp = Database.setSavepoint();
        try{
            if (tryToSaveTruckTripsAndLoadRequest() == SUCCESS) {
                return doSaveRedirect();
            }
        } catch(Exception ex) {
            handleSaveException(ex, sp);
        }
        return null;
    }

/************
*VALIDATIONS*
************/
/*
Initial Truck Trip validation
 */
    /**
     * Runs intial validation on truck trip.
     * If Truck Trip is not valid, error messages are presented and init error is set to true.
     * @param  truckTrip to validate.
     * @return           if not errors were found.
     */
    private Boolean isValidTruckTrip(FM_Truck_Trip__c truckTrip) {
        if(truckTrip != null) {
            if(!FM_TruckTrip_Utilities.STATUS_CANCELLED.equals(truckTrip.Truck_Trip_Status__c)) {
                HOG_GeneralUtilities.addPageError(FM_TruckTrip_Utilities.ERROR_WRONG_STATUS);
                initError = true;
            }
        } else {
            HOG_GeneralUtilities.addPageError(FM_TruckTrip_Utilities.ERROR_WRONG_ID_PARAM);
            initError = true;
        }
        return !initError;
    }

/*
Pre-save data validation
 */
    /**
     * @return if new truck trip is valid.
     */
    private Boolean isRenewDataValid() {
        return isNewTruckTripValid();
    }

    /**
     * Checks if all data for confirm action are valid.
     * Check is run on new Truck Trip, Load Confirmation and Stuck trip records.
     * @return if all data are entered and valid.
     */
    private Boolean isConfirmDataValid() {
        return isNewTruckTripValid()
            && isLoadConfirmationValid()
            && isStuckTruckValid();
    }

    /**
     * For now there is no validation needed for new Truck Trip.
     * We are changing only status and retrieving data from history.
     * Ability do edit anything from page is disabled.
     * Method is presented in here for future adjustments.
     * @return true
     */
    private Boolean isNewTruckTripValid() {
        //prepared if there needs to be some validation in future.
        return true;
    }

    /**
     * Validation for Load Confirmation data populated from page.
     * Runs validation on destination, ticket number, volume, unit change reason
     * and Scale Ticket Number.
     * @return true if all data is valid for inserting Load Confirmation.
     */
    private Boolean isLoadConfirmationValid() {
        return isValidDestination()
            && isValidTicketNumber()
            && isValidVolume()
            && isValidUnitChangeReason() 
            && isValidScaleTicket();
    }

/*
Stuck Truck Validations
 */
    /**
     * Validation for Stuck Truck data populated from page.
     * Runs validation only when there is stuck truck option turned on.
     * Validations are runned for Carrier, Stuck From, Stuck To and Towing Company.
     * @return true if all data is valid for inserting Stuck Truck.
     */
    private Boolean isStuckTruckValid() {
        return !isStuckTruck
            || (isStuckTruckCarrierValid()
                //&& isStuckTruckUnitValid() NOTE: validated in Truck Trip validations
                && isStuckFromValid()
                && isStuckToValid()
                && isTowingCompanyValid());
    }

    /**
     * Validate if Stuck Truck has Carrier assigned.
     */
    private Boolean isStuckTruckCarrierValid() {
        Boolean isValid = String.isNotBlank(stuckTruck.Carrier__c);
        if(!isValid) HOG_GeneralUtilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_STUCKTRUCK_MISSING_CARRIER); 
        return isValid;
    }

    /**
     * Validate if Stuck From field is populated.
     */
    private Boolean isStuckFromValid() {
        Boolean isValid = stuckTruck.Stuck_From__c != null
                        || isExternalDispatcher;
        if(!isValid) HOG_GeneralUtilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_STUCKTRUCK_MISSING_STUCK_FROM); 
        return isValid;
    }

    /**
     * Validate if Stuck To field is populated correctly.
     * this field is required only for internal dispatchers.
     * and also condition that Stuck To is bigger then Stuck From si valid only for internal dispatchers.
     */
    private Boolean isStuckToValid() {
        Boolean isValid = (stuckTruck.Stuck_To__c != null
                            && stuckTruck.Stuck_To__c > stuckTruck.Stuck_From__c)
                        || isExternalDispatcher;
        if(!isValid) HOG_GeneralUtilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_STUCKTRUCK_WRONG_STUCK_TO); 
        return isValid;
    }

    /**
     * Validate if Towing Company is assigned.
     * Validatoin is disabled for External Dispatchers.
     */
    private Boolean isTowingCompanyValid() {
        Boolean isValid = String.isNotBlank(stuckTruck.Towing_Company_Lookup__c) 
                        || isExternalDispatcher;
        if(!isValid) HOG_GeneralUtilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_STUCKTRUCK_MISSING_TOWING_COMPANY); 
        return isValid;
    }

/*
Load Confirmation validation
 */
    //jschn 01.10.2020 - W-001627 Updated
    /**
     * Validate if Ticket Number is populated.
     */
    private Boolean isValidTicketNumber() {
        List<FM_Load_Confirmation__c> confirmationsTNDuplicate = getLoadConfirmationWithTicketNumber();
        Boolean valid = (confirmationsTNDuplicate == null || confirmationsTNDuplicate.size() == 0);
        if (!valid) {
            FM_LoadConfirmation_Utilities.addPageError(ERROR_MSG_DUPLICATE_TN
                    + confirmationsTNDuplicate.get(0).Truck_Trip__r.Name);
        }
        return valid;
    }

    //jschn 01.10.2020 - W-001627 Added
    /**
    * Loads Load Confirmations based on selected Carrier for validation
    * of Ticket Number (TN should be unique per Carrier. Other Carrier can have
    * same TN).
    */
    private List<FM_Load_Confirmation__c> getLoadConfirmationWithTicketNumber() {
        return FM_LoadConfirmation_Utilities.getLoadConfirmationWithTicketNumber(
                loadConfirmation.Truck_Trip__r.Carrier__c,
                loadConfirmation.Ticket_Number__c,
                loadConfirmation.Id);
    }

    /**
     * Validate if Volume is populated.
     */
    private Boolean isValidVolume() {
        Boolean isValid = loadConfirmation.Volume__c != null;
        if(!isValid) HOG_GeneralUtilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_MISSING_VOLUME);
        return isValid;
    }

    /**
    * Validate if only one and only one Destination is filled up.
    * If not, throw error.
    */
    private Boolean isValidDestination() {
        Boolean valid = (loadConfirmation != null 
                            && String.isBlank(loadConfirmation.Destination_Facility__c) 
                        && String.isNotBlank(loadConfirmation.Destination_Location__c))
                    || (loadConfirmation != null 
                            && String.isNotBlank(loadConfirmation.Destination_Facility__c) 
                        && String.isBlank(loadConfirmation.Destination_Location__c));
        if (!valid) {
            FM_LoadConfirmation_Utilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_DESTINATION_MISSING);
        }
        return valid;
    }

    /**
    * Return true if unit # was changed and Unit Change Reason was populated
    * or unit # wasn't changed and Unit Change Reason was not populate (is blank)
    */
    private Boolean isValidUnitChangeReason() {
        if (getUnitChanged()) {
            return hasUnitChangeReason()
                    && (isOtherUnitChangeReason()
                    ? hasOtherUnitChangeReason()
                    : true);
        } else {
            return hasNoUnitChangeReason();
        }
    }

    /**
    * Return true if Unit Change reason is not blank. Otherwise return false and rise error.
    */
    private Boolean hasUnitChangeReason() {
        Boolean valid = String.isNotBlank(loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c);
        if (!valid) FM_LoadConfirmation_Utilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_UNIT_CHANGE_REASON_MISSING);
        return valid;
    }

    /**
    * Return true if Unit Change reason is blank. Otherwise return false and rise error.
    */
    private Boolean hasNoUnitChangeReason() {
        Boolean valid = String.isBlank(loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c)
                    || loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c == oldUnitChangeReason;
        if (!valid) FM_LoadConfirmation_Utilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_UNIT_CHANGE_REASON_POPULATED);
        return valid;
    }

    /**
    * Return true if Unit Change Reason Other is not blank. Otherwise return false and rise error.
    */
    private Boolean hasOtherUnitChangeReason() {
        Boolean valid = String.isNotBlank(loadConfirmation.Truck_Trip__r.Unit_Change_Other_Reason__c);
        if (!valid) FM_LoadConfirmation_Utilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_UNIT_CHANGE_COMMENT_MISSING);
        return valid;
    }

    /**
    * Checks if there is 'Other' unit change reason.
    */
    private Boolean isOtherUnitChangeReason() {
        return loadConfirmation != null
            && loadConfirmation.Truck_Trip__r != null
            && loadConfirmation.Truck_Trip__r.Unit_Change_Reason__c == FM_LoadConfirmation_Utilities.UNIT_CHANGE_REASON_OTHER;
    }

    /**
    * Custom validation on Scale Ticket Number.
    * If no STN entered - return true(valid)
    * If Destination Location entered - run validation on it.
    * If Destination Facility entered - run validation on it.
    * Otherwise return false(invalid) - no scenario where it can go to this point
    * as either Destination location or facility is required.
    */
    private Boolean isValidScaleTicket() {
        if (String.isBlank(loadConfirmation.Scale_Ticket_Number__c))     return true;
        if (String.isNotBlank(loadConfirmation.Destination_Location__c)) return isValidScaleTicketOnLocation();
        if (String.isNotBlank(loadConfirmation.Destination_Facility__c)
                && String.isNotBlank(loadConfirmation.Scale_Ticket_Number__c)) {
            return isValidScaleTicketOnFacility();
        }
        return false;
    }

    /**
    * As location doesn't have scales(there for no STN is entered or there shouldn't be validation
    * if it is unique) it will always return true. (Method is present in here just to leave here
    * note about this and for future if Husky decide that they want validation on location too).
    */
    private Boolean isValidScaleTicketOnLocation() {
        //Location doesn't have Scales (so no scale ticket number is expected) NOTE: add loc != null STN is not blank...
        //not sure if there is possibility to have location with scales.
        //They told me that it shouldn't be possible but with Fluid there is everything fluid and even this
        //doesn't need to be true in some special cases.
        return true;
    }

    //jschn 01.10.2020 - W-001627 Updated
    /**
    * Loops thru Facility confirmations list comparing entered STN with previous STNs for selected Destionation
    * facility. If there found equal STN there is an error msg logged in and validation is returned as false(invalid).
    * otherwise returned true(valid)
    */
    private Boolean isValidScaleTicketOnFacility() {
        List<FM_Load_Confirmation__c> confirmationsSTNDuplicate = getLoadConfirmationOnFacilityWithScaleTicketNumber();
        Boolean valid = (confirmationsSTNDuplicate == null || confirmationsSTNDuplicate.size() == 0);
        if (!valid) {
            String truckTripNames = extractNames(confirmationsSTNDuplicate);
            ApexPages.addMessage(
                    new ApexPages.Message(
                            ApexPages.Severity.INFO,
                            INFO_MSG_DUPLICATE_STN + truckTripNames
                    )
            );
        }

        if (isSecondAttempt()) return true;

        return valid;
    }

    //jschn 01.10.2020 - W-001627 Added
    /**
     * Extracts Truck trip names for Warning/Error msg based on Confirmations
     *
     * @param confirmations
     *
     * @return String
     */
    private String extractNames(List<FM_Load_Confirmation__c> confirmations) {
        String names = '';
        for (FM_Load_Confirmation__c conf : confirmations) {
            if (String.isNotBlank(names)) names += ', ';
            names += conf.Truck_Trip__r.Name;
        }
        return names;
    }

    //jschn 01.10.2020 - W-001627 Added
    /**
     * Checks if save attempt is made after Scale Ticket Number Duplicate INFO message was made.
     * Also it checks if scaleTicketNumber is different from previous attempt (need to rerun validation if changed)
     * After that it checks if any of message contains Duplicate STN message.
     *
     * @return
     */
    private Boolean isSecondAttempt() {
        if (!ApexPages.hasMessages(ApexPages.Severity.INFO)) return false;
        if (String.isBlank(scaleTicketNumberPreviousAttempt)
                || scaleTicketNumberPreviousAttempt != loadConfirmation.Scale_Ticket_Number__c) {
            scaleTicketNumberPreviousAttempt = loadConfirmation.Scale_Ticket_Number__c;
            return false;
        }

        Boolean secondAttempt = false;
        for (ApexPages.Message message : ApexPages.getMessages()) {
            System.debug(message.getDetail());
            System.debug(message.getDetail().contains(INFO_MSG_DUPLICATE_STN));
            if (message.getDetail().contains(INFO_MSG_DUPLICATE_STN)) {
                secondAttempt = true;
                break;
            }
        }
        return secondAttempt;
    }

    /************
    *DML ACTIONS*
    ************/
    /**
     * Inserts new Truck Trip. 
     */
    private void insertNewTruckTrip() {
        insert truckTrip;
    }

    /**
     * Update Old truck trip status and reference to new Truck trip.
     */
    private void updateOldTruckTrip() {
        oldTruckTrip.Truck_Trip_Status__c = FM_TruckTrip_Utilities.STATUS_UNCANCELLED;
        oldTruckTrip.Uncancelled_Truck_Trip__c = truckTrip.Id;
        update oldTruckTrip;
    }

    /**
     * Update Load Request.
     */
    private void updateLoadRequest() {
        update truckTrip.Load_Request__r;
    }

    /**
     * Assign new Truck Trip into Load Confirmation and insert LC.
     */
    private void insertLoadConfirmationWithNewTruckTrip() {
        loadConfirmation.Truck_Trip__c = truckTrip.Id;
        insert loadConfirmation;
    }

    /**
     * Insert Stuck Truck on new Truck Trip.
     * 1. Prefill all required data which are not populated from page
     * 2. If user is external dispatcher - Update some Stuck truck data for External Stuck Truck Record Type
     * 3. If user is internal dispatcher - Do initial insert without some data populated (bcs of validation rules)
     *                                   - update Stuck Truck inserted before with all data removed for initial insert.
     */
    private void insertStuckTruckOnNewTruckTrip() {
        if(isStuckTruck) {
            prefillStuckTruckData();
            if(isExternalDispatcher) {
                updateStuckTruckValuesAsExternalDispatcher();
                insert stuckTruck;
            } else {
                stuckTruck.Id = doInitialInsertForStuckTruck();
                update stuckTruck;
            } 
        }
    }

/*************
*SAVE HELPERS*
*************/
    /**
     * Check if data are valid for Renew action and if Confirm Mode 
     * check also if data are valid for Confirm action.
     * Insert new Truck Trip
     * Update old Truck Trip
     * Reset Cancel reasons on new Truck Trip and Load Request 
     * Update new Truck Trip and Load Request (avoiding Strict 
     * validation rules which I don't want to mess with, bcs it 
     * could lead to data integrity failure)
     * @return Status (not-)Success
     */
    private String tryToSaveTruckTripsAndLoadRequest() {
        if(isRenewDataValid()
        && (isRenewMode
            || isConfirmDataValid())) {
            System.debug(CLASS_NAME + '-> Load Request before renew: ' + truckTrip.Load_Request__r);
            System.debug(CLASS_NAME + '-> New Truck Trip before renew: ' + truckTrip);
            resetCancelReasonsOnTruckTrip();
            System.debug(CLASS_NAME + '-> Load Request during renew: ' + truckTrip.Load_Request__r);
            System.debug(CLASS_NAME + '-> New Truck Trip during renew: ' + truckTrip);
            updateLoadRequest();
            System.debug(CLASS_NAME + '-> Load Request after update: ' + truckTrip.Load_Request__r);
            insertNewTruckTrip();
            System.debug(CLASS_NAME + '-> New Truck trip after insert: ' + truckTrip);
            update truckTrip;
            System.debug(CLASS_NAME + '-> New Truck trip after update: ' + truckTrip);
            updateOldTruckTrip();
            System.debug(CLASS_NAME + '-> Old Truck trip after update: ' + oldTruckTrip);
            return SUCCESS;
        }
        return UNSUCCESSFUL;
    }

    /**
     * Check if data are valid for Confirm action
     * Insert Stuck Truck and Load Confirmation.
     * @return Status (not-)Success
     */
    private String tryToSaveLoadConfirmationAndStuckTruck() {
        if(isConfirmDataValid()) {
            insertStuckTruckOnNewTruckTrip();
            insertLoadConfirmationWithNewTruckTrip();
            return SUCCESS;
        }
        return UNSUCCESSFUL;
    }

    /**
     * Handle save exception.
     * Do database rollback, reset record Ids and log message.
     * @param ex exception to handle
     * @param sp Savepoint for database rollback
     */
    private void handleSaveException(Exception ex, Savepoint sp) {
        Database.rollback(sp);
        resetIds();
        logMessage(ex);
    }

    /**
     * Reset Ids for all records which are inserted to prevent error when reInsert action is runned.
     */
    private void resetIds() {
        truckTrip.Id = null;
        oldTruckTrip.Uncancelled_Truck_Trip__c = null;
        if(loadConfirmation != null) loadConfirmation.Id = null;
        if(stuckTruck != null) stuckTruck.Id = null;
    }

    /**
     * Log exception message into Page and Console Log.
     */
    private void logMessage(Exception ex) {
        HOG_GeneralUtilities.addPageError(ex.getMessage());
        System.debug(CLASS_NAME + '-> Confirm Error: ' + ex);
    }

    /**
     * Prefill some basic data for stuck truck which are required for Stuck Truck insert and are not
     * populated from Client's side.
     */
    private void prefillStuckTruckData() {
        stuckTruck.Carrier_Unit__c = truckTrip.Unit__c;
        stuckTruck.Status__c = FM_LoadConfirmation_Utilities.STUCK_TRUCK_STATUS_UNSTUCK;
        stuckTruck.Truck_Trip__c = truckTrip.Id;
        stuckTruck.Location__c = truckTrip.Load_Request__r.Source_Location__c;
        stuckTruck.Facility__c = truckTrip.Load_Request__r.Source_Facility__c;
    }

    /**
     * Update Stuck Truck record for External Dispatcher.
     * As they are not inserting Stuck From, Stuck To and Status of stuck truck
     * is directly set to Unstuck.
     */
    private void updateStuckTruckValuesAsExternalDispatcher() {
        Datetime now = Datetime.now();
        stuckTruck.Stuck_From__c = now;
        stuckTruck.Stuck_To__c = now;
        stuckTruck.Status__c = FM_LoadConfirmation_Utilities.STUCK_TRUCK_STATUS_EXTERNAL_DEFAULT;
    }

    /**
     * Do initial Insert for Stuck Truck while removing some of the fields which are restricted 
     * with Validation rules for insert action. 
     * @return Id of Stuck Truck.
     */
    private Id doInitialInsertForStuckTruck() {
        FM_Stuck_Truck__c initialStuckTruck = stuckTruck.clone(false);
        initialStuckTruck.Stuck_To__c = null;
        initialStuckTruck.Towing_Company_Lookup__c = null;
        initialStuckTruck.Status__c = FM_LoadConfirmation_Utilities.STUCK_TRUCK_STATUS_STUCK;
        insert initialStuckTruck;
        return initialStuckTruck.Id;
    }

    /**
     * Do save redirect, redirect page to new Truck Trip detail page.
     * Currently it doesn't contains any saveUrl parameter possibility.
     * @return Page Reference to new Truck Trip detail page.
     */
    private PageReference doSaveRedirect() {
        return new ApexPages.StandardController(truckTrip).view();
    }

/********
*HELPERS*
********/
/*
MODE CHANGE HELPERS
 */
    /**
     * Handle change mode to Confirm.
     * clean variables, checks if Truck Trip is Confirmable
     * if so reinitiate new Truck Trip, init Load Confirmation,
     * save oldUnit for Unit swaping and initialize Stuck Truck rec.
     * if not throw an error into page.
     */
    private void handleConfirmModeChange() {
        cleanVariables();
        if(isTruckTripConfirmable) {
            prepareNewTruckTrip();
            truckTrip.Truck_Trip_Status__c = FM_TruckTrip_Utilities.STATUS_DISPATCHED;
            initLoadConfirmation();
            oldUnit = loadConfirmation.Truck_Trip__r.Unit__c;
            initStuckTruck();
        } else {
            HOG_GeneralUtilities.addPageError(FM_LoadConfirmation_Utilities.ERROR_CONFIRMATION_NOTSUPPORTED);
        }
        
    }

    /**
     * Handle change mode to Renew.
     * Clean variales, checks if Truck Trip is Renewable
     * if so reinitiates new Truck Trip.
     * if not throw an error into page.
     */
    private void handleRenewModeChange() {
        cleanVariables();
        if(isTruckTripRenewable) {
            prepareNewTruckTrip();
        } else {
            HOG_GeneralUtilities.addPageError(FM_TruckTrip_Utilities.ERROR_RENEW_NOTSUPPORTED);
        }
    }

    /**
     * Clean all variables which could be affected by users actions for mode switching.
     */
    private void cleanVariables() {
        truckTrip = null;
        loadConfirmation = null;
        oldUnit = null;
        oldUnitChangeReason = null;
        stuckTruck = null;
        isStuckTruck = false;
    }

    /**
     * Clone old Truck Trip. Reset data from History,
     * reset Truck Trip status, reset Load Request status.
     * reset Run Sheet Date for new TT when in renew mode(to be visible for
     * dispatchers)
     */
    private void prepareNewTruckTrip() {
        truckTrip = oldTruckTrip.clone(false, true);
        resetTruckTripDataFromHistory();
        resetLoadRequestStatus();
        if(isRenewMode) truckTrip.Run_Sheet_Date__c = Date.today();
    }

    /**
     * Initialize Load Confirmation and prefill data from Truck Trip.
     */
    private void initLoadConfirmation() {
        loadConfirmation = new FM_Load_Confirmation__c();
        loadConfirmation.Truck_Trip__r = truckTrip;
        prefillData();
    }

    /**
     * Initialize Stuck Truck based on User (External, Internal dispatcher).
     */
    private void initStuckTruck() {
        if (isExternalDispatcher) {
            stuckTruck = FM_LoadConfirmation_Utilities.getExternalStuckTruck();
        } else {
            stuckTruck = FM_LoadConfirmation_Utilities.getInternalStuckTruck();
        }
        stuckTruck.Carrier__c = truckTrip.Carrier__c;
    }

    /**
     * Reset data for new Truck Trip from History Item.
     */
    private void resetTruckTripDataFromHistory() {
        if (truckTripHistory != null && truckTripHistory.size() > 0) {
            for (FM_Truck_Trip__History historyItem : truckTripHistory) {
                truckTrip = FM_TruckTrip_Utilities.setValueFromHistoryItem(truckTrip,
                        historyItem.Field,
                        String.valueOf(historyItem.OldValue),
                        selectedMode);
            }
        }
    }

/*
Status reset
 */
    /**
     * Reset Load Request Status to most basic status allowed for selected mode respectively.
     */
    private void resetLoadRequestStatus() {
        if (isConfirmMode) {
            truckTrip.Load_Request__r.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED;
        } else if(isRenewMode) {
            if(FM_TruckTrip_Utilities.STATUS_BOOKED.equals(truckTrip.Truck_Trip_Status__c)) {
                truckTrip.Load_Request__r.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_BOOKED;
            } else {
                truckTrip.Load_Request__r.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
            }
        }
    }

    /**
     * Reset Cancel reason for Load Request and Truck Trip.
     */
    private void resetCancelReasonsOnTruckTrip() {
        truckTrip.Load_Request_Cancel_Reason__c = null;
        truckTrip.Load_Request_Cancel_Comments__c = null;
        truckTrip.Load_Request__r.Cancel_Comments__c = null;
        truckTrip.Load_Request__r.Cancel_Reason__c = null;
    }

/*
Load Confirmation data prefill
 */
    /**
    * Resets Load confirmation data (product and both destinations) and if there is Truck Trip lookup entered,
    * it will lad Destination and product from it. If there is destination facility loaded it will also reload,
    * Facility Confirmations list for Scale Ticket Number validation.
    */
    private void prefillData() {
        if (loadConfirmation != null && loadConfirmation.Truck_Trip__r != null) {
            setDestination();
            setProduct();
        }
    }

    /**
    * It will prefill Destination Facility or Destination Location based on Truck Trip Destination Id
    * and facility Type field.
    * If Facility type is Facility, it will load Destination Id to Destination Facility,
    * if Facility type is Location, it will load Destination Id to Destination Location,
    * If Facility type is blank, it will load Destination Id to Destination Location.
    * If Destination Id is blank, it will load Location Lookup value (if not blank) to Destination Location.
    */
    private void setDestination() {
        Map<String,Id> destinationIdMap = FM_LoadConfirmation_Utilities.getDestinationIdMap(loadConfirmation.Truck_Trip__r);
        if (destinationIdMap.containsKey(FM_TruckTrip_Utilities.FACILITY_TYPE_LOCATION)) {
            loadConfirmation.Destination_Location__c = destinationIdMap.get(FM_TruckTrip_Utilities.FACILITY_TYPE_LOCATION);
        }
        if (destinationIdMap.containsKey(FM_TruckTrip_Utilities.FACILITY_TYPE_FACILITY)) {
            loadConfirmation.Destination_Facility__c = destinationIdMap.get(FM_TruckTrip_Utilities.FACILITY_TYPE_FACILITY);
        }
    }

    /**
    * It will prefill Product for Load Confirmation from Truck Trip Product field if present.
    */
    private void setProduct() {
        if (String.isNotBlank(loadConfirmation.Truck_Trip__r.Product__c)) {
            loadConfirmation.Product__c = loadConfirmation.Truck_Trip__r.Product__c;
        }
    }

}