/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_CalendarService class
History:        mbrimus 18/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_CalendarServiceTestNP {

    @IsTest
    static void test_setFlocTypeAndFlocName() {
        Id recordTypeId = [
                SELECT Id
                FROM RecordType
                WHERE SobjectType = 'Field__c' AND Name = 'HOG - Control Room Centre Record'
        ].Id;
        // FLOC Hierarchy
        Business_Unit__c unit = HOG_TestDataFactory.createBusinessUnit('TestBU', 'H1', true);
        Operating_District__c district = HOG_TestDataFactory.createOperatingDistrict('TestBD', unit.Id, 'H1-01', true);
        Field__c field = HOG_TestDataFactory.createAMU('500', district.Id, recordTypeId, 'H1-01-004', true);
        Location__c wellLocation = HOG_TestDataFactory.createLocation('TestLocation', field.Id, 'H1-01-004-10007', null, true);
        HOG_TestDataFactory.createWellEvent(wellLocation.Id, 'H1-01-004-10007-100001', null, true);
        Work_Order_Activity__c woa = VTT_TestDataFactory.createWorkOrderActivity('H1-01-004-10007-100001', null, true);

        Test.startTest();
        new VTT_LTNG_CalendarService().setFlocTypeAndFlocName(woa, new VTT_CalendarEventModel());
        Test.stopTest();
    }

    @IsTest
    static void test_Constants() {
        Test.startTest();
        String DTFORMAT = VTT_LTNG_CalendarConstants.DTFORMAT;
        Test.stopTest();
    }
}