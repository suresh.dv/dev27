/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class that works as endpoint for VG_CreateExemptionRedirectQA Lightning component
Test Class:     HOG_VG_AlertExemptionRedirectLtngTest
History:        jschn 2019-02-01 - Created.
                jschn 2020-01-23 - US 001742 - added check for Type (Engineering Notification)
*************************************************************************************************/
public with sharing class HOG_VG_AlertExemptionRedirectLtng {

    private static final String CLASS_NAME = String.valueOf(HOG_VG_AlertExemptionRedirectLtng.class);

    public static final String PLACEHOLDER = '###';
    public static final String ERROR_MSG_WRONG_USER = 'Only ' + PLACEHOLDER + ' is allowed to create exemption request.';
    public static final String ERROR_MSG_WRONG_STATUS = 'The Exemption can only be created on Not Started status.';
    public static final String ERROR_MSG_WRONG_TYPE = 'The Exemption can not be created on Alert with type of Engineering Notification.';
    public static final String ERROR_MSG_QUERY_ERROR = 'Query malfunctioned. Please contact administrator.';

    @TestVisible
    private static final String ALLOWED_CUSTOM_PERMISSION_NAME = 'HOG_Vent_Gas_create_Subtask';

    @AuraEnabled
    public static HOG_CustomResponse checkExemptionRedirect(String recordId) {
        HOG_Vent_Gas_Alert__c alert;
        HOG_CustomResponse response;
        Id userId = UserInfo.getUserId();

        try {
            alert = HOG_VentGas_Utilities.retrieveAlert(recordId).get(0);
            //jschn 2020-01-23 - US 001742
            if(hasCorrectStatus(alert)
                    && isCorrectUser(alert, userId)
                    && hasCorrectType(alert)) {
                response = new HOG_CustomResponseImpl(alert);
            } else {
                response = new HOG_CustomResponseImpl(getErrorMsg(alert));
            }
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' -> checkExemptionRedirect - ERROR: ' + ex.getMessage());
            System.debug(CLASS_NAME + ' -> checkExemptionRedirect - ERROR: ' + ERROR_MSG_QUERY_ERROR);
            response = new HOG_CustomResponseImpl(ERROR_MSG_QUERY_ERROR);
        }

        System.debug(CLASS_NAME + ' -> checkExemptionRedirect - Id param: '    + recordId);
        System.debug(CLASS_NAME + ' -> checkExemptionRedirect - response: '    + JSON.serialize(response));
        return response;
    }

    private static Boolean hasCorrectStatus(HOG_Vent_Gas_Alert__c alert) {
        return alert != null
                && HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED.equals(alert.Status__c);
    }

    private static Boolean isCorrectUser(HOG_Vent_Gas_Alert__c alert, Id userId) {
        return alert != null
                && (alert.Production_Engineer__c == userId
                        || HOG_CustomPermissionService.isUserInCustomPermission(new Set<String>{ALLOWED_CUSTOM_PERMISSION_NAME}));
    }

    private static Boolean hasCorrectType(HOG_Vent_Gas_Alert__c alert) {
        return alert != null
                && alert.Type__c != HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF;
    }

    private static String getErrorMsg(HOG_Vent_Gas_Alert__c alert) {
        String errorMsg = '';
        if(!hasCorrectStatus(alert)) {
            errorMsg = ERROR_MSG_WRONG_STATUS;
        } else if(!hasCorrectType(alert)) {
            //jschn 2020-01-23 - US 001742
            errorMsg = ERROR_MSG_WRONG_TYPE;
        } else {
            errorMsg = ERROR_MSG_WRONG_USER.replace(PLACEHOLDER, alert.Production_Engineer__r.Name);
        }
        return errorMsg;
    }

}