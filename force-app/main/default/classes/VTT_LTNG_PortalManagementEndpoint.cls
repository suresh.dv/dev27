/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_LTNG_PortalManagementEndpoint for Lightning Vendor Community
Test Class:     VTT_LTNG_PortalManagementEndpointTest
History:        mbrimus 28/11/2019. - Created.
*************************************************************************************************/
public with sharing class VTT_LTNG_PortalManagementEndpoint {
    private static final String CLASS_NAME = String.valueOf(VTT_LTNG_PortalManagementEndpoint.class);

    @AuraEnabled
    public static HOG_CustomResponseImpl loadUserData() {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        response = new VTT_LTNG_PortalManagementService().loadUserData();

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl loadAccountContacts(String accountId) {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        response = new VTT_LTNG_PortalManagementService().getContacts(accountId);

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl migrateToCommunity(Contact contact) {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_PortalManagementService().migrate(contact);
            System.debug(CLASS_NAME + ' migrateToLightning DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' migrateToLightning exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl enableLightningUser(Contact contact, String userType) {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        System.debug('enableLightningUser Contact ' + contact);
        System.debug('enableLightningUser userType ' + userType);

        try {
            response = new VTT_LTNG_PortalManagementService().enabledForLightningCommunity(contact, userType);
            System.debug(CLASS_NAME + ' migrateToLightning DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' migrateToLightning exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    @AuraEnabled
    public static HOG_CustomResponseImpl disableUser(Contact contact, Boolean isCommunityUser) {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        System.debug('Contact ' + contact);
        System.debug('isCommunityUser ' + isCommunityUser);

        try {
            if (isCommunityUser) {
                response = new VTT_LTNG_PortalManagementService().disableAsCommunityUser(contact);
            } else {
                response = new VTT_LTNG_PortalManagementService().disableAsInternalUser(contact);
            }

            System.debug(CLASS_NAME + ' disableUser DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' disableUser exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }
}