/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Base class for Email Services around EPD
Test Class:     Tested as part of all implementation.
History:        jschn 2019-06-28 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing abstract class EPD_EmailServiceBase {

    private static final String CLASS_NAME = String.valueOf(EPD_EmailServiceBase.class);

    protected List<EPD_Engine_Performance_Data__c> forms;
    protected Map<Id, List<Id>> recipientsMap;
    protected Map<Id, List<Messaging.EmailFileAttachment>> attachmentsByEPDId;
    protected Id templateId;

    /**
     * Constructor that sets all common variables.
     * As this Email service is designed to run in bulk, it works with mapped variables.
     *
     * @param forms
     */
    public void prepareVariables(List<EPD_Engine_Performance_Data__c> forms) {
        this.forms = forms;
        setEmailTemplate();
        buildRecipientsMap();
        attachmentsByEPDId = new Map<Id, List<Messaging.EmailFileAttachment>>();
        prepareImplementationSpecificVariables();
    }

    /**
     * This method will generate emails for sending and send them.
     * It will first set flags, then it will try to load template, set custom content if any is required,
     * set attachments, set sender and set recipients.
     * This is just base class, so actual implementation for all of those methods are in actual implementations
     * for certain type of EPD Email service.
     */
    public void sendEmails(List<EPD_Engine_Performance_Data__c> forms) {
        System.debug(CLASS_NAME + ' -> sendEmails. START');

        prepareVariables(forms);

        List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();

        for(EPD_Engine_Performance_Data__c form : forms) {
            if(validRecordForEmail(form)) {
                Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();

                System.debug(CLASS_NAME + ' -> sendEmails. Generating email for form ID: ' + form.Id);

                emailMessage.setSaveAsActivity(false);
                emailMessage.setWhatId(form.Id);
                setCustomContent(emailMessage, form);
                emailMessage.setFileAttachments(setAttachments(form));
                setSender(emailMessage);
                emailMessage.setToAddresses(recipientsMap.get(form.Id));
                setCCAddresses(emailMessage);

                System.debug(CLASS_NAME + ' -> sendEmails. Email generated Successfully: ' + JSON.serialize(emailMessage));

                emailMessages.add(emailMessage);
            } else {
                System.debug(CLASS_NAME + ' -> sendEmails. Skipping record: ' + form.Id);
            }
        }

        Messaging.sendEmail(emailMessages);

        System.debug(CLASS_NAME + ' -> sendEmails. END');
    }

    protected abstract void prepareImplementationSpecificVariables();
    protected abstract void buildRecipientsMap();
    protected abstract void setEmailTemplate();
    protected abstract List<Messaging.EmailFileAttachment> setAttachments(EPD_Engine_Performance_Data__c form);
    protected abstract void setSender(Messaging.SingleEmailMessage emailMessage);
    protected abstract void setCustomContent(Messaging.SingleEmailMessage emailMessage, EPD_Engine_Performance_Data__c form);
    protected abstract Boolean validRecordForEmail(EPD_Engine_Performance_Data__c form);
    protected abstract void setCCAddresses(Messaging.SingleEmailMessage emailMessage);

}