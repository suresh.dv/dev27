@isTest
public class PTValidateBillingTestClass {
    private static testmethod void ValidateTrigger(){
        try{
                PTTestData.createPTTestRecords();
                PTTestData.createVBBillingTestRecords();
            	PTValidateBillingCacheQuery.serviceListQueried = false;
            	PTValidateBillingRateCacheQuery.rateListQueried = false;
            	PTTestData.createVBBillingTestRecord2();
            	PTValidateBillingCacheQuery.serviceListQueried = false;
            	PTValidateBillingRateCacheQuery.rateListQueried = false;
            	PTTestData.createVBBillingTestRecord3();
            	PTValidateBillingCacheQuery.serviceListQueried = false;
            	PTValidateBillingRateCacheQuery.rateListQueried = false;
                PTTestData.createNEWVBBillingTestRecords();
        }catch(Exception e){
            System.assert(e.getMessage().contains('Site Id not found'),e.getMessage());
        }
    }
}