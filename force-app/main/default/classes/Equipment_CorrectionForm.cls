/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_CorrectionForm {
	@AuraEnabled
	public Equipment_Correction_Form__c correctionForm;
	@AuraEnabled
	public List<ContentDocumentLink> correctionFormFiles;
	@AuraEnabled
	public List<Attachment> correctionFormAttachments;

	public Equipment_CorrectionForm() {
		this.correctionForm = new Equipment_Correction_Form__c();
		this.correctionFormFiles = new List<ContentDocumentLink>();
		this.correctionFormAttachments = new List<Attachment>();
	}
}