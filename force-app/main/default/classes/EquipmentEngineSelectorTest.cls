/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EquipmentEngineSelector
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
private class EquipmentEngineSelectorTest {

    @IsTest
    static void getEngineByEquipmentId_withoutParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment_Engine__c> records = new EquipmentEngineSelector().getEngineByEquipmentId(null);
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEngineByEquipmentId_withWrongParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment_Engine__c> records = new EquipmentEngineSelector().getEngineByEquipmentId(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEngineByEquipmentId_withProperParam() {
        Integer expectedRecordCount = 1;
        EPD_TestData.createEPD();
        Equipment__c equipment = [SELECT Id FROM Equipment__c LIMIT 1];

        Test.startTest();
        List<Equipment_Engine__c> records = new EquipmentEngineSelector().getEngineByEquipmentId(equipment.Id);
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEnginesByEquipmentIds_withoutParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment_Engine__c> records = new EquipmentEngineSelector().getEngineByEquipmentId(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEnginesByEquipmentIds_withEmptyParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment_Engine__c> records = new EquipmentEngineSelector().getEnginesByEquipmentIds(new Set<Id>());
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEnginesByEquipmentIds_withWrongParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Equipment_Engine__c> records = new EquipmentEngineSelector().getEnginesByEquipmentIds( new Set<Id> { UserInfo.getUserId() } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

    @IsTest
    static void getEnginesByEquipmentIds_withProperParam() {
        Integer expectedRecordCount = 1;
        EPD_TestData.createEPD();
        Equipment__c equipment = [SELECT Id FROM Equipment__c LIMIT 1];

        Test.startTest();
        List<Equipment_Engine__c> records = new EquipmentEngineSelector().getEnginesByEquipmentIds( new Set<Id> { equipment.Id } );
        Test.stopTest();

        System.assertNotEquals(null, records);
        System.assertEquals(expectedRecordCount, records.size());
    }

}