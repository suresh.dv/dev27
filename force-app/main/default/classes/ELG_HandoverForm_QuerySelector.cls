/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:
 *  Test Class:		ELG_HandoverFrom_EndpointTest
 *  History:        Created on 7/11/2019   
 */

public inherited sharing class ELG_HandoverForm_QuerySelector implements ELG_HandoverForm_DAO {
	private static final String CLASS_NAME = String.valueOf(ELG_HandoverForm_QuerySelector.class);


	public ELG_Shift_Handover__c handoverForm(String handoverFormId) {
		return [
				SELECT Id,
						CreatedDate,
						AMU__c,
						Post__c,
						Incoming_Operator__r.Name,
						Outgoing_Operator__c,
						Shift_Status__c,
						Shift_Cycle__c,
						Signed_In__c,
						Signed_Off__c,
						Status__c,
						Reviewed__c,
						Sr_Supervisor__c,
						Sr_Supervisor__r.Name,
						Steam_Chief__c,
						Steam_Chief__r.Name,
						Shift_Lead__c,
						Shift_Lead__r.Name,
						Shift_Engineer__c,
						Shift_Engineer__r.Name,
						Review_Not_Required__c,
						Reviewed_by_Sr_Supervisor_Timestamp__c,
						Reviewed_by_Steam_Chief_Timestamp__c,
						Reviewed_by_Shift_Lead_Timestamp__c,
						Reviewed_by_Shift_Engineer_Timestamp__c,
						Shift_Assignement__c,
						Shift_Assignement__r.Trainee__r.Name,
						Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
						Shift_Assignement__r.Shift_Engineer__c,
						Shift_Assignement__r.Shift_Engineer__r.Name,
						Shift_Assignement__r.Shift_Status__c,
						Shift_Assignement__r.Primary__r.Name,
						Shift_Assignement__r.Post__r.Operating_Field_AMU__r.Name,
						Shift_Assignement__r.Post__r.Post_Name__c
				FROM ELG_Shift_Handover__c
				WHERE Id = :handoverFormId
				LIMIT 1
		];
	}

	public ELG_Shift_Assignement__c getNextShift(ELG_Shift_Handover__c handoverForm) {
		return [
				SELECT Id, Shift_Status__c
				FROM ELG_Shift_Assignement__c
				WHERE
				Post__c = :handoverForm.Shift_Assignement__r.Post__c
				AND Shift_Status__c = :ELG_Constants.SHIFT_NEW
				LIMIT 1
		];
	}

	public List<ELG_Handover_Category__c> getQuestionsBasedOnCategory(String categoryName) {
		return [
				SELECT Id,
						Order__c,
						RecordType_DeveloperName__c,
						Name, (
						SELECT Id,
								Name,
								Question__c,
								Order__c,
								Active__c,
								Question_UUID__c,
								Handover_Category__r.Name,
								Handover_Category__r.Order__c
						FROM Handover_Category_Questions__r
						WHERE Active__c = true
						ORDER BY Order__c ASC
				)
				FROM ELG_Handover_Category__c
				WHERE Name = :categoryName
		];

	}

	public List<ELG_Log_Entry__c> getLogEntriesByCategories(String shiftAssignementId) {

		return [
				SELECT Id,
						User_Log_Entry__c,
						Question__c,
						Handover_Category_Question__r.Order__c,
						Handover_Category_Question__r.Name,
						Handover_Category_Question__r.Question__c,
						Handover_Category_Question__r.Handover_Category__r.Order__c,
						Handover_Category_Question__r.Handover_Category__r.Name,
						CreatedBy.Name,
						CreatedDate
				FROM ELG_Log_Entry__c
				WHERE Shift_Assignement__c = :shiftAssignementId
				AND Handover_Category_Question__c != NULL
				ORDER BY
						Handover_Category_Question__r.Handover_Category__r.Order__c,
						Handover_Category_Question__r.Order__c,
						CreatedDate
				LIMIT 10000
		];

	}

	public List<ELG_Log_Entry__c> getGenericLogEntries(String shiftAssignmentId, String recordTypeId) {
		return [
				SELECT
						Id,
						Post__c,
						Shift_Assignement__c,
						User__c,
						User__r.Name,
						User_Log_Entry__c,
						CreatedDate,
						Operating_Field_AMU__c,
						RecordTypeId, (
						SELECT
								Id,
								User_Log_Entry__c,
								User__c,
								User__r.Name,
								CreatedDate
						FROM Correction_Entries__r
				)
				FROM ELG_Log_Entry__c
				WHERE Shift_Assignement__c = :shiftAssignmentId AND RecordTypeId = :recordTypeId
				AND Original_Log_Entry__c = NULL
				ORDER BY CreatedDate ASC
		];

	}

	public List<PermissionSetAssignment> getUserPermissions(String permissionSetName, String userId) {
		return [
				SELECT Id
				FROM PermissionSetAssignment
				WHERE PermissionSet.Name = :permissionSetName
				AND AssigneeId = :userId
		];
	}

}