global with sharing class HOG_SAPEquipmentService {

	private static Set<String> functionalLocationIds;
	private static Map<String, Facility__c> facilityMap;
	private static Map<String, Location__c> wellIdMap;
	private static Map<String,System__c> systemMap;
	private static Map<String,Sub_System__c> subSystemMap;
	private static Map<String, Functional_Equipment_Level__c> felMap;
	private static Map<String, Yard__c> yardMap;
	private static Map<String, Well_Event__c> wellEventMap;
	private static List<Equipment__c> superiorEquipment;
	private static List<Equipment__c> inferiorEquipment;

	/************************* SOAP Interface *****************************************/
	webservice static HOG_SAPApiUtils.HOG_SAPEquipmentServiceResponse processEquipment(
		HOG_SAPApiUtils.DT_SAP_EQUIP_LIST MT_SAP_EQUIPMENT) {
		initialize(MT_SAP_EQUIPMENT.EQUIPMENT);
		HOG_SAPApiUtils.HOG_SAPEquipmentServiceResponse response = processPayload(MT_SAP_EQUIPMENT.EQUIPMENT);
		return response;
	}
	/**********************************************************************************/

	private static void initialize(List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM> equipmentPayload) {
		parseFunctionalLocationIds(equipmentPayload);
		populateFacilityMap();
		populateWellIdMap();
		populateWellEventMap();
		populateSystemMap();
		populateSubSystemMap();
		poplulateFelMap();
		populateYardMap();
		superiorEquipment = new List<Equipment__c>();
		inferiorEquipment = new List<Equipment__c>();
	}

	private static void parseFunctionalLocationIds(List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM> equipmentPayload) {
		functionalLocationIds = new Set<String>();
		for(HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM equipmentPayloadEntry : equipmentPayload) {
			if(!String.isBlank(equipmentPayloadEntry.RIHEQUI_LIST.TPLNR)) 
				functionalLocationIds.add(equipmentPayloadEntry.RIHEQUI_LIST.TPLNR);
		}
	}

	private static void populateFacilityMap() {
		facilityMap = new Map<String, Facility__c>();
		for(Facility__c facility : [Select Id, Functional_Location__c
									From Facility__c
									Where Functional_Location__c In :functionalLocationIds]) {
			facilityMap.put(facility.Functional_Location__c, facility);
		}
	}

	private static void populateWellIdMap() {
		wellIdMap = new Map<String, Location__c>();
		for(Location__c wellId : [Select Id, Functional_Location_Description_SAP__c
								  From Location__c
								  Where Functional_Location_Description_SAP__c In :functionalLocationIds]) {
			wellIdMap.put(wellId.Functional_Location_Description_SAP__c, wellId);
		}
	}

	private static void populateWellEventMap() {
		wellEventMap = new Map<String, Well_Event__c>();
		for(Well_Event__c wellEvent : [Select Id, Functional_Location__c
									   From Well_Event__c
									   Where Functional_Location__c In :functionalLocationIds]) {
			wellEventMap.put(wellEvent.Functional_Location__c, wellEvent);
		}
	}
	
	//m.z. 07.07.2017 Start
	private static void populateSystemMap() {
		systemMap = new Map<String, System__c>();
		for(System__c sys : [Select Id, Functional_Location_Description_SAP__c
						     From System__c
							 Where Functional_Location_Description_SAP__c In :functionalLocationIds]) {
			systemMap.put(sys.Functional_Location_Description_SAP__c, sys);
		}
	}
	
	private static void populateSubSystemMap() {
		subSystemMap = new Map<String, Sub_System__c>();
		for(Sub_System__c subSys : [Select Id, Functional_Location_Description_SAP__c
						            From Sub_System__c
							        Where Functional_Location_Description_SAP__c In :functionalLocationIds]) {
			subSystemMap.put(subSys.Functional_Location_Description_SAP__c, subSys);
		}
	}
    //m.z. 07.07.2017 End
    
    //mb 24.8.2017 Start
    private static void poplulateFelMap() {
		felMap = new Map<String, Functional_Equipment_Level__c>();
		for(Functional_Equipment_Level__c fel : [Select Id, Functional_Location_Description_SAP__c
						     From Functional_Equipment_Level__c
							 Where Functional_Location_Description_SAP__c In :functionalLocationIds]) {
			felMap.put(fel.Functional_Location_Description_SAP__c, fel);
		}
	}
	
	private static void populateYardMap() {
		yardMap = new Map<String, Yard__c>();
		for(Yard__c yard : [Select Id, Functional_Location_Description_SAP__c
						            From Yard__c
							        Where Functional_Location_Description_SAP__c In :functionalLocationIds]) {
			yardMap.put(yard.Functional_Location_Description_SAP__c, yard);
		}
	}

    //mb 24.8.2017 end
	private static HOG_SAPApiUtils.HOG_SAPEquipmentServiceResponse 
		processPayload(List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM> equipmentPayload) {
		HOG_SAPApiUtils.HOG_SAPEquipmentServiceResponse response = new HOG_SAPApiUtils.HOG_SAPEquipmentServiceResponse();
		response.addResults(parseEquipment(equipmentPayload));
		response.addResults(upsertEquipment());
		return response;
	}

	private static List<HOG_SAPApiUtils.Result> parseEquipment(
		List<HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM> equipmentPayload) {
		HOG_EquipmentFactoryUtils.EquipmentFactory equipmentFactory =
			new HOG_EquipmentFactoryUtils.EquipmentFactory();
		List<HOG_SAPApiUtils.Result> results = new List<HOG_SAPApiUtils.Result>();

		//Parse Equipment
		for(HOG_SAPApiUtils.DT_SAP_EQUIP_ITEM equipmentPayloadEntry : equipmentPayload) {
			if(equipmentPayloadEntry.RIHEQUI_LIST == null || String.isBlank(equipmentPayloadEntry.RIHEQUI_LIST.TPLNR)) {
				HOG_SAPApiUtils.Result errorResult = new HOG_SAPApiUtils.Result(false, 
					equipmentPayloadEntry.RIHEQUI_LIST.EQUNR, false, HOG_SAPApiUtils.STATUS_ERROR_PARSE);
				errorResult.addError(new HOG_SAPApiUtils.Error(null, 'Invalid equipment payload either payload null or TPLNR missing!', 
					null));
				results.add(errorResult);
			    continue;//Skip to the next iteration, to process the payload entry into floc
			}

			if(!equipmentPayloadEntry.RIHEQUI_LIST.TPLNR.startsWithIgnoreCase('H1')) {
				HOG_SAPApiUtils.Result errorResult = new HOG_SAPApiUtils.Result(false, equipmentPayloadEntry.RIHEQUI_LIST.EQUNR,
					false, HOG_SAPApiUtils.STATUS_ERROR_INVALID_RECORD);
				errorResult.addError(new HOG_SAPApiUtils.Error(null, 
					'Equipment was not attached to a HOG FLOC object.', null));
				results.add(errorResult);
			}

			try {
				Equipment__c eq = equipmentFactory.getEquipment(equipmentPayloadEntry);

				//Assign to right floc
				if(facilityMap.containsKey(eq.Functional_Location__c)) {
					eq.Facility__r = new Facility__c(
						Functional_Location__c = eq.Functional_Location__c
					);
					eq.Location__c = null;
					eq.Well_Event__c = null;
					eq.System__c = null;
					eq.Sub_System__c = null;
					eq.Functional_Equipment_Level__c = null;
					eq.Yard__c = null;

				} else if (wellIdMap.containsKey(eq.Functional_Location__c)) {
					eq.Location__r = new Location__c(
						Functional_Location_Description_SAP__c = eq.Functional_Location__c
					);
					eq.Facility__c = null;
					eq.Well_Event__c = null;
					eq.System__c = null;
					eq.Sub_System__c = null;
					eq.Functional_Equipment_Level__c = null;
					eq.Yard__c = null;

				} else if (wellEventMap.containsKey(eq.Functional_Location__c)) {
					eq.Well_Event__r = new Well_Event__c(
						Functional_Location__c = eq.Functional_Location__c
					);
					eq.Facility__c = null;
					eq.Location__c = null;
					eq.System__c = null;
					eq.Sub_System__c = null;
					eq.Functional_Equipment_Level__c = null;
					eq.Yard__c = null;

				} else if (systemMap.containsKey(eq.Functional_Location__c)) {
					eq.System__r = new System__c(
						Functional_Location_Description_SAP__c = eq.Functional_Location__c
					);
					eq.Facility__c = null;
					eq.Location__c = null;
					eq.Well_Event__c = null;
					eq.Sub_System__c = null;
					eq.Functional_Equipment_Level__c = null;
					eq.Yard__c = null;

				} else if (subSystemMap.containsKey(eq.Functional_Location__c)) {
					eq.Sub_System__r = new Sub_System__c(
						Functional_Location_Description_SAP__c = eq.Functional_Location__c
					);
					eq.Facility__c = null;
					eq.Location__c = null;
					eq.Well_Event__c = null;
					eq.System__c = null;
					eq.Functional_Equipment_Level__c = null;
					eq.Yard__c = null;

				} else if (felMap.containsKey(eq.Functional_Location__c)) {
					eq.Sub_System__c = null;
					eq.Facility__c = null;
					eq.Location__c = null;
					eq.Well_Event__c = null;
					eq.System__c = null;
					eq.Functional_Equipment_Level__r = new Functional_Equipment_Level__c(
						Functional_Location_Description_SAP__c = eq.Functional_Location__c
					);
					eq.Yard__c = null;
					
				}else if (yardMap.containsKey(eq.Functional_Location__c)) {
					eq.Sub_System__c = null;
					eq.Facility__c = null;
					eq.Location__c = null;
					eq.Well_Event__c = null;
					eq.System__c = null;
					eq.Functional_Equipment_Level__c = null;
					eq.Yard__r = new Yard__c(
						Functional_Location_Description_SAP__c = eq.Functional_Location__c
					);
					
				} else {
					HOG_SAPApiUtils.Result errorResult = new HOG_SAPApiUtils.Result(false, eq.Equipment_Number__c,
						false, HOG_SAPApiUtils.STATUS_ERROR_PARSE);
					errorResult.addError(new HOG_SAPApiUtils.Error(null, 
						'Functional Location did not match an existing functional location: ' + eq.Functional_Location__c, null));
					results.add(errorResult);
					continue; //skip this iteration due to parsing error
				}

				//Superior or Not and only stage for upsert if no error
				if(eq.Superior_Equipment__r != null) {
					inferiorEquipment.add(eq);
				} else {
					superiorEquipment.add(eq);
				}

				//Add parsing success
				//HOG_SAPApiUtils.Result successResult = new HOG_SAPApiUtils.Result(false, eq.Equipment_Number__c,
				//	true, HOG_SAPApiUtils.STATUS_SUCCESS_PARSE);
				//results.add(successResult);
			} catch (Exception ex) {
				HOG_SAPApiUtils.Result errorResult = new HOG_SAPApiUtils.Result(false, equipmentPayloadEntry.RIHEQUI_LIST.EQUNR,
					false, HOG_SAPApiUtils.STATUS_ERROR_PARSE);
				errorResult.addError(new HOG_SAPApiUtils.Error(null, 
					ex.getMessage() + '\n' + ex.getStackTraceString(), null));
				results.add(errorResult);
			}
		}

		return results;
	}

	private static List<HOG_SAPApiUtils.Result> upsertEquipment() {
		List<Equipment__c> allEquipment = new List<Equipment__c>();
		List<HOG_SAPApiUtils.Result> results = new List<HOG_SAPApiUtils.Result>();
		List<Database.UpsertResult> upsertResults = new List<Database.UpsertResult>();

		upsertResults.addAll(Database.upsert(superiorEquipment, Equipment__c.fields.Equipment_Number__c, false));
		allEquipment.addAll(superiorEquipment);
		upsertResults.addAll(Database.upsert(inferiorEquipment, Equipment__c.fields.Equipment_Number__c, false));
		allEquipment.addAll(inferiorEquipment);
		
		System.debug('debug_allEquipment: ' + allEquipment);

		//Consolidate into api resuls for response
		for(Integer i=0; i < upsertResults.size(); i++) {
			Database.UpsertResult upsertResult = upsertResults[i];
			String statusCode = upsertResult.isSuccess() ? HOG_SAPApiUtils.STATUS_SUCCESS_UPSERT : 
				HOG_SAPApiUtils.STATUS_ERROR_UPSERT;
			String idString = (String) allEquipment[i].get('Equipment_Number__c');
			HOG_SAPApiUtils.Result result = new HOG_SAPApiUtils.Result(upsertResult.isCreated(), idString, 
				upsertResult.isSuccess(), statusCode);
			for(Database.Error err : upsertResult.getErrors()) {
				result.addError(new HOG_SAPApiUtils.Error(err.getFields(), err.getMessage(), err.getStatusCode()));
			}
			results.add(result);
		}
        System.debug('debug_results: ' + results);
		return results;
	}
}