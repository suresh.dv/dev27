/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:
 *  Test Class		ELG_TaskSearch_EndpointTest
 *  History:        Created on 10/31/2019   
 */

public class ELG_TaskSearch_EndpointService {

	private static Boolean defaultViewTaskBuilder;
	private static Boolean completedViewTaskBuilder;
	private static Boolean cancelledViewTaskBuilder;

	private static String fieldsToQuery = 'Id, ' +
			'Name, ' +
			'CreatedDate, ' +
			'CreatedBy.Name, ' +
			'Operating_Field_AMU__r.Name, ' +
			'Short_Description__c, ' +
			'Task_Description__c, ' +
			'Post__c, ' +
			'Post_Name__c, ' +
			'Status__c, ' +
			'Comment__c, ' +
			'Reason__c, ' +
			'Finished_By__r.Name, ' +
			'Finished_At__c';

	public static List<TemporaryClassToBuildObject> getAvailablePosts(String currentAMU) {
		List<ELG_Post__c> availablePosts = [
				SELECT Id, Post_Name__c
				FROM ELG_Post__c
				WHERE Operating_Field_AMU__c = :currentAMU
		];

		List<TemporaryClassToBuildObject> postsList = new List<TemporaryClassToBuildObject>();

		for (ELG_Post__c post : availablePosts) {
			postsList.add(new TemporaryClassToBuildObject(post.Id, post.Post_Name__c));
		}

		return postsList;

	}

	//Whole search logic
	public static List<DataTableWrapper> searchResults(
			Object filterSelection,
			Integer offsetSize,
			String fieldSort,
			String sortDirection,
			Boolean defaultView,
			Boolean completedView,
			Boolean cancelledView) {

		List<ELG_Task__c> queriedTasks = new List<ELG_Task__c>();
		Integer numberOfTasks = 0;

		defaultViewTaskBuilder = defaultView;
		completedViewTaskBuilder = completedView;
		cancelledViewTaskBuilder = cancelledView;

		FilterWrapper filterCriteria = (FilterWrapper) JSON.deserialize(JSON.serialize(filterSelection), FilterWrapper.class);
		String startDate = filterCriteria.startDate + 'T00:00:00z';
		String endDate = filterCriteria.endDate + 'T23:59:59z';

		queriedTasks = selectedFilter(fieldsToQuery,
				'ELG_Task__c',
				stringBuilder(JSON.serialize(filterCriteria), startDate, endDate, offsetSize, true, fieldSort, sortDirection));

		if (!queriedTasks.isEmpty()) {
			numberOfTasks = handoverCounter(stringBuilder(JSON.serialize(filterCriteria), startDate, endDate, 0, false, '', ''));
		}

		return objectBuilder(queriedTasks, numberOfTasks);
	}

	@TestVisible
	private static List<DataTableWrapper> objectBuilder(List<ELG_Task__c> queriedTasks, Integer numberOfTasks) {

		List<DataTableWrapper> response = new List<DataTableWrapper>();
		DataTableWrapper taskError = new DataTableWrapper();
		if (queriedTasks.isEmpty()) {
			taskError.hasError = true;
			taskError.errorMsg = ELG_Constants.NO_TASK_FOUND;
			response.add(taskError);
		} else if (numberOfTasks > 2050) {
			response.clear();
			taskError.hasError = true;
			taskError.errorMsg = ELG_Constants.maximumQueryLimit('Tasks', numberOfTasks);
			response.add(taskError);
		} else {
			for (ELG_Task__c task : queriedTasks) {
				DataTableWrapper preparedTask = new DataTableWrapper();
				preparedTask.taskId = task.Id;
				preparedTask.taskName = task.Name + '-' + task.Post_Name__c;
				preparedTask.nameUrl = '/' + task.Id;
				preparedTask.amuName = task.Operating_Field_AMU__r.Name;
				preparedTask.taskStatus = task.Status__c;
				preparedTask.shortDescription = task.Short_Description__c;
				if(defaultViewTaskBuilder) {
					if(task.Status__c == ELG_Constants.TASK_COMPLETED) {
						preparedTask.operators = 'Created By: ' +task.CreatedBy.Name + '\nCompleted By: ' +task.Finished_By__r.Name;
						preparedTask.dates = 'Created Date: ' + dateFormatter(task.CreatedDate) + '\nCompleted Date: ' + dateFormatter(task.Finished_At__c);
					}

					if(task.Status__c == ELG_Constants.TASK_CANCELLED) {
						preparedTask.operators = 'Created By: ' +task.CreatedBy.Name + '\nCancelled By: ' +task.Finished_By__r.Name;
						preparedTask.dates = 'Created Date: ' + dateFormatter(task.CreatedDate) + '\nCancelled Date: ' + dateFormatter(task.Finished_At__c);
					}

					if(task.Status__c == ELG_Constants.TASK_ACTIVE) {
						preparedTask.operators = 'Created By: ' +task.CreatedBy.Name;
						preparedTask.dates = 'Created Date: ' + dateFormatter(task.CreatedDate) ;
					}

				} else if (completedViewTaskBuilder) {
					preparedTask.taskComment = task.Comment__c;
					preparedTask.operators = 'Created By: ' +task.CreatedBy.Name + '\nCompleted By: ' +task.Finished_By__r.Name;
					preparedTask.dates = 'Created Date: ' + dateFormatter(task.CreatedDate) + '\nCompleted Date: ' + dateFormatter(task.Finished_At__c);
				} else if (cancelledViewTaskBuilder) {
					preparedTask.taskCancelReason = task.Reason__c;
					preparedTask.operators = 'Created By: ' +task.CreatedBy.Name + '\nCancelled By: ' +task.Finished_By__r.Name;
					preparedTask.dates = 'Created Date: ' + dateFormatter(task.CreatedDate) + '\nCancelled Date: ' + dateFormatter(task.Finished_At__c);
				}
				preparedTask.hasError = false;
				response.add(preparedTask);
			}
		}

		response[0].numberOfRecords = numberOfTasks;

		return response;
	}

	private static String dateFormatter(Datetime dateToConvert) {
		Datetime myDatetime = dateToConvert;
		String str = myDatetime.format('d,MMM yyyy, h:mm a');

		return str;
	}

	private static List<SObject> selectedFilter(String fields, String objectName, String filterSelection) {
		String preparedQuery = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				fields,
				objectName,
				filterSelection
		});

		return Database.query(preparedQuery);
	}


	private static Integer handoverCounter(String filteredQuery) {

		String numberOfRecordsBasedOnFilterCriteria = String.format('SELECT {0} FROM {1} WHERE {2}', new List<String>{
				'COUNT()',
				'ELG_Task__c',
				filteredQuery
		});

		return Database.countQuery(numberOfRecordsBasedOnFilterCriteria);
	}

	private static String stringBuilder(
			String filterSelection,
			String startDate,
			String endDate,
			Integer offsetSize,
			Boolean limitations,
			String fieldSort,
			String sortDirection) {

		List<String> whereBuilder = new List<String>();
		List<String> temporaryList = new List<String>();
		String tempoString;

		tempoString = filterSelection.replace('amuId', 'Operating_Field_AMU__c');
		tempoString = tempoString.replace('postId', 'Post__c');
		tempoString = tempoString.replace('taskStatus', 'Status__c');
		temporaryList = tempoString.remove('{').remove('}').remove('"').split(',');

		for (String str : temporaryList) {
			if (!str.contains('null')) {
				whereBuilder.add(str);
			}
		}

		String whereClause = '';

		for (String filterItem : whereBuilder) {
			List<String> filterPair = filterItem.split(':');
			if (String.isNotBlank(whereClause)) {
				whereClause += ' AND';
			}
			if (filterPair.contains('startDate')) {
				whereClause += ' CreatedDate >= ' + startDate;
			} else if (filterPair.contains('endDate')) {
				whereClause += ' CreatedDate <= ' + endDate;
			} else {
				whereClause += ' ' + filterPair.get(0) + ' = \'' + filterPair.get(1) + '\'';
			}
		}

		if (limitations) {
			whereClause += ' ORDER BY ' + fieldSort + ' ' + sortDirection + ' LIMIT 50';
			whereClause += ' OFFSET ' + offsetSize;
		}

		System.debug('BUILD QUERY ' + whereClause);

		return whereClause;
	}

	//From LWC process parameters
	public class FilterWrapper {
		@AuraEnabled
		public String amuId;
		@AuraEnabled
		public String postId;
		@AuraEnabled
		public String taskStatus;
		@AuraEnabled
		public String startDate;
		@AuraEnabled
		public String endDate;
	}


	//Class to prepare Object for LWC
	public class TemporaryClassToBuildObject {
		@AuraEnabled
		public String value;
		@AuraEnabled
		public String label;

		public TemporaryClassToBuildObject(String amuId, String amuName) {
			this.value = amuId;
			this.label = amuName;
		}
	}

	//Class that bring formatted data for LWC Data table
	public class DataTableWrapper {
		@AuraEnabled
		public Id taskId;
		@AuraEnabled
		public String taskName;
		@AuraEnabled
		public String nameUrl;
		@AuraEnabled
		public String amuName;
		@AuraEnabled
		public String postName;
		@AuraEnabled
		public String taskStatus;
		@AuraEnabled
		public String shortDescription;
		@AuraEnabled
		public String operators;
		@AuraEnabled
		public String dates;
		@AuraEnabled
		public String taskComment;
		@AuraEnabled
		public String taskCancelReason;
		@AuraEnabled
		public Integer numberOfRecords;
		@AuraEnabled
		public String errorMsg;
		@AuraEnabled
		public Boolean hasError = false;

		@AuraEnabled
		public String label;
		@AuraEnabled
		public String fieldName;
		@AuraEnabled
		public String type;

	}
}