/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Notification service for new type of Vent Gas Alerts. (Engineering Notification)
                It's designed to be used when Alerts are created to notify assigned Production Engineer
Test Class:     VG_EmailNotificationServiceTest
History:        jschn 24/01/2020 - Created. US 001751
*************************************************************************************************/
public inherited sharing class VG_EmailNotificationService {

    private final String EMAIL_SUBJECT = 'New Engineering Notification Vent Gas Alert(s)';

    @TestVisible
    private static HOG_Vent_Gas_Alert_Configuration__c settings = HOG_Vent_Gas_Alert_Configuration__c.getInstance();
    private final String baseUrl = System.Url.getSalesforceBaseUrl().getHost();
    private final OrgWideEmailAddress sender;

    @TestVisible
    private List<Messaging.SingleEmailMessage> emailList;

    /**
     * Constructor that tries to get prepare Sender for generated notifications
     */
    public VG_EmailNotificationService() {
        try{
            if(settings != null && String.isNotBlank(settings.Default_Notification_Sender_Name__c)) {
                sender = [
                        SELECT DisplayName, Address
                        FROM OrgWideEmailAddress
                        WHERE DisplayName = :settings.Default_Notification_Sender_Name__c
                        LIMIT 1
                ];
            }
        } catch(Exception ex) {
            System.debug('VG_EmailNotificationService constructor failure: ' +
                    'Org wide default address is not set or Custom Settings are wrongly configured. ' +
                    'Exception: ' + ex.getMessage());
        }
    }

    /**
     * Builds List of Email messages to be sent based on provided alerts.
     *
     * @param alerts
     *
     * @return VG_EmailNotificationService
     */
    public VG_EmailNotificationService buildNotifications(List<HOG_Vent_Gas_Alert__c> alerts) {
        emailList = new List<Messaging.SingleEmailMessage>();

        if(alerts != null) {
            Map<Id, List<HOG_Vent_Gas_Alert__c>> alertsByProductionEngineers = groupByProductionEngineers(alerts);

            for(Id productionEngineerId : alertsByProductionEngineers.keySet()) {
                emailList.add(
                        buildNotificationsForProductionEngineer(
                                alertsByProductionEngineers.get(productionEngineerId),
                                productionEngineerId
                        )
                );
            }
        }

        return this;
    }

    /**
     * Method that after validation of current Session limits sends Email notification based on generated email list
     */
    public void sendNotifications() {
        if(emailList != null && emailList.size() > 0) {
            if (Limits.getEmailInvocations() < Limits.getLimitEmailInvocations()) {
                Messaging.reserveSingleEmailCapacity(emailList.size());
                Messaging.sendEmail(emailList);
            }
        }
    }

    /**
     * Groups Alerts by Id of assigned Production Engineer. That way this service can send one notification
     * per Production Engineer with all new Alerts.
     *
     * @param alerts
     *
     * @return Map<Id, List<HOG_Vent_Gas_Alert__c>>
     */
    private Map<Id, List<HOG_Vent_Gas_Alert__c>> groupByProductionEngineers(List<HOG_Vent_Gas_Alert__c> alerts) {
        Map<Id, List<HOG_Vent_Gas_Alert__c>> groupedAlerts = new Map<Id, List<HOG_Vent_Gas_Alert__c>>();

        for(HOG_Vent_Gas_Alert__c alert : alerts) {

            if(!groupedAlerts.containsKey(alert.Production_Engineer__c)) {
                groupedAlerts.put(alert.Production_Engineer__c, new List<HOG_Vent_Gas_Alert__c>());
            }

            groupedAlerts.get(alert.Production_Engineer__c).add(alert);
        }

        return groupedAlerts;
    }

    /**
     * Method that builds Email Notification record for Alerts provided as param. All of them are set to be delivered to
     * User that Id is provided as second param.
     *
     * @param alerts
     * @param productionEngineerId
     *
     * @return Messaging.SingleEmailMessage
     */
    private Messaging.SingleEmailMessage buildNotificationsForProductionEngineer(List<HOG_Vent_Gas_Alert__c> alerts, Id productionEngineerId) {
        Messaging.SingleEmailMessage emailMessage = new Messaging.SingleEmailMessage();
        setConfigurationForMessage(emailMessage, productionEngineerId);
        setBodyOfMessage(emailMessage, alerts);

        return emailMessage;
    }

    /**
     * Sets basic configuration for Email Message.
     * Like recipient, Subject, Sender, etc.
     *
     * @param emailMessage
     * @param productionEngineerId
     */
    private void setConfigurationForMessage(Messaging.SingleEmailMessage emailMessage, Id productionEngineerId) {
        emailMessage.setTargetObjectId(productionEngineerId);
        emailMessage.setSubject(EMAIL_SUBJECT);
        emailMessage.setSaveAsActivity(false);
        if (sender != null && !Test.isRunningTest()) emailMessage.setOrgWideEmailAddressId(sender.Id);
    }

    /**
     * Builds content of Email msg based on provided Alerts.
     *
     * @param emailMessage
     * @param alerts
     */
    private void setBodyOfMessage(Messaging.SingleEmailMessage emailMessage, List<HOG_Vent_Gas_Alert__c> alerts) {
        String messageBody = '';

        for(HOG_Vent_Gas_Alert__c alert : alerts) {

            String urlAlert = '<a href="https://' + baseUrl + '/' + alert.Id + '">' + alert.Name + '</a>';
            String urlWellLocation = '<a href="https://'+ baseUrl + '/' + alert.Well_Location__c + '">' + alert.Well_Location__r.Name + '</a>';

            messageBody += '<table>';
            messageBody += '<tr><td>Alert: </td><td>' + urlAlert + '<td/></tr>';
            messageBody += '<tr><td>Type: </td><td>' + alert.Type__c + '<td/></tr>';
            messageBody += '<tr><td>Status: </td><td>' + alert.Status__c + '<td/></tr>';
            messageBody += '<tr><td>Comments: </td><td>' + (alert.Comments__c <> null ? alert.Comments__c : '') + '<td/></tr>';
            messageBody += '<tr><td>Production Engineer: </td><td>' + alert.Production_Engineer__r.Name + '<td/></tr>';
            messageBody += '<tr><td>Well Location: </td><td>' + urlWellLocation + '<td/></tr>';
            messageBody += '<tr><td>Priority: </td><td>' + alert.Priority__c + '<td/></tr>';
            messageBody += '<tr><td>Start Date:</td><td>' + (alert.Start_Date__c <> null ? alert.Start_Date__c.format() : 'Not Started') + '<td/></tr>';
            messageBody += '</table>';
            messageBody += '<hr/>';

        }

        emailMessage.setHtmlBody(messageBody);
    }

}