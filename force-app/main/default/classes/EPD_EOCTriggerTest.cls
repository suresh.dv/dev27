/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EOCTrigger
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
@IsTest
private class EPD_EOCTriggerTest {

    @IsTest
    static void delete_allowed() {
        EPD_Engine_Operating_Condition__c EOCRecord = EPD_TestData.createEOC();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = false;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete EOCRecord;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void delete_restricted() {
        EPD_Engine_Operating_Condition__c EOCRecord = EPD_TestData.createEOC();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = true;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete EOCRecord;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}