/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EquipmentEngineCreatorBatch
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
private class EquipmentEngineCreatorBatchTest {

    @IsTest
    static void testBatchWithoutMode_withoutRecords() {
        Integer expectedCount = [SELECT COUNT() FROM Equipment_Engine__c];

        Test.startTest();
        Database.executeBatch(
                new EquipmentEngineCreatorBatch(null)
        );
        Test.stopTest();

        Integer currentCount = [SELECT COUNT() FROM Equipment_Engine__c];

        System.assertEquals(expectedCount, currentCount);
    }

    @IsTest
    static void testBatchWithoutMode_withRecords() {
        EPD_TestData.createEquipmentForBatch();
        Integer expectedCount = [SELECT COUNT() FROM Equipment_Engine__c];
        expectedCount++;

        Test.startTest();
        Database.executeBatch(
                new EquipmentEngineCreatorBatch(null)
        );
        Test.stopTest();

        Integer currentCount = [SELECT COUNT() FROM Equipment_Engine__c];

        System.assertEquals(expectedCount, currentCount);


    }

    @IsTest
    static void testBatchAllMode_withoutRecords() {
        Integer expectedCount = [SELECT COUNT() FROM Equipment_Engine__c];

        Test.startTest();
        Database.executeBatch(
                new EquipmentEngineCreatorBatch(EquipmentEngineBatchMode.ALL_RECORDS)
        );
        Test.stopTest();

        Integer currentCount = [SELECT COUNT() FROM Equipment_Engine__c];

        System.assertEquals(expectedCount, currentCount);
    }

    @IsTest
    static void testBatchAllMode_withRecords() {
        EPD_TestData.createEquipmentForBatch();
        Integer expectedCount = [SELECT COUNT() FROM Equipment_Engine__c];
        expectedCount++;

        Test.startTest();
        Database.executeBatch(
                new EquipmentEngineCreatorBatch(EquipmentEngineBatchMode.ALL_RECORDS)
        );
        Test.stopTest();

        Integer currentCount = [SELECT COUNT() FROM Equipment_Engine__c];

        System.assertEquals(expectedCount, currentCount);
    }

    @IsTest
    static void testBatchNewRecsMode_withoutRecords() {
        Integer expectedCount = [SELECT COUNT() FROM Equipment_Engine__c];

        Test.startTest();
        Database.executeBatch(
                new EquipmentEngineCreatorBatch(EquipmentEngineBatchMode.NEW_RECORDS)
        );
        Test.stopTest();

        Integer currentCount = [SELECT COUNT() FROM Equipment_Engine__c];

        System.assertEquals(expectedCount, currentCount);

    }

    @IsTest
    static void testBatchNewRecsMode_withRecords() {
        EPD_TestData.createEquipmentForBatch();
        Integer expectedCount = [SELECT COUNT() FROM Equipment_Engine__c];
        expectedCount++;

        Test.startTest();
        Database.executeBatch(
                new EquipmentEngineCreatorBatch(EquipmentEngineBatchMode.NEW_RECORDS)
        );
        Test.stopTest();

        Integer currentCount = [SELECT COUNT() FROM Equipment_Engine__c];

        System.assertEquals(expectedCount, currentCount);
    }

}