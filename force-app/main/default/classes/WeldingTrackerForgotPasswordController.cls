/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class WeldingTrackerForgotPasswordController {
    public String username {get; set;}   
       
    public WeldingTrackerForgotPasswordController() {}
    
    public PageReference forgotPassword() {
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.WeldingTrackerForgotPasswordConfirm;
        pr.setRedirect(true);
        
        if (success) {              
            return pr;
        }
        return null;    
    }
}