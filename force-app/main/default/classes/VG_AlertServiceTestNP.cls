/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non Parallel unit test for VG_AlertService
History:        jschn 24/01/2020 - Created.
*************************************************************************************************/
@IsTest
private class VG_AlertServiceTestNP {

    @IsTest
    static void isValidUserForEngNotifAlert_generalUserWithoutPS() {
        VG_AlertService service = new VG_AlertService();
        User usr = HOG_VentGas_TestData.createUser();
        Boolean result;
        Boolean expectedResult = false;

        System.runAs(usr) {
            Test.startTest();
            result = service.isValidUserForEngNotifAlert();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidUserForEngNotifAlert_admin() {
        VG_AlertService service = new VG_AlertService();
        User usr = HOG_VentGas_TestData.createAdmin();
        Boolean result;
        Boolean expectedResult = true;

        System.runAs(usr) {
            Test.startTest();
            result = service.isValidUserForEngNotifAlert();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidUserForEngNotifAlert_mock_generalUserWithPS() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.permissionSelector = new PermissionMockSuccess();
        User usr = HOG_VentGas_TestData.createUser();
        Boolean result;
        Boolean expectedResult = true;

        System.runAs(usr) {
            Test.startTest();
            result = service.isValidUserForEngNotifAlert();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void isValidUserForEngNotifAlert_mock_generalUserQueryError() {
        VG_AlertService service = new VG_AlertService();
        VG_AlertService.permissionSelector = new PermissionMockFail();
        User usr = HOG_VentGas_TestData.createUser();
        Boolean result;
        Boolean expectedResult = false;

        System.runAs(usr) {
            Test.startTest();
            result = service.isValidUserForEngNotifAlert();
            Test.stopTest();
        }

        System.assertEquals(expectedResult, result);
    }

    class PermissionMockSuccess implements HOG_PermissionDAO {
        public List<PermissionSet> getPermissionSetsByName(Set<String> names) {
            return [SELECT Id FROM PermissionSet LIMIT 1];
        }
        public Integer getAssignmentCountForPermissions(Id userId, Set<Id> permissionIds) {
            return 1;
        }
    }
    class PermissionMockFail implements HOG_PermissionDAO {
        public List<PermissionSet> getPermissionSetsByName(Set<String> names) {
            throw new HOG_Exception('Simulated Query error');
        }
        public Integer getAssignmentCountForPermissions(Id userId, Set<Id> permissionIds) {
            return 1;
        }
    }

}