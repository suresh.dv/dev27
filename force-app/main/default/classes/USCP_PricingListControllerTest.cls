/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_PricingListControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_Pricing;
            Test.setCurrentPageReference(pageRef);


            //lets create some test terminal
            USCP_Terminal__c terminal =   USCP_TestData.createTerminal('trm1', 'addr1', 'city1', 'state1', 'opis1', true);
            System.AssertNotEquals(terminal.Id, Null);

            
            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            insert account;
            System.AssertNotEquals(account.Id, Null);

            Date dt =  Date.today();

            //create test product
            Product2 product = USCP_TestData.createProduct('product1',true);

            //create rack price    
            USCP_Rack_Price__c rackprice = USCP_TestData.createRackPrice(terminal.id, product.id, dt, 100, 1, true);
            
            rackprice = [select id, Price__c, Price_cent_per_gallon__c, Change__c, Change_cent_per_gallon__c from USCP_Rack_Price__c where id =:rackprice.id limit 1];
            System.AssertEquals(100,rackprice.Price__c); 
            System.AssertEquals(100,rackprice.Price_cent_per_gallon__c); 
            System.AssertEquals(1,rackprice.Change__c); 
            System.AssertEquals(1,rackprice.Change_cent_per_gallon__c); 


            
            //now lets create controller
            USCP_PricingListController controller = new USCP_PricingListController ();
            
            controller.PriceDate  = null;            
            //we should have empty list
            System.AssertEquals(0, controller.getTerminals().size());                

            //we should have empty list          
            System.AssertEquals(0, controller.getRackPrices().size()); 


            controller.PriceDate  = dt;

            //we should have valid date        
            System.AssertNotEquals(null, controller.PriceDateStr);  
            
            //we should have 1 record           
            System.AssertEquals(1, controller.getTerminals().size());                

            //we should have 1 record           
            System.AssertEquals(1, controller.getRackPrices().size()); 

            //we should have valid date            
            controller.PriceDateStr = '01/01/2001';
            System.AssertNotEquals(null, controller.PriceDate);              


            System.AssertEquals(null, controller.search()); 
            
            System.AssertNotEquals(null, controller.SaveToExcel()); 
        }            
    }            
}