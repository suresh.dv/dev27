/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO Interface to query HOG_Vent_Gas_Alert__c records for purpose of Trigger handling
                logics.
History:        jschn 24/01/2020 - Created.
*************************************************************************************************/
public interface VG_AlertTriggerDAO {

    List<HOG_Vent_Gas_Alert__c> getAlertsForTriggerByType(Set<Id> ids, Set<String> types);

}