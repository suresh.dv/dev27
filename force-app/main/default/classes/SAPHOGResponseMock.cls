@isTest
global with sharing class SAPHOGResponseMock {

	public class WorkOrdersResponseMock implements WebServiceMock {
	    public void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	        
			SAPHOGWorkOrderServices.CreateWorkOrdersResponse responseElement = new SAPHOGWorkOrderServices.CreateWorkOrdersResponse();
	   		responseElement.Message = 'SAPHOGWorkOrdersResponseMock.CreateWorkOrdersResponse Test Message';
	   		responseElement.type_x = True;

	   		responseElement.WorkOrderResponseList = new SAPHOGWorkOrderServices.CreateWorkOrderResponse[]{};

			SAPHOGWorkOrderServices.CreateWorkOrderResponse item = new SAPHOGWorkOrderServices.CreateWorkOrderResponse();				
			item.WorkOrder = new SAPHOGWorkOrderServices.WorkOrder();
			item.WorkOrder.WorkOrderNumber = '1234567';

			responseElement.WorkOrderResponseList.add(item);

	        response.put('response_x', responseElement);
	    }
	}


	public class UpdateWorkOrderActivitiesResponseMock implements WebServiceMock {
	    public void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	        
			SAPHOGWorkOrderServices.UpdateWorkOrderActivitiesResponse  responseElement = new SAPHOGWorkOrderServices.UpdateWorkOrderActivitiesResponse ();
	   		responseElement.Message = 'SAPHOGWorkOrdersResponseMock.UpdateWorkOrderActivitiesResponse  Test Message';
	   		responseElement.type_x = True;

	   		responseElement.WorkOrderActivityResponseList = new SAPHOGWorkOrderServices.UpdateWorkOrderActivityResponse[]{};

			SAPHOGWorkOrderServices.UpdateWorkOrderActivityResponse item = new SAPHOGWorkOrderServices.UpdateWorkOrderActivityResponse();				
			item.ActivityNumber = '1234567';
			item.Message = 'TEST';			
			item.ReferenceId = '1234567';						

			responseElement.WorkOrderActivityResponseList.add(item);

	        response.put('response_x', responseElement);
	    }
	}

	public class ConfirmWorkOrderActivitiesResponseMock implements WebServiceMock {
	    public void doInvoke(
	           Object stub,
	           Object request,
	           Map<String, Object> response,
	           String endpoint,
	           String soapAction,
	           String requestName,
	           String responseNS,
	           String responseName,
	           String responseType) {
	        
			SAPHOGWorkOrderServices.ConfirmWorkOrderActivitiesResponse responseElement = new SAPHOGWorkOrderServices.ConfirmWorkOrderActivitiesResponse();
	   		responseElement.Message = 'SAPHOGWorkOrdersResponseMock.ConfirmWorkOrderActivitiesResponse Test Message';
	   		responseElement.type_x = True;

	   		responseElement.WorkOrderActivityConfirmResponseList = new SAPHOGWorkOrderServices.ConfirmWorkOrderActivityResponse[]{};

			SAPHOGWorkOrderServices.ConfirmWorkOrderActivityResponse item = new SAPHOGWorkOrderServices.ConfirmWorkOrderActivityResponse();				
			item.ActivityNumber = '1234567';
			item.Message = 'TEST';			
			item.ReferenceId = '1234567';						

			responseElement.WorkOrderActivityConfirmResponseList.add(item);

	        response.put('response_x', responseElement);
	    }
	}

}