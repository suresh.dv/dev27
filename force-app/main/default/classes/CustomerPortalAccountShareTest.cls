/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class CustomerPortalAccountShareTest {

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            // Create a Parent Account with the name 100
            Account account100 = AccountTestData.createAccount('100', Null);
            insert account100;
            System.AssertNotEquals(account100.Id, Null);
            
            // Create a Child Account for the Account 100 with the name 100.1
            Account account101 = AccountTestData.createAccount('100.1', account100.Id);
            insert account101;
            System.AssertNotEquals(account101.Id, Null);
            
            // Create a Child Account for the Account 100 with the name 100.2
            Account account102 = AccountTestData.createAccount('100.2', account100.Id);
            insert account102;
            System.AssertNotEquals(account102.Id, Null);
            
            // Create a Contact for Account 100
            Contact andreAgassi = ContactTestData.createContact(account100.Id, 'Andre', 'Agassi');
            andreAgassi.Title = 'Cluster Owner';
            insert andreAgassi;
            System.AssertNotEquals(andreAgassi.Id, Null);
            
            // Create a Contact for Account 100.1
            Contact peteSamprass = ContactTestData.createContact(account101.Id, 'Mr. Pete', 'Samprass');
            peteSamprass.Title = 'Station Manager';
            insert peteSamprass;
            System.AssertNotEquals(peteSamprass.Id, Null);
            
            // Create a Contact for Account 100.2
            Contact johnSmith = ContactTestData.createContact(account102.Id, 'John', 'Smith');
            johnSmith.Title = 'Station Manager';
            insert johnSmith;
            System.AssertNotEquals(johnSmith.Id, Null);
            
            Test.startTest();
                // Create User for peteSamprass
                System.debug('Create peteSamprass Portal User'); 
                User peteSamprassUser = UserTestData.createTestCustomerPortalUser(peteSamprass.Id);
                System.AssertNotEquals(peteSamprassUser.Id, Null);
                        
                // Create User for andreAgassi
                User andreAgassiUser = UserTestData.createTestCustomerPortalUser(andreAgassi.Id);
                System.AssertNotEquals(andreAgassiUser.Id, Null);
            Test.stopTest();    
            
            peteSamprassUser = [SELECT Id, IsPortalEnabled, IsActive FROM User WHERE Id =:peteSamprassUser.Id][0];
            System.AssertEquals(peteSamprassUser.IsPortalEnabled, True);
            System.AssertEquals(peteSamprassUser.IsActive, True);
            
            andreAgassiUser = [SELECT Id, IsPortalEnabled, IsActive FROM User WHERE Id =: andreAgassiUser.Id];
            System.AssertEquals(andreAgassiUser.IsPortalEnabled, True);
            System.AssertEquals(andreAgassiUser.IsActive, True);
            
            account101 = [SELECT Id, Name, IsCustomerPortal FROM Account WHERE Id =:account101.Id];
            System.AssertEquals(account101.IsCustomerPortal, True);
            
            // Verify that andreAgassi has an AccountShare record for account101
            List<AccountShare> andreAgassiToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account101.Id];
            System.assertNotEquals(andreAgassiToAccount101Share[0].Id, Null);
            
            List<AccountShare> andreAgassiToAccount102Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account102.Id];
            System.assertNotEquals(andreAgassiToAccount102Share[0].Id, Null);
            
            // Verify that peteSamprass does not have access to account100
            List<AccountShare> peteSamprassToAccount100Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:peteSamprassUser.Id AND AccountId =:account100.Id];
            System.assertEquals(peteSamprassToAccount100Share.size(), 0);
            
            // Create a Child Account for the Account 100 with the name 100.4
            Account account104 = AccountTestData.createAccount('100.4', account100.Id);
            insert account104;
            System.AssertNotEquals(account104.Id, Null);
            
            // Verify that andreAgassi has an AccountShare record for account104
            List<AccountShare> andreAgassiToAccount104Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account104.Id];
            System.assertEquals(andreAgassiToAccount104Share.size(), 1); 
            
            // Disable the peteSamprass portal user
            peteSamprassUser = [SELECT Id, AccountId, ContactId, IsActive, IsPortalEnabled FROM User WHERE Id =:peteSamprassUser.Id];  //Added by SFDC
            peteSamprassUser.IsPortalEnabled = False;
            peteSamprassUser.IsActive = False;            
            
            update peteSamprassUser;           
            
            //peteSamprassUser = [SELECT Id, AccountId, ContactId, IsActive, IsPortalEnabled FROM User WHERE Id =:peteSamprassUser.Id]; - comment out by SFDC
            System.assertEquals(peteSamprassUser.IsActive, False);
            System.assertEquals(peteSamprassUser.IsPortalEnabled, False);            
            
            // Verify that andreAgassi still has access to account101
            andreAgassiToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account101.Id];
            System.assertNotEquals(andreAgassiToAccount101Share[0].Id, Null);
            
            // Set account101.IsCustomerPortal to false
            account101.IsCustomerPortal = False;
            update account101;
            
            //account101 = [SELECT Id, Name, IsCustomerPortal FROM Account WHERE Id =:account101.Id];
            System.assertEquals(account101.IsCustomerPortal, False);
            
            // Verify that andreAgassi still have access to account101
            andreAgassiToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account101.Id];
            System.assertEquals(andreAgassiToAccount101Share.size(), 1);
                         
            // Disable the andreAgassiUser portal user
            andreAgassiUser = [SELECT Id, AccountId, ContactId, IsActive, IsPortalEnabled FROM User WHERE Id =:andreAgassiUser.Id];  
            andreAgassiUser.IsPortalEnabled = False;
            andreAgassiUser.IsActive = False;            
            
            System.debug('Disable the andreaAgassi portal User');
            update andreAgassiUser; 
            
            System.assertEquals(andreAgassiUser.IsActive, False);
            System.assertEquals(andreAgassiUser.IsPortalEnabled, False);  
            
            // Verify that andreAgassi no longer have access to account101
            andreAgassiToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account101.Id];
            System.assertEquals(andreAgassiToAccount101Share.size(), 0);
            
            // Create a Child Account for the Account 100 with the name 100.3
            Account account103 = AccountTestData.createAccount('100.3', account100.Id);
            insert account103;
            System.AssertNotEquals(account103.Id, Null);
            
            // Verify that andrewAgassi do not have access to Account103
            List<AccountShare> andreAgassiToAccount103Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account103.Id];
            System.assertEquals(andreAgassiToAccount103Share.size(), 0);
        }        
    }
    
    static testMethod void myUnitTest2() {
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            // Create a Parent Account with the name 100
            Account account100 = AccountTestData.createAccount('100', Null);
            insert account100;
            System.AssertNotEquals(account100.Id, Null);
            
            // Create a Child Account for the Account 100 with the name 100.1
            Account account101 = AccountTestData.createAccount('100.1', account100.Id);
            insert account101;
            System.AssertNotEquals(account101.Id, Null);
            
            // Create a Parent Account with the name 200
            Account account200 = AccountTestData.createAccount('200', Null);
            insert account200;
            System.AssertNotEquals(account200.Id, Null);
            
            // Create a Contact for Account 100
            Contact andreAgassi = ContactTestData.createContact(account100.Id, 'Andre', 'Agassi');
            andreAgassi.Title = 'Cluster Owner';
            insert andreAgassi;
            System.AssertNotEquals(andreAgassi.Id, Null);
            
            // Create a Contact for Account 100.1
            Contact peteSamprass = ContactTestData.createContact(account101.Id, 'Mr. Pete', 'Samprass');
            peteSamprass.Title = 'Station Manager';
            insert peteSamprass;
            System.AssertNotEquals(peteSamprass.Id, Null);
            
            // Create a Contact for Account 200
            Contact johnSmith = ContactTestData.createContact(account200.Id, 'John', 'Smith');
            johnSmith.Title = 'Station Manager';
            insert johnSmith;
            System.AssertNotEquals(johnSmith.Id, Null);
            
            Test.startTest();
                // Create User for peteSamprass
                User peteSamprassUser = UserTestData.createTestCustomerPortalUser(peteSamprass.Id);
                System.AssertNotEquals(peteSamprassUser.Id, Null);
                        
                // Create User for andreAgassi
                User andreAgassiUser = UserTestData.createTestCustomerPortalUser(andreAgassi.Id);
                System.AssertNotEquals(andreAgassiUser.Id, Null);
                
                User johnSmithUser = UserTestData.createTestCustomerPortalUser(johnSmith.Id);
                System.AssertNotEquals(johnSmithUser.Id, Null);
            Test.stopTest();    
            
            peteSamprassUser = [SELECT Id, IsPortalEnabled, IsActive FROM User WHERE Id =:peteSamprassUser.Id][0];
            System.AssertEquals(peteSamprassUser.IsPortalEnabled, True);
            System.AssertEquals(peteSamprassUser.IsActive, True);
            
            andreAgassiUser = [SELECT Id, IsPortalEnabled, IsActive FROM User WHERE Id =: andreAgassiUser.Id];
            System.AssertEquals(andreAgassiUser.IsPortalEnabled, True);
            System.AssertEquals(andreAgassiUser.IsActive, True);
            
            johnSmithUser = [SELECT Id, IsPortalEnabled, IsActive FROM User WHERE Id =: johnSmithUser.Id];
            System.AssertEquals(johnSmithUser.IsPortalEnabled, True);
            System.AssertEquals(johnSmithUser.IsActive, True);
            
            account101 = [SELECT Id, Name, IsCustomerPortal FROM Account WHERE Id =:account101.Id];
            System.AssertEquals(account101.IsCustomerPortal, True);
            
            // Verify that andreAgassi has an AccountShare record for account101
            List<AccountShare> andreAgassiToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account101.Id];
            System.assertNotEquals(andreAgassiToAccount101Share[0].Id, Null);
            
            // Verify that peteSamprass does not have access to account100
            List<AccountShare> peteSamprassToAccount100Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:peteSamprassUser.Id AND AccountId =:account100.Id];
            System.assertEquals(peteSamprassToAccount100Share.size(), 0);
            
            // Verify that johnSmith does not have access to account101
            List<AccountShare> johnSmithToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:johnSmithUser.Id AND AccountId =:account101.Id];
            System.assertEquals(johnSmithToAccount101Share.size(), 0);
            
            // Re-parent account101 from account100 to account200
            account101.ParentId = account200.Id;
            update account101;
            
            // Verify that andreAgassi no longer has an AccountShare record for account101
            andreAgassiToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:andreAgassiUser.Id AND AccountId =:account101.Id];
            System.assertEquals(andreAgassiToAccount101Share.size(), 0);
            
            // Verify that johnSmith does have access to account101
            johnSmithToAccount101Share = [SELECT Id, UserOrGroupId, AccountId, AccountAccessLevel FROM AccountShare WHERE UserOrGroupId =:johnSmithUser.Id AND AccountId =:account101.Id];
            System.assertEquals(johnSmithToAccount101Share.size(), 1);
        }        
    }
}