/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_WorkFlowEngineService class
                TODO this need some more work as mostly VTT_LTNG_WorkFlowEngineService
                is covered by VTT_LTNG_WorkFlowEngineHandlerTest and tests here have been
                written in a hurry so forgive me...
History:        mbrimus 21/11/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_WorkFlowEngineServiceTest {

    @IsTest
    static void getEngineState_NoIdProvided() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.engineDAO = new EngineDaoEmpty();
        result = new VTT_LTNG_WorkFlowEngineService().getEngineState(null);

        Test.stopTest();
    }

    @IsTest
    static void getEngineState_WOAIdProvided_ButNotFound() {
        HOG_CustomResponseImpl result;

        Test.startTest();

        VTT_LTNG_DAOProvider.engineDAO = new EngineDaoEmpty();
        result = new VTT_LTNG_WorkFlowEngineService().getEngineState('someId');

        Test.stopTest();
    }

    @IsTest
    static void getEngineState_NoTradesmanData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        EngineDaoEmpty dao = new EngineDaoEmpty();
        dao.hasActivities = true;
        VTT_LTNG_DAOProvider.engineDAO = dao;
        result = new VTT_LTNG_WorkFlowEngineService().getEngineState('someId');

        Test.stopTest();
    }

    @IsTest
    static void getEngineState_MultipleTradesmanData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        EngineDao dao = new EngineDao();
        dao.hasMultipleTradesman = true;
        VTT_LTNG_DAOProvider.engineDAO = dao;
        result = new VTT_LTNG_WorkFlowEngineService().getEngineState('someId');

        Test.stopTest();
    }

    @IsTest
    static void getEngineState_WithData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        VTT_LTNG_DAOProvider.engineDAO = new EngineDao();
        result = new VTT_LTNG_WorkFlowEngineService().getEngineState('someId');

        Test.stopTest();
    }

    @IsTest
    static void hasActivityChanged() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        VTT_LTNG_DAOProvider.engineDAO = new EngineDao();
        Work_Order_Activity__c ac = new Work_Order_Activity__c(
                LastModifiedById = UserInfo.getUserId(),
                LastModifiedDate = System.today()
        );
        result = new VTT_LTNG_WorkFlowEngineService().hasActivityChanged(ac);

        Test.stopTest();
    }

    @IsTest
    static void canActivityBePutOnHold() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        VTT_LTNG_DAOProvider.engineDAO = new EngineDao();
        Work_Order_Activity__c ac = new Work_Order_Activity__c(
                LastModifiedById = UserInfo.getUserId(),
                LastModifiedDate = System.today()
        );
        result = new VTT_LTNG_WorkFlowEngineService().canActivityBePutOnHold(ac, null);

        Test.stopTest();
    }

    @IsTest
    static void getDataForJobComplete() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        VTT_LTNG_DAOProvider.engineDAO = new EngineDao();
        Work_Order_Activity__c ac = new Work_Order_Activity__c(
                LastModifiedById = UserInfo.getUserId(),
                LastModifiedDate = System.today()
        );
        ac.Maintenance_Work_Order__r = new HOG_Maintenance_Servicing_Form__c();
        result = new VTT_LTNG_WorkFlowEngineService().getDataForJobComplete(ac, new Contact());

        Test.stopTest();
    }

    @IsTest
    static void getPermitHolderData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        VTT_LTNG_DAOProvider.engineDAO = new EngineDao();
        Work_Order_Activity__c ac = new Work_Order_Activity__c(
                LastModifiedById = UserInfo.getUserId(),
                LastModifiedDate = System.today()
        );
        ac.Maintenance_Work_Order__r = new HOG_Maintenance_Servicing_Form__c();
        result = new VTT_LTNG_WorkFlowEngineService().getPermitHolderData(ac, new Contact());

        Test.stopTest();
    }

    @IsTest
    static void getDamage() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        VTT_LTNG_DAOProvider.engineDAO = new EngineDao();
        result = new VTT_LTNG_WorkFlowEngineService().getDamage('someId');

        Test.stopTest();
    }

    @IsTest
    static void handleJobOnHoldEnd() {

        Test.startTest();
        VTT_LTNG_DAOProvider.engineDAO = new EngineDao();
        VTT_LTNG_Request request = new VTT_LTNG_Request();
        request.actionName = System.Label.VTT_LTNG_Job_On_Hold_Action;
        request.currentLatitude = 1;
        request.currentLongitude = 1;
        request.actionComment = 'Some comment';
        request.requestReschedule = false;
        request.isOtherTradesmanStillWorking = false;
        request.activitiesToComplete = new List<VTT_LTNG_AvailableActivity>();
        VTT_LTNG_EngineAction putLogOnHoldAction =
                VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBONHOLD);
        try {
            new VTT_LTNG_WorkFlowEngineService().handleJobOnHoldEnd(request, putLogOnHoldAction, new Work_Order_Activity__c(), new Contact());
        } catch (Exception e) {
            // TODO
        }
        Test.stopTest();
    }

    public class EngineDao implements VTT_LTNG_WorkFlowEngineDAO {
        public Boolean hasMultipleTradesman = false;

        public List<Work_Order_Activity__c> getActivitiesWithAssignment(String woaId) {
            return new List<Work_Order_Activity__c>{
                    (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(),
                            VTT_FieldDefaults.CLASS_NAME)
            };
        }

        public List<Contact> getTradesmanContacts() {
            if (hasMultipleTradesman) {
                return new List<Contact>{
                        (Contact) HOG_SObjectFactory.createSObject(new Contact()),
                        (Contact) HOG_SObjectFactory.createSObject(new Contact())
                };
            } else {
                return new List<Contact>();
            }
        }

        public List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesmanAndActivity(String woaId, String tradesmanId) {
            return VTT_TestDataFactory.createLogEntries(10, false, null);
        }

        public Work_Order_Activity__c getLatestVersionOfActivity(String woaId) {
            return (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(),
                    VTT_FieldDefaults.CLASS_NAME);
        }

        public List<HOG_Work_Execution_Close_Out_Checklist__c> getCloseOutChecklistForToday(Work_Order_Activity__c activity, Contact tradesman) {
            return (List<HOG_Work_Execution_Close_Out_Checklist__c>) HOG_SObjectFactory.createSObjectList(new HOG_Work_Execution_Close_Out_Checklist__c(), 1,
                    VTT_FieldDefaults.CLASS_NAME);
        }

        public List<Contact> getAssignedTradesman(Id workOrderActivityId, Id userId) {
            return new List<Contact>();
        }

        public List<Work_Order_Activity_Assignment__c> getNonRejectedAssignment(Id activityId, Id tradesmanId) {
            return new List<Work_Order_Activity_Assignment__c>();
        }

        public List<Contact> getOtherWorkingTradesman(Work_Order_Activity__c workOrderActivity, List<Contact> assignedTradesmenList, Contact tradesman) {
            return new List<Contact>();
        }

        public List<VTT_LTNG_AvailableActivity> getAvailableActivitiesToComplete(Contact tradesman, Work_Order_Activity__c workOrderActivity) {
            return new List<VTT_LTNG_AvailableActivity>();
        }

        public Double getRunningTallyTime(Work_Order_Activity__c activity, List<Work_Order_Activity_Log__c> activityLogs) {
            return 0.0;
        }

        public List<Work_Order_Activity_Log__c> getActivityLogs(Id activityId, Id userId) {
            return new List<Work_Order_Activity_Log__c>();
        }

        public List<HOG_Part__c> getPartsByCatalogueCode(String catalogueCode) {
            return new List<HOG_Part__c>();
        }

        public List<HOG_Damage__c> getDamageByPartCode(String partCode) {
            return new List<HOG_Damage__c>();
        }

        public List<HOG_Cause__c> getCause() {
            return new List<HOG_Cause__c>();
        }
    }

    public class EngineDaoEmpty implements VTT_LTNG_WorkFlowEngineDAO {
        public Boolean hasActivities = false;

        public List<Work_Order_Activity__c> getActivitiesWithAssignment(String woaId) {
            if (hasActivities) {
                return new List<Work_Order_Activity__c>{
                        (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(),
                                VTT_FieldDefaults.CLASS_NAME)
                };
            } else {
                return new List<Work_Order_Activity__c>();
            }
        }

        public List<Contact> getTradesmanContacts() {
            return new List<Contact>();
        }

        public List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesmanAndActivity(String woaId, String tradesmanId) {
            return null;
        }

        public Work_Order_Activity__c getLatestVersionOfActivity(String woaId) {
            return null;
        }

        public List<HOG_Work_Execution_Close_Out_Checklist__c> getCloseOutChecklistForToday(Work_Order_Activity__c activity, Contact tradesman) {
            return null;
        }

        public List<Contact> getAssignedTradesman(Id workOrderActivityId, Id userId) {
            return null;
        }

        public List<Work_Order_Activity_Assignment__c> getNonRejectedAssignment(Id activityId, Id tradesmanId) {
            return null;
        }

        public List<Contact> getOtherWorkingTradesman(Work_Order_Activity__c workOrderActivity, List<Contact> assignedTradesmenList, Contact tradesman) {
            return null;
        }

        public List<VTT_LTNG_AvailableActivity> getAvailableActivitiesToComplete(Contact tradesman, Work_Order_Activity__c workOrderActivity) {
            return null;
        }

        public Double getRunningTallyTime(Work_Order_Activity__c activity, List<Work_Order_Activity_Log__c> activityLogs) {
            return 0.0;
        }

        public List<Work_Order_Activity_Log__c> getActivityLogs(Id activityId, Id userId) {
            return null;
        }

        public List<HOG_Part__c> getPartsByCatalogueCode(String catalogueCode) {
            return null;
        }

        public List<HOG_Damage__c> getDamageByPartCode(String partCode) {
            return null;
        }

        public List<HOG_Cause__c> getCause() {
            return null;
        }
    }
}