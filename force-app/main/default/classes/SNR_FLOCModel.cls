/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Model class for data passed to View, mostly depending on FLOC type and selected
                values in picklists.
Test Class:     TODO
History:        mbrimus 2020-01-14 - Created.
*************************************************************************************************/
public inherited sharing class SNR_FLOCModel {
    // Well Tracker related parameter - display M3/day wellOilProdM3Day
    @AuraEnabled
    public Boolean wellOilProdM3DayVisible;

    // Well Tracker related parameter - wellDownOn date when well was put down
    @AuraEnabled
    public Boolean wellDownOn;

    // Well Tracker Oil_Prod_m3_day__c
    @AuraEnabled
    public Decimal wellOilProdM3Day;

    // Well Tracker Well_Status__c
    @AuraEnabled
    public String wellTrackerStatus;

    // Well Tracker Control_Centre__c
    @AuraEnabled
    public String controlCentre;

    // Status of Well Location - varies based on FLOC, default is empty string
    @AuraEnabled
    public String wellLocationStatus;

    // Flag that indicates if FLOC is deleted in SAP
    @AuraEnabled
    public Boolean recordIsDLFL;

    // Name field from FLOC
    @AuraEnabled
    public String functionalLocationRecordName;

    // Id field from FLOC
    @AuraEnabled
    public String functionalLocationRecordId;

    // SAP Object Id from FLOC
    @AuraEnabled
    public String functionalLocationObjectId;

    // Well of Facility Type.. Facility_Type__c or Well_Type__c
    @AuraEnabled
    public String functionalLocationType;

    // Indicator if SNR that is being created is for well tracker and well event was selected
    @AuraEnabled
    public Boolean wellEventSelected;

    // Surface_Location__c from Various FLOCs
    @AuraEnabled
    public String functionalLocationSurface;

    // Default is empty string else its Status__c field from various FLOCs
    @AuraEnabled
    public String functionalLocationStatus;

    // Maintenance_Plant__c field from various FLOCs
    @AuraEnabled
    public String maintenancePlanningPlant;

    // Business_Unit__c field from various FLOCs
    @AuraEnabled
    public String operatingBusiness;

    // Operating_District__c field from various FLOCs
    @AuraEnabled
    public String operatingDistrict;

    // Route__c field from various FLOCs
    @AuraEnabled
    public String operatingRoute;

    // Operating_Field_AMU__c field from various FLOCs
    @AuraEnabled
    public String operatingField;

    // Facility__c field from various FLOCs
    @AuraEnabled
    public String operatingFacility;

    // Id of FLOC
    @AuraEnabled
    public String operatingWellEvent;

    // Id of FLOC
    @AuraEnabled
    public String operatingLocation;

    // Id of FLOC
    @AuraEnabled
    public String operatingSystem;

    // Id of FLOC
    @AuraEnabled
    public String operatingSubSystem;

    // Id of FLOC
    @AuraEnabled
    public String operatingFel;

    // Id of FLOC
    @AuraEnabled
    public String operatingYard;

    // Id of FLOC
    @AuraEnabled
    public String operatingEquipment;

    // Default TRUE else in case of well trackers its checks for if supplied welltracker is latest one...
    @AuraEnabled
    public Boolean recentWellTrackerRecord;

    // PVR_AVGVOL_5D_OIL__c from well location
    @AuraEnabled
    public Decimal avgVol5DOil;

    // PVR_AVGVOL_5D_WATER__c from Well location
    @AuraEnabled
    public Decimal avgVol5DWater;

    // PVR_AVGVOL_5D_SAND__c from Well location
    @AuraEnabled
    public Decimal avgVol5DSand;

    // PVR_AVGVOL_5D_GAS__c from well location
    @AuraEnabled
    public Decimal avgVol5DGas;

    // PVR_AVGVOL_5D_COND__c from well location
    @AuraEnabled
    public Decimal avgVol5DCond;

    // PVR_AVGVOL_UPDATED__c from well location
    @AuraEnabled
    public Datetime pvrAvgVolUpdated;

    // PVR_AVGVOL_5D_LAST_MEASURED__c from well location
    @AuraEnabled
    public Datetime pvrAvgVol5DLastMeasured;

    // equipmentRecord[0].Location__r.PVR_AVGVOL_5D_NO_OF_MEASURES__c != null ? equipmentRecord[0].Location__r.PVR_AVGVOL_5D_NO_OF_MEASURES__c: 0
    @AuraEnabled
    public Decimal pvrAvgVol5DNoOfMeasures;

    // Label that is representing FLOC type
    @AuraEnabled
    public String functionalLocationObjectLabel;

    // Flag that renders Production impacted checkbox - it depends on FLOC type see MAPSHOWPRODUCTIONIMPACTED
    @AuraEnabled
    public Boolean showProductionImpacted;

    // Flag that renders Equipment down checkbox - it depends on FLOC type see MAPSHOWEQUIPMENTDOWN
    @AuraEnabled
    public Boolean showEquipmentDown;

    // Flag that renders equipment informations - depends on type of FLOC see MAPSHOWPEQUIPMENTINFO
    @AuraEnabled
    public Boolean showEquipmentInfo;

    // Well tracker information - see MAPSHOWPEQUIPMENTINFO
    @AuraEnabled
    public Boolean showTrackerInfo;

    // Flag that renders PVR data - see MAPSHOWPVRAVGINFO
    @AuraEnabled
    public Boolean showPVRAvgInfo;

    // Value from equipmentRecord[0].Manufacturer__c
    @AuraEnabled
    public String equipmentManufacturer;

    // Value from equipmentRecord[0].Manufacturer_Serial_No__c;
    @AuraEnabled
    public String equipmentManufacturerSerialNo;

    // Value from equipmentRecord[0].Equipment_Category__c
    @AuraEnabled
    public String equipmentCategory;

    // Value from equipment record
    @AuraEnabled
    public String equipmentNumber;

    // Value from equipmentRecord[0].Catalogue_Code__c
    @AuraEnabled
    public String equipmentCatalogueCode;

    // Value taken from HOG_Service_Required__c based. Value taken from Supervised__c field
    @AuraEnabled
    public Boolean serviceSupervised;

    // Value taken from HOG_Service_Required__c based. Value taken from Non_Recurring__c field
    @AuraEnabled
    public Boolean serviceNonRecurring;

    // Value taken from HOG_Service_Required__c based. Value taken from Vendor_Company_Is_Not_Required__c field
    @AuraEnabled
    public Boolean serviceVendorCompanyIsNotRequired;

    // Value taken from HOG_Service_Required__c based. Value taken from HOG_Service_Code_Group__r.Code_Group__c field
    @AuraEnabled
    public String notificationCodeGroup;

    // Value taken from HOG_Service_Required__c based. Value taken from Vendor_Invoicing_Personnel_Is_Required__c field
    @AuraEnabled
    public Boolean vendorInvoicingPersonnelIsRequired;

    // Value taken from HOG_Service_Required__c based. Value taken from Service_Time_Is_Required__c field
    @AuraEnabled
    public Boolean serviceTimeIsRequired;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean generateRigProgramRecord;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean generateWorkOrderRecord;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean generateWorkOrderNumber;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean generateNotificationNumber;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean generateNumbersFromSAP;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean manualInputWorkOrderNumber;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean allowWorkOrderLinking;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public Boolean serviceEventVisible;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public String notificationTypeCode;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public String notificationOrderTypeCode;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public String notificationUserStatusCode;

    // Depends on which Service Type is selected - for community its only Maintenance
    @AuraEnabled
    public String mainWorkCentre;

    public SNR_FLOCModel setOperatingWellEvent(String operatingWellEvent){
        this.operatingWellEvent = operatingWellEvent;
        return this;
    }

    public SNR_FLOCModel setOperatingLocation(String operatingLocation){
        this.operatingLocation = operatingLocation;
        return this;
    }

    public SNR_FLOCModel setOperatingSystem(String operatingSystem){
        this.operatingSystem = operatingSystem;
        return this;
    }

    public SNR_FLOCModel setOperatingSubSystem(String operatingSubSystem){
        this.operatingSubSystem = operatingSubSystem;
        return this;
    }

    public SNR_FLOCModel setOperatingEquipment(String operatingEquipment){
        this.operatingEquipment = operatingEquipment;
        return this;
    }

    public SNR_FLOCModel setOperatingFel(String operatingFel){
        this.operatingFel = operatingFel;
        return this;
    }

    public SNR_FLOCModel setOperatingYard(String operatingYard){
        this.operatingYard = operatingYard;
        return this;
    }

    public SNR_FLOCModel setEquipmentNumber(String equipmentNumber) {
        this.equipmentNumber = equipmentNumber;
        return this;
    }

    public SNR_FLOCModel setWellEventSelected(Boolean wellEventSelected) {
        this.wellEventSelected = wellEventSelected;
        return this;
    }

    public SNR_FLOCModel setGenerateRigProgramRecord(Boolean generateRigProgramRecord) {
        this.generateRigProgramRecord = generateRigProgramRecord;
        return this;
    }

    public SNR_FLOCModel setGenerateWorkOrderRecord(Boolean generateWorkOrderRecord) {
        this.generateWorkOrderRecord = generateWorkOrderRecord;
        return this;
    }

    public SNR_FLOCModel setGenerateWorkOrderNumber(Boolean generateWorkOrderNumber) {
        this.generateWorkOrderNumber = generateWorkOrderNumber;
        return this;
    }

    public SNR_FLOCModel setGenerateNotificationNumber(Boolean generateNotificationNumber) {
        this.generateNotificationNumber = generateNotificationNumber;
        return this;
    }

    public SNR_FLOCModel setGenerateNumbersFromSAP(Boolean generateNumbersFromSAP) {
        this.generateNumbersFromSAP = generateNumbersFromSAP;
        return this;
    }

    public SNR_FLOCModel setManualInputWorkOrderNumber(Boolean manualInputWorkOrderNumber) {
        this.manualInputWorkOrderNumber = manualInputWorkOrderNumber;
        return this;
    }

    public SNR_FLOCModel setAllowWorkOrderLinking(Boolean allowWorkOrderLinking) {
        this.allowWorkOrderLinking = allowWorkOrderLinking;
        return this;
    }

    public SNR_FLOCModel setServiceEventVisible(Boolean serviceEventVisible) {
        this.serviceEventVisible = serviceEventVisible;
        return this;
    }

    public SNR_FLOCModel setNotificationTypeCode(String notificationTypeCode) {
        this.notificationTypeCode = notificationTypeCode;
        return this;
    }

    public SNR_FLOCModel setNotificationOrderTypeCode(String notificationOrderTypeCode) {
        this.notificationOrderTypeCode = notificationOrderTypeCode;
        return this;
    }

    public SNR_FLOCModel setNotificationUserStatusCode(String notificationUserStatusCode) {
        this.notificationUserStatusCode = notificationUserStatusCode;
        return this;
    }

    public SNR_FLOCModel setMainWorkCentre(String mainWorkCentre) {
        this.mainWorkCentre = mainWorkCentre;
        return this;
    }

    public SNR_FLOCModel setServiceSupervised(Boolean serviceSupervised) {
        this.serviceSupervised = serviceSupervised;
        return this;
    }

    public SNR_FLOCModel setServiceNonRecurring(Boolean serviceNonRecurring) {
        this.serviceNonRecurring = serviceNonRecurring;
        return this;
    }

    public SNR_FLOCModel setServiceVendorCompanyIsNotRequired(Boolean serviceVendorCompanyIsNotRequired) {
        this.serviceVendorCompanyIsNotRequired = serviceVendorCompanyIsNotRequired;
        return this;
    }

    public SNR_FLOCModel setNotificationCodeGroup(String notificationCodeGroup) {
        this.notificationCodeGroup = notificationCodeGroup;
        return this;
    }

    public SNR_FLOCModel setVendorInvoicingPersonnelIsRequired(Boolean vendorInvoicingPersonnelIsRequired) {
        this.vendorInvoicingPersonnelIsRequired = vendorInvoicingPersonnelIsRequired;
        return this;
    }

    public SNR_FLOCModel setServiceTimeIsRequired(Boolean serviceTimeIsRequired) {
        this.serviceTimeIsRequired = serviceTimeIsRequired;
        return this;
    }

    public SNR_FLOCModel setEquipmentCatalogueCode(String equipmentCatalogueCode) {
        this.equipmentCatalogueCode = equipmentCatalogueCode;
        return this;
    }

    public SNR_FLOCModel setEquipmentCategory(String equipmentCategory) {
        this.equipmentCategory = equipmentCategory;
        return this;
    }

    public SNR_FLOCModel setEquipmentManufacturerSerialNo(String equipmentManufacturerSerialNo) {
        this.equipmentManufacturerSerialNo = equipmentManufacturerSerialNo;
        return this;
    }

    public SNR_FLOCModel setEquipmentManufacturer(String equipmentManufacturer) {
        this.equipmentManufacturer = equipmentManufacturer;
        return this;
    }

    public SNR_FLOCModel setFunctionalLocationObjectLabel(String functionalLocationObjectLabel) {
        this.functionalLocationObjectLabel = functionalLocationObjectLabel;
        return this;
    }

    public SNR_FLOCModel setShowProductionImpacted(Boolean showProductionImpacted) {
        this.showProductionImpacted = showProductionImpacted;
        return this;
    }

    public SNR_FLOCModel setShowEquipmentDown(Boolean showEquipmentDown) {
        this.showEquipmentDown = showEquipmentDown;
        return this;
    }

    public SNR_FLOCModel setShowEquipmentInfo(Boolean showEquipmentInfo) {
        this.showEquipmentInfo = showEquipmentInfo;
        return this;
    }

    public SNR_FLOCModel setShowTrackerInfo(Boolean showTrackerInfo) {
        this.showTrackerInfo = showTrackerInfo;
        return this;
    }

    public SNR_FLOCModel setShowPVRAvgInfo(Boolean showPVRAvgInfo) {
        this.showPVRAvgInfo = showPVRAvgInfo;
        return this;
    }

    public SNR_FLOCModel setAvgVol5DCond(Decimal avgVol5DCond) {
        this.avgVol5DCond = avgVol5DCond;
        return this;
    }

    public SNR_FLOCModel setPvrAvgVolUpdated(Datetime pvrAvgVolUpdated) {
        this.pvrAvgVolUpdated = pvrAvgVolUpdated;
        return this;
    }

    public SNR_FLOCModel setPvrAvgVol5DLastMeasured(Datetime pvrAvgVol5DLastMeasured) {
        this.pvrAvgVol5DLastMeasured = pvrAvgVol5DLastMeasured;
        return this;
    }

    public SNR_FLOCModel setPvrAvgVol5DNoOfMeasures(Decimal pvrAvgVol5DNoOfMeasures) {
        this.pvrAvgVol5DNoOfMeasures = pvrAvgVol5DNoOfMeasures;
        return this;
    }

    public SNR_FLOCModel setAvgVol5DOil(Decimal avgVol5DOil) {
        this.avgVol5DOil = avgVol5DOil != null ? avgVol5DOil : 0;
        return this;
    }

    public SNR_FLOCModel setAvgVol5DWater(Decimal avgVol5DWater) {
        this.avgVol5DWater = avgVol5DWater != null ? avgVol5DWater : 0;
        return this;
    }

    public SNR_FLOCModel setAvgVol5DSand(Decimal avgVol5DSand) {
        this.avgVol5DSand = avgVol5DSand != null ? avgVol5DSand : 0;
        return this;
    }

    public SNR_FLOCModel setAvgVol5DGas(Decimal avgVol5DGas) {
        this.avgVol5DGas = avgVol5DGas != null ? avgVol5DGas : 0;
        return this;
    }

    public SNR_FLOCModel setRecentWellTrackerRecord(Boolean recentWellTrackerRecord) {
        this.recentWellTrackerRecord = recentWellTrackerRecord;
        return this;
    }

    public SNR_FLOCModel setOperatingFacility(String operatingFacility) {
        this.operatingFacility = operatingFacility;
        return this;
    }

    public SNR_FLOCModel setOperatingField(String operatingField) {
        this.operatingField = operatingField;
        return this;
    }

    public SNR_FLOCModel setOperatingRoute(String operatingRoute) {
        this.operatingRoute = operatingRoute;
        return this;
    }

    public SNR_FLOCModel setOperatingDistrict(String operatingDistrict) {
        this.operatingDistrict = operatingDistrict;
        return this;
    }

    public SNR_FLOCModel setOperatingBusiness(String operatingBusiness) {
        this.operatingBusiness = operatingBusiness;
        return this;
    }

    public SNR_FLOCModel setMaintenancePlanningPlant(String maintenancePlanningPlant) {
        this.maintenancePlanningPlant = maintenancePlanningPlant;
        return this;
    }

    public SNR_FLOCModel setFunctionalLocationStatus(String functionalLocationStatus) {
        this.functionalLocationStatus = functionalLocationStatus;
        return this;
    }

    public SNR_FLOCModel setFunctionalLocationSurface(String functionalLocationSurface) {
        this.functionalLocationSurface = functionalLocationSurface;
        return this;
    }

    public SNR_FLOCModel setFunctionalLocationType(String functionalLocationType) {
        this.functionalLocationType = functionalLocationType;
        return this;
    }

    public SNR_FLOCModel setFunctionalLocationObjectId(String functionalLocationObjectId) {
        this.functionalLocationObjectId = functionalLocationObjectId;
        return this;
    }

    public SNR_FLOCModel setFunctionalLocationRecordId(String functionalLocationRecordId) {
        this.functionalLocationRecordId = functionalLocationRecordId;
        return this;
    }

    public SNR_FLOCModel setFunctionalLocationRecordName(String functionalLocationRecordName) {
        this.functionalLocationRecordName = functionalLocationRecordName;
        return this;
    }

    public SNR_FLOCModel setRecordIsDLFL(Boolean recordIsDLFL) {
        this.recordIsDLFL = recordIsDLFL;
        return this;
    }

    public SNR_FLOCModel setWellLocationStatus(String wellLocationStatus) {
        this.wellLocationStatus = wellLocationStatus;
        return this;
    }

    public SNR_FLOCModel setWellOilProdM3DayVisible(Boolean wellOilProdM3DayVisible) {
        this.wellOilProdM3DayVisible = wellOilProdM3DayVisible;
        return this;
    }

    public SNR_FLOCModel setWellDownOn(Boolean wellDownOn) {
        this.wellDownOn = wellDownOn;
        return this;
    }

    public SNR_FLOCModel setWellOilProdM3Day(Decimal wellOilProdM3Day) {
        this.wellOilProdM3Day = wellOilProdM3Day;
        return this;
    }

    public SNR_FLOCModel setControlCentre(String controlCentre) {
        this.controlCentre = controlCentre;
        return this;
    }

    public SNR_FLOCModel setWellTrackerStatus(Id wellTrackerStatus) {
        this.wellTrackerStatus = wellTrackerStatus;
        return this;
    }
}