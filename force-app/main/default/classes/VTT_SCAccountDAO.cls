/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DOA layer interface for Account SObject queries for VTT Search Criteria purposes
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public interface VTT_SCAccountDAO {

    List<Account> getAccountsWithContactsByRecordType(Id recordTypeId);

}