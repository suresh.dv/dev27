/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_Hierarchy_MasterEquipment {
	@AuraEnabled
	public Equipment__c equipment;

	@AuraEnabled
	public Boolean hasChild = true;
	@AuraEnabled
	public Boolean isMaster;
}