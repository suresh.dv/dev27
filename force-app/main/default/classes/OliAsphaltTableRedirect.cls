public class OliAsphaltTableRedirect {
    
	public OpportunityLineItem OppLineItem {get; set;}
	public ATS_Freight__c atsFreightandOliValidation {get; set;}
    public boolean showLinkButton {get; set;} {showLinkButton = true;}
    public string tableInformationText {get; set;}
    
    //suppliers & competitors at summary table 
    public string ats_Huskysupp1Name {get; set;}
    public string ats_Huskysupp2Name {get; set;}
    public string ats_Huskysupp3Name {get; set;}
    public string ats_Comp1Name {get; set;}
    public string ats_Comp2Name {get; set;}
    public string ats_Comp3Name {get; set;}
    public string ats_Comp4Name {get; set;}    

    public OliAsphaltTableRedirect(ApexPages.StandardController stdController) {
		this.OppLineItem = (OpportunityLineItem)stdController.getRecord();
    
        showPricingTableLink();//new 
    }
    
    public pagereference abcEditDynamic(){
          PageReference pg =  new PageReference('/apex/OliAsphaltTableEditDyn?id=' + OppLineItem.Id);
          pg.setRedirect(true);
          return pg;
    } 
    
    
 // decides wether to show or not show link button to Pricing Table   
    public void showPricingTableLink() {
        
        list<OpportunityLineItem> lstOli = new list<OpportunityLineItem>(); 
        
        lstOli = [select id, cpm_Product_Family__c from OpportunityLineItem where id =: OppLineItem.id];
                  
        try{   
            System.debug('inside showPricingTableLink!');
            system.debug('OppLineItem' + OppLineItem + 'OppLineItem.OpportunityId ' + OppLineItem.OpportunityId + 'OppLineItem.cpm_Product_Family__c' + OppLineItem.cpm_Product_Family__c);
            if (OppLineItem != null && OppLineItem.OpportunityId != null && lstOli[0].cpm_Product_Family__c != null ) { //&& OppLineItem.cpm_Product_Family__c != null, 
                System.debug('inside showPricingTableLink IF!');
                atsFreightandOliValidation = [
                    select ATS_Freight__c, Id, 
                    cpm_HuskySupplier1_ats_pricing__r.Name, cpm_HuskySupplier2_ats_pricing__r.Name, cpm_HuskySupplier3_ats_pricing__r.Name,														//Huskysupp names
                    cpm_Competitor1_ats_pricing__r.Name, cpm_Competitor2_ats_pricing__r.Name, cpm_Competitor3_ats_pricing__r.Name, cpm_Competitor4_ats_pricing__r.Name,		 					//Competitor names
                    cpm_Product_Type__c
                    from ATS_Freight__c
                    where ATS_Freight__c =: OppLineItem.OpportunityId AND cpm_Product_Type__c =: lstOli[0].cpm_Product_Family__c
                    LIMIT 1
                ];
                System.debug('atsFreightandOliValidation value: '+atsFreightandOliValidation);
                
                ats_Huskysupp1Name = atsFreightandOliValidation.cpm_HuskySupplier1_ats_pricing__r.Name;
                ats_Huskysupp2Name = atsFreightandOliValidation.cpm_HuskySupplier2_ats_pricing__r.Name;
                ats_Huskysupp3Name = atsFreightandOliValidation.cpm_HuskySupplier3_ats_pricing__r.Name;
                ats_Comp1Name = atsFreightandOliValidation.cpm_Competitor1_ats_pricing__r.Name;
                ats_Comp2Name = atsFreightandOliValidation.cpm_Competitor2_ats_pricing__r.Name;
                ats_Comp3Name = atsFreightandOliValidation.cpm_Competitor3_ats_pricing__r.Name;
                ats_Comp4Name = atsFreightandOliValidation.cpm_Competitor4_ats_pricing__r.Name;  
                
            }      
        }
        catch(exception e){
            
            System.debug('exception e: ');
            
            System.debug(' boolean true '+atsFreightandOliValidation);
            if(atsFreightandOliValidation==null){
                
                System.debug(showLinkButton);
                showLinkButton = false;
                tableInformationText = 'There is no match between ATS Freight (product type) and Product (family type), table cannot be accessed.';
                
                ats_Huskysupp1Name = 'no ATS Freight match';
                ats_Huskysupp2Name = 'no ATS Freight match';
                ats_Huskysupp3Name = 'no ATS Freight match';
                ats_Comp1Name = 'no ATS Freight match';
                ats_Comp2Name = 'no ATS Freight match';
                ats_Comp3Name = 'no ATS Freight match';
                ats_Comp4Name = 'no ATS Freight match';
                
            }
        }
    }

}