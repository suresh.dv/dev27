/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Service class containing Business Logic for VTT Lightning Calendar
Test Class:     VTT_LTNG_CalendarServiceTest, VTT_LTNG_CalendarServiceTestNP, VTT_LTNG_Calendar_EndpointTest
History:        mbrimus 2019-05-20 - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_CalendarService {
    private static final String CLASS_NAME = String.valueOf(VTT_LTNG_CalendarService.class);

    public HOG_CustomResponseImpl getEntryDataForDate(String selectedDate, String tradesmanId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        Map<String, Object> resultsMap = new Map<String, Object>();
        resultsMap.put('totals', new VTT_TotalsModel());
        resultsMap.put('summary', new List<VTT_WorkOrderSummaryModel>());
        resultsMap.put('woalist', new List<Work_Order_Activity_Log_Entry__c>());

        Date currDate = Date.valueOf(selectedDate);
        System.debug(CLASS_NAME + ' SELECTED DATE ' + currDate);

        List<Work_Order_Activity_Log_Entry__c> entries =
                VTT_LTNG_DAOProvider.calendarDao.getLogEntriesForTradesmanOnDate(tradesmanId, currDate);
        System.debug(' ReloadLogEntries() -> LogEntryList: ' + entries);

        // Counters
        Decimal TotalHours = 0;
        Decimal TotalHoursOffEquipment = 0;
        Decimal TotalHoursOnEquipment = 0;

        List<VTT_WorkOrderSummaryModel> WorkOrderSummaryList = new List<VTT_WorkOrderSummaryModel>();
        Map<String, VTT_WorkOrderSummaryModel> workOrderSummaryMap = new Map<String, VTT_WorkOrderSummaryModel>();

        for (Work_Order_Activity_Log_Entry__c rec : entries) {

            if (rec.Include_in_Calculation__c && rec.Duration_Hours__c <> null) {
                TotalHours = TotalHours + rec.Duration_Hours__c;

                if (rec.OffEquipment__c) {
                    TotalHoursOffEquipment = TotalHoursOffEquipment + rec.Duration_Hours__c;
                }
                if (rec.OnEquipment__c) {
                    TotalHoursOnEquipment = TotalHoursOnEquipment + rec.Duration_Hours__c;
                }
                VTT_WorkOrderSummaryModel workOrderSummaryItem;
                if (workOrderSummaryMap.containsKey(rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c)) {

                    workOrderSummaryItem = workOrderSummaryMap.get(rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c);
                    workOrderSummaryItem.Hours += rec.Duration_Hours__c;
                } else {

                    workOrderSummaryItem = new VTT_WorkOrderSummaryModel();
                    workOrderSummaryItem.WorkOrderName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c;
                    workOrderSummaryItem.WorkOrderId = rec.Work_Order_Activity__r.Maintenance_Work_Order__c;
                    //07-Jun-18 mz:
                    if (rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Well_Event__c != null) {
                        //CAT 9 -> Well Event
                        workOrderSummaryItem.LocationName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Well_Event__r.Name;
                    } else if (rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Yard__c != null) {
                        //CAT 8 - Yard
                        workOrderSummaryItem.LocationName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Yard__r.Name;
                    } else if (rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Functional_Equipment_Level__c != null) {
                        //CAT 7 - FEL
                        workOrderSummaryItem.LocationName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Functional_Equipment_Level__r.Name;
                    } else if (rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Sub_System__c != null) {
                        //CAT 6 - Subsystem
                        workOrderSummaryItem.LocationName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Sub_System__r.Name;
                    } else if (rec.Work_Order_Activity__r.Maintenance_Work_Order__r.System__c != null) {
                        //CAT 5 - System
                        workOrderSummaryItem.LocationName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.System__r.Name;
                    } else if (rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Location__c != null) {
                        //CAT 4 - Location
                        workOrderSummaryItem.LocationName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Location__r.Name;
                    } else if (rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Facility__c != null) {
                        //CAT 3 - Facility
                        workOrderSummaryItem.LocationName = rec.Work_Order_Activity__r.Maintenance_Work_Order__r.Facility__r.Name;
                    }
                    workOrderSummaryItem.Hours = rec.Duration_Hours__c;
                    workOrderSummaryMap.put(workOrderSummaryItem.WorkOrderName, workOrderSummaryItem);
                    WorkOrderSummaryList.add(workOrderSummaryItem);
                }
            }
        }

        WorkOrderSummaryList.sort();

        String TotalHoursText = VTT_Utilities.HoursToString(TotalHours);
        String TotalHoursOnEquipmentText = VTT_Utilities.HoursToString(TotalHoursOnEquipment);
        String TotalHoursOffEquipmentText = VTT_Utilities.HoursToString(TotalHoursOffEquipment);

        resultsMap.put('totals', new VTT_TotalsModel(TotalHoursText, TotalHoursOnEquipmentText, TotalHoursOffEquipmentText));
        resultsMap.put('summary', WorkOrderSummaryList);
        resultsMap.put('woalist', entries);
        response.addResult(resultsMap);
        return response;
    }

    public HOG_CustomResponseImpl getCurrentUserData() {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Map<String, Object> resultsMap = new Map<String, Object>();
        resultsMap.put('isAdminUser', false);
        resultsMap.put('isSupervisor', false);
        resultsMap.put('userData', VTT_LTNG_DAOProvider.calendarDao.getTradesmanInfo());

        Boolean isSuperVisor = false;

        // First check if user has proper permission sets, if he does then return, else check if he is system admin
        Map<Id, PermissionSet> ps =
                VTT_LTNG_DAOProvider.calendarDao.getSupervisorPermissionSets();
        List<PermissionSetAssignment> psalist =
                VTT_LTNG_DAOProvider.calendarDao.getPermissionSetAssignmentsOfCurrentUser(ps.keySet());

        if (psalist.size() > 0) {
            isSuperVisor = true;
        }

        if (isSuperVisor) {
            resultsMap.put('isSupervisor', isSuperVisor);
        } else {
            resultsMap.put('isAdminUser', VTT_LTNG_DAOProvider.calendarDao.isUserSystemAdmin());
        }

        response.addResult(resultsMap);
        return response;
    }

    public HOG_CustomResponseImpl getVendorAccounts() {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        response.addResult(VTT_LTNG_DAOProvider.calendarDao.getVendorAccounts());
        return response;
    }

    public HOG_CustomResponseImpl getTradesmanForAccount(String accountId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        response.addResult(VTT_LTNG_DAOProvider.calendarDao.getTradesmanForAccount(accountId));
        return response;
    }

    public HOG_CustomResponseImpl getOnHoldActivities(String tradesmanId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        response.addResult(VTT_LTNG_DAOProvider.calendarDao.getOnHoldActivities(tradesmanId));
        return response;
    }

    public HOG_CustomResponseImpl getTradesmanEvents(String tradesmanID, Date startDate, Date endDate) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        Set<Id> workedActivitySet = new Set<Id>();
        Set<Id> workLogActivitySet = new Set<Id>();
        Map<Id, Work_Order_Activity_Log__c> workLogMap = new Map<Id, Work_Order_Activity_Log__c>();

        List<VTT_CalendarEventModel> events = new List<VTT_CalendarEventModel>();

        System.debug(CLASS_NAME + ' getTradesmanEvents() -> tradesmanID: ' + tradesmanID);

        // Logs for last 90 days for tradesmen and one entry with each log
        Datetime now = System.now();
        List<Work_Order_Activity_Log__c> workLogList =
                VTT_LTNG_DAOProvider.calendarDao.getActivityLogsForTradesman(tradesmanID, startDate, endDate);
        System.debug(CLASS_NAME + ' workLogList: ' + workLogList);
        System.debug(CLASS_NAME + ' getActivityLogsForTradesman took ' + (System.now().getTime() - now.getTime()));
        //make work log unique -> store & show only latest daily work log
        //15-Oct-18 mz: W-001291
        for (Work_Order_Activity_Log__c workLog : workLogList) {

            if (workLogMap.isEmpty() || !workLogMap.containsKey(workLog.Work_Order_Activity__c)) {
                workLogMap.put(workLog.Work_Order_Activity__c, workLog);
            }
        }

        for (Work_Order_Activity_Log__c workLog : workLogMap.values()) {
            //add activity ID to the set so we can use it later in
            //LoadMyScheduledActivities in order to ignore scheduled activities if  Tradesman already working on it
            buildEvents(workedActivitySet, workLog, workLogActivitySet, events);
        }

        if (workLogActivitySet.size() > 0) {
            //handle Stale Activities only
            now = System.now();
            List<Work_Order_Activity_Log__c> autoCompletedLogs =
                    VTT_LTNG_DAOProvider.calendarDao.getAutoCompletedActivities(workLogActivitySet);
            System.debug(CLASS_NAME + ' autoCompletedLogs: ' + autoCompletedLogs);
            System.debug(CLASS_NAME + ' getAutoCompletedActivities took ' + (System.now().getTime() - now.getTime()));
            for (Work_Order_Activity_Log__c workLog : autoCompletedLogs) {
                addAutoCompletedActitiviesAsEvent(workLog, events);
            }
        }

        // MY SCHEDULED ACTIVITIES
        now = System.now();
        List<Work_Order_Activity__c> scheduledActivitiesList =
                VTT_LTNG_DAOProvider.calendarDao.getScheduledActivities(workedActivitySet, tradesmanID, startDate, endDate);
        System.debug(CLASS_NAME + ' scheduledActivitiesList: ' + scheduledActivitiesList);
        System.debug(CLASS_NAME + ' getScheduledActivities took ' + (System.now().getTime() - now.getTime()));
        for (Work_Order_Activity__c activity : scheduledActivitiesList) {
            buildScheduledEvents(activity, events);
        }

        response.addResult(events);
        return response;
    }

    private void buildScheduledEvents(Work_Order_Activity__c activity, List<VTT_CalendarEventModel> events) {
        Datetime startDT = activity.Scheduled_Start_Date__c;
        Datetime endDT = activity.Scheduled_Finish_Date__c;
        if (endDT == null) {

            endDT = Datetime.now();
        }
        if (startDT <> null && endDT <> null) {

            VTT_CalendarEventModel campEvent = new VTT_CalendarEventModel();
            campEvent.title = activity.Name;
            campEvent.allDay = false;
            campEvent.startString = startDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);
            campEvent.endString = endDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);
// NOT USED IN LIGHTNING
//                campEvent.url = 'VTT_ActivityDetailTimeLog?id=' + activity.Id +
//                        '&retUrl=' + EncodingUtil.urlEncode('/apex/VTT_TradesmanCalendar?VendorID=' +
//                        AccountID + '&TradesmanID=' +
//                        TradesmanID, 'UTF-8');

            if (activity.Status__c == VTT_Utilities.ACTIVITY_STATUS_ONHOLD) {
                campEvent.className = 'event-activitylog-onHold-rescheduled';
            } else {
                campEvent.className = 'event-scheduled';
            }
            campEvent.activityId = activity.Id;
            campEvent.woWONumber = activity.Maintenance_Work_Order__r.Work_Order_Number__c;
            campEvent.woPriorityNumber = activity.Maintenance_Work_Order__r.Work_Order_Priority_Number__c;
            campEvent.amuName = activity.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Name;
            campEvent.equipmentTagNumber = setTagNumber(activity);
            campEvent.woFlocType = '';

            setFlocTypeAndFlocName(activity, campEvent);
            events.add(campEvent);

        }
    }

    private void buildEvents(Set<Id> workedActivitySet, Work_Order_Activity_Log__c workLog, Set<Id> workLogActivitySet, List<VTT_CalendarEventModel> events) {
        workedActivitySet.add(workLog.Work_Order_Activity__c);
        workLogActivitySet.add(workLog.Id);

        Datetime startDT = workLog.Started_New__c;
        Datetime endDT = workLog.Finished_New__c;
        if (workLog.Work_Order_Activity_Log_Entries2__r != null &&
                workLog.Work_Order_Activity_Log_Entries2__r.size() > 0 &&
                workLog.Work_Order_Activity_Log_Entries2__r[0].Status__c == VTT_Utilities.LOGENTRY_JOBONHOLD) {
            endDT = workLog.Work_Order_Activity_Log_Entries2__r[0].TimeStamp__c;
        }

        if (endDT == null) {

            if (startDT.date() == Date.today()) {
                endDT = Datetime.now();
            }
        }

        if (startDT <> null && endDT <> null) {

            VTT_CalendarEventModel campEvent = new VTT_CalendarEventModel();
            campEvent.title = workLog.Work_Order_Activity__r.Name;
            campEvent.allDay = false;
            campEvent.startString = startDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);
            campEvent.endString = endDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);

            if (workLog.Work_Order_Activity__r.Status__c == VTT_Utilities.ACTIVITY_STATUS_COMPLETED) {
                campEvent.className = 'event-activitylog-complete';
            }
            //On Hold Activities -> if Scheduled Start Date is lower or Equal as On Hold Date we will display only activity with On Hold Date on calendar (gray one)
            else if (workLog.Work_Order_Activity_Log_Entries2__r.size() > 0 &&
                    workLog.Work_Order_Activity_Log_Entries2__r[0].Status__c == VTT_Utilities.LOGENTRY_JOBONHOLD &&
                    workLog.Work_Order_Activity__r.Scheduled_Start_Date__c.date() <= endDT) {
                campEvent.startString = endDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);
                campEvent.className = 'event-activitylog-onHold';
            }
            //On Hold Activities -> if Scheduled Start Date is Higher than On Hold Date we will display activity start date
            //and will disaply both On Hold Activity (gray) and On Hold Re-Shheduled activity placeholder (yellow one) on Activity calendar
            else if (workLog.Work_Order_Activity_Log_Entries2__r.size() > 0 &&
                    workLog.Work_Order_Activity_Log_Entries2__r[0].Status__c == VTT_Utilities.LOGENTRY_JOBONHOLD &&
                    workLog.Work_Order_Activity__r.Scheduled_Start_Date__c.date() > endDT) {
                workedActivitySet.remove(workLog.Work_Order_Activity__c);
                campEvent.startString = endDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);
                campEvent.className = 'event-activitylog-onHold';
            } else {
                campEvent.className = 'event-activitylog';
            }
            campEvent.activityId = workLog.Work_Order_Activity__c;
            campEvent.woPriorityNumber = workLog.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Priority_Number__c;
            campEvent.woWONumber = workLog.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c;
            campEvent.woFlocType = '';
            campEvent.amuName = workLog.Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Name;
            campEvent.equipmentTagNumber = setTagNumber(workLog.Work_Order_Activity__r);

            setFlocTypeAndFlocName(workLog.Work_Order_Activity__r, campEvent);
            events.add(campEvent);
        }
    }

    private static String setTagNumber(Work_Order_Activity__c activity) {
        String tag = '';
        if (activity != null) {
            if (activity.Equipment__c != null) {
                tag = activity.Equipment__r.Tag_Number__c;
                return tag;
            }
            if (activity.Maintenance_Work_Order__r.Equipment__c != null) {
                tag = activity.Maintenance_Work_Order__r.Equipment__r.Tag_Number__c;
                return tag;
            }
        }
        return tag;
    }

    @TestVisible
    private void addAutoCompletedActitiviesAsEvent(Work_Order_Activity_Log__c workLog, List<VTT_CalendarEventModel> events) {
        Boolean eventProblem = false;
        Datetime startDT = workLog.Started_New__c;
        Datetime endDT = workLog.Finished_New__c;

        if (endDT == null) {

            if (startDT.date() == Date.today()) {
                endDT = Datetime.now();
            } else if (workLog.Work_Order_Activity_Log_Entries2__r == null ||
                    workLog.Work_Order_Activity_Log_Entries2__r.size() == 0) {
                endDT = startDT + 1 / 24; // add one hour
                eventProblem = true;
            }

        }

        if ((startDT <> null && endDT <> null) && eventProblem) {

            VTT_CalendarEventModel campEvent = new VTT_CalendarEventModel();
            campEvent.title = workLog.Work_Order_Activity__r.Name;
            campEvent.allDay = false;
            campEvent.startString = startDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);
            campEvent.endString = endDT.format(VTT_LTNG_CalendarConstants.DTFORMAT);

            campEvent.className = 'event-activitylog-err';
            campEvent.activityId = workLog.Work_Order_Activity__c;
            campEvent.woPriorityNumber = workLog.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Priority_Number__c;
            campEvent.woWONumber = workLog.Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c;
            campEvent.woFlocType = '';
            campEvent.amuName = workLog.Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Name;
            campEvent.equipmentTagNumber = setTagNumber(workLog.Work_Order_Activity__r);

            setFlocTypeAndFlocName(workLog.Work_Order_Activity__r, campEvent);
            events.add(campEvent);
        }
    }

    @TestVisible
    private void setFlocTypeAndFlocName(Work_Order_Activity__c activity, VTT_CalendarEventModel campEvent) {
        if (activity != null) {
            if (activity.Maintenance_Work_Order__r.Well_Event__c != null) {
                //CAT 9 -> Well Event
                campEvent.woFlocType = Well_Event__c.SObjectType.getDescribe().getLabel();
                campEvent.woFlocName = activity.Maintenance_Work_Order__r.Well_Event__r.Name;
            } else if (activity.Maintenance_Work_Order__r.Yard__c != null) {
                //CAT 8 - Yard
                campEvent.woFlocType = Yard__c.SObjectType.getDescribe().getLabel();
                campEvent.woFlocName = activity.Maintenance_Work_Order__r.Yard__r.Name;
            } else if (activity.Maintenance_Work_Order__r.Functional_Equipment_Level__c != null) {
                //CAT 7 - FEL
                campEvent.woFlocType = Functional_Equipment_Level__c.SObjectType.getDescribe().getLabel();
                campEvent.woFlocName = activity.Maintenance_Work_Order__r.Functional_Equipment_Level__r.Name;
            } else if (activity.Maintenance_Work_Order__r.Sub_System__c != null) {
                //CAT 6 - Subsystem
                campEvent.woFlocType = Sub_System__c.SObjectType.getDescribe().getLabel();
                campEvent.woFlocName = activity.Maintenance_Work_Order__r.Sub_System__r.Name;
            } else if (activity.Maintenance_Work_Order__r.System__c != null) {
                //CAT 5 - System
                campEvent.woFlocType = System__c.SObjectType.getDescribe().getLabel();
                campEvent.woFlocName = activity.Maintenance_Work_Order__r.System__r.Name;
            } else if (activity.Maintenance_Work_Order__r.Location__c != null) {
                //CAT 4 - Location
                campEvent.woFlocType = Location__c.SObjectType.getDescribe().getLabel();
                campEvent.woFlocName = activity.Maintenance_Work_Order__r.Location__r.Name;
            } else if (activity.Maintenance_Work_Order__r.Facility__c != null) {
                //CAT 3 - Facility
                campEvent.woFlocType = Facility__c.SObjectType.getDescribe().getLabel();
                campEvent.woFlocName = activity.Maintenance_Work_Order__r.Facility__r.Name;
            }
        }
    }
}