global class USVC_XRefWS80 {
    
// Version: 8.00
// 
// Date chganged Sep 9, 2019
// Added filter functionality to drop certain products. 
// 
// Date changed June 21, 2019
// Added the logic for the MtrlXLoc and MtrlXLocDtl tables
// 
// Date Changed: June 18, 2019 
// Added condition records for materials. 
// 
// Date Changed: April  17,2019   
// Definition of USVC_T4 Complex response object 
//
//
      global class USVC_T4C {
          
      
          WebService String location;
          WebService String carrier_code;
          WebService String shipper_code;
          WebService String tankage;
          WebService String line;
          WebService String transtype;
          WebService String impexprtType;
          WebService String plantid;
          WebService String scheduler;
          Webservice String assetid;
          Webservice date   transdate;
          Webservice String message;
          Webservice String filtersts;
          WebService List<USVC_Material> material;
          
      
       public USVC_T4C(String location, String carrier_code,  String shipper_code, String tankage, String line, String transtype,
                       String impexprttype,
                       String plantid, String scheduler, String assetid, Date transdate, String message, String filtersts,  List<USVC_Material> material) {
           
            
               this.location = location;           
               this.carrier_code = carrier_code;
               this.shipper_code = shipper_code;
               this.tankage      = tankage;
               this.line         = line;
               this.transtype    = transtype;
               this.impexprttype = impexprttype;
                           
               this.plantid = plantid;
               this.scheduler = scheduler;
               this.assetid = assetid;
   			   this.transdate = transdate;
               this.message = message;
               this.filtersts = filtersts;
               this.material = material;

              
          }
   
   }

     global class USVC_MtrlCondition {  
           WebService String trgt_material_code;
           WebService String trgt_material_name;  
         
         public USVC_MtrlCondition(String trgt_material_code, String trgt_material_name){
             this.trgt_material_code = trgt_material_code;
             this.trgt_material_name = trgt_material_name;
         }
  
     }
    
    
    //  Definition of the USVC_MtrlXLoc class   
    
    global class MtrlLocDtl {  
           WebService String Location_ID;
           WebService String Pipeline_Segment; 
           WebService String Tankage_Code;
           WebService String Import_Export_Type;
           WebService String Asset_Code;
         
         public MtrlLocDtl(String Location_ID, String Pipeline_Segment, String Tankage_Code, String Import_Export_Type,
                             String Asset_Code){
             this.Location_ID = Location_ID;
             this.Pipeline_Segment = Pipeline_Segment;
             this.Tankage_Code = Tankage_Code;
             this.Import_Export_Type = Import_Export_Type;
             this.Asset_Code = Asset_Code;
         }
  
     }



    
//   
// Definition of the USVC_Material Object
//   
    global class USVC_Material {
              WebService String src_material_code;
              WebService String src_material_name;
              WebService STring src_uom;
              WebService String trgt_material_code;
              WebService String trgt_material_name;
              WebService String trgt_uom;
        
    }       
    
   global class USVC_PIC {
          
          WebService String pi_tag;
          WebService String plantid;
          WebService String assetid;
          WebService String uom;

          WebService String meastype;
          Webservice String pi_message;
          
      
       public USVC_PIC(String pi_tag, String plantid,  String assetid, String uom,  String meastype, String pi_message) {
           
            
               this.pi_tag = pi_tag;   
               this.plantid = plantid;
               this.assetid = assetid;
               this.uom = uom;
               this.meastype = meastype;
               this.pi_message = pi_message;             
             
      
          }
   
   }  
    
  
//  
//*********************************************************************************************
// Webservice to provide simple combined and complex USVC_T4 lookup 
// ********************************************************************************************
// //List<USVC_Material> source_material

   webService static USVC_T4C getUSVC_T4C(String source_app, String source_location, String carrier_code, String shipper_code,
                                          String tankage, String line, String transtype, String impexprttype,
                                          Date transdate, List<USVC_Material> source_material) { 
  
        String message;
                                              
        List<USVC_Application__c> appList = [Select i.id, i.Name From USVC_Application__c i];
                                                   
        Map<String, String> appMap = new Map<String, String>(); 
                                             
                                             
   // Load Application  into a map
       integer i = 0;
       for (USVC_Application__c app : appList){ 
          appMap.put(appList[i].Name, appList[i].id);    
          i++;
          }   // end of for loop
                                             
         String sourceAppID = appMap.get(source_app);                                         
                                            
//                                            
 //------     Lookup Locations   -------------------------------------    // 
// 
// 
//      System.Debug('Location Lookup');
//      
        List<USVC_LocationRef__c> nLocationL = [Select  i.Name,  i.Carrier_Code__c, i.Carrier_Name__c, i.Location_ID__c,i.Location_No__c,
                                                i.Tankage__c,
                                   i.Plant_ID__c, i.Scheduler__c, i.Shipper_Code__c, i.USVC_Application__c,i.Application__c                  
                                FROM USVC_LocationRef__c i                                         
                                  WHERE i.USVC_Application__c = :sourceAppID and 
                                        i.Tankage__c = :tankage and
                                        i.Location_ID__c = :source_location];                                 
    IF(nLocationL.isEmpty()) {
                     message  = '**No Matching Plant ID for given Location code: ' + source_location + ' and Tankage ' + tankage;
                     USVC_LocationRef__c nLocrec = new USVC_LocationRef__c();
                     nLocrec.Carrier_Code__c = ' ';
                      nLocrec.Carrier_Name__c = ' ';
                     nLocrec.Shipper_Code__c = ' ';
                     nLocrec.Location_ID__c = ' ';
                     nLocrec.Tankage__c = ' ';
                     nLocrec.Plant_ID__c = ' ';
                     nLocrec.Scheduler__c = ' ';
                     nLocationL.add(nLocrec);
              
            
        }    //   end of   IF(nLocationL.isEmpty()) {                                       
                         
 //
 //------     Lookup Material   -------------------------------------                                    
 //                                                                                                                           

                                             
   List<USVC_Material> trgt_materialList = new List<USVC_Material>();   
   String assetid_tmp;   
   String pipeline_tmp;   
   boolean exclusion = false;
   String filtersts =''; 
                                           
                                             
   for(integer k = 0; K < source_material.size(); k++) {  
       
     List<USVC_MtrlXRef__c> nmtrlxref = [Select i.Name, i.Source_App__c, i.Source_Material_Code__c, i.Source_Mtr_Text__c,
                                    i.Source_Material_Name__c, i.Source_UOM__c,i.Target_App__c,i.Target_Material_Code__c,
                                    i.Target_Material_Name__c, i.Target_UOM__c,i.Target_Mtr_Text__c, i.MtrlLoc_Text__c,
                                    i.Exclusion__c,
                             (Select Location_ID__c,Pipe_LIne_Segment__c,Tankage_Code__c, Import_Export_Type__c, Asset_Code__c, Name FROM USVC_MtrlXRefDtl__r order by Name),
                             (Select Name, Effective_Date_From__c, Effective_Date_To__c,  Target_Material_Code__c, Target_Mtr_Text__c FROM USVC_MtrlXRefCndtn__r order by Name)                 
                                FROM USVC_MtrlXRef__c i                                         
                                  WHERE i.Source_App__c = :sourceAppID and     
                                        i.Source_Material_Code__c  = :source_material[k].src_material_code];
  
        if(!nmtrlxref.isEmpty() && nmtrlxref[0].Exclusion__c == true){
             filtersts = 'F';
              continue;
          }     
       
       
       
              
// Read the USVC_MtrlXLoc and USVC_MtrlXLocDtl tables. 
 	   IF(!nmtrlxref.isEmpty()) {
           List<USVC_MtrlXLoc__c> nmtrlxloc = [Select i.Name,
                             (Select Location_ID__c, Pipeline_Segment__c, Tankage_Code__c, Import_Export_Type__c, Asset_Code__c, Name FROM USVC_MtrlXLocDtls__r order by Name)    
                                FROM USVC_MtrlXLoc__c i                                         
                                  WHERE i.Name = :nmtrlxref[0].MtrlLoc_Text__c];
       
      // Split the MtrlXRefDtl from the MtrlXRef
  
 
//Location and Trankage Records   
               List<USVC_MtrlXRefDtl__c> mtrlxrefdtl = new List<USVC_MtrlXrefDtl__c>();
               List<MtrlLocDtl> nmtrllocdtl = new List<MtrlLocDtl>();
            
 // Material Location Records
 // 
      		   List<USVC_MtrlXLocDtl__c> mtrlxlocdtl = new List<USVC_MtrlXLocDtl__c>(); 
  
         
//             System.Debug('Array Size of MTRLXLOCDTL  ' +    mtrlxlocdtl.size());
 //            
 //            
 // Condition Records           
               List<USVC_MtrlXRefCndtn__c> mtrlxrefcndtn = new List<USVC_MtrlXRefCndtn__c>();
            
               i = 0;
               k = 0;

         
      for(USVC_MtrlXRef__c mtrlx : nmtrlxref){
             mtrlxrefdtl = nmtrlxref[i].USVC_MtrlXRefDtl__r;
             mtrlxrefcndtn = nmtrlxref[i].USVC_MtrlXRefCndtn__r;
                    
             k = 0;
          

                   
            IF(!mtrlxrefdtl.isEmpty()){
               	 if(transtype != 'S'){ 
                      for(USVC_MtrlXRefDtl__c mtrlxdtl : mtrlxrefdtl){
   
                          
                          
                        if(mtrlxrefdtl[k].Location_ID__c == source_location && mtrlxrefdtl[k].Tankage_Code__c == tankage
                          && mtrlxrefdtl[k].Pipe_Line_Segment__c == Null){
                                 
                 			MtrlLocDtl mtrllocdtl = new MtrlLocDtl(mtrlxrefdtl[k].Location_ID__c, 
                                                    mtrlxrefdtl[k].Pipe_Line_Segment__c,
                                                    mtrlxrefdtl[k].Tankage_Code__c,
                                      				mtrlxrefdtl[k].Import_Export_Type__c,
                 									mtrlxrefdtl[k].Asset_Code__c);
          
                         nmtrllocdtl.add(mtrllocdtl);
                          }  
          
                      k++;               
                   }
          }else {
              for(USVC_MtrlXRefDtl__c mtrlxdtl : mtrlxrefdtl){
                 
                                if(mtrlxrefdtl[k].Location_ID__c == source_location && mtrlxrefdtl[k].Tankage_Code__c == tankage &&
                                  (mtrlxrefdtl[k].Pipe_LIne_Segment__c == line ||
                                   mtrlxrefdtl[k].Pipe_LIne_Segment__c == null ||
                                   mtrlxrefdtl[k].Pipe_LIne_Segment__c == '')){
            
 									MtrlLocDtl mtrllocdtl = new MtrlLocDtl(mtrlxrefdtl[k].Location_ID__c, 
                                                    mtrlxrefdtl[k].Pipe_Line_Segment__c,
                                                    mtrlxrefdtl[k].Tankage_Code__c,
                                      				mtrlxrefdtl[k].Import_Export_Type__c,
                 									mtrlxrefdtl[k].Asset_Code__c);     
                                      
   		           
                          nmtrllocdtl.add(mtrllocdtl);
                          }     //end of if
                  
           
                      k++;               
                   }   //end of for loop
                        
                    } //end of else
          }
                 i++;  //Increment USVC_MtrlXREf 
               
             }    //    for(USVC_MtrlXRef__c mtrlx : nmtrlxref){
   
           
           
          IF(nmtrllocdtl.isEmpty()) {
                  
              
   // Browse over mtrlxlocdtl to find the right combination. 
                if(!nmtrlxloc.isEmpty()){
                  mtrlxlocdtl = nmtrlxloc[0].USVC_MtrlXLocDtls__r;
 //                 System.Debug('$$$Array Size of nmtrllocdtl ' +    nmtrllocdtl.size());  
                  nmtrllocdtl = getAssetID(mtrlxlocdtl, transtype, source_location, tankage, line);            
                  }


      //        System.Debug('Array Size of NMTRLLOCDTL  ' +    nmtrllocdtl.size());                
     //         System.Debug('$$$Asset ID:  ' +    nmtrllocdtl[0].Asset_Code);
     //         System.Debug('$$$Pipeline Segment:  ' +    nmtrllocdtl[0].Pipeline_Segment);     
      
              } //End of       IF(nmtrllocdtl.isEmpty())
  
       
        USVC_Material usvc_matrl = new USVC_Material();
       
        usvc_matrl.src_material_code = nmtrlxref[0].Source_Material_Code__c;
        usvc_matrl.src_material_name = nmtrlxref[0].Source_Mtr_Text__c;
        usvc_matrl.src_uom = nmtrlxref[0].Source_UOM__c;
     
   
                  
     // Determine the right target material based on a date condition record  
     // 

           
        if(!mtrlxrefcndtn.isEmpty()){
       			 USVC_MtrlCondition mtrlcnd = getMaterial(transdate,mtrlxrefcndtn,nmtrlxref[0].Target_Material_Code__c,
                                                    nmtrlxref[0].Target_Mtr_Text__c);
            
                 usvc_matrl.trgt_material_code = mtrlcnd.trgt_material_code;
                 usvc_matrl.trgt_material_name = mtrlcnd.trgt_material_name;
           }else{
                 usvc_matrl.trgt_material_code = nmtrlxref[0].Target_Material_Code__c;
                 usvc_matrl.trgt_material_name = nmtrlxref[0].Target_Mtr_Text__c;
               
           }     
                 
         usvc_matrl.trgt_uom = nmtrlxref[0].Target_UOM__c;   
          
                  
 //              
 //              
         IF(nmtrllocdtl.isEmpty()) {
                     message  = '**No Matching Asset ID for given Location code: ' + source_location + ' and Tankage ' + tankage;
                     assetid_tmp =  ' ';  
                     pipeline_tmp = ' ';
                 } else {
                     assetid_tmp = nmtrllocdtl[0].Asset_Code;  
                	 pipeline_tmp = nmtrllocdtl[0].PipeLine_Segment;
      				 }
           

           
           trgt_materialList.add(usvc_matrl);
   
         
	     }    //  End of If -->     IF(!nmtrlxref.isEmpty()) {   
          else {
           message = 'No Material found for ' + source_material[k].src_material_code;
   
        }    
               
     }   //  for(integer k = 0; K < source_material.size(); k++) {    
               
 //    System.Debug('Plant ID ' +    nLocationL[0].Plant_ID__c );                                                        
       System.Debug('++++Asset ID ' +    assetid_tmp);
 //    System.Debug('Spiral Material   ' +    trgt_materialList[0].trgt_material_code);                                            
  //    
  // Build the return object
  //               
                                              
          USVC_T4C returnT4C = new USVC_T4C(nLocationL[0].Location_ID__c,                                  
                                        nLocationL[0].Carrier_Code__c,  
                                         nLocationL[0].Shipper_Code__c,  
                                         nLocationL[0].Tankage__c, 
                                         pipeline_tmp,
                                         transtype,
                                         impexprttype,
                                         nLocationL[0].Plant_ID__c,
                                         nLocationL[0].Scheduler__c,
                                         assetid_tmp,
                                         transdate,      
                                         message,
                                         filtersts,
                                         trgt_materialList);                                             
  
                                             
     Return returnT4C;    
  } // End of Function
    
//*************************************************************************************   
//    
// Method to find the find  the Asset ID
//   
//*************************************************************************************     


    private static  List<MtrlLocDtl> getAssetID(List<USVC_MtrlXLocDtl__c> mtrlxlocdtl,
                                        String transtype, String source_location, String tankage, String line ){
     		
            integer k =0;
                 System.Debug('&&mtrlxlocdtl size  ' +    mtrlxlocdtl.size());
                                                  
            List<MtrlLocDtl> nmtrllocdtl = new List<MtrlLocDtl>();
    
                                            
  	     if(transtype != 'S'){ 
              for(USVC_MtrlXLocDtl__c mtrlxloc : mtrlxlocdtl){
                 
                        if(mtrlxlocdtl[k].Location_ID__c == source_location && mtrlxlocdtl[k].Tankage_Code__c == tankage
                          && mtrlxlocdtl[k].Pipeline_Segment__c == Null){
                         
                         System.Debug('&&Source Location  ' +    source_location);
                         System.Debug('&&Tankage  ' +    tankage);
                         System.Debug('&&Line  ' +    line);
      
 							MtrlLocDtl mtrllocdtl = new MtrlLocDtl(mtrlxlocdtl[k].Location_ID__c, 
                                                    mtrlxlocdtl[k].Pipeline_Segment__c,
                                                    mtrlxlocdtl[k].Tankage_Code__c,
                                      				mtrlxlocdtl[k].Import_Export_Type__c,
                 									mtrlxlocdtl[k].Asset_Code__c);     
                                      
                
                          nmtrllocdtl.add(mtrllocdtl);
                             }
    
                      k++;               
                   }                          
            }else {
                   for(USVC_MtrlXLocDtl__c mtrlxloc : mtrlxlocdtl){            
                                if(mtrlxlocdtl[k].Location_ID__c == source_location && mtrlxlocdtl[k].Tankage_Code__c == tankage &&
                                  (mtrlxlocdtl[k].Pipeline_Segment__c == line ||
                                  mtrlxlocdtl[k].Pipeline_Segment__c == null ||
                                  mtrlxlocdtl[k].Pipeline_Segment__c == '' )){
                                      
      
 							MtrlLocDtl mtrllocdtl = new MtrlLocDtl(mtrlxlocdtl[k].Location_ID__c, 
                                                    mtrlxlocdtl[k].Pipeline_Segment__c,
                                                    mtrlxlocdtl[k].Tankage_Code__c,
                                      				mtrlxlocdtl[k].Import_Export_Type__c,
                 									mtrlxlocdtl[k].Asset_Code__c);              
                                      
                        
            
                           nmtrllocdtl.add(mtrllocdtl);
                          }   

                      k++;               
                   }   
                        
                }       
     // System.Debug('---Asset Code:  ' +    nmtrllocdtl[0].Asset_Code);
     // System.Debug('---Pipeline Segment:  ' +    nmtrllocdtl[0].Pipeline_Segment);  
                                            
     return  nmtrllocdtl;
    }    
    
//*************************************************************************************   
//    
// Method to find the material code from the condition record. 
//   
//*************************************************************************************
//  
    private static USVC_MtrlCondition getMaterial(Date compdate, List<USVC_MtrlXRefCndtn__c> mtrlxrefcndtn,
                                                 String materialcode, String Materialtext){
     	
            boolean found = false;                                       
            String mtrlcode = ''; 
            integer k = 0;
        
      	 USVC_MtrlCondition mtrlcondition = new USVC_MtrlCondition('','');
         
        if(!mtrlxrefcndtn.isEmpty()){
            
            for(USVC_MtrlXRefCndtn__c mcndtn : mtrlxrefcndtn){ 
              if(compdate >= mtrlxrefcndtn[k].Effective_Date_From__c && 
                 compdate <= mtrlxrefcndtn[k].Effective_Date_To__c){
                          mtrlcondition.trgt_material_code = mtrlxrefcndtn[k].Target_Material_Code__c; 
                          mtrlcondition.trgt_material_name = mtrlxrefcndtn[k].Target_Mtr_Text__c; 
                          found = true;
                     	  break;
                 }
            k++;
            }
            If(!found){
              mtrlcondition.trgt_material_code = materialcode; 
              mtrlcondition.trgt_material_name = materialtext; 
      		  }
        }else {
            mtrlcondition.trgt_material_code = materialcode; 
            mtrlcondition.trgt_material_name = materialtext; 
        }
        
     return mtrlcondition;
    } 
//  
//*********************************************************************************************
// Webservice to get plant id and asset id for PI Tags
// ********************************************************************************************
//    


 
   webService static USVC_PIC getUSVC_PIC(String pi_tag, String plantid, String assetid, 
                                          String meastype){   
   
                                              
              String message_pi;
                                              
              System.Debug('PI TAG : ' + pi_tag);
                                              
                                 
            List<USVC_PI_Tag__c> nusvc_piL = [Select  i.Name, i.PI_Tag__c, i.Plant_ID__c, i.Asset_ID__c,  i.Unit_of_Measure__c,
                                   i.Measurement_Type__c                
                                FROM USVC_PI_Tag__c i                                         
                                  WHERE i.PI_Tag__c = :pi_tag];
   
             USVC_PIC returnPIC;
                                              
                System.Debug('Table Size: ' + nusvc_piL.size());

                                              
                                              
              IF(nusvc_piL.size() == 0)  {
                  message_pi  = '**PI Tag Not Found ' + pi_tag;
                   
                     returnPIC = new USVC_PIC(pi_tag,  
                                            '','','','',message_pi); 
              
                   }
                  else {
                      message_pi = '';
                       returnPIC = new USVC_PIC(nusvc_piL[0].PI_Tag__c,  
                               nusvc_piL[0].Plant_ID__c, 
                               nusvc_piL[0].Asset_ID__c,  
                               nusvc_piL[0].Unit_of_Measure__c,  
                               nusvc_piL[0].Measurement_Type__c,                                     
                               message_pi); 
             
                  }                  

              return returnPIC;
                
     }

}