/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 7/17/2019   
 */

@IsTest
public class ELG_HandoverForm_EndpointTest {

	/*Positive tests*/

	@IsTest
	static void handoverFormTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		ELG_Shift_Handover__c createdHandover = [
				SELECT Id,
						Shift_Assignement__c,
						Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
						Sr_Supervisor__c,
						Steam_Chief__c,
						Shift_Engineer__c,
						Shift_Lead__c
				FROM ELG_Shift_Handover__c
		];

		Test.startTest();
		System.runAs(usr) {

			result = ELG_HandoverForm_Endpoint.loadHandoverFromDatabase(createdHandover.Id, null);

			System.assertNotEquals(null, result, 'No value returned from Query');
			/*System.assertEquals(createdHandover.Id, handover.Id, 'Id of queried Handover is not the same as passed parameter');*/
		}
		Test.stopTest();
	}

	@IsTest
	static void preparedLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		List<ELG_Handover_Category_Question__c> questions = ELG_TestDataFactory.createQuestions('Test', 2, true);
		Id recordTypeId = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];

		Test.startTest();
		System.runAs(usr) {

			result = ELG_HandoverForm_Endpoint.loadCategoryAndQuestions('Test Cat');
			List<ELG_Log_Entry__c> listOfEntries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);

			System.assertNotEquals(null, result.resultObjects, 'No value returned from Query');
			System.assertEquals(2, listOfEntries.size(), 'Number of Log Entries is different');

			System.assertEquals(questions[0].Question__c, listOfEntries[0].Question__c, 'Question is different form Log Entry questions');
			System.assertEquals(questions[0].Id, listOfEntries[0].Handover_Category_Question__c, 'Id is different form Log Entry questions');
			System.assertEquals(recordTypeId, listOfEntries[0].RecordTypeId, 'Record Type Id is different form Log Entry questions');

			System.assertEquals(questions[1].Question__c, listOfEntries[1].Question__c, 'Question is different form Log Entry questions');
			System.assertEquals(questions[1].Id, listOfEntries[1].Handover_Category_Question__c, 'Id is different form Log Entry questions');
			System.assertEquals(recordTypeId, listOfEntries[1].RecordTypeId, 'Record Type Id is different form Log Entry questions');
		}
		Test.stopTest();
	}

	@IsTest
	static void insertLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_TestDataFactory.createQuestions('Test', 5, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];

		Test.startTest();
		System.runAs(usr) {

			result = ELG_HandoverForm_Endpoint.loadCategoryAndQuestions('Test Cat');
			List<ELG_Log_Entry__c> listOfEntries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);
			for (ELG_Log_Entry__c entry : listOfEntries) {
				entry.User_Log_Entry__c = 'Something';
			}

			System.assertNotEquals(null, result.resultObjects);
			result = ELG_HandoverForm_Endpoint.logEntriesFromComponent(listOfEntries, shift);
			List<ELG_Log_Entry__c> createdEntries = [
					SELECT Shift_Assignement__c, Post__c, Operating_Field_AMU__c
					FROM ELG_Log_Entry__c
			];

			System.assertEquals(5, createdEntries.size());
			for (ELG_Log_Entry__c entry : createdEntries) {
				System.assertEquals(shift.Id, entry.Shift_Assignement__c, 'Created entry do not match Shift Id');
				System.assertEquals(shift.Post__c, entry.Post__c, 'Created entry do not match Post Id');
				System.assertEquals(shift.Post__r.Operating_Field_AMU__c, entry.Operating_Field_AMU__c, 'Created entry do not match AMU Id');
			}

		}
		Test.stopTest();
	}

	@IsTest
	static void loadGenericLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		Field__c amuField = [SELECT Id FROM Field__c LIMIT 1];
		ELG_TestDataFactory.createLogEntries(5, amuField, post, shift, true);
		Id recordTypeId = Schema.SObjectType.ELG_Log_Entry__c.getRecordTypeInfosByDeveloperName()
				.get(ELG_Constants.GENERIC_LOG_ENTRY_RECORDTYPE).getRecordTypeId();
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];

		Test.startTest();
		System.runAs(usr) {

			result = ELG_HandoverForm_Endpoint.loadGenericLogEntries(shift.Id);
			List<ELG_Log_Entry__c> listOfEntries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);

			System.assertNotEquals(null, result.resultObjects, 'No value returned from Query');
			System.assertEquals(5, listOfEntries.size(), 'Created logs were 5, query should return 5');

			for (ELG_Log_Entry__c entry : listOfEntries) {
				System.assertEquals(recordTypeId, entry.RecordTypeId, 'Record type do not match Generic Id of Log Entry');
			}
		}
		Test.stopTest();
	}

	@IsTest
	static void getAllHandoverLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		List<ELG_Handover_Category_Question__c> questions = ELG_TestDataFactory.createQuestions('Test', 5, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id, Shift_Assignement__c FROM ELG_Shift_Handover__c];
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];

		Test.startTest();
		System.runAs(usr) {

			result = ELG_HandoverForm_Endpoint.loadCategoryAndQuestions('Test Cat');
			List<ELG_Log_Entry__c> listOfEntries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);
			for (ELG_Log_Entry__c entry : listOfEntries) {
				entry.User_Log_Entry__c = 'Something';
			}

			System.assertNotEquals(null, result.resultObjects, 'No value returned from Query');
			ELG_HandoverForm_Endpoint.logEntriesFromComponent(listOfEntries, shift);
			result = ELG_HandoverForm_Endpoint.loadHandoverFromDatabase(createdHandover.Id, null);

			Integer counter = 0;
			for (ELG_Handover_Category_Question__c question : questions) {
				System.assertEquals(counter, question.Order__c, 'Returned value not in ASC order');
				counter++;
			}

			counter = 0;
			List<ELG_HandoverForm_Service.HandoverQuestionCategoryWrapper> wrappers = (List<ELG_HandoverForm_Service.HandoverQuestionCategoryWrapper>) result.resultObjects[1];
			for (ELG_HandoverForm_Service.HandoverQuestionCategoryWrapper categories : wrappers) {
				System.assertEquals('Test Cat', categories.questionCategory.Name, 'Category name do not match');
				for (ELG_HandoverForm_Service.HandoverQuestionLogEntriesWrapper logEntries : categories.questionsWithLogEntries) {
					System.assertEquals(false, logEntries.Log_Entries.isEmpty(), 'No Log Entries has returned');
					for (ELG_Log_Entry__c entry : logEntries.Log_Entries) {
						System.assertEquals(entry.Handover_Category_Question__r.Order__c, questions[counter].Order__c, 'Order of question in not matching');
						System.assertEquals(questions[counter].Id, entry.Handover_Category_Question__c, 'Id of question is not the same as Questions Id');
						System.assertEquals(questions[counter].Question__c, entry.Question__c, 'Question is not the same as Custom object Question');
						counter++;
					}
				}
			}

		}
		Test.stopTest();
	}

	@IsTest
	static void createdNewShiftAndHandoverTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id, Shift_Assignement__c FROM ELG_Shift_Handover__c];
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		shift.Primary__c = usr.Id;
		shift.Shift_Takeover_Reason__c = 'I am Batman';
		update shift;

		Test.startTest();
		System.runAs(usr) {

			requestResult = ELG_HandoverForm_Endpoint.updateHandoverAndStatus(createdHandover.Id);
			System.assertEquals(true, requestResult.success, 'Should be successful response');

			ELG_Shift_Handover__c newHandover = [
					SELECT Id, Shift_Status__c
					FROM ELG_Shift_Handover__c
					WHERE Shift_Status__c = :ELG_Constants.SHIFT_NEW
					LIMIT 1
			];

			System.assertNotEquals(null, newHandover, 'Query returned no Handover. Should be one. (Process Builder)');
			System.assertEquals(ELG_Constants.SHIFT_NEW, newHandover.Shift_Status__c, 'Status is not New (Process Builder)');

			ELG_Shift_Assignement__c newShift = [
					SELECT Id, Primary__c
					FROM ELG_Shift_Assignement__c
					WHERE Shift_Status__c = :ELG_Constants.SHIFT_NEW
					LIMIT 1
			];
			System.assertNotEquals(null, newShift, 'Query returned no Shift. Should be one. (Process Builder)');
			System.assertEquals(null, newShift.Primary__c, 'Primary is not null');

		}
		Test.stopTest();
	}

	@IsTest
	static void acceptHandoverTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id, Shift_Assignement__c FROM ELG_Shift_Handover__c];
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		shift.Primary__c = usr.Id;
		shift.Shift_Takeover_Reason__c = 'I am Batman';
		update shift;

		Test.startTest();
		System.runAs(usr) {

			requestResult = ELG_HandoverForm_Endpoint.updateHandoverAndStatus(createdHandover.Id);
			System.assertEquals(true, requestResult.success, 'Should be successful response');
			createdHandover = [
					SELECT Id, Status__c, Incoming_Operator__c
					FROM ELG_Shift_Handover__c
					WHERE Status__c = :ELG_Constants.HANDOVER_IN_PROGRESS
					LIMIT 1
			];
			requestResult = ELG_HandoverForm_Endpoint.acceptHandover(createdHandover, usr.Id, null);
			System.assertEquals(true, requestResult.success, 'Should be successful response');

			ELG_Shift_Handover__c anotherHo = [
					SELECT Id,
							Status__c,
							Incoming_Operator__c,
							Signed_In__c
					FROM ELG_Shift_Handover__c
					WHERE Status__c = :ELG_Constants.HANDOVER_ACCEPTED
					AND Signed_In__c != NULL
					LIMIT 1
			];
			System.assertNotEquals(null, anotherHo.Signed_In__c, 'Signed in does not have value');
			System.assertEquals(ELG_Constants.HANDOVER_ACCEPTED, anotherHo.Status__c, 'Status is not Completed');
		}
		Test.stopTest();
	}

	@IsTest
	static void updateHandoverReviewTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [
				SELECT Id,
						Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
						Sr_Supervisor__c,
						Steam_Chief__c,
						Shift_Engineer__c,
						Shift_Lead__c
				FROM ELG_Shift_Handover__c
		];
		User usr = [SELECT Id FROM User WHERE Alias = 'BabaJaga'];
		List<String> sectionsForUpdate = new List<String>{
				'supervisorReview', 'steamChiefReview', 'shiftLeadReview', 'shiftEngineer'
		};

		Test.startTest();
		System.runAs(usr) {

			requestResult = ELG_HandoverForm_Endpoint.updateHandoverReview(createdHandover, sectionsForUpdate, usr.Id);
			System.assertEquals(true, requestResult.success, 'Should be successful response');
			ELG_Shift_Handover__c queriedHandover = [
					SELECT Id,
							Sr_Supervisor__c,
							Steam_Chief__c,
							Shift_Lead__c,
							Shift_Engineer__c,
							Reviewed__c
					FROM ELG_Shift_Handover__c
			];


			System.assertEquals(true, queriedHandover.Reviewed__c, 'Formula evaluated to false, should be true');
			System.assertEquals(usr.Id, queriedHandover.Sr_Supervisor__c, 'Review failed on Supervisor');
			System.assertEquals(usr.Id, queriedHandover.Steam_Chief__c, 'Review failed on Steam Chief');
			System.assertEquals(usr.Id, queriedHandover.Shift_Lead__c, 'Review failed on Shift Lead');
			System.assertEquals(usr.Id, queriedHandover.Shift_Engineer__c, 'Review failed on Shift Engineer');
		}
		Test.stopTest();
	}

	/*Negative tests*/

	@IsTest
	static void getEmptyHandoverFormTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);

		Test.startTest();
		result = ELG_HandoverForm_Endpoint.loadHandoverFromDatabase(shift.Id, null);
		System.assertEquals(null, result.resultObjects, 'Returned a value, should be null');
		Test.stopTest();
	}

	@IsTest
	static void preparedLogEntriesErrorMsgTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		HOG_RequestResult requestResult;
		ELG_TestDataFactory.createQuestions('Test', 2, true);
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Wrong_Handover_Category_Name
		};

		Test.startTest();
		result = ELG_HandoverForm_Endpoint.loadCategoryAndQuestions(null);
		requestResult = result;

		System.assertEquals(null, result.resultObjects, 'Returned a value, should be null');
		System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		Test.stopTest();
	}

	@IsTest
	static void insertEmptyLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_TestDataFactory.createQuestions('Test', 2, true);

		Test.startTest();
		result = ELG_HandoverForm_Endpoint.loadCategoryAndQuestions('Test Cat');
		List<ELG_Log_Entry__c> listOfEntries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);
		System.assertNotEquals(null, result.resultObjects, 'Should be successful response');

		result = ELG_HandoverForm_Endpoint.logEntriesFromComponent(listOfEntries, shift);
		requestResult = result;
		List<String> errorMsg = new List<String>{
				System.Label.ELG_No_Questions_Answered
		};

		List<ELG_Log_Entry__c> createdEntries = [SELECT Id FROM ELG_Log_Entry__c];
		System.assertEquals(0, createdEntries.size(), 'Returned Entries, should be none');
		System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		Test.stopTest();
	}

	@IsTest
	static void loadEmptyGenericLogEntriesTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);

		Test.startTest();
		result = ELG_HandoverForm_Endpoint.loadGenericLogEntries(shift.Id);
		List<ELG_Log_Entry__c> listOfEntries = (List<ELG_Log_Entry__c>) result.resultObjects.get(0);
		System.assertNotEquals(null, result.resultObjects, 'Result should be successful');
		System.assertEquals(true, listOfEntries.isEmpty(), 'Query returned value, should be none');
		Test.stopTest();
	}

	/*@IsTest
	static void failToStartHandoverProcessTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id FROM ELG_Shift_Handover__c];
		User primary = [SELECT Id FROM User WHERE Alias = 'theOne'];
		User wrongUser = [SELECT Id FROM User WHERE Alias = 'BabaJaga'];
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Invalid_Incoming_Operator,
				System.Label.ELG_Only_Primary_Can_Start_Handover,
				System.Label.ELG_Handover_Cannot_Be_Updated
		};

		shift.Primary__c = primary.Id;
		shift.Shift_Takeover_Reason__c = 'I am Batman';
		update shift;

		Test.startTest();
		System.runAs(primary) {

			requestResult = ELG_HandoverForm_Endpoint.updateHandoverAndStatus(createdHandover.Id, null, primary.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');

			requestResult = ELG_HandoverForm_Endpoint.updateHandoverAndStatus(createdHandover.Id, primary.Id, wrongUser.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[1], requestResult.errors[0], 'Wrong Error Message');

			createdHandover.Status__c = ELG_Constants.HANDOVER_ACCEPTED;
			update createdHandover;
			requestResult = ELG_HandoverForm_Endpoint.updateHandoverAndStatus(createdHandover.Id, primary.Id, wrongUser.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[2], requestResult.errors[0], 'Wrong Error Message');
		}
		Test.stopTest();
	}*/

	/*@IsTest
	static void failToStartHandoverProcessOnWrongPermissionsSetTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id, Shift_Assignement__c FROM ELG_Shift_Handover__c];
		User primary = [SELECT Id FROM User WHERE Alias = 'theOne'];
		User wrongUser = [SELECT Id FROM User WHERE Alias = 'BabaJaga'];
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Wrong_Permission_Set
		};

		shift.Primary__c = primary.Id;
		shift.Shift_Takeover_Reason__c = 'I am Batman';
		update shift;

		Test.startTest();
		System.runAs(wrongUser) {
			requestResult = ELG_HandoverForm_Endpoint.updateHandoverAndStatus(createdHandover.Id, primary.Id, wrongUser.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		}
		Test.stopTest();
	}*/

	@IsTest
	static void acceptHandoverFailOnWrongPermissionSetTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id FROM ELG_Shift_Handover__c];
		User wrongUser = [SELECT Id FROM User WHERE Alias = 'BabaJaga'];
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Wrong_Permission_Set
		};

		Test.startTest();
		System.runAs(wrongUser) {
			requestResult = ELG_HandoverForm_Endpoint.acceptHandover(createdHandover, wrongUser.Id, null);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		}
		Test.stopTest();
	}

	@IsTest
	static void updateReviewFailOnWrongPermissionSetTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id FROM ELG_Shift_Handover__c];
		User wrongUser = [SELECT Id FROM User WHERE Alias = 'theOne'];
		List<String> sectionsForUpdate = new List<String>{
				'supervisorReview', 'steamChiefReview', 'shiftLeadReview', 'shiftEngineer'
		};
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Wrong_Permission_Set
		};

		Test.startTest();
		System.runAs(wrongUser) {
			requestResult = ELG_HandoverForm_Endpoint.updateHandoverReview(createdHandover, sectionsForUpdate, wrongUser.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		}
		Test.stopTest();
	}

	@IsTest
	static void updateReviewFailOnAlreadyReviewedTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [
				SELECT Id,
						Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
						Sr_Supervisor__c,
						Steam_Chief__c,
						Shift_Engineer__c,
						Shift_Lead__c
				FROM ELG_Shift_Handover__c
		];
		User usr = [SELECT Id FROM User WHERE Alias = 'BabaJaga'];
		List<String> sectionsForUpdate = new List<String>{
				'supervisorReview', 'steamChiefReview', 'shiftLeadReview', 'shiftEngineer'
		};
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Handover_Cannot_Be_Updated
		};

		Test.startTest();
		System.runAs(usr) {
			requestResult = ELG_HandoverForm_Endpoint.updateHandoverReview(createdHandover, sectionsForUpdate, usr.Id);
			System.assertEquals(true, requestResult.success, 'Should be successful response');

			createdHandover = [
					SELECT Id,
							Reviewed__c,
							Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
							Sr_Supervisor__c,
							Steam_Chief__c,
							Shift_Engineer__c,
							Shift_Lead__c
					FROM ELG_Shift_Handover__c
			];
			requestResult = ELG_HandoverForm_Endpoint.updateHandoverReview(createdHandover, sectionsForUpdate, usr.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		}
		Test.stopTest();
	}

	@IsTest
	static void updateReviewFailOnEmptySectionsTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [
				SELECT Id, Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c,
						Sr_Supervisor__c,
						Steam_Chief__c,
						Shift_Engineer__c,
						Shift_Lead__c
				FROM ELG_Shift_Handover__c
		];
		User usr = [SELECT Id FROM User WHERE Alias = 'BabaJaga'];
		List<String> sectionsForUpdate = new List<String>();
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Empty_Review_Selection
		};

		Test.startTest();
		System.runAs(usr) {
			requestResult = ELG_HandoverForm_Endpoint.updateHandoverReview(createdHandover, sectionsForUpdate, usr.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		}
		Test.stopTest();
	}

	@IsTest
	static void updateShiftEngineerReviewFailOnWrongPermissionSetTest() {
		HOG_RequestResult requestResult;
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Handover__c createdHandover = [SELECT Id FROM ELG_Shift_Handover__c];
		User wrongUser = [SELECT Id FROM User WHERE Alias = 'theOne'];
		List<String> errorMsg = new List<String>{
				System.Label.ELG_Wrong_Permission_Set
		};

		Test.startTest();
		System.runAs(wrongUser) {
			requestResult = ELG_HandoverForm_Endpoint.updateHandoverReviewByShiftEngineer(createdHandover, wrongUser.Id);
			System.assertEquals(false, requestResult.success, 'Successful response, should be false');
			System.assertEquals(errorMsg[0], requestResult.errors[0], 'Wrong Error Message');
		}
		Test.stopTest();
	}

	/*Test Data*/
	@TestSetup
	static void testData() {
		ELG_TestDataFactory.createReviewerAndLogEntryUsers();
	}
}