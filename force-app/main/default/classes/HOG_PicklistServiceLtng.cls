/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Test Class: 	HOG_PicklistServiceLtngTest
Description:    HOG Service class for lightning containing endpoints for picklist purposes.
History:        jschn 07.10.2018 - Created.
**************************************************************************************************/
public class HOG_PicklistServiceLtng {
    
	private static final String CLASS_NAME = 'HOG_PicklistServiceLtng';

    /**
	 * Retrieves list of picklist items based on Object API Name and Field API Name.
	 * Values in list are wrapped inside PicklistItem wrapper.
	 * @param  objectName which contains desired picklist field
	 * @param  fieldName  for which you want to get picklist options
	 * @return            List of picklist options
	 */
    @AuraEnabled
	public static List<PicklistItem> getPicklist(String objectName, String fieldName) {
		System.debug(CLASS_NAME + '-> : objectName: ' + objectName);
		System.debug(CLASS_NAME + '-> : fieldName: ' + fieldName);
		List<PicklistItem> picklist = new List<PicklistItem>();
		try {
			List<SelectOption> options = HOG_GeneralUtilities.getPicklistvalues(objectName, fieldName);

			for (SelectOption option : options) {
				picklist.add(new PicklistItem(option));
			}
		} catch (Exception ex) {
			System.debug(CLASS_NAME + '-> getPicklist error: ' + ex.getMessage());
		}

		System.debug(CLASS_NAME + '-> List<PicklistItem>: ' + picklist);
		return picklist;
	}

	/**
	 * Wrapper for picklist values. 
	 */
	public class PicklistItem {
		@AuraEnabled public String value {get;set;}
		@AuraEnabled public String label {get;set;}

		PicklistItem(SelectOption option) {
			value = option.getValue();
			label = option.getLabel();
		}
	}
    
}