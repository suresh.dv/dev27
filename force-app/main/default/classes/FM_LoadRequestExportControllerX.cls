/*----------------------------------------------------------------------------------------------------
Author     : Miroslav Zelina
Company    : Husky Energy
Description: A controller to Export Load Requests
             Used for FM_LoadRequestExportCsv and FM_LoadRequestExportXls Pages
Test Class : FM_LoadRequestExportControllerXTest
History    : 05.17.17 mz Initial Revision 
             05.19.17 mz added fileType recognition
             05.19.17 mz added CRLF at the end of the lines and handling of double quotes 
------------------------------------------------------------------------------------------------------*/
public class FM_LoadRequestExportControllerX {

    public string header{get;set;}
    public List<loadRequestWrapper> loadRequestWrapperList {get; set;}
    public class loadRequestWrapper{
        
        public string loadRequest{get; set;}
        public string runSheet{get; set;}
        public string carrier {get; set;}
        public string loadType{get; set;}
        public string product{get; set;}
        public string shift {get; set;}
        public string comments {get; set;}
        
    }
   
    public string Filetype{get;set;}
    public string isExcel {get;set;}
    public string isCsv {get;set;}   
   
 
 
    //* CONSTRUCTOR *//
    public FM_LoadRequestExportControllerX(){
        
            Filetype = (System.currentPageReference().getParameters().get('isCsv') == 'true') ? 'csv' : 'excel';
            loadRequestWrapperList = new List<loadRequestWrapper>();
            header = (Filetype == 'csv') ? '"Load Request","Run Sheet","Carrier","Load Type","Product","Shift","Comments"' + '\r' : ''; //Define the Header for csv, for excel it's defined on VF Page itself - both have to be the same
    }
 
   
    //* Export Method *// - action on both VF Pages
    public void exportLoadRequests(){
          //Query for Load Request - this most probably will be dynamic Query based on parameters choosed on Export VF Page which we don't know yet
          string queryString = 'SELECT Name, Run_Sheet_Lookup__r.source_system_id__c, Carrier__r.Carrier_Name__c, Load_Type__c, Product__c, Shift__c, Standing_Comments__c FROM FM_Load_Request__c ORDER BY NAME ASC'; 
          List<FM_Load_Request__c> loadRequestList = DataBase.Query(queryString);
          
          //Populate the LR Wrapper List which is iterating on csv / xls VF Pages which generates the files
          if(loadRequestList.size()>0){
              
              for(FM_Load_Request__c lr :loadRequestList){
                  
                  loadRequestWrapper lrw = new loadRequestWrapper();
                  
                  if (Filetype == 'excel') {
                     
                     lrw.loadRequest = lr.Name;
                     lrw.runSheet = lr.Run_Sheet_Lookup__r.source_system_id__c;
                     lrw.carrier = lr.Carrier__r.Carrier_Name__c;
                     lrw.loadType = lr.Load_Type__c;
                     lrw.product = lr.Product__c;
                     lrw.shift = lr.Shift__c;
                     lrw.comments = lr.Standing_Comments__c;  
                  }
                  
                  if (Filetype == 'csv') {
                  
                     lrw.loadRequest = convertToCsvFormat(lr.Name);
                     lrw.runSheet = convertToCsvFormat(lr.Run_Sheet_Lookup__r.source_system_id__c);
                     lrw.carrier = convertToCsvFormat(lr.Carrier__r.Carrier_Name__c);
                     lrw.loadType = convertToCsvFormat(lr.Load_Type__c);
                     lrw.product = convertToCsvFormat(lr.Product__c);
                     lrw.shift = convertToCsvFormat(lr.Shift__c);
                     lrw.comments = convertToCsvFormat(lr.Standing_Comments__c) + '\r';  //adding CR at the end of the line  LF is already there) so it will be CRLF as required
                  }
                  
                  loadRequestWrapperList.add(lrw);  
              }
              
              loadRequestWrapperList[loadRequestWrapperList.Size() - 1].comments  = loadRequestWrapperList[loadRequestWrapperList.Size() - 1].comments + '\n'; //adding LF at the end of the last line (CR is already there) so it will be CRLF as required
          }
    }
    
    public String xlsHeader{
        get{
            String strHeader = '';
            strHeader += '<?xml version="1.0"?>';
            strHeader += '<?mso-application progid="Excel.Sheet"?>';
            return strHeader;
        }
    }
    
    
    public String convertToCsvFormat(string s){
        
        if(s <> null) {
        
            if (s.contains('"')) {
            
                s = s.escapeCsv();  //enclosing of double quotes if needed
            }
            
            
            else{
            
                s = '"' + s + '"'; //adding double quotes if there is not any double quote in string
            }
        }   
        
        else {
            
            s = '""'; //adding double quotes if there is no value for field
        }
        
            return s;
    }
}