/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector for SObject Work_Order_Activity_Search_Criteria__c
Test Class:     VTT_WOA_SearchCriteriaSelectorTest
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_SearchCriteriaSelector implements VTT_WOA_SearchCriteriaDAO {

    /**
     * Queries all Work_Order_Activity_Search_Criteria__c that User with provided UserId has created.
     *
     * @param userId
     *
     * @return List<Work_Order_Activity_Search_Criteria__c>
     */
    public List<Work_Order_Activity_Search_Criteria__c> getUserActivitySearchFilters(Id userId) {
        return [
                SELECT Id, Name, Assigned_Name__c, Filter_String__c, User__c, Last_Search_Criteria_Selected__c
                FROM Work_Order_Activity_Search_Criteria__c
                WHERE User__c =: userId
        ];
    }

}