/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Damage__c
-------------------------------------------------------------------------------------------------*/
@isTest 
public class HOGCauseTestData
{
    public static HOG_Cause__c createHOGCause
    (
    	String name,    	
    	String cause_Code, 
    	String cause_Description,
    	String code_Group,
    	Boolean active
    )
    {                
        HOG_Cause__c results = new HOG_Cause__c
            (           
                Name = name,
                Cause_Code__c = cause_Code,
                Cause_Description__c = cause_Description,
                Code_Group__c = code_Group,
                Active__c = active
            ); 

        return results;
    }
}