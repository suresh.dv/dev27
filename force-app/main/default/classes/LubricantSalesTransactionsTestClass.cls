/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LubricantSalesTransactionsTestClass {

    public static User createLubricantSalesUser(String userName, String roleName) {
        Profile p = [SELECT Id FROM Profile WHERE Name='Lubricant User'];
        UserRole role = [select Id from UserRole where DeveloperName =: roleName];
        User u = new User(
            Alias = 'standt', 
            Email='salesrep@retail.com', 
            EmailEncodingKey='UTF-8', 
            LastName='Retail User', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName= userName,
            UserRoleId = role.Id);
        insert u;
        return u;
    }

    public static User createSystemAdminUser(String userName) {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u = new User(
            Alias = 'standt', 
            Email='salesrep@retail.com', 
            EmailEncodingKey='UTF-8', 
            LastName='Retail User', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName= userName);
        insert u;
        return u;
    }
     
    public static User create110AccountOwner(String userName)
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='Service Account'];
        User u = new User(
            Alias = 'aaaaa', 
            Email='retail.dataloader@retail.com', 
            EmailEncodingKey='UTF-8', 
            LastName='DataLoader', 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', 
            UserName= userName);
        
        insert u;
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Retail_Info_Data_Loader'];
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = u.Id;
        psa.PermissionSetId = ps.Id;
        insert psa;
        return u;
    }
    
    @isTest
    static void testFLGoalPage() {
        
        User salesRep = createLubricantSalesUser('salesrep@retail.com', 'Commercial_Sales_Representative');
        //insert salesRep;
        
        User acctOwner = create110AccountOwner('account110@retail.com');
        
        User systemAdmin = createSystemAdminUser('admin@retail.com');
        //insert systemAdmin;
        
        Account act;
        
        System.runAs(acctOwner)
        {
            act = new Account(name='test account', SMS_Number__c = '111111', OwnerId = salesRep.Id);
            RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and DeveloperName = 'X110_Accounts'];
            act.RecordTypeId = RecType.Id;
            insert act;
        }

        System.runAs(systemAdmin)
        {
            Lubricant_Product_Segment__c segmentObj = new Lubricant_Product_Segment__c(Name = 'Antifreeze W/Washer', Reference_Id__c = 'TEST');
            insert segmentObj;
            List<Lubricant_Product_Segment__c> segmentList = [SELECT Id FROM Lubricant_Product_Segment__c LIMIT 1];
            
            List<Lubricant_Sales_Transaction_By_Quarter__c> transList = new List<Lubricant_Sales_Transaction_By_Quarter__c>();
            for(integer i= 1; i < 12; i=i+3)
            {
                Lubricant_Sales_Transaction_By_Quarter__c trans = new Lubricant_Sales_Transaction_By_Quarter__c();
                trans.Account__c = act.Id;
                trans.Sales_Rep__c = salesRep.Id;
                trans.OwnerId = salesRep.Id;
                trans.Product_Sector__c = 'Lube';
                trans.Product_Segment__c = segmentList[0].Id;
                trans.Volume_Actual_L__c = 100 * i;
                trans.Volume_Goal_L__c = 200 * i;
                trans.Gross_Profit_Actual__c = 200 * i;
                trans.Gross_Profit_Goal__c = 300 * i;
                trans.SOW_Volume_Actual_L__c = 400 * i;
                trans.SOW_Volume_Goal_L__c = 500 * i ;
                trans.SOW_Gross_Profit_Actual__c = 600 * i;
                trans.SOW_Gross_Profit_Goal__c = 700 * i;
                trans.Latest_Transaction_Date__c = date.newInstance(System.Today().year(),i,1);
                transList.add(trans);
            }
            insert transList;
        }

        System.runAs(salesRep)
        {
            
            Test.startTest();
            //test LubricantSalesTransactionGoalEdit page
            Test.setCurrentPage(new PageReference('LubricantSalesTransactionGoalEdit'));
            LubricantSalesTransactionsGoalCtrl ctrl = new LubricantSalesTransactionsGoalCtrl();

            ApexPages.StandardSetController standardSetCtrl = ctrl.standardSetCtrl;
            System.assertNotEquals(null, standardSetCtrl);
            
            //Set Search Criteria
            ctrl.selectedYear = String.valueOf(System.Today().year());
            ctrl.view = salesRep.Id; 
            ctrl.resetSegmentandAccount();
            ctrl.selectedSector = 'Lube';
               
            List<SelectOption> segments = ctrl.getProductSegmentList();
            // The segment list contains also the "--- All ---" that's why the size is 2 even though we added only one.            
            System.assertEquals(2, segments.size());
            
            List<SelectOption> accounts = ctrl.getAccountList();
            // The account list contains also the "--- All ---" that's why the size is 2 even though we added only one.  
            System.assertEquals(2, accounts.size());
            
            standardSetCtrl = ctrl.standardSetCtrl;
            System.assertEquals(4, standardSetCtrl.getResultSize());
            
            List<Lubricant_Sales_Transaction_By_Quarter__c> resultPerPage = ctrl.getResults();
            System.assertEquals(4, resultPerPage.size());
            ctrl.last();
            ctrl.first();
            ctrl.next();
            ctrl.previous();
            ctrl.SortField = 'Product_Segment__c';
            ctrl.SortDirection = 'desc';
            ctrl.SortToggle();
            
            //add search filter and Runsearch
            ctrl.selectedSector = 'Lube';
            ctrl.selectedSegment = segments[1].getValue();
            ctrl.selectedQuarter = 'Q1';
            ctrl.refreshData();
            System.assertEquals(1, ctrl.standardSetCtrl.getResultSize());
            ctrl.Save();
            ctrl.Cancel();
            
            Test.stopTest();
        }
        
    }

    static testMethod void testLubricantSalesCopyTransactionToNextYearBatchJob()
    {
        User salesRep = createLubricantSalesUser('salesrep@retail.com', 'Commercial_Sales_Representative');
        //insert salesRep;

        User systemAdmin = createSystemAdminUser('admin@retail.com');
        //insert systemAdmin;
        
        System.runAs(systemAdmin)
        {
            Test.startTest();
            Account act = new Account(name='test account', SMS_Number__c = '111111');
                insert act;
            
            Lubricant_Product_Segment__c segmentObj = new Lubricant_Product_Segment__c(Name = 'Antifreeze W/Washer', Reference_Id__c = 'TEST');
            insert segmentObj;
            List<Lubricant_Product_Segment__c> segmentList = [SELECT Id FROM Lubricant_Product_Segment__c LIMIT 1];
    
            List<Lubricant_Sales_Transaction_By_Quarter__c> transList = new List<Lubricant_Sales_Transaction_By_Quarter__c>();
            for(integer i= 1; i < 12; i=i+3)
            {
                Lubricant_Sales_Transaction_By_Quarter__c trans = new Lubricant_Sales_Transaction_By_Quarter__c();
                trans.Account__c = act.Id;
                trans.Sales_Rep__c = salesRep.Id;
                trans.Product_Sector__c = 'Lube';
                trans.Product_Segment__c = segmentList[0].Id;
                trans.Volume_Actual_L__c = 100 * i;
                trans.Volume_Goal_L__c = 200 * i;
                trans.Gross_Profit_Actual__c = 200 * i;
                trans.Gross_Profit_Goal__c = 300 * i;
                trans.SOW_Volume_Actual_L__c = 400 * i;
                trans.SOW_Volume_Goal_L__c = 500 * i ;
                trans.SOW_Gross_Profit_Actual__c = 600 * i;
                trans.SOW_Gross_Profit_Goal__c = 700 * i;
                trans.Latest_Transaction_Date__c = date.newInstance(2014,i,1);
                transList.add(trans);
            }
            insert transList;
            Database.executeBatch(new LubricantSalesCopyTransactionBatch());
            Test.stopTest();
        }
    }

    static testMethod void testTriggers() 
    {
        User salesRep = createLubricantSalesUser('salesrep@retail.com', 'Commercial_Sales_Representative');
        //insert salesRep;
        
        User systemAdmin = createSystemAdminUser('admin@retail.com');
        //insert systemAdmin;
        
        System.runAs(systemAdmin)
        {
            Test.startTest();
            Account act = TestUtilities.createTestAccount();
                insert act;
            
            Lubricant_Product_Segment__c segmentObj = new Lubricant_Product_Segment__c(Name = 'Antifreeze W/Washer', Reference_Id__c = 'TEST');
            insert segmentObj;
            List<Lubricant_Product_Segment__c> segmentList = [SELECT Id FROM Lubricant_Product_Segment__c LIMIT 1];
    
            Lubricant_Sales_Transaction_By_Quarter__c trans = new Lubricant_Sales_Transaction_By_Quarter__c();
            trans.Account__c = act.Id;
            trans.Sales_Rep__c = salesRep.Id;
            trans.Product_Sector__c = 'Lube';
            trans.Product_Segment__c = segmentList[0].Id;
            trans.Volume_Actual_L__c = 100;
            trans.Volume_Goal_L__c = 200;
            trans.Gross_Profit_Actual__c = 200;
            trans.Gross_Profit_Goal__c = 300;
            trans.SOW_Volume_Actual_L__c = 400;
            trans.SOW_Volume_Goal_L__c = 500;
            trans.SOW_Gross_Profit_Actual__c = 600;
            trans.SOW_Gross_Profit_Goal__c = 700;
            trans.Latest_Transaction_Date__c = date.newInstance(2015,1,1);
            insert trans;
    
            Lubricant_Sales_Transaction_By_Quarter__c duplicateTrans = new Lubricant_Sales_Transaction_By_Quarter__c();
            duplicateTrans.Account__c = act.Id;
            duplicateTrans.Sales_Rep__c = salesRep.Id;
            duplicateTrans.Product_Sector__c = 'Lube';
            duplicateTrans.Product_Segment__c = segmentList[0].Id;
            duplicateTrans.Volume_Goal_L__c = 500;
            duplicateTrans.Gross_Profit_Goal__c = 30000;
            duplicateTrans.Latest_Transaction_Date__c = date.newInstance(2015,1,1);
            insert duplicateTrans;
        
            //Test
            Lubricant_Sales_Transaction_By_Quarter__c updatedTrans = [Select Id, Volume_Goal_L__c, Gross_Profit_Goal__c
                                                                      From Lubricant_Sales_Transaction_By_Quarter__c
                                                                      Where Id =: trans.Id];
            System.assertEquals(updatedTrans.Volume_Goal_L__c, duplicateTrans.Volume_Goal_L__c);
            System.assertEquals(updatedTrans.Gross_Profit_Goal__c, duplicateTrans.Gross_Profit_Goal__c);
            Test.stopTest();
        }
    }
 
}