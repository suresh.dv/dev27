/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 3/6/2020   
 */

public class ELG_ShiftSummary_Endpoint {

	@AuraEnabled
	public static ELG_Shift_Summary__c getShiftSummaries(String shiftId) {
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c();

		try {
			summary = ELG_ShiftSummary_Service.getShiftSummaryOrCreateNew(shiftId);
		} catch (Exception ex) {
			throw new HOG_Exception(ex.getMessage());
		}

		return summary;
	}

	@AuraEnabled
	public static void saveShiftSummary(ELG_Shift_Summary__c shiftSummary) {
		try {
			update shiftSummary;
		} catch (Exception ex) {
			throw new HOG_Exception(ex.getMessage());
		}
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl loadShiftHandovers(List<String> shiftIds) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			response.addResult(ELG_ShiftSummary_Selector.getShiftHandovers(shiftIds));
		} catch (Exception ex) {
			response.addError(ex);
		}
		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl loadSummaryBasedOnShift(String shiftId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			ELG_Shift_Summary__c summary = ELG_ShiftSummary_Selector.getShiftSummaryBasedOnShift(shiftId)[0];
			response.addResult(summary);

			// handover is queried to get Header Information for the component
			ELG_Shift_Handover__c handover = ELG_ShiftSummary_Selector.getHandoverForm(shiftId);
			response.addResult(handover);

		} catch (Exception ex) {
			response.addError(ex);
		}
		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl loadSummaryBasedOnRecordId(String recordId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		try {
			ELG_Shift_Summary__c summary = ELG_ShiftSummary_Selector.getShiftSummaryBasedOnRecordId(recordId);
			response.addResult(summary);

			ELG_Shift_Handover__c handover = ELG_ShiftSummary_Selector.getHandoverForm(summary.Shift_Assignement__c);
			response.addResult(handover);

		} catch (Exception ex) {
			response.addError(ex);
		}
		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl signOffFromSummaryAndUpdateHandover(String shiftId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Shift_Handover__c handover = ELG_ShiftSummary_Selector.getHandoverForm(shiftId);

		SavePoint sp = Database.setSavepoint();
		try {
			response.addResult(ELG_ShiftSummary_Service.signOffShiftSummary(shiftId));
			response.addResult(ELG_HandoverForm_Endpoint.updateHandoverAndStatus(handover.Id));
		} catch (Exception ex) {
			Database.rollback(sp);
			response.addError(ex);
		}

		return response;
	}

	@AuraEnabled
	public static HOG_CustomResponseImpl takeNewShiftAndUpdateSummary(String shiftId) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		Id currentUser = UserInfo.getUserId();
		ELG_Shift_Handover__c handover = ELG_ShiftSummary_Selector.getHandoverForm(shiftId);

		SavePoint sp = Database.setSavepoint();
		try {
			response.addResult(ELG_ShiftSummary_Service.acceptSummary(shiftId, currentUser));
			response.addResult(ELG_HandoverForm_Endpoint.acceptHandover(handover, UserInfo.getUserId(), null));
		} catch (Exception ex) {
			Database.rollback(sp);
			response.addError(ex);
		}

		return response;
	}

}