/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint Handler for Search Criteria VTT List View component
Test Class:     VTT_WOA_SearchCriteria_HandlerTest
                VTT_WOA_SearchCriteria_HandlerTestNP
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_SearchCriteria_Handler {

    /**
     * This method builds class containing filter options and flags required to render front end correctly.
     * First it will validate if param is present.
     * Then based on current user it will check if user is Admin or Vendor Supervisor.
     * It also loads Contact information for the users and builds Options class.
     *
     * @param userId
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options getFilterFormOptions(Id userId) {
        validateUserIdParam(userId);

        VTT_TradesmanFlags tradesmanFlags = new VTT_Utility_EndpointHandler().getTradesmanInfo();

        return new VTT_WOA_SearchCriteria_Options()
                .setActivityStatuses()
                .setFlags(tradesmanFlags.isAdmin, tradesmanFlags.isVendorSupervisor)
                .setTradesman(tradesmanFlags.tradesman)
                .setOrderTypes(VTT_Constants.FILTERABLE_ORDER_TYPES)
                .setAMUs(VTT_WOA_SCDAOProvider.AMUDAO.getAMUsByBusinessUnitName(VTT_Constants.BUSINESS_UNIT_NAME_HOG))
                .setVendors(VTT_WOA_SCDAOProvider.accountDAO.getAccountsWithContactsByRecordType(VTT_Constants.VENDOR_ACCOUNT_RECORD_TYPE_ID))
                .setTradesmans(VTT_WOA_SCDAOProvider.contactDAO.getContactsByAccount(tradesmanFlags.tradesman.AccountId))
                .buildCleanCriteria();
    }

    /**
     * Based on Account Id it will query contacts which are assigned to that Account, builds a List that has format
     * that is supported in Lightning combobox and returns the result.
     *
     * @param accountId
     *
     * @return List<HOG_PicklistItem_Simple>
     */
    public List<HOG_PicklistItem_Simple> getTradesmanForAccount(String accountId) {
        VTT_WOA_SearchCriteria_Options tradesmanOptions = new VTT_WOA_SearchCriteria_Options();

        if(String.isBlank(accountId) || accountId == '1') {
            tradesmanOptions.setTradesmans(new List<Contact>());
        } else {
            tradesmanOptions.setTradesmans(VTT_WOA_SCDAOProvider.contactDAO.getContactsByAccount(accountId));
        }

        return tradesmanOptions.tradesmans;
    }

    /**
     * Retrieves stored VTT Activity List View filters based on provided User Id.
     * Parameter is validated first.
     *
     * @param userId
     *
     * @return List<Work_Order_Activity_Search_Criteria__c>
     */
    public List<Work_Order_Activity_Search_Criteria__c> getStoredFilters(Id userId) {
        validateUserIdParam(userId);

        return VTT_WOA_SCDAOProvider.searchCriteriaDAO.getUserActivitySearchFilters(userId);
    }

    /**
     * Sets filter as last selected based on provided filter Id and User Id.
     * First both Parameters are validated and error is thrown if any of them are invalid (empty)
     * Then it will query all filters for current user.
     * For all other except the one that Id was provided as parameter is Last_Search_Criteria_Selected__c set to false.
     * For the One is this field set to true.
     *
     * @param filterId
     * @param userId
     *
     * @return List<Work_Order_Activity_Search_Criteria__c>
     */
    public List<Work_Order_Activity_Search_Criteria__c> setLastSelectedFilter(Id filterId, Id userId) {
        validateFilterId(filterId);
        validateUserIdParam(userId);

        List<Work_Order_Activity_Search_Criteria__c> criteria = VTT_WOA_SCDAOProvider.searchCriteriaDAO.getUserActivitySearchFilters(userId);

        for(Work_Order_Activity_Search_Criteria__c criteriaItem : criteria) {
            criteriaItem.Last_Search_Criteria_Selected__c = criteriaItem.Id == filterId;
        }

        update criteria;

        return criteria;
    }

    /**
     * This method saves filter into Stored Filters for later use.
     * First it validates all parameters and if any of them is missing, it will throw an error.
     * If everything is alright, Work_Order_Activity_Search_Criteria__c record is build from provided params and inserted.
     * it's also set as last Selected Filter and returned
     *
     * @param filterString
     * @param filterName
     * @param userId
     *
     * @return Work_Order_Activity_Search_Criteria__c
     */
    public Work_Order_Activity_Search_Criteria__c saveFilter(String filterString, String filterName, Id userId) {
        validateFilterParams(filterString, filterName);
        validateUserIdParam(userId);

        Work_Order_Activity_Search_Criteria__c filter = new Work_Order_Activity_Search_Criteria__c();
        filter.Assigned_Name__c = filterName;
        filter.Filter_String__c = JSON.serialize(JSON.deserialize(filterString, VTT_WOA_SearchCriteria.class));
        filter.User__c = userId;
        filter.Last_Search_Criteria_Selected__c = true;
        insert filter;
        //Even this is used to un-select other records.
        setLastSelectedFilter(filter.Id, userId);

        return filter;
    }

    /**
     * Deletes filter based on provided Id.
     * First it does the validation on param (empty check), then it deletes the filter and return 'Success' String.
     *
     * @param filterId
     *
     * @return String
     */
    public String deleteFilter(Id filterId) {
        validateFilterId(filterId);

        delete new Work_Order_Activity_Search_Criteria__c(
                Id = filterId
        );

        return 'Success';
    }

    /**
     * Runs blank check and if Param is blank, it throws meaningful error.
     *
     * @param userId
     */
    private void validateUserIdParam(Id userId) {
        if(String.isBlank(userId)) {
            HOG_ExceptionUtils.throwError(
                    String.format(
                            Label.HOG_Param_Not_Provided,
                            new List<String> {
                                    'User Id'
                            }
                    )
            );
        }
    }

    /**
     * Runs blank check and if Param is blank, it throws meaningful error.
     *
     * @param filterId
     */
    private void validateFilterId(Id filterId) {
        if(String.isBlank(filterId)) {
            HOG_ExceptionUtils.throwError(
                    String.format(
                            Label.HOG_Param_Not_Provided,
                            new List<String> {
                                    'Filter Id'
                            }
                    )
            );
        }
    }

    /**
     * Runs blank check and if any of the Params are blank, it throws meaningful error.
     *
     * @param filterString
     * @param filterName
     */
    private void validateFilterParams(String filterString, String filterName) {
        if(String.isBlank(filterString)) {
            HOG_ExceptionUtils.throwError(
                    String.format(
                            Label.HOG_Params_Not_Provided,
                            new List<String> {
                                    'Filter values'
                            }
                    )
            );
        }
        if(String.isBlank(filterName)) {
            HOG_ExceptionUtils.throwError(
                    String.format(
                            Label.HOG_Param_Not_Provided,
                            new List<String> {
                                    'Filter Name'
                            }
                    )
            );
        }
    }

}