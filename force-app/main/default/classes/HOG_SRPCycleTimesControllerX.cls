/*----------------------------------------------------------------------------------------------------
Author     : Miroslav Zelina
Company    : Husky Energy
Description: A controller class for displaying Cycle Times 
             - calculations for specific dates on EOJ,SRP & ED
Test Class : HOG_SRPCycleTimesControllerXTest
History    : 04.27.17 mz Initial Revision 
             04.28.17 mz Added Info Messages
             05.04.17 mz Fixed logic for ED with correct EOJ
             06.04.17 mz simplified logic
------------------------------------------------------------------------------------------------------*/
public with sharing class HOG_SRPCycleTimesControllerX {
    
    //Private Variables
    @TestVisible
    private Id srpId;
    private List<HOG_EOJ__c> EOJList;
    private List<HOG_Engineering_Diagnosis__c> EDList;
    private Set<Id> EOJIdSet;
    private Map<Id, HOG_Engineering_Diagnosis__c> eojIdToEDMap;
    
    //VF Page variables
    public HOG_EOJ__c eoj {get; set;}
    public HOG_Engineering_Diagnosis__c ed {get; set;}
    
    //Constructor
    public HOG_SRPCycleTimesControllerX(ApexPages.StandardController stdController) {
        
        srpId = stdController.getId();
        getEOJandEDRecords();
    }
    
    
    private void getEOJandEDRecords() {
        
       EOJList = new List<HOG_EOJ__c>(); 
       EDList = new List<HOG_Engineering_Diagnosis__c>();
       eoj = new HOG_EOJ__c();
       ed = new HOG_Engineering_Diagnosis__c();

       
       //Query for related EOJ(s) per SRP
       EOJList = [SELECT Id, Well_Down_to_Service_Completed__c, Fully_Approved_to_Service_Started__c, 
                         Serv_Started_to_Serv_Completed__c
                  FROM HOG_EOJ__c
                  WHERE Service_Rig_Program__c =: srpId
                  ORDER BY CreatedDate ASC];
                  
                  
       if (!EOJList.isEmpty() && EOJList <> null){
           
           eoj = EOJList[0];
           
           //Query for oldest ED - we don't need to filter on EOJ per ED, 
           //as we query for oldest EOJ once we creating new ED
           //so there will allways be the same EOJ
           EDList = [SELECT Id, Serv_Completed_to_ED_Completed__c, End_Of_Job__c
                     FROM HOG_Engineering_Diagnosis__c
                     WHERE Service_Rig_Program__c = :srpId
                     ORDER BY CreatedDate ASC]; 
            
           if (EOJList.Size() > 1){
            
              ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,
                    'This SRP has more than one End of Job, Cycle Times are calculated based on the oldest one.'));  
           } 
       }
       
       
       if (!EDList.isEmpty() && EDList <> null){
           
           ed = EDList[0];

           if (EDList.Size() > 1){
               
              ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.info,
                    'This SRP has more than one Engineering Diagnosis form, Cycle Times are calculated based on the oldest one.'));
           }
       }
    }
}