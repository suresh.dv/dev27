/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 10/26/2019   
 */

@IsTest
public class ELG_Task_EndpointTest {

	@IsTest
	static void createTaskTest() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		post = [SELECT Id, Operating_Field_AMU__c FROM ELG_Post__c LIMIT 1];
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		ELG_User_Setting__c settings = new ELG_User_Setting__c(User__c = usr.Id, Operating_Field_AMU__c = post.Operating_Field_AMU__c);
		insert settings;

		Test.startTest();
		System.runAs(usr) {
			ELG_Task_Endpoint.getUserSettings();
			response = ELG_Task_Endpoint.createPostTask(post.Id, 'short description', 'task description');
			System.assertNotEquals(null, response.resultObjects[0]);

			response = ELG_Task_Endpoint.getPostTasks(post.Id);
			System.assertNotEquals(null, response.resultObjects[0]);


			List<ELG_Task__c> task = [
					SELECT Id,
							Operating_Field_AMU__c,
							Short_Description__c,
							Task_Description__c,
							Status__c
					FROM ELG_Task__c
					WHERE Post__c = :post.Id
			];

			System.assertEquals(1, task.size());
			System.assertEquals(post.Operating_Field_AMU__c, task[0].Operating_Field_AMU__c);
			System.assertEquals(ELG_Constants.TASK_ACTIVE, task[0].Status__c);
			System.assertEquals('short description', task[0].Short_Description__c);
			System.assertEquals('task description', task[0].Task_Description__c);
		}
		Test.stopTest();
	}

	@IsTest
	static void getAvailablePostsFromAMUTest() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		post = [SELECT Id, Operating_Field_AMU__c FROM ELG_Post__c LIMIT 1];
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		ELG_User_Setting__c settings = new ELG_User_Setting__c(User__c = usr.Id, Operating_Field_AMU__c = post.Operating_Field_AMU__c);
		insert settings;

		Test.startTest();
		System.runAs(usr) {
			response = ELG_Task_Endpoint.getAMUPosts(post.Operating_Field_AMU__c);
			System.assertNotEquals(null, response.resultObjects[0]);
			System.assertEquals(1, response.resultObjects.size());
		}
		Test.stopTest();
	}

	@IsTest
	static void updateTaskTest() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		post = [SELECT Id, Operating_Field_AMU__c FROM ELG_Post__c LIMIT 1];

		Test.startTest();
		ELG_Task_Endpoint.getUserSettings();
		ELG_Task__c task = new ELG_Task__c(Post__c = post.Id,
				Operating_Field_AMU__c = post.Operating_Field_AMU__c,
				Short_Description__c = 'old value',
				Task_Description__c = 'old task description');
		insert task;

		ELG_Task_Endpoint.getTask(post.Id);
		ELG_Task_Endpoint.getTask(task.Id);

		task.Short_Description__c = 'new value';
		task.Task_Description__c = 'new task description';

		response = ELG_Task_Endpoint.saveEditedTask(task);
		System.assertNotEquals(null, response.resultObjects[0]);

		List<ELG_Task__c> taskList = [
				SELECT Id,
						Operating_Field_AMU__c,
						Short_Description__c,
						Task_Description__c
				FROM ELG_Task__c
				WHERE Post__c = :post.Id
				LIMIT 1
		];

		System.assertEquals(1, taskList.size());
		System.assertEquals('new value', taskList[0].Short_Description__c);
		System.assertEquals('new task description', taskList[0].Task_Description__c);

		Test.stopTest();
	}

	@IsTest
	static void taskUpdateCompleteTest() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		post = [SELECT Id, Operating_Field_AMU__c FROM ELG_Post__c LIMIT 1];

		Test.startTest();
		ELG_Task__c task = new ELG_Task__c(Post__c = post.Id,
				Operating_Field_AMU__c = post.Operating_Field_AMU__c,
				Short_Description__c = 'old value',
				Task_Description__c = 'old task description');
		insert task;

		response = ELG_Task_Endpoint.taskUpdateComplete(task.Id, usr.Id, 'Comment');
		System.assertNotEquals(null, response.resultObjects[0]);

		List<ELG_Task__c> taskList = [
				SELECT Id,
						Shift_Assignement__c,
						Comment__c,
						Status__c,
						Finished_By__c,
						Finished_At__c
				FROM ELG_Task__c
				WHERE Post__c = :post.Id
				LIMIT 1
		];

		System.assertEquals(1, taskList.size());
		System.assertEquals(shift.Id, taskList[0].Shift_Assignement__c);
		System.assertEquals('Comment', taskList[0].Comment__c);
		System.assertEquals(ELG_Constants.TASK_COMPLETED, taskList[0].Status__c);
		System.assertEquals(usr.Id, taskList[0].Finished_By__c);
		System.assertNotEquals(null, taskList[0].Finished_At__c);
		Test.stopTest();
	}

	@IsTest
	static void taskUpdateCancelledTest() {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		User usr = [SELECT Id FROM User WHERE Alias = 'theOne'];
		post = [SELECT Id, Operating_Field_AMU__c FROM ELG_Post__c LIMIT 1];

		Test.startTest();
		ELG_Task__c task = new ELG_Task__c(Post__c = post.Id,
				Operating_Field_AMU__c = post.Operating_Field_AMU__c,
				Short_Description__c = 'old value',
				Task_Description__c = 'old task description');
		insert task;

		response = ELG_Task_Endpoint.taskUpdateCancelled(task.Id, usr.Id, 'Reason');
		System.assertNotEquals(null, response.resultObjects[0]);

		List<ELG_Task__c> taskList = [
				SELECT Id,
						Shift_Assignement__c,
						Reason__c,
						Status__c,
						Finished_By__c,
						Finished_At__c
				FROM ELG_Task__c
				WHERE Post__c = :post.Id
				LIMIT 1
		];

		System.assertEquals(1, taskList.size());
		System.assertEquals(shift.Id, taskList[0].Shift_Assignement__c);
		System.assertEquals('Reason', taskList[0].Reason__c);
		System.assertEquals(ELG_Constants.TASK_CANCELLED, taskList[0].Status__c);
		System.assertEquals(usr.Id, taskList[0].Finished_By__c);
		System.assertNotEquals(null, taskList[0].Finished_At__c);
		Test.stopTest();
	}

	/*Test Data*/
	@TestSetup
	static void testData() {
		ELG_TestDataFactory.createLogEntryUser();
	}

}