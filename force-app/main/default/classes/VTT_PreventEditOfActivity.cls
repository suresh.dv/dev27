/*************************************************************************************************\
Author:         Martin Panak
Company:        Husky Energy
Description:    Controller for preventing edit of Activity by Vendor Community Tradesman 
Test class      VTT_PreventEditOfActivityTest 
History:        mp 09.07.2018 - Created for W-001165 Vendor is able to edit work order information
**************************************************************************************************/
public with sharing class VTT_PreventEditOfActivity {
    public static Id userId {get;set;}
    public static String retUrl {get;set;}
    public static String activityId {get;set;}

    public VTT_PreventEditOfActivity (ApexPages.StandardController stdController) {
        userId = userinfo.getuserId();
        activityId = ApexPages.currentPage().getParameters().get('id');
        retUrl = ApexPages.currentPage().getParameters().get('retURL');
        retUrl = retUrl.replace('/a', '');
    }

    /**
     * [Redirect to standard/community edit page for Work Order Activity]
     * - For Vendor Community Tradesman and Supervisors show error Page
     * - For all other users redirect to standard edit Page for Work Order Activity
     * @return [PageReference]
     */
    public PageReference redirect(){
        if(isPortalUser()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.WARNING, System.Label.VTT_PreventEditOfActivity_ErrorMessage));
            return null;
        } else {
            PageReference editPage = new PageReference('/'+ activityId+'/e?retURL=%2Fa'+retUrl+ '&nooverride=1');   
            editPage.setRedirect(true);
            return editPage;
        }
    }
    /**
     * [Check if the current user is a portal user]
     * @return [Boolean]
     */
    public static Boolean isPortalUser(){
        String userType = UserInfo.getUserType();
        if(userType == 'PowerPartner'){
            return true;
        }
        return false;
    }
  }