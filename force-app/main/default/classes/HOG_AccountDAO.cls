/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DOA layer interface for Account SObject queries for general VTT purposes
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public interface HOG_AccountDAO {

    List<Account> getHuskyAccountsByRecordTypeId(Id recordTypeId);

}