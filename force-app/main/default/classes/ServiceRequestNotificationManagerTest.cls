/*---------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: Test class for ServiceRequestNotificationManager
History    : 12-Jul-2018 mz: Update test for 2Z FIX
             TBD -> include some asserts ! 
----------------------------------------------------------------------------------------------------------*/  
@isTest
private class ServiceRequestNotificationManagerTest
{
    static List<HOG_Maintenance_Servicing_Form__c> riglessServicingForm;
	static List<HOG_Service_Request_Notification_Form__c> serviceRequest;
	static List<Well_Tracker__c> wellTracker;
    static List<HOG_Notification_Type__c> notificationType;
    static List<HOG_Notification_Type_Priority__c> notificationTypePriority;
    static List<HOG_Work_Order_Type__c> workOrderType;
	static Equipment__c equipmentLocation;    
    static HOG_Service_Code_MAT__c serviceCodeMAT;
    static HOG_Service_Category__c serviceCategory;
    static HOG_Service_Code_Group__c serviceCodeGroup;
    static HOG_Service_Required__c serviceRequiredList;
	static HOG_User_Status__c userStatus;
	static HOG_Service_Priority__c servicePriorityList;
	static Business_Unit__c businessUnit;
	static Business_Department__c businessDepartment;
	static Operating_District__c operatingDistrict;
	static Field__c field;
	static Route__c route;
	static Location__c location;	
	static Facility__c facility;		
	static Account account;
    static User operator1;
    static User operator2;
	static User operator3;
    static User operator4;
    static User operator5;
    static User operator6;
    static Contact contact1;
	static Contact contact2;
	static Contact contact3;
    static Contact contact4;
    static Contact contact5;
    static Contact contact6;
	static HOG_Settings__c settings;
	static Id recordTypeId;
	static String NOTIFICATIONOBJECTNAME = 'HOG_Service_Request_Notification_Form__c';
	static List<String> WORKORDERRECORDTYPE = new List<String>{'Flushby - Production Flush', 'Coil Tubing Unit - Rod Side Entry'};

    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Location_Test()
    {                                                
		SetupTestData();
        
            User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
            System.runAs (thisUser) {
		
		    Profile cro_profile = [SELECT Id FROM Profile WHERE Name='Standard HOG - General User'];
            UserRole cro_role = [SELECT Id FROM UserRole WHERE Name ='Control Room Operator'  Limit 1];
            List<Contact> contacts = [SELECT Id, Name, User__c FROM Contact ORDER BY Name];
            
            operator1 = UserTestData.createTestUser(cro_profile, cro_role);
            operator1.FirstName = 'Operator';
            operator1.LastName = '1';
            operator1.MobilePhone = '111-111-111';
            insert operator1;
            contacts[0].User__c = operator1.Id;
            
            operator2 = UserTestData.createTestUser(cro_profile, cro_role);
            operator2.FirstName = 'Operator';
            operator2.LastName = '2';
            operator2.MobilePhone = '222-222-222';
            insert operator2;
            contacts[1].User__c = operator2.Id;
            
            operator3 = UserTestData.createTestUser(cro_profile, cro_role);
            operator3.FirstName = 'Operator';
            operator3.LastName = '3';
            insert operator3;
            contacts[2].User__c = operator3.Id;
            
            operator4 = UserTestData.createTestUser(cro_profile, cro_role);
            operator4.FirstName = 'Operator';
            operator4.LastName = '4';
            operator4.MobilePhone = '444-444-444';
            insert operator4;
            contacts[3].User__c = operator4.Id;
            
            operator5 = UserTestData.createTestUser(cro_profile, cro_role);
            operator5.FirstName = 'Operator';
            operator5.LastName = '5';
            operator5.MobilePhone = '555-555-555';
            insert operator5;
            contacts[4].User__c = operator5.Id;
            
            operator6 = UserTestData.createTestUser(cro_profile, cro_role);
            operator6.FirstName = 'Operator';
            operator6.LastName = '6';
            operator6.MobilePhone = '666-666-666';
            insert operator6;

            update contacts;
            
            
            route.Operator_1_User__c = operator1.Id;
    		route.Operator_2_User__c = operator2.Id;
    		route.Operator_3_User__c = operator3.Id;
    		route.Operator_4_User__c = operator4.Id;
    		route.Operator_5_User__c = operator5.Id;
    		//route.Operator_6_User__c = operator6.Id;
    
        	update route;
        	
        	serviceRequest[0].Operator_Route_Lookup__c = route.Id;
        	update serviceRequest;
            }

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[0].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[0].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[0].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring
        
        System.AssertEquals(3,[Select Id from HOG_Maintenance_Servicing_Form__c].Size());

        Test.startTest();

            ServiceRequestNotificationManager.process();
        
        Test.stopTest();
        
        System.AssertEquals(4,[Select Id from HOG_Maintenance_Servicing_Form__c].Size());
        
        List<HOG_Maintenance_Servicing_Form__c> woList = [SELECT Id, Operator_1_detail__c, Operator_2_detail__c,
                                                                 Operator_3_detail__c, Operator_4_detail__c,
                                                                 Operator_5_detail__c, Operator_6_detail__c
                                                          FROM HOG_Maintenance_Servicing_Form__c
                                                          WHERE Name = 'Test Location - Pending Work Order'];
        
        System.AssertEquals('Operator 1 | 2Z-11111 | 111-111-111', woList[0].Operator_1_detail__c);
    	System.AssertEquals('Operator 2 | 2Z-22222 | 222-222-222', woList[0].Operator_2_detail__c);
    	System.AssertEquals('Operator 3 | 2Z-33333 | N/A', woList[0].Operator_3_detail__c);
    	System.AssertEquals('Operator 4 | 2Z-44444 | 444-444-444', woList[0].Operator_4_detail__c);
        System.AssertEquals('Operator 5 | N/A | 555-555-555', woList[0].Operator_5_detail__c);
        System.AssertEquals('N/A | N/A | N/A', woList[0].Operator_6_detail__c);
    }
 
    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Facility_Test()
    {        
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[1].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[1].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[1].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }    

    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Equipment_Test()
    {        
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[2].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[2].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[2].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }

/*
    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Location_WO_Mock_Test()
    {                                                
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[0].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[0].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[0].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGWorkOrdersResponseMock());

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }
    
    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Location_NO_Mock_Test()
    {                                                
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[0].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[0].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[2].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGWorkOrdersResponseMock());

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }

    @isTest (SeeAllData = false) 
    static void ServiceRequestNotificationManager_Facility_Mock_Test()
    {        
		SetupTestData();

        // --------- start Insert Task records
        String taskSubject = 'Create Work Order';
        String taskStatus = 'Not Started';
        String taskRecurrenceType = 'RecursDaily';
        String taskRecurrenceInstance;
        String taskRecurrenceMonthOfYear;
        Integer taskRecurrenceDayOfWeekMask;
        Integer taskRecurrenceInterval = 1;
        Integer taskRecurrenceDayOfMonth;                    
        Date taskRecurrenceStartDateOnly = serviceRequest[1].Recurring_Start_Date__c;
        Date taskRecurrenceEndDateOnly = serviceRequest[1].Recurring_End_Date__c;
        Id taskWhatId = serviceRequest[1].Id;
        Id taskOwnerId = UserInfo.getUserId();
                
        Integer frequencyTaskQuantity = 1;
        String frequency = 'Daily';
        String frequencyDaily = 'Every weekday';
                                    
        for (Integer taskQuantity = 0; taskQuantity < frequencyTaskQuantity; taskQuantity++)
        {
            Task taskRecurringRecord = new Task
                (               
                    Subject = taskSubject,
                    Status = taskStatus,
                    RecurrenceType = taskRecurrenceType,
                    RecurrenceMonthOfYear = taskRecurrenceMonthOfYear,
                    RecurrenceDayOfMonth = taskRecurrenceDayOfMonth,
                    RecurrenceDayOfWeekMask = taskRecurrenceDayOfWeekMask,
                    RecurrenceInterval = taskRecurrenceInterval,
                    RecurrenceInstance = taskRecurrenceInstance,
                    RecurrenceStartDateOnly = taskRecurrenceStartDateOnly,
                    RecurrenceEndDateOnly = taskRecurrenceEndDateOnly,
                    IsRecurrence = true,
                    WhatId = taskWhatId,
                    OwnerId = taskOwnerId
                );
                
            insert taskRecurringRecord;

        }
                        
        List<Task> insertedTask = 
            [Select WhatId, OwnerId, Status, ActivityDate From Task Where What.Type ='HOG_Service_Request_Notification_Form__c'];
        // --------- end Insert Task records if reccuring

        Test.startTest();

        // This causes a fake response to be generated
        Test.setMock(WebServiceMock.class, new SAPHOGCreateNotificationsResponseMock());

        ServiceRequestNotificationManager.process();
        
        Test.stopTest();        
    }    
*/
    
	private static void SetupTestData()
	{        
        //-- Begin setup of data needed to test the Service Request Notification controller --//
                
        //-- Setup Service Category
        serviceCategory = ServiceCategoryTestData.createServiceCategory('Test Category');
        insert serviceCategory;
        //--

        //-- Setup Service Code Group
        serviceCodeGroup = ServiceCodeGroupTestData.createServiceCodeGroup('Test Group', true);
        insert serviceCodeGroup;
        //--
        
        //-- Setup Notification Type
	    notificationType = new List<HOG_Notification_Type__c>();        
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, false, true, true, true));
 		notificationType.add((HOG_Notification_Type__c)NotificationTypeTestData.createNotificationType(serviceCategory.Id, true, false, true, true));
        insert notificationType;
        //--
 
        //-- Setup Service Code MAT        
 	    serviceCodeMAT = ServiceCodeMATTestData.createServiceCodeMAT('Test MAT1', 'TS1', WORKORDERRECORDTYPE[0]);
        insert serviceCodeMAT;
        //--

        //-- Setup Service Required
        serviceRequiredList = ServiceRequiredTestData.createServiceRequired('Test Service Required', true, true, true);
        insert serviceRequiredList;
        //--

        //-- Setup Service User Status
		userStatus = UserStatusTestData.createUserStatus('Test User Status', 'Test Description');
        insert userStatus;
        //--

        //-- Setup Service - Priority
		servicePriorityList = ServicePriorityTestData.createServicePriority('Service Priority 1', 1, '1');
        insert servicePriorityList;
        //--

        //-- Setup Notification Type - Priority Detail
     	notificationTypePriority = new List<HOG_Notification_Type_Priority__c>();
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[0].Id, servicePriorityList.Id, false));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, true));                
        notificationTypePriority.add((HOG_Notification_Type_Priority__c)NotificationTypePriorityTestData.createNotificationTypePriority(notificationType[1].Id, servicePriorityList.Id, false));                
        insert notificationTypePriority;        
        //--
                
        //-- Setup Work Order Type
 	    workOrderType = new List<HOG_Work_Order_Type__c>();
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[0].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[0], true));
 	    workOrderType.add(WorkOrderTypeTestData.createWorkOrderType(notificationType[1].Id, serviceCodeMAT.Id, serviceRequiredList.Id, userStatus.Id, WORKORDERRECORDTYPE[1], true));
        insert workOrderType;
        //--
                                        
        //-- Setup objects for Well Tracker                                
        businessUnit = new Business_Unit__c(Name = 'Test Business Unit');
        insert businessUnit;

		businessDepartment = BusinessDepartmentTestData.createBusinessDepartment('Test Business Department');    
        insert businessDepartment;

		operatingDistrict = OperatingDistrictTestData.createOperatingDistrict('Test Field', businessDepartment.Id, businessUnit.Id);
        insert operatingDistrict;

        recordTypeId = [Select Id From RecordType Where SObjectType = 'Field__c' And Name = 'HOG - Control Room Centre Record'].Id;

		field = FieldTestData.createField('Test Field Control Centre', operatingDistrict.Id, recordTypeId);
        insert field;

		route = RouteTestData.createRoute('999');
        insert route;                  

		location = LocationTestData.createLocation('Test Location', route.Id, field.Id);
		location.DLFL__c = false;		
        insert location;

 		equipmentLocation = EquipmentTestData.createEquipment(location);

		facility = FacilityTestData.createFacility('Test Facility', route.Id, field.Id);
        insert facility;        

		account = AccountTestData.createAccount('Test Account', null);			
        insert account;
        
        contact1 = ContactTestData.createContact(account.Id, 'Operator', '1');
        contact2 = ContactTestData.createContact(account.Id, 'Operator', '2');
        contact3 = ContactTestData.createContact(account.Id, 'Operator', '3');
        contact4 = ContactTestData.createContact(account.Id, 'Operator', '4');
        contact5 = ContactTestData.createContact(account.Id, 'Operator', '5');
        contact6 = ContactTestData.createContact(account.Id, 'Operator', '6');
        
        contact1.X2Z_Employee_ID__c = '2Z-11111';
        contact2.X2Z_Employee_ID__c = '2Z-22222';
        contact3.X2Z_Employee_ID__c = '2Z-33333';
        contact4.X2Z_Employee_ID__c = '2Z-44444';
        contact6.X2Z_Employee_ID__c = '2Z-66666';
        
        List<Contact> contactList = new List<Contact>{contact1, contact2, contact3, contact4, contact5, contact6};
        insert contactList;

		wellTracker = new List<Well_Tracker__c>();
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(location.Id, null, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
		wellTracker.add((Well_Tracker__c)WellTrackerTestData.createWellTracker(null, facility.Id, field.Id, 1, Datetime.valueOf('2013-06-20 12:00:00'), 'Producing', 'Engineering'));
        insert wellTracker;
        //--

	    ServiceRequestNotificationUtilities.executeTriggerCode = false;
    	WellServicingUtilities.executeTriggerCode = false;
    	MaintenanceServicingUtilities.executeTriggerCode = false;

		serviceRequest = new List<HOG_Service_Request_Notification_Form__c>();

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[0].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'7521256',
				'98765430',
				'Test Title',
				'Test Detail',
				Date.today(),
				Date.today() + 10
			));
			
		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[1].Id,
				workOrderType[1].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[3].Id,
				notificationTypePriority[2].Id,
				account.Id,
				'7521257',
				'98765431',
				'Test Title',
				'Test Detail',
				Date.today(),
				Date.today() + 10
			));

		serviceRequest.add((HOG_Service_Request_Notification_Form__c)ServiceRequestNotificationFormTestData.createServiceRequestNotificationForm
			(
				wellTracker[0].Id,
				workOrderType[1].Id,
				null,
				null,
				null,
				null,
				null,
				null,
				notificationTypePriority[1].Id,
				notificationTypePriority[0].Id,
				account.Id,
				'7521258',
				'98765432',
				'Test Title',
				'Test Detail',
				Date.today(),
				Date.today() + 10
			));

		 serviceRequest[0].Location_Lookup__c = location.Id;
         serviceRequest[0].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Well Tracker');        
		 
		 serviceRequest[1].Facility_Lookup__c = facility.Id;
         serviceRequest[1].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Facility Tracker');        
		 		 
		 serviceRequest[2].Location_Lookup__c = location.Id;
		 serviceRequest[2].Equipment__c = equipmentLocation.Id;
         serviceRequest[2].RecordTypeId = ServiceRequestNotificationUtilities.readObjectRecordTypeId(NOTIFICATIONOBJECTNAME, 'Equipment');        

         insert serviceRequest;
        //-- End setup of data needed to test the Service Request Notification controller --//

        //-- Start setup data for Work Order        
        riglessServicingForm = new List<HOG_Maintenance_Servicing_Form__c>();
        
		ServiceRequestNotificationUtilities.ServiceRequest request = new ServiceRequestNotificationUtilities.ServiceRequest(); 

    	request.workOrderName = 'Test Location';    	
    	request.workOrderCount = 1;
		request.notificationType = notificationType[0].Id;
        request.serviceTimeIsRequired = false;
		request.vendorInvoicingPersonnelIsRequired = false;		
        					
		request.record = serviceRequest[0];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[1];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
		request.record = serviceRequest[2];
        riglessServicingForm.add((HOG_Maintenance_Servicing_Form__c)ServiceRequestNotificationUtilities.createServiceWorkOrder(request));
        
        insert riglessServicingForm;                               
        //-- End setup data for Work Order        

		settings = HOGSettingsTestData.createHOGSettings();
		insert settings;
	}    
}