public class HOG_SelectResponseImpl extends HOG_SelectResponse{

	public HOG_SelectResponseImpl(SObject result){
		addResult(new List<SObject>{result});
	}

	public HOG_SelectResponseImpl(List<SObject> results){
		addResult(results);
	}

	public HOG_SelectResponseImpl(String errorMsg) {
		addError(errorMsg);
	}

	public HOG_SelectResponseImpl(Exception ex) {
		addError(ex.getMessage());
	}

	public void addResult(List<SObject> results) {
		success = true;
		if(resultObjects == null) resultObjects = new List<SObject>();
		resultObjects.addAll(results);
	}

	public List<String> addError(Exception ex) {
		addError(ex.getMessage());
		return errors;
	}

	public List<String> addError(String errorMsg) {
		success = false;
		if(errors == null) errors = new List<String>();
		errors.add(errorMsg);
		return errors;
	}

}