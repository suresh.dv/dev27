/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 12/3/2019   
 */

public class Equipment_Forms_QuerySelector {

	/* ADD EQUIPMENT FORM */
	public static List<Equipment_Missing_Form__c> getActiveAddForms(String flockId) {
		return [
				SELECT Id,
						Name,
						Status__c,
						Equipment_Status__c,
						Manufacturer__c,
						Manufacturer_Serial_No__c,
						Model_Number__c,
						Tag_Number__c,
						CreatedDate,
						CreatedBy.Name
				FROM Equipment_Missing_Form__c
				//WHERE flockAPI = :flockId
		];
	}

	public static Equipment_Missing_Form__c getAddEquipmentForm(String formId) {
		return [
				SELECT Id,
						Name,
						Description__c,
						Status__c,
						Equipment_Status__c,
						Manufacturer__c,
						Manufacturer_Serial_No__c,
						Model_Number__c,
						Tag_Number__c,
						Comments__c,
						Reason__c,
						Safety_Critical_Equipment__c,
						Regulatory_Equipment__c,
						Part_of_Package__c,
						Expiration_Date__c,
						P_ID__c,
						Serial_plate_of_asset__c,
						Attachment__c,
						CreatedDate,
						CreatedBy.Name,
						LastModifiedDate,
						LastModifiedBy.Name,
						Location__r.Name,
						Facility__r.Name,
						Well_Event__r.Name,
						Functional_Equipment_Level__r.Name,
						Yard__r.Name,
						System__r.Name,
						Sub_System__r.Name,
						Location__r.Route__c,
						Location__r.Route__r.Name,
						Facility__r.Plant_Section__c,
						Facility__r.Plant_Section__r.Name,
						Well_Event__r.Route__c,
						Well_Event__r.Route__r.Name,
						Functional_Equipment_Level__r.Route__c,
						Functional_Equipment_Level__r.Route__r.Name,
						Yard__r.Route__c,
						Yard__r.Route__r.Name,
						System__r.Route__c,
						System__r.Route__r.Name,
						Sub_System__r.Route__c,
						Sub_System__r.Route__r.Name,
						Location__r.Planner_Group__c,
						Facility__r.Planner_Group__c,
						Well_Event__r.Planner_Group__c,
						Functional_Equipment_Level__r.Planner_Group__c,
						Yard__r.Planner_Group__c,
						System__r.Planner_Group__c,
						Sub_System__r.Planner_Group__c,
						Location__r.Functional_Location__c,
						Facility__r.Functional_Location__c,
						Well_Event__r.Functional_Location__c,
						Functional_Equipment_Level__r.Functional_Location__c,
						Yard__r.Functional_Location__c,
						System__r.Functional_Location__c,
						Sub_System__r.Functional_Location__c
				FROM Equipment_Missing_Form__c
				WHERE Id = :formId
		];
	}

	/* EQUIPMENT TRANSFER FORM */
	public static Equipment_Transfer_Form__c getEquipmentTransferForm(String formId) {
		return [
				SELECT Id,
						Name,
						Reason_for_Transfer_Additional_Details__c,
						Status__c,
						Date_of_Physical_Transfer__c,
						Authorized_By__r.Name,
						Maintenance_Work_Order__c,
						Shipped_Via__c,
						Waybill_No__c,
						Comments__c,
						Reason__c,
						Safety_Critical_Equipment__c,
						Regulatory_Equipment__c,
						Charge_No__c,
						Expiration_Date__c,
						CreatedDate,
						CreatedBy.Name,
						LastModifiedBy.Name,
						LastModifiedDate,
						Maintenance_Work_Order__r.Name,
						TO_Destination__c,
						FROM_Source__c,
						To_Location__c,
						To_Location__r.Name,
						To_Location__r.Route__c,
						To_Location__r.Route__r.Name,
						To_Location__r.Planner_Group__c,
						To_Facility__c,
						To_Facility__r.Name,
						To_Facility__r.Plant_Section__c,
						To_Facility__r.Plant_Section__r.Name,
						To_Facility__r.Planner_Group__c,
						To_Functional_Equipment_Level__c,
						To_Functional_Equipment_Level__r.Name,
						To_Functional_Equipment_Level__r.Route__c,
						To_Functional_Equipment_Level__r.Route__r.Name,
						To_Functional_Equipment_Level__r.Planner_Group__c,
						To_Yard__c,
						To_Yard__r.Name,
						To_Yard__r.Route__c,
						To_Yard__r.Route__r.Name,
						To_Yard__r.Planner_Group__c,
						To_System__c,
						To_System__r.Name,
						To_System__r.Route__c,
						To_System__r.Route__r.Name,
						To_System__r.Planner_Group__c,
						To_Sub_System__c,
						To_Sub_System__r.Name,
						To_Sub_System__r.Route__c,
						To_Sub_System__r.Route__r.Name,
						To_Sub_System__r.Planner_Group__c,
						To_Well_Event__c,
						To_Well_Event__r.Name,
						To_Well_Event__r.Route__c,
						To_Well_Event__r.Route__r.Name,
						To_Well_Event__r.Planner_Group__c,
						From_Location__c,
						From_Location__r.Name,
						From_Location__r.Route__c,
						From_Location__r.Route__r.Name,
						From_Location__r.Planner_Group__c,
						From_Facility__c,
						From_Facility__r.Name,
						From_Facility__r.Plant_Section__c,
						From_Facility__r.Plant_Section__r.Name,
						From_Facility__r.Planner_Group__c,
						From_Functional_Equipment_Level__c,
						From_Functional_Equipment_Level__r.Name,
						From_Functional_Equipment_Level__r.Route__c,
						From_Functional_Equipment_Level__r.Route__r.Name,
						From_Functional_Equipment_Level__r.Planner_Group__c,
						From_Yard__c,
						From_Yard__r.Name,
						From_Yard__r.Route__c,
						From_Yard__r.Route__r.Name,
						From_Yard__r.Planner_Group__c,
						From_System__c,
						From_System__r.Name,
						From_System__r.Route__c,
						From_System__r.Route__r.Name,
						From_System__r.Planner_Group__c,
						From_Sub_System__c,
						From_Sub_System__r.Name,
						From_Sub_System__r.Route__c,
						From_Sub_System__r.Route__r.Name,
						From_Sub_System__r.Planner_Group__c,
						From_Well_Event__c,
						From_Well_Event__r.Name,
						From_Well_Event__r.Route__c,
						From_Well_Event__r.Route__r.Name,
						From_Well_Event__r.Planner_Group__c
				FROM Equipment_Transfer_Form__c
				WHERE Id = :formId
		];
	}

	public static List<Equipment_Transfer_Item__c> getEquipmentTransferItems(String formId) {
		return [
				SELECT Id,
						Tag_Colour__c,
						Equipment__r.Id,
						Equipment__r.Name,
						Equipment__r.Description_of_Equipment__c,
						Equipment__r.Manufacturer__c,
						Equipment__r.Manufacturer_Serial_No__c,
						Equipment__r.Model_Number__c,
						Equipment__r.Tag_Number__c,
						Equipment__r.Location__r.Name,
						Equipment__r.Facility__r.Name,
						Equipment__r.Well_Event__r.Name,
						Equipment__r.Functional_Equipment_Level__r.Name,
						Equipment__r.Yard__r.Name,
						Equipment__r.System__r.Name,
						Equipment__r.Sub_System__r.Name
				FROM Equipment_Transfer_Item__c
				WHERE Equipment_Transfer_Form__c = :formId
            	ORDER BY Equipment__r.Name
		];
	}

	public static List<Equipment__c> getSelectedEquipment(List<String> selectedEquipments) {
		return [
				SELECT Id,
						Name,
						Equipment_Number__c,
						Description_of_Equipment__c,
						Manufacturer__c,
						Manufacturer_Serial_No__c,
						Model_Number__c,
						Tag_Number__c,
						Location__r.Name,
						Facility__r.Name,
						Well_Event__r.Name,
						Functional_Equipment_Level__r.Name,
						Yard__r.Name,
						System__r.Name,
						Sub_System__r.Name
				FROM Equipment__c
				WHERE Id IN :selectedEquipments
				ORDER BY Name
				LIMIT 50
		];
	}

	public static List<Equipment__c> getEquipmentBasedOnSearchCriteria(String userInput, List<String> selectedTransferItems) {
		return [
				SELECT Id,
						Name,
						Equipment_Number__c,
						Description_of_Equipment__c,
						Manufacturer__c,
						Manufacturer_Serial_No__c,
						Model_Number__c,
						Tag_Number__c,
						Location__r.Name,
						Facility__r.Name,
						Well_Event__r.Name,
						Functional_Equipment_Level__r.Name,
						Yard__r.Name,
						System__r.Name,
						Sub_System__r.Name
				FROM Equipment__c
				WHERE (Name LIKE :userInput
				OR Manufacturer__c LIKE :userInput
				OR Manufacturer_Serial_No__c LIKE :userInput
				OR Equipment_Number__c LIKE :userInput
				OR Model_Number__c LIKE :userInput
				OR Tag_Number__c LIKE :userInput
				OR Location__r.Name LIKE :userInput
				OR Facility__r.Name LIKE :userInput
				OR Well_Event__r.Name LIKE :userInput
				OR Functional_Equipment_Level__r.Name LIKE :userInput
				OR Yard__r.Name LIKE :userInput
				OR System__r.Name LIKE :userInput
				OR Sub_System__r.Name LIKE :userInput)
				AND Id NOT IN :selectedTransferItems
				ORDER BY Name
				LIMIT 50
		];
	}

	public static List<Equipment__c> getEquipmentBasedOnSearchCriteriaFromForm(String userInput, String flocId, List<String> selectedTransferItems) {
		return [
				SELECT Id,
						Name,
						Equipment_Number__c,
						Description_of_Equipment__c,
						Manufacturer__c,
						Manufacturer_Serial_No__c,
						Model_Number__c,
						Tag_Number__c,
						Location__r.Name,
						Facility__r.Name,
						Well_Event__r.Name,
						Functional_Equipment_Level__r.Name,
						Yard__r.Name,
						System__r.Name,
						Sub_System__r.Name
				FROM Equipment__c
				WHERE (Name LIKE :userInput
				OR Manufacturer__c LIKE :userInput
				OR Manufacturer_Serial_No__c LIKE :userInput
				OR Equipment_Number__c LIKE :userInput
				OR Model_Number__c LIKE :userInput
				OR Tag_Number__c LIKE :userInput
				OR Location__r.Name LIKE :userInput
				OR Facility__r.Name LIKE :userInput
				OR Well_Event__r.Name LIKE :userInput
				OR Functional_Equipment_Level__r.Name LIKE :userInput
				OR Yard__r.Name LIKE :userInput
				OR System__r.Name LIKE :userInput
				OR Sub_System__r.Name LIKE :userInput)
				AND (Location__c = :flocId
				OR Facility__c = :flocId
				OR Functional_Equipment_Level__c = :flocId
				OR Yard__c = :flocId
				OR System__c = :flocId
				OR Sub_System__c = :flocId
				OR Well_Event__c = :flocId)
				AND Id NOT IN :selectedTransferItems
				ORDER BY Name
				LIMIT 50
		];
	}

	public static List<Equipment__c> initialEquipmentForFromForm(String flocId, List<String> selectedTransferItems) {
		return [
				SELECT Id,
						Name,
						Equipment_Number__c,
						Description_of_Equipment__c,
						Manufacturer__c,
						Manufacturer_Serial_No__c,
						Model_Number__c,
						Tag_Number__c,
						Location__r.Name,
						Facility__r.Name,
						Well_Event__r.Name,
						Functional_Equipment_Level__r.Name,
						Yard__r.Name,
						System__r.Name,
						Sub_System__r.Name
				FROM Equipment__c
				WHERE (Location__c = :flocId
				OR Facility__c = :flocId
				OR Functional_Equipment_Level__c = :flocId
				OR Yard__c = :flocId
				OR System__c = :flocId
				OR Sub_System__c = :flocId
				OR Well_Event__c = :flocId)
				AND Id NOT IN :selectedTransferItems
				ORDER BY Name
				LIMIT 50
		];
	}

	/* DATA UPDATE FORM */
	public static Equipment__c getEquipmentForNewDataUpdateForm(String equipmentId) {
		return [
				SELECT Id,
						Name,
						Description_of_Equipment__c,
						Manufacturer__c,
						Model_Number__c,
						Manufacturer_Serial_No__c,
						Planner_Group__c,
						Tag_Number__c,
						Location__r.Name,
						Facility__r.Name,
						Well_Event__r.Name,
						Functional_Equipment_Level__r.Name,
						Yard__r.Name,
						System__r.Name,
						Sub_System__r.Name,
						Location__r.Route__r.Name,
						Facility__r.Plant_Section__r.Name,
						Well_Event__r.Route__r.Name,
						Functional_Equipment_Level__r.Route__r.Name,
						Yard__r.Route__r.Name,
						System__r.Route__r.Name,
						Sub_System__r.Route__r.Name,
						Location__r.Functional_Location__c,
						Facility__r.Functional_Location__c,
						Well_Event__r.Functional_Location__c,
						Functional_Equipment_Level__r.Functional_Location__c,
						Yard__r.Functional_Location__c,
						System__r.Functional_Location__c,
						Sub_System__r.Functional_Location__c
				FROM Equipment__c
				WHERE Id = :equipmentId
		];
	}

	public static Equipment_Correction_Form__c getDataUpdateForm(String formId) {
		return [
				SELECT
						Id,
						Name,
						Description_old__c,
						Description_new__c,
						Manufacturer_old__c,
						Manufacturer_new__c,
						Model_Number_old__c,
						Model_Number_new__c,
						Manufacturer_Serial_No_old__c,
						Manufacturer_Serial_No_new__c,
						Tag_Number_old__c,
						Tag_Number_new__c,
						Safety_Critical_Equipment__c,
						Regulatory_Equipment__c,
						Comments__c,
						Reason__c,
						Expiration_Date__c,
						Equipment_Status__c,
						Status__c,
						CreatedBy.Name,
						CreatedDate,
						LastModifiedBy.Name,
						LastModifiedDate,
						Equipment__r.Name,
						Equipment__r.Planner_Group__c,
						Equipment__r.Location__r.Name,
						Equipment__r.Facility__r.Name,
						Equipment__r.Well_Event__r.Name,
						Equipment__r.Functional_Equipment_Level__r.Name,
						Equipment__r.Yard__r.Name,
						Equipment__r.System__r.Name,
						Equipment__r.Sub_System__r.Name,
						Equipment__r.Location__r.Route__r.Name,
						Equipment__r.Facility__r.Plant_Section__r.Name,
						Equipment__r.Well_Event__r.Route__r.Name,
						Equipment__r.Functional_Equipment_Level__r.Route__r.Name,
						Equipment__r.Yard__r.Route__r.Name,
						Equipment__r.System__r.Route__r.Name,
						Equipment__r.Sub_System__r.Route__r.Name,
						Equipment__r.Functional_Location__c
				FROM Equipment_Correction_Form__c
				WHERE Id = :formId
				LIMIT 1
		];
	}
}