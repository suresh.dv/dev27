/**
 * Created by mzelina@sk.ibm.com on 29/01/2019.
 */
@IsTest
private class SPCC_CostElementDetailCtrlXTest {

    public static Final Date startDate = Date.today();
    public static Final Date endDate = Date.today() + 1;

    @testSetup static void setup() {
        SPCC_Utilities.executeTriggerCode = false;

        SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('TestProject', '12345', '','Other');
        insert ewr;

        SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('A1-F7GH56-4D-J7-K2', 'A1-F7GH56-4D-J7-K2', 'Test AFE', ewr.Id);
        insert afe;

        SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, 'Test Category',
                'Test Description',
                '6000000',
                'Test Cose Element',
                5000,
                0);
        insert costElement;


        Account vendor = SPCC_TestData.createSPCCVendor('Test Account 1');
        insert vendor;

        //Setup - Add Blanket Purchase Order with BPOLI
        SPCC_Blanket_Purchase_Order__c bpo = SPCC_TestData.createBPO('1111111111', vendor.Id, startDate, endDate);
        insert bpo;

        SPCC_BPO_Line_Item__c bpoLi1 = SPCC_TestData.createBpoLineItem(bpo.Id, '6000000', 5000, '0010', 'Test BPO Line Item');
        insert bpoLi1;

        SPCC_BPO_Line_Item__c bpoLi2 = SPCC_TestData.createBpoLineItem(bpo.Id, '6000000', 5000, '0020', 'Test BPO Line Item');
        insert bpoLi2;

        SPCC_BPO_Line_Item__c bpoLi3 = SPCC_TestData.createBpoLineItem(bpo.Id, '6000000', 5000, '0030', 'Test BPO Line Item');
        insert bpoLi3;

        SPCC_Service_Entry_Sheet__c ses1 = SPCC_TestData.createSESforBPO(bpo.Id, bpoLi1.Id, 2000, startDate, 'ses 1');
        ses1.Cost_Element__c = costElement.Id;
        insert ses1;

        SPCC_Service_Entry_Sheet__c ses2 = SPCC_TestData.createSESforBPO(bpo.Id, bpoLi2.Id, 1000, startDate, 'ses 2');
        ses2.Cost_Element__c = costElement.Id;
        insert ses2;

        SPCC_Service_Entry_Sheet__c ses3 = SPCC_TestData.createSESforBPO(bpo.Id, bpoLi3.Id, 8000, startDate, 'ses 3');
        ses3.Cost_Element__c = costElement.Id;
        insert ses3;
    }

    @IsTest
    static void testController() {

        //Setup -- Query Setup Objects
        List<SPCC_Cost_Element__c> costElement = [Select Id, Name, GL_Number__c
                                                  From SPCC_Cost_Element__c];

        User runningUser = [Select Id From User Where Id =: UserInfo.getUserId()];

        System.runAs(runningUser) {

            Test.startTest();

            PageReference pRef = Page.SPCC_CostElementDetail;
            Test.setCurrentPage(pRef);

            //Create Controller
            ApexPages.StandardController stdController = new ApexPages.StandardController(costElement[0]);
            SPCC_CostElementDetailCtrlX extCtrl = new SPCC_CostElementDetailCtrlX(stdController);

            System.assertEquals(3, extCtrl.getBpoLineItems().Size());

            Test.stopTest();
        }
    }
}