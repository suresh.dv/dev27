public class SM_RunSheetControllerExportX
{
    // ************************************************************************ //
    // ** THIS CONTROLLER EXTENSION IS USED FOR EXPORTING SAND DATA TO EXCEL ** //
    // ************************************************************************ //

    private static Integer iPageCount = 1000;
    private List<SM_Run_Sheet_Detail__c> lFullExport;

    public Map<id, String> H2SReadingsMap {get; set;}

    //////////////////////////
    // * MAIN CONSTRUCTOR * //
    //////////////////////////

    public SM_RunSheetControllerExportX(ApexPages.StandardSetController controller)
    {
        ApexPages.StandardSetController oSet = controller;
        oSet.setPageSize(iPageCount);

        // Retrieve FULL Export Data
        lFullExport = [SELECT Sand_Id__c, Sand_Run_Sheet__r.Text_Date__c, 
                            Sand_Run_Sheet__r.Operator_Phone_Number__c, 
                            Sand_Run_Sheet__r.Well_Location__r.Route__r.Name, 
                            Sand_Run_Sheet__r.Well_Location__r.Name, 
                            Sand_Run_Sheet__r.Well_Location__r.Cost_Center__c, 
                            Sand_Run_Sheet__r.Hazardous_H2S__c, 
                            Sand_Run_Sheet__r.Well_Location__r.Senior__c, Id, Name, 
                            Tank__c, Sand_Level__c, Water_Level__c, 
                            Sand_Run_Sheet__r.OIL_Prod_m3__c, Work_Order__c, 
                            Sand_Run_Sheet__r.Water_Prod_m3__c, 
                            Priority__c, Location_for_Water_Well__c, 
                            Location_for_Water_Well__r.Name, Status__c, 
                            Text_Vendor_Date_given_out__c, Location_for_Water_Tank__c, 
                            Flood_Up__c, Blow_off_location__c, Sting_Type__c, 
                            Sand_Run_Sheet__r.Text_Water_Transfer__c, Sand_Run_Sheet__r.Saved_H2s__c, 
                            Sand_Run_Sheet__r.Employee2z__c, Sand_Run_Sheet__r.RM__c, 
                            Chem_and_Rate__c, Vendor_Date_given_out__c, 
                            Text_Flowline_Push__c, Tank_Level__c, Comments__c, 
                            Text_Flood_Up__c, Sand_Run_Sheet__r.Sand_Lead__r.Name,
                            Primary_Disposal_Facility__c,
                            Primary_Disposal_Facility_Ext__c,
                            Disposal_Destination__c,
                            Primary_Vendor__c,
                            Primary_Disposal_Facility__r.Name,
                            Primary_Disposal_Facility_Ext__r.Picklist_Value__c,
                            Sand_Run_Sheet__r.Well_Location__r.Surface_Location__c,
                            Sand_Run_Sheet__r.Well_Location__r.PVR_AVGVOL_30D_SAND__c 
                            FROM SM_Run_Sheet_Detail__c 
                            WHERE Id IN :oSet.getRecords() 
                            AND Tank_Enabled__c = true 
                            ORDER BY Sand_Run_Sheet__r.Well_Location__r.Route__r.Name, 
                            Sand_Run_Sheet__r.Name, Tank__c];

        H2SReadingsMap = new Map<id,string>();
        Set<id> wellLocationSet = new Set<ID>();
        for(SM_Run_Sheet_Detail__c rec :lFullExport)
        {
            wellLocationSet.add(rec.Sand_Run_Sheet__r.Well_Location__c);
        }

        List<Location__c> wellLocationList = [select id, name, 
            (SELECT H2S_Reading__c from H2S_Readings__r order by CreatedDate DESC LIMIT 1) 
            FROM  Location__c 
            WHERE id in :wellLocationSet];
        for(Location__c rec :wellLocationList)
        {
            if(rec.H2S_readings__r.size()> 0 )
            {
                H2SReadingsMap.put(rec.id, rec.H2S_readings__r[0].H2S_Reading__c);
            }
            else
            {
                H2SReadingsMap.put(rec.id, '');
            }
            
        }
    }

    public String sExcelHeader
    {
        get
        {
            String sFullHeader = '';

            sFullHeader += '<?xml version="1.0"?>';
            sFullHeader += '<?mso-application progid="Excel.Sheet"?>';

            return sFullHeader;
        }
    }

    ///////////////////////////
    // * GET + SET METHODS * //
    ///////////////////////////

    public List<SM_Run_Sheet_Detail__c> getFullExport()
    {
        return lFullExport;
    }

/*
KK 2015-08-07 - the following 2 methods are currently not being used.
    public PageReference ExportList()
    {
      return new Pagereference('/apex/SM_ExportRunSheet');
    }    
 
    // KK 2015-08-07 send the user back to the List View page.  By only appending the SObjectType's prefix, it goes to the List View.
    // To go to the tab view, append '/o' after the SObjectType's prefix, i.e. '/001' versus '/001/o' 
    public String getRedirectToListLink()
    {
        Schema.DescribeSObjectResult result = SM_Run_Sheet__c.SObjectType.getDescribe();
        return '/' + result.getKeyPrefix();
    }
*/  
}