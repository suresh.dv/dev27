/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_ErrorPrettifyService
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_ErrorPrettifyServiceTest {

    @IsTest
    static void prettifyErrorMessage_withoutParam() {
        String errorMessage = null;

        Test.startTest();
        errorMessage = HOG_ErrorPrettifyService.prettifyErrorMessage(errorMessage);
        Test.stopTest();

        System.assertEquals(null, errorMessage);
    }

    @IsTest
    static void prettifyErrorMessage_empty() {
        String errorMessage = '';

        Test.startTest();
        errorMessage = HOG_ErrorPrettifyService.prettifyErrorMessage(errorMessage);
        Test.stopTest();

        System.assertEquals('', errorMessage);
    }

    @IsTest
    static void prettifyErrorMessage_withoutAnythingToRemove() {
        String errorMessage = 'SomeError';
        String expectedMessage = String.format(
                Label.HOG_Generic_Error_Message,
                new List<String> {errorMessage}
        );
        String prettifiedMessage;

        Test.startTest();
        prettifiedMessage = HOG_ErrorPrettifyService.prettifyErrorMessage(errorMessage);
        Test.stopTest();

        System.assertEquals(expectedMessage, prettifiedMessage);
    }

    @IsTest
    static void prettifyErrorMessage_customValError() {
        String errorMessage = 'SomeError';
        String expectedMessage = String.format(
                Label.HOG_Generic_Error_Message,
                new List<String> {errorMessage}
        );
        String prettifiedMessage;

        Test.startTest();
        prettifiedMessage = HOG_ErrorPrettifyService.prettifyErrorMessage(
                HOG_ErrorPrettifyService.CUSTOM_VALIDATION_ERROR + errorMessage
        );
        Test.stopTest();

        System.assertEquals(expectedMessage, prettifiedMessage);
    }

    @IsTest
    static void prettifyErrorMessage_fieldMissing() {
        String errorMessage = 'SomeError';
        String expectedMessage = String.format(
                Label.HOG_Generic_Error_Message,
                new List<String> {errorMessage}
        );
        String prettifiedMessage;

        Test.startTest();
        prettifiedMessage = HOG_ErrorPrettifyService.prettifyErrorMessage(
                HOG_ErrorPrettifyService.FIELD_MISSING + errorMessage
        );
        Test.stopTest();

        System.assertEquals(expectedMessage, prettifiedMessage);
    }

    @IsTest
    static void prettifyErrorMessage_sObjectException() {
        String errorMessage = 'SomeError';
        String expectedMessage = String.format(
                Label.HOG_Generic_Error_Message,
                new List<String> {errorMessage}
        );
        String prettifiedMessage;

        Test.startTest();
        prettifiedMessage = HOG_ErrorPrettifyService.prettifyErrorMessage(
                HOG_ErrorPrettifyService.SOBJECT_EXCEPTION + errorMessage
        );
        Test.stopTest();

        System.assertEquals(expectedMessage, prettifiedMessage);
    }

}