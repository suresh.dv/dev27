/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Controller for Service Entry Sheet List Page
History:        ssd 09.26.2016 - Initial Revision
**************************************************************************************************/
public with sharing class SPCC_ServiceEntrySheetListCtrl extends DynamicListController{
    
    /**************
    ** Variables **
    **************/
    //Filter
    public FilterWrapper pageFilter {get; private set;}
    
    //Records
    public List<ServiceEntrySheetWrapper> currentPageServiceEntrySheets {get; private set;}

    //User Security
    public SPCC_Utilities.UserPermissions userPermissions {get; private set;}
    public SPCC_Utilities.UserDetails userDetails {get; private set;}

    //Dependent Picklist Options
    public Map<Id, List<SelectOption>> afeOptionsPerEwr {get; private set;}
    public Map<Id, List<SelectOption>> vendorOptionsPerEwr {get; private set;}

    //Url Params
    private UrlParams params;
    
    /*****************
    ** Constructors **
    *****************/
    public SPCC_ServiceEntrySheetListCtrl() {
        super('Select Id, Name, Amount__c, Date_Entered__c, Purchase_Order__c, Purchase_Order__r.Purchase_Order_Number__c, ' +
              '       Purchase_Order__r.Engineering_Work_Request__c, Purchase_Order__r.Engineering_Work_Request__r.Project_Name__c, ' +
              '       Purchase_Order__r.Authorization_For_Expenditure__c, Purchase_Order__r.Authorization_For_Expenditure__r.Name, ' +
              '       Purchase_Order__r.Cost_Code__c, Purchase_Order__r.Cost_Code__r.Cost_Code_Number__c, ' +
              '       Purchase_Order__r.Vendor_Account__r.Name ' +
              'From SPCC_Service_Entry_Sheet__c');

        //User Security
        this.userPermissions = new SPCC_Utilities.UserPermissions();
        this.userDetails = new SPCC_Utilities.UserDetails();

        //Dependent Picklist Initialization
        populateDependentOptions();

        //Apply Url Params
        getUrlParams();
        applyUrlParams();

        //Load Initial Data
        refreshData();
    }

    /************************
    ** Getters and Setters **
    ************************/
    public List<SelectOption> ewrOptions {
        get {
            if(ewrOptions == null || ewrOptions.isEmpty()) {
                ewrOptions = new List<SelectOption>();
				
                if(!userPermissions.isEPC) {
                    ewrOptions.add(new SelectOption('', '- Any -'));
                }

                List<SPCC_Engineering_Work_Request__c> ewrList = [Select Id, Name, Project_Name__c
                                                                  From SPCC_Engineering_Work_Request__c];
                for(SPCC_Engineering_Work_Request__c ewr : ewrList) {
                    ewrOptions.add(new SelectOption(ewr.Id, ewr.Name));
                }
            }
            return ewrOptions;
        }
        private set;
    }

    public List<SelectOption> afeOptions {
        get {
            if(!String.isBlank(pageFilter.ewrId)) {
                if(afeOptions != null) 
                    afeOptions.clear();
                else
                    afeOptions = new List<SelectOption>();

                afeOptions.add(new SelectOption('', '- Any -'));
                if(afeOptionsPerEwr.get(pageFilter.ewrId) != null)
                    afeOptions.addAll(afeOptionsPerEwr.get(pageFilter.ewrId));
            }
            return afeOptions;
        }
        private set;
    }

    public List<SelectOption> glOptions {
        get {
            if(glOptions == null || glOptions.isEmpty()) {
                glOptions = new List<SelectOption>();

                List<SPCC_GL_Code_Entry__mdt> costElementConfigItemList = [Select DeveloperName, MasterLabel, Category__c, 
                                                                                  GL_Number__c
                                                                           From SPCC_GL_Code_Entry__mdt
                                                                           Where Category__c <> :SPCC_Utilities.GL_CODE_CATEGORY_OTHER];
                glOptions.add(new SelectOption('', '- Any -'));
                for(SPCC_GL_Code_Entry__mdt costElementConfigItem : costElementConfigItemList) {
                    glOptions.add(new SelectOption(costElementConfigItem.GL_Number__c, costElementConfigItem.MasterLabel));
                }
            }
            return glOptions;
        }
        private set;
    }

    public List<SelectOption> costCodeOptions {
        get {
            if(costCodeOptions == null || costCodeOptions.isEmpty()) {
                costCodeOptions = new List<SelectOption>();

                costCodeOptions.add(new SelectOption('', '- Any -'));

                //TODO: Add cost code options once available
            }
            return costCodeOptions;
        }
        private set;
    }

    public List<SelectOption> vendorOptions {
        get {
            if(!String.isBlank(pageFilter.ewrId)) {
                if(vendorOptions != null) 
                    vendorOptions.clear();
                else
                    vendorOptions = new List<SelectOption>();

                if(!userPermissions.isEPC) {
                    vendorOptions.add(new SelectOption('', '- Any -'));
                    if(vendorOptionsPerEwr.get(pageFilter.ewrId) != null)
                        vendorOptions.addAll(vendorOptionsPerEwr.get(pageFilter.ewrId));
                }
            }
            return vendorOptions;
        }
        private set;
    }
    
    /************
    ** Actions **
    *************/
    public override void reloadpage() {
        if(currentPageServiceEntrySheets != null){
            currentPageServiceEntrySheets.clear();
        }
        currentPageServiceEntrySheets = getCurrentPageServiceEntrySheets();    
    }
    
    public override void first() {
        super.first();
        reloadpage();
    }

    public override void previous() {
        super.previous();
        reloadpage();
    }

    public override void next() {
        super.next();
        reloadpage();
    }

    public override void last() {
        super.last();
        reloadpage();
    }

    public PageReference populateDependentOptionsAction() {
        return null;
    }

    public PageReference refreshData() {
        try {
            this.whereClause = pageFilter.createSOQLFilter();
            System.debug('whereClause: ' + this.whereClause);
            query();
            reloadpage();
        } catch (QueryException ex) {
            System.debug('RefreshData->Exception: ' + ex.getMessage());
            SPCC_Utilities.logErrorOnPage(ex);
        }

        return null;
    }

    public PageReference clearFilter() {
        pageFilter.clear();
        return null;
    }

    public PageReference newServiceEntrySheetRedirect() {
        return new PageReference('/apex/SPCC_ServiceEntrySheetCreate?returl=' + ApexPages.currentPage().getURL());
    }

    /**********************
    ** Utility Functions **
    **********************/
    private void getUrlParams() {
        params = new UrlParams();
        params.retUrl = ApexPages.currentPage().getParameters().get('retUrl');
        params.filter = ApexPages.currentPage().getParameters().get('filter');
    }

    private void applyUrlParams() {
        this.pageFilter = (!String.isBlank(params.filter)) ? 
            (FilterWrapper) JSON.deserialize(params.filter, FilterWrapper.class) : new FilterWrapper();
    }

    private List<ServiceEntrySheetWrapper> getCurrentPageServiceEntrySheets() {
        List<ServiceEntrySheetWrapper> result = new List<ServiceEntrySheetWrapper>();
        for(SPCC_Service_Entry_Sheet__c ses : (List<SPCC_Service_Entry_Sheet__c>) getRecords()) {
            result.add(new ServiceEntrySheetWrapper(ses));
        }
        return result;
    }

    private void populateDependentOptions() {
        //Populate AFE Options
        afeOptionsPerEwr = new Map<Id, List<SelectOption>>();
        List<SPCC_Authorization_For_Expenditure__c> afeList = [Select Id, Name, Engineering_Work_Request__c
                                                               From SPCC_Authorization_For_Expenditure__c];
        for(SPCC_Authorization_For_Expenditure__c afe : afeList) {
            if(afeOptionsPerEwr.containsKey(afe.Engineering_Work_Request__c)) {
                afeOptionsPerEwr.get(afe.Engineering_Work_Request__c).add(new SelectOption(afe.Id, afe.Name));
            } else {
                afeOptionsPerEwr.put(afe.Engineering_Work_Request__c, new List<SelectOption>{new SelectOption(afe.Id, afe.Name)});
            }
        }

        //Populate Vendor Options
        vendorOptionsPerEwr = new Map<Id, List<SelectOption>>();
        List<SPCC_Vendor_Account_Assignment__c> vaaList = [Select Id, Name, Engineering_Work_Request__c, 
                                                                  Vendor_Account__c, Vendor_Account__r.Name
                                                           From SPCC_Vendor_Account_Assignment__c];
        for(SPCC_Vendor_Account_Assignment__c vaa : vaaList) {
            if(vendorOptionsPerEwr.containsKey(vaa.Engineering_Work_Request__c)) {
                vendorOptionsPerEwr.get(vaa.Engineering_Work_Request__c).add(new SelectOption(vaa.Vendor_Account__c, vaa.Vendor_Account__r.Name));
            } else {
                vendorOptionsPerEwr.put(vaa.Engineering_Work_Request__c, new List<SelectOption>{new SelectOption(vaa.Vendor_Account__c, vaa.Vendor_Account__r.Name)});
            }
        }
    }
    
    /********************
    ** Wrapper Classes **
    ********************/
    public class FilterWrapper {
        public String ewrId {get; set;}
        public String afeId {get; set;}
        public String glNumber {get; set;}
        public String costCodeNumber {get; set;}
        public String poNumber {get; set;}
        public String vendorId {get; set;}
        
        public FilterWrapper() {
            clear();
        }
        
        public String createSOQLFilter() {
            String filteredSOQL = ' Name != null';
            filteredSOQL += (String.isEmpty(ewrId)) ? '' : 
                ' And Purchase_Order__r.Engineering_Work_Request__c = \'' + ewrId + '\'';
            filteredSOQL += (String.isEmpty(afeId)) ? '' : 
                ' And Purchase_Order__r.Authorization_for_Expenditure__c = \'' + afeId + '\'';
            filteredSOQL += (String.isEmpty(glNumber)) ? '' : 
                ' And Line_Item__r.Cost_Element__r.GL_Number__c = \'' + glNumber + '\'';
            filteredSOQL += (String.isEmpty(costCodeNumber)) ? '' : 
                ' And Purchase_Order__r.Cost_Code__r.Cost_Code_Number__c = \'' + costCodeNumber + '\'';
            filteredSOQL += (String.isEmpty(poNumber)) ? '' : 
                ' And Line_Item__r.Purchase_Order__r.Purchase_Order_Number__c Like \'%' + String.escapeSingleQuotes(poNumber) + '%\'';
            filteredSOQL += (String.isEmpty(vendorId)) ? '' : 
                ' And Purchase_Order__r.Vendor_Account__c = \'' + vendorId + '\'';
            return filteredSOQL;
        }

        public void clear() {
            ewrId = '';
            afeId = '';
            glNumber = '';
            costCodeNumber = '';
            poNumber = '';
            vendorId = '';
        }
    }

    public class ServiceEntrySheetWrapper {
        public SPCC_Service_Entry_Sheet__c serviceEntrySheet {get; private set;}

        public ServiceEntrySheetWrapper(SPCC_Service_Entry_Sheet__c serviceEntrySheet) {
            this.serviceEntrySheet = serviceEntrySheet;
        }
    }

    private class UrlParams {
        String retUrl;
        String filter;
    }
}