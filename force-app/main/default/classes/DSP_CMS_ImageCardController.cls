global virtual with sharing class DSP_CMS_ImageCardController extends cms.ContentTemplateController
{
    // Edit page constructor
    global DSP_CMS_ImageCardController(cms.CreateContentController cc)
    {
        super(cc);
    }
    
    global DSP_CMS_ImageCardController(cms.GenerateContent cc){
        super(cc);
    }
    
    // Constructor
    global DSP_CMS_ImageCardController()
    {}
    
    global virtual override String getHTML()
    {
        return '';
    }
    
    public String title
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('title')) ?  getProperty('title') : '';
            return returnValue;
        }
        set;
    }
    
    public String image
    {
        get
        {
            String returnValue = String.isNotBlank(getProperty('image')) ?  getProperty('image') : '';
            return returnValue;
        }
        set;
    }
    
    public String getImageCardHTML()
    {        
        String html = '';
        
        html += '<div class="col s12 m12 l12">';
        html +=     '<div class="card extra-padding">';
        html +=         '<div class="square"><div class="center-align"><i class="mdi-action-question-answer"></i></div></div>';
        html +=         '<div class="card-title-icon">';
        html +=             '<span class="title-smallcaps card-title title-text">'+title+'</span>';
        html +=         '</div>';
        html +=         '<div class="center-align card-content">';
        html +=             '<div class="col s12 m12 l12">';
        html +=                 '<img src="'+image+'" />';
        html +=             '</div>';
        html +=             '<div class="col s12 m12 l12">';
        html +=                 '<div class="section">';
        html +=                     '<a target="_blank" href="'+image+'" class="btn full-view">View Full</a>';
        html +=                 '</div>';
        html +=             '</div>';
        html +=         '</div>';
        html +=     '</div>';
        html += '</div>';
        
        return html;
    }
}