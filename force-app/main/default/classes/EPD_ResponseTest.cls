/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_Response
History:        jschn 2019-07-11 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_ResponseTest {

    @IsTest
    static void constructor_null() {
        EPD_Response result;
        EPD_FormStructure structure;
        Boolean expectedSuccessFlag = true;

        result = new EPD_Response(structure);

        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(null, result.formStructure);
    }

    @IsTest
    static void constructor_structure() {
        EPD_Response result;
        EPD_FormStructure structure = new EPD_FormStructure();
        Boolean expectedSuccessFlag = true;

        result = new EPD_Response(structure);

        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(structure, result.formStructure);
    }

    @IsTest
    static void constructor_exception() {
        EPD_Response result;
        String exceptionMessage = 'exception';
        Boolean expectedSuccessFlag = false;

        result = new EPD_Response(new TestException(exceptionMessage));

        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(null, result.formStructure);
        System.assertEquals(1, result.errors.size());
        System.assert(result.errors.get(0).contains(exceptionMessage));
    }

    @IsTest
    static void constructor_HOG_Exception() {
        EPD_Response result;
        String exceptionMessage = 'exception';
        Boolean expectedSuccessFlag = false;

        result = new EPD_Response(new HOG_Exception(exceptionMessage));

        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(null, result.formStructure);
        System.assertEquals(1, result.errors.size());
        System.assertEquals(exceptionMessage, result.errors.get(0));
    }

    @IsTest
    static void constructor_stringMessage() {
        EPD_Response result;
        String exceptionMessage = 'exception';
        Boolean expectedSuccessFlag = false;

        result = new EPD_Response(exceptionMessage);

        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(null, result.formStructure);
        System.assertEquals(1, result.errors.size());
        System.assertEquals(exceptionMessage, result.errors.get(0));
    }

    @IsTest
    static void addError() {
        EPD_Response result;
        String exceptionMessage1 = 'exception1';
        String exceptionMessage2 = 'exception2';
        String exceptionMessage3 = 'exception3';
        Boolean expectedSuccessFlag = false;
        EPD_FormStructure structure;

        result = new EPD_Response(structure);
        result.addError(new TestException(exceptionMessage1));
        result.addError(new HOG_Exception(exceptionMessage2));
        result.addError(exceptionMessage3);

        System.assertEquals(expectedSuccessFlag, result.success);
        System.assertEquals(null, result.formStructure);
        System.assertEquals(3, result.errors.size());
        System.assert(result.errors.get(0).contains(exceptionMessage1));
        System.assertEquals(exceptionMessage2, result.errors.get(1));
        System.assertEquals(exceptionMessage3, result.errors.get(2));
    }

    public class TestException extends Exception {}

}