/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 11/21/2019   
 */

public class Equipment_Forms_Endpoint {


	@AuraEnabled
	public static void insertAddEquipmentForm(
			String flocId,
			Equipment_Missing_Form__c addForm,
			String serialPlateBase64Data,
			String serialPlateName,
			String attachmentBase64Data,
			String attachmentFileName) {

		Savepoint sp = Database.setSavepoint();
		try {
			Equipment_Forms_EndpointService.insertAddEquipmentForm(
					flocId,
					addForm,
					serialPlateBase64Data,
					serialPlateName,
					attachmentBase64Data,
					attachmentFileName
			);

		} catch (Exception ex) {
			Database.rollback(sp);
			throw new HOG_Exception(ex.getMessage());
		}
	}


	@AuraEnabled
	public static void updateAddEquipmentForm(
			Equipment_Missing_Form__c addForm,
			Object handleUploads) {

		try {
			Equipment_Forms_EndpointService.updateAddEquipmentForm(addForm, handleUploads);
		} catch (Exception ex) {
			throw new HOG_Exception(ex.getMessage());
		}
	}

	/**
	 * Method to Load Add Equipment Form based on record Id
	 *
	 * @param formId
	 *
	 * @return
	 */
	@AuraEnabled
	public static Equipment_AddForm  loadAddEquipmentForm(String formId) {
		try {
			return Equipment_Forms_EndpointService.loadAddEquipmentForm(formId);
		} catch (Exception ex) {
			throw new HOG_Exception(EquipmentUtilities.ERROR_ON_LOAD_MESSAGE);
		}
	}

	@AuraEnabled
	public static HOG_SObject_Information getFloc(String flockId) {
		try {
			return Equipment_Forms_EndpointService.getFloc(flockId);
		} catch (Exception ex) {
			throw new HOG_Exception(EquipmentUtilities.ERROR_ON_LOAD_MESSAGE);
		}
	}

	@AuraEnabled
	public static void rejectAddEquipmentForm(String formId, String rejectionReason) {
		try {
			Equipment_Forms_EndpointService.rejectAddEquipmentForm(formId, rejectionReason);
		} catch (Exception ex) {
			throw new HOG_Exception(ex.getMessage());
		}
	}

	@AuraEnabled
	public static void processAddEquipmentForm(String formId) {
		try {
		Equipment_Forms_EndpointService.processAddEquipmentForm(formId);
		} catch (Exception ex) {
			throw new HOG_Exception(ex.getMessage());
		}
	}


	@AuraEnabled
	public static List<Object> getFLOCKEquipment(String flockId) {
		return Equipment_Forms_EndpointService.getFLOCKEquipment(flockId);
	}

	@AuraEnabled
	public static List<Equipment_Hierarchy_SlaveEquipment> somethingMore(String equipmentId, String flockId) {
		return Equipment_Forms_EndpointService.getSlaveEquipment(equipmentId, flockId);
	}

	@AuraEnabled
	public static List<Object> something(List<Equipment__c> equipmentList) {
		return Equipment_Forms_EndpointService.buildMasterEquipment(equipmentList);
	}

	@AuraEnabled
	public static String insertTransferFormEquipmentForm(
			String fromSource,
			String toDestination,
			Equipment_Transfer_Form__c transferForm,
			List<Object> itemTransfer) {
		String message = '';


		try {

			message = Equipment_Forms_EndpointService.insertTransferFormEquipmentForm(fromSource, toDestination, transferForm, itemTransfer);

		} catch (Exception ex) {
			message = ex.getMessage();
			throw new HOG_Exception(ex.getMessage());
		}

		return message;
	}


	@AuraEnabled
	public static String updateTransferEquipmentForm(
			Equipment_Transfer_Form__c transferForm,
			Boolean isFromForm,
			String destination,
			List<String> itemsToRemove,
			List<Object> equipmentItems,
			List<String> filesToRemove,
			List<String> attachmentsToRemove,
			List<Object> itemTransfer,
			List<Object> itemFilesToInsert) {

		String message = '';

		try {

			message = Equipment_Forms_EndpointService.updateTransferEquipmentForm(
					transferForm,
					isFromForm,
					destination,
					itemsToRemove,
					equipmentItems,
					filesToRemove,
					attachmentsToRemove,
					itemTransfer,
					itemFilesToInsert);

		} catch (Exception ex) {
			message = ex.getMessage();
			HOG_ExceptionUtils.throwError(ex.getMessage());
		}

		return message;
	}

	@AuraEnabled
	public static Equipment_TransferForm loadTransferForm(String formId) {
		return Equipment_Forms_EndpointService.loadTransferForm(formId);
	}


	@AuraEnabled
	public static List<Equipment_TransferForm_Item> loadEquipmentTransferItems(String formId) {
		return Equipment_Forms_EndpointService.loadEquipmentTransferItems(formId);
	}

	@AuraEnabled
	public static List<Equipment__c> loadSelectedEquipments(List<String> selectedEquipments) {
		return Equipment_Forms_QuerySelector.getSelectedEquipment(selectedEquipments);
	}


	@AuraEnabled
	public static List<Equipment__c> loadEquipments(String userInput, Boolean isFromForm, String flocId, Boolean getInitialEquipment, List<String> selectedTransferItems) {
		return Equipment_Forms_EndpointService.loadEquipments(userInput, isFromForm, flocId, getInitialEquipment, selectedTransferItems);
	}

	//todo looks like to upper one can be enough
	@AuraEnabled
	public static List<Equipment__c> getEquipmentForFromForm(String flocId, List<String> selectedTransferItems) {
		return Equipment_Forms_QuerySelector.initialEquipmentForFromForm(flocId, selectedTransferItems);
	}



	@AuraEnabled
	public static Equipment__c loadEquipmentForNewDataUpdateForm(String equipmentId) {
		return Equipment_Forms_QuerySelector.getEquipmentForNewDataUpdateForm(equipmentId);
	}

	@AuraEnabled
	public static String insertNewDataUpdateForm(
			String equipmentId,
			Equipment_Correction_Form__c correctionForm,
			String attachmentBase64Data,
			String attachmentFileName) {

		String message = '';
		Savepoint sp = Database.setSavepoint();
		try {

			message = Equipment_Forms_EndpointService.insertNewDataUpdateForm(equipmentId, correctionForm, attachmentBase64Data, attachmentFileName);

		} catch (Exception ex) {
			message = ex.getMessage();
			Database.rollback(sp);
			throw new HOG_Exception(ex.getMessage());
		}

		return message;
	}

	@AuraEnabled
	public static Boolean updateDataUpdateForm(
			Equipment_Correction_Form__c correctionForm,
			Object handleUploads) {

		Boolean formWasUpdated = false;

		try {

			formWasUpdated = Equipment_Forms_EndpointService.updateDataUpdateForm(correctionForm, handleUploads);

		} catch (Exception ex) {
			formWasUpdated = false;
			HOG_ExceptionUtils.throwError(ex.getMessage());
		}
		System.debug('Show me return update ' + formWasUpdated);
		return formWasUpdated;
	}

	@AuraEnabled
	public static Equipment_CorrectionForm loadCorrectionForm(String formId) {
		return Equipment_Forms_EndpointService.loadCorrectionForm(formId);
	}

	@AuraEnabled
	public static void rejectCorrectionEquipmentForm(String formId, String rejectionReason) {
		Equipment_Forms_EndpointService.rejectCorrectionEquipmentForm(formId, rejectionReason);
	}

	@AuraEnabled
	public static void processCorrectionEquipmentForm(String formId) {
		Equipment_Forms_EndpointService.processCorrectionEquipmentForm(formId);
	}



	@AuraEnabled
	public static Boolean getUserPermission() {
		EquipmentUtilities.UserPermissions userPermissions = new EquipmentUtilities.UserPermissions();
		return userPermissions.isHogAdmin || userPermissions.isSystemAdmin || userPermissions.isSapSupportLead;
	}

	@AuraEnabled
	public static Date getExpirationDate() {
		HOG_Equipment_Request_Configuration__c settings = HOG_Equipment_Request_Configuration__c.getInstance();
		Date expirationDate;

		if (settings.Equipment_Request_Expiration_Date_Days__c == null) {
			expirationDate = Date.today().addDays(60);
		} else {
			expirationDate = Date.today().addDays(Integer.valueOf(settings.Equipment_Request_Expiration_Date_Days__c));
		}

		return expirationDate;
	}


	@AuraEnabled
	public static HOG_SObject_Information loadFLOC(String flockId) {
		return Equipment_Forms_EndpointService.loadFLOC(flockId);
	}


	@AuraEnabled
	public static Equipment_ActiveForms equipmentFormsForWorkOrder(String flocId) {
		return Equipment_Forms_EndpointService.equipmentFormsForWorkOrder(flocId);
	}

	@AuraEnabled
	public static HOG_Maintenance_Servicing_Form__c getRecord(Id recordId) {
		/*
		TODO:
			Move to DAO Layer
		 */
		return [
				SELECT Name, Well_Event__c, Yard__c, Functional_Equipment_Level__c, Sub_System__c, System__c,
						Location__c, Facility__c, Operating_Field_AMU__c, VTT_Activities_Count__c, VTT_Activities_New_Count__c
				FROM HOG_Maintenance_Servicing_Form__c
				WHERE Id = :recordId
		];
	}

}