@isTest
public class MyHuskyCommunityLoginControllerTest {

    @isTest
    static void testConstructor() {
        PageReference pageRef = new PageReference('https://huskyenergy.force.com/RetailerPortal');
        Test.setCurrentPage(pageRef);

        MyHuskyCommunityLoginController communityLoginController = new MyHuskyCommunityLoginController();
        System.assertEquals(UserInfo.getOrganizationId(), communityLoginController.orgId);
        System.assertEquals('/apex/LOM_Dealer_Dashboard?sfdc.tabName=01rG0000000iiBD', communityLoginController.startURL);
    }

    @isTest
    static void testSiteUrl() {
        PageReference pageRef = new PageReference('https://huskyenergy.force.com/RetailerPortal');
        Test.setCurrentPage(pageRef);

        MyHuskyCommunityLoginController communityLoginController = new MyHuskyCommunityLoginController();
        String siteUrl = communityLoginController.getEncodedSiteUrl();

        System.assertNotEquals(null, siteUrl);
    }

    @isTest
    static void testStartUrl() {
        PageReference pageRef = new PageReference('https://huskyenergy.force.com/RetailerPortal');
        Test.setCurrentPage(pageRef);

        MyHuskyCommunityLoginController communityLoginController = new MyHuskyCommunityLoginController();
        String startUrl = communityLoginController.getEncodedStartUrl();

        System.assertNotEquals(null, startUrl);
    }   

    @isTest
    static void testLogin() {
        PageReference pageRef = new PageReference('https://huskyenergy.force.com/RetailerPortal');
        Test.setCurrentPage(pageRef);

        MyHuskyCommunityLoginController communityLoginController = new MyHuskyCommunityLoginController();
        communityLoginController.login();        
    } 

}