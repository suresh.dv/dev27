/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Wrapper class that holds information for VTT Activity List View Search Criteria.
Test Class:     VTT_WOA_SearchCriteriaTest
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_SearchCriteria {

    @AuraEnabled
    public Date scheduledFromFilter;

    @AuraEnabled
    public Date scheduledToFilter;

    @AuraEnabled
    public String routeFilter;

    @AuraEnabled
    public String filterName;

    @AuraEnabled
    public Boolean hideCompletedFilter;

    @AuraEnabled
    public String amuFilter;

    @AuraEnabled
    public String vendorFilter;

    @AuraEnabled
    public String tradesmanFilter;

    @AuraEnabled
    public String plannerGroupFilter;

    @AuraEnabled
    public List<String> activityMultiStatusFilter;

    public static VTT_WOA_AssignmentDAO assignmentSelector = new VTT_WOA_AssignmentDAOImpl();

    private static final String ANY_VALUE_KEY = '1';
    private static final String LOGICAL_AND = ' AND ';
    private static final String LOGICAL_OR = ' OR ';
    private static final String EQUALS = ' = ';
    private static final String NOT_EQUALS = ' <> ';
    private static final String EQUALS_MORE = ' >= ';
    private static final String EQUALS_LESS = ' <= ';
    private static final String IN_SIGN = ' IN ';
    private static final String IN_CONDITION_BASE = '(\'\'{0}\'\')';
    private static final String INNER_CONDITION_BASE = LOGICAL_AND + '({0})';
    private static final String LIKE_CONDITION_BASE = '{0} LIKE \'%{1}%\'';
    private static final String NULL_SIGN = 'NULL';
    private static final String NAME_CONDITION = 'Name != null';
    private static final String DELETE_CONDITION = 'SAP_Deleted__c = false';
    private static final String STATUS_CONDITION = 'Status__c <> \'' + VTT_Utilities.ACTIVITY_STATUS_CANCELLED + '\'';
    private static final String OUTER_WHERE_CLAUSE = LOGICAL_AND + '(RecordType.DeveloperName = \'Maintenance_Servicing\'' +
                                                     LOGICAL_OR + 'RecordType.DeveloperName = \'Well_Site_Construction\')';
    private static final String WHERE_CLAUSE_BASE = 'WHERE ' + NAME_CONDITION
            + LOGICAL_AND + DELETE_CONDITION
            + LOGICAL_AND + STATUS_CONDITION
            + LOGICAL_AND +  VTT_Utilities.ACTIVITIES_TO_BE_SHOWN;

    private static final String SCH_START_DATE_BASE = 'DAY_ONLY(convertTimezone(Scheduled_Start_Date__c))';

    private static final String FIELD_WO_PLANNER_GROUP = 'Maintenance_Work_Order__r.Planner_Group__c';
    private static final String FIELD_WOA_VENDOR = 'Assigned_Vendor__c';
    private static final String FIELD_WOA_NUM_OF_ASSIGNEES = 'Number_of_assigned_people__c';
    private static final String FIELD_WOA_ID = 'Id';
    private static final String FIELD_WOA_STATUS = 'Status__c';
    private static final String FIELD_WO_PLANT_SECTION = 'Maintenance_Work_Order__r.Plant_Section__c';
    private static final String FIELD_WO_AMU = 'Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c';
    private static final List<String> FIELDS_NAME_FILTER = new List<String> {
            'Name',
            'Assigned_Text__c',
            'Functional_Location_Description_SAP__c',
            'Maintenance_Work_Order__r.Name',
            'Location_Name__c'
    };

    /**
     * Returns Query WOA sort order for list view
     *
     * @return String
     */
    public String getActivitySort() {
        return VTT_Utilities.WOA_SORT_ORDER;
    }

    /**
     * Returns Query outer Where clause for list view
     *
     * @return String
     */
    public String buildOuterWhereClause() {
        return OUTER_WHERE_CLAUSE;
    }

    /**
     * Builds and returns Query inner Where clause for list view
     *
     * @return String
     */
    public String buildInnerWhereClause() {
        String whereClause = WHERE_CLAUSE_BASE;

        whereClause += appendPlannerGroupFilter();
        whereClause += appendVendorFilter();
        whereClause += appendTradesmanFilter();
        whereClause += appendRouteFilter();
        whereClause += appendAMUFilter();
        whereClause += appendScheduledFromFilter();
        whereClause += appendScheduledToFilter();
        whereClause += appendNameFilter();
        whereClause += appendCompletedFilter();
        whereClause += appendMultiStatusFilter();

        return whereClause;
    }

    /**
     * Builds AND [FIELD] <> '[VALUE]' query condition String
     *
     * @param field
     * @param value
     *
     * @return String
     */
    private String buildAndNotEqualsCondition(String field, String value) {
        return LOGICAL_AND + field + NOT_EQUALS + '\'' + String.escapeSingleQuotes(value) + '\'';
    }

    /**
     * Builds AND [FIELD] = '[VALUE]' query condition
     *
     * @param field
     * @param value
     *
     * @return String
     */
    private String buildAndEqualsCondition(String field, String value) {
        return LOGICAL_AND + field + EQUALS + '\'' + String.escapeSingleQuotes(value) + '\'';
    }

    /**
     * Builds AND [FIELD] = [VALUE] query condition
     *
     * @param field
     * @param value
     *
     * @return String
     */
    private String buildAndEqualsCondition(String field, Integer value) {
        return LOGICAL_AND + field + EQUALS + value;
    }

    /**
     * Builds AND [FIELD] = NULL query condition
     *
     * @param field
     *
     * @return String
     */
    private String buildAndNullCondition(String field) {
        return LOGICAL_AND + field + EQUALS + NULL_SIGN;
    }

    /**
     * Builds AND [FIELD] IN ('[VALUES]') query condition
     *
     * @param field
     * @param values
     *
     * @return String
     */
    private String buildInCondition(String field, List<String> values) {
        return LOGICAL_AND + field + IN_SIGN + String.format(
                IN_CONDITION_BASE,
                new List<String> {
                        String.join(values, '\', \'')
                }
        );
    }

    /**
     * Builds AND DAY_ONLY(convertTimezone(Scheduled_Start_Date__c)) [SIGN] [VALUE] query condition
     *
     * @param sign
     * @param value
     *
     * @return String
     */
    private String buildQueryDateString(String sign, String value) {
        return LOGICAL_AND + SCH_START_DATE_BASE + sign + value;
    }

    /**
     * Builds Date string in YYYY-MM-DD format from Date
     *
     * @param dt
     *
     * @return String
     */
    private String buildDateString(Date dt) {
        return dt.year() + '-' + get2NumericalValue(dt.month()) + '-' + get2NumericalValue(dt.day());
    }

    /**
     * Will return String in XX format from number.
     *
     * @param value
     *
     * @return String
     */
    private String get2NumericalValue(Integer value) {
        return value < 10 ?
                '0' + String.valueOf(value) :
                String.valueOf(value);
    }

    /**
     * Builds Name Filter based on provided value
     *
     * @param value
     *
     * @return String
     */
    private String buildNameFilter(String value) {
        return INNER_CONDITION_BASE.replace('{0}', buildLikeConditions(value));
    }

    /**
     * Builds like condition for Name filter based on value
     *
     * @param value
     *
     * @return String
     */
    private String buildLikeConditions(String value) {
        String likeConditions = '';

        String escapedValue = String.escapeSingleQuotes(value);
        for(String field : FIELDS_NAME_FILTER) {
            String likeBase = String.isBlank(likeConditions) ? LIKE_CONDITION_BASE : LOGICAL_OR + LIKE_CONDITION_BASE;
            likeConditions += likeBase.replace('{0}', field).replace('{1}', escapedValue);
        }

        return likeConditions;
    }

    /**
     * Builds Planner Group filter if present
     *
     * @return String
     */
    private String appendPlannerGroupFilter() {
        String plannerGroupFilter = '';

        if(String.isNotBlank(this.plannerGroupFilter)) {
            plannerGroupFilter = buildAndEqualsCondition(FIELD_WO_PLANNER_GROUP, this.plannerGroupFilter);
        }

        return plannerGroupFilter;
    }

    /**
     * Builds Vendor Filter if present
     * if vendor filter is equal to ANY_VALUE_KEY it will build condition to null
     *
     * @return String
     */
    private String appendVendorFilter() {
        String vendorFilter = '';

        if(String.isNotBlank(this.vendorFilter) && this.vendorFilter != ANY_VALUE_KEY){
            vendorFilter = buildAndEqualsCondition(FIELD_WOA_VENDOR, this.vendorFilter);
        } else if(this.vendorFilter == ANY_VALUE_KEY) {
            vendorFilter = buildAndNullCondition(FIELD_WOA_VENDOR);
        }

        return vendorFilter;
    }

    /**
     * Builds Tradesman Filter if present
     * If tradesman filter is equal to ANY_VALUE_KEY it will build condition to NUM of Assignees.
     *
     * @return String
     */
    private String appendTradesmanFilter() {
        String tradesmanFilter = '';

        if(String.isNotBlank(this.tradesmanFilter) && this.tradesmanFilter != ANY_VALUE_KEY) {
            tradesmanFilter = buildInCondition(FIELD_WOA_ID, getTradesmenAssignedWOAs(this.tradesmanFilter));
        } else if(this.tradesmanFilter == ANY_VALUE_KEY){
            tradesmanFilter = buildAndEqualsCondition(FIELD_WOA_NUM_OF_ASSIGNEES, 0);
        }

        return tradesmanFilter;
    }

    /**
     * Builds Plant Section(Route) filter if present
     *
     * @return String
     */
    private String appendRouteFilter() {
        String routeFilter = '';

        if(String.isNotBlank(this.routeFilter)){
            routeFilter = buildAndEqualsCondition(FIELD_WO_PLANT_SECTION, this.routeFilter);
        }

        return routeFilter;
    }

    /**
     * Builds AMU filter if present
     *
     * @return String
     */
    private String appendAMUFilter() {
        String AMUFilter = '';

        if(String.isNotBlank(this.amuFilter)){
            AMUFilter = buildAndEqualsCondition(FIELD_WO_AMU, this.amuFilter);
        }

        return AMUFilter;
    }

    /**
     * Builds Scheduled From Filter if present
     *
     * @return String
     */
    private String appendScheduledFromFilter() {
        String scheduledFromFilter = '';

        if(this.scheduledFromFilter != null){
            scheduledFromFilter = buildQueryDateString(EQUALS_MORE, buildDateString(this.scheduledFromFilter));
        }

        return scheduledFromFilter;
    }

    /**
     * Builds Scheduled To Filter if present
     *
     * @return String
     */
    private String appendScheduledToFilter() {
        String scheduledToFilter = '';

        if(this.scheduledToFilter != null){
            scheduledToFilter = buildQueryDateString(EQUALS_LESS, buildDateString(this.scheduledToFilter));
        }

        return scheduledToFilter;
    }

    /**
     * Builds Name (any) filter if present
     *
     * @return String
     */
    private String appendNameFilter() {
        String nameFilter = '';

        if(String.isNotBlank(filterName)) {
            nameFilter = buildNameFilter(filterName);
        }

        return nameFilter;
    }

    /**
     * Builds Hide Completed filter if present
     *
     * @return String
     */
    private String appendCompletedFilter() {
        String completedFilter = '';

        if(hideCompletedFilter != null && hideCompletedFilter){
            completedFilter = buildAndNotEqualsCondition(FIELD_WOA_STATUS, VTT_Utilities.ACTIVITY_STATUS_COMPLETED);
        }

        return completedFilter;
    }

    /**
     * Builds Multi Activity Status filter if present
     *
     * @return String
     */
    private String appendMultiStatusFilter() {
        String multiStatusFilter = '';

        if(activityMultiStatusFilter != null && activityMultiStatusFilter.size()> 0) {
            multiStatusFilter = buildInCondition(FIELD_WOA_STATUS, activityMultiStatusFilter);
        }

        return multiStatusFilter;
    }

    /**
     * Builds List of Activity IDs based on non-rejected Assignments for selected tradesman
     *
     * @param tradesmanFilter
     *
     * @return List<String>
     */
    private List<String> getTradesmenAssignedWOAs(String tradesmanFilter){
        List<String> ids = new List<String>();

        for(Work_Order_Activity_Assignment__c i: assignmentSelector.getNonRejectedAssignmentsByTradesman(tradesmanFilter)){
            ids.add(i.Work_Order_Activity__c);
        }

        return ids;
    }

}