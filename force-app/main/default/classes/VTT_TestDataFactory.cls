/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Related Class:  VTT_FieldDefaults
				HOG_SObjectFactory
Description:    Vendor Time Tracking SObject field defaults for Testing purposes. 
History:        mbrimus 05.31.2018 - Created.
				mbrimus 05.31.2018 - Added new WO and WOA methods - simple WO/WOA creation.
				mpanak  21.06.2018 - Added createWorkOrderActivitiesForMultipleWOs method
				mpanak  25.07.2018 - Added CreateAccount method
****************************************************************************************************
WARNING.. WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..WARNING..
****************************************************************************************************
DO NOT CHANGE THIS CLASS, you can extend but dont change any values these, if you have to then rerun these tests

VTT_UtilitiesTest
VTT_TradesmanScheduleControllerTest
VTT_PreventEditOfWorkOrderTest
VTT_PreventEditOfActivityTest
VTT_ActivityDetailTimeLogControllerXTest
**************************************************************************************************/
@isTest
public class VTT_TestDataFactory {
	public static Decimal random = Math.random();
/*
Work Order Activity
*/
	private static Work_Order_Activity__c createWorkOrderActivity() {
		return (Work_Order_Activity__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	/**
	 * Creates Work Order Activity with default values
	 * @param  name        name of activity
	 * @param  userStatus  userStatus of activity
	 * @param  workOrderId Maintenance_Work_Order__c
	 * @param  amuId       Operating_Field_AMU__c
	 * @param  doInsert    indicates if returned record is stored in DB
	 * @return             Work_Order_Activity__c
	 */
	public static Work_Order_Activity__c createWorkOrderActivity(String userStatus, String name, Id workOrderId, Id amuId, Boolean doInsert) {
		Work_Order_Activity__c workOrderActivity = createWorkOrderActivity();
		workOrderActivity.User_Status__c = userStatus;
		workOrderActivity.Name = name;
		workOrderActivity.Maintenance_Work_Order__c = workOrderId;
		workOrderActivity.Operating_Field_AMU__c = amuId;
        if(doInsert) insert workOrderActivity;
		return workOrderActivity;
	}

	/**
	 * Creates Work Order Activity with default values
	 * @param  floc        	floc value
	 * @param  flocCategory floc category
	 * @param  doInsert    	indicates if returned record is stored in DB
	 * @return             	Work_Order_Activity__c
	 */
	public static Work_Order_Activity__c createWorkOrderActivity(String floc, Integer flocCategory, Boolean doInsert) {
		HOG_Maintenance_Servicing_Form__c wo = createWorkOrder(true);
		Work_Order_Activity__c workOrderActivity = createWorkOrderActivity();
		workOrderActivity.Functional_Location__c = floc;
		workOrderActivity.Functional_Location_Category_SAP__c = flocCategory;
		workOrderActivity.Maintenance_Work_Order__c = wo.Id;
		if(doInsert) insert workOrderActivity;
		return workOrderActivity;
	}

	/**
	 * Creates defined number of Work Order Activities per Multiple WOs and with populated flocs from WO
	 * @param  workOrders                  list of work orders
	 * @param  numberOfActivitiesForEachWO defined number of activities to be created for each work order
	 * @param  doInsert                    indicates if returned record is stored in DB
	 * @return                             List Work_Order_Activity__c
	 */
	public static List<Work_Order_Activity__c> createWorkOrderActivitiesForMultipleWOs(List<HOG_Maintenance_Servicing_Form__c> workOrders, Integer numberOfActivitiesForEachWO, Boolean doInsert){
		List<Work_Order_Activity__c> activities = new List<Work_Order_Activity__c>();

		for(HOG_Maintenance_Servicing_Form__c wo: workOrders){
			
			for(Integer num = 0; num < numberOfActivitiesForEachWO; num++)
            {
                Work_Order_Activity__c woa = new Work_Order_Activity__c
                (
                    Name = 'Test Name ' + random,
                    Status__c = 'New',
                    SAP_Assigned__c = null,
                    User_Status__c = '3BEX',
					Maintenance_Work_Order__c = wo.Id,
					Well_Event__c = wo.Well_Event__c,
					Yard__c = wo.Yard__c,
		            Functional_Equipment_Level__c = wo.Functional_Equipment_Level__c,
		            Sub_System__c = wo.Sub_System__c,
					System__c = wo.System__c,
					Location__c = wo.Location__c,
					Facility__c = wo.Facility__c
                );
                activities.add(woa);
            }
		}

		if(doInsert) insert activities;
		return activities;
	}

	public static Work_Order_Activity__c createWorkOrderActivityWithSpecificFloc(Id woId, Id amuId, String floc, String flocName, Id flocId, Boolean doInsert){
		Work_Order_Activity__c workOrderActivity = createworkOrderActivity('New','Test woa',woId, amuId, false);

		workOrderActivity.Name = 'Test Work Order on' + flocName + random;
		workOrderActivity.Maintenance_Work_Order__c = woId;

		workOrderActivity.Functional_Location__c = floc;
		workOrderActivity.Well_Event__c = (flocName == 'Well Event') ? flocId : null;
		workOrderActivity.Yard__c = (flocName == 'Yard') ? flocId : null;
		workOrderActivity.Functional_Equipment_Level__c = (flocName == 'FEL') ? flocId : null;
		workOrderActivity.Sub_System__c = (flocName == 'Sub System') ? flocId : null;
		workOrderActivity.System__c = (flocName == 'System') ? flocId : null;
		workOrderActivity.Location__c = (flocName == 'Location') ? flocId : null;
		workOrderActivity.Facility__c = (flocName == 'Facility') ? flocId : null;

		if(doInsert) insert workOrderActivity;
		return workOrderActivity;
	}
	/*
	Work Order
	*/
	private static HOG_Maintenance_Servicing_Form__c createWorkOrder() {
		return (HOG_Maintenance_Servicing_Form__c) HOG_SObjectFactory.createSObject(new HOG_Maintenance_Servicing_Form__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	public static HOG_Maintenance_Servicing_Form__c createWorkOrder(Id serviceRequestId, Id notificationTypeId, Boolean doInsert) {
		HOG_Maintenance_Servicing_Form__c workOrder = createWorkOrder();
		workOrder.HOG_Service_Request_Notification_Form__c = serviceRequestId;
		workOrder.HOG_Notification_Type__c = notificationTypeId;
        if(doInsert) insert workOrder;
		return workOrder;
	}

	/**
	 * Creates Work Order with default values
	 * @param  doInsert indicates if returned record is stored in DB
	 * @return          HOG_Maintenance_Servicing_Form__c
	 */
	public static HOG_Maintenance_Servicing_Form__c createWorkOrder(Boolean doInsert) {
		HOG_Service_Category__c serviceCategory = createServiceCategory(true);
        HOG_Notification_Type__c notificationType = createNotificationType(serviceCategory.id, true);
        HOG_Service_Code_MAT__c serviceCodeMat = createServiceCodeMat(true);
        HOG_Service_Required__c serviceRequired = createServiceRequired(true);
        HOG_User_Status__c userStatus = createUserStatus(true);
        HOG_Work_Order_Type__c workOrderType =  createWorkOrderType(notificationType.id, 
																	serviceCodeMat.id, 
																	serviceRequired.id,  
																	userStatus.id, 
																	true);
       	HOG_Service_Request_Notification_Form__c snr = createServiceRequestNotification(workOrderType.id, true);

       	HOG_Maintenance_Servicing_Form__c workOrder = createWorkOrder(snr.Id, notificationType.Id, false);
       	if(doInsert) insert workOrder;
		return workOrder;
	}

/*
Service Request Notification
*/
	private static HOG_Service_Request_Notification_Form__c createServiceRequestNotification() {
		return (HOG_Service_Request_Notification_Form__c) HOG_SObjectFactory.createSObject(new HOG_Service_Request_Notification_Form__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	/**
	 * Creates Service Request Notification with default values
	 * @param  workOrderTypeId HOG_Work_Order_Type__c
	 * @param  doInsert        indicates if returned record is stored in DB
	 * @return                 HOG_Service_Request_Notification_Form__c
	 */
	public static HOG_Service_Request_Notification_Form__c createServiceRequestNotification(Id workOrderTypeId, Boolean doInsert) {
		HOG_Service_Request_Notification_Form__c notification = createServiceRequestNotification();
		notification.HOG_Work_Order_Type__c = workOrderTypeId;
        if(doInsert) insert notification;
		return notification;
	}

/*
Work order type
*/
	private static HOG_Work_Order_Type__c createWorkOrderType() {
		return (HOG_Work_Order_Type__c) HOG_SObjectFactory.createSObject(new HOG_Work_Order_Type__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	/**
	 * Creates Assignement Rule with default values
	 * @param  notificationTypeId HOG_Notification_Type__c
	 * @param  serviceCodeMATId   HOG_Service_Code_MAT__c
	 * @param  serviceRequiredId  HOG_Service_Required__c
	 * @param  userStatusId       HOG_User_Status__c
	 * @param  doInsert           indicates if returned record is stored in DB
	 * @return                    HOG_Work_Order_Type__c
	 */
	public static HOG_Work_Order_Type__c createWorkOrderType(Id notificationTypeId, Id serviceCodeMATId, Id serviceRequiredId, Id userStatusId, Boolean doInsert) {
		HOG_Work_Order_Type__c workOrderType = createWorkOrderType();
		workOrderType.HOG_Notification_Type__c = notificationTypeId;
        workOrderType.HOG_Service_Code_MAT__c = serviceCodeMATId;
        workOrderType.HOG_Service_Required__c = serviceRequiredId;
        workOrderType.HOG_User_Status__c = userStatusId;
		if(doInsert) insert workOrderType;
		return workOrderType;
	}

/*
User status
*/
	public static HOG_User_Status__c createUserStatus() {
		return (HOG_User_Status__c) HOG_SObjectFactory.createSObject(new HOG_User_Status__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	public static HOG_User_Status__c createUserStatus(Boolean doInsert) {
		HOG_User_Status__c userStatus = createUserStatus();
		if(doInsert) insert userStatus;
		return userStatus;
	}

/*
Service Required
*/
	public static HOG_Service_Required__c createServiceRequired() {
		return (HOG_Service_Required__c) HOG_SObjectFactory.createSObject(new HOG_Service_Required__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	public static HOG_Service_Required__c createServiceRequired(Boolean doInsert) {
		HOG_Service_Required__c serviceRequired = createServiceRequired();
		if(doInsert) insert serviceRequired;
		return serviceRequired;
	}

/*
Service Code Mat
*/
	public static HOG_Service_Code_MAT__c createServiceCodeMat() {
		return (HOG_Service_Code_MAT__c) HOG_SObjectFactory.createSObject(new HOG_Service_Code_MAT__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	public static HOG_Service_Code_MAT__c createServiceCodeMat(Boolean doInsert) {
		HOG_Service_Code_MAT__c codeMat = createServiceCodeMat();
		if(doInsert) insert codeMat;
		return codeMat;
	}

/*
Service Category
*/
	public static HOG_Service_Category__c createServiceCategory() {
		return (HOG_Service_Category__c) HOG_SObjectFactory.createSObject(new HOG_Service_Category__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	public static HOG_Service_Category__c createServiceCategory(Boolean doInsert) {
		HOG_Service_Category__c category =createServiceCategory();
		if(doInsert) insert category;
		return category;
	}

/*
Notification Type
*/
	private static HOG_Notification_Type__c createNotificationType() {
		return (HOG_Notification_Type__c) HOG_SObjectFactory.createSObject(new HOG_Notification_Type__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	public static HOG_Notification_Type__c createNotificationType(Id serviceCategoryId, Boolean doInsert) {
		HOG_Notification_Type__c notificationType = createNotificationType();
		notificationType.HOG_Service_Category__c = serviceCategoryId;
		if(doInsert) insert notificationType;
		return notificationType;
	}
	
/*
Assignement Rules
*/
	
	/**
	 * Creates Assignement Rule with default values
	 * @param  doInsert indicates if returned record is stored in DB
	 * @return          Work_Order_Activity_Assignment_Rule__c
	 */
	private static Work_Order_Activity_Assignment_Rule__c createAssignementRule() {
		return (Work_Order_Activity_Assignment_Rule__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity_Assignment_Rule__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	public static Work_Order_Activity_Assignment_Rule__c createAssignementRule(Id accountId, Boolean doInsert) {
		Work_Order_Activity_Assignment_Rule__c rule = createAssignementRule();
		rule.Assigned_To_Vendor__c = accountId;
		if(doInsert) insert rule;
		return rule;
	}

	public static Work_Order_Activity_Assignment_Rule__c createAssignementRule(Id accountId, String workCenter, String unloadingPoint, String recipient, Boolean doInsert) {
		Work_Order_Activity_Assignment_Rule__c rule = createAssignementRule();
		rule.Assigned_To_Vendor__c = accountId;
		rule.Recipient__c = recipient;
		rule.Unloading_Point__c = unloadingPoint;
		rule.Work_Center__c = workCenter;
		if(doInsert) insert rule;
		return rule;
	}

	public static Work_Order_Activity_Assignment_Rule__c createAssignementRule(String pName, ID pAccountID,
		String pMATCode, String pOrderType, String pPlannerGroup, String pPlantSection, 
		String pRecipient, String pUnloadingPoint, String pWorkCenter, Boolean doInsert) {
		
		Work_Order_Activity_Assignment_Rule__c rule = createAssignementRule();
		rule.Name = pName;
    	rule.Active__c = true;
    	rule.MAT_Code__c = pMATCode;
    	rule.Order_Type__c = pOrderType;
    	rule.Planner_Group__c = pPlannerGroup;
    	rule.Plant_Section__c = pPlantSection;
    	rule.Recipient__c = pRecipient;
    	rule.Unloading_Point__c = pUnloadingPoint;
    	rule.Work_Center__c = pWorkCenter;
    	rule.Assigned_To_Vendor__c = pAccountID;
		
		if(doInsert) insert rule;
		return rule;
	}

	private static List<Work_Order_Activity_Assignment_Rule__c> createAssignementRules(Integer numOfRecords) {
		return (List<Work_Order_Activity_Assignment_Rule__c>) HOG_SObjectFactory.createSObjectList(new Work_Order_Activity_Assignment_Rule__c(), 
																				numOfRecords, 
																				VTT_FieldDefaults.CLASS_NAME);
	}

	public static List<Work_Order_Activity_Assignment_Rule__c> createAssignementRules(Id accountId, Integer numOfRecords, Boolean doInsert) {
		List<Work_Order_Activity_Assignment_Rule__c> rules = createAssignementRules(numOfRecords);
		for(Work_Order_Activity_Assignment_Rule__c rule : rules) {
			rule.Assigned_To_Vendor__c = accountId;
		}
		if(doInsert) insert rules;
		return rules;
	}

	/*
	Assignement Rules Items
	*/
	private static Work_Order_Activity_Assignment_Rule_Item__c createAssignementRuleItem() {
		return (Work_Order_Activity_Assignment_Rule_Item__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity_Assignment_Rule_Item__c(), 
																	VTT_FieldDefaults.CLASS_NAME);
	}

	/**
	 * Creates Assignement Rule Item with default values
	 * @param  doInsert indicates if returned record is stored in DB
	 * @return          Work_Order_Activity_Assignment_Rule_Item__c
	 */
	public static Work_Order_Activity_Assignment_Rule_Item__c createAssignementRuleItem(Id tradesmenId, Id ruleId, Boolean doInsert) {
		Work_Order_Activity_Assignment_Rule_Item__c ruleItem = createAssignementRuleItem();
		ruleItem.Rule__c = ruleId;
		ruleItem.Tradesman__c = tradesmenId;
		if(doInsert) insert ruleItem;
		return ruleItem;
	}

	/*
	Log Entry
    */
	/**
	 * Create log entry
	 *
	 * @return
	 */
	private static Work_Order_Activity_Log_Entry__c createLogEntry() {
		return (Work_Order_Activity_Log_Entry__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity_Log_Entry__c(),
				VTT_FieldDefaults.CLASS_NAME);
	}

	/**
	 * Creates multiple entries
	 *
	 * @param numberOfEntries
	 * @param doInsert
	 *
	 * @return
	 */
	public static List<Work_Order_Activity_Log_Entry__c> createLogEntries(Integer numberOfEntries, Boolean doInsert, String logId){
				List<Work_Order_Activity_Log_Entry__c> entries = new List<Work_Order_Activity_Log_Entry__c>();
        List<String> statuses = new List<String>{ 'Start Job', 'Start at Equipment', 'Finished at Equipment'};

		for(Integer num = 0; num < numberOfEntries; num++) {
			Work_Order_Activity_Log_Entry__c entry = createLogEntry();
            entry.Status__c = statuses.get(0);
            entry.StatusEnd__c = System.today().addDays(-1);
            entry.TimeStamp__c = System.today();
            entry.Work_Order_Activity_Log_Lookup__c = logId;
            entry.recalculateFormulas();
			entries.add(entry);
		}
		if(doInsert) insert entries;
		return entries;
	}

    /*
	Log
    */
    /**
     * Create log
     *
     * @return
     */
    private static Work_Order_Activity_Log__c createLog() {
        return (Work_Order_Activity_Log__c) HOG_SObjectFactory.createSObject(new Work_Order_Activity_Log__c(),
                VTT_FieldDefaults.CLASS_NAME);
    }

    /**
     * Creates log for tradesman on date
     *
     * @param numberOfEntries
     * @param doInsert
     *
     * @return
     */
    public static Work_Order_Activity_Log__c createLogOnDateForTradesman(String tradesmanId, Date currentDate, Boolean doInsert){
        Work_Order_Activity_Log__c entry = createLog();
        entry.Date__c = currentDate;
        entry.Tradesman__c = tradesmanId;
		entry.recalculateFormulas();
        if(doInsert) insert entry;
        return entry;
    }

	/*
	VTT_LTNG_Request
    */
	public static String getRequestData(String actionName){
		VTT_LTNG_Request request = new VTT_LTNG_Request();
		request.actionName = actionName;
		request.currentLatitude = 1;
		request.currentLongitude = 1;
		request.actionComment = 'Some comment';
        request.requestReschedule = false;
        request.isOtherTradesmanStillWorking = false;
        request.activitiesToComplete = new List<VTT_LTNG_AvailableActivity>();

		return JSON.serialize(request);
	}

	public static VTT_LTNG_Request getRequestData(String actionName, Boolean otherTradesmanWorking, Boolean reschedule){
		VTT_LTNG_Request request = new VTT_LTNG_Request();
		request.actionName = actionName;
		request.currentLatitude = 1;
		request.currentLongitude = 1;
		request.actionComment = 'Some comment';
		request.requestReschedule = reschedule;
		request.isOtherTradesmanStillWorking = otherTradesmanWorking;
		request.activitiesToComplete = new List<VTT_LTNG_AvailableActivity>();
		return request;
	}

 /*
ACCOUNT
    */
   /**
    * [createPartnerAccount description]
    * @param  accountName     [description]
    * @param  parentAccountId [description]
    * @param  doInsert        [description]
    * @return                 [description]
    */
   	public static Account createAccount(String accountName, Id parentAccountId, Boolean doInsert) {
		Account account = new Account();
		account.Name = accountName;
		account.ParentId = (parentAccountId != Null) ? parentAccountId : Null;

		if(doInsert) insert account;
		return account;
	}
}