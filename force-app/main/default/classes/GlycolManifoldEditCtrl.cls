public with sharing class GlycolManifoldEditCtrl {
    
    private Glycol_Manifold__c currentManifold;
    private ApexPages.StandardController con;
    public GlycolManifoldEditCtrl(ApexPages.StandardController stdCon)
    {
        currentManifold = (Glycol_Manifold__c)stdCon.getRecord();
        con = stdCon;
    }
    // plant picklist
    public List<SelectOption> getPlantItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        // Add a "Choose a Plant" option
        options.add(new SelectOption('-1', '-- Choose A Plant --'));
        for (List<Plant__c> pList : [SELECT Name FROM Plant__c ORDER BY Name])
        {
            for (Plant__c p : pList)
            {
                options.add(new SelectOption(p.ID, p.Name));
            }
        }
        return options;             
    }
  
    // unit picklist
    public List<SelectOption> getUnitItems()
    {
        List<SelectOption> options = new List<SelectOption>();
        
        // Add a "Choose a Unit" option
        options.add(new SelectOption('-1', '-- Choose A Unit --'));
        
        if (currentManifold.Plant__c != null)
        {
            for (List<Unit__c> uList : [SELECT Name FROM Unit__c WHERE Plant__c = : currentManifold.Plant__c ORDER BY Name])
            {
                for (Unit__c unit : uList) 
                {
                    options.add(new SelectOption(unit.Id, unit.Name));
                }
            }
        }
        return options;
    }
    public void  RefreshUnitList() 
    {
        currentManifold.Unit__c = null;
    } 
    public PageReference saveAndNew() 
    {     
        
        try
        {     
            con.save();
        }
        catch(Exception e)
        {
            ApexPages.addMessages(e);
        }
      
        PageReference nextPage = new PageReference('/a2h/e');
        nextPage.setRedirect(true);
        return nextPage;
      
    }
}