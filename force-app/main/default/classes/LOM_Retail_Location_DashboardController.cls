public with sharing class LOM_Retail_Location_DashboardController {

    private final Retail_Location__c objRetail_Location ;
    private String ID;

    public LOM_Retail_Location_DashboardController  (ApexPages.StandardController stdController){
    
        this.objRetail_Location = (Retail_Location__c)stdController.getRecord();
        ID = objRetail_Location.ID;
    
        dateFrom = Date.Today()-1;
        dateTo = Date.Today()-1;

        
        DepartmentSalesType  = 'department';
        FuelSalesType  = 'grade';
    }


    public Date dateFrom {get;set;}
    public Date dateTo {get;set;}    

    public Boolean DepartmentSalesHasData {get;set;}   
    public Boolean FuelSalesHasData {get;set;}   
    public Boolean TenderHasData {get;set;}       
    public Boolean CashierExceptionsHasData {get;set;}       
    public Boolean CarWashHasData {get;set;} 
    public Boolean CustomerActivityHasData {get;set;}     
    public Boolean AvgHourlyActivityHasData {get;set;}         
    
    public List<StatementData> DepartmentSalesData{get; set;}    
    public List<StatementData> FuelSalesData{get; set;}        
    public List<StatementData> TenderData{get; set;}            
    public List<StatementData> CashierExceptionsData{get; set;}                
    public List<StatementData> CustomerActivityData{get; set;}    
    public List<HourlyActivityData> AvgHourlyActivityData{get; set;}    
   
    public List<StatementData> CarWashDataByDepartment{get; set;}   
   
   
    public String DepartmentSalesType {get;set;}    
    public String FuelSalesType {get;set;}        
    
    public String dateFromStr 
    {
        get
        {
            Date d = dateFrom;
            return d.format() ;
        }
        set
        {
            dateFrom = convertDate(value);    //Date.valueOf(value);
        }
    }
    public String dateToStr 
    {
        get
        {
            Date d = dateTo ;
            return d.format() ;
        }
        set
        {
            dateTo =  convertDate(value); //Date.valueOf(value);
        }
    }    
    
    public PageReference RefreshData()
    {
    system.debug('RefreshData');
        retrieveDepartmentSales();
        retrieveFuelSales();   
        retrieveTenders();  
        retrieveCashierExceptions();
        retrieveCarWashData();
        retrieveCustomerActivityData();
        return null;
    }   
    public PageReference RefreshDepartmentSalesData()
    {
    system.debug('RefreshData');
        retrieveDepartmentSales();
        return null;
    }      
    public PageReference RefreshFuelSalesData()
    {
    system.debug('RefreshData');
        retrieveFuelSales();
        return null;
    }      


    public void retrieveCashierExceptions() {
        List<StatementData> tempData = new List<StatementData>();
        AggregateResult[] AgR;

          //get totals ignoring MOTOL OIL (code 100) grouped by location
          AgR = [SELECT Employee__c Name, COUNT(ID) Total 
          FROM LOM_Cashier_Exception__c
          WHERE     Date_Time__c >= :dateFrom AND   Date_Time__c <= :dateTo 
          and Retail_Location__c = :ID
          GROUP BY Employee__c]; 
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData .add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         CashierExceptionsData = tempData ;
         CashierExceptionsHasData = (tempData .size()>0);
    }

//Tender
    public void retrieveTenders() {
        List<StatementData> tempData = new List<StatementData>();
        AggregateResult[] AgR;

          //get totals ignoring MOTOL OIL (code 100) grouped by location
          AgR = [SELECT Card_Type__c Name, SUM(Store_Trans_Amount__c) Total 
          FROM LOM_Tender__c
          WHERE Date__c >= :dateFrom AND Date__c <= :dateTo 
          and Retail_Location__c = :ID
          GROUP BY Card_Type__c]; 
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData .add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         TenderData= tempData ;
         TenderHasData= (tempData .size()>0);
    }


     
    
//DepartmentSales
    public void retrieveDepartmentSales() {
    system.debug('retrieveDepSalesByDealer');
        List<StatementData> tempDepSales = new List<StatementData>();
        AggregateResult[] AgR;

        //exclude motor oil and car wash codes
        List<String> excludedCodes = new List<String> {'100','88','92','93','94'};

        if(DepartmentSalesType=='dealer')
        {
          //get totals ignoring MOTOL OIL (code 100) grouped by location
          AgR = [SELECT Retail_Location__r.Name Name, SUM(Sales__c) Total FROM LOM_Department_Sale__c
          WHERE Sales__c > 0 AND  Code__c not in :excludedCodes AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          and Retail_Location__c = :ID
          GROUP BY Retail_Location__r.Name ORDER BY SUM(Sales__c) DESC LIMIT 10] ; 
        }
        else
        {
          //get totals ignoring MOTOL OIL (code 100) grouped by department
          AgR = [SELECT Department__c Name, SUM(Sales__c) Total FROM LOM_Department_Sale__c
          WHERE Sales__c > 0 AND  Code__c not in :excludedCodes AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          and Retail_Location__c = :ID          
          GROUP BY  Department__c  ORDER BY SUM(Sales__c) DESC LIMIT 10];        
        }  
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempDepSales.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         DepartmentSalesData= tempDepSales;
         DepartmentSalesHasData = (DepartmentSalesData.size()>0);
    }
    

//retrieveCarWashData
    public void retrieveCarWashData() {
    
    //only include car wash codes
    List<String> carwashCodes = new List<String> {'88','92','93','94'};
    
        List<StatementData> tempData;
        AggregateResult[] AgR;

        tempData = new List<StatementData>();

          AgR = [SELECT Department__c Name, SUM(Sales__c) Total FROM LOM_Department_Sale__c
          WHERE Sales__c > 0 AND  Code__c in :carwashCodes AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          and Retail_Location__c = :ID   
          GROUP BY  Department__c ORDER BY SUM(Sales__c) DESC LIMIT 10];        
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         CarWashHasData = (tempData.size()>0);
         CarWashDataByDepartment= tempData;
    }



//retrieveCarWashData
    public void retrieveCustomerActivityData() {
    
        List<StatementData> tempData = new List<StatementData>();
        List<HourlyActivityData> tempData1 = new List<HourlyActivityData>(); 
                
        AggregateResult[] AgR;

          AgR = [select Retail_Location__c, 
                  Hour__c Hour, AVG(Sales_amount__c) AVG_SALES, 
                  AVG(Customers_Count__c) AVG_CUSTOMERS  
                  from LOM_Customer_Activity__c 
                  WHERE Retail_Location__c = :ID   
                  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
                  Group by Retail_Location__c, Hour__c ORDER BY AVG(Sales_amount__c) DESC LIMIT 10];
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData.add(new StatementData( String.valueOf(s.get('Hour')), Integer.valueof(s.get('AVG_SALES'))));
         }

         CustomerActivityHasData= (tempData.size()>0);
         CustomerActivityData = tempData;         
         
          AgR = [select Retail_Location__c, 
                  Hour__c Hour, AVG(Sales_amount__c) AVG_SALES, 
                  AVG(Customers_Count__c) AVG_CUSTOMERS  
                  from LOM_Customer_Activity__c 
                  WHERE Retail_Location__c = :ID   
                  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
                  Group by Retail_Location__c, Hour__c];  
                  
                         
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            tempData1.add(new HourlyActivityData(
                                String.valueOf(s.get('Hour')), 
                                Integer.valueof(s.get('AVG_CUSTOMERS')), 
                                Integer.valueof(s.get('AVG_SALES'))
                                )); 
         }         
         
         AvgHourlyActivityData = tempData1;         
    }



//FuelSales
    public void retrieveFuelSales() {
        List<StatementData> data = new List<StatementData>();
        AggregateResult[] AgR;
        
        if(FuelSalesType=='dealer')
        {
         AgR = [SELECT Retail_Location__r.Name Name, SUM(Total_Vol__c) Total FROM LOM_Daily_Fuel_Sale__c
          WHERE Total_Vol__c > 0  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          and Retail_Location__c = :ID          
          GROUP BY Retail_Location__r.Name]; 
        }
        else
        {
         AgR = [SELECT  Grade__c Name, SUM(Total_Vol__c) Total FROM LOM_Daily_Fuel_Sale__c
          WHERE Total_Vol__c > 0  AND Date__c >= :dateFrom AND Date__c <= :dateTo 
          and Retail_Location__c = :ID          
          GROUP BY  Grade__c];         
        }  
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            data.add(new StatementData( String.valueOf(s.get('Name')), Integer.valueof(s.get('Total'))));
         }
         
         FuelSalesHasData = (data.size()>0);
         FuelSalesData =  data;
    }
    
    public List<StatementData> getUnpaidStatementsByDealer() {
        List<StatementData> data = new List<StatementData>();
        
          AggregateResult[] AgR = [SELECT Retail_Location__r.Name Location, SUM(Amount__c) Total FROM LOM_Invoice__c
          WHERE Amount__c > 0 AND Paid_Of_Date__c = NULL
          and Retail_Location__c = :ID          
          GROUP BY Retail_Location__r.Name]; 
        
        // Loop through the list and update the Name field
        for(AggregateResult s : AgR){
            data.add(new StatementData( String.valueOf(s.get('Location')), Integer.valueof(s.get('Total'))));
         }
         
        return data;
    }    
    



    // Wrapper class
    public class StatementData{

        public String name { get; set; }
        public Integer data { get; set; }
        public String dataCurrency { get; set; }        
        public String dataDecimal { get; set; }          

        public StatementData(String name,  Integer data) {
            this.name = name;
            this.data = data;


            //this.dataCurrency  = '$' + string.valueOf(data);
            List<String> args = new String[]{'0','number','###,###,###,##0.00'};
            String s = String.format(data.format(), args);
            System.debug(s);
            this.dataCurrency  ='$' + s;
            this.dataDecimal = s;
        }
    }
    
        // Wrapper class
        public class HourlyActivityData {
            public String Hour{ get; set; }
            public Integer numCustomers { get; set; }
            public Double avgSales { get; set; }
            public String avgSalesCurrency { get; set; }             

         public HourlyActivityData (String hour) {
             this(hour, 0, 0);
         }
         public HourlyActivityData (String hour, Integer numCustomers, Double avgSales) {
             this.Hour= hour;
             this.numCustomers = numCustomers;
             this.avgSales = avgSales;
             
            //this.dataCurrency  = '$' + string.valueOf(data);
            List<String> args = new String[]{'0','number','###,###,###,##0.00'};
            String s = String.format(avgSales.format(), args);
            this.avgSalesCurrency ='$' + s;
         }
     }    
    
    
    
  // --------------------------------------------------------------------------
  // Search car seal given its car seal ID
  public PageReference refreshDashBoardByDate() {
    //dateFrom = carrier1.ActivityDate; 
    //dateTo = carrier2.ActivityDate; 
    return null;
    }     
    
    //convert string dates in MM/dd/YYYY format into Date
    private Date convertDate(String inDate) {
           String[] dateParts = inDate.split('/');
           Integer day = Integer.valueOf(dateParts[0]);
           Integer month= Integer.valueOf(dateParts[1]);           
           Integer year = Integer.valueOf(dateParts[2]);                      
          

           Date dt = Date.newInstance(year , month, day);
              
           return dt ;    
  }    
  
    
}