public class PTCalculatePCSOCacheQuery {
	//public static List<Service__c> pcsoInfo = [select id,ownerid,utility_company__r.name,(select billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r order by billing_date__c desc limit 12),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c='Valid') from service__c where service_status__c in ('Active','Idle') and utility_company__r.name!='Saskpower'];
    public static boolean pcsoInfoQueried = false;
    public static integer pcsoMonthsInt = integer.valueOf(returnControllerSettingValue('allPCSOs'));
    //public static List<Service__c> pcsoInfo = database.query('select id,site_id__c,ownerid,utility_company__r.name,(select Average_Billing_Demand_last_12_months__c,Average_Metered_Demand_last_12_months__c,Average_kVA_Demand_last_12_months__c,Average_Total_Volume_last_12_months__c, billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r order by billing_date__c desc limit '+pcsoMonthsInt+'),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c=\'Valid\') from service__c where service_status__c in (\'Active\',\'Idle\') and utility_company__r.name!=\'Saskpower\'') ;
    public static List<Service__c> pcsoInfo = database.query('select id,ownerid,utility_company__r.name,(select Average_Billing_Demand_last_12_months__c,Average_Metered_Demand_last_12_months__c,Average_kVA_Demand_last_12_months__c,Average_Total_Volume_last_12_months__c,  billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r where billing_date__c = LAST_N_MONTHS:6 order by billing_date__c desc),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c=\'Valid\') from service__c where service_status__c in (\'Active\',\'Idle\') and utility_company__r.name in (\'ATCO Electric\',\'FortisAlberta\')');
    public static decimal returnControllerSettingValue(String name){
        Decimal controllerSettingValue;
        if(PT_PCSO_Settings__c.getValues(name) != null){
            controllerSettingValue = PT_PCSO_Settings__c.getValues(name).PCSO_Value__c;
        }
        else{
            controllerSettingValue = 0;
        }
        return controllerSettingValue;
    }
    
    public static List<Service__c> queryPcsoInfo(){
        if(pcsoInfoQueried == false){
            pcsoInfo = database.query('select id,ownerid,utility_company__r.name,(select Average_Billing_Demand_last_12_months__c,Average_Metered_Demand_last_12_months__c,Average_kVA_Demand_last_12_months__c,Average_Total_Volume_last_12_months__c,  billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r where billing_date__c = LAST_N_MONTHS:6 order by billing_date__c desc),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c=\'Valid\') from service__c where service_status__c in (\'Active\',\'Idle\') and utility_company__r.name in (\'ATCO Electric\',\'FortisAlberta\')');
        	//pcsoInfo = database.query('select id,ownerid,utility_company__r.name,(select Average_Billing_Demand_last_12_months__c,Average_Metered_Demand_last_12_months__c,Average_kVA_Demand_last_12_months__c,Average_Total_Volume_last_12_months__c,  billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r order by billing_date__c desc limit '+pcsoMonthsInt+'),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c=\'Valid\') from service__c where service_status__c in (\'Active\',\'Idle\') and utility_company__r.name!=\'Saskpower\'') ;
            //pcsoInfo = [select id,ownerid,utility_company__r.name,(select billing_demand_kw__c,metered_demand_kva__c,metered_demand_kw__c,total_volume__c,wire_cost__c,ratchet_billing__c,invalid_billing__c from utility_billings__r where billing_date__c = LAST_N_MONTHS:pcsoMonthsInt order by billing_date__c desc limit 12),(select id,name,pcso_status__c,pcso_type__c,system_review__c,months_valid__c from pcsos__r where system_review__c='Valid') from service__c where service_status__c in ('Active','Idle') and utility_company__r.name!='Saskpower'];    
        }
        pcsoInfoQueried = true;
        return pcsoInfo;
    }
}