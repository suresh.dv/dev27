/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint class for FM_TruckTripCancelRedirectQA lightning component.
                It queries all necessary records, do validation and return target URL for redirect
                or error messages.
Test Class:     FM_TruckTripCancelRedirectCtrlLtngTest
History:        jschn 2019-01-25 - Created.
*************************************************************************************************/
public with sharing class FM_TruckTripCancelRedirectCtrlLtng {

    private static final String CLASS_NAME = String.valueOf(FM_TruckTripCancelRedirectCtrlLtng.class);
    private static final String CANCEL_TARGET_URL = '/apex/FM_CancelTruckTrip?id=';
    public static final String ERROR_UNKNOWN_TRUCK_TRIP = 'Unknown Truck Trip. Truck Trip retrieve unsuccessful.';

    @AuraEnabled
    public static HOG_CustomResponse checkCancelRedirect(String recordId) {
        FM_Truck_Trip__c truckTrip = FM_TruckTrip_Utilities.getTruckTripById(recordId);
        UserRole role = HOG_UserInfoLtng.getCurrentUserRole();

        HOG_CustomResponse response = null;

        if (hasCorrectRoleOrStatus(truckTrip, role)) {
            HOG_CustomResponseImpl resp = new HOG_CustomResponseImpl();
            resp.addResult(CANCEL_TARGET_URL + recordId);
            response = resp;
        } else {
            String errorMsg = getErrorMsg(truckTrip, role);
            response = new HOG_CustomResponseImpl(errorMsg);
        }

        System.debug(CLASS_NAME + ' -> checkCancelRedirect - Id param: '    + recordId);
        System.debug(CLASS_NAME + ' -> checkCancelRedirect - Truck Trip: '  + JSON.serialize(truckTrip));
        System.debug(CLASS_NAME + ' -> checkCancelRedirect - Role: '        + JSON.serialize(role));
        System.debug(CLASS_NAME + ' -> checkCancelRedirect - response: '    + JSON.serialize(response));
        return response;
    }

    private static Boolean hasCorrectRoleOrStatus(FM_Truck_Trip__c truckTrip, UserRole role) {
        return truckTrip != null
                && role != null
                && (role.Name != FM_Utilities.FLUID_MANAGEMENT_VENDOR_USER_ROLE
                        || FM_TruckTrip_Utilities.STATUS_EXPORTED.equals(truckTrip.Truck_Trip_Status__c));
    }

    private static String getErrorMsg(FM_Truck_Trip__c truckTrip, UserRole role) {
        String errorMsg = '';
        if(truckTrip == null) {
            errorMsg = ERROR_UNKNOWN_TRUCK_TRIP;
        } else if(role == null) {
            errorMsg = HOG_UserInfoLtng.ERROR_USER_WITHOUT_ROLE;
        } else {
            errorMsg = FM_CancelTruckTripControllerX.ERROR_WRONG_STATUS_FOR_VENDOR;
        }
        return errorMsg;
    }

}