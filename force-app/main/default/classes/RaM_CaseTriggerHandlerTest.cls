/**************************************************************************************************************
  CreatedBy : Accenture
  Organization:Accenture
  Purpose   :  Test class For Trigger RaM_AutoAssignP1Ticket & RaM_CaseTriggerHandler
  Version:1.0.0.1
*****************************************************************************************************************/
@isTest
Private class RaM_CaseTriggerHandlerTest
{
  @testSetUp static void setup()
  {
     UserRole role1 = new UserRole(Name = 'Maintenance Tech');
     insert role1;
     
     UserRole role2 = new UserRole(Name = 'Help desk');
     insert role2;
     
     Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
     User u = new User(Alias = 'newUser', Email='newuser@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='testnewuser@testorg.com', UserRoleId = role1.Id);
      insert u;
      
      Profile p1 = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User u1 = new User(Alias = 'newUser1', Email='newuser@testorg1.com',
         EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p1.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='testnewuser@testorg1.com', UserRoleId = role2.Id);
      insert u1;
      
      /*List<UserRole> userRoleList = new list<UserRole>();
      UserRole role1 = new UserRole(Name = 'Maintenance Tech');
      userRoleList.add(role1);
      UserRole role2 = new UserRole(Name = 'Help desk');
      userRoleList.add(role2);
      UserRole role3 = new UserRole(Name = 'RaM_Vendor');
      userRoleList.add(role3);
      
      Insert userRoleList;*/
      
      System.runAs(u){
        
      // Insert account
      Account accountRecord = new Account(Name = 'Husky Account',VendorGroup__c='BUNN-O-MATIC');
      accountRecord.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
      insert accountRecord;
      
      //Insert Contact
      Contact contactRecord = new Contact(LastName = 'Glenn Mundi', ContactLogin__c='PRQ1234');
      contactRecord.AccountId = accountRecord.id;
      insert contactRecord;    
      
      // Insert Location
      RaM_Location__c locationRecord = new RaM_Location__c(Name = '10001',Service_Area__c = 'Island',SAPFuncLOC__c = 'C1-02-002-01006', Location_Classification__c ='Urban',R_M_Location__c=true);
      insert locationRecord;
     
      //Insert Equipment
      RaM_Equipment__c equipmentRecord = new RaM_Equipment__c(Name = 'Test1');
      equipmentRecord.FunctionalLocation__c = 'C1-02-002-01006';
      equipmentRecord.Equipment_Type__c = 'ELECTRICAL'; 
      equipmentRecord.Status__c = 'Active';
      equipmentRecord.Equipment_Class__c = 'ELECTRICAL PANEL';
      insert equipmentRecord;
      
      // Insert Vendor Assignment
      RaM_VendorAssignment__c vaRecord = new RaM_VendorAssignment__c();
      vaRecord.Account__c = accountRecord.id;
      vaRecord.Equipment_Type_VA__c = 'ELECTRICAL';
      vaRecord.Equipment_Class_VA__c = 'ELECTRICAL PANEL'; 
      vaRecord.Priority__c = 1;
      vaRecord.ServiceArea__c='Island';
      vaRecord.Location_Classification_VA__c='Urban';
      insert vaRecord; 
      
      //Insert Entitlement Record
      Entitlement entitlementRecord = new Entitlement(Name = 'SLA Husky Energy',AccountId = AccountRecord.id);
      insert entitlementRecord; 
      
      // Insert Ram Setting 
      RaM_Setting__c cusSetting=new RaM_Setting__c();
      cusSetting.name='Case Next Counter';
      cusSetting.R_M_Ticket_Number_Next_Sequence__c=1234567;
      insert cusSetting;
      
    }      
   
   }
   //  Method for Positive Scenario of P1 
   static testMethod void PositiveCases(){
      //Search internal husky useruser
      User u2 = [select id from user where UserName='testnewuser@testorg.com'];
      Test.startTest();  
      System.runAs(u2) {
            // Queried all records those inserted in test settup process.

            Ram_Location__c locationRecord = [select id,Name  from Ram_Location__c limit 1];
            RaM_VendorAssignment__c  vaRecord = [select id,Account__c,Equipment_Type_VA__c,Equipment_Class_VA__c,ServiceArea__c,Location_Classification_VA__c,Priority__c from RaM_VendorAssignment__c limit 1];

            RaM_Equipment__c equipmentRecord = [select id,Equipment_Type__c,Equipment_Class__c from RaM_Equipment__c]; 

            List<Case> caseRecords = new List<Case>();
            // To get case record type ids with out using query
            Id childTicketRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Child Case').getRecordTypeId(); 
            Id ramTicketRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();

                 
            //insert cases  
            Case c1 = new Case(RecordTypeId = ramTicketRecordTypeId,Status = 'New',Priority = 'P1-Emergency',Origin = 'Maintenance Tech',LocationNumber__c = LocationRecord.id,EquipmentName__c=equipmentRecord.id, Location_Contact__c='Amit Singh',Equipment_Type__c='ELECTRICAL',Equipment_Class__c='ELECTRICAL PANEL', Location_Classification__c='Urban',Service_Area__c='Island' , subject='test', description='test'  );    
            System.assertEquals(c1.status,'New');
            insert c1;      

            System.assertEquals(c1.Equipment_Type__c,'ELECTRICAL');   
            System.assertEquals(c1.Equipment_Class__c,'ELECTRICAL PANEL');
            System.assertEquals(c1.Location_Classification__c,'Urban');
            System.assertEquals(c1.Service_Area__c,'Island');

            Case c2 = new Case(RecordTypeId = childTicketRecordTypeId,Status = 'New',Priority = 'P2-Break/Fix',Origin = 'Maintenance Tech',LocationNumber__c = LocationRecord.id,EquipmentName__c=equipmentRecord.id, Location_Contact__c='Amit Singh',Equipment_Type__c='ELECTRICAL',Equipment_Class__c='ELECTRICAL PANEL', Location_Classification__c='Urban',Service_Area__c='Island'  , subject='test', description='test' );    
            System.assertEquals(c2.status,'New');
            insert c2;
            //to remove Entitlement name when SLAExempt__c field's value as true
            c1.SLAExempt__c = true;
            update c1; // Update case  
            
            c2.status = 'Work in Progress';
            update c2;

            c2.status = 'Resolved';
            update c2;

            Test.stopTest();
     }
  }
  static testMethod void PositiveCases1(){
      //Search internal husky useruser
      User u2 = [select id from user where UserName='testnewuser@testorg.com'];
 
      System.runAs(u2) {
            // Queried all records those inserted in test settup process.
            Ram_Location__c locationRecord = [select id,Name  from Ram_Location__c limit 1];
            RaM_VendorAssignment__c  vaRecord = [select id,Account__c,Equipment_Type_VA__c,Equipment_Class_VA__c,ServiceArea__c,Location_Classification_VA__c,Priority__c from RaM_VendorAssignment__c limit 1];

            RaM_Equipment__c equipmentRecord = [select id,Equipment_Type__c,Equipment_Class__c from RaM_Equipment__c]; 

            // To get case record type ids with out using query
            Id childTicketRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Child Case').getRecordTypeId(); 
            
            
           
            Test.startTest(); 
            Case c4 = new Case(RecordTypeId = childTicketRecordTypeId,Status = 'New',Priority = 'P2-Break/Fix',Origin = 'Maintenance Tech',LocationNumber__c = LocationRecord.id,EquipmentName__c=equipmentRecord.id, Location_Contact__c='Amit Singh',Equipment_Type__c='ELECTRICAL',Equipment_Class__c='ELECTRICAL PANEL', Location_Classification__c='Urban',Service_Area__c='Island'  , subject='test', description='test'  );                
            insert c4;

            System.assertEquals(c4.Equipment_Type__c,'ELECTRICAL');   
            System.assertEquals(c4.Equipment_Class__c,'ELECTRICAL PANEL');
            System.assertEquals(c4.Location_Classification__c,'Urban');
            System.assertEquals(c4.Service_Area__c,'Island');
            
            c4.Status='Assigned';
            c4.Vendor_Assignment__c=vaRecord.id;
            update c4;
            System.assertEquals(c4.Status,'Assigned');
            
            c4.Status='Completed';
            update c4;
            System.assertEquals(c4.Status,'Completed');
            
            //Create work information for updating Case status to Vendor Completed
            RaM_WorkInformation__c wiRecord = new RaM_WorkInformation__c();
            wiRecord.Ticket__c = c4.id;
            wiRecord.WorkLogSmall__c = 'Test';  
            insert wiRecord;
            
            c4.Status='Vendor Completed';
            c4.Case_Completed_Date__c= datetime.now();
            update c4;
            System.assertEquals(c4.Status,'Vendor Completed');
            
            list<id> caseIds=new list<id>();
            caseIds.add(c4.id); 
             // This causes a fake response to be generated
            Test.setMock(WebServiceMock.class, new RaM_NotificationWorkOrderMockTest());
            // Call the method that invokes a callout
            RaM_CaseTriggerHandler.callSAPPIinterface(caseIds);
                
            Test.stopTest();
     }
     }
}