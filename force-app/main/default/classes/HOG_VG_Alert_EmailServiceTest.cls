/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Test class for HOG_VG_Alert_EmailService
History:        jschn 25/10/2018 - Created.
*************************************************************************************************/
@IsTest
private class HOG_VG_Alert_EmailServiceTest {

    @IsTest
    static void getNotificationQuery_urgent_wrongDay() {
        prepareSettings(false);
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();

        Test.startTest();
        String query = service.getNotificationQuery(HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS, 'SUN');
        Test.stopTest();

        System.assert(String.isNotBlank(query));
        System.assert(query.contains('Priority__c = NULL'));
    }

    @IsTest
    static void getNotificationQuery_urgent_correctDay() {
        prepareSettings(false);
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();

        Test.startTest();
        String query = service.getNotificationQuery(HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS, 'MON');
        Test.stopTest();

        System.assert(String.isNotBlank(query));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_URGENT + '\''));
    }

    @IsTest
    static void getNotificationQuery_urgent_daily() {
        prepareSettings(true);
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();

        Test.startTest();
        String query = service.getNotificationQuery(HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS, 'SUN');
        Test.stopTest();

        System.assert(String.isNotBlank(query));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_URGENT + '\''));
    }

    @IsTest
    static void getNotificationQuery_nonUrgent_wrongDay() {
        prepareSettings(false);
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();

        Test.startTest();
        String query = service.getNotificationQuery(HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS, 'SUN');
        Test.stopTest();

        System.assert(String.isNotBlank(query));
        System.assert(query.contains('Priority__c = NULL'));
    }

    @IsTest
    static void getNotificationQuery_nonUrgent_correctDay() {
        prepareSettings(false);
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();

        Test.startTest();
        String query = service.getNotificationQuery(HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS, 'MON');
        Test.stopTest();

        System.assert(String.isNotBlank(query));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_HIGH + '\''));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM + '\''));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_LOW + '\''));
    }

    @IsTest
    static void getNotificationQuery_nonUrgent_daily() {
        prepareSettings(true);
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();

        Test.startTest();
        String query = service.getNotificationQuery(HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS, 'SUN');
        Test.stopTest();

        System.assert(String.isNotBlank(query));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_HIGH + '\''));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM + '\''));
        System.assert(query.contains('Priority__c = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_LOW + '\''));
    }

    @IsTest
    static void getNotificationQuery_wrongType() {
        prepareSettings(true);
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();

        Test.startTest();
        String query = service.getNotificationQuery(HOG_VGEmailNotificationsType.PRODUCTION_ENGINEER_REASSIGNMENT,
                'SUN');
        Test.stopTest();

        System.assert(String.isNotBlank(query));
        System.assert(query.contains('Priority__c = NULL'));
    }

    @IsTest
    static void getDayOfWeek() {
        String mondayExpected = 'MON';
        String tuesdayExpected = 'TUE';
        String wednesdayExpected = 'WED';
        String thursdayExpected = 'THU';
        String fridayExpected = 'FRI';
        String saturdayExpected = 'SAT';
        String sundayExpected = 'SUN';

        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();
        Test.startTest();
        String monday = service.getDayOfWeek(Datetime.newInstance(2018, 10, 22));
        String tuesday = service.getDayOfWeek(Datetime.newInstance(2018, 10, 23));
        String wednesday = service.getDayOfWeek(Datetime.newInstance(2018, 10, 24));
        String thursday = service.getDayOfWeek(Datetime.newInstance(2018, 10, 25));
        String friday = service.getDayOfWeek(Datetime.newInstance(2018, 10, 26));
        String saturday = service.getDayOfWeek(Datetime.newInstance(2018, 10, 27));
        String sunday = service.getDayOfWeek(Datetime.newInstance(2018, 10, 28));
        Test.stopTest();

        System.assertEquals(mondayExpected, monday);
        System.assertEquals(tuesdayExpected, tuesday);
        System.assertEquals(wednesdayExpected, wednesday);
        System.assertEquals(thursdayExpected, thursday);
        System.assertEquals(fridayExpected, friday);
        System.assertEquals(saturdayExpected, saturday);
        System.assertEquals(sundayExpected, sunday);

    }

    @IsTest
    static void generateInCompleteVentGasAlertEmail_urgent() {
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();
        List<HOG_Vent_Gas_Alert__c> alerts = [
                SELECT Id, Name, Comments__c, Start_Date__c, Status__c, Type__c,
                            Well_Location__c, Well_Location__r.Name, Production_Engineer__c,
                            Production_Engineer__r.Name, Priority__c
                FROM HOG_Vent_Gas_Alert__c
        ];
        Test.startTest();
        List<Messaging.SingleEmailMessage> emails = service.generateInCompleteVentGasAlertEmails(
                alerts,
                HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS
        );
        Test.stopTest();

        Set<Id> uniqueProductionEngineers = new Set<Id>();
        for(HOG_Vent_Gas_Alert__c alert : alerts) {
            uniqueProductionEngineers.add(alert.Production_Engineer__c);
        }
        System.assertNotEquals(null, emails);
        System.assertEquals(uniqueProductionEngineers.size(), emails.size());
        for(Messaging.SingleEmailMessage mail : emails) {
            System.assert(mail.getSubject() == service.URGENT_EMAIL_SUBJECT);
        }
    }

    @IsTest
    static void generateInCompleteVentGasAlertEmail_nonUrgent() {
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();
        List<HOG_Vent_Gas_Alert__c> alerts = [
                SELECT Id, Name, Comments__c, Start_Date__c, Status__c, Type__c,
                            Well_Location__c, Well_Location__r.Name, Production_Engineer__c,
                            Production_Engineer__r.Name, Priority__c
                FROM HOG_Vent_Gas_Alert__c
        ];
        Test.startTest();
        List<Messaging.SingleEmailMessage> emails = service.generateInCompleteVentGasAlertEmails(
                alerts,
                HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS
        );
        Test.stopTest();

        Set<Id> uniqueProductionEngineers = new Set<Id>();
        for(HOG_Vent_Gas_Alert__c alert : alerts) {
            uniqueProductionEngineers.add(alert.Production_Engineer__c);
        }
        System.assertNotEquals(null, emails);
        System.assertEquals(uniqueProductionEngineers.size(), emails.size());
        for(Messaging.SingleEmailMessage mail : emails) {
            System.assert(mail.getSubject() == service.NON_URGENT_EMAIL_SUBJECT);
        }
    }

    @IsTest
    static void generateInCompleteVentGasAlertEmail_reAssigned() {
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();
        List<HOG_Vent_Gas_Alert__c> alerts = [
                SELECT Id, Name, Comments__c, Start_Date__c, Status__c, Type__c,
                            Well_Location__c, Well_Location__r.Name, Production_Engineer__c,
                            Production_Engineer__r.Name, Priority__c
                FROM HOG_Vent_Gas_Alert__c
        ];
        Test.startTest();
        List<Messaging.SingleEmailMessage> emails = service.generateInCompleteVentGasAlertEmails(
                alerts,
                HOG_VGEmailNotificationsType.PRODUCTION_ENGINEER_REASSIGNMENT
        );
        Test.stopTest();

        Set<Id> uniqueProductionEngineers = new Set<Id>();
        for(HOG_Vent_Gas_Alert__c alert : alerts) {
            uniqueProductionEngineers.add(alert.Production_Engineer__c);
        }
        System.assertNotEquals(null, emails);
        System.assertEquals(uniqueProductionEngineers.size(), emails.size());
        for(Messaging.SingleEmailMessage mail : emails) {
            System.assert(mail.getSubject() == service.PRODUCTION_ENGINEER_REASSIGNMENT_EMAIL_SUBJECT);
        }
    }

    @IsTest
    static void generateEmailNotification_success() {
        Id alertId = getAlert();
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();
        User usr = [SELECT Id FROM User LIMIT 1];

        Test.startTest();
        service.generateEmailNotification(
                new List<List<String>>{new List<String>{alertId, usr.Id}},
                HOG_VGEmailNotificationsType.PRODUCTION_ENGINEER_REASSIGNMENT
        );
        Test.stopTest();

        System.assert(true);
    }

    @IsTest
    static void generateEmailNotification_fail() {
        Id alertId = getAlert();
        HOG_VG_Alert_EmailService service = new HOG_VG_Alert_EmailService();
        User usr = [SELECT Id FROM User LIMIT 1];

        Test.startTest();
        try {
            service.generateEmailNotification(
                    new List<List<String>>{
                            new List<String>{
                                    alertId, 'abc'
                            }
                    },
                    HOG_VGEmailNotificationsType.PRODUCTION_ENGINEER_REASSIGNMENT
            );
        } catch(Exception ex) {
            System.assert(true);
        }
        Test.stopTest();


    }

    private static void prepareSettings(Boolean daily) {
        HOG_Vent_Gas_Alert_Configuration__c config = new HOG_Vent_Gas_Alert_Configuration__c(
                Urgent_Priority_Alert_Daily_Notification__c = daily,
                High_Priority_Alert_Daily_Notification__c = daily,
                Medium_Priority_Alert_Daily_Notification__c = daily,
                Low_Priority_Alert_Daily_Notification__c = daily
        );
        insert config;
    }

    private static Id getAlert() {
        Location__c location = HOG_VentGas_TestData.createLocationForAlert(true);
        User usr = HOG_VentGas_TestData.createUser('HOG Lloyd', 'SF', 'Lloyd');
        return HOG_VentGas_TestData.createAlert(location.Id, usr.Id, true).Id;
    }

}