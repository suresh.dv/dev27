/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class that handles assignments and un-assignments
Test Class:     VTT_WOA_Assignment_ServiceTest
                VTT_WOA_Assignment_ServiceTestNP
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_Assignment_Service {

    public static VTT_WOA_AssignmentDAO WOAAssignmentDAO = new VTT_WOA_AssignmentDAOImpl();
    private static final List<String> KEY_FIELDS = new List<String> {'Tradesman__c','Work_Order_Activity__c'};

    /**
     * Runs assignment without tradesmen.
     * If vendor account Id is provided and at least on tradesman Id is in List, it will run assignment logic for provided
     * params.
     * If not Invalid Case response is returned.
     *
     * @param activities
     * @param tradesmenIds
     * @param vendorAccountId
     *
     * @return VTT_WOA_Assignment_Response
     */
    public VTT_WOA_Assignment_Response runAssignmentWithoutTradesmen(
            List<Work_Order_Activity__c> activities,
            List<Id> tradesmenIds,
            Id vendorAccountId) {

        if (String.isNotBlank(vendorAccountId) && (tradesmenIds == null || tradesmenIds.size() == 0)) {
            return runAssignment(
                    activities,
                    tradesmenIds,
                    vendorAccountId,
                    VTT_WOA_Assignment_AssignedTextAction.CLEAR,
                    VTT_WOA_Assignment_VendorIdAction.PRESERVE,
                    VTT_WOA_Assignment_Action.SKIP_NEW_ASSIGNMENTS
            );
        }
        return VTT_WOA_Assignment_Response.INVALID_CASE;
    }

    /**
     * Runs assignment with tradesmen.
     * If vendor account is provided and at least one tradesman Id is in list, it will run assignment logic for provided
     * params.
     * If not Invalid Case response is returned.
     *
     * @param activities
     * @param tradesmenIds
     * @param vendorAccountId
     *
     * @return VTT_WOA_Assignment_Response
     */
    public VTT_WOA_Assignment_Response runAssignmentWithTradesmen(
            List<Work_Order_Activity__c> activities,
            List<Id> tradesmenIds,
            Id vendorAccountId) {

        if(String.isNotBlank(vendorAccountId) && tradesmenIds != null && tradesmenIds.size() > 0) {
            return runAssignment(
                    activities,
                    tradesmenIds,
                    vendorAccountId,
                    VTT_WOA_Assignment_AssignedTextAction.PRESERVE,
                    VTT_WOA_Assignment_VendorIdAction.PRESERVE,
                    VTT_WOA_Assignment_Action.BUILD_NEW_ASSIGNMENTS
            );
        }

        return VTT_WOA_Assignment_Response.INVALID_CASE;
    }

    /**
     * Run un-assignment without tradesmen
     * If user is Administrator, activities are provided and tradesmen is not provided, assignment logic will be run.
     * In this case logic will clear Assigned Text, Vendor Id and it won't create new assignments.
     * If any of conditions are invalid, Invalid Case response is returned.
     *
     * @param activities
     * @param tradesmenIds
     * @param vendorAccountId
     *
     * @return VTT_WOA_Assignment_Response
     */
    public VTT_WOA_Assignment_Response runUnAssignmentWithoutTradesmen(
            List<Work_Order_Activity__c> activities,
            List<Id> tradesmenIds,
            Id vendorAccountId) {

        if(VTT_Utilities.IsAdminUser() && activities != null && (tradesmenIds == null || tradesmenIds.size() == 0)) {
            return runAssignment(
                    activities,
                    tradesmenIds,
                    vendorAccountId,
                    VTT_WOA_Assignment_AssignedTextAction.CLEAR,
                    VTT_WOA_Assignment_VendorIdAction.CLEAR,
                    VTT_WOA_Assignment_Action.SKIP_NEW_ASSIGNMENTS
            );
        }

        return VTT_WOA_Assignment_Response.INVALID_CASE;
    }

    /**
     * Run un-assignment with tradesmen
     * if activities list is at least empty (not null) and tradesman Id list contains at least one item, all assignments
     * for provided tradesman and activity are deleted.
     * If any of conditions are invalid, Invalid Case response is returned
     *
     * @param activities
     * @param tradesmenIds
     *
     * @return VTT_WOA_Assignment_Response
     */
    public VTT_WOA_Assignment_Response runUnAssignmentWithTradesmen(
            List<Work_Order_Activity__c> activities,
            List<Id> tradesmenIds) {

        if(activities != null && tradesmenIds != null && tradesmenIds.size() > 0) {
            return persistAssignmentChanges(
                    WOAAssignmentDAO.getAssignments(activities, tradesmenIds),
                    new List<Work_Order_Activity__c>(),
                    new List<Work_Order_Activity_Assignment__c>()
            );
        }

        return VTT_WOA_Assignment_Response.INVALID_CASE;
    }

    /**
     * Multipurpose method which is used for assignment and un-assignment of tradesmen to activity.
     * First it will get set of Ids of WOA records that are meant for Assignments clean up.
     * Then based on generated list and provided activities and tradesman IDs, all assignment are selected.
     * Then special Set and map is generated that value/key consist from part Tradesman and Activity Id.
     * These special items are required to filter down all Assignments that would be deleted and recreated without any change
     * which is not necessary and can cause error when trying to delete assignment for tradesman which is currently
     * working on Activity.
     * For any other tradesman/activity pair, new assignment is built.
     * Last step of the logic is to store changes, new records and delete assignments that are supposed to be deleted.
     *
     * @param activities
     * @param tradesmenIds
     * @param vendorAccountId
     * @param assignedTextAction
     * @param vendorIdAction
     * @param assignmentsAction
     *
     * @return VTT_WOA_Assignment_Response
     */
    private VTT_WOA_Assignment_Response runAssignment(
            List<Work_Order_Activity__c> activities,
            List<Id> tradesmenIds,
            Id vendorAccountId,
            VTT_WOA_Assignment_AssignedTextAction assignedTextAction,
            VTT_WOA_Assignment_VendorIdAction vendorIdAction,
            VTT_WOA_Assignment_Action assignmentsAction) {

        Set<Id> activitySetToDeleteAssignments = getActivityIdsForDeletionAndClearAssignments(
                activities,
                vendorAccountId,
                assignedTextAction,
                vendorIdAction
        );

        List<Work_Order_Activity_Assignment__c> newAssignmentList = new List<Work_Order_Activity_Assignment__c>();

        List<Work_Order_Activity_Assignment__c> unFilteredAssignmentsToDelete = WOAAssignmentDAO.getAssignments(
                activitySetToDeleteAssignments,
                activities,
                tradesmenIds
        );

        System.debug(unFilteredAssignmentsToDelete.size());

        //Building Sets,Maps and lists to exclude records, that would be recreated, from deletion.
        //This should prevent getting error on assignment new Tradesman to activity which is being worked on.
        Set<String> tradesmanActivityKeySet = buildTradesmanActivityKeySet(activities, tradesmenIds);
        Map<String, Work_Order_Activity_Assignment__c> assignmentsByTradesmanActivityKey = buildAssignmentByKeyMap(unFilteredAssignmentsToDelete, KEY_FIELDS);
        List<Work_Order_Activity_Assignment__c> assignmentsToDelete = new List<Work_Order_Activity_Assignment__c>();
        Map<String, Work_Order_Activity_Assignment__c> unchangedAssignmentsByTradesmanActivityKey = new Map<String, Work_Order_Activity_Assignment__c>();

        for(String assignmentKey : assignmentsByTradesmanActivityKey.keySet()) {
            if(tradesmanActivityKeySet.contains(assignmentKey)) {
                unchangedAssignmentsByTradesmanActivityKey.put(assignmentKey, assignmentsByTradesmanActivityKey.get(assignmentKey));
            } else {
                assignmentsToDelete.add(assignmentsByTradesmanActivityKey.get(assignmentKey));
            }
        }

        if (VTT_WOA_Assignment_Action.BUILD_NEW_ASSIGNMENTS.equals(assignmentsAction)) {
            newAssignmentList = buildNewAssignments(activities, tradesmenIds, unchangedAssignmentsByTradesmanActivityKey);
            newAssignmentList.addAll(unchangedAssignmentsByTradesmanActivityKey.values());
        }

        System.debug('assignmentsToDelete: ' + JSON.serialize(assignmentsToDelete));
        System.debug('activities: ' + JSON.serialize(activities));
        System.debug('newAssignmentList: ' + JSON.serialize(newAssignmentList));

        return persistAssignmentChanges(
                assignmentsToDelete,
                activities,
                newAssignmentList
        );
    }

    /**
     * This method is used for building Tradesman+Activity Id pairs for excluding records from deletion.
     *
     * @param activities
     * @param tradesmenIds
     *
     * @return Set<String>
     */
    private Set<String> buildTradesmanActivityKeySet(List<Work_Order_Activity__c> activities, List<Id> tradesmenIds) {
        Set<String> keySet = new Set<String>();

        if(activities != null && tradesmenIds != null) {
            for(Work_Order_Activity__c activity : activities) {
                for(Id tradesmanId : tradesmenIds) {
                    keySet.add(String.valueOf(tradesmanId) + String.valueOf(activity.Id));
                }
            }
        }

        return keySet;
    }

    /**
     * This method is user for creating map of assignments by special key that consists from Ids of Tradesman and Activity
     * This is used for building new assignments and excluding pairs that already have assignment.
     *
     * @param unFilteredAssignmentsToDelete
     * @param keyFields
     *
     * @return Map<String, Work_Order_Activity_Assignment__c>
     */
    private Map<String, Work_Order_Activity_Assignment__c> buildAssignmentByKeyMap(List<Work_Order_Activity_Assignment__c> unFilteredAssignmentsToDelete, List<String> keyFields) {
        Map<String, Work_Order_Activity_Assignment__c> assignmentsByTradesmanActivityKey = new Map<String, Work_Order_Activity_Assignment__c>();

        for(Work_Order_Activity_Assignment__c assignment : unFilteredAssignmentsToDelete) {
            assignmentsByTradesmanActivityKey.put(HOG_GeneralUtilities.buildKey(assignment, keyFields), assignment);
        }

        return assignmentsByTradesmanActivityKey;
    }

    /**
     * This method builds new WOA assignments based on provided list of Activities and Tradesmen.
     * It also uses map containing existing assignments and special key (Tradesman + Activity Ids) to skip creation
     * for Assignments that already exist.
     *
     * @param activities
     * @param tradesmenIds
     * @param unchangedAssignmentsByTradesmanActivityKey
     *
     * @return List<Work_Order_Activity_Assignment__c>
     */
    private List<Work_Order_Activity_Assignment__c> buildNewAssignments(
            List<Work_Order_Activity__c> activities,
            List<Id> tradesmenIds,
            Map<String, Work_Order_Activity_Assignment__c> unchangedAssignmentsByTradesmanActivityKey) {

        List<Work_Order_Activity_Assignment__c> newAssignmentList = new List<Work_Order_Activity_Assignment__c>();

        for (Work_Order_Activity__c activityRecord : activities) {
            for (Id tradesmanId : tradesmenIds) {

                Work_Order_Activity_Assignment__c assignmentRecord = new Work_Order_Activity_Assignment__c();
                assignmentRecord.Tradesman__c = tradesmanId;
                assignmentRecord.Work_Order_Activity__c = activityRecord.Id;
                if(!unchangedAssignmentsByTradesmanActivityKey.containsKey(HOG_GeneralUtilities.buildKey(assignmentRecord, KEY_FIELDS))) {
                    newAssignmentList.add(assignmentRecord);
                }
            }
        }

        return newAssignmentList;
    }

    /**
     * This method builds set of Ids of Activity that assignments needs to be revisited and deleted if necessary.
     * It also clean Assigned Vendor and Assigned Text if proper params are provided.
     * If PRESERVE Vendor ID Action is used, Vendor Account Id is used to populate Assigned Vendor for provided
     * activities.
     *
     * @param activities
     * @param vendorAccountId
     * @param assignedTextAction
     * @param vendorIdAction
     *
     * @return Set<Id>
     */
    private Set<Id> getActivityIdsForDeletionAndClearAssignments(
            List<Work_Order_Activity__c> activities,
            Id vendorAccountId,
            VTT_WOA_Assignment_AssignedTextAction assignedTextAction,
            VTT_WOA_Assignment_VendorIdAction vendorIdAction) {

        Set<Id> activitySetToDeleteAssignments = new Set<Id>();

        for (Work_Order_Activity__c activityRecord : activities) {

            if (activityRecord.Assigned_Vendor__c <> null && activityRecord.Assigned_Vendor__c <> vendorAccountId) {
                activitySetToDeleteAssignments.add(activityRecord.Id);
                if(VTT_WOA_Assignment_VendorIdAction.CLEAR.equals(vendorIdAction)) {
                    activityRecord.Assigned_Vendor__c = null;
                }
                if(VTT_WOA_Assignment_AssignedTextAction.CLEAR.equals(assignedTextAction)) {
                    activityRecord.Assigned_Text__c = null;
                }
            }

            if(VTT_WOA_Assignment_VendorIdAction.PRESERVE.equals(vendorIdAction)) {
                activityRecord.Assigned_Vendor__c = vendorAccountId;
            }
        }

        return activitySetToDeleteAssignments;
    }

    /**
     * Persist provided records.
     * deletes assignments, updates activities and upserts new assignments.
     * If any part of the logic fails, it will do rollback, and throw an custom error.
     * Otherwise it will return SUCCESS response.
     *
     * @param assignments
     * @param activities
     * @param newAssignmentList
     *
     * @return VTT_WOA_Assignment_Response
     */
    private VTT_WOA_Assignment_Response persistAssignmentChanges(
            List<Work_Order_Activity_Assignment__c> assignments,
            List<Work_Order_Activity__c> activities,
            List<Work_Order_Activity_Assignment__c> newAssignmentList) {

        final Savepoint savePoint = Database.setSavepoint();

        try {
            delete assignments;
            update activities;
            upsert newAssignmentList;
        } catch (Exception e) {
            Database.rollback(savePoint);
            HOG_ExceptionUtils.throwError(e.getMessage());
        }

        return VTT_WOA_Assignment_Response.SUCCESS;
    }

}