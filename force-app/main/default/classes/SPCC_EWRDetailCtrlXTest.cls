/**
 * Created by mzelina@sk.ibm.com on 14/01/2019.
 */
@IsTest
private class SPCC_EWRDetailCtrlXTest {

    public static Final Date startDate = Date.today();
    public static Final Date endDate = Date.today() + 1;

    @testSetup static void setup() {
        SPCC_Utilities.executeTriggerCode = false;

        SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('TestProject', '12345', '','Other');
        insert ewr;

        SPCC_Engineering_Work_Request__c ewr2 = SPCC_TestData.createEWR('TestProject 2', '54321', '','Other');
        insert ewr2;

        SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('A1-F7GH56-4D-J7-K2', 'A1-F7GH56-4D-J7-K2', 'Test AFE', ewr.Id);
        insert afe;

        SPCC_Authorization_for_Expenditure__c afe2 = SPCC_TestData.createAFE('A1-F7GH56-4D-J7-K3', 'A1-F7GH56-4D-J7-K3', 'Test AFE 2', ewr2.Id);
        insert afe2;

        SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, 'Test Category',
                'Test Description',
                '6000000',
                'Test Cose Element',
                5000,
                0);
        insert costElement;

        SPCC_Cost_Element__c costElement2 = SPCC_TestData.createCostElement(afe2.Id, 'Test Category',
                'Test Description 2',
                '6000000',
                'Test Cose Element 2',
                10000,
                0);
        insert costElement2;

        Account vendor = SPCC_TestData.createSPCCVendor('Test Account 1');
        insert vendor;

        //Setup - Add Blanket Purchase Order with BPOLI
        SPCC_Blanket_Purchase_Order__c bpo = SPCC_TestData.createBPO('1111111111', vendor.Id, startDate, endDate);
        insert bpo;

        SPCC_BPO_Line_Item__c bpoLi = SPCC_TestData.createBpoLineItem(bpo.Id, '6000000', 5000, '0010', 'Test BPO Line Item');
        insert bpoLi;

        SPCC_Service_Entry_Sheet__c ses1 = SPCC_TestData.createSESforBPO(bpo.Id, bpoLi.Id, 2000, startDate, 'ses 1');
        ses1.Cost_Element__c = costElement.Id;
        insert ses1;

        SPCC_Service_Entry_Sheet__c ses2 = SPCC_TestData.createSESforBPO(bpo.Id, bpoLi.Id, 1000, startDate, 'ses 2');
        ses2.Cost_Element__c = costElement.Id;
        insert ses2;

        SPCC_Service_Entry_Sheet__c ses3 = SPCC_TestData.createSESforBPO(bpo.Id, bpoLi.Id, 8000, startDate, 'ses 3');
        ses3.Cost_Element__c = costElement2.Id;
        insert ses3;
    }

    @IsTest
    static void testController() {

        //Setup -- Query Setup Objects
        List<SPCC_Engineering_Work_Request__c> ewrList = [Select Id, Name, EWR_Number__c
        From SPCC_Engineering_Work_Request__c];
        User runningUser = [Select Id From User Where Id =: UserInfo.getUserId()];

        System.runAs(runningUser) {

            Test.startTest();

            PageReference pRef = Page.SPCC_EWRDetail;
            Test.setCurrentPage(pRef);

            //Create Controller
            ApexPages.StandardController stdController = new ApexPages.StandardController(ewrList[0]);
            SPCC_EWRDetailCtrlX extCtrl = new SPCC_EWRDetailCtrlX(stdController);

            System.assertEquals(3000, extCtrl.getBPOs()[0].totalAmount);
            System.assertEquals(1, extCtrl.getBPOs().Size());

            Test.stopTest();
        }
    }
}