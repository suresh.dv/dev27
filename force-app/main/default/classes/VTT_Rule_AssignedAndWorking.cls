/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_Rule_AssignedAndWorking check to see which actions can user do when he is already
                working on activity
Test Class:     VTT_LTNG_WorkFlowEngineTest
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public with sharing class VTT_Rule_AssignedAndWorking implements VTT_LTNG_EngineRule {
    Work_Order_Activity__c activity;
    Contact tradesman;
    Work_Order_Activity_Log_Entry__c lastLogEntry;
    public Boolean hasEPDSubmitted;
    public Boolean isEPDFormRequired;
    public Boolean isEPDFormAvailable = false;

    Set<VTT_LTNG_EngineMessage> messages = new Set<VTT_LTNG_EngineMessage>();
    Set<VTT_LTNG_EngineAction> availableActions = new Set<VTT_LTNG_EngineAction>();
    Boolean passed = false;

    public VTT_Rule_AssignedAndWorking(
            Work_Order_Activity__c activity,
            Contact tradesman,
            Work_Order_Activity_Log_Entry__c lastLogEntry,
            Boolean hasEPDSubmitted,
            Boolean isEPDFormRequired
    ) {
        this.activity = activity;
        this.tradesman = tradesman;
        this.lastLogEntry = lastLogEntry;
        this.hasEPDSubmitted = hasEPDSubmitted;
        this.isEPDFormRequired = isEPDFormRequired;
    }

    public Boolean passedRule() {
        return this.passed;
    }

    public void processRule() {
        //this.availableActions.add(new VTT_LTNG_EngineAction('CanCancelActivity'));
        if (activity.Status__c == VTT_Utilities.ACTIVITY_STATUS_ONHOLD || lastLogEntry.Status__c == VTT_Utilities.LOGENTRY_JOBONHOLD) {
            this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTJOB));
            if (VTT_Utilities.IsThermalActivity(activity)) {

                this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_LTNG_WorkFlowEngineConstants.START_JOB_AT_EQUIPMENT));
            }

            // EPD
            addEPD(hasEPDSubmitted, isEPDFormRequired);

            return;
        } else {
            //if there are any other tradesman working on this activity, you can't put this activity on Hold
            Boolean canPutOnHold = true;
            for (Contact workingTradesman : activity.Working_Tradesmen__r) {
                if (workingTradesman.Id != tradesman.Id) {
                    canPutOnHold = false;
                    break;
                }
            }
            if (canPutOnHold) {
                this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBONHOLD));
            }
        }

        if (lastLogEntry.Status__c == VTT_Utilities.LOGENTRY_STARTJOB) {
            this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTATSITE));
            // EPD
            addEPD(hasEPDSubmitted, isEPDFormRequired);
        }

        if (lastLogEntry.Status__c == VTT_Utilities.LOGENTRY_STARTATSITE) {
            this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_FINISHEDATSITE));
            //ssd New workflow to complete job while working
            if (VTT_Utilities.IsThermalActivity(activity) && (!isEPDFormRequired || hasEPDSubmitted)) {

                this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBCOMPLETE));
            }

            // EPD
            addEPD(hasEPDSubmitted, isEPDFormRequired);
        }

        if (lastLogEntry.Status__c == VTT_Utilities.LOGENTRY_FINISHEDATSITE) {
            this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTATSITE));
            this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY));
            if (!isEPDFormRequired || hasEPDSubmitted) {
                this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBCOMPLETE));
            }

            // EPD
            addEPD(hasEPDSubmitted, isEPDFormRequired);
            System.debug('woActivity.Working_Tradesmen__r new ' + activity.Working_Tradesmen__r);
            //if there are any other tradesman working on this activity, you can't put this activity on Hold
            Boolean canPutOnHold = true;
            for (Contact workingtradesman : activity.Working_Tradesmen__r) {
                if (workingtradesman.Id != tradesman.Id) {
                    //ssd always allow to complete job but if other people working simply finish for the day for the current
                    //tradesman
                    canPutOnHold = false;
                }
            }
            if (canPutOnHold) {
                this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_JOBONHOLD));
            }
            return;
        }

        if (lastLogEntry.Status__c == VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY) {
            this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_Utilities.LOGENTRY_STARTJOB));
            if (VTT_Utilities.IsThermalActivity(activity)) {
                this.availableActions.add(VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_LTNG_WorkFlowEngineConstants.START_JOB_AT_EQUIPMENT));
            }
            // EPD
            addEPD(hasEPDSubmitted, isEPDFormRequired);
            return;
        }
    }

    private void addEPD(Boolean hasEPDSubmitted, Boolean isEPDFormRequired) {
        this.isEPDFormAvailable = true;
        if (!hasEPDSubmitted && isEPDFormRequired) {
            this.availableActions.add(
                    VTT_LTNG_WorkFlowEngineConstants.WORKFLOW_ACTION_MAP.get(VTT_LTNG_WorkFlowEngineConstants.EPD_LABEL)
            );
        }
    }

    public Set<VTT_LTNG_EngineMessage> getMessages() {
        return messages;
    }

    public Set<VTT_LTNG_EngineAction> getAvailableActions() {
        return availableActions;
    }
}