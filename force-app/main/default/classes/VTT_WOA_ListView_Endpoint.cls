/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Endpoint that handles actions for VTT Activity List View
Test Class:     VTT_WOA_ListView_EndpointTest
History:        jschn 28/08/2019 - Created.
*************************************************************************************************/
public without sharing class VTT_WOA_ListView_Endpoint {

    private static final String CLASS_NAME = String.valueOf(VTT_WOA_ListView_Endpoint.class);

    public static VTT_WOA_ListViewDAO woaSelector = new VTT_WorkOrderSelector();

    /**
     * Retrieves (WO) records based on filter string (should be in VTT_WOA_SearchCriteria class format), sorting, offset
     * and queryLimit.
     *
     * @param filterString
     * @param sorting
     * @param offset
     * @param queryLimit
     *
     * @return List<HOG_Maintenance_Servicing_Form__c>
     */
    @AuraEnabled
    public static List<HOG_Maintenance_Servicing_Form__c> getRecordsByFilter(String filterString, String sorting, Integer offset, Integer queryLimit) {
        VTT_WOA_SearchCriteria filter = (VTT_WOA_SearchCriteria) JSON.deserialize(filterString, VTT_WOA_SearchCriteria.class);
        return woaSelector.getRecordsByFilter(filter, sorting, offset, queryLimit);
    }

    /**
     * Runs auto assignment logic on provided activities.
     * Only works if at least on activity is present.Either way it returns SUCCESS string
     *
     * @param activities
     *
     * @return String
     */
    @AuraEnabled
    public static String runAutoAssignmentOnWOAs(List<Work_Order_Activity__c> activities) {

        if(activities != null && activities.size() > 0) {
            System.debug(CLASS_NAME + ' -> runAutoAssignmentOnWOAs. Execute AutoAssignments for activities: ');
            System.debug(CLASS_NAME + ' -> runAutoAssignmentOnWOAs. ' + activities);

            VTT_Utilities.AutoAssignActivities(activities, null);
        }

        return 'SUCCESS';
    }

    /**
     * Updates tradesman Started work on provided contact.
     * If logic fails, it will rethrow exception with additional information.
     * After everything runs well, Tradesman Info (Contact) is reloaded and returned.
     *
     * @param tradesman
     *
     * @return Contact
     */
    @AuraEnabled
    public static Contact startWork(Contact tradesman) {
        try {
            tradesman.Tradesman_Status__c = VTT_Utilities.TRADESMAN_STATUS_STARTEDWORK;
            tradesman.Tradesman_Status_Date__c = Datetime.now();
            tradesman.Tradesman_Still_Working__c = true;
            update tradesman;
        } catch(DmlException ex) {
            System.debug(CLASS_NAME + ' -> startWork. DML Exception:' + ex.getMessage());
            System.debug(CLASS_NAME + ' -> startWork. Stack Trace:' + ex.getStackTraceString());
            throw new HOG_Exception('Cannot start work: ' + ex.getMessage());
        } finally {
            tradesman = new VTT_Utility_EndpointHandler().loadTradesmanInfo();
        }
        return tradesman;
    }

}