/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VGTasksEmailNotifications.cls
History:        jschn 05.02.2018 - Created.
**************************************************************************************************/
@IsTest
private class HOG_VGTasksEmailNotificationsTest {
	
	@IsTest static void testBatchNotifications_NonUrgent() {

		Test.startTest();
		Database.executeBatch(new HOG_VGTasksEmailNotifications(HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_TASKS));
		
		Integer dmlRowsBfr = Limits.getDmlRows();
		Integer dmlStatementsBfr = Limits.getDmlStatements();
		Integer queriesBfr = Limits.getQueries();

		Test.stopTest();
		Integer dmlRowsAftr = Limits.getDmlRows();
		Integer dmlStatementsAftr = Limits.getDmlStatements();
		Integer queriesAftr = Limits.getQueries();

		System.assertNotEquals(dmlStatementsBfr, dmlStatementsAftr);
		System.assertNotEquals(dmlRowsBfr, dmlRowsAftr);
		System.assertNotEquals(queriesBfr, queriesAftr);
		System.assertEquals(11,dmlRowsAftr);
		System.assertEquals(11,dmlStatementsAftr);
		//System.assertEquals(3,queriesAftr);
	}

	@IsTest static void testBatchNotifications_Urgent() {
		Test.startTest();
		Database.executeBatch(new HOG_VGTasksEmailNotifications(HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_TASKS));

		Integer dmlRowsBfr = Limits.getDmlRows();
		Integer dmlStatementsBfr = Limits.getDmlStatements();
		Integer queriesBfr = Limits.getQueries();

		Test.stopTest();
		Integer dmlRowsAftr = Limits.getDmlRows();
		Integer dmlStatementsAftr = Limits.getDmlStatements();
		Integer queriesAftr = Limits.getQueries();

		System.assertNotEquals(dmlStatementsBfr, dmlStatementsAftr);
		System.assertNotEquals(dmlRowsBfr, dmlRowsAftr);
		System.assertNotEquals(queriesBfr, queriesAftr);
		System.assertEquals(11,dmlRowsAftr);
		System.assertEquals(11,dmlStatementsAftr);
		//System.assertEquals(3,queriesAftr);
	}
	
	@TestSetup static void createData() {
		insert new HOG_Vent_Gas_Alert_Configuration__c(
				Urgent_Priority_Alert_Daily_Notification__c = true,
				Medium_Priority_Alert_Daily_Notification__c = true,
				Low_Priority_Alert_Daily_Notification__c = true,
				High_Priority_Alert_Daily_Notification__c = true,
				Default_Notification_Sender_Name__c = 'Bruce Wayne'
		);
		Location__c loc = HOG_VentGas_TestData.createLocation();
		User user1 = HOG_VentGas_TestData.createUser('Bruce', 'Wayne', 'Batman');
		User user2 = HOG_VentGas_TestData.createUser('Nick', 'Grayson', 'Robin');

		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
																		'TestDescription', 
																		loc.Id,
																		user1.Id, 
																		HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
																		HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
																		HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		HOG_Vent_Gas_Alert_Task__c t = HOG_VentGas_TestData.createTask(alert.Id, 
																	user1.Id, 
																	user2.Id, 
																	'TestTask', 
																	alert.Priority__c, 
																	true, 
																	HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																	HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
		t = HOG_VentGas_TestData.createTask(alert.Id, 
											user1.Id, 
											null, 
											'TestTask', 
											alert.Priority__c, 
											false, 
											HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
											HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
	}

}