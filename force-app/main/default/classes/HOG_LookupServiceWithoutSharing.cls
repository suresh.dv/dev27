/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Trampoline class that disables sharing settings for session.
Test Class:     HOG_LookupServiceLtngTest
History:        mbrimus 2020-01-22 - Created.
*************************************************************************************************/
public without sharing class HOG_LookupServiceWithoutSharing {

    public List<SObject> getData(String queryString) {
        return new HOG_LookupServiceDAO().getData(queryString);
    }
}