public with sharing class Glycol_AddMultiBranchEquipCtrl 
{
    //select/deselect item in contect from the page
    public String contextEquip {get;set;}
    public string searchString{get;set;} // search keyword
    public List<Glycol_Branch_Equipment_Association__c> selectedEquipments {get;set;}
    public Glycol_Manifold_Branch__c currentBranch {get;set;}
    public Glycol_Piping_Line_Equipment__c newEquip {get;set;}
    public boolean displayMsg {get;set;}
    // Number of results listing page
    private final Integer RESULT_PER_PAGE = 20;
    private String branchId;
    private Set<Id> existingEquipIds;
    
    public Glycol_AddMultiBranchEquipCtrl()
    {
        branchId = ApexPages.currentPage().getParameters().get('branchId');
        if (branchId != null)
        {
            currentBranch = [select Id, Return_Manifold__c, Return_Branch_Num__c, Supply_Manifold__c, 
                                    Supply_Branch_Num__c 
                            FROM Glycol_Manifold_Branch__c 
                            WHERE Id =: branchId];
            loadSelectedEquipmentList();
        }
        newEquip = new Glycol_Piping_Line_Equipment__c();
        displayMsg = false;
        
    }
    public void loadSelectedEquipmentList()
    {
        selectedEquipments = 
                   [SELECT Id, Piping_Line_Equipment__c, Piping_Line_Equipment__r.Name, 
                           Piping_Line_Equipment__r.Line_Size__c 
                   FROM Glycol_Branch_Equipment_Association__c 
                   WHERE Manifold_Branch__c =: branchId];
        existingEquipIds = new Set<Id>();           
        for(Glycol_Branch_Equipment_Association__c item : selectedEquipments)
        {
            existingEquipIds.add(item.Piping_Line_Equipment__c);
        }
    }
    public List<Glycol_Piping_Line_Equipment__c> availableEquipments
    {
        get
        {
            if (queryController != null)
            {  
                return (List<Glycol_Piping_Line_Equipment__c>)queryController.getRecords();
            }
            return null;   
        }
    }   
      // performs the keyword search
    public void runSearch() 
    {
        System.debug('run search');
        try
        {
            queryController = new Apexpages.Standardsetcontroller(
                      Database.getQueryLocator(performSearch()));
            queryController.setPageSize(RESULT_PER_PAGE);
        }
        catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                                                       String.valueOf(e)));
        }
      
    }
    private Apexpages.Standardsetcontroller queryController
    {
        get
        {
            if (queryController == null)
            {
                queryController = new Apexpages.Standardsetcontroller(
                      Database.getQueryLocator(performSearch()));
                queryController.setPageSize(RESULT_PER_PAGE);
            }
            return queryController;
        }
        set;
    }  
    
    //Add the Equipment to selected branch
    public void AddEquipment()
    {
        System.debug('add equipment');
        try
        {
            Id equipId = System.currentPageReference().getParameters().get('equipId');
            Glycol_Piping_Line_Equipment__c selectedEquip; 
            for(Glycol_Piping_Line_Equipment__c item : availableEquipments)
            {
                if (item.Id == equipId)
                {
                   selectedEquip = item;
                   break;
                }
            }
            if (selectedEquip != null)
            {
                Glycol_Branch_Equipment_Association__c branchEquip = new Glycol_Branch_Equipment_Association__c();
                
                branchEquip.Manifold_Branch__c = branchId;
                branchEquip.Piping_Line_Equipment__c = equipId;
                branchEquip.Piping_Line_Equipment__r = selectedEquip;
                
                
                insert branchEquip;
                
                selectedEquipments.add(branchEquip);
                existingEquipIds.add(equipId);
                runSearch();
            }
            
        }
        catch(DmlException ex)
        {
            ApexPages.addMessages(ex);
        }
    }
    // removing Equipment from the branch
    public void RemoveEquipment()
    {
        System.debug('remove equipment');
        try
        {
            Id associateId = System.currentPageReference().getParameters().get('associateId');
            
            integer index= 0;
           
            for(Glycol_Branch_Equipment_Association__c item : selectedEquipments)
            {
                if (item.id == associateId)
                {
                    Id equipId = item.Piping_Line_Equipment__c;
                    delete item;
                    Glycol_Branch_Equipment_Association__c removedItem = selectedEquipments.remove(index);
                    existingEquipIds.remove(equipId);
                    break;
                }
                index++;
            }
            runSearch();
        }
        catch(DmlException ex)
        {
            ApexPages.addMessages(ex);
        }
    }
   
    public void SaveEquipment()
    {
        insert newEquip;
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'An equipment has been created successful');//Same old drama 
        ApexPages.addMessage(myMsg);
        newEquip = new Glycol_Piping_Line_Equipment__c();
        runSearch() ;
       
    }
    public void clearMsg()
    {
        if (ApexPages.hasMessages())
           ApexPages.getMessages().clear();
    }
      // --------------------------------------------------------------------------
    // Go to the first page
    public void first() {
        queryController.first();
        
    }
    
    // --------------------------------------------------------------------------
    // Go to the last page
    public void last() {
        queryController.last();
        
    }
    
    // --------------------------------------------------------------------------
    // Go to the previous page
    public void previous() {
        if (hasPrevious)
            queryController.previous();
    }
    
    // --------------------------------------------------------------------------
    // Go to the next page
    public void next() {
        if (hasNext)
            queryController.next();
    }
    
    // Checks whether there are more search result.
    public Boolean hasNext {
        get{
            return queryController.getHasNext();
        }
    }
    
    // Checks whther there are previous result
    public Boolean hasPrevious {
        get{
            return queryController.getHasPrevious();
        }
    }
    
    // Current page number
   public Integer pageNumber {
        get{
            return queryController.getPageNumber();
        }
   }
   public Integer resultSize
   {
        get{
            return queryController.getResultSize();
        }
   } 
   public integer pageSize
   {
        get{
            return queryController.getPageSize();
        }
   }
   public String getPaginationInfo()
   {
     // {!IF(queryController.PageNumber == 1,1,((queryController.PageNumber -1) * queryController.PageSize)+1)}-{!IF(queryController.resultSize < queryController.PageSize * queryController.PageNumber,queryController.resultSize,queryController.PageNumber * queryController.pageSize)} of {!queryController.resultSize}
        String txt = '';
        if (pageNumber == 1)
            txt += 1;
        else
            txt += String.valueOf((pageNumber - 1) * pageSize + 1);
        txt += '-';
        if (resultSize < pageSize * pageNumber)
            txt += resultSize;
        else
            txt += pageSize * pageNumber;
        
        txt += ' of ' + resultSize;        
        return txt;        
   } 
      // run the search and return the records found. 
    private String performSearch() 
    {
        System.debug('searchString =' + searchString);
        String soql = 'select id, name, Line_Size__c from Glycol_Piping_Line_Equipment__c' ;
        soql += ' where Id not in :existingEquipIds';  
        
        if(searchString != '' && searchString != null)
          soql +=   ' and name LIKE \'' + searchString +'%\'';
        
        soql += ' order by Name';
        System.debug('soql =' + soql);
        return soql;
    
    }
    public PageReference GoBackManifoldBranch()
    {
        PageReference nextPage = new PageReference('/' + branchId);
        nextPage.setRedirect(true);
        return nextPage;
    }
}