/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_TerminalListControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_Terminals;
            Test.setCurrentPageReference(pageRef);


            //lets create some test terminal
            USCP_Terminal__c terminal =   USCP_TestData.createTerminal('trm1', 'addr1', 'city1', 'state1', 'opis1', true);
            System.AssertNotEquals(terminal.Id, Null);
            //lets create another test terminal
            terminal =   USCP_TestData.createTerminal('trm2', 'addr2', 'city2', 'state2', 'opis2', true);
            System.AssertNotEquals(terminal.Id, Null);
            
         
            //create test product
            Product2 product = USCP_TestData.createProduct('product1',true);
            //associate this product with second terminal            
            USCP_TestData.createTerminalProductAssociation(terminal.id, product.id, true);
            //create another test product
            product = USCP_TestData.createProduct('product2',true);
            //associate this product with second terminal            
            USCP_TestData.createTerminalProductAssociation(terminal.id, product.id, true);
        
            //now lets create controller
            USCP_TerminalListController controller = new USCP_TerminalListController();
            //we should have 2 records           
            System.AssertEquals(2, controller.getTerminalRecords().size());             
            //we should have 2 records            
            System.AssertEquals(2, controller.getAllTerminalRecords().size());              

            //we should have 3 records (2 with empty select option)
            System.AssertEquals(3, controller.States.size());  
            System.AssertEquals(3, controller.Cities.size());  
            System.AssertEquals(3, controller.OPISMarkets.size()); 
            System.AssertEquals(3, controller.Products.size()); 
            
            //set search parameters for the controller
            controller.Terminal = '1';
            controller.City = '1';
            controller.State= 'state1';
            controller.OPISMarket= '1';       

            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getTerminalRecords().size());    

            controller.ClearFilter();  
            //we should have 2 record now
            System.AssertEquals(2, controller.getTerminalRecords().size());    

            //lets search by product
            controller.Product= '1';
            controller.RefreshData();  
            //we should have 1 record now
            System.AssertEquals(1, controller.getTerminalRecords().size());    
            controller.ClearFilter();  
            
            controller.search();  
            controller.sort();  
            controller.first();  
            controller.previous();  
            controller.next();  
            controller.last();  
            controller.getHasPrevious();  
            controller.getHasNext();  
            controller.getResultSize();  
            controller.getPageCount();  
            controller.getPageNumber();  
            controller.save();  
            controller.cancel();  
            controller.getcontroller();  
        }
    }

}