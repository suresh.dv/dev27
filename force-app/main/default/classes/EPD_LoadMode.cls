/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Modes for load actions for EPD Form
History:        jschn 2019-06-04 - Created. - EPD R1
*************************************************************************************************/
public enum EPD_LoadMode {

    DIRECT_LOAD,
    INDIRECT_LOAD

}