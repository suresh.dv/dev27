/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Schedulable class for sending notifications for Vent Gas Alerts and Tasks
Test class:     HOG_VGEmailNotificationsSchedulableTest
History:        sdhond 04.23.2018 - Created.
				jschon 04.26.2018 - added reschedule ability and Tasks Notifications batch
				jschon 25/10/2018 - Added batch job start for urgent and non urgent alerts separately
								- US W-001290, W-001293, W-001304
**************************************************************************************************/
global class HOG_VGEmailNotificationsSchedulable implements Schedulable {

	public static final String RELATED_JOB_NAME 	= 'HOG - PVR Average Volume Update Batch' + (Test.isRunningTest() ? ' Test':'');
	public static final String JOB_NAME 			= 'HOG Vent Gas Notification' + (Test.isRunningTest() ? ' Test':'');
	public static final String RESCHEDULED_JOB_NAME 	= 'HOG Vent Gas Notification Retry' + (Test.isRunningTest() ? ' Test':'');

	private static final Integer BATCH_JOB_CHUNK_SIZE = 2000;

	/**
	* Checks if related job has runned already.
	* If not reschedule self to run 30 mins later.
	* If yes run notification batch job.
	*/
	global void execute(SchedulableContext sc) {
		CronTrigger pvrRollupJob = getPVRRollUpJob();
		if(pvrRollupJob == null || pvrRollupJobRunned(pvrRollupJob)) {
			HOG_ScheduleUtilities.abortCompletedJobsLike(RESCHEDULED_JOB_NAME);
			runNotificationBatchJobs();
		} else {
			rescheduleRun();
		}
	}

	/**
	* Retrieve number of completed related jobs.
	* Related job is HOG PVR Roll up to get well event values to location.
	*/
	private CronTrigger getPVRRollUpJob() {
		List<CronJobDetail> details = [SELECT Id FROM CronJobDetail WHERE Name =: RELATED_JOB_NAME];
		if(details != null && details.size() > 0) {
			List<CronTrigger> triggers = [SELECT Id, NextFireTime, CronExpression
					FROM CronTrigger
					WHERE CronJobDetailId = :details.get(0).Id];
			System.debug('triggers: ' + triggers);
			return triggers[0];
		}
		return null;
	}

	/**
	* Checks if PVR Roll up scheduled batch job has been runned today already.
	*/
	private Boolean pvrRollupJobRunned(CronTrigger pvrRollupJob) {
		Datetime nextFireTime = pvrRollupJob.NextFireTime;
		Date nextRun = Date.newInstance(nextFireTime.year(), nextFireTime.month(), nextFireTime.day());
		Date today = Date.today();
		System.debug('nextRun: ' + nextRun + ' today: ' + today);
		return today < nextRun;
	}

	/**
	* Gets crone String which targets run for 30 mins later.
	*/
	private String getCroneString() {
		Datetime t = Datetime.now().addMinutes(30);
		return '0 '
			+ String.valueOf(t.minute())
			+ ' '
			+ String.valueOf(t.hour())
			+ ' '
			+ String.valueOf(t.day())
			+ ' '
			+ String.valueOf(t.month())
			+ ' ? '
			+ String.valueOf(t.year());
	}

	/**
	* Create scheduled job which will run only once (non repeatable job) for same class.
	*/
	private void rescheduleRun() {
		//check if reschedule job already scheduled
		String retryJobNameLike = RESCHEDULED_JOB_NAME + '%';
		List<CronJobDetail> cronJobDetails = [SELECT Id FROM CronJobDetail WHERE Name LIKE :retryJobNameLike];

		if(cronJobDetails.size() <= 0) {
			HOG_VGEmailNotificationsSchedulable m = new HOG_VGEmailNotificationsSchedulable();
			String sch = getCroneString();
			System.schedule(RESCHEDULED_JOB_NAME + ' - ' + String.valueOf(Datetime.now()), sch, m);
		}
	}

	/**
	* Run notifications batch jobs for VG Alert and VG Alert Tasks.
	*/
	private void runNotificationBatchJobs() {
		Database.executeBatch(new HOG_VGTasksEmailNotifications(
			HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_TASKS), BATCH_JOB_CHUNK_SIZE);
		Database.executeBatch(new HOG_VGTasksEmailNotifications(
				HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_TASKS), BATCH_JOB_CHUNK_SIZE);
		Database.executeBatch(new HOG_VGEmailNotifications(
			HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS), BATCH_JOB_CHUNK_SIZE);
		Database.executeBatch(new HOG_VGEmailNotifications(
			HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS), BATCH_JOB_CHUNK_SIZE);
	}

	@TestVisible public void runRescheduleRun() {
		rescheduleRun();
	}

}