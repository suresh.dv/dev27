/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of HOG_VG_AlertStrategyProvider for Low Variance Alerts.
                This class contains logic for deciding whether Low Variance Alert should be
                created.
Test Class:     HOG_VG_LowVarianceAlertStrategyPrvdrTest
History:        jschn 19/10/2018 - Created.
                jschn 19/03/2019 - added HOG_Vent_Gas_Alert_Configuration__c & isNormalProducer method ( US-001449 )
*************************************************************************************************/
public with sharing class HOG_VG_LowVarianceAlertStrategyProvider extends HOG_VG_AlertStrategyProvider {

    private HOG_Vent_Gas_Alert_Configuration__c ventGasSettings;

    public HOG_VG_LowVarianceAlertStrategyProvider(List<Location__c> locations,
            Map<Id, Location__c> oldLocationsMap,
            Map<String, HOG_Vent_Gas_Alert_Exemption_Request__c> ventGasExemptionsByLocationId,
            Map<String, HOG_Vent_Gas_Alert__c> activeAlertsByLocationAndType) {
        super(locations, oldLocationsMap, ventGasExemptionsByLocationId, activeAlertsByLocationAndType);
        ventGasSettings = HOG_Vent_Gas_Alert_Configuration__c.getInstance();
    }

    /**
     * Implementation of decision logic for creating Low Variance Alert.
     * Checks whether there is another active alert as well as if there is an active exemption on requested Location.
     * Last, it will check if there are all necessary conditions met.
     *
     * @param location
     *
     * @return
     */
    public override Boolean shouldCreate(Location__c location) {
        return !super.ventGasExemptionsByLocationId.containsKey(location.Id)
                && !super.activeAlertsByLocationAndType.containsKey(location.Id + HOG_VentGas_Utilities.ALERT_TYPE_LOW_VARIANCE)
                && lowVarianceTriggered(location)
                && isNormalProducer(location);
    }

    /**
     * Checks if there are all necessary conditions met for creating requested Alert.
     *
     * @param location
     *
     * @return
     */
    private Boolean lowVarianceTriggered(Location__c location) {
        return location.Low_Variance__c == 'YES'
                && super.isCorrectLocation(location)
                && correctDateToCreate();
    }

    /**
     * Checks if Oil Production (Monthly Trucked Oil) and PVR GOR Factor are different from zero(or null).
     * Also it checks if Production Hours of Location are bigger then value in Custom Settings.
     * On first line there is check, that will return true always when ventGasSettings are not present.
     *
     * @param location
     *
     * @return
     */
    private Boolean isNormalProducer(Location__c location) {
        if (ventGasSettings == null) return true;
        return location != null
                && location.Monthly_Trucked_Oil__c != 0
                && location.Monthly_Trucked_Oil__c != null
                && location.PVR_GOR_Factor__c != 0
                && location.PVR_GOR_Factor__c != null
                && location.PVR_Hours_On_Prod__c != null
                && location.PVR_Hours_On_Prod__c >= ventGasSettings.Low_Variance_Production_Hrs_Threshold__c;
    }

    /**
     * Compares today's day number with value from Vent Gas settings.
     *
     * @return
     */
    protected override Boolean correctDateToCreate() {
        Date today = Date.today();
        return today.day() == super.settings.Alerts_Create_Day_of_month__c;
    }

}