/**
 * Created by MartinBilicka on 19. 3. 2019.
 * Controller extension for Well Optimization "Create new" visualforce page.
 * Purpose is to include the autocomplete logic for Control Room Center,
 * Final Speed and Pump Rate fields when Well Location is chosen. It checks
 * the last Well Optimization with the same Well Location and use its values
 * and prepopulate them.
 */

public with sharing class WS_WellOptimizationControllerX {

    private ApexPages.StandardController sc = NULL;
    public Well_Optimization__c wellOpt {get; set;}
    private Well_Optimization__c wo;

    public WS_WellOptimizationControllerX(ApexPages.StandardController stdCtr){
        this.sc = stdCtr;
        this.wellOpt = (Well_Optimization__c)stdCtr.getRecord();
        if(String.isNotBlank(this.wellOpt.Well_Location__c)){
            autocompleteFields();
        }
    }

    /**
    * filling Control_Room_Centre__c, Pump_Rate_m_RPM_Day__c, Final_Speed__c
    * either with previous Well Optimization's (with same location) values
    * or with null/0 values if there is no previous Well Optimization
    */
    public void getValuesForWO(){
        //String.isNotBlank(wellOpt.Well_Location__c)
        List<Well_Optimization__c> woList = [
                SELECT Control_Room_Centre__c, Pump_Rate_m_RPM_Day__c, Final_Speed__c
                FROM Well_Optimization__c
                WHERE Well_Location__c = :wellOpt.Well_Location__c
                ORDER BY CreatedDate DESC
                LIMIT 1
        ];

        if (!woList.isEmpty()) {
            wo = woList[0];
        }else{
            wo = new Well_Optimization__c(Control_Room_Centre__c=NULL,Pump_Rate_m_RPM_Day__c=0, Final_Speed__c=0);
        }
    }

    /**
     * main method that is assigning values for autocomplete
     */
    public void autocompleteFields(){
        getValuesForWO();
        System.debug('wo = ' + wo);
        this.wellOpt.Control_Room_Centre__c=String.isNotBlank(wo.Control_Room_Centre__c) ? wo.Control_Room_Centre__c:NULL;
        this.wellOpt.Initial_Speed__c=wo.Final_Speed__c!=NULL ? wo.Final_Speed__c:0;
        this.wellOpt.Pump_Rate_m_RPM_Day__c=wo.Pump_Rate_m_RPM_Day__c!=NULL ? wo.Pump_Rate_m_RPM_Day__c:0;
    }

    /**
     * Method that mimics the OOTB Save and New button on the visualforce page
     */
    public Pagereference saveAndNew(){
        sc.save();
        PageReference pr = Page.WS_WellOptimizationCreate;
        pr.setRedirect(true);
        return pr;
    }
}