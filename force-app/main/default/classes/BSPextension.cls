public class BSPextension {
   
    private Boolean Upstream {get; set;}
    private Boolean Downstream {get; set;}
    private Boolean Corporate {get; set;}
    private final BP_Idea__c idea;
    public ApexPages.StandardController stdCntrlr {get; set;}
    public Integer projectDuration {get; private set;}
    public Integer year1 {get; private set;}
    public Integer year2 {get; private set;}
    public Integer year3 {get; private set;}
    
    
    public BSPextension(ApexPages.StandardController controller) {

        List<string> fields = new List<String>{'Stream__c','Upstream__c','Downstream__c','Corporate__c'};
        controller.addFields(fields);
        this.idea = (BP_Idea__c)controller.getRecord();
        
        
        if (idea.Stream__c == 'Upstream')
            
        {
            idea.Upstream__c = true;
            
        }
            if (idea.Stream__c == 'Downstream')
            
        {
            idea.Downstream__c = true;
            
        }
            if (idea.Stream__c == 'Corporate/Enterprise')
            
        {
            idea.Corporate__c = true;
            
        }
        
        Date startDate = idea.Estimated_Start_Date__c == null ? Date.today() : idea.Estimated_Start_Date__c;
        Date endDate = Date.today();
        
        //handle null end date and non null start date. Avoid negative month result
        if(idea.Estimated_End_Date__c == null && idea.Estimated_Start_Date__c != null){
            endDate = idea.Estimated_Start_Date__c;
        } else {
            endDate = idea.Estimated_End_Date__c;
        }
        
        try{
            //calculate the estimated project duration in months
            projectDuration = startDate.monthsBetween(endDate);
        } catch(Exception exp) {
            projectDuration = 0;
        }            
        
        year1 = getProjectYear(startDate == null ? Date.today() : startDate, 0); //current year = 0
        year2 = getProjectYear(startDate == null ? Date.today() : startDate, 1);
        year3 = getProjectYear(startDate == null ? Date.today() : startDate, 2);
              
    }
    
    private Integer getProjectYear(Date startDate, Integer pYear){
        
        Date nextYear = startDate.addYears(pYear);
        return nextYear.year();
    }
}