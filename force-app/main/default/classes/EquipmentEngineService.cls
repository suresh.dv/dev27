/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class containing all methods and business logic for Equipment Engine SObject.
Test Class:     EquipmentEngineServiceTest
                EquipmentEngineServiceTestNP
History:        jschn 2019-05-27 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EquipmentEngineService {

    private static final String CLASS_NAME = String.valueOf(EquipmentEngineService.class);

    /**
     * Loads Equipment Engine record from DB (with selector) and if inappropriate number of records are retrieved
     * it throws an exception. Expected number of records is 1.
     * If conditions are met, desired record is returned.
     *
     * @param equipmentId
     *
     * @return Equipment_Engine__c
     */
    public Equipment_Engine__c getEngineByEquipmentId(Id equipmentId) {
        Equipment_Engine__c engine;

        List<Equipment_Engine__c> engines = EPD_DAOProvider.equipmentEngineDAO.getEngineByEquipmentId(equipmentId);

        if(EPD_Utility.isSingleRecord(engines)) {
            engine = engines.get(0);
        } else {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Unexpected_Engines_for_EQ,
                    new List<String> {
                            equipmentId
                    }
            );
        }

        return engine;
    }

    /**
     * This method will update relevant information on Equipment Engine records based on parent Equipment records.
     *
     * @param equipments
     *
     * @return EquipmentEngineService
     */
    public EquipmentEngineService updateEngines(List<Equipment__c> equipments, EquipmentEngineSaveMode saveMode) {
        System.debug(CLASS_NAME + ' -> updateEngines. updating existing engines based on Equipments');
        List<Equipment_Engine__c> engines = getEnginesByEquipment(equipments);

        for(Equipment_Engine__c engine : engines) {
            updateEngineFields(engine);
        }

        if(EquipmentEngineSaveMode.ALL_OR_NOTHING_DISABLED.equals(saveMode)) {
            List<Database.SaveResult> results = Database.update(engines, false);
            handlePersistErrors(results, engines);
        } else {
            update engines;
        }
        return this;
    }

    /**
     * This method filters equipments without Equipment Engine records and creates those records for them.
     *
     * @param equipments
     *
     * @return EquipmentEngineService
     */
    public EquipmentEngineService createEngines(List<Equipment__c> equipments, EquipmentEngineSaveMode saveMode) {
        System.debug(CLASS_NAME + ' -> createEngines. creating engines based on Equipments');
        List<Equipment__c> equipmentsWithoutEngine = getFilteredEquipmentsWithoutEngine(equipments);
        List<Equipment_Engine__c> engines = new List<Equipment_Engine__c>();

        for(Equipment__c eq : equipmentsWithoutEngine) {
            engines.add(buildEngineFromEquipment(eq));
        }

        if(EquipmentEngineSaveMode.ALL_OR_NOTHING_DISABLED.equals(saveMode)) {
            List<Database.SaveResult> results = Database.insert(engines, false);
            handlePersistErrors(results, engines);
        } else {
            insert engines;
        }
        return this;
    }

    /**
     * Call Email Service that will send notification on persist errors.
     *
     * @param results
     */
    private void handlePersistErrors(List<Database.SaveResult> results, List<Equipment_Engine__c> engines) {
        try {
            new EquipmentEngineEmailService().sendNotificationOnFail(results, engines);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' -> handlePersistErrors. Exception occurred: ' + ex.getMessage());
        }
    }

    /**
     * This will query Equipment engines record based on provided Equipments.
     *
     * @param equipments
     *
     * @return List<Equipment_Engine__c>
     */
    private List<Equipment_Engine__c> getEnginesByEquipment(List<Equipment__c> equipments) {
        Set<Id> equipmentIds = HOG_GeneralUtilities.getSetOfIds(equipments);
        List<Equipment_Engine__c> engines = EPD_DAOProvider.equipmentEngineDAO.getEnginesByEquipmentIds(equipmentIds);
        return engines;
    }

    /**
     * Updates values from parent Equipment record to Engine record.
     *
     * @param engine
     */
    private void updateEngineFields(Equipment_Engine__c engine) {
        engine.Manufacturer__c = engine.Equipment__r.Manufacturer__c.toUpperCase();
        engine.Model__c = engine.Equipment__r.Model_Number__c.toUpperCase();
        engine.Tag__c = engine.Equipment__r.Tag_Number__c;
        engine.Serial_Number__c = engine.Equipment__r.Manufacturer_Serial_No__c;
        System.debug(CLASS_NAME + ' -> updateEngineFields. Updated record: ' + JSON.serialize(engine));
    }

    /**
     * This method filters out Equipments that have engine records already created.
     * First it will query all engine records based on equipments. Then it will extract all parent IDs from the list
     * and run through equipment list where it will check if current Equipment ID is in the extracted set of IDs.
     * If not, record is assigned to new List which will be returned in the end.
     *
     * @param equipments
     *
     * @return List<Equipment__c>
     */
    private List<Equipment__c> getFilteredEquipmentsWithoutEngine(List<Equipment__c> equipments) {
        Set<Id> equipmentIds = HOG_GeneralUtilities.getSetOfIds(equipments);
        List<Equipment_Engine__c> engines = EPD_DAOProvider.equipmentEngineDAO.getEnginesByEquipmentIds(equipmentIds);
        Set<Id> enginesParentIds = HOG_GeneralUtilities.getSetOfParentIds(engines, 'Equipment__c');

        List<Equipment__c> equipmentsWithoutEngine = new List<Equipment__c>();

        for(Equipment__c equipment : equipments) {
            if(!enginesParentIds.contains(equipment.Id)) {
                equipmentsWithoutEngine.add(equipment);
            }
        }

        return equipmentsWithoutEngine;
    }

    /**
     * Creates Equipment Engine record based on Equipment.
     *
     * @param equipment
     *
     * @return Equipment_Engine__c
     */
    private Equipment_Engine__c buildEngineFromEquipment(Equipment__c equipment) {
        String modelNumber = String.isNotBlank(equipment.Model_Number__c) ? equipment.Model_Number__c.toUpperCase() : '';
        String manufacturer = String.isNotBlank(equipment.Manufacturer__c) ? equipment.Manufacturer__c.toUpperCase() : '';
        return new Equipment_Engine__c(
                Equipment__c = equipment.Id,
                Manufacturer__c = manufacturer,
                Model__c = modelNumber,
                Tag__c = equipment.Tag_Number__c,
                Serial_Number__c = equipment.Manufacturer_Serial_No__c
        );
    }

}