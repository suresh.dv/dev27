/**
 * Created by mzelina@sk.ibm.com on 28/01/2019.
 */
public with sharing class SPCC_CostElementDetailCtrlX {

    private SPCC_Cost_Element__c costElement;
    private List<SPCC_BPO_Line_Item__c> bpoLineItemsList;
    private Set<Id> bpoLineItemsIdSet;

    public SPCC_CostElementDetailCtrlX(ApexPages.StandardController controller) {
        this.costElement = (SPCC_Cost_Element__c) controller.getRecord();
    }

    //fill the list of related blanket POs
    public List<SPCC_BPO_Line_Item__c> getBpoLineItems(){

        bpoLineItemsIdSet= new Set<Id>();

        for (SPCC_Service_Entry_Sheet__c ses : [SELECT BPO_Line_Item__c, Cost_Element__c
                                                FROM SPCC_Service_Entry_Sheet__c
                                                WHERE Cost_Element__c = :costElement.Id]){

            bpoLineItemsIdSet.add(ses.BPO_Line_Item__c);

        }

        bpoLineItemsList = new List<SPCC_BPO_Line_Item__c>([SELECT Name, Item__c, Committed_Amount__c, Incurred_Cost__c,
                                                                   Short_Text__c, Blanket_Purchase_Order__r.Name
                                                            FROM SPCC_BPO_Line_Item__c
                                                            WHERE Id IN :bpoLineItemsIdSet]);
        return bpoLineItemsList;
    }
}