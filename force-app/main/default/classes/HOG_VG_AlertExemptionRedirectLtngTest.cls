/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for class HOG_VG_AlertExemptionRedirectLtng
History:        jschn 2019-02-14 - Created.
                jschn 2020-01-23 - US 001742 - updated coverage
*************************************************************************************************/
@IsTest
private class HOG_VG_AlertExemptionRedirectLtngTest {

    @IsTest
    static void checkExemptionRedirect_withoutParam() {

        Test.startTest();
        HOG_CustomResponse response = HOG_VG_AlertExemptionRedirectLtng.checkExemptionRedirect(null);
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(HOG_VG_AlertExemptionRedirectLtng.ERROR_MSG_QUERY_ERROR, response.errors.get(0));
    }

    @IsTest
    static void checkExemptionRedirect_wrongParam() {

        Test.startTest();
        HOG_CustomResponse response = HOG_VG_AlertExemptionRedirectLtng.checkExemptionRedirect('123456789abcdef');
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(HOG_VG_AlertExemptionRedirectLtng.ERROR_MSG_QUERY_ERROR, response.errors.get(0));
    }

    @IsTest
    static void checkExemptionRedirect_goodParam_wrongUser() {
        HOG_Vent_Gas_Alert__c alert = prepareData(true);
        User runningUser = [SELECT Id, Name FROM User WHERE Alias = 'badBoy'];
        HOG_CustomResponse response;


        Test.startTest();
        System.runAs(runningUser) {
            response = HOG_VG_AlertExemptionRedirectLtng.checkExemptionRedirect(alert.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(
                HOG_VG_AlertExemptionRedirectLtng.ERROR_MSG_WRONG_USER.replace(
                        HOG_VG_AlertExemptionRedirectLtng.PLACEHOLDER,
                        alert.Production_Engineer__r.Name
                ),
                response.errors.get(0)
        );
    }

    @IsTest
    static void checkExemptionRedirect_goodParam_wrongAlert() {
        HOG_Vent_Gas_Alert__c alert = prepareData(false);
        User runningUser = [SELECT Id FROM User WHERE Alias = 'peBoy'];
        HOG_CustomResponse response;


        Test.startTest();
        System.runAs(runningUser) {
            response = HOG_VG_AlertExemptionRedirectLtng.checkExemptionRedirect(alert.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(HOG_VG_AlertExemptionRedirectLtng.ERROR_MSG_WRONG_STATUS, response.errors.get(0));
    }

    //jschn 2020-01-23 - US 001742
    @IsTest
    static void checkExemptionRedirect_goodParam_wrongAlertType() {
        HOG_Vent_Gas_Alert__c alert = prepareInvalidTypeAlert();
        User runningUser = [SELECT Id FROM User WHERE Alias = 'peBoy'];
        HOG_CustomResponse response;

        Test.startTest();
        System.runAs(runningUser) {
            response = HOG_VG_AlertExemptionRedirectLtng.checkExemptionRedirect(alert.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(HOG_VG_AlertExemptionRedirectLtng.ERROR_MSG_WRONG_TYPE, response.errors.get(0));
    }

    @IsTest
    static void checkExemptionRedirect_successProductionEngineer() {
        HOG_Vent_Gas_Alert__c alert = prepareData(true);
        User runningUser = [SELECT Id FROM User WHERE Alias = 'peBoy'];
        HOG_CustomResponse response;

        Test.startTest();
        System.runAs(runningUser) {
            response = HOG_VG_AlertExemptionRedirectLtng.checkExemptionRedirect(alert.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.success);
        System.assertNotEquals(null, response.resultObjects);
        System.assertEquals(null, response.errors);
        System.assertEquals(1, response.resultObjects.size());
    }

    @IsTest
    static void checkExemptionRedirect_successWithCustomPermission() {
        HOG_Vent_Gas_Alert__c alert = prepareData(true);
        User runningUser = [SELECT Id FROM User WHERE Alias = 'goodBoy'];
        HOG_CustomResponse response;


        Test.startTest();
        System.runAs(runningUser) {
            response = HOG_VG_AlertExemptionRedirectLtng.checkExemptionRedirect(alert.Id);
        }
        Test.stopTest();

        System.assert(response != null);
        System.assert(response.success);
        System.assertNotEquals(null, response.resultObjects);
        System.assertEquals(null, response.errors);
        System.assertEquals(1, response.resultObjects.size());
    }

    @TestSetup
    private static void prepareUsers() {
        HOG_TestDataFactory.createUser('Bad','TestUser','badBoy');
        User usr2 = HOG_TestDataFactory.createUser('Good','TestUser','goodBoy');
        usr2 = HOG_TestDataFactory.assignPermissionSet(usr2, 'HOG_Production_Engineer');
        User usr3 = HOG_TestDataFactory.createUser('Production','Engineer','peBoy');
        usr3 = HOG_TestDataFactory.assignPermissionSet(usr3, 'HOG_Production_Engineer');
        CustomPermission permission = [
                SELECT Id
                FROM CustomPermission
                WHERE DeveloperName =: HOG_VG_AlertExemptionRedirectLtng.ALLOWED_CUSTOM_PERMISSION_NAME
        ];
        PermissionSetAssignment assignment = [
                SELECT PermissionSetId
                FROM PermissionSetAssignment
                WHERE AssigneeId =: usr2.Id
                AND PermissionSet.Name = 'HOG_Production_Engineer'
        ];
        SetupEntityAccess access = new SetupEntityAccess(
                SetupEntityId = permission.Id,
                ParentId = assignment.PermissionSetId
        );
        insert access;
    }

    private static HOG_Vent_Gas_Alert__c prepareData(Boolean validAlert) {
        User usr = [SELECT Id, Name FROM User WHERE Alias = 'peBoy'];
        Location__c loc = HOG_VentGas_TestData.createLocation();

        HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(
                HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST,
                'TestDescription',
                loc.Id,
                usr.Id,
                HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
                HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
                HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST
        );
        if(!validAlert) {
            alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_ACKNOWLEDGE;
            update alert;
        }
        alert.Production_Engineer__r = usr;
        return alert;
    }

    //jschn 2020-01-23 - US 001742
    private static HOG_Vent_Gas_Alert__c prepareInvalidTypeAlert() {
        User usr = [SELECT Id, Name FROM User WHERE Alias = 'peBoy'];
        Location__c loc = HOG_VentGas_TestData.createLocation();

        HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(
                HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST,
                'TestDescription',
                loc.Id,
                usr.Id,
                HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM,
                HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED,
                HOG_VentGas_Utilities.ALERT_TYPE_ENG_NOTIF
        );
        alert.Production_Engineer__r = usr;
        return alert;
    }

}