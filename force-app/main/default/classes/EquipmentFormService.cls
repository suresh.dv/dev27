/*
 *  Author:         Maros Grajcar
 *  Company:        Husky Energy
 *  Description:    Methods for EquipmentTransfer & EquipmentMissing controllers
 *  Test class:     EquipmentMissingFormTest & EquipmentTransferFormTest
 *  History:        Created on 10/17/2018
 *  				jschn 07/08/2019 - changed last parameter in requestRejectionTemplateEmail and changed logic for
 *  								getLeadEmailsList. Also refactored logic for extracting Planner Group and used
 *  								same logic to extract Functional Location. 	Email Service Enhancement R
 */

public class EquipmentFormService {

	public static final String COMBINATION_DELIMITER = '@';

	/**
	 * sends email with template
	 *
	 * @param recordId = request Id
	 * @param recipientEmailId = user who will receive an email
	 * @param leadEmails = list of planner group contacts, if empty will be skipped
	 *
	 * @return List<Messaging.SingleEmailMessage>
	 */
	public static List<Messaging.SingleEmailMessage> requestRejectionTemplateEmail(Id recordId, Id recipientEmailId, Set<String> plannerGroupFuncLocationCombinations) {
		EmailTemplate et = [SELECT Name FROM EmailTemplate WHERE Name = 'HOG - Equipment Request Rejected'];
		List<String> leadEmails = getLeadEmailsList(plannerGroupFuncLocationCombinations, EquipmentUtilities.ROLE_TRADEMAN_LEAD);

		Messaging.SingleEmailMessage rejectionEmail = Messaging.renderStoredEmailTemplate(et.Id, recipientEmailId, recordId);
		rejectionEmail.setTargetObjectId(recipientEmailId);
		rejectionEmail.setSaveAsActivity(false);
		if (!leadEmails.isEmpty()) {
			rejectionEmail.ccAddresses = leadEmails;
		}

		Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
				rejectionEmail
		};
		Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
		if (results[0].success) {
			System.debug('The email was sent successfully.');
		} else {
			System.debug('The email failed to send: ' + results[0].errors[0].message);
		}

		return messages;
	}

	public static void formProcessedTemplateEmail(Id recordId, Set<String> plannerGroupFuncLocationCombinations) {
		EmailTemplate et = [SELECT Name FROM EmailTemplate WHERE Name = 'HOG - Equipment Request Processed'];
		List<String> emails = new List<String>();

		getEmailsForProcess(plannerGroupFuncLocationCombinations, EquipmentUtilities.ROLE_PLANNER);
		getEmailsForProcess(plannerGroupFuncLocationCombinations, EquipmentUtilities.ROLE_TRADEMAN_LEAD);

		if (Equipment_EmailTemplateWrapper.contactId != null) {

			Messaging.SingleEmailMessage processedEmail = Messaging.renderStoredEmailTemplate(et.Id, Equipment_EmailTemplateWrapper.contactId, recordId);
			processedEmail.setTargetObjectId(Equipment_EmailTemplateWrapper.contactId);
			processedEmail.setSaveAsActivity(false);

			emails.addAll(Equipment_EmailTemplateWrapper.plannerGroupEmails);
			processedEmail.toAddresses = emails;

			Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{
					processedEmail
			};

			Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

			if (results[0].success) {
				System.debug('The email was sent successfully.');
			} else {
				System.debug('The email failed to send: ' + results[0].errors[0].message);
			}
		}

	}


	/**
	 * goes over list of contacts and get email addresses, if no address on contact checks user's email
	 * if getPlannerGroupNotificationTypes returns empty List
	 *
	 * @return List<String>
	 */
	private static List<String> getLeadEmailsList(Set<String> plannerGroupFuncLocationCombinations, String plannerGroupRole) {
		Set<String> plannerGroupsSet = getPlannerGroupsSetFromCombinationStringSet(plannerGroupFuncLocationCombinations);
		List<HOG_Planner_Group_Notification_Type__c> plannerGroups = getPlannerGroupNotificationTypes(plannerGroupsSet, plannerGroupRole);

		Set<String> emailSet = new Set<String>();

		System.debug('getLeadEmailsList based on: ' + plannerGroupFuncLocationCombinations);

		for (String plannerGroupFuncLocationCombination : plannerGroupFuncLocationCombinations) {
			for (HOG_Planner_Group_Notification_Type__c grp : plannerGroups) {
				System.debug('Has correct combination format: ' + hasCorrectCombinationFormat(plannerGroupFuncLocationCombination));
				if (hasCorrectCombinationFormat(plannerGroupFuncLocationCombination)) {

					List<String> splitCombination = plannerGroupFuncLocationCombination.split(COMBINATION_DELIMITER);
					System.debug('splitCombination index 1: ' + splitCombination.get(1));
					System.debug('FLOC / FLOC_Excluded: ' + grp.FLOC__c + ' / ' + grp.FLOC_Excluded__c);
					System.debug('Contact email: ' + grp.Contact__r.Email);
					if (VTT_Utilities.isCorrectFLOC(splitCombination.get(1), grp.FLOC__c, grp.FLOC_Excluded__c)) {

						String email = getEmail(grp);
						if (String.isNotBlank(email)) {
							emailSet.add(email);
						}

					}

				}

			}
		}
		return new List<String>(emailSet);
	}

	/**
	 * goes over list of contacts and get email addresses, if no address on contact checks user's email
	 * if getPlannerGroupNotificationTypes returns empty List
	 *
	 * @return List<String>
	 */
	private static void getEmailsForProcess(Set<String> plannerGroupFuncLocationCombinations, String plannerGroupRole) {
		Set<String> plannerGroupsSet = getPlannerGroupsSetFromCombinationStringSet(plannerGroupFuncLocationCombinations);
		List<HOG_Planner_Group_Notification_Type__c> plannerGroups = getPlannerGroupNotificationTypes(plannerGroupsSet, plannerGroupRole);

		//Set<String> emailSet = new Set<String>();

		System.debug('getLeadEmailsList based on: ' + plannerGroupFuncLocationCombinations);

		for (String plannerGroupFuncLocationCombination : plannerGroupFuncLocationCombinations) {
			for (HOG_Planner_Group_Notification_Type__c grp : plannerGroups) {
				System.debug('Has correct combination format: ' + hasCorrectCombinationFormat(plannerGroupFuncLocationCombination));
				if (hasCorrectCombinationFormat(plannerGroupFuncLocationCombination)) {

					List<String> splitCombination = plannerGroupFuncLocationCombination.split(COMBINATION_DELIMITER);
					System.debug('splitCombination index 1: ' + splitCombination.get(1));
					System.debug('FLOC / FLOC_Excluded: ' + grp.FLOC__c + ' / ' + grp.FLOC_Excluded__c);
					System.debug('Contact email: ' + grp.Contact__r.Email);
					if (VTT_Utilities.isCorrectFLOC(splitCombination.get(1), grp.FLOC__c, grp.FLOC_Excluded__c)) {

						String email = getEmail(grp);
						if (String.isNotBlank(email)) {
							if (Equipment_EmailTemplateWrapper.contactId == null) {
								Equipment_EmailTemplateWrapper.contactId = getContactId(grp);
							} else {
								Equipment_EmailTemplateWrapper.plannerGroupEmails.add(email);
							}
						}

					}

				}

			}
		}
	}

	private static String getEmail(HOG_Planner_Group_Notification_Type__c grp) {
		String email = '';
		if (grp.Contact__r.Email != null) {
			email = grp.Contact__r.Email;
		} else if (grp.Contact__r.User__r.Email != null) {
			email = grp.Contact__r.User__r.Email;
		}
		return email;
	}

	private static String getContactId(HOG_Planner_Group_Notification_Type__c grp) {
		String contactId = '';
		if (grp.Contact__r.Email != null) {
			contactId = grp.Contact__c;
		} else if (grp.Contact__r.User__r.Email != null) {
			contactId = grp.Contact__r.User__c;
		}
		return contactId;
	}

	private static Set<String> getPlannerGroupsSetFromCombinationStringSet(Set<String> plannerGroupCombinationStrings) {
		Set<String> plannerGroups = new Set<String>();
		for (String plannerGroup : plannerGroupCombinationStrings) {
			if (hasCorrectCombinationFormat(plannerGroup)) {
				plannerGroups.add(plannerGroup.split(COMBINATION_DELIMITER)[0]);
			}
		}
		return plannerGroups;
	}

	private static Boolean hasCorrectCombinationFormat(String combinationString) {
		return combinationString.contains(COMBINATION_DELIMITER)
				&& combinationString.split(COMBINATION_DELIMITER).size() == 2
				&& String.isNotBlank(combinationString.split(COMBINATION_DELIMITER).get(1));
	}

	/**
	 * two list with contact Ids and emails, put those two lists into one
	 * if wont find anything, will return empty List
	 *
	 * @return List<HOG_Planner_Group_Notification_Type__c>
	 */
	private static List<HOG_Planner_Group_Notification_Type__c> getPlannerGroupNotificationTypes(Set<String> plannerGroups, String plannerGroupRole) {
		List<HOG_Planner_Group_Notification_Type__c> groupedPlannerGroupNotifications = new List<HOG_Planner_Group_Notification_Type__c>();
		groupedPlannerGroupNotifications.addAll(leadEmailListToDestination(plannerGroups, plannerGroupRole));

		return groupedPlannerGroupNotifications;
	}

	/**
	 * get Contact and email from Planner Group and only of Trademan Lead, from location
	 * if wont find anything, will return empty List
	 *
	 * @param toDestinationPlannerGroup = string
	 *
	 * @return List<HOG_Planner_Group_Notification_Type__c>
	*/
	private static List<HOG_Planner_Group_Notification_Type__c> leadEmailListToDestination(Set<String> plannerGroup, String plannerGroupRole) {
		List<HOG_Planner_Group_Notification_Type__c> emailList = new List<HOG_Planner_Group_Notification_Type__c>();

		if (plannerGroup != null) {
			List<HOG_Planner_Group__c> plannerGroupList = [
					SELECT Name, (
							SELECT Contact__r.Email, Contact__r.User__r.Email, FLOC__c, FLOC_Excluded__c
							FROM HOG_Planner_Group_Notification_Types__r
							WHERE Role_Type__c = :plannerGroupRole
					)
					FROM HOG_Planner_Group__c
					WHERE Name IN :plannerGroup
			];

			if (!plannerGroupList.isEmpty()) {
				for (HOG_Planner_Group__c plGroup : plannerGroupList) {
					emailList.addAll(plGroup.HOG_Planner_Group_Notification_Types__r);
				}

			}
		}

		return emailList;
	}

	/**
	 * goes over locations, if will find correct one, will get planner group from location
	 * this one is for To_Destination__c
	 *
	 * @param form = pass whole object Equipment_Transfer_Form__c
	 *
	 * @return String = usually a string of 3 number eg. 500, 520, an so on
	 */
	public String toDestinationPlannerGroup(Equipment_Transfer_Form__c form) {
		return extractValueFromLookups(form, flocFields, targetPrefix, plannerGroupFieldAPI);
	}

	/**
	 * goes over locations, if will find correct one, will get planner group from location
	 * this one is for From_Source__c
	 *
	 * @param form = pass whole object Equipment_Transfer_Form__c
	 *
	 * @return String = usually a string of 3 number eg. 500, 520, an so on
	 */
	public String fromSourcePlannerGroup(Equipment_Transfer_Form__c form) {
		return extractValueFromLookups(form, flocFields, sourcePrefix, plannerGroupFieldAPI);
	}

	/**
	 * goes over locations, if will find correct one, will get planner group from location
	 *
	 * @param form = pass whole object Equipment_Missing_Form__c
	 *
	 * @return String = usually a string of 3 number eg. 500, 520, an so on
	 */
	public String addEquipmentPlannerGroup(Equipment_Missing_Form__c form) {
		return extractValueFromLookups(form, flocFields, withoutPrefix, plannerGroupFieldAPI);
	}

	public String getFunctionalLocationForFromSource(Equipment_Transfer_Form__c form) {
		return extractValueFromLookups(form, flocFields, sourcePrefix, functionalLocationFieldAPI);
	}

	public String getFunctionalLocationForToDestination(Equipment_Transfer_Form__c form) {
		return extractValueFromLookups(form, flocFields, targetPrefix, functionalLocationFieldAPI);
	}

	public String getFunctionalLocationForDestination(Equipment_Missing_Form__c form) {
		return extractValueFromLookups(form, flocFields, withoutPrefix, functionalLocationFieldAPI);
	}

	private static final String SUFFIX = '*';
	private static final String PREFIX = '#';
	private static final String targetPrefix = 'To_';
	private static final String sourcePrefix = 'From_';
	private static final String withoutPrefix = '';
	private static final String fieldSuffix = 'c';
	private static final String lookupSuffix = 'r';
	private static final String functionalLocationFieldAPI = 'Functional_Location__c';
	private static final String plannerGroupFieldAPI = 'Planner_Group__c';

	/**
	* Combinations:
	 * Location__c, Location__r,
	 * To_Location__c, To_Location__r,
	 * From_Location__c, From_Location__r
	 * Facility__c, Facility__r,
	 * To_Facility__c, To_Facility__r,
	 * From_Facility__c, From_Facility__r
	 * Yard__c, Yard__r,
	 * To_Yard__c, To_Yard__r,
	 * From_Yard__c, From_Yard__r
	 * Well_Event__c, Well_Event__r,
	 * To_Well_Event__c, To_Well_Event__r,
	 * From_Well_Event__c, From_Well_Event__r
	 * Functional_Equipment_Level__c, Functional_Equipment_Level__r,
	 * To_Functional_Equipment_Level__c, To_Functional_Equipment_Level__r,
	 * From_Functional_Equipment_Level__c, From_Functional_Equipment_Level__r
	 * System__c, System__r,
	 * To_System__c, To_System__r,
	 * From_System__c, From_System__r
	 * Sub_System__c, Sub_System__r,
	 * To_Sub_System__c, To_Sub_System__r,
	 * From_Sub_System__c, From_Sub_System__r
	 */
	private static final Set<String> flocFields = new Set<String>{
			PREFIX + 'Location__' + SUFFIX,
			PREFIX + 'Facility__' + SUFFIX,
			PREFIX + 'Yard__' + SUFFIX,
			PREFIX + 'Well_Event__' + SUFFIX,
			PREFIX + 'Functional_Equipment_Level__' + SUFFIX,
			PREFIX + 'System__' + SUFFIX,
			PREFIX + 'Sub_System__' + SUFFIX
	};

	private String extractValueFromLookups(SObject toExtract, Set<String> targetFields, String fieldPrefix, String fieldAPI) {
		String value = ' ';

		for (String toFLOC : targetFields) {
			if (toExtract.get(toFLOC.replace(PREFIX, fieldPrefix).replace(SUFFIX, fieldSuffix)) != null) {
				value = String.valueOf(
						toExtract.getSObject(toFLOC.replace(PREFIX, fieldPrefix).replace(SUFFIX, lookupSuffix)).get(fieldAPI)
				);
				break;
			}
		}
		return value;
	}

}