global class VTT_UpdateActivityStatusBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
	public static final String JOBTYPE_RETRY_UPDATE_INTEGRATION_ERRORS = 'UPDATE_INTEGRATION_ERRORS';
	public static final String INTEGRATION_ERROR_QUERY = 'Select Id, Name, Status__c, Status_Icon__c, Display_Name__c, ' +
						        						 ' Error_Log__c, Work_Order_Activity__c, RecordTypeId ' +
						                                 ' From VTT_Integration_Error__c ' +
						                                 ' Where Status__c = \'' + VTT_IntegrationUtilities.STATUS_ERROR + '\'' +
						                                 ' And RecordType.DeveloperName = \'' + 
						                                 VTT_IntegrationUtilities.RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR + '\'';

	public String jobType {get; private set;}
	public String query {get; private set;}
	
	global VTT_UpdateActivityStatusBatch() {
		this(JOBTYPE_RETRY_UPDATE_INTEGRATION_ERRORS);
	}

	global VTT_UpdateActivityStatusBatch(String jobType) {
		this.jobType = jobType;

		query = '';
		if(jobType == JOBTYPE_RETRY_UPDATE_INTEGRATION_ERRORS) {
			query = INTEGRATION_ERROR_QUERY;
		}
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('start: ' + query);
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
   		Set<Id> activityIdSet = new Set<Id>();
		//Gather IDs of activities in scope for Integration Errors
		Id updateAcStatusRecordTypeId = Schema.SObjectType.VTT_Integration_Error__c.getRecordTypeInfosByName()
                .get('UpdateActivityStatusError').getRecordTypeId();
		for(VTT_Integration_Error__c integrationError : (List<VTT_Integration_Error__c>) scope) {
				activityIdSet.add(integrationError.Work_Order_Activity__c);
		}
		
		//Query Activities and callout
		List<Work_Order_Activity__c> activitiesToUpdate = [Select Id, Name, Operation_Number__c, Sub_Operation_Number__c,
								                                  Maintenance_Work_Order__r.Work_Order_Number__c, Description__c, 
								                                  Work_Details__c, Status__c, User_Status__c,
								                                  (Select Id, Name, Status__c, Status_Icon__c, Error_Log__c
								                            	   From VTT_Integration_Error__r
								                            	   Where RecordType.DeveloperName =: VTT_IntegrationUtilities.RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR
								                                   Order By SystemModStamp Desc)
								                           From Work_Order_Activity__c where Id in :activityIdSet];
		System.debug('activitiesToUpdate: ' + activitiesToUpdate);
		VTT_IntegrationUtilities.handleActivityUpdateResponse(VTT_IntegrationUtilities.SAP_UpdateWorkOrderActivityStatuses(
            activitiesToUpdate), activitiesToUpdate);
	}
	
	global void finish(Database.BatchableContext BC) {
		//Nothing here yet
	}
	
}