@isTest
private class OpportunityProductEditTest {
    
    // create test date  
    @testSetup static void setup() {        
        
        //create account
        Account acc= new Account(
            Name = 'testAcc',
            BillingStreet = 'testStreet',
            BillingCity = 'tectcity',
            BillingState = 'testState',
            BillingPostalCode = '123',
            BillingCountry = 'testcountry',
            Description = 'testdesc'
        );       	
        insert acc;    
        
        //create opportunity
        Opportunity opp= new Opportunity(
            AccountId = acc.id,
            Amount = 1234.00,
            Description = 'testdesc',
            Name = 'testOpp',
            StageName = 'Prospecting',
            CloseDate = System.Today()
        );        
       	insert opp;
        
        // Insert a test product.
        Product2 prod = new Product2(
            Name = 'Laptop X200', 
            Family = 'Hardware'
        );
        insert prod;
        
        // Get standard price book ID.
        // This is available irrespective of the state of SeeAllData.
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, 
            Product2Id = prod.Id,
            UnitPrice = 10000, 
            IsActive = true
        );
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(
            Name='Custom Pricebook', 
            isActive=true
        );
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, 
            Product2Id = prod.Id,
            UnitPrice = 12000, 
            IsActive = true
        );
        insert customPrice;
                               
        // create opportunityLineItem
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = opp.Id,
            PricebookEntryId = customPrice.Id,
            Quantity = 1,
            UnitPrice = customPrice.UnitPrice, 
            ServiceDate = System.today()
		);
        
      	insert oli;  		
    }	
    
        
    // test which group a user is from - CardlockAndFleet group
    @isTest static void testCardlockFleetUser(){        
        
        OpportunityLineItem[] opp = [SELECT Id, Name FROM OpportunityLineItem];        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(opp[0]);
        
        UserRole newUsrRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'CPM Commercial Sales Manager');
        insert newUsrRole;
        
        User usr = TestDataUser.createAdmin(newUsrRole.Id);   
        
        System.runAs(usr) {                        
            OpportunityProductViewAndEdit ctrl = new OpportunityProductViewAndEdit(stdCtrl);         
            System.assert(ctrl.isCardlockFleetRole);            			
        } 		
    }
       
    // test which group a user is from - WholeSales group
    @isTest static void testWholeSalesUser(){
        
        OpportunityLineItem opp = [SELECT Id, Name FROM OpportunityLineItem].get(0);
        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(opp);
        
        UserRole newUsrRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'National Wholesale Manager Refinery Products');
        insert newUsrRole;
        
        User usr = TestDataUser.createAdmin(newUsrRole.Id);   
        
        System.runAs(usr) {                        
            OpportunityProductViewAndEdit ctrl = new OpportunityProductViewAndEdit(stdCtrl);
            System.assert(ctrl.isWholesaleRole);              			
        } 		
    }
    
    // test which group a user is from - Default group
    @isTest static void testDefaultUser(){
        
        OpportunityLineItem opp = [SELECT Id, Name FROM OpportunityLineItem].get(0);        
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(opp);
        User usr = TestDataUser.createAdmin('default');   
        
        System.runAs(usr) {                        
            OpportunityProductViewAndEdit ctrl = new OpportunityProductViewAndEdit(stdCtrl);
            System.assert(ctrl.isDefaultUserRole);              			
        } 		
    }
    
    //test if user has a role        
    @isTest static void testUserRole() {
        String res = OpportunityUtilities.getUserRoleName();
        System.assert(res != null);        
    }
    
    // test user roles    
    @isTest static void testCpmommercialSalesManager() {
        Boolean res = OpportunityUtilities.isCardlockFleetRole('CPM Commercial Sales Manager');
        System.assertEquals(true, res);
    }
    
    @isTest static void testCommercialFuelAccountRepresentative() {
        Boolean res = OpportunityUtilities.isCardlockFleetRole('Commercial Fuel Account Representative');
        System.assertEquals(true, res);
    }
    
    @isTest static void testCpmCardlockAccountManger() {
        Boolean res = OpportunityUtilities.isCardlockFleetRole('CPM Cardlock Account Manger');
        System.assertEquals(true, res);
    }
    
    @isTest static void testCardlockSalesRepresentative() {
        Boolean res = OpportunityUtilities.isCardlockFleetRole('Cardlock Sales Representative');
        System.assertEquals(true, res);
    }
    
    @isTest static void testFleetSalesRepresentative() {
        Boolean res = OpportunityUtilities.isCardlockFleetRole('Fleet Sales Representative');
        System.assertEquals(true, res);
    }
    
    @isTest static void testNationalWholesaleManagerRefineryProducts() {
        Boolean res = OpportunityUtilities.isWholesaleRole('National Wholesale Manager Refinery Products');
        System.assertEquals(true, res);
    }    
}