/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for FM_LoadConfirmationEditCtrl.cls
History:        jschn XX.XX.2018 - Created.
                jschn 05.21.2018 - Added testExternal + refactored test setup
                jschn 05.31.2018 - added testDuplicateConfirmation to test new functionality for US W-001114
                jschn 03/21/2019 - Added new test method testScaleTicketNumberValidationWithSecondAttempt
                                 - US W-001418
**************************************************************************************************/
@IsTest
private class FM_LoadConfirmationEditCtrlTest {

    private static final String DEBUG_PREFIX = 'FM_LoadConfirmationEditCtrlTest: ';

    @IsTest static void testEdit() {
        ApexPages.currentPage().getParameters().put('id', [SELECT Id FROM FM_Load_Confirmation__c][0].Id);
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
        FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

        Test.startTest();
        String oldProduct = controller.confirmation.Product__c;
        controller.confirmation.Product__c = (oldProduct == 'O') ? 'W' : 'O';
        String newProduct = controller.confirmation.Product__c;
        System.assertNotEquals(oldProduct, controller.confirmation.Product__c);
        controller.save();
        System.assert(!ApexPages.hasMessages());
        System.assertEquals(newProduct, [SELECT Product__c FROM FM_Load_Confirmation__c][0].Product__c);
        Test.stopTest();
    }

    @IsTest static void testCancel() {
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
        FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

        System.assertEquals('/' + FM_Load_Confirmation__c.SObjectType.getDescribe().getKeyPrefix(), controller.cancel().getUrl());
        System.assert(controller.cancel().getRedirect());
        ApexPages.currentPage().getParameters().put('retURL', '/testReturnUrl');
        System.assertEquals('/testReturnUrl', controller.cancel().getUrl());
        System.assert(controller.cancel().getRedirect());
    }

    @IsTest static void testNewSave() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Test.startTest();
            Integer oldCount = [SELECT COUNT() FROM FM_Load_Confirmation__c];
            String destinationFacId = [SELECT Id FROM Facility__c][0].Id;

            controller.confirmation = new FM_Load_Confirmation__c(Truck_Trip__c = [SELECT Id FROM FM_Truck_Trip__c WHERE Truck_Trip_Status__c != :FM_Utilities.TRUCKTRIP_STATUS_COMPLETED][0].Id,
                    Ticket_Number__c = 'testticket2',
                    Volume__c = 30);
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = destinationFacId;
            controller.confirmation.Product__c = 'O';
            PageReference pr = controller.save();

            System.assert(oldCount < [SELECT COUNT() FROM FM_Load_Confirmation__c]);
            System.assertEquals(FM_Utilities.TRUCKTRIP_STATUS_COMPLETED, [SELECT Truck_Trip_Status__c FROM FM_Truck_Trip__c WHERE Id = :controller.confirmation.Truck_Trip__c][0].Truck_Trip_Status__c);
            System.assertEquals(new ApexPages.StandardController(controller.confirmation.Truck_Trip__r).view().getUrl(), pr.getUrl());

            ApexPages.currentPage().getParameters().put(FM_LoadConfirmation_Utilities.SAVERETURN_URL_PARAM_KEY, '/testSave');
            pr = controller.save();

            System.assertEquals(new PageReference('/testSave').getUrl(), pr.getUrl());
            Test.stopTest();
        }
    }

    @IsTest static void testNewSaveLtng() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Test.startTest();
            Integer oldCount = [SELECT COUNT() FROM FM_Load_Confirmation__c];
            String destinationFacId = [SELECT Id FROM Facility__c][0].Id;

            controller.confirmation = new FM_Load_Confirmation__c(Truck_Trip__c = [SELECT Id FROM FM_Truck_Trip__c WHERE Truck_Trip_Status__c != :FM_Utilities.TRUCKTRIP_STATUS_COMPLETED][0].Id,
                    Ticket_Number__c = 'testticket2',
                    Volume__c = 30);
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = destinationFacId;
            controller.confirmation.Product__c = 'O';
            controller.saveLtng();

            System.assert(oldCount < [SELECT COUNT() FROM FM_Load_Confirmation__c]);
            System.assertEquals(FM_Utilities.TRUCKTRIP_STATUS_COMPLETED, [SELECT Truck_Trip_Status__c FROM FM_Truck_Trip__c WHERE Id = :controller.confirmation.Truck_Trip__c][0].Truck_Trip_Status__c);
            System.assertEquals(true, controller.successSave);

            Test.stopTest();
        }
    }

    @IsTest static void ltngUser() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Test.startTest();
            System.assertEquals(false, controller.isLightningUser);

            Test.stopTest();
        }
    }

    @IsTest static void testNewSaveAndNew() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {
            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Test.startTest();
            Integer oldCount = [SELECT COUNT() FROM FM_Load_Confirmation__c];
            String destinationFacId = [SELECT Id FROM Facility__c][0].Id;

            controller.confirmation = new FM_Load_Confirmation__c(Truck_Trip__c = [SELECT Id FROM FM_Truck_Trip__c WHERE Truck_Trip_Status__c != :FM_Utilities.TRUCKTRIP_STATUS_COMPLETED][0].Id,
                    Ticket_Number__c = 'testticket2',
                    Volume__c = 30);
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = destinationFacId;
            controller.confirmation.Product__c = 'O';
            PageReference pr = controller.saveAndNew();

            System.debug(DEBUG_PREFIX + 'Msg: ' + ApexPages.getMessages());
            System.assert(oldCount < [SELECT COUNT() FROM FM_Load_Confirmation__c]);
            System.assertEquals(FM_Utilities.TRUCKTRIP_STATUS_COMPLETED, [SELECT Truck_Trip_Status__c FROM FM_Truck_Trip__c WHERE Id = :controller.confirmation.Truck_Trip__c][0].Truck_Trip_Status__c);
            System.assertEquals(Page.FM_LoadConfirmationEdit.getUrl(), pr.getUrl());
            Test.stopTest();
        }
    }

    @IsTest static void testNegativeSave() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {

            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Test.startTest();
            controller.save();
            System.assert(ApexPages.hasMessages());
            controller.saveAndNew();
            System.assert(ApexPages.hasMessages());
            controller.handleTruckTripChange();
            System.assertEquals(null, controller.confirmation.Truck_Trip__r);
            Test.stopTest();
        }
    }

    @IsTest static void testScaleTicketNumberValidation() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {

            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Id facilityId = [SELECT Id FROM Facility__c LIMIT 1][0].Id;
            Id facilityId2 = [SELECT Id FROM Facility__c WHERE Id != :facilityId LIMIT 1][0].Id;
            Id locationId = [SELECT Id FROM Location__c LIMIT 1][0].Id;
            List<FM_Truck_Trip__c> tts = [SELECT Id FROM FM_Truck_Trip__c WHERE Truck_Trip_Status__c != 'Completed'];
            System.assert(facilityId != null);
            System.assert(facilityId2 != null);
            System.assert(locationId != null);
            System.assert(tts != null && tts.size() > 2);

            Test.startTest();
            //test set location
            controller.confirmation.Truck_Trip__c = tts.get(0).Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = facilityId;
            controller.confirmation.Destination_Location__c = locationId;
            controller.handleDestinationLocationChange();
            System.assertEquals(null, controller.confirmation.Destination_Facility__c);
            controller.confirmation.Ticket_Number__c = 'test2';
            controller.confirmation.Volume__c = 30;
            System.debug(DEBUG_PREFIX + ' bfr save msg: ' + ApexPages.getMessages());
            controller.saveAndNew();
            System.debug(DEBUG_PREFIX + ' aftr save msg: ' + ApexPages.getMessages());
            System.assert(!ApexPages.hasMessages());

            controller = new FM_LoadConfirmationEditCtrl(stdCtrl);
            //test set facility
            controller.confirmation.Truck_Trip__c = tts.get(1).Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = facilityId;
            controller.confirmation.Destination_Location__c = locationId;
            controller.handleDestinationFacilityChange();
            System.assertEquals(null, controller.confirmation.Destination_Location__c);
            controller.confirmation.Ticket_Number__c = 'test3';
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Scale_Ticket_Number__c = 'test1';
            controller.saveAndNew();
            System.debug(DEBUG_PREFIX + ' Messages: ' + ApexPages.getMessages());
            System.assert(!ApexPages.hasMessages());

            controller = new FM_LoadConfirmationEditCtrl(stdCtrl);
            //test set facilty (duplicate ticket for selected facility)
            controller.confirmation.Truck_Trip__c = tts.get(2).Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = facilityId;
            controller.confirmation.Destination_Location__c = locationId;
            controller.handleDestinationFacilityChange();
            System.assertEquals(null, controller.confirmation.Destination_Location__c);
            controller.confirmation.Ticket_Number__c = 'test4';
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Scale_Ticket_Number__c = 'test1';
            controller.saveAndNew();
            System.assert(ApexPages.hasMessages());
            System.assertEquals(1, ApexPages.getMessages().size());

            //test set facilty (duplicate ticket but other facility (no validation error))
            controller.confirmation.Destination_Facility__c = facilityId2;
            controller.handleDestinationFacilityChange();
            System.assertEquals(null, controller.confirmation.Destination_Location__c);
            controller.confirmation.Ticket_Number__c = 'test4';
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Scale_Ticket_Number__c = 'test1';
            controller.saveAndNew();


            Test.stopTest();
        }
    }

    @IsTest static void testScaleTicketNumberValidationWithSecondAttempt() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {

            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Id facilityId = [SELECT Id FROM Facility__c LIMIT 1][0].Id;
            Id facilityId2 = [SELECT Id FROM Facility__c WHERE Id != :facilityId LIMIT 1][0].Id;
            Id locationId = [SELECT Id FROM Location__c LIMIT 1][0].Id;
            List<FM_Truck_Trip__c> tts = [SELECT Id FROM FM_Truck_Trip__c WHERE Truck_Trip_Status__c != 'Completed'];
            System.assert(facilityId != null);
            System.assert(facilityId2 != null);
            System.assert(locationId != null);
            System.assert(tts != null && tts.size() > 2);

            Test.startTest();
            controller.confirmation.Truck_Trip__c = tts.get(0).Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = facilityId;
            controller.handleDestinationFacilityChange();
            System.assertEquals(null, controller.confirmation.Destination_Location__c);
            controller.confirmation.Ticket_Number__c = 'test3';
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Scale_Ticket_Number__c = 'test1';
            controller.saveAndNew();
            System.debug(DEBUG_PREFIX + ' Messages: ' + ApexPages.getMessages());
            System.assert(!ApexPages.hasMessages());
            Integer confirmationInitCount = [SELECT COUNT() FROM FM_Load_Confirmation__c];

            controller = new FM_LoadConfirmationEditCtrl(stdCtrl);
            controller.confirmation.Truck_Trip__c = tts.get(1).Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = facilityId;
            controller.handleDestinationFacilityChange();
            System.assertEquals(null, controller.confirmation.Destination_Location__c);
            controller.confirmation.Ticket_Number__c = 'test4';
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Scale_Ticket_Number__c = 'test1';
            controller.save();
            System.debug(DEBUG_PREFIX + ' Messages: ' + ApexPages.getMessages());
            System.assert(ApexPages.hasMessages());
            System.debug(ApexPages.getMessages());
            Integer confirmationFirstAttemptCount = [SELECT COUNT() FROM FM_Load_Confirmation__c];

            controller.save(); //second attempt
            System.debug(ApexPages.getMessages());
            Integer confirmationSecondAttemptCount = [SELECT COUNT() FROM FM_Load_Confirmation__c];
            Test.stopTest();

            System.assertEquals(confirmationInitCount, confirmationFirstAttemptCount);
            System.assertNotEquals(confirmationFirstAttemptCount, confirmationSecondAttemptCount);
            System.assertEquals(confirmationFirstAttemptCount + 1, confirmationSecondAttemptCount);
        }
    }

    @IsTest static void testDuplicateConfirmationValidation() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {

            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            List<FM_Truck_Trip__c> tts = [SELECT Id FROM FM_Truck_Trip__c WHERE Truck_Trip_Status__c != 'Completed'];

            Id facilityId = [SELECT Id FROM Facility__c LIMIT 1][0].Id;

            Test.startTest();
            //test set location
            controller.confirmation.Truck_Trip__c = tts.get(0).Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = facilityId;
            controller.confirmation.Ticket_Number__c = 'test2';
            controller.confirmation.Volume__c = 30;
            controller.saveAndNew();
            System.assert(!ApexPages.hasMessages());

            controller = new FM_LoadConfirmationEditCtrl(stdCtrl);
            //test duplicate confirmation on same TT
            controller.confirmation.Truck_Trip__c = tts.get(0).Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = facilityId;
            controller.confirmation.Ticket_Number__c = 'test3';
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Scale_Ticket_Number__c = 'test3';
            System.debug(DEBUG_PREFIX + ' bfr save msg: ' + ApexPages.getMessages());
            controller.saveAndNew();
            System.debug(DEBUG_PREFIX + ' aftr save msg: ' + ApexPages.getMessages());
            System.assert(ApexPages.hasMessages());
            System.assertEquals(1, ApexPages.getMessages().size());

            Test.stopTest();
        }
    }

    @IsTest
    static void testValidatoinsAndHandlers() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher');

        System.runAs(usr) {

            ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);

            Test.startTest();
            controller.confirmation = new FM_Load_Confirmation__c(Truck_Trip__c = [SELECT Id FROM FM_Truck_Trip__c WHERE Truck_Trip_Status__c != :FM_Utilities.TRUCKTRIP_STATUS_COMPLETED][0].Id,
                    Ticket_Number__c = 'testticket1',
                    Volume__c = 30);
            controller.handleTruckTripChange();
            controller.confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c = 'something to say?';
            controller.confirmation.Truck_Trip__r.Unit_Change_Reason__c = 'Stuck';
            controller.handleUnitChange();
            controller.confirmation.Truck_Trip__r.Unit__c = [SELECT Id FROM Carrier_Unit__c WHERE Id != :controller.confirmation.Truck_Trip__r.Unit__c][0].Id;
            System.assert(String.isBlank(controller.confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c));

            controller.confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c = 'something to say?';
            controller.confirmation.Truck_Trip__r.Unit_Change_Reason__c = 'Stuck';
            controller.handleUnitChange();
            System.assert(String.isBlank(controller.confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c));

            controller.confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c = 'something to say?';
            controller.confirmation.Truck_Trip__r.Unit_Change_Reason__c = 'Other';
            controller.handleUnitChange();
            System.assert(String.isNotBlank(controller.confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c));

            controller.confirmation.Truck_Trip__r.Unit_Change_Other_Reason__c = '';
            System.assertEquals(0, ApexPages.getMessages().size());
            controller.save();
            System.assertEquals(1, ApexPages.getMessages().size());

            Test.stopTest();
        }
    }

    @IsTest static void testExternal() {
        User usr = HOG_TestDataFactory.createUser('Tony', 'Stark', 'IronMan');
        usr = HOG_TestDataFactory.assignPermissionSet(usr, 'HOG_FM_Dispatcher_Vendor');
        //missing testing validations - will adjust next time.

        FM_Truck_Trip__c ttWithoutUnit = [SELECT Id FROM FM_Truck_Trip__c WHERE Unit__c = NULL].get(0);
        FM_Truck_Trip__c ttWithUnit = [SELECT Id FROM FM_Truck_Trip__c WHERE Unit__c != NULL].get(0);
        Integer stuckTrucks = [SELECT COUNT() FROM FM_Stuck_Truck__c];
        String destinationFacId = [SELECT Id FROM Facility__c][0].Id;

        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(new FM_Load_Confirmation__c());
        System.runAs(usr) {

            FM_LoadConfirmationEditCtrl controller = new FM_LoadConfirmationEditCtrl(stdCtrl);
            System.assert(controller.isExternalDispatcher);
            System.assert(controller.stuckTruck != null);

            //testing with unit
            controller = new FM_LoadConfirmationEditCtrl(stdCtrl);
            controller.confirmation.Truck_Trip__c = ttWithUnit.Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = destinationFacId;
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Ticket_Number__c = 'test2';
            controller.isStuckTruck = true;
            controller.handleStuckTruckCheckboxChange();
            controller.confirmation.Truck_Trip__r.Truck_Trip_Status__c = 'Exported';
            controller.saveAndNew();
            System.debug(DEBUG_PREFIX + ' aftr save msg: ' + ApexPages.getMessages());

            //Testing without unit
            controller = new FM_LoadConfirmationEditCtrl(stdCtrl);
            controller.confirmation.Truck_Trip__c = ttWithoutUnit.Id;
            controller.handleTruckTripChange();
            controller.confirmation.Destination_Facility__c = destinationFacId;
            controller.confirmation.Volume__c = 30;
            controller.confirmation.Ticket_Number__c = 'test3';
            controller.isStuckTruck = true;
            controller.handleStuckTruckCheckboxChange();
            controller.saveAndNew();
            System.debug(DEBUG_PREFIX + ' aftr2 save msg: ' + ApexPages.getMessages());
        }

        Integer postTestStuckTrucks = [SELECT COUNT() FROM FM_Stuck_Truck__c];
        System.assertNotEquals(postTestStuckTrucks, stuckTrucks);

    }

    @TestSetup
    static void setData() {
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);

        Field__c field = HOG_TestDataFactory.createAMU(true);

        Carrier__c carrier = FM_TestDataFactory.createCarrier(acc.Id, true);
        Carrier_Unit__c unit = FM_TestDataFactory.createCarrierUnit(carrier.Id, true);
        FM_TestDataFactory.createCarrierUnit('BMW 330DX E91', carrier.Id, true);

        Route__c route = HOG_TestDataFactory.createRoute(true);

        Location__c location = HOG_TestDataFactory.createLocation(route.Id, field.Id, true);
        Location__c location2 = HOG_TestDataFactory.createLocation('Location2', route.Id, field.Id, true);

        HOG_TestDataFactory.createEquipmentTank(location, true);

        Facility__c facility = HOG_TestDataFactory.createFacility(route.Id, field.Id, true);
        HOG_TestDataFactory.createFacility('Facility2', route.Id, field.Id, true);

        HOG_TestDataFactory.createEquipmentTank(location2, true);


        List<FM_Load_Request__c> loadRequestList = new List<FM_Load_Request__c>();
        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'O', null,
                'Night',
                location.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'W', null,
                'Night',
                location.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'O', null,
                'Day',
                location.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'W', null,
                'Day',
                location.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        //Create load request for nonRunSheet test
        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'O', null,
                'Night',
                location2.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'W', null,
                'Day',
                location2.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnFacility(carrier.Id,
                location.Id, null,
                'O', null,
                'Night',
                facility.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        loadRequestList.add(FM_TestDataFactory.createLoadRequestOnFacility(carrier.Id,
                location.Id, null,
                'W', null,
                'Day',
                facility.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                false));

        insert loadRequestList;

        List<FM_Truck_Trip__c> truckTrips = FM_TestDataFactory.createTruckTrips(loadRequestList,
                'Today',
                FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED,
                unit.Id,
                true);

        FM_Load_Request__c loadRequest = FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null,
                facility.Id,
                'W', null,
                'Day',
                location2.Id,
                FM_LoadRequest_Utilities.LOADREQUEST_STATUS_DISPATCHED,
                true);
        FM_Truck_Trip__c tt = FM_TestDataFactory.createTruckTrip(loadRequest.Id, false);
        tt.Truck_Trip_Status__c = 'Exported';
        tt.Carrier__c = carrier.Id;
        insert tt;

        FM_TestDataFactory.createLoadConfirmation(truckTrips.get(1).Id,
                facility.Id,
                null,
                'O',
                null,
                'testticket1',
                30,
                false,
                true);
    }

}