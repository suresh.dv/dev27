public with sharing class RaMPortalController {

    @AuraEnabled
    public static List<Case> getRaMOpenCases(){       
        User usr = [SELECT Id, Contact.AccountId FROM User WHERE Id =: UserInfo.getUserId()];
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getRaMOpenCasesByVendor(usr.Contact.AccountId);
    }   

    @AuraEnabled(cacheable=true)
    public static Case getRaMCaseByCaseNumber(String caseNumber){
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getRaMCaseByCaseNumber(caseNumber);

    } 
    
    @AuraEnabled(cacheable=true)
    public static List<RaMPortalManagementService.PortalUserWrapper> getPortalUsers(){
        RaMPortalManagementService portalService = new RaMPortalManagementService();
        return portalService.getPortalUsersByAccount(UserInfo.getUserId());
    }
    
    @AuraEnabled(cacheable=true)
    public static List<RaMPortalManagementService.CurrentAssignment> getAssignmentUsersByCase(String caseRecordId){
        RaMPortalManagementService portalService = new RaMPortalManagementService();
        return portalService.getAssignmentUsersByCase(caseRecordId, UserInfo.getUserId());
    }        

    @AuraEnabled
    public static void updateContact(String contactId, String firstName, String lastName, String emailAddress, String mobilePhone,
                        String businessPhone, String homePhone, String otherPhone, String title, String description,
                        String mailingStreet, String mailingCity, String mailingState, String mailingPostalCode, String mailingCountry){
        
        Contact cont = new Contact();
        cont.Id = contactId;
        cont.FirstName = firstName;
        cont.LastName = lastName;
        cont.Email = emailAddress;
        cont.MobilePhone = mobilePhone;
        cont.Phone = businessPhone;
        cont.HomePhone = homePhone;
        cont.OtherPhone = otherPhone;
        cont.Title = title;
        cont.Description = description;
        cont.MailingStreet = mailingStreet;
        cont.MailingCity = mailingCity;
        cont.MailingState = mailingState;
        cont.MailingPostalCode = mailingPostalCode;
        cont.MailingCountry = mailingCountry;

        RaMPortalManagementService service = new RaMPortalManagementService();
        service.updateContact(cont);
    }

    @AuraEnabled
    public static void createContact(String accountId, String firstName, String lastName, String emailAddress, String mobilePhone,
                        String businessPhone, String homePhone, String otherPhone, String title, String description,
                        String mailingStreet, String mailingCity, String mailingState, String mailingPostalCode, String mailingCountry){
        
        Contact cont = new Contact();
        cont.AccountId = accountId;
        cont.FirstName = firstName;
        cont.LastName = lastName;
        cont.Email = emailAddress;
        cont.MobilePhone = mobilePhone;
        cont.Phone = businessPhone;
        cont.HomePhone = homePhone;
        cont.OtherPhone = otherPhone;
        cont.Title = title;
        cont.Description = description;
        cont.MailingStreet = mailingStreet;
        cont.MailingCity = mailingCity;
        cont.MailingState = mailingState;
        cont.MailingPostalCode = mailingPostalCode;
        cont.MailingCountry = mailingCountry;

        RaMPortalManagementService service = new RaMPortalManagementService();
        service.createContact(cont);
    }    

    @AuraEnabled
    public static void deactivateUser(String userId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        service.deactivatePortalUser(userId);       
    }

    @AuraEnabled
    public static void activateOrCreateUser(String contactId, String isConverted, String userId, Boolean needPasswordReset, String userType){
        RaMPortalManagementService service = new RaMPortalManagementService();
        service.activateOrCreateUser(contactId, isConverted, userId, needPasswordReset, userType);
    }    

    @AuraEnabled
    public static void assignCaseToTechnician(String recordId, List<String> technicianIds, String subject, String ticketNumber){
        RaMPortalManagementService service = new RaMPortalManagementService();
        service.assignCaseToTechnician(recordId, technicianIds, subject, ticketNumber);
    }
    
    @AuraEnabled
    public static List<RaMPortalManagementService.CaseAssignment> getAssignmentsByCase(String caseNumber){
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getAssignmentsByCase(caseNumber);
    }

    @AuraEnabled
    public static List<RaMPortalManagementService.CaseAssignment> getAssignmentsByContact(String contactId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getAssignmentByContact(contactId);        
    }

    @AuraEnabled
    public static List<RaMPortalManagementService.OpenActivity> getOpenActivityListByTechnician(String userId) {
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getOpenActivityList(userId);
    }

    @AuraEnabled
    public static RaMPortalManagementService.OpenActivity getActivityDetails(String activityId, String userId) {
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getActivityDetails(activityId, userId);
    } 
    
    @AuraEnabled
    public static void updateActivityStatus(String recordId, String lat, String lon, String comments, String currentStatus, String nextStatus, String errorCode, String caseRecordId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        service.updateActivity(recordId, lat, lon, comments, currentStatus, nextStatus, errorCode, caseRecordId);
    }

    @AuraEnabled
    public static void updatePrimaryTechnician(String caseId, String newTechnicianId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        service.updatePrimaryTechnician(caseId, newTechnicianId);
    }

    @AuraEnabled
    public static void removeTechnicianAssignment(String assignmentRecordId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        service.unassignTechnician(assignmentRecordId);
    }  
    
    @AuraEnabled
    public static void reassignTechnician(String assignmentRecordId, String newTechnicianId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        service.reassignNewTechnician(assignmentRecordId, newTechnicianId);        
    }

    @AuraEnabled
    public static RaMPortalManagementService.LocationMarker getLocationMarkers(String caseRecordId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getActivityLocations(caseRecordId);     
    }

    @AuraEnabled
    public static List<RaMPortalManagementService.CombineAttachment> getConbineAttachments(String caseNumber){
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getCombinedAttachments(caseNumber);
    }

    @AuraEnabled
    public static List<RaMPortalManagementService.ActivityComments> getActivityComments(String caseRecordId){
        RaMPortalManagementService service = new RaMPortalManagementService();
        return service.getActivityCommentsByCase(caseRecordId);        
    }

}