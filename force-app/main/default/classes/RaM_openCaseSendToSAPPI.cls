/**********************************************************************************
  CreatedBy : Accenture
  Organization:Accenture
  Purpose   :  Batch apex class to send all open cases to SAP PI in JSON format
  Test Class:  RaM_openCaseSendToSAPPI
  Version:1.0.0.1
**********************************************************************************/

global class RaM_openCaseSendToSAPPI implements Database.Batchable<sObject> , Database.AllowsCallouts , Database.Stateful {
    global string query ;
    global string openTicketInformation ;


    global database.querylocator start(Database.BatchableContext BC){
        openTicketInformation = RaM_ConstantUtility.BLANK;
        return Database.getQueryLocator(query);    
    }

    /*global void execute(SchedulableContext SC){

    System.schedule('RaM Open Tickets Interface 1', '0 0 * * * ?', new RaM_scheduledopenCaseSendToSAPPI());
    System.schedule('RaM Open Tickets Interface 2', '0 15 * * * ?', new RaM_scheduledopenCaseSendToSAPPI());
    System.schedule('RaM Open Tickets Interface 3', '0 30 * * * ?', new RaM_scheduledopenCaseSendToSAPPI());
    System.schedule('RaM Open Tickets Interface 4', '0 45 * * * ?', new RaM_scheduledopenCaseSendToSAPPI());

    }*/

    global void execute(Database.BatchableContext BC, List<Case> scope){ 
    
        for(Case caseIns : scope){
            String openTicket = caseIns.Ticket_Number_R_M__c + RaM_ConstantUtility.COMA + caseIns.LocationNumber__r.name + '\n';
            openTicketInformation = openTicketInformation + openTicket;
        }

        HuskyExceptionManager.log('RaM_openCaseSendToSAPPI', openTicketInformation, 'INFO');
        
    }

    global void finish(Database.BatchableContext BC){
        RaM_SalesforceComPm.CASE_element caseInfoIns = new RaM_SalesforceComPm.CASE_element();
        list<RaM_SalesforceComPm.CASE_element> caseInfoList = new list<RaM_SalesforceComPm.CASE_element>();

        if(openTicketInformation != ''){
            caseInfoIns.caseNumber = openTicketInformation;
        } 

        if(caseInfoIns != null)   {
            caseInfoList.add(caseInfoIns);
            
            HOG_Settings__c settingsHOG = HOG_Settings__c.getInstance();
            RaM_SalesforceComPm.HTTPS_Port  connectionIns = new RaM_SalesforceComPm.HTTPS_Port();
            connectionIns.inputHttpHeaders_x = new Map<String, String>();
            String authString = EncodingUtil.base64Encode(Blob.valueOf(settingsHOG.SAP_DPVR_Username__c + ':' + settingsHOG.SAP_DPVR_Password__c));
            connectionIns.inputHttpHeaders_x.put('Authorization', 'Basic ' + authString);
            connectionIns.clientCertName_x = 'HuskyCertificate';
            
            try{
                connectionIns.SI_SFDC_TicketQuery_OB(caseInfoList);  

            } catch(Exception exp) {
                HuskyExceptionManager.log('RaM_openCaseSendToSAPPI', exp);
            }
            
        }
    }
}