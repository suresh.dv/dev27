@isTest
public with sharing class RaMPortalManagementServiceTest {

    @testSetUp 
    static void setup() {
        String accountRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();

        Profile headOfficeProfile = [SELECT Id FROM Profile WHERE Name = 'Category Manager User'];
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'RaM Lightning Vendor Community'];
        UserRole role = [SELECT Id, Name FROM UserRole WHERE Name = 'Retail Fuels & Convenience - Staff'];

        User headOffice = TestUtilities.createTestUser(headOfficeProfile);
        headOffice.Email = 'head_office_unit_test@huskyenergy.huskyram.com';
        headOffice.UserName = 'head_office_unit_test@huskyenergy.huskyram.com';
        headOffice.UserRoleId = role.Id;

        insert headOffice;       

        System.runAs(headOffice) {
            Account vendor1 = new Account(Name = 'RaM Vendor Account 1');
            vendor1.RecordTypeId = accountRecordType;
    
            Account vendor2 = new Account(Name = 'RaM Vendor Account 2');
            vendor2.RecordTypeId = accountRecordType;
    
            insert vendor1;
            insert vendor2;

            Contact vendor1Contact1 = new Contact(FirstName = 'V1', LastName = 'Vendor 1 Dispatcher');
            vendor1Contact1.AccountId = vendor1.id;
            vendor1Contact1.Email = 'vendor1_dispatcher@vendor1.com';
            vendor1Contact1.RecordTypeId = contactRecordType;   
            
            Contact vendor1Contact2 = new Contact(FirstName = 'V1', LastName = 'Vendor 1 Technicain');
            vendor1Contact2.AccountId = vendor1.id;
            vendor1Contact2.Email = 'vendor1_technicain@vendor1.com';
            vendor1Contact2.RecordTypeId = contactRecordType;              
    
            Contact vendor2Contact1 = new Contact(FirstName = 'V2', LastName = 'Vendor 2 Dispatcher');
            vendor2Contact1.AccountId = vendor2.id;
            vendor2Contact1.Email = 'vendor2_dispatcher@vendor2.com';
            vendor2Contact1.RecordTypeId = contactRecordType;  
            
            Contact vendor2Contact2 = new Contact(FirstName = 'V2', LastName = 'Vendor 2 Technicain');
            vendor2Contact2.AccountId = vendor2.id;
            vendor2Contact2.Email = 'vendor2_technicain@vendor2.com';
            vendor2Contact2.RecordTypeId = contactRecordType;             
    
            insert vendor1Contact1;
            insert vendor1Contact2;
            insert vendor2Contact1;
            insert vendor2Contact2;
            
            RaMPortalUtility.createPortalUser(vendor1Contact1, 'RaM Dispatcher');
            RaMPortalUtility.createPortalUser(vendor1Contact2, 'RaM Technician');
            RaMPortalUtility.createPortalUser(vendor2Contact1, 'RaM Dispatcher');
            RaMPortalUtility.createPortalUser(vendor2Contact2, 'RaM Technician');

            RaM_Location__c locationRecord = new RaM_Location__c(Name = '1234', Service_Area__c = 'Island', SAPFuncLOC__c = 'C1-04-002-00042', 
                        Location_Classification__c = 'Urban', R_M_Location__c = true, LocationName__c = 'Unit Test Location', Retailer__c = 'Unit Test Retailer',
                        Physical_Address_1__c = 'Unit Test Address 1', Physical_Address_2__c = 'Unit Test Address 2', City__c = 'Calgary', Province__c = 'AB', 
                        Postal_Code__c = 'T2W8T6', Phone__c = '4031236655', District_Manager__c = 'Test Manager', Maintenance_Tech_User__c = null);

            insert locationRecord; 
            
            RaM_Equipment__c ramEquipment = new RaM_Equipment__c(Name = 'RP0042SSDVR0001-16 Channel DVR--');
            ramEquipment.FunctionalLocation__c = 'C1-04-002-00042';
            ramEquipment.Equipment_Type__c = 'ELECTRICAL';
            ramEquipment.Equipment_Class__c = 'ELECTRICAL PANEL';
            ramEquipment.Status__c = 'Active';
            
            insert ramEquipment;

            RaM_VendorAssignment__c vendorAssignment = new RaM_VendorAssignment__c();
            vendorAssignment.Account__c = vendor1.id;
            vendorAssignment.Equipment_Type_VA__c = 'ELECTRICAL';
            vendorAssignment.Equipment_Class_VA__c = 'ELECTRICAL PANEL';
            vendorAssignment.Priority__c = 1;
            vendorAssignment.ServiceArea__c = 'Island';
            vendorAssignment.Location_Classification_VA__c = 'Urban';
            
            insert vendorAssignment;

            RaM_Setting__c cusSetting = new RaM_Setting__c();
            cusSetting.name = 'Case Next Counter';
            cusSetting.R_M_Ticket_Number_Next_Sequence__c = 123456;
            insert cusSetting;             

            Case kase1 = new Case(RecordTypeId = caseRecordType,
                                    Status = 'New',
                                    Priority = 'P2-Emergency',
                                    Origin = 'Maintenance Tech',
                                    LocationNumber__c = locationRecord.id,
                                    EquipmentName__c = ramEquipment.id,                                 
                                    Equipment_Type__c='ELECTRICAL',
                                    Equipment_Class__c='ELECTRICAL PANEL',
                                    Location_Contact__c = 'Retailer Contact', 
                                    Location_Classification__c = 'Urban',
                                    Service_Area__c = 'Island' , 
                                    subject = 'Unit Test RaM Case', 
                                    description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat purus odio. Donec lacus mi, condimentum a diam eget, posuere mollis purus. Donec ac velit.' ); 

            insert kase1;   

            kase1.Status = 'Assigned';
            kase1.Vendor_Assignment__c = vendorAssignment.id;
            update kase1;

            List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact2.Id];
            List<Case> kases = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

            Test.startTest();
            RaMPortalUtility.createAndAssignCase(kases[0], users);
            Test.stopTest();
        }
    }

    @isTest
    private static void test_getRaMOpenCasesByVendor() {
        Test.startTest();
        List<Account> accounts = [SELECT Id, Name FROM Account WHERE RecordType.Name = 'RaM Vendor' AND Name = 'RaM Vendor Account 1'];

        RaMPortalManagementService service = new RaMPortalManagementService();
        List<Case> vendor1Cases = service.getRaMOpenCasesByVendor(accounts[0].Id);
        System.assertEquals(1, vendor1Cases.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getPortalUsersByAccount() {
        Test.startTest();
        Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
        List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id)];

        RaMPortalManagementService service = new RaMPortalManagementService();
        List<RaMPortalManagementService.PortalUserWrapper> userWrappers = service.getPortalUsersByAccount(users[0].Id);

        System.assertEquals(1, userWrappers.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getAssignmentUsersByCase() {
        Test.startTest();
        Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
        List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND Email = 'vendor1_technicain@vendor1.com')];
        
        Case kase1 = [SELECT Id FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];

        // RaMPortalManagementService service = new RaMPortalManagementService();
        // List<RaMPortalManagementService.CurrentAssignment> assignments = service.getAssignmentUsersByCase(kase1.Id, users[0].Id);

        // System.assertEquals(1, assignments.size());
        Test.stopTest();
    }

    @isTest
    private static void test_activateOrCreateUser() {
        Test.startTest();
        String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();

        Account vendor1 = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1' LIMIT 1];

        Contact vendor1Contact3 = new Contact(FirstName = 'V1', LastName = 'Vendor 1 Technicain 2');
        vendor1Contact3.AccountId = vendor1.id;
        vendor1Contact3.Email = 'vendor1_technicain2@vendor1.com';
        vendor1Contact3.RecordTypeId = contactRecordType;

        insert vendor1Contact3;

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        System.runAs(headOfficeUser) {
            RaMPortalManagementService service = new RaMPortalManagementService();
            service.activateOrCreateUser(vendor1Contact3.Id, 'false', null, null, 'RaM Technician');
        }   

        List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :vendor1.id];
        System.assertEquals(3, contacts.size());
        Test.stopTest();
    }

    @isTest
    private static void test_assignCaseToTechnician() {
        Test.startTest();
        String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();

        Account vendor1 = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1' LIMIT 1];

        Contact vendor1Contact3 = new Contact(FirstName = 'V1', LastName = 'Vendor 1 Technicain 2');
        vendor1Contact3.AccountId = vendor1.id;
        vendor1Contact3.Email = 'vendor1_technicain2@vendor1.com';
        vendor1Contact3.RecordTypeId = contactRecordType;

        insert vendor1Contact3;

        User headOfficeUser = [SELECT Id FROM USER WHERE UserName = 'head_office_unit_test@huskyenergy.huskyram.com'];

        RaMPortalManagementService service = new RaMPortalManagementService();

        System.runAs(headOfficeUser) {
            service.activateOrCreateUser(vendor1Contact3.Id, 'false', null, null, 'RaM Technician');
        }  

        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        User usr = [SELECT Id FROM User WHERE ContactId = :vendor1Contact3.Id];
        List<String> technicians = new List<String>();
        technicians.add(usr.Id);
        
        service.assignCaseToTechnician(kase.Id, technicians, kase.Subject, kase.Ticket_Number_R_M__c);

        List<RaM_Case_Assignment__c> caseAssignments = [SELECT Id, Assign_To__c, Subject__c, Ticket_Number__c 
                                                        FROM RaM_Case_Assignment__c
                                                        WHERE Parent_Case__c = :kase.Id
                                                            AND Assign_To__c = :usr.Id];

        System.assertEquals(1, caseAssignments.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getAssignmentsByCase() {
        Test.startTest();
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];

        RaMPortalManagementService service = new RaMPortalManagementService();
        List<RaMPortalManagementService.CaseAssignment> caseAssignments = service.getAssignmentsByCase(kase.CaseNumber);

        System.assertEquals(1, caseAssignments.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getAssignmentByContact() {
        Test.startTest();
        Contact vendor1Technician = [SELECT Id, Email, FirstName, LastName FROM Contact WHERE LastName = 'Vendor 1 Technicain'];

        RaMPortalManagementService service = new RaMPortalManagementService();
        List<RaMPortalManagementService.CaseAssignment> assignments = service.getAssignmentByContact(vendor1Technician.Id);

        System.assertEquals(1, assignments.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getOpenActivityList() {
        Test.startTest();
        Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
        List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND Email = 'vendor1_technicain@vendor1.com')];
        
        RaMPortalManagementService service = new RaMPortalManagementService();
        List<RaMPortalManagementService.OpenActivity> activities = service.getOpenActivityList(users[0].Id);

        System.assertEquals(1, activities.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getActivityDetails() {
        Test.startTest();
        Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
        List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND Email = 'vendor1_technicain@vendor1.com')];
        
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];
        
        List<RaM_Case_Assignment__c> assignments = [SELECT Id FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];

        RaMPortalManagementService service = new RaMPortalManagementService();
        RaMPortalManagementService.OpenActivity activityInfo = service.getActivityDetails(assignments[0].Id, users[0].Id);

        System.assertEquals(kase.Ticket_Number_R_M__c, activityInfo.tkt.ticketNumber);
        Test.stopTest();
    }

    @isTest
    private static void test_updateActivity() {
        Test.startTest();
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];
        
        List<RaM_Case_Assignment__c> assignments = [SELECT Id FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];

        Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
        List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND Email = 'vendor1_technicain@vendor1.com')];
        
        System.runAs(users[0]) {
            //Using Lat and Long for Calgary tower
            RaMPortalManagementService service = new RaMPortalManagementService();
            service.updateActivity(assignments[0].Id, '51.048615', '-114.070847', 'Work start comments', 'Assigned', 'Work In Progress', '', kase.Id);
        }

        List<RaM_Case_Assignment_Activity__c> activities = [SELECT Id FROM RaM_Case_Assignment_Activity__c WHERE RaM_Case_Assignment__c = :assignments[0].Id];
        System.assertEquals(1, activities.size());

        List<RaM_WorkInformation__c> workInfo = [SELECT Id FROM RaM_WorkInformation__c WHERE Ticket__c = :kase.Id];
        System.assertEquals(1, workInfo.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getActivityLocations() {
        Test.startTest();
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];
        
        List<RaM_Case_Assignment__c> assignments = [SELECT Id FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase.Id];

        Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
        List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND Email = 'vendor1_technicain@vendor1.com')];
        
        RaMPortalManagementService service = new RaMPortalManagementService();
        System.runAs(users[0]) {
            //Using Lat and Long for Calgary tower
            service.updateActivity(assignments[0].Id, '51.048615', '-114.070847', 'Work start comments', 'Assigned', 'Work In Progress', '', kase.Id);
        }
        
        RaMPortalManagementService.LocationMarker marker = service.getActivityLocations(kase.Id);
        List<RaM_Case_Assignment_Activity__c> activities = marker.activities;

        System.assertEquals(1, activities.size());
        Test.stopTest();
    }

    @isTest
    private static void test_getCombinedAttachments() {
        Test.startTest();
        String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();
        Case kase = [SELECT Id, Subject, CaseNumber, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];
        
        Attachment attach = new Attachment();    
        attach.Name='Unit Test Attachment';
        attach.body = Blob.valueOf('Unit Test Attachment Body');
        attach.parentId = kase.id;
        insert attach;

        RaMPortalManagementService service = new RaMPortalManagementService();
        List<RaMPortalManagementService.CombineAttachment> combineAttachments = service.getCombinedAttachments(kase.CaseNumber);

        System.assertEquals(1, combineAttachments.size());
        Test.stopTest();
    }


}