public class PTControllerOldPCSO {
    private limitWrapper[] pcsoFinalList = new limitWrapper[]{};
        public integer amountOfDays = integer.valueOf(returnControllerSettingValue('oldPCSODaysRecalculate'));    	
    
    public decimal returnControllerSettingValue(String name){
        Decimal controllerSettingValue;
        if(PT_PCSO_Settings__c.getValues(name) != null){
            controllerSettingValue = PT_PCSO_Settings__c.getValues(name).PCSO_Value__c;
        }
        else{
            controllerSettingValue = 365;
        }
        return controllerSettingValue;
    }
        
        private final integer listLimit = 999;
    
    public limitWrapper[] getpcsoFinalList()
    {
        pcsoFinalList = new limitWrapper[]{};
        List<Service__c> serviceRefinedList = new List<Service__c>();
        Map<id,pcso__history> pcsoHistoryMap = new map<id,pcso__history>();
        Map<id,pcso__c> pcsosToEvaluateMap = new map<id,pcso__c>();
        List <PCSO__history> pcsoHistory = new List<PCSO__history>();
        objectWrapper[] objectWrappedList = new objectWrapper[]{};
        integer counter = 0;
        integer loopCount = 0;
        
        if(amountOfDays == 0){pcsoHistory = database.query('select id,parentid,oldvalue,newvalue,createddate from PCSO__history where createddate <= LAST_N_DAYS:'+amountOfDays+' and field in(\'Cost_Saving__c\',\'Buydown_Cost__c\')');}    
        else{pcsoHistory = database.query('select id,parentid,oldvalue,newvalue,createddate from PCSO__history where createddate < LAST_N_DAYS:'+amountOfDays+' and field in(\'Cost_Saving__c\',\'Buydown_Cost__c\')');}
        
        for(PCSO__History historyPCSO: pcsoHistory){
        	  	pcsoHistoryMap.put(historyPCSO.parentid,historyPCSO);              
        }
        
        List <Service__c> serviceList = database.query('select id,name,utility_rate_id__r.name,utility_company__r.name,contract_start_date__c,site_id__c,minimum_contract_demand__c,(select id,name,billing_date__c,Average_Billing_Demand_last_12_months__c,	Average_Metered_Demand_last_12_months__c,Average_Total_Volume_last_12_months__c,Average_Wire_Cost_last_12_months__c,Average_KVA_Demand_last_12_months__c from utility_billings__r order by billing_date__c desc limit 1),(SELECT Id, Name,service_id__r.name,pcso_type__c FROM PCSOS__r where pcso_status__c in (\'Created\',\'Reviewing\',\'Deferred\') and system_review__c=\'Valid\') from service__c where service_status__c in (\'Active\',\'Idle\')');
            for(Service__c serviceToCheck: serviceList){
                for(PCSO__c pcso: serviceToCheck.pcsos__r){
                    if(pcsoHistoryMap.containsKey(pcso.id)){
                    	serviceRefinedList.add(serviceToCheck);    
                    }
                }            
        }
        
        for(Service__c service : serviceRefinedList)
        {
            List<Utility_Billing__c> utilityBillingList = service.utility_billings__r;
            if(counter < listLimit)
            {
				system.debug(Service);                
                for(PCSO__c pcso : service.pcsos__r){
                    if(pcsosToEvaluateMap.containskey(pcso.id) != true){
                    	objectWrapper objectWrapped = new objectWrapper(service,pcso,utilityBillingList.get(0));
                    	objectWrappedList.add(objectWrapped);
                    	counter++;
                        pcsosToEvaluateMap.put(pcso.id, pcso);
                    }
                }
            }
            else
            {
                loopCount++;
                for(PCSO__c pcso : service.pcsos__r){
                    if(pcsosToEvaluateMap.containskey(pcso.id) != true){
                    	pcsoFinalList.add(new limitWrapper(objectWrappedList,loopCount));
                    	objectWrappedList = new objectWrapper[]{};
                    	objectWrapper objectWrapped = new objectWrapper(service,pcso,utilityBillingList.get(0));
                    	objectWrappedList.add(objectWrapped);
                    	counter = 0;
                    	pcsosToEvaluateMap.put(pcso.id,pcso);
                    }
                }
            }            
        }
        
        if(counter > 0)
        {
            loopCount++;
            pcsoFinalList.add(new limitWrapper(objectWrappedList,loopCount));
        }
        
        return pcsoFinalList;
    }
    
    public class limitWrapper
    {
        public objectWrapper[] objectsWrapper {get;set;}
        public integer blockNumber {get;set;}
        public limitWrapper(objectWrapper[] objectWrapper, integer i)
        {
            objectsWrapper = objectWrapper;
            blockNumber = i;
        }
        
    }
    
    public class objectWrapper
    {
        public service__c service {get;set;}
        public pcso__c pcso {get;set;}
        public utility_billing__c utilityBilling{get;set;}
        
        public objectWrapper(service__c serviceRecord,pcso__c pcsoRecord,utility_billing__c utilityBillingRecord){
            service = serviceRecord;
            pcso = pcsoRecord;
            utilityBilling = utilityBillingRecord;
        }
    }
}