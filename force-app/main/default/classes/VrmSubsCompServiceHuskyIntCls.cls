/*
Created: 6th January 2019
Description: WebService class to pull data from ServiceNow and store it in Vendor Subscription Title Components and also push parent Vendor Subscription Title data into ServiceNow
Test Class: VrmServiceHuskyIntegrationClsTest
*/
global class VrmSubsCompServiceHuskyIntCls
{
    global class VSTInfo
    {
        webservice String name;    
        webservice String u_service_delivery_specialist;
        webservice String sys_id;
    }
    
    global class PushSNowInfo
    {
        webservice String u_vrm_id;   
        webservice String u_license_expiration;
        webservice String u_mss_owner;
        webservice String u_mss_url;
        webservice String sys_id;
    }
    
    global class ResponseCls
    {
        webservice String sysId;
        webservice String msg;
    }
    
    webservice static List<ResponseCls> SNowToVrm(List<VSTInfo> vstObj)
    {
        Map<String,Id> userNameMap = new Map<String,Id>();
        
        List<jbrvrm__Vendor_Subscription_Title__c> vstList = new List<jbrvrm__Vendor_Subscription_Title__c>();
        List<jbrvrm__Vendor_Subscription_Title_Component__c> vstCompList = new List<jbrvrm__Vendor_Subscription_Title_Component__c>();
        List<String> alreadyUsedVstId = new List<String>();
        
        Set<String> sysIdSet = new Set<String>();
        
        List<User> userList = [Select id, name, Alias from User LIMIT 50000];
        
        if(userList != null && userList.size()>0)
        {
            for(User u : userList)
            {
                userNameMap.put(u.name.toLowerCase()+' ('+u.alias.toLowerCase()+')',u.id);
            }
        }
        
        if(vstObj != null && vstObj.size()>0)
        {
            for(VSTInfo obj : vstObj)
            {
                sysIdSet.add(obj.sys_id);
            }
        }
        
        Map<Id,jbrvrm__Vendor_Subscription_Title_Component__c> vstCompMap = new Map<Id,jbrvrm__Vendor_Subscription_Title_Component__c>([Select id, jbrvrm__ServiceNow_CI__c, jbrvrm__ServiceNow_Sys_Id__c , jbrvrm__Vendor_Subscription_Title__c from jbrvrm__Vendor_Subscription_Title_Component__c where jbrvrm__ServiceNow_Sys_Id__c =: sysIdSet]);
        List<ResponseCls> respClsList = new List<ResponseCls>();
        if(vstCompMap != null && vstCompMap.size()>0)
        {
            for(Id vstId : vstCompMap.keySet())
            {
                for(VSTInfo obj : vstObj)
                {
                    if(obj.sys_id == vstCompMap.get(vstId).jbrvrm__ServiceNow_Sys_Id__c)
                    {
                        //Storing values in Vendor Subscription Title Component
                        
                        jbrvrm__Vendor_Subscription_Title_Component__c vObj = new jbrvrm__Vendor_Subscription_Title_Component__c();
                        vObj.Id = vstId;
                        vObj.jbrvrm__ServiceNow_CI__c = obj.name;
                        vObj.jbrvrm__ServiceNow_Sys_Id__c = vstCompMap.get(vstId).jbrvrm__ServiceNow_Sys_Id__c;
                        vstCompList.add(vObj);
                        
                        if(vstCompMap.get(vstId).jbrvrm__Vendor_Subscription_Title__c != null && !(alreadyUsedVstId.contains(vstCompMap.get(vstId).jbrvrm__Vendor_Subscription_Title__c)))
                        {
                            jbrvrm__Vendor_Subscription_Title__c parentObj = new jbrvrm__Vendor_Subscription_Title__c();
                            parentObj.Id = vstCompMap.get(vstId).jbrvrm__Vendor_Subscription_Title__c;
                            
                            String[] deliverySpecialists = obj.u_service_delivery_specialist.split(',');
                            if(userNameMap.containsKey(deliverySpecialists[0].toLowerCase()))
                            {
                                parentObj.jbrvrm__Ops_Reviewer__c = userNameMap.get(deliverySpecialists[0].toLowerCase());
                            }
                            vstList.add(parentObj);
                            alreadyUsedVstId.add(vstCompMap.get(vstId).jbrvrm__Vendor_Subscription_Title__c);
                        }
                    }
                }
            }
            Database.SaveResult[] srVstList = Database.update(vstList, false);
            Database.SaveResult[] srList = Database.update(vstCompList, false);
            if(srList != null)
            {
                for (Database.SaveResult sr : srList) 
                {
                    ResponseCls respObj = new ResponseCls();
                    if (sr.isSuccess()) 
                    {
                        System.debug('Successfully inserted component : ' + sr.getId());
                        respObj.sysId = vstCompMap.get(sr.getId()).jbrvrm__ServiceNow_Sys_Id__c;
                        respObj.msg = 'Updated Successfully';
                        
                        respClsList.add(respObj);
                    }
                    else 
                    {
                        for(Database.Error err : sr.getErrors()) 
                        {
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            respObj.sysId = vstCompMap.get(sr.getId()).jbrvrm__ServiceNow_Sys_Id__c;
                            respObj.msg = 'Error - ' +err.getMessage();
                            respClsList.add(respObj);
                            
                        }
                    }
                    
                    system.debug('#### respClsList: ' + respClsList);
                }   
            }
        }
        
        return respClsList;
    }   
    
    webservice static List<PushSNowInfo> VrmToSNow()
    {
        Map<Id,String> userNameMap = new Map<Id,String>();
        
        List<PushSNowInfo> PushSNowInfoList = new List<PushSNowInfo>();
        List<jbrvrm__Vendor_Subscription_Title__c> existingVSTList = new List<jbrvrm__Vendor_Subscription_Title__c>();
        existingVSTList = [Select id, name, jbrvrm__ServiceNow_SYS_ID__c, jbrvrm__Contract_End_Date__c, jbrvrm__Vendor_Analyst__c, (Select id, jbrvrm__Vendor_Subscription_Title__c, jbrvrm__ServiceNow_SYS_ID__c from jbrvrm__Vendor_Subscription_Title_Components__r where jbrvrm__ServiceNow_SYS_ID__c != null AND jbrvrm__ServiceNow_SYS_ID__c != '' order by LastModifiedDate DESC) from jbrvrm__Vendor_Subscription_Title__c order by LastModifiedDate DESC];
        
        List<String> alreadyUsedSysId = new List<String>();
        List<User> userList = [Select id, name, Alias from User LIMIT 50000];
        
        if(userList != null && userList.size()>0)
        {
            for(User u : userList)
            {
                userNameMap.put(u.id,u.name.toLowerCase()+' ('+u.alias.toLowerCase()+')');
            }
        }
        
        if(existingVSTList != null && existingVSTList.size()>0)
        {
            for(jbrvrm__Vendor_Subscription_Title__c vst : existingVSTList)
            {
                if(vst.jbrvrm__Vendor_Subscription_Title_Components__r.size()>0)
                {
                    for(jbrvrm__Vendor_Subscription_Title_Component__c vstc : vst.jbrvrm__Vendor_Subscription_Title_Components__r)
                    {
                        if(!(alreadyUsedSysId.contains(vstc.jbrvrm__ServiceNow_SYS_ID__c)))
                        {
                            PushSNowInfo psObj = new PushSNowInfo();
                            psObj.sys_id = vstc.jbrvrm__ServiceNow_SYS_ID__c;
                            psObj.u_vrm_id = vst.Name;
                            psObj.u_mss_url = System.URL.getSalesforceBaseUrl().toExternalForm() + '/'+vst.Id;
                            if(userNameMap.containsKey(vst.jbrvrm__Vendor_Analyst__c))
                            {
                                psObj.u_mss_owner = userNameMap.get(vst.jbrvrm__Vendor_Analyst__c);
                            }    
                            if(vst.jbrvrm__Contract_End_Date__c != null)
                            {
                                String month = '';
                                String day = '';
                                if(vst.jbrvrm__Contract_End_Date__c.month() >= 1 && vst.jbrvrm__Contract_End_Date__c.month() <=9)
                                {
                                    month = '0'+vst.jbrvrm__Contract_End_Date__c.month();
                                }
                                else
                                {
                                    month = ''+vst.jbrvrm__Contract_End_Date__c.month();
                                }
                                if(vst.jbrvrm__Contract_End_Date__c.day() >=1 && vst.jbrvrm__Contract_End_Date__c.day() <=9)
                                {
                                    day = '0'+vst.jbrvrm__Contract_End_Date__c.day();
                                }
                                else
                                {
                                    day = ''+vst.jbrvrm__Contract_End_Date__c.day();
                                }
                                psObj.u_license_expiration = vst.jbrvrm__Contract_End_Date__c.year()+'-'+ month + '-' + day;
                            }
                            
                            PushSNowInfoList.add(psObj);
                            alreadyUsedSysId.add(vstc.jbrvrm__ServiceNow_SYS_ID__c);
                        }
                    }
                    
                
                }
            }
        }
        System.debug('#### PushSNowInfoList - ' + PushSNowInfoList);
        return PushSNowInfoList;
    }
}