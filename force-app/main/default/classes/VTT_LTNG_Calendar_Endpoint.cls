/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Endpoint for VTT Lightning Calendar
Test Class:     VTT_LTNG_Calendar_EndpointTest
History:        mbrim 2019-08-22 - Created.
*************************************************************************************************/
public with sharing class VTT_LTNG_Calendar_Endpoint {
    private static final String CLASS_NAME = String.valueOf(VTT_LTNG_Calendar_Endpoint.class);

    @AuraEnabled
    public static HOG_CustomResponseImpl loadCurrentUserData() {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_CalendarService().getCurrentUserData();
            System.debug(CLASS_NAME + ' loadCurrentUserData DATA ' + response);
        } catch (Exception ex){
            System.debug(CLASS_NAME + ' loadCurrentUserData exception ' + ex.getMessage());
            response.addError(ex);
        }

        return response;
    }

    /**
     * Endpoint for getting details about work order and activities when user clicks on date on the
     * calendar
     *
     * @param selectedDate - Date that user selected
     * @param tradesmanId  - Tradesmen that is selected on calendar
     *
     * @return Details of events on this date
     */
    @AuraEnabled
    public static HOG_CustomResponseImpl loadEventDataForDate(String selectedDate, String tradesmanId) {

        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

        try {
            response = new VTT_LTNG_CalendarService().getEntryDataForDate(selectedDate, tradesmanId);
            System.debug(CLASS_NAME + ' loadEventDataForDate DATA ' + response);
        } catch (Exception ex){
            System.debug(CLASS_NAME + ' loadEventDataForDate exception ' + ex.getMessage());
            response.addError(ex);
        }

       return response;
    }

    /**
    * Loads accounts for picklist
    *
    * @return
    */
    @AuraEnabled
    public static HOG_CustomResponseImpl loadVendorAccounts() {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new VTT_LTNG_CalendarService().getVendorAccounts();
            System.debug(CLASS_NAME + ' loadVendorAccounts DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' loadVendorAccounts exception ' + ex.getMessage());
            response.addError(ex);
        }
        return response;
    }

    /**
    * Loads list of tradesman for given account
    *
    * @return
    */
    @AuraEnabled
    public static HOG_CustomResponseImpl loadTradesman(String accountId) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new VTT_LTNG_CalendarService().getTradesmanForAccount(accountId);
            System.debug(CLASS_NAME + ' loadTradesman DATA ' + response);
        } catch (Exception ex) {
            System.debug(CLASS_NAME + ' loadTradesman exception ' + ex.getMessage());
            response.addError(ex);
        }
        return response;
    }

    /**
     * Endpoint for loading event for calendar
     *
     * @param tradesmanID - selected tradesman
     *
     * @return
     */
    @AuraEnabled
    public static HOG_CustomResponseImpl loadEventsForTradesman(String tradesmanID, Date startDate, Date endDate) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            if(tradesmanID != null){
                response = new VTT_LTNG_CalendarService().getTradesmanEvents(tradesmanID, startDate, endDate);
            } else {
                response.addError('Tradesman has to be selected. Try to refresh the page and select again..');
            }
        } catch (Exception ex) {
            response.addError(ex);
        }
        return response;

    }

    @AuraEnabled
    public static HOG_CustomResponseImpl loadOnHoldActivities(String tradesmanID) {
        HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
        try {
            response = new VTT_LTNG_CalendarService().getOnHoldActivities(tradesmanID);
        } catch (Exception ex) {
            response.addError(ex);
        }
        return response;

    }
}