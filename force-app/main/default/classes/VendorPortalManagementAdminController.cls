/*-------------------------------------------------------------------------------------------------
Author     : Marcel Brimus
Company    : Husky Energy
Description: Controller for Vendor Portal Management Page used by HOG Admins
Inputs     : N/A
Test Class : VendorPortalMngAdminConTest
Changes    : 10.06. W-001545 & 1546 - added method to get lightning user
									- new method to get RecordType
									- created new Buttons on page for lightning and added scripts
---------------------------------------------------------------------------------------------------*/
public without sharing class VendorPortalManagementAdminController {
	public Profile vendorProfile { get; private set; }
	public List<SelectOption> accountSelect { get; set; }
	public List<Account> portalAccounts { get; private set; }
	public String selectedAccountId { get; set; }
	public Account selectedAccount { get; set; }

	//Used for conditional rendering
	public String contactIdToEnablePortal { get; set; }
	public String contactIdToDisablePortal { get; set; }
	public String userType { get; set; }

	// used for paginantion
	public Integer sizePortalEnabled { get; set; }
	public Integer sizePortalDisabled { get; set; }
	public Integer noOfRecordsPortalEnabled { get; set; }
	public Integer noOfRecordsPortalDisabled { get; set; }
	public List<SelectOption> paginationSizeOptionsPortalEnabled { get; set; }
	public List<SelectOption> paginationSizeOptionsPortalDisabled { get; set; }

	public VendorPortalManagementAdminController() {
		this.vendorProfile = [Select id,name from Profile where name = :VendorPortalUtility.VENDOR_PORTAL_PROFILE limit 1];
		this.portalAccounts = [
				SELECT Id,
						Name,
						Remaining_Licenses__c,
						Total_Licenses__c,
						Used_Licenses__c,
						BillingAddress,
						BillingStreet,
						BillingCity,
						BillingState,
						BillingPostalCode,
						BillingCountry
				FROM Account
				WHERE IsPartner = true
				ORDER BY Name ASC
		];
		popupateAccountSelect(this.portalAccounts);

		// default pagination list size, values
		this.sizePortalEnabled = 10;
		this.paginationSizeOptionsPortalEnabled = new List<SelectOption>();
		this.paginationSizeOptionsPortalEnabled.add(new SelectOption('10', '10'));
		this.paginationSizeOptionsPortalEnabled.add(new SelectOption('25', '25'));
		this.paginationSizeOptionsPortalEnabled.add(new SelectOption('50', '50'));
		this.paginationSizeOptionsPortalEnabled.add(new SelectOption('100', '100'));

		this.sizePortalDisabled = 10;
		this.paginationSizeOptionsPortalDisabled = new List<SelectOption>();
		this.paginationSizeOptionsPortalDisabled.add(new SelectOption('10', '10'));
		this.paginationSizeOptionsPortalDisabled.add(new SelectOption('25', '25'));
		this.paginationSizeOptionsPortalDisabled.add(new SelectOption('50', '50'));
		this.paginationSizeOptionsPortalDisabled.add(new SelectOption('100', '100'));

	}

	public void popupateAccountSelect(List<Account> accounts) {
		if (accounts != null && accounts.size() > 0) {
			this.accountSelect = new List<SelectOption>();
			for (Account a : accounts) {
				this.accountSelect.add(new SelectOption(a.Id, a.Name));
			}
			//Onload populate first account info
			this.selectedAccount = accounts.get(0);
			this.selectedAccountId = this.selectedAccount.id;
		}
	}

	public PageReference getAccountInfo() {
		this.selectedAccount = [
				SELECT Id,
						Name,
						Remaining_Licenses__c,
						Total_Licenses__c,
						Used_Licenses__c,
						BillingAddress,
						BillingStreet,
						BillingCity,
						BillingState,
						BillingPostalCode,
						BillingCountry
				FROM Account
				WHERE id = :selectedAccountId
		];

		this.setConPortalDisabled = null;
		this.setConPortalEnabled = null;

		this.refreshPageSizePortalDisabled();
		this.refreshPageSizePortalEnabled();

		return null;
	}

	// pagination controller for portal DISABLED users
	public ApexPages.StandardSetController setConPortalDisabled {
		get {
			if (setConPortalDisabled == null) {
				setConPortalDisabled = new ApexPages.StandardSetController(Database.getQueryLocator(
				[
						SELECT
								id, Name, firstName, lastName, is_Vendor_Portal_User__c, email,
								AccountId
						FROM Contact
						WHERE AccountId = :selectedAccountId
						AND RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact')
						AND is_Vendor_Portal_User__c = false
						ORDER BY Name
				]));
				setConPortalDisabled.setPageSize(this.sizePortalDisabled);
				this.noOfRecordsPortalDisabled = setConPortalDisabled.getResultSize();
			}
			return setConPortalDisabled;
		}
		set;
	}

	public List<Contact> getPortalDisabledVendorSet() {
		return (List<Contact>) setConPortalDisabled.getRecords();
	}

	public PageReference refreshPageSizePortalDisabled() {
		setConPortalDisabled.setPageSize(this.sizePortalDisabled);
		return null;
	}

	// pagination controller for portal ENABLED users
	public ApexPages.StandardSetController setConPortalEnabled {
		get {
			if (setConPortalEnabled == null) {
				setConPortalEnabled = new ApexPages.StandardSetController(Database.getQueryLocator(
				[
						SELECT id,
								Name, firstName, lastName, is_Vendor_Portal_User__c, Vendor_Portal_User_Role__c,
								email, User_Name__c, AccountId
						FROM Contact
						WHERE AccountId = :selectedAccountId
						AND RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact')
						AND is_Vendor_Portal_User__c = true
						AND User__c <> null
						ORDER BY Name
				]));
				setConPortalEnabled.setPageSize(this.sizePortalEnabled);
				this.noOfRecordsPortalEnabled = setConPortalEnabled.getResultSize();
			}
			return setConPortalEnabled;
		}
		set;
	}

	public List<Contact> getPortalEnabledVendorSet() {
		return (List<Contact>) setConPortalEnabled.getRecords();
	}

	//Changes the size of pagination
	public PageReference refreshPageSizePortalEnabled() {
		setConPortalEnabled.setPageSize(this.sizePortalEnabled);
		return null;
	}

	//This method creates Portal user under this contact
	//Firing trigger on User object that will add this user to Public Group (for sharing) and assign him PS
	//Using this on previously activated user will re-activate him, removes and adds correct permission set
	public PageReference enablePortalUser() {
		System.debug('Enabling this contact as user: ' + contactIdToEnablePortal);
		Contact c = [SELECT id, Name, firstName, lastName, is_Vendor_Portal_User__c, email, AccountId FROM Contact WHERE id = :contactIdToEnablePortal];


		if (c.firstName == null || c.lastName == null || c.email == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: First Name, Last Name and Email cannot be empty on contact...'));
		} else if (this.selectedAccount.Remaining_Licenses__c < 1) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: You cannot create any more users because your Account doesnt have any more remaining licenses'));
		} else {
			VendorportalUtility.createOrReactivatePortalUser(contactIdToEnablePortal, c.email, c.firstName, c.lastName, vendorProfile.id, userType);
		}
		return null;
	}

	//This is used to disable portal user based on clicked contact - this contact needs to be enabled as portal user
	//Disabling user will remove him from Public Group (used for sharing)
	//Contact will remain and can be reactivated again
	public PageReference disablePortalUser() {

		Contact c = [SELECT id, AccountId FROM Contact WHERE id = :contactIdToDisablePortal];
		Account a = [SELECT id FROM Account WHERE Id = :c.AccountId];
		User user = [
				SELECT Id, Name, IsPortalEnabled, ProfileId, ContactID, IsActive, UserName, Email
				FROM user
				WHERE IsActive = true
				AND IsPortalEnabled = true
				AND ProfileId = :vendorProfile.id
				AND ContactID = :c.id
		];

		user.IsActive = false;
		//user.UserName = user.UserName + '.disabled';
		user.Email = user.Email + '.disabled';
		System.debug('Disabling this user: ' + user);
		try {
			update user;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'You have successfully disabled: ' + user.name + ', please refresh this page after a while in order to changes to appear'));
		} catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Error when disabling user - ' + ex.getMessage()));
			System.debug('EXCEPTION: ' + ex);
		}

		return null;
	}

	public Boolean getIsLightningUser() {
		return HOG_GeneralUtilities.isUserUsingLightning();
	}

	public Id getVendorRecordType() {
		RecordType HOGVendorContact = [SELECT Id,Name FROM RecordType WHERE Name = 'HOG Vendor Contact' LIMIT 1];
		return HOGVendorContact.Id;
	}
}