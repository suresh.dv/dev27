@isTest
public class DEFT_UtilitiesTest {

    @isTest static void TestTriggerBeforeInsertEOJ() {
        User deftUser = DEFT_TestData.createDeftUser();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

        User runningUser = [Select Id, Name From User Where Id = :UserInfo.getUserId()];
        System.runAs(runningUser) {
            //Setup
            Route__c route = RouteTestData.createRoute('Test Route');
            route.Operator_1_User__c = deftUser.Id;
            route.Operator_2_User__c = deftUser.Id;
            route.Operator_3_User__c = deftUser.Id;
            route.Operator_4_User__c = deftUser.Id;
            route.Operator_5_User__c = deftUser.Id;
            route.Operator_6_User__c = deftUser.Id;
            insert route;

            //Create Service Rig Program
            HOG_Service_Rig_Program__c serviceRigProgram = DEFT_TestData.createServiceRigProgram(deftUser, productionCoordinator, serviceRigCoordinator);
            serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
            serviceRigProgram.Execution_Approver__c = serviceRigPlanner.Id;
            update serviceRigProgram;

            Test.startTest();
            approveSrpFinancial(serviceRigProgram, runningUser);
            approveSrpPET(serviceRigProgram, runningUser);

            HOG_EOJ__c eoj = DEFT_TestData.createEOJReport(serviceRigProgram, 'New');

            //Test Email Fields
            eoj = [
                    Select Id, Name, Production_Engineer_Email__c, Field_Operator1_Email__c, Field_Operator2_Email__c,
                            Field_Operator3_Email__c, Field_Operator4_Email__c, Field_Operator5_Email__c, Field_Operator6_Email__c,
                            Field_Senior_Email__c, Production_Coordinator_Email__c,Operations_Coordinator_Email__c,
                            Service_Rig_Coordinator_Email__c, Service_Rig_Planner_Email__c
                    From HOG_EOJ__c
                    Where Id = :eoj.Id
            ];

            System.debug('deft test eoj: ' + eoj);
            System.debug('deft test eoj.Field_Operator5_Email__c: ' + eoj.Field_Operator5_Email__c);
            System.debug('deft test eoj.Field_Operator6_Email__c: ' + eoj.Field_Operator6_Email__c);
            System.debug('deft test deftUser.Email: ' + deftUser.Email);
            System.assert(eoj.Production_Engineer_Email__c.equalsIgnoreCase(deftUser.Email));
            System.assert(eoj.Field_Operator1_Email__c.equalsIgnoreCase(deftUser.Email));
            System.assert(eoj.Field_Operator2_Email__c.equalsIgnoreCase(deftUser.Email));
            System.assert(eoj.Field_Operator3_Email__c.equalsIgnoreCase(deftUser.Email));
            System.assert(eoj.Field_Operator4_Email__c.equalsIgnoreCase(deftUser.Email));
            //System.assert(eoj.Field_Operator5_Email__c.equalsIgnoreCase(deftUser.Email));
            //System.assert(eoj.Field_Operator6_Email__c.equalsIgnoreCase(deftUser.Email));
            System.assert(eoj.Field_Senior_Email__c.equalsIgnoreCase(deftUser.Email));
            System.assert(eoj.Production_Coordinator_Email__c.equalsIgnoreCase(productionCoordinator.Email));
            System.assert(eoj.Operations_Coordinator_Email__c.equalsIgnoreCase(deftUser.Email));
            //System.assert(eoj.Service_Rig_Coordinator_Email__c.equalsIgnoreCase(serviceRigCoordinator.Email));
            System.assert(eoj.Service_Rig_Planner_Email__c.equalsIgnoreCase(serviceRigPlanner.Email));
        }
    }

    @isTest static void TestRigCompaniesPicklists() {
        User runningUser = [Select Id, Name From User Where Id = :UserInfo.getUserId()];
        System.runAs(runningUser) {
            //Setup
            Account acc = DEFT_TestData.createTestAccount();
            Vendor_Unit_Company__c vuc = DEFT_TestData.createVUC(acc.Id, 'Rig Company');
            HOG_Rig__c rig = DEFT_TestData.createRig(vuc.Id);

            List<SelectOption> RigCompanies = new List<SelectOption>(DEFT_Utilities.getRigCompanies());
            List<SelectOption> Rigs = new List<SelectOption>(DEFT_Utilities.getRigs(vuc.Id));

            System.assertEquals(RigCompanies.size(), 2);
            System.assertEquals(Rigs.size(), 2);
        }
    }

    public static void approveSrpFinancial(HOG_Service_Rig_Program__c serviceRigProgram, User runningUser) {
        //Create Financial Approval Request for the SRP
        Approval.ProcessSubmitRequest financialApprovalRequest = new Approval.ProcessSubmitRequest();
        financialApprovalRequest.setComments('SRP Financial Approval Request');
        financialApprovalRequest.setObjectId(serviceRigProgram.Id);

        //Submit the record to specific process and skip the criteria evaluation
        financialApprovalRequest.setProcessDefinitionNameOrId('Service_Rig_Program_Financial_Approval');
        financialApprovalRequest.setSkipEntryCriteria(true);

        //Submit the approval request
        Approval.ProcessResult finApprovalResult = Approval.process(financialApprovalRequest);

        //Verify the finApprovalResult
        System.assert(finApprovalResult.isSuccess());
        System.assertEquals(
                'Pending', finApprovalResult.getInstanceStatus(),
                'Instance Status' + finApprovalResult.getInstanceStatus());

        // Approve the submitted request
        List<Id> financialApprovalRequestWorkIds = finApprovalResult.getNewWorkitemIds();
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest financialStep =
                new Approval.ProcessWorkitemRequest();
        financialStep.setComments('Financial Approving request.');
        financialStep.setAction('Approve');
        financialStep.setNextApproverIds(new Id[]{
                runningUser.Id
        });
        financialStep.setWorkitemId(financialApprovalRequestWorkIds.get(0));
        //Submit the approval request
        finApprovalResult = Approval.process(financialStep);
    }

    public static void approveSrpPET(HOG_Service_Rig_Program__c serviceRigProgram, User runningUser) {
        //Create PET Approval Request for the SRP
        Approval.ProcessSubmitRequest petApprovalRequest = new Approval.ProcessSubmitRequest();
        petApprovalRequest.setComments('SRP PET Approval Request');
        petApprovalRequest.setObjectId(serviceRigProgram.Id);

        //Submit the record to specific process and skip the criteria evaluation
        petApprovalRequest.setProcessDefinitionNameOrId('Service_Rig_Program_P_E_T_Review');
        petApprovalRequest.setSkipEntryCriteria(true);

        //Submit the approval request
        Approval.ProcessResult petApprovalResult = Approval.process(petApprovalRequest);

        //Verify the petApprovalResult
        System.assert(petApprovalResult.isSuccess());
        System.assertEquals(
                'Pending', petApprovalResult.getInstanceStatus(),
                'Instance Status' + petApprovalResult.getInstanceStatus());

        // Approve the submitted request
        List<Id> petApprovalRequestWorkIds = petApprovalResult.getNewWorkitemIds();

        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest executionStep =
                new Approval.ProcessWorkitemRequest();
        executionStep.setComments('Execution Approving request.');
        executionStep.setAction('Approve');
        executionStep.setNextApproverIds(new Id[]{
                runningUser.Id
        });

        // Use the ID from the newly created item to specify the item to be worked
        executionStep.setWorkitemId(petApprovalRequestWorkIds.get(0));

        // Submit the request for approval
        Approval.ProcessResult executionResult = Approval.process(executionStep);

        // Verify the results
        System.assert(executionResult.isSuccess(), 'Result Status:' + executionResult.isSuccess());

        // Technical Approval
        petApprovalRequestWorkIds = executionResult.getNewWorkitemIds();
        Approval.ProcessWorkitemRequest technicalApproval = new Approval.ProcessWorkitemRequest();
        technicalApproval.setComments('technicalApproval Approving Request');
        technicalApproval.setAction('Approve');
        technicalApproval.setWorkitemId(petApprovalRequestWorkIds.get(0));

        //Submit technical approval request
        Approval.ProcessResult techResult = Approval.process(technicalApproval);

        System.assertEquals(
                'Approved', techResult.getInstanceStatus(),
                'Instance Status ' + techResult.getInstanceStatus());
    }

}