@isTest
public class NCRLoginControllerTest {
  @IsTest(SeeAllData = True) 
    public static void testNCRLoginController () {
       NCRLoginController controller = new NCRLoginController();
       System.assertNotEquals(null, controller.forwardToCustomAuthPage());    
        /*
        Account account = AccountTestData.createAccount('accountName', null);
        Contact contact = ContactTestData.createContact(account.Id, 'contactFName', 'contactLName');
        insert contact;
        User usr = UserTestData.createTestCustomerPortalUser(contact.Id);
        */
        controller.username = 'test@huskyenergy.com';
        controller.password = 'test1234';
        System.assert(controller.login() ==  null);
        
        List<User> usrList = [SELECT Id, Username, Email, IsActive, UserType, IsPortalEnabled, ContactId 
                          FROM User 
                      WHERE UserType =: 'PowerPartner' AND IsPortalEnabled = True];
        
        
        if(usrList.size() > 0) {
            controller.username = usrList[0].Username;
            System.assert(controller.login() ==  null);
        }
        
        //controller.username = usr.Username;
        //System.assert(controller.login() ==  null);
    }    
}