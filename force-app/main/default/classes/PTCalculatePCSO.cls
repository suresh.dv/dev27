public class PTCalculatePCSO{
    public double powerFactorReviewValueDouble = double.valueOf(returnControllerSettingValue('powerFactorReviewValue'));
    public double reviewContractMinimumValueDouble = double.valueOf(returnControllerSettingValue('reviewContractMinimumValue'));    
    public integer pcsoMonthsInt = integer.valueOf(returnControllerSettingValue('allPCSOs'));
    public integer FortisAlbertaLowestRateMinimum = integer.valueOf(returnControllerSettingValue('FortisAlbertaLowestRateMinimum'));
    public integer ATCOLowestRateMinimum = integer.valueOf(returnControllerSettingValue('ATCOLowestRateMinimum'));
    public double FARoundingErrorLowerLimit = double.valueOf(returnControllerSettingValue('FARoundingErrorLowerLimit'));
    public double FARoundingErrorUpperLimit = double.valueOf(returnControllerSettingValue('FARoundingErrorUpperLimit'));
    public List<string> pcsoList = new List<string>{'Idle/Salvage','Power Factor Review','Review Contract Minimum'};
    public string PCSOTYPE;
    public integer PCSOCounter;
    public integer newPCSOCounter;
    public integer validatePCSOCounter;
    //public string pcsoMonthsString = string.valueOf(pcsoMonthsInt);
    
    public decimal returnControllerSettingValue(String name){
        Decimal controllerSettingValue;
        if(PT_PCSO_Settings__c.getValues(name) != null){
            controllerSettingValue = PT_PCSO_Settings__c.getValues(name).PCSO_Value__c;
        }
        else{
            controllerSettingValue = 0;
        }
        return controllerSettingValue;
    }
    
    public void CalculatePCSO(){
        //The next line of code initialize the initialServiceList with the PCSO information related to the Service
        List<Service__c> initialServiceList = PTCalculatePCSOCacheQuery.queryPcsoInfo();
        
        //The next lines of code initialite all the variables that are going to be used to execute this code
        List<Service__c> zeroMeteringRefinedServiceList = new List<Service__c>();
        List<Service__c> powerFactorRefinedServiceList = new List<Service__c>();
        List<Service__c> reviewContractRefinedServiceList = new List<Service__c>();
        Map<id,Service__c> reviewContractRefinedServiceMap = new Map<id,Service__c>();
        Map<id,Service__c> billingMap = new Map<id,Service__c>(initialServiceList);
        //Map<id,Service__c> billingMap = PTCalculatePCSOBillingCacheQuery.querybillingMap();
        List<Service__c> zeroMeteringValidateServiceList = new List<Service__c>();
        List<Service__c> powerFactorValidateServiceList = new List<Service__c>();
        List<Service__c> reviewContractValidateServiceList = new List<Service__c>();             
        
        for(Service__c service:initialServiceList){
            if(service.utility_billings__r.size() >0 && service.pcsos__r.size() == 0){// Validates that the service contains billings and there is no PCSO linked to the Service.
                zeroMeteringRefinedServiceList.add(service);
                powerFactorRefinedServiceList.add(service);
                reviewContractRefinedServiceMap.put(service.id,service);
                system.debug(reviewContractRefinedServiceMap.size());
            }
            else{
                PCSOTYPE = '';
                for(String pcsoType:pcsoList){
                    PCSOTYPE = pcsoType;
                    PCSOCounter = 0;
                    newPCSOCounter = 0;
                    validatePCSOCounter = 0;
                	for(PCSO__c pcso:service.pcsos__r){
                    	if(PCSOTYPE == pcso.PCSO_type__c){
                            PCSOCounter = PCSOCounter + 1;
                            if(pcso.PCSO_Status__c == 'Implemented'){
                                newPCSOCounter = newPCSOCounter + 1;
                            }
                            else if(pcso.PCSO_Status__c != 'Implemented' && pcso.PCSO_Status__c != 'Audited'){
                                validatePCSOCounter = validatePCSOCounter + 1;
                            }
                    	}
                	}
                    if(PCSOTYPE == 'Idle/Salvage' && PCSOCounter == newPCSOCounter){zeroMeteringRefinedServiceList.add(service);}
                    else if(PCSOTYPE == 'Review Contract Minimum' && PCSOCounter == newPCSOCounter){reviewContractRefinedServiceMap.put(service.id,service);}
                    else if(PCSOTYPE == 'Power Factor Review' && PCSOCounter == newPCSOCounter){powerFactorRefinedServiceList.add(service);}
                    else if(PCSOTYPE == 'Idle/Salvage' && validatePCSOCounter >= 1){zeroMeteringValidateServiceList.add(service);}
                    else if(PCSOTYPE == 'Review Contract Minimum' && validatePCSOCounter >= 1){reviewContractValidateServiceList.add(service);}
                    else if(PCSOTYPE == 'Power Factor Review' && validatePCSOCounter >= 1){powerFactorValidateServiceList.add(service);}
                }
                  
            }    
        }          
        
        if(zeroMeteringRefinedServiceList.size() > 0){ //This condition evaluates all the Services that were added to the zeroMeteringRefinedServiceList and if a PCSO is of the type Idle/Salvaged is assigned to the service it will take the Service out of the reviewContractRefinedServiceMap
            List<PCSO__c> zeroMeteringPCSOList = CalculateZeroMetering(zeroMeteringRefinedServiceList,billingMap);
            if (zeroMeteringPCSOList.size() > 0){
                insert zeroMeteringPCSOList;
                for(PCSO__c serviceToRemove: zeroMeteringPCSOList){
                    reviewContractRefinedServiceMap.remove(serviceToRemove.Service_id__c);    
                }
                reviewContractRefinedServiceList = reviewContractRefinedServiceMap.values();              
            }
            else{
                reviewContractRefinedServiceList = reviewContractRefinedServiceMap.values();    
            }            
        }
        
        if(powerFactorRefinedServiceList.size() > 0){ //This condition evaluates all the Services that were added to the power FactorRefinedServiceList
            List<PCSO__c> powerFactorPCSOList = CalculatePowerFactor(powerFactorRefinedServiceList,billingMap);
            if (powerFactorPCSOList.size() > 0){
                insert powerFactorPCSOList;
            }
        }
        
        if(reviewContractRefinedServiceList.size() > 0){ //This condition evaluates all the Services that were added to the reviewContractRefinedServiceList
            List<PCSO__c> reviewContractMinimumPCSOList = CalculateReviewCM(reviewContractRefinedServiceList,billingMap);
            if (reviewContractMinimumPCSOList.size() > 0){
                insert reviewContractMinimumPCSOList;
            }
        }
        
        if(zeroMeteringValidateServiceList.size() > 0){ //All Services added to the zeroMeteringValidateServiceList will be validated in the follwing lines of codes
            ValidatePCSOS(zeroMeteringValidateServiceList,'Zero Metering');
        }
        
        if(reviewContractValidateServiceList.size() > 0){ //All PCSOs added to the reviewContractValidatedServiceList will be validated in the follwing lines of codes
            ValidatePCSOS(reviewContractValidateServiceList,'Review Contract Minimum');    
        }
        
        if(powerFactorValidateServiceList.size() > 0){ //All PCSOs added to the powerFactorValidateServiceList will be validated in the follwing lines of codes
            ValidatePCSOS(powerFactorValidateServiceList,'Power Factor Review');     
        }
        
    }    
    public List<PCSO__c> CalculateZeroMetering(List<Service__c> refinedServiceList, Map<id,Service__c> billingMap){ //Method to evaluate all the Services that are inside the zeroMeteringRefinedServiceList
        List<PCSO__c> pcsoInsertList = new List<PCSO__c>();
        for(Service__c service : refinedServiceList){
            integer counter = 0;
            integer billingCounter = 0;
            integer validBillingCounter = 0;
            Utility_Billing__c baselineBilling = new Utility_Billing__c();
            for(Utility_Billing__c utilityBilling:service.utility_billings__r){              
                if(validBillingCounter < pcsoMonthsInt){
                    if(utilityBilling.Invalid_Billing__c == false){ //Validates that the billing taken into account is valid
                        validBillingCounter++;
                        if(utilityBilling.Ratchet_billing__c == false && utilityBilling.metered_demand_kw__c == 0 && utilityBilling.Total_Volume__c == 0) //Validates that the billing meet the conditions of an Idle/Salvage case
                        {
                            if(counter == 0)
                            {
                                Service__c billingServiceMap = billingMap.get(service.id);
                                if(billingServiceMap != null){ //This grabs the baseline values that are going to be stored in the PCSO
                                    baselineBilling.Average_Billing_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Billing_Demand_last_12_months__c;
                                    baselineBilling.Average_Metered_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Metered_Demand_last_12_months__c;
                                    baselineBilling.Average_kVA_Demand_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_kVA_Demand_last_12_months__c;
                                    baselineBilling.Average_Total_Volume_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_Total_Volume_last_12_months__c;
                                }
                            }
                            counter++;
                            if(utilityBilling.Billing_Demand_KW__c > 0){
                                billingCounter++;
                            }
                        }
                    }
                }
                else{
                    break;
                }
            }
            if(counter >= pcsoMonthsInt && billingCounter > 1){//If counter is greater or equal to Custom Setting variable it means that there are at least some amount of billings or more that complies with the condition Idle/Salvage
                PCSO__c pcso = new PCSO__c();
                pcso.OwnerId=service.OwnerId;
                pcso.Service_id__c = service.Id;
                pcso.PCSO_type__c = 'Idle/Salvage';
                pcso.System_Review__c = 'Valid';
                pcso.Months_Valid__c = pcsoMonthsInt;
                pcso.Billing_Demand_kW_Baseline__c = baselineBilling.Average_Billing_Demand_last_12_months__c;
                pcso.Metered_Demand_kW_Baseline__c = baselineBilling.Average_Metered_Demand_last_12_months__c;
                pcso.Metered_Demand_kVA_Baseline__c = baselineBilling.Average_kVA_Demand_last_12_months__c;
                pcso.Energy_Consumption_kWh_Baseline__c = baselineBilling.Average_Total_Volume_last_12_months__c;
                pcsoInsertList.add(pcso);
            }                                  
        }
        return pcsoInsertList;
    }
    
    public List<PCSO__c> CalculatePowerFactor(List<Service__c> refinedServiceList, Map<id,Service__c> billingMap){
        List<PCSO__c> pcsoInsertList = new List<PCSO__c>();
        for(Service__c service : refinedServiceList){
            integer counter = 0;
            integer validBillingCounter = 0;
            Utility_Billing__c baselineBilling = new Utility_Billing__c();
            for(Utility_Billing__c utilityBilling:service.utility_billings__r){
                if(validBillingCounter < pcsoMonthsInt){
                    if(utilityBilling.Invalid_Billing__c == false){ //Validates that the billing taken into account is valid
                        validBillingCounter++;
                        if(utilityBilling.Ratchet_billing__c == false && utilityBilling.Metered_Demand_KVA__c > 0 && utilityBilling.Metered_Demand_KW__c > 0 && (utilityBilling.Metered_Demand_KW__c / utilityBilling.Metered_Demand_KVA__c) < powerFactorReviewValueDouble) //Validates that the billing meet the conditions of a Power Factorcase
                        {
                            if(counter == 0)
                            {
                                Service__c billingServiceMap = billingMap.get(service.id);
                                if(billingServiceMap != null){ //This grabs the baseline values that are going to be stored in the PCSO
                                    baselineBilling.Average_Billing_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Billing_Demand_last_12_months__c;
                                    baselineBilling.Average_Metered_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Metered_Demand_last_12_months__c;
                                    baselineBilling.Average_kVA_Demand_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_kVA_Demand_last_12_months__c;
                                    baselineBilling.Average_Total_Volume_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_Total_Volume_last_12_months__c;
                                }
                            }                          
                            counter++;
                        }
                    }
                }
                else{
                    break;
                }
            }
            if(counter >= pcsoMonthsInt){//If counter is greater or equal to Custom Setting variable it means that there are at least some amount of billings or more that complies with the condition Idle/Salvage
                PCSO__c pcso = new PCSO__c();
                pcso.OwnerId=service.OwnerId;
                pcso.Service_id__c = service.Id;
                pcso.PCSO_type__c = 'Power Factor Review';
                pcso.System_Review__c = 'Valid';
                pcso.Months_Valid__c = pcsoMonthsInt;
                pcso.Billing_Demand_kW_Baseline__c = baselineBilling.Average_Billing_Demand_last_12_months__c;
                pcso.Metered_Demand_kW_Baseline__c = baselineBilling.Average_Metered_Demand_last_12_months__c;
                pcso.Metered_Demand_kVA_Baseline__c = baselineBilling.Average_kVA_Demand_last_12_months__c;
                pcso.Energy_Consumption_kWh_Baseline__c = baselineBilling.Average_Total_Volume_last_12_months__c;                
                pcsoInsertList.add(pcso);
            }                                  
        }
        return pcsoInsertList;
    }
    
    public List<PCSO__c> CalculateReviewCM(List<Service__c> refinedServiceList, Map<id,Service__c> billingMap){
        List<PCSO__c> pcsoInsertList = new List<PCSO__c>();
        for(Service__c service : refinedServiceList){
            integer counter = 0;
            integer validBillingCounter = 0;
            Utility_Billing__c baselineBilling = new Utility_Billing__c();
            if(service.utility_company__r.name == 'FortisAlberta'){ //When calculating Review of Contract Minimum, FortisAlberta has a different way to evalute it thant ATCO
                for(Utility_Billing__c utilityBilling:service.utility_billings__r){
                    if(utilityBilling.Metered_Demand_KW__c > 0 && utilityBilling.Total_Volume__c > 0 && utilityBilling.Wire_Cost__c > 0 && utilityBilling.Metered_Demand_KW__c != null && utilityBilling.Total_Volume__c != null && utilityBilling.Wire_Cost__c != null){//This condition was created to avoid the data errors coming from Enmax's reports
                        if(validBillingCounter < pcsoMonthsInt){
                            if(utilityBilling.Invalid_Billing__c == false ){ //Validates that the billing taken into account is valid
                                validBillingCounter++;
                                if((utilityBilling.Metered_Demand_KVA__c*0.9).setScale(2,RoundingMode.HALF_UP)+FARoundingErrorUpperLimit <double.valueof(utilityBilling.Billing_Demand_KW__c) || (utilityBilling.Metered_Demand_KVA__c*0.9).setScale(2,RoundingMode.HALF_UP)-FARoundingErrorLowerLimit >double.valueof(utilityBilling.Billing_Demand_KW__c)){ // Range created to avoid creating false RCM PCSOs when Fortis is billing at 90% of KVA because of the bad Power Factor
                                    if(utilityBilling.Ratchet_billing__c == false && utilityBilling.Metered_Demand_KW__c < utilityBilling.Billing_Demand_KW__c && ((utilityBilling.Billing_Demand_KW__c - utilityBilling.Metered_Demand_KW__c)/utilityBilling.Billing_Demand_KW__c) >= reviewContractMinimumValueDouble && utilityBilling.Billing_Demand_KW__c> FortisAlbertaLowestRateMinimum) //Validates that the billing meet the conditions of a Review of Contract Minimum case
                                    {
                                        if(counter == 0)
                                        {
                                            Service__c billingServiceMap = billingMap.get(service.id);
                                            if(billingServiceMap != null){ //This grabs the baseline values that are going to be stored in the PCSO
                                                baselineBilling.Average_Billing_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Billing_Demand_last_12_months__c;
                                                baselineBilling.Average_Metered_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Metered_Demand_last_12_months__c;
                                                baselineBilling.Average_kVA_Demand_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_kVA_Demand_last_12_months__c;
                                                baselineBilling.Average_Total_Volume_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_Total_Volume_last_12_months__c;
                                            }
                                        }                                   
                                        counter++;
                                    }
                                } 
                            }
                        }
                        else{
                            break;
                        }
                    }
                } 
            }
            else{
                for(Utility_Billing__c utilityBilling:service.utility_billings__r){
                    if(utilityBilling.Metered_Demand_KW__c > 0 && utilityBilling.Total_Volume__c > 0 && utilityBilling.Wire_Cost__c > 0 && utilityBilling.Metered_Demand_KW__c != null && utilityBilling.Total_Volume__c != null && utilityBilling.Wire_Cost__c != null){//This condition was created to avoid the data errors coming from Enmax's reports
                        if(validBillingCounter < pcsoMonthsInt){
                            if(utilityBilling.Invalid_Billing__c == false ){
                                validBillingCounter++;
                                if(utilityBilling.Ratchet_billing__c == false && utilityBilling.Metered_Demand_KW__c < utilityBilling.Billing_Demand_KW__c && ((utilityBilling.Billing_Demand_KW__c - utilityBilling.Metered_Demand_KW__c)/utilityBilling.Billing_Demand_KW__c) >= reviewContractMinimumValueDouble && utilityBilling.Billing_Demand_KW__c> ATCOLowestRateMinimum ) //Validates that the billing meet the conditions of a Review of Contract Minimum case
                                {
                                    if(counter == 0)
                                    {
                                        Service__c billingServiceMap = billingMap.get(service.id);
                                        if(billingServiceMap != null){ //This grabs the baseline values that are going to be stored in the PCSO
                                            baselineBilling.Average_Billing_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Billing_Demand_last_12_months__c;
                                            baselineBilling.Average_Metered_Demand_last_12_months__c =  (billingServiceMap.Utility_Billings__r.get(0)).Average_Metered_Demand_last_12_months__c;
                                            baselineBilling.Average_kVA_Demand_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_kVA_Demand_last_12_months__c;
                                            baselineBilling.Average_Total_Volume_last_12_months__c = (billingServiceMap.Utility_Billings__r.get(0)).Average_Total_Volume_last_12_months__c;
                                        }
                                    }                                   
                                    counter++;
                                }
                            }
                        }
                        else{
                            break;
                        }
                    }
                }                
            }
            if(counter >= pcsoMonthsInt){//If counter is greater or equal to Custom Setting variable it means that there are at least some amount of billings or more that complies with the condition Idle/Salvage
                PCSO__c pcso = new PCSO__c();
                pcso.OwnerId=service.OwnerId;
                pcso.Service_id__c = service.Id;
                pcso.PCSO_type__c = 'Review Contract Minimum';
                pcso.System_Review__c = 'Valid';
                pcso.Months_Valid__c = pcsoMonthsInt;
                pcso.Billing_Demand_kW_Baseline__c = baselineBilling.Average_Billing_Demand_last_12_months__c;
                pcso.Metered_Demand_kW_Baseline__c = baselineBilling.Average_Metered_Demand_last_12_months__c;
                pcso.Metered_Demand_kVA_Baseline__c = baselineBilling.Average_kVA_Demand_last_12_months__c;
                pcso.Energy_Consumption_kWh_Baseline__c = baselineBilling.Average_Total_Volume_last_12_months__c;                
                pcsoInsertList.add(pcso);
            }                                  
        }
        return pcsoInsertList;
    }    
    
    public void ValidatePCSOS(List<Service__c> serviceList,String listType){
        List<PCSO__c> invalidPCSOList = new List<PCSO__c>();
        List<PCSO__c> validPCSOList = new List<PCSO__c>();
        List<PCSO_Snapshot__c> pcsoSnapshotList = new List<PCSO_Snapshot__c>();
        if(listType == 'Zero Metering'){ //This validates that the PCSO of the type Idle/Salvage is still valid
            for(Service__c service : serviceList){
                integer counter = 0;
                if(service.Utility_Billings__r.size() > 0){
                    if((service.Utility_Billings__r.get(0)).metered_demand_kw__c > 0 && (service.Utility_Billings__r.get(0)).Invalid_Billing__c == false){ //If in the last billing of the Service, the metered demand is greater than 0, the PCSO is not longer valid
                        counter++;
                    }
                }
                for(PCSO__c pcso : service.pcsos__r){
                    if(pcso.PCSO_type__c == 'Idle/Salvage' && pcso.PCSO_Status__c != 'Implemented' && pcso.PCSO_Status__c != 'Audited'){ 
                        if(counter > 0){
                            pcso.System_Review__c = 'Invalid';
                            pcso.Months_Valid__c = 0;
							pcso.Buydown_Cost__c = 0;
							pcso.Cost_Saving__c = 0;                              
                            invalidPCSOList.add(pcso);
                            break;
                        }
                        else{
                            if(pcso.Months_Valid__c != null){
                            	pcso.Months_Valid__c = pcso.Months_Valid__c + 1;    
                            }                            
                            validPCSOList.add(pcso); 					                           
                        }
                    }    
                }
            }
        }
        else if(listType == 'Power Factor Review'){ //This validates that the PCSO of the type Power Factor is still valid
            for(Service__c service : serviceList){
                integer counter = 0;
                if(service.Utility_Billings__r.size() > 0){
                    if((service.Utility_Billings__r.get(0)).Metered_Demand_KVA__c > 0 && (service.Utility_Billings__r.get(0)).Invalid_Billing__c == false && (service.Utility_Billings__r.get(0).Metered_Demand_KW__c / (service.Utility_Billings__r.get(0)).Metered_Demand_KVA__c) > powerFactorReviewValueDouble){ //If in the last billing of the Service, the Power Factor is greate than the value specified in the custom setting, the PCSO is not longer valid
                        counter++;
                    }
                }              
                for(PCSO__c pcso : service.pcsos__r){
                    if(pcso.PCSO_type__c == 'Power Factor Review' && pcso.PCSO_Status__c != 'Implemented' && pcso.PCSO_Status__c != 'Audited'){ 
                        if(counter > 0){
                            pcso.System_Review__c = 'Invalid';
                            pcso.Months_Valid__c = 0;
							pcso.Buydown_Cost__c = 0;
							pcso.Cost_Saving__c = 0;                              
                            invalidPCSOList.add(pcso);
                            break;
                        }
                        else{
                            if(pcso.Months_Valid__c != null){
                            	pcso.Months_Valid__c = pcso.Months_Valid__c + 1;    
                            }                            
                            validPCSOList.add(pcso);                                                     
                        }
                    }    
                }
            }            
        }
        else{ //This validates that the PCSO of the type Review of Contract Minimum is still valid
            for(Service__c service : serviceList){
                integer counter = 0;
                if(service.Utility_Billings__r.size() > 0){
                    if((service.Utility_Billings__r.get(0)).Metered_Demand_KW__c >= (service.Utility_Billings__r.get(0)).Billing_Demand_KW__c && (service.Utility_Billings__r.get(0)).Invalid_Billing__c == false){ //If in the last billing of the Service, the metered demand is greater or equal to the billing demand, the PCSO is not longer valid
                        counter++;
                    }
                }                
                for(PCSO__c pcso : service.pcsos__r){
                    if(pcso.PCSO_type__c == 'Review Contract Minimum' && pcso.PCSO_Status__c != 'Implemented' && pcso.PCSO_Status__c != 'Audited'){
                        if(counter > 0){
                            pcso.System_Review__c = 'Invalid';
                            pcso.Months_Valid__c = 0;
							pcso.Buydown_Cost__c = 0;
							pcso.Cost_Saving__c = 0;                            
                            invalidPCSOList.add(pcso);
                            break;
                        }
                        else{
                            if(pcso.Months_Valid__c != null){
                            	pcso.Months_Valid__c = pcso.Months_Valid__c + 1;    
                            }                            
                            validPCSOList.add(pcso);                                                         
                        }
                    }    
                }
            }              
        }
        
        if(invalidPCSOList.size() > 0){
            //The following lines of code is to remove duplicate and update the invalid pcsos
            Set <PCSO__c> finalPCSOSet = new Set<PCSO__c>();
            List <PCSO__c> finalInvalidPCSOList = new List<PCSO__c>();
            finalPCSOSet.addAll(invalidPCSOList);
            finalInvalidPCSOList.addAll(finalPCSOSet);
            update finalInvalidPCSOList;    
        }
        
        if(validPCSOList.size() > 0){
            //The following lines of code is to remove duplicate and update the valid pcsos
            Set <PCSO__c> finalPCSOSet = new Set<PCSO__c>();
            List <PCSO__c> finalValidPCSOList = new List<PCSO__c>();
            finalPCSOSet.addAll(validPCSOList);
            finalValidPCSOList.addAll(finalPCSOSet);
            update finalValidPCSOList;
        }        
        
    }
    
}