/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Implementation of HOG_VG_AlertProvider for purpose of creating High Variance
                Alerts.
Test Class:     HOG_VG_HighVarianceAlertProviderTest
History:        jschn 19/10/2018 - Created. - W-001276, W-001305
*************************************************************************************************/
public with sharing class HOG_VG_HighVarianceAlertProvider extends HOG_VG_AlertProvider {

    /**
     * Implementation that creates instance of High Variance alert for insertion.
     *
     * @param location
     *
     * @return
     */
    public override HOG_Vent_Gas_Alert__c createAlert(Location__c location) {
        return super.prepopulateGeneralData(new HOG_Vent_Gas_Alert__c(
                Name = location.Name + ' - '
                        + HOG_VentGas_Utilities.ALERT_TYPE_ABV_HIGH_VARIANCE + ' - '
                        + String.valueOf(Date.today()),
                Priority__c = HOG_VentGas_Utilities.ALERT_PRIORITY_HIGH,
                Type__c = HOG_VentGas_Utilities.ALERT_TYPE_HIGH_VARIANCE
        ), location);
    }

}