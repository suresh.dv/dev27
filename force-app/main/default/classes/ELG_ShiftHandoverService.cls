/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Service class containing Business Logic for Shift Handover part of E-Log Application
Test Class:     ELG_ShiftHandoverServiceTest
History:        mbrimus 2019-05-20 - Created.
*************************************************************************************************/
public inherited sharing class ELG_ShiftHandoverService {
	private static final String CLASS_NAME = String.valueOf(ELG_ShiftHandoverService.class);

	public HOG_CustomResponseImpl getBaseShiftHandoverData(){
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		List<ELG_User_Setting__c> settings = ELG_DAOProvider.shiftHandoverDao.getUsersSettings();

		Map<String, Object> resultsMap = new Map<String, Object>();
		resultsMap.put(ELG_Constants.USER_SETTINGS_KEY, null);
		resultsMap.put(ELG_Constants.AVAILABLE_POSTS_KEY, new List<ELG_Post__c>());
		resultsMap.put(ELG_Constants.IS_SHIFTENGINEER_KEY, ELG_Utilities.hasEditAccessForReviewShiftEngineer());
		resultsMap.put(ELG_Constants.PRIMARYONSHIFT_KEY, null);

		if(settings != null && settings.size() == 1){
			resultsMap.put(ELG_Constants.USER_SETTINGS_KEY, settings.get(0));
			resultsMap.put(ELG_Constants.AVAILABLE_POSTS_KEY,
					ELG_DAOProvider.shiftHandoverDao.getAvailablePostsForAMU(settings.get(0).Operating_Field_AMU__c));
			// Info about users current shift
			List<ELG_Shift_Assignement__c> assignment = ELG_DAOProvider.shiftHandoverDao.getCurrentShiftAssignmentForUser(settings.get(0).Operating_Field_AMU__c);
			if(assignment.size() > 0){
				resultsMap.put(ELG_Constants.PRIMARYONSHIFT_KEY, assignment.get(0));
			}
		}
		response.addResult(resultsMap);
		return response;
	}

	public HOG_CustomResponseImpl getAllLogsForActiveShiftAndPost(String shiftId, String facilityId){
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<ELG_Log_Entry__c> logEntries = ELG_DAOProvider.shiftHandoverDao.getAllLogsForActiveShiftAndPost(shiftId, facilityId);
		response.addResult(logEntries);
		return response;
	}

	public HOG_CustomResponseImpl getHandoverDocumentByShift(String shiftId){
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		ELG_Shift_Handover__c handover = ELG_DAOProvider.shiftHandoverDao.getHandoverByShift(shiftId);
		response.addResult(handover);
		return response;
	}

	public HOG_CustomResponseImpl getHandoverForTakePost(){
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();

		List<ELG_User_Setting__c> settings = ELG_DAOProvider.shiftHandoverDao.getUsersSettings();
		if(settings != null && settings.size() == 1){
			String amuId = settings.get(0).Operating_Field_AMU__c;
			List<ELG_Shift_Handover__c> handover =
					ELG_DAOProvider.shiftHandoverDao.getHandoverForTakePost(amuId);
			response.addResult(handover);
		} else {
			response.addError('Setting for this user has not been found');
		}
		return response;
	}

	public HOG_CustomResponseImpl getAllQuestionCategories(){
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<ELG_Handover_Category__c> questions = ELG_DAOProvider.shiftHandoverDao.getAllQuestionCategories();
		response.addResult(questions);
		return response;
	}

	public List<ELG_Handover_Category_Question__c> getAllActiveQuestions(){
		return ELG_DAOProvider.shiftHandoverDao.getAllActiveQuestions();
	}

	public HOG_CustomResponseImpl getShiftWithMissingShiftEngineer(String amuId){
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<ELG_Shift_Assignement__c> steamShiftWithMissingEngineer = ELG_DAOProvider.shiftHandoverDao.getSteamShiftWithMissingShiftEngineer(amuId);
		if(steamShiftWithMissingEngineer.size() > 0){
			response.addResult(steamShiftWithMissingEngineer);
			response.addResult(steamShiftWithMissingEngineer.size());
		}
		return response;
	}

	public HOG_CustomResponseImpl updateShiftEngineerOnMorePosts(List<String> shiftIds, String currentUser) {
		HOG_CustomResponseImpl response = new HOG_CustomResponseImpl();
		List<ELG_Shift_Assignement__c> shifts = new List<ELG_Shift_Assignement__c>();

		for (String shiftId : shiftIds) {
			shifts.add(new ELG_Shift_Assignement__c(Id = shiftId, Shift_Engineer__c = currentUser));
		}

		update shifts;
		response.addResult(shifts);
		return response;
	}

//    public HOG_DMLResponseImpl acceptHandover(ELG_Shift_Handover__c handover){
//        return new HOG_DMLResponseImpl(ELG_HandoverForm_Service.acceptHandoverAndChangeStatus(handover));
//    }
}