/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Database queries for Vendor Portal Management
History:        mbrim 2019-28-11 - Created.
*************************************************************************************************/
public inherited sharing class VTT_LTNG_PortalManagementSelector implements VTT_LTNG_PortalManagementDAO {

    public List<Account> getAccounts() {
        return [
                SELECT Id,
                        Name,
                        Remaining_Licenses__c,
                        Total_Licenses__c,
                        Used_Licenses__c,
                        BillingAddress,
                        BillingStreet,
                        BillingCity,
                        BillingState,
                        BillingPostalCode,
                        BillingCountry
                FROM Account
                WHERE IsPartner = TRUE
                ORDER BY Name ASC
                LIMIT 10000
        ];
    }

    public List<Contact> getUsersContact(Id userId) {
        return [
                SELECT Id,
                        AccountId,
                        Account.Name,
                        Account.Id,
                        Account.Total_Licenses__c,
                        Account.Remaining_Licenses__c,
                        Account.Used_Licenses__c
                FROM Contact
                WHERE User__c = :userId
                AND Account.IsPartner = TRUE
                LIMIT 10000
        ];
    }

    public List<User> getUserById(Id userId) {
        return [
                SELECT Id,
                        FirstName,
                        LastName,
                        Vendor_Portal_User_Type__c,
                        Username,
                        CommunityNickname
                FROM User
                WHERE Id = :userId
                AND Vendor_Portal_User_Type__c <> NULL
                LIMIT 10000
        ];
    }

    public List<Contact> getContactsForAccount(Id accountId, Boolean isEnabled) {
        if (isEnabled) {
            return [
                    SELECT
                            Id,
                            Name,
                            FirstName,
                            LastName,
                            is_Vendor_Portal_User__c,
                            Email,
                            User_Name__c,
                            Vendor_Portal_User_Role__c,
                            AccountId,
                            Account.Name,
                            User__r.Vendor_Portal_User_Type__c
                    FROM Contact
                    WHERE AccountId = :accountId
                    AND RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact')
                    AND is_Vendor_Portal_User__c = :isEnabled
                    AND User__c <> NULL
                    ORDER BY Name
                    LIMIT 10000
            ];
        } else {
            return [
                    SELECT
                            Id,
                            Name,
                            FirstName,
                            LastName,
                            is_Vendor_Portal_User__c,
                            Email,
                            Vendor_Portal_User_Role__c,
                            AccountId
                    FROM Contact
                    WHERE AccountId = :accountId
                    AND RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'HOG_Vendor_Contact')
                    AND is_Vendor_Portal_User__c = :isEnabled
                    ORDER BY Name
                    LIMIT 10000
            ];
        }

    }

    public Account getAccountById(String accountId) {
        Account ac = new Account();
        List<Account> accounts = [
                SELECT Id,
                        Name,
                        Remaining_Licenses__c,
                        Total_Licenses__c,
                        Used_Licenses__c,
                        BillingAddress,
                        BillingStreet,
                        BillingCity,
                        BillingState,
                        BillingPostalCode,
                        BillingCountry
                FROM Account
                WHERE IsPartner = TRUE
                AND Id = :accountId
                ORDER BY Name ASC
                LIMIT 1
        ];
        if (accounts.size() > 0) {
            ac = accounts.get(0);
        }
        return ac;
    }
}