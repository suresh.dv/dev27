/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Form structure class for Part Replacement SObject
Test Class:     EPD_FormStructurePartTest
History:        jschn 2019-06-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructurePart extends EPD_FormStructureBase {

    private static final String CLASS_NAME = String.valueOf(EPD_FormStructurePart.class);

    @AuraEnabled
    public Id recordId;

    @AuraEnabled
    public Id epdId;

    @AuraEnabled
    public String partName {get;set;}

    @AuraEnabled
    public Boolean replaced {get;set;}

    @AuraEnabled
    public Boolean inspected {get;set;}

    @AuraEnabled
    public String comments {get;set;}

    /**
     * This method maps local values into Part Replacement SObject.
     *
     * @return SObject (EPD_Part_Replacement__c)
     */
    public override SObject getRecord() {
        System.debug(CLASS_NAME + ' -> getRecord. Part Structure: ' + JSON.serialize(this));

        return new EPD_Part_Replacement__c(
                Id = recordId,
                Engine_Performance_Data__c = epdId,
                Name = partName,
                Replaced__c = replaced,
                Inspected__c = inspected,
                Comments__c = comments
        );
    }

    /**
     * Maps Part Replacement record into local variables.
     *
     * @param obj
     *
     * @return EPD_FormStructureBase
     */
    public override EPD_FormStructureBase setRecord(SObject obj) {
        System.debug(CLASS_NAME + ' -> setRecord. START. Object param: ' + JSON.serialize(obj));

        EPD_Part_Replacement__c partReplacement = (EPD_Part_Replacement__c) obj;
        recordId = partReplacement.Id;
        epdId = partReplacement.Engine_Performance_Data__c;
        partName = partReplacement.Name;
        replaced = partReplacement.Replaced__c;
        inspected = partReplacement.Inspected__c;
        comments = partReplacement.Comments__c;

        System.debug(CLASS_NAME + ' -> setRecord. END. Part Structure: ' + JSON.serialize(this));
        return this;
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuildingString = '';
        contentBuildingString += '"recordId":' + (String.isNotBlank(recordId) ? ('"' + recordId + '"') : 'null') + ',';
        contentBuildingString += '"epdId":' + (String.isNotBlank(epdId) ? ('"' + epdId + '"') : 'null') + ',';
        contentBuildingString += '"partName":' + (String.isNotBlank(partName) ? ('"' + partName + '"') : 'null') + ',';
        contentBuildingString += '"replaced":' + replaced + ',';
        contentBuildingString += '"inspected":' + inspected + ',';
        contentBuildingString += '"comments":' + (String.isNotBlank(comments) ? ('"' + comments + '"') : 'null');
        return '{' + contentBuildingString + '}';
    }

}