/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Utilities for SNR Service - used to build proper response
Test Class:     TODO
History:        mbrimus 2020-01-14 - Created.
*************************************************************************************************/
public inherited sharing class SNR_Utilities {
    private LightningNotificationRecordType notificationRecordType = new LightningNotificationRecordType();

    public SNR_FLOCModel getModel(String recordId, String objectType, HOG_Notification_Type__c notificationType) {

        SNR_FLOCModel model = new SNR_FLOCModel();

        if (notificationRecordType.OBJECTNAMETORECORDTYPEMAP.containsKey(objectType)) {
            String recordTypeName = notificationRecordType.OBJECTNAMETORECORDTYPEMAP.get(objectType);

            model.setFunctionalLocationObjectLabel(notificationRecordType.MAPOBJECTLABEL.get(recordTypeName))
                    .setShowProductionImpacted(notificationRecordType.MAPSHOWPRODUCTIONIMPACTED.get(recordTypeName))
                    .setShowEquipmentDown(notificationRecordType.MAPSHOWEQUIPMENTDOWN.get(recordTypeName))
                    .setShowEquipmentInfo(notificationRecordType.MAPSHOWPEQUIPMENTINFO.get(recordTypeName))
                    .setShowTrackerInfo(notificationRecordType.MAPSHOWTRACKERINFO.get(recordTypeName))
                    .setShowPVRAvgInfo(notificationRecordType.MAPSHOWPVRAVGINFO.get(recordTypeName))
                    .setServiceSupervised(true)
                    .setServiceNonRecurring(true)
                    .setServiceVendorCompanyIsNotRequired(true)
                    .setVendorInvoicingPersonnelIsRequired(false)
                    .setServiceTimeIsRequired(false)
                    .setGenerateRigProgramRecord(notificationType.Generate_Rig_Program_Record__c)
                    .setGenerateWorkOrderRecord(notificationType.Generate_Work_Order_Record__c)
                    .setGenerateWorkOrderNumber(notificationType.Auto_Generate_Work_Order_Number__c)
                    .setGenerateNotificationNumber(notificationType.Auto_generate_Notification_Number__c)
                    .setGenerateNumbersFromSAP(notificationType.Generate_Numbers_From_SAP__c)
                    .setManualInputWorkOrderNumber(notificationType.Allow_Manual_Update_Of_Work_Order_Number__c && !notificationType.Auto_Generate_Work_Order_Number__c)
                    .setAllowWorkOrderLinking(notificationType.Allow_Work_Order_Linking__c)
                    .setServiceEventVisible(notificationType.Allow_Event_Selection__c)
                    .setNotificationTypeCode(notificationType.Notification_Type__c)
                    .setNotificationOrderTypeCode(notificationType.Order_Type__c)
                    .setNotificationUserStatusCode(notificationType.HOG_User_Status__r.Code__c)
                    .setMainWorkCentre(notificationType.Main_Work_Centre__c)
                    .setWellEventSelected(false);

            updateModelWithFLOCData(recordTypeName, model, recordId);
        } else {
            // TODO what to do if invalid API is passed?
        }

        return model;
    }

    public SNR_FLOCModel getModelWithServiceRequired(
            String recordId,
            String recordTypeName,
            HOG_Service_Required__c serviceRequired,
            HOG_Notification_Type__c notificationType) {
        SNR_FLOCModel model = getModel(recordId, recordTypeName, notificationType);
        updateModelWithFLOCData(recordTypeName, model, recordId);
        updateModelWithServiceRequiredData(model, serviceRequired);
        return model;
    }

    private void updateModelWithServiceRequiredData(SNR_FLOCModel model, HOG_Service_Required__c serviceRequired) {

        if (serviceRequired != null) {
            model.setServiceSupervised(serviceRequired.Supervised__c)
                    .setServiceNonRecurring(serviceRequired.Non_Recurring__c)
                    .setServiceVendorCompanyIsNotRequired(serviceRequired.Vendor_Company_Is_Not_Required__c)
                    .setNotificationCodeGroup(serviceRequired.HOG_Service_Code_Group__r.Code_Group__c)
                    .setVendorInvoicingPersonnelIsRequired(serviceRequired.Vendor_Invoicing_Personnel_Is_Required__c)
                    .setServiceTimeIsRequired(serviceRequired.Service_Time_Is_Required__c);
        } else {
            // Defaults?? See ServiceRequestNotification getServiceSpecificsNames
            model.setNotificationCodeGroup('')
                    .setServiceSupervised(true)
                    .setServiceNonRecurring(true)
                    .setServiceVendorCompanyIsNotRequired(true)
                    .setVendorInvoicingPersonnelIsRequired(false)
                    .setServiceTimeIsRequired(false);
        }
    }

    /**
     * Decision logic for populating Model data based on given objectType
     *
     * @param objectType
     * @param model
     * @param recordId
     */
    private void updateModelWithFLOCData(String objectType, SNR_FLOCModel model, String recordId) {
        if (objectType == notificationRecordType.WELLTRACKER ||
                objectType == notificationRecordType.FACILITYTRACKER) {
            // TODO - not supported yet
        } else if (objectType == notificationRecordType.WELL) {
            executeWell(model, recordId);
        } else if (objectType == notificationRecordType.FACILITY) {
            executeFacility(model, recordId);
        } else if (objectType == notificationRecordType.EQUIPMENT) {
            executeEquipment(model, recordId);
        } else if (objectType == notificationRecordType.WELLEVENT) {
            executeWellEvent(model, recordId);
        } else if (objectType == notificationRecordType.MAINSYSTEM) {
            executeSystem(model, recordId);
        } else if (objectType == notificationRecordType.SUBSYSTEM) {
            executeSubSystem(model, recordId);
        } else if (objectType == notificationRecordType.YARD) {
            executeYard(model, recordId);
        } else if (objectType == notificationRecordType.FUNCTIONALEQUIPMENT) {
            executeFEL(model, recordId);
        }
    }
    private void executeFEL(SNR_FLOCModel model, String recordId) {
        List<Functional_Equipment_Level__c> felRecord = SNR_DAOProvider.snrDao.getFEL(recordId);
        if (felRecord.size() > 0) {
            model.setFunctionalLocationRecordId(felRecord[0].Id)
                    .setWellOilProdM3DayVisible(false)
                    .setRecentWellTrackerRecord(true)
                    .setRecentWellTrackerRecord(true)
                    .setRecordIsDLFL(felRecord[0].DLFL__c)
                    .setFunctionalLocationRecordName(felRecord[0].Name)
                    .setFunctionalLocationObjectId(felRecord[0].SAP_Object_ID__c)
                    .setFunctionalLocationSurface(felRecord[0].Surface_Location__c)
                    .setMaintenancePlanningPlant(felRecord[0].Maintenance_Plant__c)
                    .setOperatingBusiness(felRecord[0].Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                    .setOperatingDistrict(felRecord[0].Operating_Field_AMU__r.Operating_District__c)
                    .setOperatingFacility(felRecord[0].Facility__c)
                    .setOperatingRoute(felRecord[0].Route__c)
                    .setOperatingField(felRecord[0].Operating_Field_AMU__c)
                    .setOperatingFel(felRecord[0].Id);
        }
    }

    private void executeYard(SNR_FLOCModel model, String recordId) {
        List<Yard__c> yard = SNR_DAOProvider.snrDao.getOneYard(recordId);
        if (yard.size() > 0) {
            model.setFunctionalLocationRecordId(yard[0].Id)
                    .setWellOilProdM3DayVisible(false)
                    .setRecentWellTrackerRecord(true)
                    .setRecentWellTrackerRecord(true)
                    .setRecordIsDLFL(yard[0].DLFL__c)
                    .setFunctionalLocationRecordName(yard[0].Name)
                    .setFunctionalLocationObjectId(yard[0].SAP_Object_ID__c)
                    .setFunctionalLocationSurface(yard[0].Surface_Location__c)
                    .setMaintenancePlanningPlant(yard[0].Maintenance_Plant__c)
                    .setOperatingBusiness(yard[0].Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                    .setOperatingDistrict(yard[0].Operating_Field_AMU__r.Operating_District__c)
                    .setOperatingFacility(yard[0].Facility__c)
                    .setOperatingRoute(yard[0].Route__c)
                    .setOperatingField(yard[0].Operating_Field_AMU__c)
                    .setOperatingYard(yard[0].Id);
        }
    }

    private void executeSubSystem(SNR_FLOCModel model, String recordId) {
        List<Sub_System__c> subSystem = SNR_DAOProvider.snrDao.getOneSubSystem(recordId);
        if (subSystem.size() > 0) {
            model.setFunctionalLocationRecordId(subSystem[0].Id)
                    .setWellOilProdM3DayVisible(false)
                    .setRecentWellTrackerRecord(true)
                    .setFunctionalLocationRecordName(subSystem[0].Name)
                    .setFunctionalLocationObjectId(subSystem[0].SAP_Object_ID__c)
                    .setFunctionalLocationSurface(subSystem[0].Surface_Location__c)
                    .setMaintenancePlanningPlant(subSystem[0].Maintenance_Plant__c)
                    .setOperatingRoute(subSystem[0].Route__c)
                    .setOperatingSubSystem(subSystem[0].Id);

            // Based on is Sub System is under System or Well Location fill out BU and AMU
            if (subSystem[0].Well_ID__c != null) {
                model.setOperatingBusiness(subSystem[0].Well_ID__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                        .setOperatingDistrict(subSystem[0].Well_ID__r.Operating_Field_AMU__r.Operating_District__c)
                        .setOperatingField(subSystem[0].Well_ID__r.Operating_Field_AMU__c);
            } else if (subSystem[0].System__c != null) {
                model.setOperatingBusiness(subSystem[0].System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                        .setOperatingDistrict(subSystem[0].System__r.Operating_Field_AMU__r.Operating_District__c)
                        .setOperatingField(subSystem[0].System__r.Operating_Field_AMU__c);
            } else if (subSystem[0].Facility__c != null) {
                model.setOperatingBusiness(subSystem[0].Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                        .setOperatingDistrict(subSystem[0].Facility__r.Operating_Field_AMU__r.Operating_District__c)
                        .setOperatingField(subSystem[0].Facility__r.Operating_Field_AMU__c);
            }
        }
    }

    private void executeSystem(SNR_FLOCModel model, String recordId) {
        List<System__c> systemRecord = SNR_DAOProvider.snrDao.getOneSystem(recordId);
        if (systemRecord.size() > 0) {
            model.setFunctionalLocationRecordId(systemRecord[0].Id)
                    .setWellOilProdM3DayVisible(false)
                    .setRecentWellTrackerRecord(true)
                    .setRecordIsDLFL(systemRecord[0].DLFL__c)
                    .setFunctionalLocationRecordName(systemRecord[0].Name)
                    .setFunctionalLocationObjectId(systemRecord[0].SAP_Object_ID__c)
                    .setFunctionalLocationSurface(systemRecord[0].Surface_Location__c)
                    .setMaintenancePlanningPlant(systemRecord[0].Maintenance_Plant__c)
                    .setOperatingBusiness(systemRecord[0].Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                    .setOperatingDistrict(systemRecord[0].Operating_Field_AMU__r.Operating_District__c)
                    .setOperatingRoute(systemRecord[0].Route__c)
                    .setOperatingField(systemRecord[0].Operating_Field_AMU__c)
                    .setOperatingSystem(systemRecord[0].Id);
        }
    }

    private void executeWellEvent(SNR_FLOCModel model, String recordId) {
        List<Well_Event__c> event = SNR_DAOProvider.snrDao.getOneWellEvent(recordId);
        if (event.size() > 0) {
            model.setWellLocationStatus(event[0].Current_Well_Status__c)
                    .setFunctionalLocationRecordId(event[0].Id)
                    .setWellOilProdM3DayVisible(false)
                    .setRecentWellTrackerRecord(true)
                    .setRecordIsDLFL(event[0].DLFL__c)
                    .setFunctionalLocationRecordName(event[0].Name)
                    .setFunctionalLocationObjectId(event[0].SAP_Object_ID__c)
                    .setFunctionalLocationType(event[0].Well_Type__c)
                    .setFunctionalLocationSurface(event[0].Surface_Location__c)
                    .setFunctionalLocationStatus(event[0].Status__c)
                    .setMaintenancePlanningPlant(event[0].Well_ID__r.Maintenance_Plant__c)
                    .setOperatingBusiness(event[0].Well_ID__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                    .setOperatingDistrict(event[0].Well_ID__r.Operating_Field_AMU__r.Operating_District__c)
                    .setOperatingFacility(event[0].Well_ID__r.Facility__c)
                    .setOperatingRoute(event[0].Well_ID__r.Route__c)
                    .setOperatingField(event[0].Well_ID__r.Operating_Field_AMU__c)
                    .setOperatingLocation(event[0].Well_ID__c)
                    .setOperatingWellEvent(event[0].Id)

                    .setAvgVol5DOil(event[0].PVR_AVGVOL_5D_OIL__c)
                    .setAvgVol5DWater(event[0].PVR_AVGVOL_5D_WATER__c)
                    .setAvgVol5DSand(event[0].PVR_AVGVOL_5D_SAND__c)
                    .setAvgVol5DGas(event[0].PVR_AVGVOL_5D_GAS__c)
                    .setAvgVol5DCond(event[0].PVR_AVGVOL_5D_COND__c)

                    .setPvrAvgVol5DNoOfMeasures(event[0].PVR_AVGVOL_5D_NO_OF_MEASURES__c != null ? event[0].PVR_AVGVOL_5D_NO_OF_MEASURES__c : 0)
                    .setPvrAvgVolUpdated(event[0].PVR_AVGVOL_UPDATED__c)
                    .setPvrAvgVol5DLastMeasured(event[0].PVR_AVGVOL_5D_LAST_MEASURED__c);
        }
    }

    private void executeEquipment(SNR_FLOCModel model, String recordId) {
        List<Equipment__c> equipment = SNR_DAOProvider.snrDao.getOneEquipment(recordId);

        if (equipment.size() > 0) {
            model.setRecentWellTrackerRecord(true)
                    .setWellOilProdM3DayVisible(false)
                    .setFunctionalLocationRecordId(equipment[0].Id)
                    .setOperatingEquipment(equipment[0].Id)
                    .setRecordIsDLFL(equipment[0].DLFL__c)
                    .setFunctionalLocationRecordName(equipment[0].Equipment_Number__c)
                    .setEquipmentNumber(equipment[0].Equipment_Number__c)
                    .setFunctionalLocationStatus(equipment[0].Status__c)
                    .setMaintenancePlanningPlant(equipment[0].Maintenance_Plant__c)
                    .setEquipmentManufacturer(equipment[0].Manufacturer__c)
                    .setEquipmentManufacturerSerialNo(equipment[0].Manufacturer_Serial_No__c)
                    .setEquipmentCategory(equipment[0].Equipment_Category__c)
                    .setEquipmentCatalogueCode(equipment[0].Catalogue_Code__c)
                    .setShowPVRAvgInfo(false);

            if (equipment[0].Location__c != null) {
                handleEquipmentOnWellLocation(model, equipment[0]);
            } else if (equipment[0].Facility__c != null) {
                handleEquipmentOnFacility(model, equipment[0]);
            } else if (equipment[0].System__c != null) {
                handleEquipmentOnSystem(model, equipment[0]);
            } else if (equipment[0].Sub_System__c != null) {
                handleEquipmentOnSubSystem(model, equipment[0]);
            } else if (equipment[0].Functional_Equipment_Level__c != null) {
                handleEquipmentOnFEL(model, equipment[0]);
            } else if (equipment[0].Yard__c != null) {
                handleEquipmentOnYard(model, equipment[0]);
            }
        }
    }

    /**
     * Populates FLOC Model based on data from Facility
     *
     * @param model
     * @param recordId
     *
     */
    private void executeFacility(SNR_FLOCModel model, String recordId) {
        List<Facility__c> facility = SNR_DAOProvider.snrDao.getOneFacility(recordId);
        if (facility.size() > 0) {
            model.setWellLocationStatus(facility[0].Current_Facility_Status__c)
                    .setRecentWellTrackerRecord(true)
                    .setRecordIsDLFL(facility[0].DLFL__c)
                    .setFunctionalLocationRecordName(facility[0].Name)
                    .setFunctionalLocationObjectId(facility[0].SAP_Object_ID__c)
                    .setFunctionalLocationType(facility[0].Facility_Type__c)
                    .setFunctionalLocationSurface(facility[0].Surface_Location__c)
                    .setFunctionalLocationStatus(facility[0].Status__c)
                    .setMaintenancePlanningPlant(facility[0].Maintenance_Plant__c)
                    .setOperatingBusiness(facility[0].Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                    .setOperatingDistrict(facility[0].Operating_Field_AMU__r.Operating_District__c)
                    .setOperatingRoute(facility[0].Plant_Section__c)
                    .setOperatingField(facility[0].Operating_Field_AMU__c)
                    .setOperatingFacility(facility[0].Id);
        }
    }

    /**
     * Populates FLOC Model based on data from Well location
     *
     * @param model
     * @param recordId
     *
     * @return
     */
    private void executeWell(SNR_FLOCModel model, String recordId) {
        List<Location__c> wellRecord = SNR_DAOProvider.snrDao.getOneWellLocation(recordId);
        if (wellRecord.size() > 0) {
            model.setWellOilProdM3DayVisible(false)
                    .setWellLocationStatus(wellRecord[0].Current_Well_Status__c)
                    .setRecentWellTrackerRecord(true)
                    .setRecordIsDLFL(wellRecord[0].DLFL__c)
                    .setFunctionalLocationRecordName(wellRecord[0].Name)
                    .setFunctionalLocationObjectId(wellRecord[0].SAP_Object_ID__c)
                    .setFunctionalLocationType(wellRecord[0].Well_Type__c)
                    .setFunctionalLocationSurface(wellRecord[0].Surface_Location__c)
                    .setFunctionalLocationStatus(wellRecord[0].Status__c)
                    .setMaintenancePlanningPlant(wellRecord[0].Maintenance_Plant__c)
                    .setOperatingBusiness(wellRecord[0].Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                    .setOperatingDistrict(wellRecord[0].Operating_Field_AMU__r.Operating_District__c)
                    .setOperatingFacility(wellRecord[0].Facility__c)
                    .setOperatingRoute(wellRecord[0].Route__c)
                    .setOperatingField(wellRecord[0].Operating_Field_AMU__c)
                    .setOperatingLocation(wellRecord[0].Id)

                    .setAvgVol5DOil(wellRecord[0].PVR_AVGVOL_5D_OIL__c)
                    .setAvgVol5DWater(wellRecord[0].PVR_AVGVOL_5D_WATER__c)
                    .setAvgVol5DSand(wellRecord[0].PVR_AVGVOL_5D_SAND__c)
                    .setAvgVol5DGas(wellRecord[0].PVR_AVGVOL_5D_GAS__c)
                    .setAvgVol5DCond(wellRecord[0].PVR_AVGVOL_5D_COND__c)

                    .setPvrAvgVol5DNoOfMeasures(wellRecord[0].PVR_AVGVOL_5D_NO_OF_MEASURES__c != null ? wellRecord[0].PVR_AVGVOL_5D_NO_OF_MEASURES__c : 0)
                    .setPvrAvgVolUpdated(wellRecord[0].PVR_AVGVOL_UPDATED__c)
                    .setPvrAvgVol5DLastMeasured(wellRecord[0].PVR_AVGVOL_5D_LAST_MEASURED__c);
            // TODO not here
//            serviceCompletedAndRunning = false;
//            serviceRequestFormRecordIsCompleted = !String.IsEmpty(serviceRequestForm.Work_Order_Number__c);
        }
    }

    private void handleEquipmentOnYard(SNR_FLOCModel model, Equipment__c equipment) {
        model.setFunctionalLocationObjectId(equipment.Yard__r.SAP_Object_ID__c)
                .setFunctionalLocationRecordName(equipment.Name + ' - ' + equipment.Yard__r.Name)
                .setOperatingBusiness(equipment.Yard__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                .setOperatingDistrict(equipment.Yard__r.Operating_Field_AMU__r.Operating_District__c)
                .setOperatingRoute(equipment.Yard__r.Route__c)
                .setOperatingField(equipment.Yard__r.Operating_Field_AMU__c)
                .setOperatingFacility(equipment.Yard__r.Facility__c)
                .setOperatingLocation(equipment.Yard__r.Well_ID__c)
                .setOperatingYard(equipment.Yard__c);
    }

    private void handleEquipmentOnFEL(SNR_FLOCModel model, Equipment__c equipment) {
        model.setFunctionalLocationObjectId(equipment.Functional_Equipment_Level__r.SAP_Object_ID__c)
                .setFunctionalLocationRecordName(equipment.Name + ' - ' + equipment.Functional_Equipment_Level__r.Name)
                .setOperatingBusiness(equipment.Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                .setOperatingDistrict(equipment.Functional_Equipment_Level__r.Operating_Field_AMU__r.Operating_District__c)
                .setOperatingRoute(equipment.Functional_Equipment_Level__r.Route__c)
                .setOperatingField(equipment.Functional_Equipment_Level__r.Operating_Field_AMU__c)
                .setOperatingFel(equipment.Functional_Equipment_Level__c)
                .setOperatingSystem(equipment.Functional_Equipment_Level__r.System__c)
                .setOperatingSubSystem(equipment.Functional_Equipment_Level__r.Sub_System__c)
                .setOperatingLocation(equipment.Functional_Equipment_Level__r.Sub_System__r.Well_ID__c)
                .setOperatingFacility(equipment.Functional_Equipment_Level__r.Facility__c);
    }

    private void handleEquipmentOnSubSystem(SNR_FLOCModel model, Equipment__c equipment) {
        model.setFunctionalLocationObjectId(equipment.Sub_System__r.SAP_Object_ID__c)
                .setFunctionalLocationRecordName(equipment.Name + ' - ' + equipment.Sub_System__r.Name)
                .setOperatingBusiness(equipment.Sub_System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                .setOperatingDistrict(equipment.Sub_System__r.Operating_Field_AMU__r.Operating_District__c)
                .setOperatingRoute(equipment.Sub_System__r.Route__c)
                .setOperatingField(equipment.Sub_System__r.Operating_Field_AMU__c)
                .setOperatingSubSystem(equipment.Sub_System__c)
                .setOperatingSystem(equipment.Sub_System__r.System__c)
                .setOperatingLocation(equipment.Sub_System__r.Well_ID__c);
    }

    private void handleEquipmentOnSystem(SNR_FLOCModel model, Equipment__c equipment) {
        model.setFunctionalLocationObjectId(equipment.System__r.SAP_Object_ID__c)
                .setFunctionalLocationRecordName(equipment.Name + ' - ' + equipment.System__r.Name)
                .setOperatingBusiness(equipment.System__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                .setOperatingDistrict(equipment.System__r.Operating_Field_AMU__r.Operating_District__c)
                .setOperatingRoute(equipment.System__r.Route__c)
                .setOperatingField(equipment.System__r.Operating_Field_AMU__c)
                .setOperatingFacility(equipment.System__r.Facility__c)
                .setOperatingSystem(equipment.System__c);
    }

    private void handleEquipmentOnFacility(SNR_FLOCModel model, Equipment__c equipment) {
        model.setFunctionalLocationObjectId(equipment.Facility__r.SAP_Object_ID__c)
                .setFunctionalLocationRecordName(equipment.Name + ' - ' + equipment.Facility__r.Name)
                .setWellLocationStatus(equipment.Facility__r.Current_Facility_Status__c)
                .setOperatingBusiness(equipment.Facility__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                .setOperatingDistrict(equipment.Facility__r.Operating_Field_AMU__r.Operating_District__c)
                .setOperatingRoute(equipment.Facility__r.Plant_Section__c)
                .setOperatingField(equipment.Facility__r.Operating_Field_AMU__c)
                .setOperatingFacility(equipment.Facility__c);
    }

    private void handleEquipmentOnWellLocation(SNR_FLOCModel model, Equipment__c equipment) {
        model.setShowPVRAvgInfo(true)
                .setFunctionalLocationObjectId(equipment.Location__r.SAP_Object_ID__c)
                .setFunctionalLocationRecordName(equipment.Name + ' - ' + equipment.Location__r.Name)
                .setWellLocationStatus(equipment.Location__r.Current_Well_Status__c)
                .setOperatingBusiness(equipment.Location__r.Operating_Field_AMU__r.Operating_District__r.Business_Unit__c)
                .setOperatingDistrict(equipment.Location__r.Operating_Field_AMU__r.Operating_District__c)
                .setOperatingFacility(equipment.Location__r.Facility__c)
                .setOperatingRoute(equipment.Location__r.Route__c)
                .setOperatingField(equipment.Location__r.Operating_Field_AMU__c)
                .setOperatingLocation(equipment.Location__c)

                .setAvgVol5DOil(equipment.Location__r.PVR_AVGVOL_5D_OIL__c)
                .setAvgVol5DWater(equipment.Location__r.PVR_AVGVOL_5D_WATER__c)
                .setAvgVol5DSand(equipment.Location__r.PVR_AVGVOL_5D_SAND__c)
                .setAvgVol5DGas(equipment.Location__r.PVR_AVGVOL_5D_GAS__c)
                .setAvgVol5DCond(equipment.Location__r.PVR_AVGVOL_5D_COND__c)

                .setPvrAvgVol5DNoOfMeasures(equipment.Location__r.PVR_AVGVOL_5D_NO_OF_MEASURES__c != null ? equipment.Location__r.PVR_AVGVOL_5D_NO_OF_MEASURES__c : 0)
                .setPvrAvgVolUpdated(equipment.Location__r.PVR_AVGVOL_UPDATED__c)
                .setPvrAvgVol5DLastMeasured(equipment.Location__r.PVR_AVGVOL_5D_LAST_MEASURED__c);
    }

    public static Map<String, String> getServiceMATCodesMap(List<AggregateResult> serviceSpecifics) {
        Map<String, String> matCodes = new Map<String, String>();
        for (AggregateResult ar : serviceSpecifics) {
            matCodes.put((Id) ar.get('Id'), (String) ar.get('MAT_Code__c'));
        }
        return matCodes;
    }

    public static Map<String, String> getServicePriorityMap(List<HOG_Notification_Type_Priority__c> priorities) {
        Map<String, String> matCodes = new Map<String, String>();
        for (HOG_Notification_Type_Priority__c r : priorities) {
            matCodes.put(r.Id, r.HOG_Service_Priority__r.Priority_Code__c);
        }
        return matCodes;
    }

    public class LightningNotificationRecordType {
        // These are not in use atm
        public final String NOTIFICATIONOBJECTNAME = 'HOG_Service_Request_Notification_Form__c';
        public final String WELLTRACKER = String.valueOf(new Well_Tracker__c().getSObjectType());
        public final String FACILITYTRACKER = 'Facility Tracker';

        // Used.. this matches with recordtypes of SNR
        public final String WELL = 'Well ID';
        public final String FACILITY = 'Facility';
        public final String EQUIPMENT = 'Equipment';
        public final String MAINSYSTEM = 'System';
        public final String SUBSYSTEM = 'Sub-system';
        public final String YARD = 'Yard';
        public final String FUNCTIONALEQUIPMENT = 'Functional Equipment Level';
        public final String WELLEVENT = 'Well Event';
        public final String OPERATINGFIELD = 'Operating Field';
        public final String OPERATINGDISTRICT = 'Operating District';
        public final String BUSINESSUNIT = 'Business Unit';

        // Added in migration to lightning
        public final Map<String, String> OBJECTNAMETORECORDTYPEMAP =
                new Map<String, String>{
                        String.valueOf(new Location__c().getSObjectType()) => 'Well ID',
                        String.valueOf(new Facility__c().getSObjectType()) => 'Facility',
                        String.valueOf(new Equipment__c().getSObjectType()) => 'Equipment',
                        String.valueOf(new System__c().getSObjectType()) => 'System',
                        String.valueOf(new Sub_System__c().getSObjectType()) => 'Sub-system',
                        String.valueOf(new Yard__c().getSObjectType()) => 'Yard',
                        String.valueOf(new Functional_Equipment_Level__c().getSObjectType()) => 'Functional Equipment Level',
                        String.valueOf(new Well_Event__c().getSObjectType()) => 'Well Event',
                        String.valueOf(new Field__c().getSObjectType()) => 'Operating Field',
                        String.valueOf(new Operating_District__c().getSObjectType()) => 'Operating District',
                        String.valueOf(new Business_Unit__c().getSObjectType()) => 'Business Unit'
                };

        public final Map<String, String> MAPOBJECTNAME =
                new Map<String, String>{
                        WELLTRACKER => 'Location__c',
                        FACILITYTRACKER => 'Facility__c',
                        WELL => 'Location__c',
                        FACILITY => 'Facility__c',
                        EQUIPMENT => 'Equipment__c',
                        MAINSYSTEM => 'Location__c',
                        SUBSYSTEM => 'Location__c',
                        YARD => 'Location__c',
                        FUNCTIONALEQUIPMENT => 'Location__c',
                        OPERATINGFIELD => 'Field__c',
                        OPERATINGDISTRICT => 'Operating_District__c',
                        BUSINESSUNIT => 'Business_Unit__c',
                        WELLEVENT => 'Location__c'
                };

        public final Map<String, String> MAPOBJECTLABEL =
                new Map<String, String>{
                        WELLTRACKER => 'Location UWI',
                        FACILITYTRACKER => 'Facility',
                        WELL => 'Location UWI',
                        FACILITY => 'Facility',
                        EQUIPMENT => 'Equipment',
                        MAINSYSTEM => 'System',
                        SUBSYSTEM => 'Sub-system',
                        YARD => 'Yard',
                        FUNCTIONALEQUIPMENT => 'Functional Equipment Level',
                        OPERATINGFIELD => 'Operating Field',
                        OPERATINGDISTRICT => 'Operating District',
                        BUSINESSUNIT => 'Business Unit',
                        WELLEVENT => 'Well Event'
                };

        public final Map<String, Boolean> MAPSHOWPRODUCTIONIMPACTED =
                new Map<String, Boolean>{
                        WELLTRACKER => false,
                        FACILITYTRACKER => false,
                        WELL => true,
                        FACILITY => true,
                        EQUIPMENT => true,
                        MAINSYSTEM => true,
                        SUBSYSTEM => true,
                        YARD => true,
                        FUNCTIONALEQUIPMENT => true,
                        OPERATINGFIELD => false,
                        OPERATINGDISTRICT => false,
                        BUSINESSUNIT => false,
                        WELLEVENT => true
                };

        public final Map<String, Boolean> MAPSHOWEQUIPMENTDOWN =
                new Map<String, Boolean>{
                        WELLTRACKER => false,
                        FACILITYTRACKER => false,
                        WELL => true,
                        FACILITY => true,
                        EQUIPMENT => true,
                        MAINSYSTEM => true,
                        SUBSYSTEM => true,
                        YARD => true,
                        FUNCTIONALEQUIPMENT => true,
                        OPERATINGFIELD => false,
                        OPERATINGDISTRICT => false,
                        BUSINESSUNIT => false,
                        WELLEVENT => true
                };

        public final Map<String, Boolean> MAPSHOWPEQUIPMENTINFO =
                new Map<String, Boolean>{
                        WELLTRACKER => false,
                        FACILITYTRACKER => false,
                        WELL => false,
                        FACILITY => false,
                        EQUIPMENT => true,
                        MAINSYSTEM => false,
                        SUBSYSTEM => false,
                        YARD => false,
                        FUNCTIONALEQUIPMENT => false,
                        OPERATINGFIELD => false,
                        OPERATINGDISTRICT => false,
                        BUSINESSUNIT => false,
                        WELLEVENT => false
                };

        public final Map<String, Boolean> MAPSHOWPVRAVGINFO =
                new Map<String, Boolean>{
                        WELLTRACKER => false,
                        FACILITYTRACKER => false,
                        WELL => true,
                        FACILITY => false,
                        EQUIPMENT => true,
                        MAINSYSTEM => false,
                        SUBSYSTEM => false,
                        YARD => false,
                        FUNCTIONALEQUIPMENT => false,
                        OPERATINGFIELD => false,
                        OPERATINGDISTRICT => false,
                        BUSINESSUNIT => false,
                        WELLEVENT => true
                };

        public final Map<String, Boolean> MAPSHOWTRACKERINFO =
                new Map<String, Boolean>{
                        WELLTRACKER => true,
                        FACILITYTRACKER => true,
                        WELL => true,
                        FACILITY => true,
                        EQUIPMENT => false,
                        MAINSYSTEM => false,
                        SUBSYSTEM => false,
                        YARD => false,
                        FUNCTIONALEQUIPMENT => false,
                        OPERATINGFIELD => false,
                        OPERATINGDISTRICT => false,
                        BUSINESSUNIT => false,
                        WELLEVENT => false
                };

        public LightningNotificationRecordType() {
        }
    }
}