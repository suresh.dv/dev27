public class CO_OptimizationTrackerEditExt {

    public Well_Optimization_Tracker__c wot {get;set;}
    public Location__c location {get;set;}
    public List<Attachment> attachmentsList {get;set;}
    public List<Attachment> existingAttachmentsList {get;set;}
    public String ActiveTab {set;}
    // public List<Attachment> attachmentsToDelete {get;set;}
    
    public String on_Hold_Status = 'On Hold';
    public String cancelled_Status = 'Cancelled';
    public String in_Progress_PE_Status = 'In Progress - Production Engineer';
    public String in_Progress_G_Status = 'In Progress - Geologist';
    public String in_Progress_DE_Status = 'In Progress - Development Engineer';
    public String pending_Managers_Approval_Status = 'Pending Manager\'s Approval';
    public String approved_Status = 'Approved';
    public String completed_Status = 'Completed';
    public String complete_Status = 'Complete';
    public String notApplicable_Status = 'Not Applicable';
    public String in_Progress_Status = 'In Progress';
    
    // public Boolean showAddButton {get;set;}
    
    
    public static final Integer NUM_ATTACHMENTS_TO_ADD=1;
    
    public String WSCFLink = 'http://huskynet.huskyenergy.com/dept_corporate/fd_services/pg_corporate_forms_home.html';
    

    public CO_OptimizationTrackerEditExt(ApexPages.StandardController stdController) {
        if(!Test.isRunningTest()) {
            stdController.addFields(new List<String>{'Optimization_Request__c','Well_Location__c','Optimization_Tracker_Status__c',
                                                 'Opt_Type__c','Reason__c','Predicted_Incremental_Oil_bopd__c',
                                                 'Predicted_Incremental_Water_bwpd__c','Panel_Approved_Date__c','Comments_water_flowlined_gas_sales_etc__c',
                                                 'Current_State__c','Job_Complete_Date__c','AFE__c','AFE_Amount__c','Overall_Values_Budget__c','Overall_Values_Actual__c',
                                                 'Actual_Spend__c','RoR__c','Job_Purpose_Description__c','RMLC_Approval__c','RMLC_Approval_Description__c',
                                                 'RMLC_Complete_Date__c','Regulatory_Approvals__c','Regulatory_Approvals_Description__c','Regulatory_Approvals_Complete_Date__c',
                                                 'Financial_Approval__c','Financial_Approval_Description__c','Financial_Approval_Complete_Date__c','Construction__c','Construction_Description__c',
                                                 'Construction_Complete_Date__c','Surface_Equipment__c','Surface_Equipment_Description__c','Surface_Equipment_Complete_Date__c',
                                                 'Construction_Access_Road_Upgrade__c','Construction_Berm__c','Construction_Lease_Upgrade__c',
                                                 'Utilize_Surplus_Equipment__c','From_Location__c','From_Location_Containment__c','Engine_Size__c','Type_of_Drive__c','Tank_Size__c','Tubing_Rotator__c',
                                                 'Water_Transfer_Set_up__c','Containment__c','Type_of_Shack__c','Silent_Mufflers__c','Cattle_Guards__c','Propane__c','Line_Gas__c','Correct_Signage__c',
                                                 'Optimization_Category__c','Downhole_Equipment__c','Downhole_Equipment_Complete_Date__c','Downhole_Equipment_Description__c',
                                                 'Tubing_size__c','Tubing_Hanger_Type__c','Rod_Grade__c','Rod_Size__c','Rod_Type__c','Pin_Size__c','Casing_Grade__c','Casing_Diameter__c',
                                                 'Casing_Weight__c','Pump_Type__c','Pump_Vendor__c','Pump_Displacement_Size__c','Pump_Lift__c','Pump_Elastomer__c','Test_Efficiency__c',
                                                 'Intake_Depth__c','Rotor_Tag_Type__c',
                                                 'EQ_AFE__c','EQ_AFE_Amount__c','EQ_Overall_Values_Budget__c','EQ_Overall_Values_Actual__c','EQ_Actual_Spend__c',
                                                 //Production Engineer Form Fields
                                                 'Shut_in_Fluid_Level__c','Shut_in_Casing_Pressure__c','Reservoir_Pressure__c','Lease_Condition__c','Is_the_Well_Equipped__c',
                                                 'Is_the_Well_Flowlined__c','Proposed_Job__c','Capital__c','Estimated_Op_Cost__c','Production_Engineers_Comments__c','Production_Engineer_Portion_Complete__c',
                                                 'Geologists_review_required__c','Well_Flowline__c','Production_Engineer_Attachments__c',
                                                 //Development Engineer Form Fields
                                                 'Zones_of_Interest__c','Current_Production_Oil_m3_d__c','Current_Production_Water_m3_d__c','Incremental_Oil_Production_m3_d__c','Incremental_Water_Production_m3_d__c',
                                                 'CTD_Oil_m3__c','CTD_Water_m3__c','Reserves_m3__c','Reserves_Booked__c','FL_bbl__c','FD_bbl__c','ROR_Atax_35_Brent_Flat__c','ROR_Atax_45_Brent_Flat__c',
                                                 'Payout_Period_Months_35_Brent_Flat__c','Payout_Period_Months_45_Brent_Flat__c','Major_Risks__c','Mitigation_to_Risks__c','Viscosity_cP__c','Development_Engineers_Comments__c',
                                                 'Development_Engineer_Portion_Complete__c','Peep_Cases__c','Analog_Well_Information__c','Development_Engineer_Attachments__c',
                                                 //Geologist Form Fields
                                                 'Net_oil_Pay__c','Structure_Top_Reservoir__c','Structure_Base_Reservoir__c','G_O_contact__c','O_W_contact__c','Geologists_Comments__c','Geologist_Portion_Complete__c',
                                                 'Structure_Map__c','Net_Pay_Map__c','Logs__c','Comparison_X_Section__c',
                                                 //Development Managment
                                                 'Development_Management_Validation__c','Development_Management_Comments__c',
                                                 'Cycle_Time_Days__c', 'Initiate_WSCF__c', 'WSCF_Link__c', 'Baseline_Oil_pd__c', 'Baseline_Water_pd__c',
                                                 'Development_Engineer_Review_Required__c', 'Asset_Manager_Review_Required__c'
                                                 });
                                                }
        this.wot = (Well_Optimization_Tracker__c)stdController.getRecord();
        getWellLocationsData();
        getAttachments();
        addAttachment();
        //showAddButton = true;
        wot.WSCF_Link__c = WSCFLink;
        System.debug('DATA ' + this.wot);
    }
    
    //Get attachment if wot is present on load(on edit mode)
    public void getAttachments() {
        if (wot.id != null) {
            existingAttachmentsList = [SELECT Name, ParentId, CreatedDate, Owner.Name, Owner.UserRole.Name, Description FROM Attachment WHERE parentId=:wot.id];
        }
    }
    
    //Create new empty attachment to show in list(to be able to select new attachment)
    public void addAttachment() {
        if (attachmentsList == null) {
            attachmentsList = new List<Attachment>();
        } 
        for (Integer i = 0; i < NUM_ATTACHMENTS_TO_ADD; i++) {
            attachmentsList.add(new Attachment());
        }
    }
    
    public void saveAttachments() {
        validateWellLocationField();
        
        if (!ApexPages.hasMessages() && attachmentsList.get(0).name != null){
            SavePoint sp = Database.setSavepoint();
            try {
                if (!wot.Initiate_WSCF__c) {
                    wot.WSCF_Link__c = null;
                }
                upsert wot;
                List<Attachment> toInsert=new List<Attachment>();
                for (Attachment newAtt : attachmentsList)
                {
                    if (newAtt.Body!=null)
                    {
                        newAtt.parentId=wot.id;
                        toInsert.add(newAtt);
                    }
                }
                insert toInsert;
            } catch (DMLException e) {
                Database.rollBack(sp);
            }
        }
        attachmentsList.clear();
        addAttachment();
        existingAttachmentsList = null;
        getAttachments(); 
    }
    
    public void removeAttachment() {
        Id attachmentId = String.valueOf(ApexPages.currentPage().getParameters().get('attId'));
        for (Attachment att : existingAttachmentsList) {
            if (att.id == attachmentId) {
                delete att;
            }
        }
        getAttachments();
    }
    
    public String getActiveTab() {
        //PE,G,DE,DM
        if (wot.Optimization_Tracker_Status__c == in_Progress_G_Status) {
            return 'G';
        }
        if (wot.Optimization_Tracker_Status__c == in_Progress_DE_Status) {
            return 'DE';
        }
        if (wot.Optimization_Tracker_Status__c == approved_Status 
            && wot.Asset_Manager_Review_Required__c) {
            return 'DM';
        }
        if (wot.Optimization_Tracker_Status__c == in_Progress_PE_Status 
        || wot.Optimization_Tracker_Status__c == on_Hold_Status 
        || wot.Optimization_Tracker_Status__c == cancelled_Status
        || wot.Optimization_Tracker_Status__c == approved_Status) {
            return 'PE';
        }
        return 'DM';
    }

    public Boolean getNewFromLocation(){
        if(String.isBlank(wot.Well_Location__c)){
            return false;
        }
        return true;
    }
    
    public Boolean ShowCycleTimeDays() {
        return wot.Current_State__c != null && wot.Job_Complete_Date__c != null;
    }
    
    public Boolean getIsEdit() {
        return wot.id != null;
    }
    
    public Boolean getIsStatusActive() {
        if (wot.Optimization_Tracker_Status__c == approved_Status 
        || wot.Optimization_Tracker_Status__c == in_Progress_DE_Status 
        || wot.Optimization_Tracker_Status__c == in_Progress_G_Status
        || wot.Optimization_Tracker_Status__c == in_Progress_PE_Status
        || wot.Optimization_Tracker_Status__c == pending_Managers_Approval_Status) {
           return true;
        }
        return false;
    }
    
    public Boolean getIsHeaderVisible(){
        if(wot.Optimization_Request__c != null && wot.Optimization_Category__c != null) {
            return true;
        }
        return false;
    }

    public Boolean getIsOnHold(){
        if(getIsHeaderVisible() && wot.Optimization_Tracker_Status__c == on_Hold_Status) {
            return true;
        }
        return false;
    }

    public Boolean getIsApproved(){
        if(getIsHeaderVisible() && wot.Optimization_Tracker_Status__c == approved_Status) {
            return true;
        }
        return false;
    }

    public Boolean getIsCancelled(){
        if(getIsHeaderVisible() && wot.Optimization_Tracker_Status__c == cancelled_Status) {
            return true;
        }
        return false;
    }

    public Boolean getIsDownhole(){
        if(wot.Optimization_Request__c.contains('Downhole')) {
            return true;
        }
        return false;
    }

    public Boolean getIsSurface(){
        if(wot.Optimization_Request__c.contains('Surface')) {
            return true;
        }
        return false;
    }
    
    //get true if all neccessary completions is done.
    public Boolean getArePortionsCompleted(){
        return wot.Production_Engineer_Portion_Complete__c
            && (!wot.Development_Engineer_Review_Required__c || wot.Development_Engineer_Portion_Complete__c)
            && (!wot.Geologists_review_required__c || wot.Geologist_Portion_Complete__c);
    }
    
    public Boolean getIsNewWellCompletionStatus(){
        if(wot.Opt_Type__c=='New Well Completion') {
            return true;
        }
        return false;
    }

    //sets status based on portion completition.
    public void setStatus() {
        //BTW Development_Management_* is asset management. 
        if (!getIsOnHold() && !getIsCancelled()) {
            if (getArePortionsCompleted()) {
                if (!wot.Asset_Manager_Review_Required__c || wot.Development_Management_Validation__c) {
                    wot.Optimization_Tracker_Status__c = approved_Status;
                } else {
                    wot.Optimization_Tracker_Status__c = pending_Managers_Approval_Status;
                }
            } else {
                if (wot.Development_Engineer_Review_Required__c)
                    wot.Optimization_Tracker_Status__c = in_Progress_DE_Status;
                if (wot.Geologists_review_required__c && !wot.Geologist_Portion_Complete__c)  {
                    wot.Optimization_Tracker_Status__c = in_Progress_G_Status;
                }
                if (!wot.Production_Engineer_Portion_Complete__c) {
                    wot.Optimization_Tracker_Status__c = in_Progress_PE_Status;
                }
            }
        }
    }
    
    //add parent id to attachments and upsert them
    public void upsertAttachments() {
        if (wot.id != null && attachmentsList != null) {
            List<Attachment> upsertList = new List<Attachment>();
            for (Attachment att : attachmentsList) {
                if(att.body != null) {
                    if (att.parentId == null) {
                        att.parentId = wot.id;
                    }
                    upsertList.add(att);
                }
            }
            try {
                System.debug(upsertList);
                upsert upsertList;
            } catch (DMLException e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.FATAL, 'Error uploading attachment(s)'));
                attachmentsList = new List<Attachment>();
                getAttachments();
            } finally {
                attachmentsList.clear();
                upsertList.clear();
            }
        } else {
            if (wot.id == null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.FATAL, 'Error cannot create well intervention'));
            }
        }
    }
    
    //validate all required fields and any fields with relation to others.
    public void validateFields() {
        if (getIsApproved()) {
            if (wot.Well_Location__c == null
            || wot.Predicted_Incremental_Oil_bopd__c == null
            || wot.Predicted_Incremental_Water_bwpd__c == null
            || wot.Panel_Approved_Date__c == null
            || wot.Current_State__c == null
            || wot.RMLC_Approval__c == null
            || wot.Regulatory_Approvals__c == null
            || wot.Financial_Approval__c == null
            || wot.Construction__c == null
            || wot.Opt_Type__c == null
            || wot.Current_State__c != null && wot.Current_State__c.contains(completed_Status) && wot.Job_Complete_Date__c == null
            || wot.RMLC_Approval__c != null && wot.RMLC_Approval__c.contains(complete_Status) && wot.RMLC_Complete_Date__c == null
            || wot.Regulatory_Approvals__c != null && wot.Regulatory_Approvals__c.contains(complete_Status) && wot.Regulatory_Approvals_Complete_Date__c == null
            || wot.Financial_Approval__c != null && wot.Financial_Approval__c.contains(complete_Status) && wot.Financial_Approval_Complete_Date__c == null
            || wot.Construction__c != null && wot.Construction__c.contains(complete_Status) && wot.Construction_Complete_Date__c == null
            || wot.Downhole_Equipment__c == null
            || ((wot.Construction__c != null && (wot.Construction__c == in_Progress_Status || wot.Construction__c == complete_Status)) && (wot.Construction_Berm__c != true && wot.Construction_Access_Road_Upgrade__c != true && wot.Construction_Lease_Upgrade__c != true ))
            || (wot.Surface_Equipment__c == null && wot.Downhole_Equipment__c == null)
            || ((wot.Surface_Equipment__c != null && wot.Surface_Equipment__c.contains(complete_Status) && wot.Surface_Equipment_Complete_Date__c == null) || (wot.Downhole_Equipment__c != null && wot.Downhole_Equipment__c.contains(complete_Status) && wot.Downhole_Equipment_Complete_Date__c == null))
            || (((wot.Optimization_Request__c.contains('Surface') || wot.Optimization_Request__c.contains('Downhole')) && wot.Surface_Equipment__c != null && (wot.Surface_Equipment__c == in_Progress_Status || wot.Surface_Equipment__c == complete_Status)) 
                        && (wot.From_Location__c == null || wot.Engine_Size__c == null 
                            || wot.Type_of_Drive__c == null || wot.Tank_Size__c == null 
                            || wot.Containment__c == null || wot.Type_of_Shack__c == null 
                            || wot.Correct_Signage__c == null 
                            || ((wot.Containment__c == 'Steel' || wot.Containment__c == 'Berm') && wot.From_Location_Containment__c == null)))){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please fill all required fields.'));
                validateProductionDataFields();
                validatePanelApprovedDateField();
                validateCurrentStateField();
                validateRMLCApprovalField();
                validateRegulatoryApprovalsField();
                validateFinancialApprovalField();
                validateConstructionField();
                validateOptTypeField();
                validateJobCompleteDateField();
                validateRMLCCompleteDateField();
                validateRegulatoryApprovalsCompleteDateField();
                validateFinancialApprovalCompleteDateField();
                validateConstructionCompleteDate();
                validateConstructionBernAccessAndLeaseFields();
                
                if(wot.Optimization_Request__c.contains('Surface') || wot.Optimization_Request__c.contains('Downhole')){
                    System.debug('Im inside Surface');
                    validateSurfaceEquipmentField();
                    validateSurfaceEquipmentCompleteDate();
                    validateDownholeEquipmentField();
                    validateDownholeEquipmentCompleteDate();
                    if(wot.Surface_Equipment__c != null && !wot.Surface_Equipment__c.contains(notApplicable_Status)) {
                        validateFromLocationField();
                        validateFromLocationContainmentField();
                        validateEngineSizeField();
                        validateTypeOfDriveField();
                        validateTankSizeField();
                        validateContainmentField();
                        validateTypeOfShackField();
                        validateCorrectSignageField();
                    }
                }
            }
            if (((wot.Job_Complete_Date__c != null && wot.RMLC_Complete_Date__c != null) && (wot.Job_Complete_Date__c < wot.RMLC_Complete_Date__c))
            || ((wot.Job_Complete_Date__c != null && wot.Regulatory_Approvals_Complete_Date__c != null) && (wot.Job_Complete_Date__c < wot.Regulatory_Approvals_Complete_Date__c))
            || ((wot.Job_Complete_Date__c != null && wot.Financial_Approval_Complete_Date__c != null) && (wot.Job_Complete_Date__c < wot.Financial_Approval_Complete_Date__c))
            || ((wot.Job_Complete_Date__c != null && wot.Construction_Complete_Date__c != null) && (wot.Job_Complete_Date__c < wot.Construction_Complete_Date__c))
            || ((wot.Job_Complete_Date__c != null && wot.Surface_Equipment_Complete_Date__c != null) && (wot.Job_Complete_Date__c < wot.Surface_Equipment_Complete_Date__c))) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Incorrectly filled date fields!'));
                validateRMLCDateField();
                validateRegulatoryDateField();
                validateFinancialDateField();
                validateConstructionDateField();
                //validateSurfaceEquipmentDateField();
            }
        } else {
            validateProductionDataFields();
        }
    }

    public PageReference saveTracker() {
        if(wot.Optimization_Tracker_Status__c.contains(cancelled_Status) || wot.Optimization_Tracker_Status__c.contains(on_Hold_Status)) {
            if(wot.Opt_Type__c == null || String.isBlank(wot.Reason__c)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR ,'Please fill all required fields.'));
                validateProductionDataFields();
                validateReasonField();
                
                return null;
            } else {
                validateProductionDataFields();
                validateReasonField();
                
                if (!ApexPages.hasMessages()) {
                    setPVRData();
                    
                    setStatus();
                    upsert wot;
                    
                    upsertAttachments();
                    
                    PageReference detailPage = new PageReference('/' + wot.id);
                    return detailPage;
                }
                return null;
            }
        }
        validateFields();

        if(!ApexPages.hasMessages()){
            setPVRData();
            
            setStatus();
            if (!wot.Initiate_WSCF__c) {
                wot.WSCF_Link__c = null;
            } 
            System.debug(wot.WSCF_Link__c);
            upsert wot;
            
            upsertAttachments();
            
            PageReference detailPage = new PageReference('/' + wot.id);
            return detailPage;
        }

        return null;
    }
        
    private void setPVRData() {
        if(location != null) {
            wot.Oil_FromPVR__c = location.PVR_Produced_Oil__c;
            wot.Water_FromPVR__c = location.PVR_Produced_Water__c;
            wot.Sand_FromPVR__c = location.PVR_Produced_Sand__c;
            wot.Date_Generated_FromPVR__c = location.PVR_Month_To_Day_Generated__c;
            wot.RecordTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = 'Well_Location' LIMIT 1].Id;
        }
    }
    
    public PageReference getWellLocationsData() {
            
        System.debug('Getting the location id:: '+wot.Well_Location__c);
        /*If the Well Intervention has been created from a Well Location, wot.Well_Location__c != null*/
        if(wot.Well_Location__c != null) {
            /*We query for the location as well as all its events.
            */
            wot.Well_Event__c = null;
            this.location = [SELECT PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_WATER__c,
                            PVR_Month_To_Day_Generated__c, PVR_Produced_Oil__c, PVR_Produced_Sand__c,
                            PVR_Produced_Water__c, PVR_Hours_On_Prod__c,
                                (SELECT PVR_AVGVOL_30D_OIL__c, PVR_AVGVOL_30D_WATER__c,
                                PVR_Month_To_Day_Generated__c, PVR_Produced_Oil__c, PVR_Produced_Sand__c,
                                PVR_Produced_Water__c, PVR_Hours_On_Prod__c 
                                FROM Well_Events__r 
                                WHERE Well_ID__c =: wot.Well_Location__c AND Status__c = 'PROD'
                                ORDER BY Name DESC NULLS LAST)
                            FROM Location__c 
                            WHERE ID =: wot.Well_Location__c 
                            LIMIT 1];
            List<Well_Event__c> listOfEvents = this.location.Well_Events__r;
            System.debug('Well Events:: '+listOfEvents);
            for(Well_Event__c event : listOfEvents) {
                System.debug('PVR_Hours_On_Prod__c:: '+event.PVR_Hours_On_Prod__c);
            }
            
            /* We check if the location has any events, if its does then we go for the latest event i.e., the first Event in the list
                Then we check if the event has the necessary PVR data that we need, if PVR_Hours_On_Prod__c is null or 0, we skip
            */
            if(listOfEvents.size() > 0 ) {

                if(!getIsEdit()) {
                    wot.Baseline_Oil_pd__c                  = listOfEvents[0].PVR_AVGVOL_30D_OIL__c;
                    wot.Baseline_Water_pd__c                = listOfEvents[0].PVR_AVGVOL_30D_WATER__c;
                    System.debug('wot.Baseline_Oil_pd__c: ' + wot.Baseline_Oil_pd__c);
                    System.debug('wot.Baseline_Water_pd__c: ' + wot.Baseline_Water_pd__c);
                }


                if(listOfEvents[0].PVR_Hours_On_Prod__c != null && listOfEvents[0].PVR_Hours_On_Prod__c != 0) {
                    if (listOfEvents[0].PVR_Produced_Oil__c != null) 
                        location.PVR_Produced_Oil__c = (listOfEvents[0].PVR_Produced_Oil__c/listOfEvents[0].PVR_Hours_On_Prod__c)*24;
                    if (listOfEvents[0].PVR_Produced_Sand__c != null)
                        location.PVR_Produced_Sand__c = (listOfEvents[0].PVR_Produced_Sand__c/listOfEvents[0].PVR_Hours_On_Prod__c)*24;
                    if (listOfEvents[0].PVR_Produced_Water__c != null)
                        location.PVR_Produced_Water__c = (listOfEvents[0].PVR_Produced_Water__c/listOfEvents[0].PVR_Hours_On_Prod__c)*24;
                    location.PVR_Month_To_Day_Generated__c = listOfEvents[0].PVR_Month_To_Day_Generated__c;
                } else {
                    location.PVR_Produced_Oil__c = 0;
                    location.PVR_Produced_Sand__c = 0;
                    location.PVR_Produced_Water__c = 0;
                    location.PVR_Month_To_Day_Generated__c = null; 
                }
                return null;
            }
            
            /* If the well has no events or if the PVR data is not populated
                we then load data directly from the location's PVR data
                again, we check if the PVR_Hours_On_Prod__c is null or 0, is true we populate 0's for all the fields
            */
            if(listOfEvents.size() == 0 && location.PVR_Hours_On_Prod__c == null) {
                location.PVR_Produced_Oil__c = 0;
                location.PVR_Produced_Sand__c = 0;
                location.PVR_Produced_Water__c = 0;
                location.PVR_Month_To_Day_Generated__c = null;
                return null;
            }
            /*Else if the location has no events but has PVR data, we populate it*/
            if(listOfEvents.size() == 0 && location.PVR_Hours_On_Prod__c != null && location.PVR_Hours_On_Prod__c != 0) {
                if(!getIsEdit()) {
                    wot.Baseline_Oil_pd__c                  = location.PVR_AVGVOL_30D_OIL__c;
                    wot.Baseline_Water_pd__c                = location.PVR_AVGVOL_30D_WATER__c;
                    System.debug('wot.Baseline_Oil_pd__c: ' + wot.Baseline_Oil_pd__c);
                    System.debug('wot.Baseline_Water_pd__c: ' + wot.Baseline_Water_pd__c);
                }

                if (location.PVR_Produced_Oil__c != null)
                    location.PVR_Produced_Oil__c = (location.PVR_Produced_Oil__c/location.PVR_Hours_On_Prod__c)*24;
                if (location.PVR_Produced_Sand__c != null)
                    location.PVR_Produced_Sand__c = (location.PVR_Produced_Sand__c/location.PVR_Hours_On_Prod__c)*24;
                if (location.PVR_Produced_Water__c != null)
                    location.PVR_Produced_Water__c = (location.PVR_Produced_Water__c/location.PVR_Hours_On_Prod__c)*24;
                return null;
            }
        }
        return null;
    }

    public void validateProductionDataFields() {
        if (getIsHeaderVisible()) {
            validateOptTypeField();
            validateStatusField();
            validateWellLocationField();
            validatePredictedInceremtalOilField();
            validatePredictedIncrementalWaterField();
        }
    }

    public void validateRMLCDateField() {
        if(wot.Job_Complete_Date__c != null && wot.RMLC_Complete_Date__c != null){
            if(wot.Job_Complete_Date__c < wot.RMLC_Complete_Date__c){
                wot.Job_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the RMLC Complete Date');
                wot.RMLC_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the RMLC Complete Date');
            }
        }
    }

    public void validateRegulatoryDateField() {
        if(wot.Job_Complete_Date__c != null && wot.Regulatory_Approvals_Complete_Date__c != null){
            if(wot.Job_Complete_Date__c < wot.Regulatory_Approvals_Complete_Date__c){
                wot.Job_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Regulatory Approvals Complete Date');
                wot.Regulatory_Approvals_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Regulatory Approvals Complete Date');
            }
        }
    }

    public void validateFinancialDateField() {
        if(wot.Job_Complete_Date__c != null && wot.Financial_Approval_Complete_Date__c != null){
            if(wot.Job_Complete_Date__c < wot.Financial_Approval_Complete_Date__c){
                wot.Job_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Financial Approvals Complete Date');
                wot.Financial_Approval_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Financial Approvals Complete Date');
            }
        }
    }

    public void validateConstructionDateField() {
        if(wot.Job_Complete_Date__c != null && wot.Construction_Complete_Date__c != null){
            if(wot.Job_Complete_Date__c < wot.Construction_Complete_Date__c){
                wot.Job_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Contruction Complete Date');
                wot.Construction_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Contruction Complete Date');
            }
        }
    }

    public void validateSurfaceEquipmentDateField() {
        if(wot.Job_Complete_Date__c != null && wot.Surface_Equipment_Complete_Date__c != null){
            if(wot.Job_Complete_Date__c < wot.Surface_Equipment_Complete_Date__c){
                wot.Job_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Surface Equipment Complete Date');
                wot.Surface_Equipment_Complete_Date__c.addError('The Job Complete Date for Optimization Progress cannot be before the Surface Equipment Complete Date');
            }
        }
    }
    
    public void validateJobCompleteDateField() {
        if(wot.Current_State__c != null && wot.Current_State__c.contains(completed_Status) && wot.Job_Complete_Date__c == null) {
            wot.Job_Complete_Date__c.addError('Required field!');
        }
    }       
    
    public void validateRMLCCompleteDateField() {
        if(wot.RMLC_Approval__c != null && wot.RMLC_Approval__c.contains(complete_Status) && wot.RMLC_Complete_Date__c == null) {
            wot.RMLC_Complete_Date__c.addError('Required field!');
        }
    }       
    
    public void validateRegulatoryApprovalsCompleteDateField() {
        if(wot.Regulatory_Approvals__c != null && wot.Regulatory_Approvals__c.contains(complete_Status) && wot.Regulatory_Approvals_Complete_Date__c == null) {
            wot.Regulatory_Approvals_Complete_Date__c.addError('Required field!');
        }
    }       
    
    public void validateFinancialApprovalCompleteDateField() {
        if(wot.Financial_Approval__c != null && wot.Financial_Approval__c.contains(complete_Status) && wot.Financial_Approval_Complete_Date__c == null) {
            wot.Financial_Approval_Complete_Date__c.addError('Required field!');
        }
    }       
    
    public void validateConstructionCompleteDate() {
        if(wot.Construction__c != null && wot.Construction__c.contains(complete_Status) && wot.Construction_Complete_Date__c == null) {
            wot.Construction_Complete_Date__c.addError('');
        }
    }       
    
    public void validateSurfaceEquipmentCompleteDate() {
        if(wot.Surface_Equipment__c != null && wot.Surface_Equipment__c.contains(complete_Status) && wot.Surface_Equipment_Complete_Date__c == null) {
            wot.Surface_Equipment_Complete_Date__c.addError('Required field!');
        }
    }

    public void validateDownholeEquipmentCompleteDate() {
        if(wot.Downhole_Equipment__c != null && wot.Downhole_Equipment__c.contains(complete_Status) && wot.Surface_Equipment_Complete_Date__c == null) {
            wot.Downhole_Equipment_Complete_Date__c.addError('Required field!');
        }
    }
    
    public void validateConstructionBernAccessAndLeaseFields() {
        if(wot.Construction__c != null && (wot.Construction__c == in_Progress_Status || wot.Construction__c == complete_Status)){
            if(wot.Construction_Berm__c != true && wot.Construction_Access_Road_Upgrade__c != true && wot.Construction_Lease_Upgrade__c != true ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please select at least one of these checkboxes: Access Road Upgrade, Lease Upgrade, Berm.'));
            }
        }
    }
    
    public void validateSurfaceEquipmentField(){
        if (wot.Surface_Equipment__c == null) {
            wot.Surface_Equipment__c.addError('Required field!');
        }
    }

    public void validateDownholeEquipmentField(){
        if (wot.Downhole_Equipment__c == null) {
            wot.Downhole_Equipment__c.addError('Required field!');
        }
    }
    
    public void validateFromLocationField(){
        if (wot.From_Location__c == null) {
            wot.From_Location__c.addError('Required field!');
        }
    }

    //added on 24.4.2017 FromLocationContainment
     public void validateFromLocationContainmentField(){ 
        if ((wot.Containment__c == 'Steel' || wot.Containment__c == 'Berm') && wot.From_Location_Containment__c == null) {
            wot.From_Location_Containment__c.addError('Required field!');
        }
    }

    public void validateEngineSizeField(){
        if (wot.Engine_Size__c == null) {
            wot.Engine_Size__c.addError('Required field!');
        }
    }
    
    public void validateTypeOfDriveField(){
        if (wot.Type_of_Drive__c == null) {
            wot.Type_of_Drive__c.addError('Required field!');
        }
    }
    
    public void validateTankSizeField(){
        if (wot.Tank_Size__c == null) {
            wot.Tank_Size__c.addError('Required field!');
        }
    }
    
    public void validateContainmentField(){
        if (wot.Containment__c == null) {
            wot.Containment__c.addError('Required field!');
        }
    }
    
    public void validateTypeOfShackField(){
        if (wot.Type_of_Shack__c == null) {
            wot.Type_of_Shack__c.addError('Required field!');
        }
    }
    
    public void validateCorrectSignageField(){
        if (wot.Correct_Signage__c == null) {
            wot.Correct_Signage__c.addError('Required field!');
        }
    }
    
    public void validateOptTypeField(){
        if(wot.Opt_Type__c == null){
            wot.Opt_Type__c.addError('Required field!');
        }
    }
    
    public void validatePanelApprovedDateField(){
        if (wot.Panel_Approved_Date__c == null) {
            wot.Panel_Approved_Date__c.addError('Required field!');
        }
    }
    
    public void validateCurrentStateField(){
        if (wot.Current_State__c == null) {
            wot.Current_State__c.addError('Required field!');
        }
    }
    
    public void validateRMLCApprovalField(){
        if (wot.RMLC_Approval__c == null) {
            wot.RMLC_Approval__c.addError('Required field!');
        }
    }
    
    public void validateRegulatoryApprovalsField(){
        if (wot.Regulatory_Approvals__c == null) {
            wot.Regulatory_Approvals__c.addError('Required field!');
        }
    }
    
    public void validateFinancialApprovalField(){
        if (wot.Financial_Approval__c == null) {
            wot.Financial_Approval__c.addError('Required field!');
        }
    }
    
    public void validateConstructionField(){
        if (wot.Construction__c == null) {
            wot.Construction__c.addError('Required field!');
        }
    }
    
    public void validateWellLocationField() {
        if (wot.Well_Location__c == null) {
            wot.Well_Location__c.addError('Required field!');
        }
    }
    
    public void validatePredictedInceremtalOilField() {
        if (wot.Predicted_Incremental_Oil_bopd__c == null) {
            wot.Predicted_Incremental_Oil_bopd__c.addError('Required field!');
        }
    }
    
    public void validatePredictedIncrementalWaterField() {
        if (wot.Predicted_Incremental_Water_bwpd__c == null) {
            wot.Predicted_Incremental_Water_bwpd__c.addError('Required field!');
        }
    }
    
    public void validateStatusField() {
        if (wot.Optimization_Tracker_Status__c == null) {
            wot.Optimization_Tracker_Status__c.addError('Required field!');
        }
    }
    
    public void validateReasonField() {
        if (String.isBlank(wot.Reason__c)) {
            wot.Reason__c.addError('Required field!');
        }
    }    
}