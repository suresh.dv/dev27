/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Wrapper class that holds values for picklists that are rendered on WOA List View.
Test Class:     VTT_WOA_SearchCriteria_OptionsTest
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_WOA_SearchCriteria_Options {

    private static final String BLANK = '';
    private static final String ANY_STRING = '- Any -';
    private static final String UNASSIGNED_KEY = '1';
    private static final String UNASSIGNED = '- Unassigned -';

    @AuraEnabled
    public VTT_WOA_SearchCriteria cleanCriteria;

    @AuraEnabled
    public Boolean isAdmin;

    @AuraEnabled
    public Boolean isVendorSupervisor;

    @AuraEnabled
    public Contact tradesman;

    @AuraEnabled
    public List<HOG_PicklistItem_Simple> vendors;

    @AuraEnabled
    public List<HOG_PicklistItem_Simple> tradesmans;

    @AuraEnabled
    public List<HOG_PicklistItem_Simple> AMUs;

    @AuraEnabled
    public List<HOG_PicklistItem_Simple> orderTypes;

    @AuraEnabled
    public List<HOG_PicklistItem_Simple> activityStatuses;

    /**
     * create instance of Search Criteria, with vendor and tradesman contacts
     * Used for initial load of filter in List View and for clear action.
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options buildCleanCriteria() {
        cleanCriteria = new VTT_WOA_SearchCriteria();
        if(tradesman != null) {
            cleanCriteria.vendorFilter = tradesman.AccountId;
            cleanCriteria.tradesmanFilter = tradesman.Id;
        }
        cleanCriteria.hideCompletedFilter = true;
        return this;
    }

    /**
     * Set's isAdmin and isVendorSupervisor flags
     *
     * @param isAdmin
     * @param isVendorSupervisor
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options setFlags(Boolean isAdmin, Boolean isVendorSupervisor) {
        this.isAdmin = isAdmin;
        this.isVendorSupervisor = isVendorSupervisor;
        return this;
    }

    /**
     * Set tradesman contact
     *
     * @param tradesman
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options setTradesman(Contact tradesman) {
        this.tradesman = tradesman;
        return this;
    }

    /**
     * Builds list of Vendors Picklist Items based on list of Accounts
     * Uses Id and Name as pairs.
     *
     * @param vendors
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options setVendors(List<Account> vendors) {
        this.vendors = new List<HOG_PicklistItem_Simple>();

        this.vendors.add(new HOG_PicklistItem_Simple(BLANK, ANY_STRING));
        this.vendors.add(new HOG_PicklistItem_Simple(UNASSIGNED_KEY, UNASSIGNED));

        for(Account vendor : vendors) {
            this.vendors.add(
                    new HOG_PicklistItem_Simple(
                            vendor.Id,
                            vendor.Name
                    )
            );
        }

        return this;
    }

    /**
     * Builds list of Tradesmen picklist Items based on list of Contacts
     * Uses Id and Name as pairs
     * Adds Any and None options
     *
     * @param tradesmans
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options setTradesmans(List<Contact> tradesmans) {
        this.tradesmans = new List<HOG_PicklistItem_Simple>();

        this.tradesmans.add(new HOG_PicklistItem_Simple(BLANK, ANY_STRING));
        this.tradesmans.add(new HOG_PicklistItem_Simple(UNASSIGNED_KEY, UNASSIGNED));

        for(Contact tradesman : tradesmans) {
            this.tradesmans.add(
                    new HOG_PicklistItem_Simple(
                            tradesman.Id,
                            tradesman.Name
                    )
            );
        }

        return this;
    }

    /**
     * Builds list of AMUs picklist items based on list of Field(AMUs)
     * Uses Id and Name as pairs
     * Adds Any option
     *
     * @param AMUs
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options setAMUs(List<Field__c> AMUs) {
        this.AMUs = new List<HOG_PicklistItem_Simple>();

        this.AMUs.add(new HOG_PicklistItem_Simple(BLANK, ANY_STRING));

        for(Field__c field : AMUs) {
            this.AMUs.add(
                    new HOG_PicklistItem_Simple(
                            field.Id,
                            field.Name
                    )
            );
        }

        return this;
    }

    /**
     * Builds list of Order Type picklist items based on list of Strings
     * Uses value for both value and label
     * Adds Any option
     *
     * @param orderTypes
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options setOrderTypes(List<String> orderTypes) {
        this.orderTypes = new List<HOG_PicklistItem_Simple>();

        this.orderTypes.add(new HOG_PicklistItem_Simple(BLANK, ANY_STRING));

        for(String orderType : orderTypes) {
            this.orderTypes.add(
                    new HOG_PicklistItem_Simple(
                            orderType,
                            orderType
                    )
            );
        }

        return this;
    }

    /**
     * Builds list of Activity Status picklist items based on VTT Constant containing Filterable WOA Statuses
     *
     * @return VTT_WOA_SearchCriteria_Options
     */
    public VTT_WOA_SearchCriteria_Options setActivityStatuses() {
        this.activityStatuses = new List<HOG_PicklistItem_Simple>();

        for(Schema.PicklistEntry activityStatus : VTT_Constants.FILTERABLE_WOA_STATUSES) {
            this.activityStatuses.add(
                    new HOG_PicklistItem_Simple(
                            activityStatus.value,
                            activityStatus.label
                    )
            );
        }

        return this;
    }

}