/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Test Class: 	HOG_GeneralUtilitiesTest
Description:    HOG Utility class containing general methods usefull thru all HOG aplications.  
History:        jschn 05.16.2018 - Created.
                jschn 06.01.2018 - added getUrlParam, addPageError
                jschn 10.10.2019 - added buildKey (part of Lightning Community migration)
**************************************************************************************************/
public with sharing class HOG_GeneralUtilities {

    public static final String SOBJECT_STRING       = '*SOBJECT*';
    public static final String BASIC_QUERY_STRING   = 'SELECT Id, Name '
                                                    + 'FROM ' + SOBJECT_STRING;
    public static final Boolean DEBUG_ON = true;

	/**
	 * Get default value from picklist field.
     * You can run it with getDefaultOrFirstForPicklist(SObjectAPIName.FieldAPIName.getDescribe().getSObjectField());
	 * @param  theField Picklist field for which you want to get Default of first value.
	 * @return          Default or first value for picklist.
	 */
	public static String getDefaultOrFirstForPicklist(Schema.SObjectField theField) {
        if (theField != null 
    	&& theField.getDescribe().isAccessible()) {
    		
    		List<Schema.PicklistEntry> picklistEntries = theField.getDescribe().getPicklistValues();
            
            for(Schema.PicklistEntry ple : picklistEntries) {
        
                if(ple.isDefaultValue()) return ple.getValue();

            }

            if (picklistEntries != null && picklistEntries.size() > 0) {
                return picklistEntries.get(0).getValue();
            }
    	}

        return '';
    }

    /**
     * This returns true if user is loggedin in lightning
     *
     * @return
     */
    public static Boolean isUserUsingLightning(){
        if(UserInfo.getUiThemeDisplayed() == 'Theme4d'){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Converts List of SObjects into List of Ids.
     * @param  objects List of SObjects to convert.
     * @return         List of Ids.
     */
    public static List<Id> getListOfIds(List<SObject> objects) {
        return new List<Id>(getSetOfIds(objects));
    }

    /**
     * Converts List of SObjects into Set of Ids.
     * @param  objects List of SObjects to convert.
     * @return         Set of Ids.
     */
    public static Set<Id> getSetOfIds(List<SObject> objects) {
        return (new Map<Id, SObject>(objects)).keySet();
    }

    /**
     * Gets list of parent ids from List of child SObjects based on lookup field name
     * @param  childObjectList List of child SObjects
     * @param  lookupFieldName Lookup field name
     * @return                 List of parent Ids
     */
    public static List<Id> getListOfParentIds(List<SObject> childObjectList, String lookupFieldName) {
        return new List<Id>(getSetOfParentIds(childObjectList, lookupFieldName));
    }

    /**
     * Get set of parent ids from List of child SObjects based on lookup field name
     * @param  childObjectList List of child SObjects
     * @param  lookupFieldName Lookup field name
     * @return                 List of parent Ids
     */
    public static Set<Id> getSetOfParentIds(List<SObject> childObjectList, String lookupFieldName){
        Set<Id> parentIds = new Set<Id>();
        for(SObject obj : childObjectList){
            parentIds.add((Id) obj.get(lookupFieldName));
        }
        return parentIds;
    }

    /**
     * get url param value based on key
     * @param  paramKey key under which is value stored
     * @return          String value
     */
    public static String getUrlParam(String paramKey) {
        return ApexPages.currentPage().getParameters().get(paramKey);
    }

    /**
     * Set url param value based on key and value provided as parameters
     * @param  paramKey key under which will be value stored
     * @param  value    value which you want to add 
     */
    public static void setUrlParam(String paramKey, String value) {
        ApexPages.currentPage().getParameters().put(paramKey, value);
    }

    /**
     * Add error msg into page.
     * @param msg Msg to display on page.
     */
    public static void addPageError(String msg) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, msg));
    }

    /**
     * Checks if there is Apex Message with ERROR severity.
     * @return true if ERROR occured.
     */
    public static Boolean hasPageError() {
        for (ApexPages.Message msg : ApexPages.getMessages()) {
            if (msg.getSeverity() == ApexPages.Severity.ERROR) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets picklist for requested field on object.
     * @param  objName   API Name of sobject where requested field is
     * @param  fieldName API Name of field
     * @return           Picklist(list of selectOptions)
     */
    public static List<SelectOption> getPicklistvalues(String objName, String fieldName) {
        List<SelectOption> options = new List<SelectOption>();
        
        Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();

        for(Schema.PicklistEntry ple : fieldMap.get(fieldName).getDescribe().getPicklistValues()) {
            options.add(new SelectOption(ple.getLabel(), ple.getValue()));
        }
        return options;
    }


    /**
     * Used to navigate back to record ID in lightning
     * !!!!!!!!!!!!!!!!!!!! WARNING !!!!!!!!!!!!!!!!!
     * This might not work in future as this is not properly documented by Salesforce
     *
     */
    @AuraEnabled
    public static void redirectToThisPage(String redirectId){
        PageReference p = new PageReference(redirectId);
        // KLUDGE
        Aura.redirect(p);
    }

    /**
     * Generic method for building key from record values based on provided list of Field API Names
     *
     * @param record
     * @param fields
     *
     * @return String
     */
    public static String buildKey(SObject record, List<String> fields) {
        String key = '';
        if(record != null && fields != null) {
            for(String field : fields) {
                key += String.valueOf(record.get(field));
            }
        }
        return key;
    }

    /**
     * Returns substring of value based on length.
     * If value doesn't have enough characters to do full substring original value is returned.
     *
     * @param value
     * @param length
     *
     * @return String
     */
    public static String getSubstring(String value, Integer length) {
        String result = null;

        if(String.isNotBlank(value)) {

            if(value.length() >= length) {
                result = value.substring(0, length);
            } else {
                result = value;
            }

        }

        return result;
    }

}