/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VGEmailNotificationsSchedulable.cls
History:        jschn 05.02.2018 - Created.
                mp 18.09.2018 - fixed rescheduledJob throwing null pointer exception in case of the rescheduled part not working
**************************************************************************************************/
@IsTest
private class HOG_VGEmailNotificationsSchedulableTest {
    
    @IsTest
    static void testRescheduledRun() {
        String retryJobName = HOG_VGEmailNotificationsSchedulable.RESCHEDULED_JOB_NAME + '%';

        Test.startTest();
        HOG_VGEmailNotificationsSchedulable m = new HOG_VGEmailNotificationsSchedulable();
        String sch = '0 0 8 * * ?';
        System.schedule(HOG_VGEmailNotificationsSchedulable.JOB_NAME, sch, m);

        //related job
        HOG_PVRUpdateBatchSchedulable relatedJob = new HOG_PVRUpdateBatchSchedulable();
        String schRJ = '0 30 9 * * ?';
        System.schedule(HOG_VGEmailNotificationsSchedulable.RELATED_JOB_NAME, schRJ, relatedJob);

        m.runRescheduleRun();
        Test.stopTest();

        //Check Rescheduled Job
        Datetime t = Datetime.now().addMinutes(30);
        String rescheduleCronString = '0 '
                + String.valueOf(t.minute())
                + ' '
                + String.valueOf(t.hour())
                + ' '
                + String.valueOf(t.day())
                + ' '
                + String.valueOf(t.month())
                + ' ? '
                + String.valueOf(t.year());
        List<CronTrigger> cronTriggers = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
                                          FROM CronTrigger
                                          WHERE CronJobDetail.Name LIKE :retryJobName];
        System.assertEquals(cronTriggers.size(), 1);
        System.assertEquals(cronTriggers[0].CronExpression, rescheduleCronString);

        //NOTE: There is no way to assert anything except for CronExpression. 
        //TimesTrigger is not incrementing bcs CronTrigger is not fired which means value is not updated even thou scheduled class fires. 
        //and while there is no record update I don't have anything to assert really.
    }

    @IsTest static void testRemovingCompletedRescheduledJobs() {
        Test.startTest();
        HOG_VGEmailNotificationsSchedulable m = new HOG_VGEmailNotificationsSchedulable();
        String sch = '0 0 8 * * ?';
        System.schedule(HOG_VGEmailNotificationsSchedulable.JOB_NAME, sch, m);

        m = new HOG_VGEmailNotificationsSchedulable();
        String schRsch = '0 30 8 * * ?';
        System.schedule(HOG_VGEmailNotificationsSchedulable.RESCHEDULED_JOB_NAME, schRsch, m);
        Test.stopTest();

        //NOTE: There is no way to assert anything. TimesTrigger is not incrementing bcs CronTrigger is not fired which means value is not updated even thou scheduled class fires. 
        //and while there is no record update I don't have anything to assert really.
        // plus while there is no cronTrigger table update which means that rescheduled job runs without setting DELETED state on itself after finish, I don't have ability to test
        // and cover System.abortJob part.
    }
    
    @IsTest static void testDirectScheduledRun() {
        Test.startTest();
        HOG_VGEmailNotificationsSchedulable m = new HOG_VGEmailNotificationsSchedulable();
        String sch = '0 0 8 * * ?';
        System.schedule(HOG_VGEmailNotificationsSchedulable.JOB_NAME, sch, m);
        Test.stopTest();

        //NOTE: There is no way to assert anything. TimesTrigger is not incrementing bcs CronTrigger is not fired which means value is not updated even thou scheduled class fires. 
        //and while there is no record update I don't have anything to assert really.
    }
    
}