/*************************************************************************************************
Author:         Miro Zelina
Company:        Husky Energy
Description:    Controller for Blanket Purchase Order (BPO) create and edit page
Test Class:     SPCC_BPOControllerXTest
History:        mz  23-Nov-18   - Initial Revision
**************************************************************************************************/
public with sharing class SPCC_BPOControllerX {

    public SPCC_Blanket_Purchase_Order__c bpo {get; set;}
    private ApexPages.StandardController stdController;

    public List<BlanketPOLineItemWrapper> blanketPOlineItems {get; private set; }
    public List<SPCC_BPO_Line_Item__c> blanketPOlineItemsToDelete;

    public List<String> vendorAccountOptionsList {get;  set;}

    public List<SelectOption> glCodesOptionsList { get; set;}

    public Map<String, Account> vendorNameToVendorMap {get;  set;}

    public Map<String, String> glCodeNumberToLabelMap  {get; set;}

    //User Security
    public SPCC_Utilities.UserPermissions userPermissions {get; set;}
    public SPCC_Utilities.UserDetails userDetails;

    //Page Mode
    private ViewMode mode;

    //Constructor
    public SPCC_BPOControllerX(ApexPages.StandardController controller) {
        stdController = controller;

        if(!test.isRunningTest()){
            stdController.addFields(new List<String>{'Name', 'Description__c', 'Vendor_Account__c',
                                                     'Vendor_Account__r.Name', 'Vendor_Account__r.SAP_ID__c',
                                                     'Validity_Start_Date__c', 'Validity_End_Date__c'});
        }

        this.bpo = (SPCC_Blanket_Purchase_Order__c)controller.getRecord();
        this.userPermissions = new SPCC_Utilities.UserPermissions();
        this.userDetails = new SPCC_Utilities.UserDetails();

        //Set Mode
        mode = (bpo.Id == null) ? ViewMode.CREATE : ViewMode.EDIT;

        populateGLCodes();
        populateExistingBPOLineItems();
        populateVendorAccounts();

        blanketPOlineItemsToDelete = new List<SPCC_BPO_Line_Item__c>();
    }



    //Custom save method
    public ApexPages.PageReference saveBPO() {

        Savepoint sp = Database.setSavepoint();
        Boolean isError = false;


        if(bpo.Name == null){
            SPCC_Utilities.logErrorOnPage('Blanket PO Number is required field. Please enter <b> Blanket PO Number </b>');
            isError = true;
        }

        if(bpo.Vendor_Account__c == null) {
            SPCC_Utilities.logErrorOnPage('Vendor is required field. Please select <b> Vendor Account </b>');
            isError = true;
        }

        if (bpo.Validity_Start_Date__c == null || bpo.Validity_End_Date__c == null) {
            SPCC_Utilities.logErrorOnPage('Validity Start and End Dates are required fields. Please select <b> Validity Start and End Date </b>');
            isError = true;
        }

        //No Line Items Specified
        else if(blanketPOlineItems == null || blanketPOlineItems.isEmpty()) {
            SPCC_Utilities.logErrorOnPage('No line items added to the Blanket Purchase Order. ' +
                    'At least one <b>Line Item</b> is required. Click "<b>Add New Blanket PO Line Item</b>" button to Add Line Item.' );
            isError = true;
        }

        if(blanketPOlineItems != null) {

            for (BlanketPOLineItemWrapper blanketPOLI : blanketPOlineItems) {

                if (blanketPOLI.blanketPOlineItem.GL_Number__c == '' || blanketPOLI.blanketPOlineItem.GL_Number__c == null){

                    SPCC_Utilities.logErrorOnPage('G/L Number is required field. Please select <b>G/L Number</b> for Line Item #' + blanketPOLI.blanketPOlineItem.Item__c);
                    isError = true;
                }

                if (blanketPOLI.blanketPOlineItem.Short_Text__c == '' || blanketPOLI.blanketPOlineItem.Short_Text__c == null){

                    SPCC_Utilities.logErrorOnPage('Short Text is required field. Please type <b>Short Text</b> for Line Item #' + blanketPOLI.blanketPOlineItem.Item__c);
                    isError = true;
                }

                if (blanketPOLI.blanketPOlineItem.Committed_Amount__c == null){

                    SPCC_Utilities.logErrorOnPage('Amount is required field. Please enter <b>Amount</b> for Line Item #' + blanketPOLI.blanketPOlineItem.Item__c);
                    isError = true;
                }
            }
        }

        if (isError == true){
            return null;
        }

        try {
            /*
                combination of inserting and updating records based on ViewMode.CREATE has been used because simple upsert caused error,
                when user tried to create a record (that violated validation),
                and after correcting error-causing fields, the record ID was preserved which caused upsert error
            */

            if(mode == ViewMode.CREATE) {
                bpo.Id = null;
                insert bpo;
            } else update bpo;

            // Then save all Line Items if not CSR
            if (getIsCSRUser() == false){

                List<SPCC_BPO_Line_Item__c> blanketPOlineItemsToUpsert = new List<SPCC_BPO_Line_Item__c>();

                for (BlanketPOLineItemWrapper blanketPOLI : blanketPOlineItems) {
                    blanketPOLI.blanketPOlineItem.Blanket_Purchase_Order__c = bpo.Id;
                    blanketPOlineItemsToUpsert.add(blanketPOLI.blanketPOlineItem);
                }
                upsert blanketPOlineItemsToUpsert;
                delete(blanketPOlineItemsToDelete);
            }

            PageReference pr = new PageReference('/'+ bpo.Id);
            pr.setRedirect(true);
            return pr;
        }
        catch (DmlException ex) {

            SPCC_Utilities.logErrorOnPage(ex);
            Database.rollback(sp);
            return null;
        }
    }



    //Custom save and new method
    public PageReference saveAndNew() {
        PageReference pr;
        pr = saveBPO();
        if (pr == null) {
            return null;
        }

        pr = Page.SPCC_BPOCreate;
        pr.setRedirect(true);
        return pr;
    }



    public PageReference refresh() {
        return null;
    }



    //Check if currently logged user is assigned with CSR permission set
    public boolean getIsCSRUser(){

        if (userPermissions.isCSR == true){

            return true;
        }
        return false;
    }



    private void populateVendorAccounts() {

        vendorAccountOptionsList = new List<String>();
        vendorNameToVendorMap = new Map<String, Account>();


            for(Account vendor : [SELECT Id, Name, RecordType.DeveloperName, SAP_ID__c
                                  FROM Account
                                  WHERE RecordType.DeveloperName =: SPCC_Utilities.SPCC_VENDOR_ACCOUNT
                                  ORDER BY Name ASC]) {

                vendorAccountOptionsList.add(vendor.Name + ' (' + vendor.SAP_ID__c + ')');
                vendorNameToVendorMap.put(vendor.Name + ' (' + vendor.SAP_ID__c + ')', vendor);
            }
    }




    private void populateGLCodes() {

        glCodesOptionsList = new List<SelectOption>();
        glCodeNumberToLabelMap = new Map<String, String>();
        //glCodeSet = new Set<String>();

        List<SPCC_Misc_GL_Code_Entry__mdt> miscGLCodeList = new List<SPCC_Misc_GL_Code_Entry__mdt>();

        glCodesOptionsList.add(new SelectOption('', '- NONE -'));

        for (SPCC_GL_Code_Entry__mdt glCode : [SELECT GL_Number__c, MasterLabel
                                               FROM SPCC_GL_Code_Entry__mdt
                                               ORDER BY MasterLabel ASC ]) {

            glCodesOptionsList.add(new SelectOption(glCode.GL_Number__c,
                    glCode.GL_Number__c + ' - ' + glCode.MasterLabel));
            glCodeNumberToLabelMap.put(glCode.GL_Number__c, glCode.GL_Number__c + ' - ' + glCode.MasterLabel);
        }

        miscGLCodeList = [SELECT GL_Number__c, MasterLabel
                          FROM SPCC_Misc_GL_Code_Entry__mdt
                          ORDER BY MasterLabel ASC];

        if (miscGLCodeList != null && miscGLCodeList.Size() > 0) {

            glCodesOptionsList.add(new SelectOption('misc',' - MISCELLANEOUS -', true));

            for (SPCC_Misc_GL_Code_Entry__mdt miscGlCode : miscGLCodeList) {

                glCodesOptionsList.add(new SelectOption(miscGlCode.GL_Number__c,
                        miscGlCode.GL_Number__c + ' - ' + miscGlCode.MasterLabel));
                glCodeNumberToLabelMap.put(miscGlCode.GL_Number__c, miscGlCode.GL_Number__c + ' - ' + miscGlCode.MasterLabel);
            }
        }
    }
    


    public String vendorId {
        get{
            if(mode == ViewMode.CREATE){
                return '';
            }
            else return bpo.Vendor_Account__r.Name + ' (' + bpo.Vendor_Account__r.SAP_ID__c + ')';
        }
        set{
                if (vendorNameToVendorMap.get(value) != null){

                    bpo.Vendor_Account__c = vendorNameToVendorMap.get(value).Id;
                    bpo.Vendor_Account__r = vendorNameToVendorMap.get(value);
                }
                else{
                    bpo.Vendor_Account__c = null;
                    bpo.Vendor_Account__r = null;
                }

        }
    }


    // Add multiple Line Items to Blanket PO
    public PageReference addBPOLineItem() {
        SPCC_BPO_Line_Item__c newBPOLineItem = new SPCC_BPO_Line_Item__c();
        blanketPOlineItems.add(new BlanketPOLineItemWrapper(newBPOLineItem, blanketPOlineItems, glCodeNumberToLabelMap));
        refresh();
        return null;
    }



    public PageReference removeBPOLineItem() {
        Integer newBPOLIIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('newBPOLIIndex'));
        BlanketPOLineItemWrapper newBPOLineItemToRemove = blanketPOlineItems.get(newBPOLIIndex);
        if (newBPOLineItemToRemove.blanketPOlineItem.Id != null) {
            blanketPOlineItemsToDelete.add(newBPOLineItemToRemove.blanketPOlineItem);
        }
        blanketPOlineItems.remove(newBPOLIIndex);
        return null;
    }

    public  boolean getDisabledAddNewBPOButton(){

        if ((bpo.Name != null && bpo.Name != '') &&
                (bpo.Vendor_Account__c != null) &&
                (bpo.Validity_Start_Date__c != null) &&
                (bpo.Validity_End_Date__c != null)){

            return false;
        }

        else {

            return true;
        }
    }


    /**********************
    ** Utility Functions **
    **********************/
    private void populateExistingBPOLineItems() {
        blanketPOlineItems = new List<BlanketPOLineItemWrapper>();
        if(this.bpo.Id != null) {
            for(SPCC_BPO_Line_Item__c blanketPOlineItem : [SELECT Id, Name, Blanket_Purchase_Order__c,
                                                                  GL_Number__c, Committed_Amount__c,
                                                                  Item__c, Short_Text__c, Incurred_Cost_Rate__c
                                                           FROM SPCC_BPO_Line_Item__c
                                                           WHERE Blanket_Purchase_Order__c =: this.bpo.Id]) {
                blanketPOlineItems.add(new BlanketPOLineItemWrapper(blanketPOlineItem, blanketPOlineItems, glCodeNumberToLabelMap));
            }
        }
    }



    public Enum ViewMode {
        CREATE,
        EDIT,
        VIEW
    }



    public Class BlanketPOLineItemWrapper {
        public SPCC_BPO_Line_Item__c blanketPOlineItem {get; private set;}
        public List<BlanketPOLineItemWrapper> blanketPOlineItems;
        public Map<String, String> glCodeNumberToLabelMap;

        /*
        //m.z.: 29-March-2019 - fixed bug with renumbering line items [W-001444]
        public String getItemsOptions(Integer index) {
            Schema.DescribeFieldResult fieldResult = SPCC_BPO_Line_Item__c.Item__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            System.debug('PLE: ' + ple);
            return ple[index].getValue();
        }
        */

        public BlanketPOLineItemWrapper(SPCC_BPO_Line_Item__c blanketPOlineItem, List<BlanketPOLineItemWrapper> blanketPOlineItems, Map<String, String> glCodeNumberToLabelMap) {
            this.blanketPOlineItem = blanketPOlineItem;
            //this.blanketPOlineItem.Item__c = getItemsOptions(blanketPOlineItems.size());
            this.glCodeNumberToLabelMap = glCodeNumberToLabelMap;
        }


        public String glCode {

            get{

                return blanketPOlineItem.Id != null && glCodeNumberToLabelMap.containsKey(blanketPOlineItem.GL_Number__c.left(7)) ? blanketPOlineItem.GL_Number__c.left(7) : '';
            }

            set{

                this.blanketPOlineItem.GL_Number__c =  glCodeNumberToLabelMap.get(value);
            }
        }
    }
}