/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DOA layer interface for Contact SObject queries for general VTT purposes
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public interface HOG_ContactDAO {

    List<Contact> getActiveContactsByAccId(Id accountId);

}