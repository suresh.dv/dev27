public class StringUtilities {
    /**
    * Returns a string equivalent to the orignal without any white spaces.
    * 
    * @param original - The original string.
    *
    * @return The modified string or original if there are no white spaces.
    **/
	public static String stripWhiteSpaces(String original) {
        if (original == null) {
            return original;
        }
        
        String result = '';
        Integer length = original.length();
        for (Integer currPos = 0; currPos < length; currPos++) {
            String currXter = original.substring(currPos, (currPos + 1));
            if (!String.isBlank(currXter)) {
                result += currXter;
            }
        }
        
        return result;
    }
}