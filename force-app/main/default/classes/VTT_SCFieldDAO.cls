/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DOA layer interface for Field(AMU) SObject queries for VTT Search Criteria purposes
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public interface VTT_SCFieldDAO {

    List<Field__c> getAMUsByBusinessUnitName(String name);

}