/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VG_AlertFileService
History:        jschn 23/01/2020 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class VG_AlertFileServiceTest {

    @IsTest
    static void saveFiles_withoutParams() {
        String expectedMsg = Label.VG_File_Upload_Error;
        String errorMsg;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean expectedResult;
        Boolean result;
        String param1;
        Id param2;

        Test.startTest();
        try {
            result = new VG_AlertFileService().saveFiles(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void saveFiles_withoutParam1() {
        String expectedMsg = Label.VG_File_Upload_Error;
        String errorMsg;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean expectedResult;
        Boolean result;
        String param1;
        Id param2 = UserInfo.getUserId();

        Test.startTest();
        try {
            result = new VG_AlertFileService().saveFiles(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void saveFiles_withoutParam2() {
        String expectedMsg = Label.VG_File_Upload_Error;
        String errorMsg;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean expectedResult;
        Boolean result;
        VG_File file = new VG_File();
        file.name = 'abc';
        file.fileContents = 'abcdefghijklmnopqrstuvxyz0123456789';
        String param1 = JSON.serialize(file);
        Id param2;

        Test.startTest();
        try {
            result = new VG_AlertFileService().saveFiles(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void saveFiles_withWrongParams() {
        String expectedMsg = Label.VG_File_Upload_Error;
        String errorMsg;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean expectedResult;
        Boolean result;
        VG_File file = new VG_File();
        file.name = 'abc';
        file.fileContents = 'abcdefghijklmnopqrstuvxyz0123456789';
        String param1 = JSON.serialize(file);
        Id param2 = UserInfo.getUserId();

        Test.startTest();
        try {
            result = new VG_AlertFileService().saveFiles(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void saveFiles_withWrongParams2() {
        String expectedMsg = Label.VG_File_Upload_Error;
        String errorMsg;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        Boolean expectedResult;
        Boolean result;
        VG_File file = new VG_File();
        file.name = 'abc';
        file.fileContents = 'abcdefghijklmnopqrstuvxyz0123456789/*-+=';
        String param1 = JSON.serialize(new List<VG_File>{file});
        Id param2 = UserInfo.getUserId();

        Test.startTest();
        try {
            result = new VG_AlertFileService().saveFiles(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    static void saveFiles_withProperParams() {
        String expectedMsg;
        String errorMsg;
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Boolean expectedResult = true;
        Boolean result;
        VG_File file = new VG_File();
        file.name = 'abc';
        file.fileContents = EncodingUtil.urlEncode('abcdefghijklmnopqrstuvxyz0123456789', 'UTF-8');
        String param1 = JSON.serialize(new List<VG_File>{file});
        Id param2 = UserInfo.getUserId();

        Test.startTest();
        try {
            result = new VG_AlertFileService().saveFiles(param1, param2);
        } catch (Exception ex) {
            failFlag = true;
            errorMsg = ex.getMessage();
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(expectedMsg, errorMsg);
        System.assertEquals(expectedResult, result);
    }

}