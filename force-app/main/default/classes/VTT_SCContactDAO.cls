/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DOA layer interface for Contact SObject queries for VTT Search Criteria purposes
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public interface VTT_SCContactDAO {

    List<Contact> getContactsByAccount(Id accId);

}