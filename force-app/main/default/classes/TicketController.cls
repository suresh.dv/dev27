public with sharing class TicketController
 {
    public Boolean resolve {get;set;}
    public Boolean close {get;set;}
    public String ticketAction {get;set;}
    public Ticket__c ticketRecord {get; set;}
    public ApexPages.StandardController controller{get;set;}
    public TicketController(ApexPages.StandardController stdController) 
    {
        controller = stdController;
        if (!Test.isRunningTest())  
            controller.addFields(new List<String>{'Ticket_Stage__c', 'Ticket_Resolution_Date__c', 'Ticket_Closed_Date__c', 'Reason__c'});
        ticketRecord = (Ticket__c)controller.getRecord();
        String action = ApexPages.CurrentPage().getParameters().get('action');
        if(action=='resolve')
        {
            resolve = true;
            close = false;
            ticketAction = 'Resolve Ticket';
        }
        else if(action=='close')
        {
            resolve = false;
            close = true;
            ticketAction = 'Close Ticket';
        }
    }
    public PageReference saveTicket()
    {
        if(ticketAction == 'Resolve Ticket')
        {
            ticketRecord.Ticket_Stage__c = 'Closed - Pending Client Approval';
            ticketRecord.Ticket_Resolution_Date__c = System.today();            
        }
        else if(ticketAction == 'Close Ticket')
        {
            ticketRecord.Ticket_Stage__c = 'Completed';
            ticketRecord.Ticket_Closed_Date__c = System.today();
        }
        Database.update(ticketRecord);
        //controller.save();
        return controller.view();
    }

}