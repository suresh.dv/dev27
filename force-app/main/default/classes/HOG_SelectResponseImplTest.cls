/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_SelectResponseImpl
History:        jschn 2019-01-30 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_SelectResponseImplTest {

    @IsTest
    static void testConstructorWithObject() {
        Account acc = new Account(Name = 'test account');

        Test.startTest();
        HOG_SelectResponse response = new HOG_SelectResponseImpl(acc);
        Test.stopTest();

        System.assert(response.success);
        System.assertNotEquals(null, response.resultObjects);
        System.assertEquals(null, response.errors);
        System.assertEquals(1, response.resultObjects.size());
        System.assertEquals(acc.Name, ((Account)response.resultObjects.get(0)).Name);
    }

    @IsTest
    static void testConstructorWithListOfObjects() {
        Account acc1 = new Account(Name = 'test account1');
        Account acc2 = new Account(Name = 'test account2');
        List<Account> accounts = new List<Account>{acc1, acc2};

        Test.startTest();
        HOG_SelectResponse response = new HOG_SelectResponseImpl(accounts);
        Test.stopTest();

        System.assert(response.success);
        System.assertNotEquals(null, response.resultObjects);
        System.assertEquals(null, response.errors);
        System.assertEquals(2, response.resultObjects.size());
        System.assertEquals(acc1.Name, ((Account)response.resultObjects.get(0)).Name);
        System.assertEquals(acc2.Name, ((Account)response.resultObjects.get(1)).Name);
    }

    @IsTest
    static void testConstructorWithErrorString() {
        String error = 'Test error';

        Test.startTest();
        HOG_SelectResponse response = new HOG_SelectResponseImpl(error);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(error, response.errors.get(0));
    }

    @IsTest
    static void testConstructorWithException() {
        String error = 'Test error';
        TestException ex = new TestException(error);

        Test.startTest();
        HOG_SelectResponse response = new HOG_SelectResponseImpl(ex);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(1, response.errors.size());
        System.assertEquals(error, response.errors.get(0));
    }

    @IsTest
    static void testAddResult() {
        Account acc1 = new Account(Name = 'test account1');
        Account acc2 = new Account(Name = 'test account2');

        Test.startTest();
        HOG_SelectResponseImpl response = new HOG_SelectResponseImpl(acc1);
        response.addResult(new List<Account>{acc2});
        Test.stopTest();

        System.assert(response.success);
        System.assertNotEquals(null, response.resultObjects);
        System.assertEquals(null, response.errors);
        System.assertEquals(2, response.resultObjects.size());
        System.assertEquals(acc1.Name, ((Account) response.resultObjects.get(0)).Name);
        System.assertEquals(acc2.Name, ((Account) response.resultObjects.get(1)).Name);
    }

    @IsTest
    static void testAddErrorWithException() {
        String error1 = 'Test error1';
        String error2 = 'Test error2';
        TestException ex1 = new TestException(error1);
        TestException ex2 = new TestException(error2);

        Test.startTest();
        HOG_SelectResponseImpl response = new HOG_SelectResponseImpl(ex1);
        response.addError(ex2);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(2, response.errors.size());
        System.assertEquals(error1, response.errors.get(0));
        System.assertEquals(error2, response.errors.get(1));
    }

    @IsTest
    static void testAddErrorWithErrorMsg() {
        String error1 = 'Test error1';
        String error2 = 'Test error2';

        Test.startTest();
        HOG_SelectResponseImpl response = new HOG_SelectResponseImpl(error1);
        response.addError(error2);
        Test.stopTest();

        System.assert(!response.success);
        System.assertEquals(null, response.resultObjects);
        System.assertNotEquals(null, response.errors);
        System.assertEquals(2, response.errors.size());
        System.assertEquals(error1, response.errors.get(0));
        System.assertEquals(error2, response.errors.get(1));
    }

    public class TestException extends Exception {}

}