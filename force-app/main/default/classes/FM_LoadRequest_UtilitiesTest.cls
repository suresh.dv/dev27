@isTest
private class FM_LoadRequest_UtilitiesTest {

	@isTest static void testTriggerMethods() {
		//Get setup data
		Facility__c facility = [Select Id, Name From Facility__c].get(0);
		Location__c location = [Select Id, Name From Location__c].get(0);
		Carrier__c carrier = [Select Id, Name From Carrier__c].get(0);

		//Create LRs for testing if trucktrips created
		List<FM_Load_Request__c> lrList = new List<FM_Load_Request__c>();
		for(Integer i=1; i <= 5; i++) {
			FM_Load_Request__c lr = new FM_Load_Request__c();
			lr.Standing_Comments__c = 'Test comment ' + i;
			lr.Axle__c = 'T6X';
			lr.Load_Weight__c = 'Primary';
			lr.Act_Flow_Rate__c = 5;
			lr.Flowline_Volume__c = 10;
			lr.Carrier__c = carrier.Id;
			lr.Source_Facility__c = (i == 1 || i == 2) ? facility.id : null;
			lr.Source_Location__c = (i <> 1 && 1 <> 2) ? location.id : null;
			lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
			lr.Shift__c = 'Day';
			lr.Product__c = 'W';
			lr.Load_Type__c = 'Standard Load';
			lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
			lrList.add(lr);
		}
		insert lrList;

		//Test if trucktrips created with new status and test name of lr
		for(FM_Load_Request__c lr : [Select Id, Name, Source_Location__c, Source_Facility__c,
											Source_Facility__r.Name, Source_Location__r.Name,
											Tank_Label__c,
											(Select Id, Name, Truck_Trip_Status__c 
											 From Truck_Trips__r)
									 From FM_Load_Request__c]) {
			//Check name

			String expectedName = (lr.Source_Location__c <> null) ? lr.Source_Location__r.Name : lr.Source_Facility__r.Name;
			expectedName += ' / ' + lr.Tank_Label__c + ' - ' + Date.today().format();
			System.assertEquals(lr.Name, expectedName);

			//Check trucktrips
			System.assertEquals(lr.Truck_Trips__r.size(), 1);
			System.assertEquals(lr.Truck_Trips__r[0].Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_NEW);
		}


		//Dispatch a trucktrip and try to cancel load requested associated (Should error!)
		FM_Truck_Trip__c tt = [SELECT Id, Truck_Trip_Status__c, Load_Request__c FROM FM_Truck_Trip__c LIMIT 1].get(0);
		tt.Truck_Trip_Status__c = FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED;
		update tt;
		List<FM_Load_Request__c> lr = [SELECT Id,Name, Status__c FROM FM_Load_Request__c Where Id =: tt.Load_Request__c];
		lr[0].Cancel_Reason__c = FM_LoadRequest_Utilities.getCancelReasons().get(0).getLabel();
		lr[0].Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED;
		try {
			update lr[0];
		} catch (DmlException ex) {
			System.assert(ex.getMessage().contains('Cannot cancel load request ' + lr[0].Name + '. Since it has a dispatched truck trip assigned to it. Need to cancel the dispatched truck trip from Truck Lists tab to cancel this Load Request.'));
		}
		
		//Cancel load request and check if truck trip cancelled
		
		lrList[0].Cancel_Reason__c = FM_LoadRequest_Utilities.getCancelReasons().get(0).getLabel();
		lrList[0].Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_CANCELLED;
		update lrList[0];
		FM_Truck_Trip__c associatedTruckTrip = [Select Id, Name, Truck_Trip_Status__c
												From FM_Truck_Trip__c
												Where Load_Request__c =: lrList[0].Id];
		System.assertEquals(associatedTruckTrip.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_CANCELLED);

		//Create Load Confirmation for dispatched trucktrips
		List<FM_Load_Confirmation__c> loadConfirmations = new List<FM_Load_Confirmation__c>();
		for(FM_Truck_Trip__c truckTrip : [Select Id, Name, Load_Request__c
								   From FM_Truck_Trip__c
								   Where Truck_Trip_Status__c =: FM_Utilities.TRUCKTRIP_STATUS_DISPATCHED]) {
			FM_Load_Confirmation__c lc = new FM_Load_Confirmation__c();
			lc.Truck_Trip__c = truckTrip.Id;
			lc.Ticket_Number__c = '90210';
			lc.Scale_Ticket_Number__c = '90211';
			lc.Volume__c = 20;
			lc.Product__c = 'O';
			lc.Destination_Location__c = location.Id;
			loadConfirmations.add(lc);
		}
		insert loadConfirmations;
		FM_Truck_Trip__c truckTrip = [Select Id, Name, Truck_Trip_Status__c
							   		  From FM_Truck_Trip__c
							   		  Where Id =: loadConfirmations[0].Truck_Trip__c];
		System.assertEquals(truckTrip.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_COMPLETED);
	}

	@isTest static void testPicklistMethods() {
		List<SelectOption> locations = FM_LoadRequest_Utilities.GetLocationOptionsForLoadRequests([SELECT Id FROM Route__c LIMIT 1].get(0).Id);
		//NOTE Not sure why I get 0 locations... its probably something with bad data in testsetup... need to check
		System.assertEquals(0, locations.size());
		List<SelectOption> cancelReasons = FM_LoadRequest_Utilities.getCancelReasons();
		List<SelectOption> cReasons = new List<SelectOption>();
		for (Schema.PicklistEntry ple : FM_Load_Request__c.Cancel_Reason__c.getDescribe().getPickListValues())
            cReasons.add(new SelectOption(ple.getLabel(), ple.getValue()));
		System.assertEquals(cReasons.size(), cancelReasons.size());
	}

	@testSetup
	static void createTestData() {
		Account acc = new Account();
		acc.Name = 'MI6 Account';
		insert acc;

		Carrier__c carrier = new Carrier__c();
		carrier.Carrier__c = acc.Id;
		carrier.Carrier_Name_For_Fluid__c = 'Husky Bond';
		insert carrier;

		carrier = new Carrier__c();
		carrier.Carrier__c = acc.Id;
		carrier.Carrier_Name_For_Fluid__c = 'James Bond';
		insert carrier;

		Carrier_Unit__c carrierUnit = new Carrier_Unit__c();
		carrierUnit.Carrier__c = carrier.Id;
		carrierUnit.Unit_Email__c = 'bond@jamesbond.com';
		carrierUnit.Name = 'Bond1';
		carrierUnit.Enabled__c = true;
		insert carrierUnit;

		Business_Department__c business = new Business_Department__c();
		business.Name = 'Husky Business';
		insert business;

		Operating_District__c district = new Operating_District__c();
		district.Name = 'District 9';
		district.Business_Department__c = business.Id;
		insert district;

		Field__c field = new Field__c();
		field.Name = 'AMU Field';
		field.Operating_District__c = district.Id;
		insert field;

		Route__c route = new Route__c();
		route.Fluid_Management__c = true;
		route.Name = '007';
		route.Route_Number__c = '007';
		insert route;

		Location__c loc = new Location__c();
		loc.Name = 'London';
		loc.Surface_Location__c = 'London';
		loc.Fluid_Location_Ind__c = true;
		loc.Location_Name_for_Fluid__c = 'London HQ';
		loc.Operating_Field_AMU__c = field.Id;
		loc.Route__c = route.Id;
		loc.Functional_Location_Category__c	= 4;
		insert loc;

		Equipment__c eq = EquipmentTestData.createEquipment(loc);
		eq.Equipment_Category__c = 'D';
		eq.Object_Type__c = 'VESS_ATMOS';
		update eq;

		Equipment_Tank__c eqt = new Equipment_Tank__c();
		eqt.Equipment__c = eq.id;
		eqt.Low_Level__c = 10;
		eqt.Tank_Size_m3__c = 30;
		eqt.Tank_Label__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		insert eqt;

		List<FM_Load_Request__c> lrList = new List<FM_Load_Request__c>();

		FM_Load_Request__c lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment';
		
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		Location__c loc2 = new Location__c();
		loc2.Name = 'Brezno';
		loc2.Fluid_Location_Ind__c = true;
		loc2.Location_Name_for_Fluid__c = 'Brezno HQ';
		loc2.Operating_Field_AMU__c = field.Id;
		loc2.Route__c = route.Id;
		loc2.Functional_Location_Category__c = 4;
		insert loc2;

		//Create load request for nonRunSheet test
		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 2';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Location__c = loc2.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		Facility__c fac = new Facility__c();
		fac.Name = 'Test fac';
		fac.Facility_Name_for_Fluid__c = 'Tst Facilty';
		fac.Fluid_Facility_Ind__c = true;
		fac.Fluid_Facility_Ind_Origin__c = true;
		fac.Operating_Field_AMU__c = field.Id;
		fac.Functional_Location_Category__c	= 3;
		fac.Plant_Section__c = route.id;

		insert fac;

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Night';
		lr.Product__c = 'O';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		lr = new FM_Load_Request__c();
		lr.Standing_Comments__c = 'Test comment 3';
		lr.Axle__c = 'T6X';
		lr.Load_Weight__c = 'Primary';
		lr.Act_Flow_Rate__c = 5;
		lr.Flowline_Volume__c = 10;
		lr.Carrier__c = carrier.Id;
		lr.Source_Facility__c = fac.id;
		lr.Status__c = FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED;
		lr.Shift__c = 'Day';
		lr.Product__c = 'W';
		lr.Load_Type__c = 'Standard Load';
		lr.Tank__c = Equipment_Tank__c.Tank_Label__c.getDescribe().getPicklistValues().get(0).getLabel();
		lrList.add(lr);

		insert lrList;
	}

}