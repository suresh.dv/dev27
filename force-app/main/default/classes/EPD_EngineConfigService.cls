/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service class containing all methods and business logic regarding EPD_Engine__mdt SObject
Test Class:     EPD_EngineConfigServiceTest
History:        jschn 2019-05-25 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_EngineConfigService {

    private static final String CLASS_NAME = String.valueOf(EPD_EngineConfigService.class);

    /**
     * Retrieves Engine Config for selected manufacturer and model
     *
     * @param manufacturer
     * @param model
     *
     * @return EPD_Engine__mdt
     */
    public EPD_Engine__mdt getEngineConfig(String manufacturer, String model) {

        validateGetEngineConfigInputs(manufacturer, model);

        System.debug(CLASS_NAME + ' -> getEngineConfig. Manufacturer: ' + manufacturer + ' Model: ' + model);

        List<EPD_Engine__mdt> configs = EPD_DAOProvider.engineConfigDAO.getEngineConfig(manufacturer, model);

        return handleResponse(configs, manufacturer, model);
    }

    /**
     * Handles query results for getEngineConfig method call. It returns record if proper number of records are provided.
     * Throws and error if not.
     *
     * @param engineConfigs
     * @param manufacturer
     * @param model
     *
     * @return EPD_Engine__mdt
     */
    private EPD_Engine__mdt handleResponse(List<EPD_Engine__mdt> engineConfigs, String manufacturer, String model) {
        EPD_Engine__mdt config = null;

        System.debug(CLASS_NAME + ' -> getEngineConfig. Engine Configs: ' + JSON.serialize(engineConfigs));

        if(isSingleConfig(engineConfigs)) {

            config = engineConfigs.get(0);

        } else {

            handleGetEngineConfigCountError(manufacturer, model);

        }

        return config;
    }

    /**
     * Builds a map from all Engine configurations in the system. Map key is combination of manufacturer and model.
     *
     * @return Map<String, EPD_Engine__mdt>
     */
    public Map<String, EPD_Engine__mdt> getConfigsMap() {
        Map<String, EPD_Engine__mdt> configMap = new Map<String, EPD_Engine__mdt>();
        for(EPD_Engine__mdt engine : EPD_DAOProvider.engineConfigDAO.getEngineConfigs()) {
            configMap.put(
                    getConfigMapKeyForEngine(engine.Manufacturer__c, engine.Model__c),
                    engine
            );
        }

        return configMap;
    }

    /**
     * Method that builds key for Engine config map.
     *
     * @param manufacturer
     * @param model
     *
     * @return String
     */
    public String getConfigMapKeyForEngine(String manufacturer, String model) {
        return manufacturer + '#' + model;
    }

    /**
     * Method that is used when inappropriate number of Engine config records are provided.
     *
     * @param manufacturer
     * @param model
     */
    private void handleGetEngineConfigCountError(String manufacturer, String model) {
        System.debug(CLASS_NAME + ' getEngineConfig. Couldn\'t find proper engine config.');
        HOG_ExceptionUtils.throwError(
                Label.EPD_Unexpected_Number_of_Engine_Configs,
                new List<String> {
                        manufacturer,
                        model
                }
        );
    }

    /**
     * Validate inputs for getEngineConfig.
     *
     * @param manufacturer
     * @param model
     */
    private void validateGetEngineConfigInputs(String manufacturer, String model) {
        if(String.isBlank(manufacturer)) {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Missing_Param,
                    new List<String> { 'Engine Manufacturer' }
            );
        }
        if(String.isBlank(model)) {
            HOG_ExceptionUtils.throwError(
                    Label.EPD_Missing_Param,
                    new List<String> { 'Engine Model' }
            );
        }
    }

    /**
     * Check if appropriate number of records are provided.
     *
     * @param engineConfigs
     *
     * @return Boolean
     */
    private Boolean isSingleConfig(List<EPD_Engine__mdt> engineConfigs) {
        return engineConfigs != null && engineConfigs.size() == 1;
    }

}