/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Handler class that handles basic logic from EPD_Endpoint requests.
Test Class:     EPD_EndpointHandlerTest
                EPD_EndpointHandlerTestNP
History:        jschn 2019-05-25 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_EndpointHandler {

    private static final String CLASS_NAME = String.valueOf(EPD_EndpointHandler.class);

    /**
     * Handles save requests from Endpoint. Creates database savepoint, deserialize payload (form structure),
     * and runs persistence layer to persist records.
     * This method in 2 modes. Submit and Save. If run in submit mode, it will also run email service.
     *
     * @param formStructure
     * @param saveMode
     *
     * @return EPD_Response
     */
    public EPD_Response handleSave(String formStructure, EPD_SaveMode saveMode) {
        EPD_Response response;

        if (String.isNotBlank(formStructure)) {

            Savepoint sp = Database.setSavepoint();

            try {
                System.debug(CLASS_NAME + ' -> handleSave. FormStructure: ' + formStructure);

                EPD_FormStructure data = (EPD_FormStructure) JSON.deserialize(formStructure.trim(), EPD_FormStructure.class);

                System.debug(CLASS_NAME + ' -> handleSave. deserialized data: ' + JSON.serialize(data));

                if(EPD_Constants.SAVE_MODE_TO_BOOLEAN_MAP.get(saveMode)) {
                    data.epd.submitted = EPD_Constants.SAVE_MODE_TO_BOOLEAN_MAP.get(saveMode);
                }

                System.debug(CLASS_NAME + ' -> handleSave. data: ' + JSON.serialize(data));

                response = new EPD_FormService().handleFormSave(data);

                if(EPD_Constants.SAVE_MODE_TO_BOOLEAN_MAP.get(saveMode) && response.success) {
                    sendNotificationEmails((EPD_Engine_Performance_Data__c) response.formStructure.epd.getRecord());
                }

            } catch (Exception ex) {
                System.debug(CLASS_NAME + ' -> handleSave. Exception: ' + ex.getMessage());
                System.debug(CLASS_NAME + ' -> handleSave. Exception: ' + ex.getStackTraceString());
                response = new EPD_Response(ex);
                Database.rollback(sp);
            }

        } else {
            response = new EPD_Response(
                    String.format(
                            Label.EPD_Missing_Param,
                            new List<String> {
                                    'Form Structure'
                            }
                    )
            );
        }

        return response;
    }

    /**
     * Sends notifications for Reliability engineers and vendor.
     * Content of method is in try catch to avoid rollback if any exception occurs while sending email.
     *
     * @param epd
     */
    private void sendNotificationEmails(EPD_Engine_Performance_Data__c epd) {
        Database.executeBatch(
                new EPD_EmailBatch(
                        new EPD_EmailNotificationOnSubmitPDF(),
                        new List<EPD_Engine_Performance_Data__c>{
                                epd
                        }
                ),
                10
        );
        new EPD_EPDService().sendThresholdAlertsForWOs(
                new Set<Id> {epd.Work_Order__c}
        );
    }

    /**
     * Handles load request from endpoint. Works also in 2 modes. Direct and Indirect mode.
     * Direct mode loads form based on EPD Id.
     * Indirect mode loads form based on Work Order Activity Id. In this mode there are two ways which form is loaded.
     * First it tries if there is any unfinished EPD form and if not, it creates EPD Form based on WOA record.
     *
     * @param recordId
     * @param loadMode
     *
     * @return EPD_Response
     */
    public EPD_Response handleLoad(Id recordId, EPD_LoadMode loadMode) {
        EPD_Response response;

        if(String.isNotBlank(recordId)) {

            try {
                System.debug(CLASS_NAME + ' -> handleLoad. For recordId: ' + recordId);
                System.debug(CLASS_NAME + ' -> handleLoad. Load Mode: ' + String.valueOf(loadMode));

                List<EPD_Engine_Performance_Data__c> epdRecords = getEPDRecords(recordId, loadMode);

                response = loadEPDForm(epdRecords, loadMode, recordId);

            } catch (Exception ex) {
                System.debug(CLASS_NAME + ' -> handleLoad. Exception: ' + ex.getMessage());
                response = new EPD_Response(ex);
            }

        } else {
            response = new EPD_Response(EPD_Constants.MISSING_PARAM_BY_LOAD_MODE.get(loadMode));
        }

        return response;
    }

    /**
     * This method tries to load EPD record based on ID. Method of EPD record loading is based on Load Mode.
     *
     * @param recordId
     * @param loadMode
     *
     * @return List<EPD_Engine_Performance_Data__c>
     */
    private List<EPD_Engine_Performance_Data__c> getEPDRecords(Id recordId, EPD_LoadMode loadMode) {
        List<EPD_Engine_Performance_Data__c> epdRecords;

        if(EPD_LoadMode.DIRECT_LOAD.equals(loadMode)) {

            epdRecords = EPD_DAOProvider.EPDDAO.getEPDRecords(new Set<Id> { recordId });

        } else if (EPD_LoadMode.INDIRECT_LOAD.equals(loadMode)) {

            epdRecords = EPD_DAOProvider.EPDDAO.getEnginePerformanceDataByWOAId(recordId);

        }

        return epdRecords;
    }

    /**
     * This method decides how to load EPD Form into structure based on provided params.
     * First it checks if epdRecords list has exactly one record. If not it generates EPD Form from scratch.
     * If yes, it will use that one record to build EPD Form for response.
     *
     * @param epdRecords
     * @param loadMode
     * @param recordId
     *
     * @return EPD_Response
     */
    private EPD_Response loadEPDForm(List<EPD_Engine_Performance_Data__c> epdRecords, EPD_LoadMode loadMode, Id recordId) {
        EPD_Response response;

        if (EPD_Utility.isSingleRecord(epdRecords)) {

            response = new EPD_FormService().handleLoadForExistingEPD(epdRecords.get(0), loadMode);

        } else if (EPD_LoadMode.INDIRECT_LOAD.equals(loadMode) && EPD_Utility.isEmptyList(epdRecords)) {

            response = new EPD_FormService().handleLoadForNewEPD(recordId);

        } else {

            response = new EPD_Response(EPD_Constants.UNEXPECTED_NUM_OF_RECS_BY_LOAD_MODE.get(loadMode));

        }

        return response;
    }

}