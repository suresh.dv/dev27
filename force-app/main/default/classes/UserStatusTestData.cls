/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the User_Status__c
-------------------------------------------------------------------------------------------------*/
@isTest                
public class UserStatusTestData
{
    public static HOG_User_Status__c createUserStatus(String name, String description)
    {
        HOG_User_Status__c results = new HOG_User_Status__c
            (
                Name = name,
                Description__c = description
            );

        return results;
    }
}