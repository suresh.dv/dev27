public with sharing class SPCC_POControllerX {

    public SPCC_Purchase_Order__c po {get; set;}    
    private ApexPages.StandardController stdController;
    @TestVisible private SPCC_Authorization_for_Expenditure__c parent;
    public List<LineItemWrapper> lineItems {get; private set; }
    public List<SPCC_Line_Item__c> lineItemsToDelete;

    //URL Parameters
    public Boolean createdFromAFE {get; set;}
    public Boolean isNewPO {get; set;}
    
    public Boolean ewrSelectedFromDropdown {get; set;}

    //User Security
    //public SPCC_Utilities.UserPermissions userPermissions;
    public SPCC_Utilities.UserPermissions userPermissions {get; set;}
    public SPCC_Utilities.UserDetails userDetails;

    //Page Mode
    private ViewMode mode;

    //Constructor
    public SPCC_POControllerX(ApexPages.StandardController controller) {
        stdController = controller;
        if(!test.isRunningTest()){
            stdController.addFields(new List<String>{'Name', 'Authorization_for_Expenditure__c', 'Cost_Code__c', 
                                                     'Description__c', 'Engineering_Work_Request__c',
                                                     'Purchase_Order_Number__c', 'Status__c', 'Vendor_Account__c', 'Estimate_to_Complete__c'});
        }
        
        this.isNewPO = false;
        
        this.po = (SPCC_Purchase_Order__c)controller.getRecord();
        List<SPCC_Authorization_for_Expenditure__c> queryResult = [Select Id, Name, Engineering_Work_Request__c
                                                                   From SPCC_Authorization_for_Expenditure__c
                                                                   Where Id =: po.Authorization_for_Expenditure__c];
        this.parent = (queryResult != null && !queryResult.isEmpty()) ? queryResult.get(0) : null;
        this.createdFromAFE = (this.parent != null);

        this.userPermissions = new SPCC_Utilities.UserPermissions();
        this.userDetails = new SPCC_Utilities.UserDetails();

        //Set Mode
        mode = (po.Id == null) ? ViewMode.CREATE : ViewMode.EDIT;

        //New PO created from AFE 
        if(this.createdFromAFE) {
            po.Engineering_Work_Request__c = this.parent.Engineering_Work_Request__c;
            //Keep reference to afe and wer in url parameters if new PO is created from AFE page.
        }
        
        //property is referenced in vf page to diplsay required mark next to ewr field if it's new po only
        if (po.Id == null && po.Engineering_Work_Request__c == null){
            
            this.isNewPO = true;
        }
        
        
        this.ewrSelectedFromDropdown = false;
        // MJ Add multiple Line Items to PO
        populateExistingLineItems();
        // MJ create list to hold list of Line Items that should be removed
        lineItemsToDelete = new List<SPCC_Line_Item__c>();
    }

    //Custom save method
    public ApexPages.PageReference savePO() {
        
        Savepoint sp = Database.setSavepoint();

        //No Line Items Specified
        if(lineItems == null || lineItems.isEmpty()) {
            SPCC_Utilities.logErrorOnPage('No line items added to the Purchase Order. At least one line item is required.');
            return null;
        }

        try {
            /* 
                combination of inserting and updating records based on ViewMode.CREATE has been used because simple upsert caused error,
                when user tried to create a record (that violated validation),
                and after correcting error-causing fields, the record ID was preserved which caused upsert error
            */
            
            if(mode == ViewMode.CREATE) {
                po.Id = null; 
                insert po;
            } else update po;
            
            // Then save all Line Items if not CSR
            
            if (getIsCSRUser() == false){
                
                List<SPCC_Line_Item__c> lineItemsToUpsert = new List<SPCC_Line_Item__c>();
                
                for (LineItemWrapper li : lineItems) {
                    li.lineItem.Purchase_Order__c = po.Id;
                    lineItemsToUpsert.add(li.lineItem);
                }

                upsert lineItemsToUpsert;
                delete(lineItemsToDelete);
            }
            
            PageReference pr = new PageReference('/'+ po.Id);
            pr.setRedirect(true);
            return pr;
        }
        catch (DmlException ex) {
            SPCC_Utilities.logErrorOnPage(ex);
            Database.rollback(sp);
            return null;
        }
    }

    //Custom save and new method
    public PageReference saveAndNew() {  
        PageReference pr;
        pr = savePO();
        if (pr == null) {
            return null;
        }
        
        pr = Page.SPCC_POCreate;
        pr.setRedirect(true);    
        return pr;
    }  



    //Populate EWR picklist
    public List<SelectOption>  getEWRs(){
        
        List<SelectOption> soEWRs = new List<SelectOption>();
        List<SPCC_Engineering_Work_Request__c> lEWRs = new List<SPCC_Engineering_Work_Request__c>();

            soEWRs.add(new SelectOption('', '- NONE -')); 

        //Load EWR Picklist Values
        if (po.Name != null){

            lEWRs = [SELECT Id, Name, Project_Name__c, Start_Date__c, Status__c
                     FROM SPCC_Engineering_Work_Request__c
                     ORDER BY Name ASC];
           
            for (SPCC_Engineering_Work_Request__c ewr : lEWRs) {

                soEWRs.add(new SelectOption(ewr.Id, ewr.Name));

            } 
                         
        }

        return soEWRs;
    } 


 

    //Populate AFE picklist   
    public List<SelectOption> getAFEs(){
           
        List<SelectOption> soAFEs = new List<SelectOption>();

            soAFEs.add(new SelectOption('', '- NONE -'));
    
        //Load related AFE Picklist Values
        if (po.Engineering_Work_Request__c != null){

        
                List<SPCC_Authorization_for_Expenditure__c> lAFEs = [SELECT Id, Name, Engineering_Work_Request__c, AFE_Number__c,
                                                                            Approved_LTD_Gross__c, Description__c
                                                                     FROM SPCC_Authorization_for_Expenditure__c
                                                                     WHERE Engineering_Work_Request__c =: po.Engineering_Work_Request__c];
        
                for (SPCC_Authorization_for_Expenditure__c afe : lAFEs) {

                    soAFEs.add(new SelectOption(afe.Id, afe.AFE_Number__c));

                } 

        }
                                      
        return soAFEs;    
    }



    //Populate GL picklist
    public List<SelectOption> glOptions {
        get {
            if(glOptions == null || glOptions.isEmpty()) {
                glOptions = new List<SelectOption>();
                glOptions.add(new SelectOption('', '- NONE -'));

                //Load related GL Codes Picklist Values
                if (po.Engineering_Work_Request__c != null && po.Authorization_for_Expenditure__c != null){
                    for (SPCC_Cost_Element__c gl : [SELECT Id, Name, GL_Number__c, Label__c, AFE__c 
                                                   FROM SPCC_Cost_Element__c
                                                   WHERE AFE__c =: po.Authorization_for_Expenditure__c
                                                   ORDER BY Label__c ASC]) {
                        glOptions.add(new SelectOption(gl.Id, gl.GL_Number__c + (gl.Label__c != null ? + ' - ' + gl.Label__c : '')));
                    }   
                }
            }   
                                          
            return glOptions; 
        }
        private set;    
    }



    //Populate Cost Code picklist
    //public List<SelectOption> getCostCodes(){
           
    //    List<SelectOption> soCCs = new List<SelectOption>();
    //    soCCs.add(new SelectOption('', '- NONE -'));

    //    //Load related Cost Codes Picklist Values
    //    if (po.Engineering_Work_Request__c != null && po.Authorization_for_Expenditure__c != null && po.Cost_Element__c != null){

    //    List<SPCC_Cost_Code__c> lCCs = [SELECT Id, Name, Cost_Code_Number__c, GL_Account__c 
    //                                    FROM SPCC_Cost_Code__c
    //                                    WHERE GL_Account__c =: po.Cost_Element__c];
        
        
    //        for (SPCC_Cost_Code__c cc : lCCs) {

    //            soCCs.add(new SelectOption(cc.Id, cc.Cost_Code_Number__c));
    //        }   

    //    }
                                      
    //    return soCCs;    
    //}    
    
    //Check if currently logged user is assigned with EPC permission set only
    public boolean getIsEPCOnly(){

        if (userPermissions.isEPC == true &&
            userPermissions.isAdmin == false &&
            userPermissions.isProjectController == false &&
            userPermissions.isProjectLead == false &&
            userPermissions.isCSR == false){

            return true;
        }

        return false;
    }
    
    //Check if currently logged user is assigned with CSR permission set
    public boolean getIsCSRUser(){
        
       if (userPermissions.isCSR == true){
           
           return true;
       }
       
       return false;
    }


     
    //Populate Vendor picklist
    public List<SelectOption> getVendorAccounts(){

        List<SelectOption> soVAs = new List<SelectOption>();
        List<SPCC_Vendor_Account_Assignment__c> lVAA = new List<SPCC_Vendor_Account_Assignment__c>();

        if (po.Engineering_Work_Request__c == null) {
            soVAs.add(new SelectOption('', '- NONE -'));
        } 
        else {
            //Load all related Vendor Picklist values
            lVAA = [SELECT Id, Engineering_Work_Request__c, Vendor_Account__c, Vendor_Account__r.Name 
                    FROM SPCC_Vendor_Account_Assignment__c
                    WHERE Engineering_Work_Request__c =: po.Engineering_Work_Request__c];

            soVAs.add(new SelectOption('', '- NONE -'));        
        }
                                      
        
        for(SPCC_Vendor_Account_Assignment__c a :lVAA){

            if(a.Vendor_Account__c != null){
                             
                soVAs.add(new SelectOption(a.Vendor_Account__c, a.Vendor_Account__r.Name));     
            }
        }
        
        return soVAs;
    }   


    //Null depended picklist values if controlled picklist is nulled 
    public  PageReference updateDropDowns(){

       if (po.Engineering_Work_Request__c == null){

            po.Authorization_for_Expenditure__c = null;
            po.Cost_Code__c = null;
            po.Vendor_Account__c = null;

       }

       else if (po.Authorization_for_Expenditure__c == null){
            po.Cost_Code__c = null;
       }
       return null;
    }
    
        public PageReference ewrSelected() {
            
        ewrSelectedFromDropdown = true;
        
        return null;
    }
    
    // Add multiple Line Items to PO
    public PageReference addLineItem() {
        //if we would like to prepopulate new LI with next LI number based on index
        //SPCC_Line_Item__c newLineItem = new SPCC_Line_Item__c(Item__c = getItemsOptions(lineItems.size()));
        SPCC_Line_Item__c newLineItem = new SPCC_Line_Item__c();
        lineItems.add(new LineItemWrapper(newLineItem, lineItems));
        return null;
    }
    
    public PageReference removeLineItem() {
        Integer newLIIndex = Integer.valueOf(ApexPages.currentPage().getParameters().get('newLIIndex'));
        LineItemWrapper newLineItemToRemove = lineItems.get(newLIIndex);
        if (newLineItemToRemove.lineItem.Id != null) {
            lineItemsToDelete.add(newLineItemToRemove.lineItem);
        }
        lineItems.remove(newLIIndex);
        return null;
    }
    

   

    /**********************
    ** Utility Functions **
    **********************/
    private void populateExistingLineItems() {
        lineItems = new List<LineItemWrapper>();
        if(this.po.Id != null) {
            for(SPCC_Line_Item__c lineItem : [Select Id, Name, Cost_Element__c, Purchase_Order__c,
                                                     Cost_Element__r.GL_Number__c, Committed_Amount__c,
                                                     Item__c, Short_Text__c, Incurred_Cost_Rate__c, Incurred_Costs_Flag__c
                                              From SPCC_Line_Item__c
                                              Where Purchase_Order__c =: this.po.Id]) {
                lineItems.add(new LineItemWrapper(lineItem, lineItems));
            }
        }
    }

    public Enum ViewMode {
        CREATE,
        EDIT,
        VIEW
    }

    /*
    public String getItemsOptions(Integer index) {
        Schema.DescribeFieldResult fieldResult = SPCC_Line_Item__c.Item__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        System.debug('PLE: ' + ple);
        return ple[index].getValue();
    }
    */

    
    public Class LineItemWrapper {
        public SPCC_Line_Item__c lineItem {get; private set;}
        public List<LineItemWrapper> lineItems;
    
        public LineItemWrapper(SPCC_Line_Item__c lineItem, List<LineItemWrapper> lineItems) {
            this.lineItem = lineItem;
        }
    }
}