@isTest
private class ATSCustomerAttachmentTriggerHelperTest {
    static testmethod void atsCreationTest() {
        Account acc = new Account( Name = 'Test' );
        insert acc;
        
        Contact con = new Contact( LastName = 'Test', AccountId = acc.Id );
        insert con;
        
        Attachment attach = new Attachment( Name = 'Test', ParentId = acc.Id, Body = Blob.valueOf( 'test' ));
        insert attach;
        
        ATS_Customer_Attachment_Junction__c ats = new ATS_Customer_Attachment_Junction__c( ATS_Customer__c = con.Id, Attachment_Id__c = attach.Id );
        
        test.startTest();
            insert ats;
            
            ats = [ Select Id, Attachment_Name__c from ATS_Customer_Attachment_Junction__c where Id = :ats.Id ];
            system.assertEquals( 'Test', ats.Attachment_Name__c );
        test.stopTest();
    }
}