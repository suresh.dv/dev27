/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class HOG_SObject_Information {
	@AuraEnabled
	public SObject floc;
	@AuraEnabled
	public String flocAPI;

	public HOG_SObject_Information(SObject floc) {
		this.floc = floc;
		Id locationId = floc.Id;
		this.flocAPI = String.valueOf(locationId.getSobjectType());
	}
}