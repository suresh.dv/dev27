/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Class that holds all(or will hold all) constant values for VTT applications
Test Class:     Doesn't have dedicated Unit Test Class
                VTT_Utility_EndpointHandlerTest OR VTT_Utility_EndpointTest
                VTT_WOA_SearchCriteria_HandlerTest OR VTT_WOA_SearchCriteria_EndpointTest
                VTT_WOA_SearchCriteria_HandlerTestNP OR VTT_WOA_SearchCriteria_EndpointTest
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public inherited sharing class VTT_Constants {

//GENERIC
    public final static String BUSINESS_UNIT_NAME_HOG = 'HEAVY OIL AND GAS - LLOYDMINSTER';
    public final static Id VENDOR_ACCOUNT_RECORD_TYPE_ID = Schema.SObjectType.Account
            .getRecordTypeInfosByDeveloperName().get('HOG_Vendor_Account_Record').getRecordTypeId();

//WORK ORDER (HOG_Maintenance_Servicing_Form__c)
    public static final Set<String> ORDER_TYPES = new Set<String> {
            'MEC1',
            'MEX1',
            'MPC1',
            'MPX1',
            'MRC1',
            'MRX1',
            'GNX1'
    };
    public static final List<String> FILTERABLE_ORDER_TYPES = new List<String> (ORDER_TYPES);

    //User statuses
    public final static String WORK_ORDER_USER_STATUS_CONFIRMED = '8FC';

//WORK ORDER ACTIVITIES (Work_Order_Activity__c)
    public static final String ACTIVITY_STATUS_CANCELLED = 'Cancelled';

    public static final List<Schema.PicklistEntry> FILTERABLE_WOA_STATUSES {
        get {
            if(FILTERABLE_WOA_STATUSES == null) {
                FILTERABLE_WOA_STATUSES = new List<Schema.PicklistEntry>();
                for(Schema.PicklistEntry activityStatus : Work_Order_Activity__c.Status__c.getDescribe().getPicklistValues()) {
                    if(!NOT_FILTERABLE_WOA_STATUSES.contains(activityStatus.getValue())) {
                        FILTERABLE_WOA_STATUSES.add(activityStatus);
                    }
                }
            }
            return FILTERABLE_WOA_STATUSES;
        }
        private set;
    }
    public static final Set<String> NOT_FILTERABLE_WOA_STATUSES = new Set<String> { ACTIVITY_STATUS_CANCELLED };

//WORK ORDER ACTIVITY LOG ENTRY (Work_Order_Activity_Log_Entry__c)
    public static final Set<String> FINISHED_LOG_ENTRY_STATUSES = new Set<String> {
            VTT_Utilities.LOGENTRY_JOBCOMPLETE,
            VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY,
            VTT_Utilities.LOGENTRY_AUTOCOMPLETE
    };

//ALT CONFIRM
    public static final String ALT_CONFIRM_SESSION_CACHE_PARTITION_NAME = 'HOGListActionsCache';
    public static final String ALT_CONFIRM_SESSION_CACHE_NAME = 'ALTList';

}