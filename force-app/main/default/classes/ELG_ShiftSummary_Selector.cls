/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 3/11/2020   
 */

public class ELG_ShiftSummary_Selector {

	public static List<ELG_Shift_Assignement__c> getShiftToCreateFirstSummary(String shiftId) {
		return [
				SELECT Id
				FROM ELG_Shift_Assignement__c
				WHERE Id = :shiftId
				AND Post__r.Shift_Lead_Post__c = TRUE
				LIMIT 1
		];
	}

	public static List<ELG_Shift_Summary__c> getShiftSummaryBasedOnShift(String shiftId) {
		return [
				SELECT Id,
						HSE_Incidents__c,
						Daily_Variance_300_Barrels_or_more__c,
						Forecasted_Variance_300_Barrels_or_more__c,
						Facility_Shutdowns_Trips__c,
						Current_or_Upcoming_Operational_Issues__c,
						Status__c,
						Signed_Off__c,
						Outgoing_Shift_Lead__c,
						Signed_In__c,
						Next_Shift_Lead__r.Name,
						Shift_Assignement__r.Primary__c
				FROM ELG_Shift_Summary__c
				WHERE Shift_Assignement__c = :shiftId
				AND Shift_Assignement__r.Post__r.Shift_Lead_Post__c = TRUE
				LIMIT 1
		];
	}

	public static ELG_Shift_Summary__c getShiftSummaryBasedOnRecordId(String recordId) {
		return [
				SELECT Id,
						HSE_Incidents__c,
						Daily_Variance_300_Barrels_or_more__c,
						Forecasted_Variance_300_Barrels_or_more__c,
						Facility_Shutdowns_Trips__c,
						Current_or_Upcoming_Operational_Issues__c,
						Status__c,
						Signed_Off__c,
						Outgoing_Shift_Lead__c,
						Signed_In__c,
						Next_Shift_Lead__r.Name,
						Shift_Assignement__r.Primary__c
				FROM ELG_Shift_Summary__c
				WHERE Id = :recordId
		];
	}

	public static List<ELG_Shift_Handover__c> getShiftHandovers(List<String> shiftIds) {
		return  [
				SELECT Id, Shift_Assignement__r.Post__r.Post_Name__c
				FROM ELG_Shift_Handover__c
				WHERE Shift_Assignement__c IN :shiftIds
				ORDER BY Shift_Assignement__r.Post__r.Post_Name__c
		];
	}

	public static ELG_Shift_Handover__c getHandoverForm(String shiftId) {
		return [
				SELECT CreatedDate,
						AMU__c,
						Post__c,
						Shift_Cycle__c
				FROM ELG_Shift_Handover__c
				WHERE Shift_Assignement__c = :shiftId
				AND Shift_Assignement__r.Post__r.Shift_Lead_Post__c = TRUE
		];
	}

}