/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentCorrectionBulkClose page to close Status of multiple Equipment Data Update records
Test Class:    EquipmentCorrectionFormTest

History:       03-Jul-17 Miro Zelina - included System & Sub-System into bulk Close Form
               27-Jul-17 Miro Zelina - added role check for SAP Support Lead
			   04-Aug-17 Miro Zelina - added profile & PS check for process button
			 04.09.17    Marcel Brimus - added FEL and Yard
			 16-Nov-18 	Maros Grajcar - Sprint - Equipment Transfer Release 1
								- added Rejected button to page
								- added popup window on Rejected button
								- added new field to the object
								- added logic to get email from Planner Group
				08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude - Email Service Enhancement R
			12.09.19 	Maros Grajcar - W-001635 - sorting list for Process list view button
------------------------------------------------------------*/

public with sharing class EquipmentCorrectionBulkCloseControllerX {

	public List<Equipment_Correction_Form__c> selectedList { get; private set; }
	private EquipmentFormService equipmentUpdate;
	public Boolean displayPopup { get; set; }
	private String closePopupReason;
	public Integer CurrentIndex { get; private set; }
	public Equipment_Correction_Form__c CurrentForm { get; private set; }
	public EquipmentUtilities.UserPermissions userPermissions;

	public EquipmentCorrectionBulkCloseControllerX(ApexPages.StandardSetController controller) {
		userPermissions = new EquipmentUtilities.UserPermissions();

		if (!Test.isRunningTest()) {
			//jschn 08-Jul-19 - Added Functional_Location__c
			controller.addFields(new List<String>{
					'Name', 'Status__c', 'Equipment_Status__c', 'Description_old__c', 'Description_new__c',
					'Manufacturer_old__c', 'Manufacturer_new__c', 'Model_Number_old__c', 'Model_Number_new__c',
					'Manufacturer_Serial_No_old__c', 'Manufacturer_Serial_No_new__c', 'Tag_Number_old__c', 'Tag_Number_new__c',
					'Equipment__c', 'Equipment__r.Location__c', 'Equipment__r.Well_Event__c', 'Equipment__r.System__c', 'Equipment__r.Sub_System__c',
					'Equipment__r.Functional_Equipment_Level__c', 'Equipment__r.Yard__c', 'Equipment__r.Functional_Location__c',
					'Equipment__r.Location__r.Route__c', 'Equipment__r.Planner_Group__c', 'Equipment__r.Well_Event__r.Route__c',
					'Equipment__r.System__r.Route__c', 'Equipment__r.Sub_System__r.Route__c',
					'Equipment__r.Functional_Equipment_Level__r.Route__c', 'Equipment__r.Yard__r.Route__c',
					'CreatedById', 'CreatedDate', 'LastModifiedDate', 'LastModifiedById', 'Comments__c',
					'Equipment__r.Facility__r.Plant_Section__c', 'Equipment__r.Facility__c', 'Expiration_Date__c',
					'Reason__c', 'Regulatory_Equipment__c', 'Safety_Critical_Equipment__c', 'Send_Email_Notification__c',
					'Location_Label__c'
			});
		}

		if (!isUserAdmin) {

			selectedList = new List<Equipment_Correction_Form__c>();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
			return;
		}

		else if (controller.getSelected().size() == 0) {
			selectedList = new List<Equipment_Correction_Form__c>();
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Equipment Correction Form'));
			return;
		}
		selectedList = controller.getSelected();
		selectedList.sort();
		CurrentIndex = 1;

		CurrentForm = selectedList[CurrentIndex - 1];
	}
	//get the Attachment of the Current Form
	public Attachment photo {
		get {
			if (photo == null) {
				List<Attachment> att = [SELECT Id, Name, Body, ContentType FROM Attachment WHERE ParentId = :CurrentForm.Id];
				if (att.size() > 0)
					photo = att[0]; else
						photo = new Attachment();
			}
			return photo;
		}
		set;
	}


	public ContentDocumentLink photoFile {
		get {

			if (photoFile == null) {
				List<ContentDocumentLink> file = [SELECT Id, ContentDocumentId, ContentDocument.Title FROM ContentDocumentLink WHERE LinkedEntityId = :CurrentForm.Id];
				if (file.size() > 0)
					photoFile = file[0]; else
						photoFile = new ContentDocumentLink();
			}
			return photoFile;
		}
		set;
	}

	public Boolean getHasPreviousForm() {
		return (CurrentIndex > 1);
	}

	public Boolean getHasNextForm() {
		return (CurrentIndex < selectedList.size());
	}

	public PageReference NextForm() {
		if (getHasNextForm()) {
			CurrentIndex = CurrentIndex + 1 ;
			CurrentForm = selectedList[CurrentIndex - 1];
			photo = null;
			photoFile = null;
		}
		return null;
	}

	public PageReference PreviousForm() {
		if (getHasPreviousForm()) {
			CurrentIndex = CurrentIndex - 1 ;
			CurrentForm = selectedList[CurrentIndex - 1];
			photo = null;
			photoFile = null;
		}
		return null;
	}
	//Change Form's Status to Closed
	public PageReference CloseForm() {
		Equipment_Correction_Form__c obj = CurrentForm;

		if (obj.Status__c != EquipmentUtilities.REQUEST_STATUS_PROCESSED && obj.Status__c != EquipmentUtilities.REQUEST_STATUS_REJECTED_CLOSED && obj.Status__c != EquipmentUtilities.REQUEST_STATUS_AUTO_CLOSED) {
			System.Savepoint sp = Database.setSavepoint();
			try {
				obj.Status__c = EquipmentUtilities.REQUEST_STATUS_PROCESSED;
				update obj;
				EquipmentFormService.formProcessedTemplateEmail(CurrentForm.Id, new Set<String>{
						CurrentForm.Equipment__r.Planner_Group__c
								+ EquipmentFormService.COMBINATION_DELIMITER
								+ CurrentForm.Equipment__r.Functional_Location__c
				});

				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully PROCESSED and notification email has been sent.'));
				return null;

			} catch (Exception ex) {
				Database.rollback(sp);
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
				return null;
			}
		}

		return null;

	}

	public Boolean isUserAdmin {
		get {
			return userPermissions.isSystemAdmin
					|| userPermissions.isSapSupportLead
					|| userPermissions.isHogAdmin;
		}
		private set;
	}

	/**
	* functions for popup window, to prevent displaying the reason on salesforce side on cancel button,
	* we are using this variable to keep the old reason
	*/
	public void showPopup() {
		displayPopup = true;
		closePopupReason = CurrentForm.Reason__c;
	}
	public void closePopup() {
		displayPopup = false;
	}

	public void closePopupOnPage() {
		displayPopup = false;
		CurrentForm.Reason__c = closePopupReason;
	}

	public void saveOnPopup() {
		System.Savepoint sp = Database.setSavepoint();
		String originalStatus = CurrentForm.Status__c;

		CurrentForm.Status__c = EquipmentUtilities.REQUEST_STATUS_REJECTED;
		try {
			update CurrentForm;
			//08-Jul-19 - Jakub Schon - added logic for FLOC include/exclude
			EquipmentFormService.requestRejectionTemplateEmail(CurrentForm.Id, CurrentForm.CreatedById, new Set<String>{
					CurrentForm.Equipment__r.Planner_Group__c
							+ EquipmentFormService.COMBINATION_DELIMITER
							+ CurrentForm.Equipment__r.Functional_Location__c
			});

			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully ' +
					'REJECTED and notification email ' +
					'has been sent.'));
		} catch (Exception ex) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			Database.rollback(sp);
			CurrentForm.Status__c = originalStatus;
		}
		closePopup();
	}

	public PageReference rejectAndNotify() {
		showPopup();
		return null;
	}

}