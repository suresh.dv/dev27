public class OpEventControllerExtension extends AbstractControllerExtension { 
    
    public Boolean isNew {get; set;}
    public Operational_Event__c opEvent {get; set;}
    public Boolean locationRequired {get;set;}
    private Team_Allocation__c ta;
    private Map<Id, Process_Area__c> areaMap;
    
    // the values of the selected items
    public string selectedLevel1 {get; set;}
    public string selectedLevel2 {get; set;}
    public string selectedLevel3 {get; set;}
    
    public string selectedNone = '*';
    
    public string selectedEquipmentBySearch {get; set;}    
    
    public String searchByTagID{get; set;}
    public String recordTypeName {get;set;}
    
    
    
    public OpEventControllerExtension(ApexPages.StandardController stdController) {
        //When a Visualforce page is loaded, the fields accessible to the page are based on the fields referenced in the Visualforce markup. 
        //This method adds a reference to each field specified in fieldNames so that the controller can explicitly access those fields as well.
        if (!Test.isRunningTest())
        {
            stdController.addFields(new List<String>{'Operating_District__c', 'Time__c', 'Area__c', 'Operator_Contact__c',
                                               'Priority__c', 'Description__c','Work_Order_Number__c', 'Team_Allocation__c', 
                                               'Plant__c', 'Unit__c', 'Equipment__c', 'Area__r.Location_Required__c', 
                                               'RecordTypeId', 'RecordType.Name'});
        }
        this.opEvent = (Operational_Event__c) stdController.getRecord();
        this.isNew = String.isEmpty(this.opEvent.id);
        
        if (isNew)
        {
            if ( ApexPages.currentPage().getParameters().get('ta') != null)
            {
                opEvent.Team_Allocation__c = ApexPages.currentPage().getParameters().get('ta');
                this.ta = TeamAllocationUtilities.getTeamAllocation(this.opEvent.Team_Allocation__c);
                recordTypeName = ta.RecordType.Name;
                setDefaults();
            }
            else
            {
                this.opEvent.addError('No active Team Allocation for new Operational Event.');
                return;
            }
        }
        else
        {
            ta = TeamAllocationUtilities.getTeamAllocation(this.opEvent.Team_Allocation__c);
            recordTypeName = opEvent.RecordType.Name;
            
        }
        areaMap = new Map<Id, Process_Area__c>([select Id, Name, Location_Required__c from Process_Area__c where RecordType.Name =: recordTypeName]);
        System.debug('The areaMap is '+areaMap);
        if (isNew)
        {
            if (areaMap != null)
            {
                List<Process_Area__c> areaList = areaMap.values();
                areaList.sort();
                if(areaList.size()>0)
                locationRequired = areaList[0].Location_Required__c;  
            } 
        }  
        else
            locationRequired = opEvent.Area__r.Location_Required__c;  
     
        selectedLevel1 = String.ValueOf(opEvent.get('Plant__c'));
        selectedLevel2 = String.ValueOf(opEvent.get('Unit__c'));
        selectedLevel3 = String.ValueOf(opEvent.get('Equipment__c'));
        
        addErrorIfInactiveEvent();
    }
   
    private void setDefaults() {
      
        System.debug('set default');
        // Set time to the current value.
        this.opEvent.Time__c = Datetime.now().format('HH:mm');
        this.opEvent.Team_Allocation__c = this.ta.Id;
        this.opEvent.Operating_District__c = this.ta.Operating_District__c;
            
        List<Contact> operator = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE FirstName =:UserInfo.getFirstName() AND LastName =:UserInfo.getLastName() AND
                                    Email =:UserInfo.getUserEmail()];
        if(operator != Null && operator.size() > 0) {
            this.opEvent.Operator_Contact__c = operator[0].Id;
        }
        Map<String, Schema.Recordtypeinfo> recTypesByName = Operational_Event__c.SObjectType.getDescribe().getRecordTypeInfosByName();
        opEvent.RecordTypeId = recTypesByName.get(ta.RecordType.Name).getRecordTypeId();
        
    }
    
    private void addErrorIfInactiveEvent() {
        if (this.ta != null) {
            if (this.ta.Status__c == TeamAllocationUtilities.INACTIVE_STATUS_NAME) {
                this.opEvent.addError('Read only - This item cannot be modified since its team allocation is not active.'); 
            }
        }
    }
      
    public List<selectOption> processAreaList
    {
        get
        {
            List<selectOption> options = new List<selectOption>();
            if (areaMap != null)
            {
                List<Process_Area__c> areaList = areaMap.values();
                areaList.sort();
                 
                for(Process_Area__c a : areaList)
                {
                    options.add(new SelectOption(a.Id, a.Name));
                }
            }
            return options;
        }  
    }
    
    
    public void populatePlantUnitEquipment() {
        System.debug('selectedEquipmentBySearch : ' + selectedEquipmentBySearch);
        if(selectedEquipmentBySearch != Null) {
            Equipment__c equipment = [SELECT Id, Name, Tag_Number__c, Unit__c, Unit__r.Plant__c FROM Equipment__c WHERE Id =: selectedEquipmentBySearch];
         
            selectedLevel1 = equipment.Unit__r.Plant__c;
            selectedLevel2 = equipment.Unit__c;
            selectedLevel3 = selectedEquipmentBySearch;
        }
    }
    
    public void resetUnitAndEquipment() {
      
        if(selectedLevel2 != Null) {
            selectedLevel2 = Null;
            selectedLevel3 = Null;      
            selectedEquipmentBySearch = Null;   
        }
    }
     
    public List<selectOption> level1Items {
        get {
           
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption(selectedNone,'-- Choose a Plant --'));
            
            for (Plant__c plant : [select Id, Name from Plant__c Order By Name limit 999])
                options.add(new SelectOption(plant.Id,plant.Name));
            
            return options;           
        }
        set;
    }
    
    public List<selectOption> level2Items {
        get {
            
            List<selectOption> options = new List<selectOption>();
 
            if (selectedLevel1 != NULL && selectedLevel1 != selectedNone) {
                options.add(new SelectOption(selectedNone,'-- Choose a Unit --'));
                for (Unit__c unit : [select Id, Name from Unit__c Where Plant__c = :selectedLevel1 Order By Name limit 999])
                    options.add(new SelectOption(unit.Id,unit.Name));
            }
            return options;            
        }
        set;
    }
    
    public List<selectOption> level3Items {
        get {
            List<selectOption> options = new List<selectOption>();
            
            if (selectedLevel2 != NULL && selectedLevel1 != selectedNone) {
                options.add(new SelectOption(selectedNone,'-- Choose an Equipment --'));
                
                if(searchByTagID == Null) {
                    for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c Where Tag_Number__c != Null AND Unit__c = :selectedLevel2 Order By Tag_Number__c limit 999]) {
                        if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                        }
                        else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50))); 
                        }
                        else if(equipment.Description_of_Equipment__c == Null) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c));   
                        }
                    }
                }
                else if(searchByTagID != Null) {
                    String filterString = '%' + searchByTagID + '%';
                    for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c Where Tag_Number__c != Null AND Unit__c =:selectedLevel2 AND (Tag_Number__c like :filterString OR Description_of_Equipment__c like :filterString) Order By Tag_Number__c limit 999]) {
                        if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                        }
                        else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50))); 
                        }
                        else if(equipment.Description_of_Equipment__c == Null) {
                            options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c));   
                        }
                    }
                }
            }
        
            return options;           
        }
        set;
    }  
    
    public List<selectOption> equipmentSearchList {
        get {
       
            selectedEquipmentBySearch = Null;
            List<selectOption> options = new List<selectOption>();
            
            if(searchByTagID != Null) {
                String filterString = '%' + searchByTagID + '%';
                for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c Where Tag_Number__c != Null AND Unit__c != Null AND Unit__r.Plant__c != Null AND (Tag_Number__c like :filterString OR Description_of_Equipment__c like :filterString) Order By Tag_Number__c Limit 1000]) {
                    if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                        options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                    }
                    else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50)));  
                    }
                    else if(equipment.Description_of_Equipment__c == Null) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c));    
                    }
                }                   
            }
            else if(searchByTagID == Null) {
                for (Equipment__c equipment: [select Id, Name, Tag_Number__c, Description_of_Equipment__c from Equipment__c WHERE Tag_Number__c != Null AND Unit__c != Null AND Unit__r.Plant__c != Null Order By Tag_Number__c Limit 1000]) {
                    if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() < 50) {
                        options.add(new SelectOption(equipment.Id, equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c));
                    }
                    else if(equipment.Description_of_Equipment__c != Null && equipment.Description_of_Equipment__c.length() > 50) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c + ' ' + equipment.Description_of_Equipment__c.SubString(0,50)));  
                    }
                    else if(equipment.Description_of_Equipment__c == Null) {
                        options.add(new SelectOption(equipment.Id,equipment.Tag_Number__c));    
                    }
                }                   
            }
           
            return options;  
        }
        set;
    }   
    
  
    public void ProcessAreaIsChange()
    {
        System.debug('select area = ' + opEvent.Area__c);
        locationRequired = areaMap.get(opEvent.Area__c).Location_Required__c;
    }
   
    public String getTeamAllocationName() {
        if (this.ta != null) {
            return ta.Team_Allocation_Name__c;
        }
        
        return '';
    }
    
    public PageReference save() 
    {
        if (selectedLevel1 != null && selectedLevel1 != selectedNone)
            opEvent.Plant__c = selectedLevel1;
        
        if(selectedLevel2 != selectedNone) {
            opEvent.Unit__c = selectedLevel2;
            
            if(selectedLevel3 != selectedNone) {
                opEvent.Equipment__c = selectedLevel3;
            }
            else {
                opEvent.Equipment__c = null;
            }
        }
        else {
            opEvent.Unit__c = null;
            opEvent.Equipment__c = null;
        }
        
        try{
            //Keep a track of historical role and team assignment 
            List<Shift_Operator__c> ops = [SELECT Team__r.Name, Role__r.Name FROM Shift_Operator__c WHERE Operator_Contact__c =: opEvent.Operator_Contact__c and Team__c != null LIMIT 1];
            opEvent.Shift_Resource_Role__c = ops.get(0).Role__r.Name;
            opEvent.Shift_Resource_Team__c = ops.get(0).Team__r.Name;
        } catch(Exception exp) {
            System.debug('Exception when query Shift_Operator__c ' + exp.getMessage());
        }            
    
        return new ApexPages.StandardController(this.opEvent).save();
    }
    
    public PageReference saveAndNew() 
    {     
        if (selectedLevel1 != null && selectedLevel1 != selectedNone)
            opEvent.Plant__c = selectedLevel1;
        
        if(selectedLevel2 != selectedNone) {
            opEvent.Unit__c = selectedLevel2;
            
            if(selectedLevel3 != selectedNone) {
                opEvent.Equipment__c = selectedLevel3;              
            }
            else {
                opEvent.Equipment__c = null;
            }
        }
        else {
            opEvent.Unit__c = null;
            opEvent.Equipment__c = null;
        }
        
        try{
            //Keep a track of historical role and team assignment 
            List<Shift_Operator__c> ops = [SELECT Team__r.Name, Role__r.Name FROM Shift_Operator__c WHERE Operator_Contact__c =: opEvent.Operator_Contact__c and Team__c != null LIMIT 1];
            opEvent.Shift_Resource_Role__c = ops.get(0).Role__r.Name;
            opEvent.Shift_Resource_Team__c = ops.get(0).Team__r.Name;
        } catch(Exception exp) {
            System.debug('Exception when query Shift_Operator__c ' + exp.getMessage());
        }          
   
        // Save the Operational Event to the database        
        new ApexPages.StandardController(this.opEvent).save();

        // Go to the page that adds a new Operational Event
        // do not populate Equipment and Description
        //return new OperationalEventListController().newOperationalEvent();
        
        Operational_Event__c newOpEvent = new Operational_Event__c();
        newOpEvent.RecordTypeId = opEvent.RecordTypeId;
        newOpEvent.Team_Allocation__c = opEvent.Team_Allocation__c;
        newOpEvent.Operating_District__c = opEvent.Operating_District__c;
        newOpEvent.Operator_Contact__c = opEvent.Operator_Contact__c;
        newOpEvent.Area__c = opEvent.Area__c;
        newOpEvent.Priority__c = opEvent.Priority__c;
        newOpEvent.Plant__c = opEvent.Plant__c;
        newOpEvent.Work_Order_Number__c = opEvent.Work_Order_Number__c;
        if(selectedLevel2 != selectedNone) 
        {
            newOpEvent.Unit__c = opEvent.Unit__c;
        }
        newOpEvent.Time__c = Datetime.now().format('HH:mm');
        newOpEvent.Equipment__c = null;
        opEvent = newOpEvent;
        selectedLevel3 = null;
        return null;   
    }
}