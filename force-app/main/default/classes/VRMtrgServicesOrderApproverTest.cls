@IsTest
public class VRMtrgServicesOrderApproverTest {
    static testmethod void testprofServicesOrder(){
        test.startTest();
        
        Account a= new Account();
        a.name='test';
        a.Type='vendor';
        insert a;
        System.assertNotEquals(a.id, null);
        jbrvrm__Professional_Services_Order__c profOrder1= new jbrvrm__Professional_Services_Order__c();
        profOrder1.Name='test';
        profOrder1.jbrvrm__Vendor__c=a.Id;
        profOrder1.jbrvrm__Approver_1__c = null;
        insert profOrder1;
        System.assertNotEquals(profOrder1.Id, null);
        
        
        
        test.stopTest();
    }

}