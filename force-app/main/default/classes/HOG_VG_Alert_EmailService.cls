/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Service which provides methods for handles sending and building emails for Vent Gas
                Alert Notifications.
Test Class:     HOG_VG_Alert_EmailServiceTest
History:        jschn 25/10/2018 - Created. US W-001290, W-001293, W-001304
                jschn 21/03/2019 - Added Status into Alert Notification email template.
                                 - Ordered Alert, first Not Started and then others.
                                 - US W-001448
*************************************************************************************************/
public with sharing class HOG_VG_Alert_EmailService {

    public static final String CLASS_NAME = String.valueOf(HOG_VG_Alert_EmailService.class);

    private final HOG_Vent_Gas_Alert_Configuration__c settings = HOG_Vent_Gas_Alert_Configuration__c.getInstance();
    private final String baseUrl = System.Url.getSalesforceBaseUrl().getHost();

    private final String WEEKLY_NOTIFICATION_DAY_OF_WEEK = 'MON';

    @TestVisible
    private final String URGENT_EMAIL_SUBJECT = 'Urgent HOG Vent Gas Alert Summary';
    @TestVisible
    private final String NON_URGENT_EMAIL_SUBJECT = 'HOG Vent Gas Alert Summary';
    @TestVisible
    private final String PRODUCTION_ENGINEER_REASSIGNMENT_EMAIL_SUBJECT = 'HOG Vent Gas Alert - ' +
            'Production Engineer has been Re-Assigned';

    private static final String LIST_DELIMITER = ', ';

    private static final Set<String> INCOMPLETE_ALERT_STATES = new Set<String>{
            HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS
    };
    private static final Set<String> INCOMPLETE_TASK_STATES = new Set<String>{
            HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED
    };

    private static final String STATES_PLACEHOLDER = '*STATES*';
    private static final String CONDITION_PLACEHOLDER = '*CONDITIONS*';

    private final OrgWideEmailAddress sender;

    public HOG_VG_Alert_EmailService() {
        try{
            if(settings != null && String.isNotBlank(settings.Default_Notification_Sender_Name__c)) {
                sender = [SELECT Id, DisplayName, Address FROM OrgWideEmailAddress
                          WHERE DisplayName = :settings.Default_Notification_Sender_Name__c LIMIT 1];
            }
        } catch(Exception ex) {
            //do nothing.
        }
    }

    public OrgWideEmailAddress getSender() {
        return sender;
    }

    private String queryBaseForInCompleteAlerts = ('SELECT Id, Name, Comments__c, Start_Date__c, '
            + 'Status__c, Type__c, Well_Location__c, Well_Location__r.Name, Production_Engineer__c, '
            + 'Production_Engineer__r.Name, Priority__c '
            + 'FROM HOG_Vent_Gas_Alert__c '
            + 'WHERE Status__c IN (' + STATES_PLACEHOLDER + ')' + CONDITION_PLACEHOLDER +
            ' ORDER BY Production_Engineer__c ASC, Priority__c ASC').replace(STATES_PLACEHOLDER,
            getStringList(INCOMPLETE_ALERT_STATES));
    
    private String queryBaseForInCompleteTasks = ('SELECT Id, Name'
            +		', Assignee1__c, Assignee1__r.Name'
            + 		', Assignee2__c, Assignee2__r.Name'
            +		', Priority__c, Remind__c'
            +		', Comments__c, Subject__c'
            +		', Vent_Gas_Alert__c'
            +		', Vent_Gas_Alert__r.Type__c'
            +		', Vent_Gas_Alert__r.Well_Location__c'
            +		', Vent_Gas_Alert__r.Well_Location__r.Name'
            + ' FROM HOG_Vent_Gas_Alert_Task__c'
            + ' WHERE Status__c IN (' + STATES_PLACEHOLDER + ')'
            + ' AND Remind__c = true' + CONDITION_PLACEHOLDER +
            ' ORDER BY Assignee1__c ASC, Assignee2__c ASC, Vent_Gas_Alert__r.Priority__C ASC')
            .replace(STATES_PLACEHOLDER, getStringList(INCOMPLETE_TASK_STATES));

    public String getNotificationQuery(HOG_VGEmailNotificationsType notificationType, String dayOfWeek) {
        String additionalAlertTypesConditions = getQueryConditions(notificationType, dayOfWeek);
        String queryBase = (notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS ||
                notificationType == HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS) ?
                queryBaseForInCompleteAlerts : queryBaseForInCompleteTasks;

        if(shouldRemoveInProgressStatus(notificationType, dayOfWeek)) {
            queryBase = queryBase.replace(LIST_DELIMITER + HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, '');
            queryBase = queryBase.replace(HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, '');
        }

        return queryBase.replace(CONDITION_PLACEHOLDER, additionalAlertTypesConditions);
    }

    private Boolean shouldRemoveInProgressStatus(HOG_VGEmailNotificationsType notificationType, String dayOfWeek) {
        Boolean should = false;
        if(notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS
                || notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_TASKS) {

            should = WEEKLY_NOTIFICATION_DAY_OF_WEEK != dayOfWeek;

        }
        return should;
    }

    private String getStringList(Set<String> strings) {
        String stringList = '';
        for(String str : strings) {
            if (String.isNotBlank(stringList)) {
                stringList += LIST_DELIMITER;
            }
            stringList += '\'###\''.replace('###', str);
        }
        return stringList;
    }

    private String getQueryConditions(HOG_VGEmailNotificationsType notificationType, String dayOfWeek) {
        if (notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS ||
                notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_TASKS) {
            return getUrgentAlertQueryCondition(dayOfWeek, notificationType);
        } else if (notificationType == HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS ||
                notificationType == HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_TASKS) {
            return getNonUrgentAlertQueryCondition(dayOfWeek, notificationType);
        }
        return ' AND Priority__c = NULL';
    }

    private String getUrgentAlertQueryCondition(String dayOfWeek, HOG_VGEmailNotificationsType notificationType) {
        String conditionField = (notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS) ?
                'Priority__c' : 'Vent_Gas_Alert__r.Priority__c';
        if(settings.Urgent_Priority_Alert_Daily_Notification__c
                || WEEKLY_NOTIFICATION_DAY_OF_WEEK == dayOfWeek) {
            return ' AND ' + conditionField + ' = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_URGENT + '\'';
        }
        return ' AND ' + conditionField + ' = NULL';
    }

    private String getNonUrgentAlertQueryCondition(String dayOfWeek, HOG_VGEmailNotificationsType notificationType) {
        String initialString = ' AND (';
        String conditionField = (notificationType == HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS) ?
                'Priority__c' : 'Vent_Gas_Alert__r.Priority__c';
        String condition = initialString;
        if(settings.High_Priority_Alert_Daily_Notification__c
                || WEEKLY_NOTIFICATION_DAY_OF_WEEK == dayOfWeek) {
            condition += (' ' + conditionField + ' = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_HIGH + '\'');
        }
        if(settings.Medium_Priority_Alert_Daily_Notification__c
                || WEEKLY_NOTIFICATION_DAY_OF_WEEK == dayOfWeek) {
            condition += condition.contains(conditionField) ? ' OR' : '';
            condition += (' ' + conditionField + ' = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM + '\'');
        }
        if(settings.Low_Priority_Alert_Daily_Notification__c
                || WEEKLY_NOTIFICATION_DAY_OF_WEEK == dayOfWeek) {
            condition += condition.contains(conditionField) ? ' OR' : '';
            condition += (' ' + conditionField + ' = \'' + HOG_VentGas_Utilities.ALERT_PRIORITY_LOW + '\'');
        }
        if(condition.equals(initialString)) {
            condition = ' AND ' + conditionField + ' = NULL';
        } else {
            condition += ')';
        }
        return condition;
    }

    public String getDayOfWeek(Datetime dt) {
        return dt.format('EEE').toUpperCase();
    }

    public List<Messaging.SingleEmailMessage> generateInCompleteVentGasAlertEmails(
            List<HOG_Vent_Gas_Alert__c> ventGasAlertList,
            HOG_VGEmailNotificationsType notificationsType) {
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        Map<Id, List<HOG_Vent_Gas_Alert__c>> incompleteAlertsToProductionEngineerMap = new Map<Id, List<HOG_Vent_Gas_Alert__c>>();

        //Populate Map of Vent Gas Alerts
        for (HOG_Vent_Gas_Alert__c alert : ventGasAlertList) {
            if(alert.Production_Engineer__c != null) {
                if (incompleteAlertsToProductionEngineerMap.containsKey(alert.Production_Engineer__c)) {
                    incompleteAlertsToProductionEngineerMap.get(alert.Production_Engineer__c).add(alert);
                } else {
                    incompleteAlertsToProductionEngineerMap.put(alert.Production_Engineer__c,
                            new List<HOG_Vent_Gas_Alert__c>{
                                    alert
                            });
                }
            }
        }

        //Create Email for each production engineer in map
        for (Id prodEngineer : incompleteAlertsToProductionEngineerMap.keySet()) {
            emailList.add(generateInCompleteVentGasAlertEmail(prodEngineer,
                    incompleteAlertsToProductionEngineerMap.get(prodEngineer), notificationsType));
        }

        return emailList;
    }

    private Messaging.SingleEmailMessage generateInCompleteVentGasAlertEmail(Id productionEngineer,
            List<HOG_Vent_Gas_Alert__c> alertsList,
            HOG_VGEmailNotificationsType notificationType) {

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTargetObjectId(productionEngineer);
        email.setSubject(getSubject(notificationType));
        email.setHtmlBody(setMessageBody(alertsList));
        email.setSaveAsActivity(false);
        email = setSender(email, notificationType);

        return email;
    }

    private String setMessageBody(List<HOG_Vent_Gas_Alert__c> alertsList) {
        String notStartedAlerts = '';
        String otherAlerts = '';

        for (HOG_Vent_Gas_Alert__c alert : alertsList) {
            String tmpMessageBody = '';
            String urlAlert = '<a target="vg_alert" href="https://'
                    + baseUrl + '/apex/HOG_VGAlertView?id=' + alert.Id + '">'
                    + alert.Name + '</a>';
            String urlWellLocation = '<a target="wellLocation" href="https://'
                    + baseUrl + '/' + alert.Well_Location__c + '">'
                    + alert.Well_Location__r.Name + '</a>';
            tmpMessageBody += '<table>';
            tmpMessageBody += '<tr><td>Alert: </td><td>' + urlAlert + '<td/></tr>';
            tmpMessageBody += '<tr><td>Type: </td><td>' + alert.Type__c + '<td/></tr>';
            tmpMessageBody += '<tr><td>Status: </td><td>' + alert.Status__c + '<td/></tr>';
            tmpMessageBody += '<tr><td>Comments: </td><td>'
                    + (alert.Comments__c <> null ? alert.Comments__c : '') + '<td/></tr>';
            tmpMessageBody += '<tr><td>Production Engineer: </td><td>'
                    + alert.Production_Engineer__r.Name + '<td/></tr>';
            tmpMessageBody += '<tr><td>Well Location: </td><td>' + urlWellLocation + '<td/></tr>';
            tmpMessageBody += '<tr><td>Priority: </td><td>' + alert.Priority__c + '<td/></tr>';
            tmpMessageBody += '<tr><td>Start Date:</td><td>' + (alert.Start_Date__c <> null
                    ? alert.Start_Date__c.format() : 'Not Started') + '<td/></tr>';
            tmpMessageBody += '</table>';
            tmpMessageBody += '<hr/>';

            if(alert.Status__c == HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED) {
                notStartedAlerts += tmpMessageBody;
            } else {
                otherAlerts += tmpMessageBody;
            }
        }
        return notStartedAlerts + otherAlerts;
    }

    private String getSubject(HOG_VGEmailNotificationsType notificationType) {
        if(notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS) {
            return URGENT_EMAIL_SUBJECT;
        } else if(notificationType == HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS) {
            return NON_URGENT_EMAIL_SUBJECT;
        } else if(notificationType == HOG_VGEmailNotificationsType.PRODUCTION_ENGINEER_REASSIGNMENT) {
            return PRODUCTION_ENGINEER_REASSIGNMENT_EMAIL_SUBJECT;
        }
        return NON_URGENT_EMAIL_SUBJECT;
    }

    private Messaging.SingleEmailMessage setSender(Messaging.SingleEmailMessage email,
            HOG_VGEmailNotificationsType notificationType) {
        if(sender != null
                && (notificationType == HOG_VGEmailNotificationsType.URGENT_INCOMPLETE_ALERTS
                    ||notificationType == HOG_VGEmailNotificationsType.NON_URGENT_INCOMPLETE_ALERTS)) {
            if(!Test.isRunningTest()) email.setOrgWideEmailAddressId(sender.Id);
        }
        return email;
    }

    public void generateEmailNotification(List<List<String>> AlertIdAndProdEngId,
            HOG_VGEmailNotificationsType notificationType) {

        //check to avoid multiple notifications sent (ie. for update with DataLoader)
        if (AlertIdAndProdEngId != null &&
                AlertIdAndProdEngId.size() == 1 &&
                AlertIdAndProdEngId[0].size() == 2) {

            //get ID's of Alert and Production Engineer populated in Flow on Production Engineer Re-Assignemnt
            String AlertId = AlertIdAndProdEngId[0][0];
            String ProdEngId = AlertIdAndProdEngId[0][1];

            //query for Alert related fields needed in Emial Notification
            List<HOG_Vent_Gas_Alert__c> ventGasSingleAlertList = new List<HOG_Vent_Gas_Alert__c>(
            [
                    SELECT Id, Name, Comments__c, Start_Date__c, Status__c, Type__c,
                            Well_Location__c, Well_Location__r.Name, Production_Engineer__c,
                            Production_Engineer__r.Name, Priority__c
                    FROM HOG_Vent_Gas_Alert__c
                    WHERE Id = :AlertId
            ]);

            //check for result (which should always have only one Alert in the list)
            if (ventGasSingleAlertList != null && ventGasSingleAlertList.size() == 1) {

                //prepare email body and email template and send the notification
                Messaging.SingleEmailMessage message = generateInCompleteVentGasAlertEmail(
                        ProdEngId, ventGasSingleAlertList, notificationType);
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage>{message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

                if (results[0].success) {
                    System.debug(CLASS_NAME + ' -> The email was sent successfully.');
                } else {
                    System.debug(CLASS_NAME + ' -> The email failed to send: ' + results[0].errors[0].message);
                }
            }
        }
    }

}