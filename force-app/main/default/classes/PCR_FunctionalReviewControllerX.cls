public class PCR_FunctionalReviewControllerX{
    private ApexPages.StandardController controller;
    private Change_Request__c c {get; set;}
    public CR_Review__c review {get; private set;}
    private Id crId {get; set;}
    public String crNumber {get; private set;}
    public Boolean isEdit {get; private set;}
    
    public PCR_FunctionalReviewControllerX(){}
    
    public PCR_FunctionalReviewControllerX(ApexPages.StandardController controller) {
        List<string> fields = new List<String>();
        Map<String, Schema.FieldSet> fieldSetMap = SObjectType.CR_Review__c.FieldSets.getMap();
            
        crId = ApexPages.currentPage().getParameters().get('Change_Request');
        
        for (String fieldSetName : fieldSetMap.keySet()){
            for(Schema.FieldSetMember f : SObjectType.CR_Review__c.FieldSets.getMap().get(fieldSetName).getFields()){
                fields.add(f.getFieldPath());
            }        
        }
                
        if(!Test.isRunningTest()) {
            controller.addFields(fields);   
        }         
        
        this.controller= controller; 
        
        c = [select id ,name,Bypass_Endorsement_and_Impact_Assessment__c,Change_Status__c,Bypass_Review__c,SPA__c from Change_Request__c where id =: crId];  
        crNumber = c.Name;
        
        this.review = (CR_Review__c)this.controller.getRecord();
        
        /*if(c.Bypass_Review__c){
            doRedirect();                 
        }*/
        
        /*if(c.Change_Status__c == 'SPA Review'){
            review = new CR_Review__c();
            review.Change_Request__c = crId;
            
            isEdit = false;          
        } else {        
            review = [select id, name, Change_Request__c, Comments__c, Functional_Area2__c, Recommendation__c, Reviewed_Date__c, Reviewer__c, 
                            ChangeRequestProgramAreaReviewerKey__c, Functional_Area2__r.Name, Functional_Area2__r.id
                         from CR_Review__c where Change_Request__c =: crId];
            isEdit = false;
        } */
                             
    }
    
    
    /*public PageReference doRedirect(){
        if(!c.Bypass_Review__c){
            PageReference redirectPage = new PageReference('/apex/PCR_Functional_Review?id=' + crId);  
            redirectPage.setRedirect(true); 
            return redirectPage;    
        }
        
        PageReference redirectPage = new PageReference('/apex/Change_Request_View?id=' + crId);
        redirectPage.setRedirect(true);
        return redirectPage;                     
    }*/
    
    public PageReference SaveAndNew (){
        try{
            CR_Impacted_Functions__c f = getFunctionalArea(review.Functional_Area2__c);
            review.Change_Request__c = crId;
            review.Name = c.Name + ' - ' + f.Name;
            review.ChangeRequestProgramAreaReviewerKey__c = '' + crId + review.Functional_Area2__c;
            review.SPA__c = c.SPA__c;
                    
            insert review;
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            System.debug('Error..' + e);
            return null;
        }
        isEdit = false;
        
        review = new CR_Review__c();
        return new PageReference('/apex/PCR_Functional_Review?Change_Request=' + crId);
    }
    
    public PageReference save() {
        try{
            CR_Impacted_Functions__c f = getFunctionalArea(review.Functional_Area2__c);
            review.Name = c.Name + ' - ' + f.Name;
            review.Change_Request__c = crId;
            review.ChangeRequestProgramAreaReviewerKey__c = '' + crId + review.Functional_Area2__c;
            review.SPA__c = c.SPA__c;
            
            upsert review;
        } catch(System.DMLException e) {
            ApexPages.addMessages(e);
            System.debug('Error..' + e);
            return null;
        }
        
        return new Pagereference('/apex/Change_Request_View?id=' + crId);
    }  
    
    public PageReference cancel(){
        return new Pagereference('/apex/Change_Request_View?id=' + crId);
    }
    
    private static CR_Impacted_Functions__c getFunctionalArea(id functionalAreaId) {
        CR_Impacted_Functions__c c = new CR_Impacted_Functions__c(id = functionalAreaId);
        c = [select name from CR_Impacted_Functions__c where id = :functionalAreaId];
        return c;
    }
}