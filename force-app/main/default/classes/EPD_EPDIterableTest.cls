/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_EPDIterable
History:        jschn 2019-07-24 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class EPD_EPDIterableTest {

    @IsTest
    static void constructor_withoutParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_EPDIterable iterable;

        try {
            iterable = new EPD_EPDIterable(null);
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, iterable);
    }

    @IsTest
    static void constructor_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_EPDIterable iterable;

        try {
            iterable = new EPD_EPDIterable(new List<EPD_Engine_Performance_Data__c>());
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, iterable);
    }

    @IsTest
    static void constructor_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_EPDIterable iterable;

        try {
            iterable = new EPD_EPDIterable(
                    new List<EPD_Engine_Performance_Data__c> {
                            (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Engine_Performance_Data__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    }
            );
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, iterable);
    }

    @IsTest
    static void iterator_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_EPDIterable iterable;
        Iterator<EPD_Engine_Performance_Data__c> iterators;

        try {
            iterable = new EPD_EPDIterable(null);
            iterators = iterable.iterator();
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, iterable);
        System.assertEquals(null, iterators);
    }

    @IsTest
    static void iterator_withEmptyParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_EPDIterable iterable;
        Iterator<EPD_Engine_Performance_Data__c> iterators;

        try {
            iterable = new EPD_EPDIterable(new List<EPD_Engine_Performance_Data__c>());
            iterators = iterable.iterator();
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, iterable);
        System.assertNotEquals(null, iterators);
    }

    @IsTest
    static void iterator_withProperParam() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_EPDIterable iterable;
        Iterator<EPD_Engine_Performance_Data__c> iterators;

        try {
            iterable = new EPD_EPDIterable(
                    new List<EPD_Engine_Performance_Data__c> {
                            (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                                    new EPD_Engine_Performance_Data__c(),
                                    EPD_FieldDefaultsGeneral.CLASS_NAME,
                                    false
                            )
                    }
            );
            iterators = iterable.iterator();
        } catch (Exception ex) {
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, iterable);
        System.assertNotEquals(null, iterators);
    }

}