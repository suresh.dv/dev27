@isTest
global with sharing class SAPHOGWorkOrderIsAliveResponseMock implements WebServiceMock{

	global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
		
		SAPHOGWorkOrderServices.IsAliveResponse responseElement = new SAPHOGWorkOrderServices.IsAliveResponse();
   		responseElement.ErrorMessage = 'SAPHOGWorkOrderServices.IsAliveResponse Test Message';
   		responseElement.Alive = True;
   		
   		response.put('response_x', responseElement);
	}
}