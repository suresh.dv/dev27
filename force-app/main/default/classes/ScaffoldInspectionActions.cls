public with sharing class ScaffoldInspectionActions {

    public static void setIsCertifiedFlag(List<Scaffold_Inspection__c> newItems) {
        for(Scaffold_Inspection__c sI : newItems) {
            if(isScaffoldItemPass(sI.Toe_board_surface__c) && isScaffoldItemPass(sI.Toe_board_point_load__c) && isScaffoldItemPass(sI.Toe_board_above_platform__c) &&
               isScaffoldItemPass(sI.Tags_updated__c) && isScaffoldItemPass(sI.Tags_accessess__c) && isScaffoldItemPass(sI.Stairs_wide__c) &&
               isScaffoldItemPass(sI.Stairs_vertical__c) && isScaffoldItemPass(sI.Stairs_height__c) && isScaffoldItemPass(sI.Stairs_depth__c) &&
               isScaffoldItemPass(sI.Stairs_angle__c) && isScaffoldItemPass(sI.Sills_Secured_from_movement__c) && isScaffoldItemPass(sI.Sills_Load_Distribution__c) &&
               isScaffoldItemPass(sI.Sills_Base_plates_upright__c) && isScaffoldItemPass(sI.Sills_Base_plates_mudsills__c) && isScaffoldItemPass(sI.Safe_Distance_walkway__c) &&
               isScaffoldItemPass(sI.Safe_Distance_impeed_acess__c) && isScaffoldItemPass(sI.Safe_Distance_conductors__c) && isScaffoldItemPass(sI.Ramps_resistance__c) &&
               isScaffoldItemPass(sI.Plum_And_Level_height__c) && isScaffoldItemPass(sI.Plum_And_Level_distance__c) && isScaffoldItemPass(sI.Platforms_span__c) &&
               isScaffoldItemPass(sI.Platforms_painted__c) && isScaffoldItemPass(sI.Platforms_mud_sills__c) && isScaffoldItemPass(sI.Platforms_movement__c) &&
               isScaffoldItemPass(sI.Platforms_minimum_wide__c) && isScaffoldItemPass(sI.Platforms_light_duty__c) && isScaffoldItemPass(sI.Platforms_Scafford_plank__c) &&
               isScaffoldItemPass(sI.Platforms_Planks__c) && isScaffoldItemPass(sI.Platforms_No_gaps__c) && isScaffoldItemPass(sI.Ladders_deck_height__c) &&
               isScaffoldItemPass(sI.Ladders_cage_from_ladder__c) && isScaffoldItemPass(sI.Ladders_cage_extends__c) && isScaffoldItemPass(sI.Ladders_cage_begin__c) &&
               isScaffoldItemPass(sI.Ladders_access_point__c) && isScaffoldItemPass(sI.Ladder_rest_platform__c) && isScaffoldItemPass(sI.Ladder_clear_span__c) &&
               isScaffoldItemPass(sI.Ladder_cage_verticals__c) && isScaffoldItemPass(sI.Hangers_main_clamp__c) && isScaffoldItemPass(sI.Hangers_joiners__c) &&
               isScaffoldItemPass(sI.Guardrails_vertical_members__c) && isScaffoldItemPass(sI.Guardrails_point_load__c) && isScaffoldItemPass(sI.Guardrails_over_height__c) &&
               isScaffoldItemPass(sI.Guardrails_grade_lumber__c) && isScaffoldItemPass(sI.Guardrails_Top_Guard_Rails__c) && isScaffoldItemPass(sI.Guardrails_Midway__c) &&
               isScaffoldItemPass(sI.Guardrails_Gap__c) && isScaffoldItemPass(sI.Engineering__c) && isScaffoldItemPass(sI.Connections_wedges__c) &&
               isScaffoldItemPass(sI.Connections_clamps__c) && isScaffoldItemPass(sI.Connections_Transoms__c) && isScaffoldItemPass(sI.Connections_Node__c) &&
               isScaffoldItemPass(sI.Connections_Ledgers__c) && isScaffoldItemPass(sI.Connections_Joints__c) && isScaffoldItemPass(sI.Connections_Bottom__c) &&
               isScaffoldItemPass(sI.Bracing_wind_force__c) && isScaffoldItemPass(sI.Bracing_head_room__c) && isScaffoldItemPass(sI.Bracing_guy_wired__c) &&
               isScaffoldItemPass(sI.Bracing_free_standing__c) && isScaffoldItemPass(sI.Bracing_csa__c) && isScaffoldItemPass(sI.Access_scafford__c) &&
               isScaffoldItemPass(sI.Access_opening__c)) {
                
                sI.isCertified__c = 'Approved';
               }
            else {
                sI.isCertified__c = 'Violation or Incomplete';
            }
        }
    }
    
    public static Boolean isScaffoldItemPass(String value) {
        if(value == 'Yes' || value == 'Not Applicable')
            return True;
        else
            return False;
    }
    
    public static void setLastPassedInspection(List<Scaffold_Inspection__c> newItems) {
        Set<Id> scaffoldIds = new Set<Id>();
        Map<Id, Date> scaffoldLastPassedInspectionDateMap = new Map<Id, Date>();
        List<Scaffold__c> scaffoldToUpdate = new List<Scaffold__c>();
        
        for(Scaffold_Inspection__c inspection : newItems) {
            scaffoldIds.add(inspection.Scaffold__c);    
        }
        
        for(AggregateResult ar : [SELECT Scaffold__c, MAX(Date__c) LastInspectionDate FROM Scaffold_Inspection__c 
                                                 WHERE isCertified__c =: 'Approved' AND isLocked__c =: True
                                                 AND Scaffold__c IN :scaffoldIds GROUP BY Scaffold__c]) {
            scaffoldLastPassedInspectionDateMap.put((Id) ar.get('Scaffold__c'), (Date) ar.get('LastInspectionDate'));                                   
        }
        
        for(Id scaffoldId : scaffoldIds) {
            Scaffold__c scaffold = new Scaffold__c();
            scaffold.Id = scaffoldId;
            scaffold.Last_Passed_Inspection__c = scaffoldLastPassedInspectionDateMap.get(scaffoldId);
            
            scaffoldToUpdate.add(scaffold);            
        }
        
        if(scaffoldToUpdate.size() > 0)
            update scaffoldToUpdate;
    }
}