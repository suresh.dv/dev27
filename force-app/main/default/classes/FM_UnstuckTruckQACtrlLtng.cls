/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller for FM_UnstuckTruckQA Lightning component, to provide stuck truck record
Test Class:     FM_UnstuckTruckQACtrlLtngTest
History:        jschn 2019-02-12 - Created.
*************************************************************************************************/
public with sharing class FM_UnstuckTruckQACtrlLtng {

    @AuraEnabled
    public static HOG_CustomResponse getActiveStuckTruckId(String truckTripId) {
        HOG_CustomResponseImpl response;
        try {
            FM_Stuck_Truck__c stuckTruck = [
                    SELECT Id
                    FROM FM_Stuck_Truck__c
                    WHERE Truck_Trip__c = :truckTripId
                    AND Status__c = :FM_Utilities.STUCK_TRUCK_STATUS_STUCK
                    LIMIT 1
            ];
            response = new HOG_CustomResponseImpl();
            response.addResult(stuckTruck.Id);
        } catch (Exception ex) {
            response = new HOG_CustomResponseImpl('You cannot unstuck a trip that was never stuck!');
            response.addError(ex);
        }
        return response;
    }

}