/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
Test Class:     XXX
History:        jschn 26/08/2019 - Created.
*************************************************************************************************/
public with sharing class HOG_SO_FieldSelector implements VTT_SCFieldDAO {

    public List<Field__c> getAMUsByBusinessUnitName(String unitName) {
        return [
                SELECT Id, Name
                FROM Field__c
                WHERE Operating_District__r.Business_Unit__r.Name =: unitName
                ORDER BY Name
        ];
    }
}