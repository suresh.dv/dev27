/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Non parallel unit test for EPD_FormService
History:        jschn 2019-07-15 - Created.
*************************************************************************************************/
@IsTest
private class EPD_FormServiceTestNP {

    @IsTest //mocked
    static void handleLoadForExistingEPD_successLoad() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        EPD_Response result;
        EPD_DAOProvider.EPDDAO = new EPDMock_for_handleLoadForExistingEPD_single();
        EPD_DAOProvider.blockInformationDAO = new BlockInfoMock_for_handleLoadForExistingEPD();
        EPD_DAOProvider.engineConfigDAO = new EngineConfigMock_for_handleLoadForExistingEPD();

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForExistingEPD(epd, EPD_LoadMode.DIRECT_LOAD);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(null, result.errors);
        System.assertEquals(true, result.success);
        System.assertNotEquals(null, result.formStructure);
    }

    @IsTest //mocked
    static void handleLoadForExistingEPD_fail() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();
        EPD_Response result;
        EPD_DAOProvider.EPDDAO = new EPDMock_for_handleLoadForExistingEPD_multi();
        EPD_DAOProvider.blockInformationDAO = new BlockInfoMock_for_handleLoadForExistingEPD();
        EPD_DAOProvider.engineConfigDAO = new EngineConfigMock_for_handleLoadForExistingEPD();

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForExistingEPD(epd, EPD_LoadMode.DIRECT_LOAD);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.errors);
        System.assertEquals(1, result.errors.size());
        System.assert(result.errors.get(0).contains('Engine Performance Data'));
    }

    public class EPDMock_for_handleLoadForExistingEPD_single implements EPD_EPDDAO {
        public List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds) {
            return new List<EPD_Engine_Performance_Data__c> {
                    (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                            new EPD_Engine_Performance_Data__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            };
        }
        public List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId) {return null;}
        public List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds) {return null;}
        public Integer getCountOfSubmittedRecords(Id workOrderActivityId) {return 0;}
        public List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId) {return null;}
    }
    public class EPDMock_for_handleLoadForExistingEPD_multi implements EPD_EPDDAO {
        public List<EPD_Engine_Performance_Data__c> getEPDRecords(Set<Id> epdIds) {
            return new List<EPD_Engine_Performance_Data__c> {
                    (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                            new EPD_Engine_Performance_Data__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    ),
                    (EPD_Engine_Performance_Data__c) HOG_SObjectFactory.createSObject(
                            new EPD_Engine_Performance_Data__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            };
        }
        public List<EPD_Engine_Performance_Data__c> getEnginePerformanceDataByWOAId(Id workOrderActivityId) {return null;}
        public List<EPD_Engine_Performance_Data__c> getEPDRecordsByWorkOrderIds(Set<Id> woIds) {return null;}
        public Integer getCountOfSubmittedRecords(Id workOrderActivityId) {return 0;}
        public List<EPD_Engine_Performance_Data__c> getPreviousEPDByEngineId(Id engineId) {return null;}
    }
    public class BlockInfoMock_for_handleLoadForExistingEPD implements EPD_BlockInformationDAO {
        public List<EPD_Block_Information__c> getBlockInformationByEPDs(Set<Id> epdIds) {
            return new List<EPD_Block_Information__c> {
                    (EPD_Block_Information__c) HOG_SObjectFactory.createSObject(
                            new EPD_Block_Information__c(),
                            EPD_FieldDefaultsGeneral.CLASS_NAME,
                            false
                    )
            };
        }
    }
    public class EngineConfigMock_for_handleLoadForExistingEPD implements EPD_EngineConfigDAO {
        public List<EPD_Engine__mdt> getEngineConfig(String manufacturer, String model) {return null;}
        public List<EPD_Engine__mdt> getEngineConfigs() {
            return new List<EPD_Engine__mdt> {
                    new EPD_Engine__mdt()
            };
        }
    }

    @IsTest
    static void handleFormSave_withoutParam() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructure structure;
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleFormSave(structure);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void handleFormSave_withProperParamUpdate() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedEPDs = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c] + 1;
        Integer expectedParts = [SELECT COUNT() FROM EPD_Part_Replacement__c] + 1;
        Integer expectedBlockInfos = [SELECT COUNT() FROM EPD_Block_Information__c] + 1;
        Integer expectedCOCStages = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c] + 1;
        Integer expectedCylinders = [SELECT COUNT() FROM EPD_Cylinder_Information__c] + 1;
        EPD_FormStructure structure = prepareStructureForPersist();
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleFormSave(structure);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(true, result.success);
        System.assertEquals(expectedEPDs, [SELECT COUNT() FROM EPD_Engine_Performance_Data__c]);
        System.assertEquals(expectedParts, [SELECT COUNT() FROM EPD_Part_Replacement__c]);
        System.assertEquals(expectedBlockInfos, [SELECT COUNT() FROM EPD_Block_Information__c]);
        System.assertEquals(expectedCOCStages, [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c]);
        System.assertEquals(expectedCylinders, [SELECT COUNT() FROM EPD_Cylinder_Information__c]);
    }

    @IsTest
    static void handleFormSave_withProperParamInsert() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        Integer expectedEPDs = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c] + 2;
        Integer expectedParts = [SELECT COUNT() FROM EPD_Part_Replacement__c] + 1;
        Integer expectedBlockInfos = [SELECT COUNT() FROM EPD_Block_Information__c] + 1;
        Integer expectedCOCStages = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c] + 1;
        Integer expectedCylinders = [SELECT COUNT() FROM EPD_Cylinder_Information__c] + 1;
        EPD_FormStructure structure = prepareStructureForPersist();
        Id previousEPDId = structure.epd.recordId;
        structure.epd.recordId = null;
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleFormSave(structure);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(previousEPDId, result.formStructure.epd.previousEPD);
        System.assertEquals(true, result.success);
        System.assertEquals(expectedEPDs, [SELECT COUNT() FROM EPD_Engine_Performance_Data__c]);
        System.assertEquals(expectedParts, [SELECT COUNT() FROM EPD_Part_Replacement__c]);
        System.assertEquals(expectedBlockInfos, [SELECT COUNT() FROM EPD_Block_Information__c]);
        System.assertEquals(expectedCOCStages, [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c]);
        System.assertEquals(expectedCylinders, [SELECT COUNT() FROM EPD_Cylinder_Information__c]);
    }

    @IsTest
    static void handleFormSave_fail() {
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;
        EPD_FormStructure structure = prepareStructureForPersist();
        Integer expectedEPDs = [SELECT COUNT() FROM EPD_Engine_Performance_Data__c];
        Integer expectedParts = [SELECT COUNT() FROM EPD_Part_Replacement__c];
        Integer expectedBlockInfos = [SELECT COUNT() FROM EPD_Block_Information__c];
        Integer expectedCOCStages = [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c];
        Integer expectedCylinders = [SELECT COUNT() FROM EPD_Cylinder_Information__c];
        structure.epd.submitted = true;
        structure.epd.hours = null;
        EPD_Response result;

        Test.startTest();
        try {
            result = new EPD_FormService().handleFormSave(structure);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
        System.assertEquals(expectedEPDs, [SELECT COUNT() FROM EPD_Engine_Performance_Data__c]);
        System.assertEquals(expectedParts, [SELECT COUNT() FROM EPD_Part_Replacement__c]);
        System.assertEquals(expectedBlockInfos, [SELECT COUNT() FROM EPD_Block_Information__c]);
        System.assertEquals(expectedCOCStages, [SELECT COUNT() FROM EPD_Compressor_Operating_Condition_Stage__c]);
        System.assertEquals(expectedCylinders, [SELECT COUNT() FROM EPD_Cylinder_Information__c]);
    }

    @IsTest
    static void handleLoadForNewEPD_secondAttemptSuccess() {
        Boolean expectedFailFlag = false;
        Boolean failFlag = false;
        EPD_Response result;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        epd.Equipment_Engine__r = new Equipment_Engine__c(Id = epd.Equipment_Engine__c);
        Equipment_Engine__c eqe = [SELECT Equipment__c FROM Equipment_Engine__c WHERE Id =: epd.Equipment_Engine__c LIMIT 1];
        HOG_Maintenance_Servicing_Form__c wo = [SELECT Id FROM HOG_Maintenance_Servicing_Form__c WHERE Id =: epd.Work_Order__c LIMIT 1];
        wo.Equipment__c = eqe.Equipment__c;
        update wo;
        Work_Order_Activity__c woa = [SELECT Id FROM Work_Order_Activity__c WHERE Id =: epd.Work_Order_Activity__c LIMIT 1];
        woa.Assigned_Vendor__c = epd.Company__c;
        update woa;

        Test.startTest();
        try {
            result = new EPD_FormService().handleLoadForNewEPD(epd.Work_Order_Activity__c);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(true, result.success);
        System.assertNotEquals(null, result.formStructure);
    }

    private static EPD_FormStructure prepareStructureForPersist(){
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createSingleEPD();
        EPD_FormStructurePart part = (EPD_FormStructurePart) new EPD_FormStructurePart().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Part_Replacement__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        EPD_FormStructureCOCStage stage = (EPD_FormStructureCOCStage) new EPD_FormStructureCOCStage().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Compressor_Operating_Condition_Stage__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        EPD_FormStructureBlock block = (EPD_FormStructureBlock) new EPD_FormStructureBlock().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Block_Information__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        );
        block.cylinders = new List<EPD_FormStructureCylinder> {
                (EPD_FormStructureCylinder) new EPD_FormStructureCylinder().setRecord(
                        HOG_SObjectFactory.createSObject(
                                new EPD_Cylinder_Information__c(),
                                EPD_FieldDefaultsGeneral.CLASS_NAME,
                                false
                        )
                )
        };
        EPD_FormStructureEOC eoc = ((EPD_FormStructureEOC) new EPD_FormStructureEOC().setRecord(
                HOG_SObjectFactory.createSObject(
                        new EPD_Engine_Operating_Condition__c(),
                        EPD_FieldDefaultsGeneral.CLASS_NAME,
                        false
                )
        )).setFlag(
                new EPD_Engine__mdt(
                        Advanced_EOC__c = true
                )
        );
        return new EPD_FormStructure()
                .setEPDStructure((EPD_FormStructureEPD) new EPD_FormStructureEPD().setRecord(epd))
                .setParts(new List<EPD_FormStructurePart> {part})
                .setCOCStages(new List<EPD_FormStructureCOCStage> {stage})
                .setEngineBlocks(new List<EPD_FormStructureBlock> {block})
                .setEOCs(new List<EPD_FormStructureEOC> {eoc});
    }

}