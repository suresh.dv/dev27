@isTest
public class WellEventTestData {
	public static Well_Event__c createEvent (String name, Id locationId)
    {                
        Well_Event__c testEvent = new Well_Event__c
            (           
                Name = name,
                Well_Id__c = locationId
            );            

        insert testEvent;
        return testEvent;
    }

    public static Well_Event__c createEvent()
    {
        Location__c testLocation = LocationTestData.createLocation();

        Well_Event__c testEvent = new Well_Event__c (Name = 'Test Well Event', Well_ID__c = testLocation.Id); 
        insert testEvent;
        return testEvent;
    }
}