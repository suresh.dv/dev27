/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    VTT_LTNG_WorkFlowEngineConstants
Test Class:     VTT_LTNG_WorkFlowEngineConstantsTest
History:        mbrimus 25/09/2019. - Created.
*************************************************************************************************/
public class VTT_LTNG_WorkFlowEngineConstants {
    // Alert theme
    public static final String SEVERITY_WARNING = 'slds-theme_warning';
    public static final String SEVERITY_ERROR = 'slds-theme_error';

    public static final String START_JOB_AT_EQUIPMENT = 'Start Job At Equipment';
    public static final String REJECT_ACTIVITY = 'Reject Activity';

    // WORKFLOW ACTIONS BUTTON LABELS
    public static final String START_JOB_LABEL = System.Label.VTT_LTNG_Start_Job_Button_Label;
    public static final String START_AT_EQUIPMENT_LABEL = System.Label.VTT_LTNG_Start_At_Equipment_Button_Label;
    public static final String START_JOB_AT_EQUIPMENT_LABEL = System.Label.VTT_LTNG_Start_Job_At_Equipment_Button_Label;
    public static final String FINISHED_JOB_AT_EQUIPMENT_LABEL = System.Label.VTT_LTNG_Finished_Job_At_Equipment_Button_Label;
    public static final String FINISHED_FOR_TODAY_LABEL = System.Label.VTT_LTNG_Finished_for_the_Day_Button_Label;
    public static final String JOB_COMPLETE_LABEL = System.Label.VTT_LTNG_Job_Complete_Button_Label;
    public static final String JOB_ONHOLD_LABEL = System.Label.VTT_LTNG_Job_On_Hold_Button_Label;
    public static final String REJECT_ACTIVITY_LABEL = System.Label.VTT_LTNG_Reject_Activity_Button_Label;
    public static final String EPD_LABEL = System.Label.VTT_LTNG_EPD_Button_Label;

    private static final VTT_LTNG_EngineAction START_JOB_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            VTT_Utilities.LOGENTRY_STARTJOB, START_JOB_LABEL, System.Label.VTT_LTNG_Start_Job_Action, 1
    );

    private static final VTT_LTNG_EngineAction START_AT_EQUIPMENT_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            VTT_Utilities.LOGENTRY_STARTATSITE, START_AT_EQUIPMENT_LABEL, System.Label.VTT_LTNG_Start_At_Equipment_Action, 2
    );

    private static final VTT_LTNG_EngineAction START_JOB_AT_EQUIPMENT_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            VTT_Utilities.LOGENTRY_STARTATSITE, START_JOB_AT_EQUIPMENT_LABEL, System.Label.VTT_LTNG_Start_Job_At_Equipment_Action, 3
    );

    private static final VTT_LTNG_EngineAction FINISHED_JOB_AT_EQUIPMENT_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            VTT_Utilities.LOGENTRY_FINISHEDATSITE, FINISHED_JOB_AT_EQUIPMENT_LABEL, System.Label.VTT_LTNG_Finished_Job_At_Equipment_Action, 4
    );

    private static final VTT_LTNG_EngineAction FINISHED_FOR_TODAY_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY, FINISHED_FOR_TODAY_LABEL, System.Label.VTT_LTNG_Finished_for_the_Day_Action, 5
    );

    private static final VTT_LTNG_EngineAction EPD_ACTION = new VTT_LTNG_EngineAction(
            null, EPD_LABEL, System.Label.VTT_LTNG_EPD_Action, 6
    );

    private static final VTT_LTNG_EngineAction ON_HOLD_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            VTT_Utilities.LOGENTRY_JOBONHOLD, JOB_ONHOLD_LABEL, System.Label.VTT_LTNG_Job_On_Hold_Action, 7
    );

    private static final VTT_LTNG_EngineAction JOB_COMPLETE_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            VTT_Utilities.LOGENTRY_JOBCOMPLETE, JOB_COMPLETE_LABEL, System.Label.VTT_LTNG_Job_Complete_Action, 8
    );

    private static final VTT_LTNG_EngineAction REJECT_ACTIVITY_ACTION = new VTT_LTNG_EngineAction(
            REJECT_ACTIVITY, REJECT_ACTIVITY_LABEL, System.Label.VTT_LTNG_Reject_Activity_Action, 9
    );

    public final static Map<String, VTT_LTNG_EngineAction> WORKFLOW_ACTION_MAP
            = new Map<String, VTT_LTNG_EngineAction>{
                    VTT_Utilities.LOGENTRY_STARTJOB => START_JOB_ACTIVITY_ACTION,
                    VTT_Utilities.LOGENTRY_STARTATSITE => START_AT_EQUIPMENT_ACTIVITY_ACTION,
                    START_JOB_AT_EQUIPMENT => START_JOB_AT_EQUIPMENT_ACTIVITY_ACTION,
                    VTT_Utilities.LOGENTRY_FINISHEDATSITE => FINISHED_JOB_AT_EQUIPMENT_ACTIVITY_ACTION,
                    VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY => FINISHED_FOR_TODAY_ACTIVITY_ACTION,
                    VTT_Utilities.LOGENTRY_JOBONHOLD => ON_HOLD_ACTIVITY_ACTION,
                    VTT_Utilities.LOGENTRY_JOBCOMPLETE => JOB_COMPLETE_ACTIVITY_ACTION,
                    REJECT_ACTIVITY => REJECT_ACTIVITY_ACTION,
                    EPD_LABEL => EPD_ACTION
            };
    
    public static Map<String, String[]> TRADESMAN_AND_ACTIVITY_STATUS_MAP = new Map<String, String[]>{
            VTT_Utilities.LOGENTRY_STARTJOB => new String[]{
                    VTT_Utilities.TRADESMAN_STATUS_PREWORK,
                    VTT_Utilities.ACTIVITY_STATUS_STARTED
            },
            VTT_Utilities.LOGENTRY_STARTATSITE => new String[]{
                    VTT_Utilities.TRADESMAN_STATUS_WORKING,
                    VTT_Utilities.ACTIVITY_STATUS_STARTEDATSITE
            },
            VTT_Utilities.LOGENTRY_FINISHEDATSITE => new String[]{
                    VTT_Utilities.TRADESMAN_STATUS_POSTWORK,
                    VTT_Utilities.ACTIVITY_STATUS_FINISHEDATSITE
            },
            VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY => new String[]{
                    VTT_Utilities.TRADESMAN_STATUS_NOTWORKING,
                    VTT_Utilities.ACTIVITY_STATUS_FINISHEDFORTHEDAY
            },
            VTT_Utilities.LOGENTRY_JOBONHOLD => new String[]{
                    VTT_Utilities.TRADESMAN_STATUS_NOTWORKING,
                    VTT_Utilities.ACTIVITY_STATUS_ONHOLD
            },
            VTT_Utilities.LOGENTRY_JOBCOMPLETE => new String[]{
                    VTT_Utilities.TRADESMAN_STATUS_NOTWORKING,
                    VTT_Utilities.ACTIVITY_STATUS_COMPLETED
            }
    };
}