@isTest
private class PREPTaxonomyChangeControllerTest {
    
    static testmethod void defaultMethods() {
    
        Id uid = UserInfo.getUserId();
                
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = '"Engineering & Construction', Active__c = true,
                                                            SCM_Manager__c = uId );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id,
                                        Category_Manager__c = uId, Category_Specialist__c = uId,
                                        Active__c = true );
        insert sub;
       
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id,
                                    Category__c = cat.Id, Sub_Category__c = sub.Id, Status__c = 'Active',
                                    Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                    Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes',
                                    SRPM_Package__c = 'As Needed', Aboriginal__c = 'No', Global_Sourcing__c = 'Yes',
                                    OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                    Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100,
                                    Reverse_Auction__c = 'No', Rebate__c = 'Multiple Area Award',
                                    Rebate_Description__c = 'testing', Rebate_Frequency__c = 'Annually',
                                    Volume_Discount__c='Yes', Volume_Discount_Description__c= 'testing',
                                    Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30',
                                    SCM_Manager__c = uId, SCM_Team__c='"E&C - Services',
                                    Category_Manager__c = uId, Category_Specialist__c = uId, GSO__c = uId );
        insert ini;
        
        ini = [ Select Id, Name from PREP_Initiative__c ];
        
        PREP_Change_Header__c pch = new PREP_Change_Header__c( Approval_Status__c='New', Change_Status__c= 'Approved',
                                        Object_Name__c='PREP_Material_Service_Group__c', Change_Type__c='Change Owner',
                                        Request_Title__c='testing', Request_Status__c='new' );
        insert pch;
        
        PREP_Change_Detail__c pcd = new PREP_Change_Detail__c( PREP_Change_Header__c=pch.id, Field_Name__c='CategoryManager',
                                        From_Field_value__c=uid, To_Field_value__c= uid, Update_Record_Name__c=ini.Name,
                                        Record_Id__c= uid );
        insert pcd;

        pch = [ SELECT Id, Name FROM PREP_Change_Header__c ];
        
        ApexPages.currentPage().getParameters().put('change', pch.Name);
        ApexPages.currentPage().getParameters().put('id', pch.Id);
        PREPTaxonomyChangeController controller = new PREPTaxonomyChangeController(new ApexPages.StandardController(ini));
        controller.getPrepChange();
        controller.cancelRequest();
        controller.submitForApproval();
    }
    
    static testmethod void materialServiceGroupChanges() {
    
        Id uid = UserInfo.getUserId();
                
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true,
                                                            SCM_Manager__c = uId );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id,
                                        Category_Manager__c = uId, Category_Specialist__c = uId,
                                        Active__c = true );
        insert sub;
       
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id,
                                    Category__c = cat.Id, Sub_Category__c = sub.Id, Status__c = 'Active',
                                    Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                    Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes',
                                    SRPM_Package__c = 'As Needed', Aboriginal__c = 'No', Global_Sourcing__c = 'Yes',
                                    OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                    Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100,
                                    Reverse_Auction__c = 'No', Rebate__c = 'Multiple Area Award',
                                    Rebate_Description__c = 'testing', Rebate_Frequency__c = 'Annually',
                                    Volume_Discount__c='Yes', Volume_Discount_Description__c= 'testing',
                                    Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30',
                                    SCM_Manager__c = uId, SCM_Team__c='E&C - Equipment',
                                    Category_Manager__c = uId, Category_Specialist__c = uId, GSO__c = uId );
        insert ini;
                
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c( Name = 'MS Name', Material_Service_Group_Name__c = 'MS',
                                                Sub_Category__c = sub.Id, Category_Manager__c = uId, SAP_Short_Text_Name__c = 'MS',
                                                Active__c = true, Type__c = 'Material', Category_Specialist__c = uId,
                                                Includes__c = 'MS', Does_Not_Include__c = 'MS', Category_Link__c = cat.Id, Discipline_Link__c = disp.Id );
        insert MS;
        
        MS = [ Select Id, Name from PREP_Material_Service_Group__c ];
        
        PREP_Change_Header__c pch = new PREP_Change_Header__c( Approval_Status__c='New', Change_Status__c= 'Approved',
                                        Object_Name__c='PREP_Material_Service_Group__c', Change_Type__c='Change Owner',
                                        Request_Title__c='testing', Request_Status__c='new' );
        insert pch;
        
        PREP_Change_Detail__c pcd = new PREP_Change_Detail__c( PREP_Change_Header__c=pch.id, Field_Name__c='CategoryManager',
                                        From_Field_value__c=uid, To_Field_value__c= uid, Update_Record_Name__c=MS.Name,
                                        Record_Id__c= uid );
        insert pcd;

        pch = [ SELECT Id, Name FROM PREP_Change_Header__c ];
        
        test.startTest();
            ApexPages.currentPage().getParameters().put('change', pch.Name);
            ApexPages.currentPage().getParameters().put('id', pch.Id);
            PREPTaxonomyChangeController controller = new PREPTaxonomyChangeController(new ApexPages.StandardController(ini));
            controller.commitChanges();
        test.stopTest();
    }
    
    static testmethod void projectInitiativeChanges() {
    
        Id uid = UserInfo.getUserId();
                
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true,
                                                            SCM_Manager__c = uId );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id,
                                        Category_Manager__c = uId, Category_Specialist__c = uId,
                                        Active__c = true );
        insert sub;
       
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id,
                                    Category__c = cat.Id, Sub_Category__c = sub.Id, Status__c = 'Active',
                                    Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                    Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes',
                                    SRPM_Package__c = 'As Needed', Aboriginal__c = 'No', Global_Sourcing__c = 'Yes',
                                    OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                    Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100,
                                    Reverse_Auction__c = 'No', Rebate__c = 'Multiple Area Award',
                                    Rebate_Description__c = 'testing', Rebate_Frequency__c = 'Annually',
                                    Volume_Discount__c='Yes', Volume_Discount_Description__c= 'testing',
                                    Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30',
                                    SCM_Manager__c = uId, SCM_Team__c='E&C - Equipment',
                                    Category_Manager__c = uId, Category_Specialist__c = uId, GSO__c = uId );
        insert ini;
        
        ini = [ Select Id, Name from PREP_Initiative__c ];
        
        PREP_Change_Header__c pch = new PREP_Change_Header__c( Approval_Status__c='New', Change_Status__c= 'Approved',
                                        Object_Name__c='PREP_prjinitiative__c', Change_Type__c='Change Owner',
                                        Request_Title__c='testing', Request_Status__c='new' );
        insert pch;
        
        PREP_Change_Detail__c pcd = new PREP_Change_Detail__c( PREP_Change_Header__c=pch.id, Field_Name__c='CategoryManager',
                                        From_Field_value__c=uid, To_Field_value__c= uid, Update_Record_Name__c=ini.Name,
                                        Record_Id__c= uid );
        insert pcd;

        pch = [ SELECT Id, Name FROM PREP_Change_Header__c ];
        
        test.startTest();
            ApexPages.currentPage().getParameters().put('change', pch.Name);
            ApexPages.currentPage().getParameters().put('id', pch.Id);
            PREPTaxonomyChangeController controller = new PREPTaxonomyChangeController(new ApexPages.StandardController(ini));
            controller.commitChanges();
        test.stopTest();
    }
    
    static testmethod void initiativeChanges() {
    
        Id uid = UserInfo.getUserId();
                
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true,
                                                            SCM_Manager__c = uId );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id,
                                        Category_Manager__c = uId, Category_Specialist__c = uId,
                                        Active__c = true );
        insert sub;
       
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id,
                                    Category__c = cat.Id, Sub_Category__c = sub.Id, Status__c = 'Active',
                                    Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                    Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes',
                                    SRPM_Package__c = 'As Needed', Aboriginal__c = 'No', Global_Sourcing__c = 'Yes',
                                    OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                    Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100,
                                    Reverse_Auction__c = 'No', Rebate__c = 'Multiple Area Award',
                                    Rebate_Description__c = 'testing', Rebate_Frequency__c = 'Annually',
                                    Volume_Discount__c='Yes', Volume_Discount_Description__c= 'testing',
                                    Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30',
                                    SCM_Manager__c = uId, SCM_Team__c='E&C - Equipment',
                                    Category_Manager__c = uId, Category_Specialist__c = uId, GSO__c = uId );
        insert ini;
        
        ini = [ Select Id, Name from PREP_Initiative__c ];
        
        PREP_Change_Header__c pch = new PREP_Change_Header__c( Approval_Status__c='New', Change_Status__c= 'Approved',
                                        Object_Name__c='PREP_Initiative__c', Change_Type__c='Change Owner',
                                        Request_Title__c='testing', Request_Status__c='new' );
        insert pch;
        
        PREP_Change_Detail__c pcd = new PREP_Change_Detail__c( PREP_Change_Header__c=pch.id, Field_Name__c='CategoryManager',
                                        From_Field_value__c=uid, To_Field_value__c= uid, Update_Record_Name__c=ini.Name,
                                        Record_Id__c= uid );
        insert pcd;

        pch = [ SELECT Id, Name FROM PREP_Change_Header__c ];
        
        test.startTest();
            ApexPages.currentPage().getParameters().put('change', pch.Name);
            ApexPages.currentPage().getParameters().put('id', pch.Id);
            PREPTaxonomyChangeController controller = new PREPTaxonomyChangeController(new ApexPages.StandardController(ini));
            controller.commitChanges();
        test.stopTest();
    }
    
    static testmethod void createTaxonomyChanges() {
    
        Id uid = UserInfo.getUserId();
                
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true,
                                                            SCM_Manager__c = uId );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id,
                                        Category_Manager__c = uId, Category_Specialist__c = uId,
                                        Active__c = true );
        insert sub;
       
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id,
                                    Category__c = cat.Id, Sub_Category__c = sub.Id, Status__c = 'Active',
                                    Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                    Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes',
                                    SRPM_Package__c = 'As Needed', Aboriginal__c = 'No', Global_Sourcing__c = 'Yes',
                                    OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                    Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100,
                                    Reverse_Auction__c = 'No', Rebate__c = 'Multiple Area Award',
                                    Rebate_Description__c = 'testing', Rebate_Frequency__c = 'Annually',
                                    Volume_Discount__c='Yes', Volume_Discount_Description__c= 'testing',
                                    Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30',
                                    SCM_Manager__c = uId, SCM_Team__c='E&C - Equipment',
                                    Category_Manager__c = uId, Category_Specialist__c = uId, GSO__c = uId );
        insert ini;
        
        ini = [ Select Id, Name from PREP_Initiative__c ];
        
        Material_Service_Group_Clone__c materialGroup = new Material_Service_Group_Clone__c( Name = 'MS Name', Material_Service_Group_Name__c = 'MS',
                                                        Sub_Category__c = sub.Id, Category_Manager__c = uId, SAP_Short_Text_Name__c = 'MS', 
                                                        Active__c = true, Type__c = 'Material', Category_Specialist__c = uId, Includes__c = 'MS',
                                                        Does_Not_Include__c = 'MS', Category_Link__c = cat.Id, Discipline__c = disp.Id );
        insert materialGroup;
        
        PREP_Change_Header__c pch = new PREP_Change_Header__c( Approval_Status__c='New', Change_Status__c= 'Approved',
                                        Object_Name__c='PREP_Material_Service_Group__c', Change_Type__c = 'Taxonomy Created',
                                        Request_Title__c='testing', Request_Status__c='new' );
        insert pch;
        
        PREP_Change_Detail__c pcd = new PREP_Change_Detail__c( PREP_Change_Header__c=pch.id, Field_Name__c='Taxonomy',
                                        From_Field_value__c=uid, To_Field_value__c= uid, Update_Record_Name__c=ini.Name,
                                        Record_Id__c= uid, To_Field_Id__c = materialGroup.Id );
        insert pcd;

        pch = [ SELECT Id, Name FROM PREP_Change_Header__c ];
        
        test.startTest();
            ApexPages.currentPage().getParameters().put('change', pch.Name);
            ApexPages.currentPage().getParameters().put('id', pch.Id);
            PREPTaxonomyChangeController controller = new PREPTaxonomyChangeController(new ApexPages.StandardController(ini));
            controller.getPrepChange();
            controller.commitChanges();
            controller.saveGLChanges();
        test.stopTest();
    }
    
    static testmethod void retireTaxonomyChanges() {
    
        Id uid = UserInfo.getUserId();
                
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true,
                                                            SCM_Manager__c = uId );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id,
                                        Category_Manager__c = uId, Category_Specialist__c = uId,
                                        Active__c = true );
        insert sub;
       
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id,
                                    Category__c = cat.Id, Sub_Category__c = sub.Id, Status__c = 'Active',
                                    Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                    Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes',
                                    SRPM_Package__c = 'As Needed', Aboriginal__c = 'No', Global_Sourcing__c = 'Yes',
                                    OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                    Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100,
                                    Reverse_Auction__c = 'No', Rebate__c = 'Multiple Area Award',
                                    Rebate_Description__c = 'testing', Rebate_Frequency__c = 'Annually',
                                    Volume_Discount__c='Yes', Volume_Discount_Description__c= 'testing',
                                    Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30',
                                    SCM_Manager__c = uId, SCM_Team__c='E&C - Equipment',
                                    Category_Manager__c = uId, Category_Specialist__c = uId, GSO__c = uId );
        insert ini;
        
        ini = [ Select Id, Name from PREP_Initiative__c ];
        
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c( Name = 'MS Name', Material_Service_Group_Name__c = 'MS',
                                                Sub_Category__c = sub.Id, Category_Manager__c = uId, SAP_Short_Text_Name__c = 'MS',
                                                Active__c = true, Type__c = 'Material', Category_Specialist__c = uId,
                                                Includes__c = 'MS', Does_Not_Include__c = 'MS', Category_Link__c = cat.Id, Discipline_Link__c = disp.Id );
        insert MS;
        
        MS = [ Select Id, Name from PREP_Material_Service_Group__c ];
        
        PREP_Change_Header__c pch = new PREP_Change_Header__c( Approval_Status__c='New', Change_Status__c= 'Approved',
                                        Object_Name__c='PREP_Material_Service_Group__c', Change_Type__c = 'Retire Taxonomy',
                                        Request_Title__c='testing', Request_Status__c='new' );
        insert pch;
        
        PREP_Change_Detail__c pcd = new PREP_Change_Detail__c( PREP_Change_Header__c=pch.id, Field_Name__c='Active',
                                        From_Field_value__c='', To_Field_value__c= 'Retire it', Update_Record_Name__c= MS.Name,
                                        Record_Id__c= uid, To_Field_Id__c = MS.Id );
        insert pcd;

        pch = [ SELECT Id, Name FROM PREP_Change_Header__c ];
        
        test.startTest();
            ApexPages.currentPage().getParameters().put('change', pch.Name);
            ApexPages.currentPage().getParameters().put('id', pch.Id);
            PREPTaxonomyChangeController controller = new PREPTaxonomyChangeController(new ApexPages.StandardController(ini));
            controller.getPrepChange();
            controller.commitChanges();
            //controller.saveGLChanges();
        test.stopTest();
    }

}