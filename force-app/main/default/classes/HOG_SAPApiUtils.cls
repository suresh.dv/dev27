/** This class is a utility class for WebServices and other API classes */
global with sharing class HOG_SAPApiUtils {
    /** Return codes for record */
    global final static String STATUS_SUCCESS_PARSE = 'PARSING_SUCCESS';
    global final static String STATUS_SUCCESS_UPSERT = 'UPSERT_SUCCESS';
    global final static String STATUS_ERROR_PARSE = 'PARSING_ERROR';
    global final static String STATUS_ERROR_UPSERT = 'UPSERT_ERROR';
    global final static String STATUS_ERROR_INVALID_RECORD = 'INVALID_RECORD';

    public virtual class UnknownException extends Exception {}
    public virtual class BadException extends Exception {}

    /****************** FLOC Service Utilities and Classes ********************/
    global class DT_SAP_RIHIFLO_LIST {
  		webservice String TPLNR_INT {get; set;}
  		webservice String TPLMA_INT {get; set;}
  		webservice String SPRAS {get; set;}
  		webservice String PLTXT {get; set;}
  		webservice String FLTYP {get; set;}
  		webservice String IWERK {get; set;}
  		webservice String IWERKI {get; set;}
  		webservice String SWERK {get; set;}
  		webservice String SWERKI {get; set;}
  		webservice String STORT {get; set;}
  		webservice String STORTI {get; set;}
  		webservice String MSGRP {get; set;}
  		webservice String MSGRPI {get; set;}
  		webservice String BEBER {get; set;}
  		webservice String BEBERI {get; set;}
  		webservice String ABCKZ {get; set;}
  		webservice String ABCKZI {get; set;}
  		webservice String EQFNR {get; set;}
  		webservice String EQFNRI {get; set;}
  		webservice String BUKRS {get; set;}
  		webservice String BUKRSI {get; set;}
  		webservice String ANLNR {get; set;}
  		webservice String ANLNRI {get; set;}
  		webservice String ANLUN {get; set;}
  		webservice String RKEOBJNR {get; set;}
  		webservice String GSBER {get; set;}
  		webservice String GSBERI {get; set;}
  		webservice String KOSTL {get; set;}
  		webservice String KOSTLI {get; set;}
  		webservice String DAUFN {get; set;}
  		webservice String DAUFNI {get; set;}
  		webservice String AUFNR {get; set;}
  		webservice String AUFNRI {get; set;}
  		webservice String TRPNR {get; set;}
  		webservice String TPLKZ {get; set;}
  		webservice String IEQUI {get; set;}
  		webservice String IEQUII {get; set;}
  		webservice String EINZL {get; set;}
  		webservice String EINZLI {get; set;}
  		webservice String TPLMA {get; set;}
  		webservice String POSNR {get; set;}
  		webservice String SUBMT {get; set;}
  		webservice String SUBMTI {get; set;}
  		webservice String MAPAR {get; set;}
  		webservice String MAPARI {get; set;}
  		webservice String ARBPL {get; set;}
  		webservice String PPSIDI {get; set;}
  		webservice String GEWRK {get; set;}
  		webservice String LGWIDI {get; set;}
  		webservice String ERDAT {get; set;}
  		webservice String ERNAM {get; set;}
  		webservice String AEDAT {get; set;}
  		webservice String AENAM {get; set;}
  		webservice String DATAB {get; set;}
  		webservice String BEGRU {get; set;}
  		webservice String LVORM {get; set;}
  		webservice String RBNR {get; set;}
  		webservice String RBNR_I {get; set;}
  		webservice String INGRP {get; set;}
  		webservice String INGRPI {get; set;}
  		webservice String KOKRS {get; set;}
  		webservice String KOKRSI {get; set;}
  		webservice String PROID {get; set;}
  		webservice String PROIDI {get; set;}
  		webservice String STTXT {get; set;}
  		webservice String ILOAN {get; set;}
  		webservice String VKORG {get; set;}
  		webservice String VKORGI {get; set;}
  		webservice String VTWEG {get; set;}
  		webservice String VTWEGI {get; set;}
  		webservice String SPART {get; set;}
  		webservice String SPARTI {get; set;}
  		webservice String USTXT {get; set;}
  		webservice String NAME_LIST {get; set;}
  		webservice String TEL_NUMBER {get; set;}
  		webservice String POST_CODE1 {get; set;}
  		webservice String CITY1 {get; set;}
  		webservice String CITY2 {get; set;}
  		webservice String COUNTRY {get; set;}
  		webservice String REGION {get; set;}
  		webservice String STREET {get; set;}
  		webservice String MANDT {get; set;}
  		webservice String ADRNR {get; set;}
  		webservice String ADRNRI {get; set;}
  		webservice String OBJNR {get; set;}
  		webservice String KZLTX {get; set;}
  		webservice String SUBMTKTX {get; set;}
  		webservice String EQART {get; set;}
  		webservice String INVNR {get; set;}
  		webservice String BRGEW {get; set;}
  		webservice String GEWEI {get; set;}
  		webservice String GROES {get; set;}
  		webservice String ANSWT {get; set;}
  		webservice String WAERS {get; set;}
  		webservice String ANSDT {get; set;}
  		webservice String HERST {get; set;}
  		webservice String HERLD {get; set;}
  		webservice String TYPBZ {get; set;}
  		webservice String BAUJJ {get; set;}
  		webservice String BAUMM {get; set;}
  		webservice String SERGE {get; set;}
  		webservice String VKBUR {get; set;}
  		webservice String VKGRP {get; set;}
  		webservice String TPLNR {get; set;}
  		webservice String TPLNR_0 {get; set;}
  		webservice String TPLNR_1 {get; set;}
  		webservice String TPLNR_2 {get; set;}
  		webservice String TPLNR_3 {get; set;}
  		webservice String ALKEY {get; set;}
  		webservice String GWLDT_K {get; set;}
  		webservice String GWLEN_K {get; set;}
  		webservice String MGANR_K {get; set;}
  		webservice String GARTX_K {get; set;}
  		webservice String GWLDT_L {get; set;}
  		webservice String GWLEN_L {get; set;}
  		webservice String MGANR_L {get; set;}
  		webservice String GARTX_L {get; set;}
  		webservice String DFPS_CP {get; set;}
  		webservice String DFPS_CP_SYS {get; set;}
  		webservice String DFPS_CP_SYST {get; set;}
  	}

  	global class DT_SAP_Functional_Location {
  		webservice List<DT_SAP_FUNC_LOC> FLOC {get; set;}
  	}

  	global class DT_ECC_FLOC_CHAR_DATA {
  		webservice String TPLNR {get; set;}
  		//webservice String CLASS {get; set;}
  		webservice String ATNAM {get; set;}
  		webservice String ZVAL {get; set;}
  		webservice String MSEHI {get; set;}
  		webservice String MSEHL {get; set;}
  	}

  	global class DT_SAP_CI_IFLOT {
  		webservice String ZZ_LSD {get; set;}
  		webservice String ZZ_WINTER_ACCESS {get; set;}
  		webservice String ZZ_SWB {get; set;}
  		webservice String ZZ_NOEQ {get; set;}
  		webservice String ZZ_ZONE {get; set;}
  		webservice String ZZ_POOL {get; set;}
  		webservice String ZZ_CORRELATION {get; set;}
  		webservice String ZZ_SWM_FLAG {get; set;}
  		webservice String ZZ_STREAM_DAY {get; set;}
  		webservice String ZZ_WELL_TYPE {get; set;}
  		webservice String ZZ_WELL_ORIENT {get; set;}
  		webservice String ZZ_UNIT_CONFIG {get; set;}
  		webservice String ZZ_SCADA_ID {get; set;}
  		webservice String ZZ_MTU_NO {get; set;}
  		webservice String ZZ_OIL_GRAVITY {get; set;}
  		webservice String ZZ_STREAM_DAY_EM {get; set;}
  		webservice String ZZ_H2S_LOCATION {get; set;}
  	}

  	global class DT_SAP_FUNC_LOC {
  		webservice DT_SAP_RIHIFLO_LIST RIHIFLO_LST {get; set;}
  		webservice DT_SAP_CI_IFLOT CI_IFLOT {get; set;}
  		webservice String RECTYPE {get; set;}
  		webservice String GTEXT {get; set;}
  		webservice String RBNRX {get; set;}
  		webservice String TYPTX {get; set;}
  		webservice String BEZEI {get; set;}
  		webservice String BUTXT {get; set;}
  		webservice String KTEXT {get; set;}
  		webservice String KTEXT1 {get; set;}
  		webservice String NAME1 {get; set;}
  		webservice String EARTX {get; set;}
  		webservice String NAME2 {get; set;}
  		webservice String FING {get; set;}
  		//webservice String CLASS {get; set;}
  		webservice String KSCHL {get; set;}
  		webservice String KTEXT2 {get; set;}
  		webservice String MAKTX {get; set;}
  		webservice String ABCTX {get; set;}
  		webservice String INNAM {get; set;}
  		webservice String SYSID_MANDT {get; set;}
  		webservice String SYST_STAT {get; set;}
  		webservice String USER_STAT {get; set;}
  		webservice DT_ECC_FLOC_CHAR_DATA FLOC_CHAR {get; set;}
  	}

  	global class HOG_SAPFLOCServiceResponse {
  		webservice List<Result> results {get; set;}

      public void addResults(List<Result> results) {
        if(this.results == null) this.results = new List<Result>();
        this.results.addAll(results);
      }
  	}
    /**************************************************************************/

    /**************** Equipment Service Utilities and Classes *****************/
    global class DT_SAP_EQUIP_ITEM {
  		webservice DT_SAP_RIHEQUI_LIST RIHEQUI_LIST {get; set;}
  		webservice List<DT_SAP_ZIPM_CHAR_DATA> EQUIP_CHAR {get; set;}
  	}

  	global class DT_SAP_RIHEQUI_LIST {
		//10/10/2019 - adding new field into service START
		webservice String ZZ_SCE  {get; set;}
		webservice String ZZ_SCE_DESC  {get; set;}
		webservice String ZZ_REGULATORY  {get; set;}
		//10/10/2019 - adding new field into service STOP
		webservice String EQUNR {get; set;}
  		webservice String SPRAS {get; set;}
  		webservice String EQKTX {get; set;}
  		webservice String EQTYP {get; set;}
  		webservice String IWERK {get; set;}
  		webservice String SWERK {get; set;}
  		webservice String STORT {get; set;}
  		webservice String MSGRP {get; set;}
  		webservice String BEBER {get; set;}
  		webservice String ABCKZ {get; set;}
  		webservice String EQFNR {get; set;}
  		webservice String BUKRS {get; set;}
  		webservice String ANLNR {get; set;}
  		webservice String ANLUN {get; set;}
  		webservice String GSBER {get; set;}
  		webservice String KOSTL {get; set;}
  		webservice String DAUFN {get; set;}
  		webservice String AUFNR {get; set;}
  		webservice String TPLNR {get; set;}
  		webservice String TPLNR_INT {get; set;}
  		webservice String TIDNR {get; set;}
  		webservice String HEQUI {get; set;}
  		webservice String HEQNR {get; set;}
  		webservice String SUBMT {get; set;}
  		webservice String SERNR {get; set;}
  		webservice String MAPAR {get; set;}
  		webservice String INGRP {get; set;}
  		webservice String ELIEF {get; set;}
  		webservice String ANSWT {get; set;}
  		webservice String KRFKZ {get; set;}
  		webservice String WAERS {get; set;}
  		webservice String INVNR {get; set;}
  		webservice String GROES {get; set;}
  		webservice String BRGEW {get; set;}
  		webservice String BAUJJ {get; set;}
  		webservice String BAUMM {get; set;}
  		webservice String HERST {get; set;}
  		webservice String HERLD {get; set;}
  		webservice String HZEIN {get; set;}
  		webservice String SERGE {get; set;}
  		webservice String KUND1 {get; set;}
  		webservice String KUND2 {get; set;}
  		webservice String KUND3 {get; set;}
  		webservice String LIZNR {get; set;}
  		webservice String ARBPL {get; set;}
  		webservice String GEWRK {get; set;}
  		webservice String ERDAT {get; set;}
  		webservice String ERNAM {get; set;}
  		webservice String AEDAZ {get; set;}
  		webservice String AENAZ {get; set;}
  		webservice String DATAB {get; set;}
  		webservice String BEGRU {get; set;}
  		webservice String KMATN {get; set;}
  		webservice String MATNR {get; set;}
  		webservice String WERK {get; set;}
  		webservice String TYPBZ {get; set;}
  		webservice String LAGER {get; set;}
  		webservice String CHARGE {get; set;}
  		webservice String KUNDE {get; set;}
  		webservice String DATBI {get; set;}
  		webservice String STTXT {get; set;}
  		webservice String ILOAN {get; set;}
  		webservice String USTXT {get; set;}
  		webservice String OBJNR {get; set;}
  		webservice String RBNR {get; set;}
  		webservice String PROID {get; set;}
  		webservice String EQART {get; set;}
  		webservice String ANSDT {get; set;}
  		webservice String VKORG {get; set;}
  		webservice String VTWEG {get; set;}
  		webservice String SPART {get; set;}
  		webservice String ADRNR {get; set;}
  		webservice String MANDT {get; set;}
  		webservice String NAME_LIST {get; set;}
  		webservice String TEL_NUMBER {get; set;}
  		webservice String POST_CODE1 {get; set;}
  		webservice String CITY1 {get; set;}
  		webservice String CITY2 {get; set;}
  		webservice String COUNTRY {get; set;}
  		webservice String REGION {get; set;}
  		webservice String STREET {get; set;}
  		webservice String RKEOBJNR {get; set;}
  		webservice String CUOBJ {get; set;}
  		webservice String GEWEI {get; set;}
  		webservice String KOKRS {get; set;}
  		webservice String GWLDT_K {get; set;}
  		webservice String GWLEN_K {get; set;}
  		webservice String MGANR_K {get; set;}
  		webservice String GARTX_K {get; set;}
  		webservice String GWLDT_L {get; set;}
  		webservice String GWLEN_L {get; set;}
  		webservice String MGANR_L {get; set;}
  		webservice String GARTX_L {get; set;}
  		webservice String AULDT {get; set;}
  		webservice String INBDT {get; set;}
  		webservice String EQLFN {get; set;}
  		webservice String SUBMTKTX {get; set;}
  		webservice String EQUZ_ERDAT {get; set;}
  		webservice String EQUZ_ERNAM {get; set;}
  		webservice String EQUZ_AEDAT {get; set;}
  		webservice String EQUZ_AENAM {get; set;}
  		webservice String EQUZN {get; set;}
  		webservice String TIMBI {get; set;}
  		webservice String LBBSA {get; set;}
  		webservice String SOBKZ {get; set;}
  		webservice String KUNNR {get; set;}
  		webservice String LIFNR {get; set;}
  		webservice String KDAUF {get; set;}
  		webservice String KDPOS {get; set;}
  		webservice String PS_PSP_PNR {get; set;}
  		webservice String B_CHARGE {get; set;}
  		webservice String MAKTX {get; set;}
  		webservice String PLTXT {get; set;}
  		webservice String KZLTX {get; set;}
  		webservice String S_EQUI {get; set;}
  		webservice String DFPS_CP {get; set;}
  		webservice String DFPS_CP_SYS {get; set;}
  		webservice String DFPS_CP_SYST {get; set;}
  		webservice String ZZ_MARKET_VALUE {get; set;}
  		webservice String ZZ_REPLACE_COST {get; set;}
  		webservice String ZZ_OWNING_FLOCN {get; set;}
  		webservice String ZZ_SASK_PST_IND {get; set;}
  		webservice String ZZ_BC_PST_IND {get; set;}
  		webservice String ZZ_MAN_PST_IND {get; set;}
  		webservice String ZZ_ON_PST_IND {get; set;}
  		webservice String ZZ_HIST_EQUIP {get; set;}
  		webservice String ZZ_CONDITION {get; set;}
  		webservice String ZZ_MKT_VAL_IND {get; set;}
  		webservice String ZZ_OWNING_KOSTL {get; set;}
  		webservice String ZZ_AB_REG {get; set;}
  		webservice String ZZ_BC_REG {get; set;}
  		webservice String ZZ_SK_REG {get; set;}
  		webservice String ZZ_MB_REG {get; set;}
  		webservice String ZZ_ON_REG {get; set;}
  		webservice String ZZ_NO_SCHED {get; set;}
  		webservice String ZZ_ABSA {get; set;}
  		webservice String ZZ_TRANSFER {get; set;}
  		webservice String ZZ_HAZARD {get; set;}
  		webservice String ZZ_MAINT {get; set;}
  		webservice String ZZ_EQUIP_AREA {get; set;}
  		webservice String ZZ_INGRESS {get; set;}
  		webservice String ZZ_PROTECT {get; set;}
  		webservice String ZZ_GAS_GROUP {get; set;}
  		webservice String ZZ_TEMP_CLASS {get; set;}
  		webservice String ZZ_NON_CODE {get; set;}
  		webservice String REC_TYPE {get; set;}
  		webservice String TPLNR_1 {get; set;}
  		webservice String TPLNR_2 {get; set;}
  		webservice String TPLNR_3 {get; set;}
  		webservice String FLTYP {get; set;}
  		webservice String ZZ_LSD {get; set;}
  		webservice String ZZ_RECTYP {get; set;}
  		webservice String ZZEQHEIR {get; set;}
  		webservice String RBNRX {get; set;}
  		webservice String TYPTX {get; set;}
  		webservice String SYSID_MANDT {get; set;}
  		webservice String ABCTX {get; set;}
  		webservice String EARTX {get; set;}
  		webservice String NAME1 {get; set;}
  		webservice String INNAM {get; set;}
  		webservice String KSCHL {get; set;}
  		webservice String TXT04 {get; set;}
  		webservice String POST1 {get; set;}
  	}

  	global class DT_SAP_ZIPM_CHAR_DATA {
  		webservice String EQUNR {get; set;}
  		//webservice String CLASS {get; set;}
  		webservice String ATNAM {get; set;}
  		webservice String ZVAL {get; set;}
  		webservice String MSEHI {get; set;}
  		webservice String MSEHL {get; set;}
  	}

  	global class DT_SAP_EQUIP_LIST {
  		webservice List<DT_SAP_EQUIP_ITEM> EQUIPMENT {get; set;}
  	}

  	global class HOG_SAPEquipmentServiceResponse {
  		webservice List<Result> results {get; set;}

      public void addResults(List<Result> results) {
        if(this.results == null) this.results = new List<Result>();
        this.results.addAll(results);
      }
  	}
    /**************************************************************************/

    /*********************** Result Structure *********************************/
    global class Result {
      webservice Boolean created {get; set;}
      webservice String id {get; set;}
      webservice Boolean success {get; set;}
      WebService List<Error> errors {get; set;}
      webservice String statusCode {get; set;}

      public Result(Boolean created, String id,
        Boolean success, String statusCode) {
        this.created = created;
        this.id = id;
        this.success = success;
        this.statusCode = statusCode;
        this.errors = new List<Error>();
      }

      public void addError(Error err) {
        this.errors.add(err);
      }
    }

    global class Error {
      webservice List<String> fields {get; set;}
      webservice String message {get; set;}
      webservice StatusCode statusCode {get; set;}

      public Error(List<String> fields, String message, 
        StatusCode statusCode) {
        this.fields = fields;
        this.message = message;
        this.statusCode = statusCode;
      }
    }
    /**************************************************************************/

    /*********************** Utility Functions ********************************/
    public static Date parseDate(String dateString) {
      if(Pattern.matches('(20\\d{2})(\\d{2})(\\d{2})', dateString)) {
        String yyyy = dateString.substring(0, 4);
        String mm = dateString.substring(4, 6);
        String dd = dateString.substring(6, dateString.length());
        return Date.parse(dd + '/' + mm + '/' + yyyy);
      }
      return null;
    }
    /**************************************************************************/
}