/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WOA_SearchCriteria_Endpoint class
History:        jschn 28/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_SearchCriteria_EndpointTest {

    @IsTest
    static void getFilterFormOptions() {
        User runningUser = VTT_TestData.createVTTUser();
        System.runAs(runningUser) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            tradesman.Current_Work_Order_Activity__c = activity.Id;
            update tradesman;
            VTT_WOA_SearchCriteria_Options result;

            Test.startTest();
            result = VTT_WOA_SearchCriteria_Endpoint.getFilterFormOptionsForCurrentUser();
            Test.stopTest();

            System.assertNotEquals(null, result, 'Response should be returned.');
            System.assertEquals(false, result.isAdmin, 'As user is running without permission, Is Admin flag should be false');
            System.assertEquals(false, result.isVendorSupervisor, 'As user is running without permission, Is Vendor Supervisor flag should be false');
            System.assertNotEquals(null, result.activityStatuses, 'activityStatuses options should not be null');
            System.assertNotEquals(null, result.orderTypes, 'orderTypes options should not be null');
            System.assertNotEquals(null, result.AMUs, 'AMUs options should not be null');
            System.assertNotEquals(null, result.vendors, 'vendors options should not be null');
            System.assertNotEquals(null, result.tradesmans, 'tradesmans options should not be null');
            System.assertNotEquals(null, result.tradesman, 'As contact is associated with running user, contact should not be null');
            System.assertNotEquals(null, result.cleanCriteria, 'Clean criteria should be present.');
        }
    }

    @IsTest
    static void loadTradesmanForAccount() {
        List<HOG_PicklistItem_Simple> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 4;
        Account vendor = VTT_TestData.createVendorAccount('Avengers');
        VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
        VTT_TestData.createTradesmanContact('Bruce', 'Banner', vendor.Id, UserInfo.getUserId());

        Test.startTest();
        try {
            result = VTT_WOA_SearchCriteria_Endpoint.loadTradesmanForAccount(vendor.Id);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As simple query is run, can\'t fail');
        System.assertNotEquals(null, result, 'As action didn\'t fail, result is not null');
        System.assertEquals(expectedCount, result.size(), 'By default there should be 2 options in the list + 2 with queried Contact record.');
    }

    @IsTest
    static void getStoredFiltersForCurrentUser() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;
        insert new Work_Order_Activity_Search_Criteria__c(
                User__c = UserInfo.getUserId(),
                Assigned_Name__c = 'Test1',
                Filter_String__c = 'Test1'
        );

        Test.startTest();
        try {
            result = VTT_WOA_SearchCriteria_Endpoint.getStoredFiltersForCurrentUser();
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'As simple query is run, can\'t fail');
        System.assertNotEquals(null, result, 'As action didn\'t fail, result is not null');
        System.assertEquals(expectedCount, result.size(), 'As we created one criteria for this user, size of the list should be 1.');
    }

    @IsTest
    static void saveFilter_success() {
        Work_Order_Activity_Search_Criteria__c result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        VTT_WOA_SearchCriteria criteria = new VTT_WOA_SearchCriteria();
        criteria.hideCompletedFilter = true;

        Test.startTest();
        try {
            result = VTT_WOA_SearchCriteria_Endpoint.saveFilter(JSON.serialize(criteria), 'Test1');
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'All params are provided, so it shouldn\'t fail');
        System.assertNotEquals(null, result, 'As action have not failed, result should not be null');
        System.assertNotEquals(null, result.Id, 'Insert should be successful => Id is present');
        System.assertEquals(true, result.Last_Search_Criteria_Selected__c, 'This action should set filter as Last Selected');
    }

    @IsTest
    static void saveFilter() {
        List<Work_Order_Activity_Search_Criteria__c> result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        List<Work_Order_Activity_Search_Criteria__c> criteria = new List<Work_Order_Activity_Search_Criteria__c> {
                new Work_Order_Activity_Search_Criteria__c(
                        User__c = UserInfo.getUserId(),
                        Assigned_Name__c = 'Test1',
                        Filter_String__c = 'Test1'
                ),
                new Work_Order_Activity_Search_Criteria__c(
                        User__c = UserInfo.getUserId(),
                        Assigned_Name__c = 'Test2',
                        Filter_String__c = 'Test2'
                )
        };
        insert criteria;

        Test.startTest();
        try {
            result = VTT_WOA_SearchCriteria_Endpoint.setLastSelectedFilter(criteria.get(0).Id);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'All params are provided, so it shouldn\'t fail');
        System.assertNotEquals(null, result, 'As action have not failed, result should not be null');
        for(Work_Order_Activity_Search_Criteria__c resultItem : result) {
            System.assertEquals(resultItem.Last_Search_Criteria_Selected__c, resultItem.Assigned_Name__c == criteria.get(0).Assigned_Name__c);
        }
    }

    @IsTest
    static void setLastSelectedFilter_success() {
        Work_Order_Activity_Search_Criteria__c result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        List<Work_Order_Activity_Search_Criteria__c> criteria = new List<Work_Order_Activity_Search_Criteria__c> {
                new Work_Order_Activity_Search_Criteria__c(
                        User__c = UserInfo.getUserId(),
                        Assigned_Name__c = 'Test1',
                        Filter_String__c = 'Test1'
                ),
                new Work_Order_Activity_Search_Criteria__c(
                        User__c = UserInfo.getUserId(),
                        Assigned_Name__c = 'Test2',
                        Filter_String__c = 'Test2'
                )
        };
        insert criteria;

        Test.startTest();
        try {
            result = VTT_WOA_SearchCriteria_Endpoint.selectFilter(criteria, criteria.get(0).Id);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'All params are provided, so it shouldn\'t fail');
        System.assertNotEquals(null, result, 'As action have not failed, result should not be null');
        System.assertEquals(result.Last_Search_Criteria_Selected__c, result.Assigned_Name__c == criteria.get(0).Assigned_Name__c);
    }

    @IsTest
    static void deleteFilter_success() {
        String result;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Work_Order_Activity_Search_Criteria__c criteria = new Work_Order_Activity_Search_Criteria__c(
                User__c = UserInfo.getUserId(),
                Assigned_Name__c = 'Test1',
                Filter_String__c = 'Test1'
        );
        insert criteria;

        Test.startTest();
        try {
            result = VTT_WOA_SearchCriteria_Endpoint.deleteFilter(criteria.Id);
        } catch (HOG_Exception ex) {
            System.debug(ex.getMessage());
            failFlag = true;
        }

        System.assertEquals(expectedFailFlag, failFlag, 'All params are provided, so it shouldn\'t fail');
        System.assertEquals('Success', result, 'Result should be String "Success"');
    }

}