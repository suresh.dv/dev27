@isTest
public class ATSTestforData {
    static Opportunity opp1;
    static Account acc;
    
    @isTest   
    static void CreateUserTest(){
    	ATSTestData.CreateUser();    
    }
    
    @isTest   
    static void createDataTest(){
        Test.startTest();
        //ATSTestData.CreateUser();
        
        Account account= AccountTestData.createAccount('Asphalt Acc', Null);
        account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
        account.ATS_Customer_Type__c = 'Emulsion';
        insert account;
        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
        //Create opportunity 1
        opp1 = new Opportunity(
            AccountId = account.id,
            OwnerId = UserInfo.getUserId(),
            Name = 'testOpp1',
            CloseDate = System.Today(),
            StageName = 'Closed Won',
            cpm_Bid_Due_Date_Time__c = System.Today(),
            cpm_Country__c = 'Canada',
            cpm_For_Season__c = '2018',
            cpm_Freight_Due_Date_Time__c = System.Today(),
            cpm_Trade_Class__c = 'Emulsion',
            //record type 'Asphalt'
            recordTypeId = recordTypeId
        );
        insert opp1;
        
        
        Contact contact = ContactTestData.createContact(account.Id, 'contactFName', 'contactLName');        
        
        insert contact;
        
        System.debug('contact.Id 1:'+contact.Id);
        ATSTestData.createTenderCustomer(account.Id, opp1.Id, contact.Id);
        ATSTestData.createOpportunityCustomer(account.Id, opp1.Id, contact.Id);
        ATSTestData.CreateOpportunity(account.Id, opp1.Id, 'testString3' );
        ATSTestData.createATSFreight(opp1.Id);
        
        System.debug('contact.Id 2:'+contact.Id);
        //test coverage for UserTestData
        UserTestData.createTestCustomerPortalUser(contact.Id);
        UserTestData.createTestUser('CPM Standard Asphalt');
        UserTestData.createTestUser();
        UserTestData.createTestsysAdminUser();
        UserTestData.createAsphalUser();
        //UserTestData.createSunriseTestUser(1);
        //UserTestData.createSunriseUser('CPMStandardAsphaltPermissionSet', 4);
        //UserTestData.createSunriseTestAdminUser(4);
        Test.stopTest();
    }
    
    @isTest
    static void sunriseTest(){
        Test.startTest();
        UserTestData.createSunriseTestUser(1);
        //UserTestData.createSunriseUser('CPMStandardAsphaltPermissionSet', 4);
        //UserTestData.createSunriseTestAdminUser(4);
        Test.stopTest();
    }
    
    @isTest
    static void createProductsTest1(){
        Test.startTest();
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
        List<Product2> productsList = ProductTestData.createProducts(2, recordTypeId);
        Test.stopTest();
    }
    
    @isTest
    static void createProductsTest2(){
        Test.startTest();
        Id recordTypeId = Schema.SObjectType.Product2.getRecordTypeInfosByName().get('Asphalt').getRecordTypeId();
        List<Product2> productsList = ProductTestData.createProducts(2);
        Test.stopTest();
    }
    
}