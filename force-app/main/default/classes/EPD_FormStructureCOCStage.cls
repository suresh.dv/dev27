/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Form structure for Compressor Operating Condition Stage
Test Class:     EPD_FormStructureCOCStageTest
History:        jschn 2019-06-25 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructureCOCStage extends EPD_FormStructureBase {

    private static final String CLASS_NAME = String.valueOf(EPD_FormStructureCOCStage.class);

    @AuraEnabled
    public Id recordId;

    @AuraEnabled
    public Id enginePerformanceDataId;

    @AuraEnabled
    public Decimal stageNumber {get;set;}

    @AuraEnabled
    public Decimal dischargePressure;
    //TODO remove after LTNG migration
    public String dischargePressureClassic {
        get {
            return String.valueOf(dischargePressure);
        }
        set {
            dischargePressureClassic = value;
            if(String.isNotBlank(value)) {
                dischargePressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal dischargeTemperature;
    //TODO remove after LTNG migration
    public String dischargeTemperatureClassic {
        get {
            return String.valueOf(dischargeTemperature);
        }
        set {
            dischargeTemperatureClassic = value;
            if(String.isNotBlank(value)) {
                dischargeTemperature = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal suctionPressure;
    //TODO remove after LTNG migration
    public String suctionPressureClassic {
        get {
            return String.valueOf(suctionPressure);
        }
        set {
            suctionPressureClassic = value;
            if(String.isNotBlank(value)) {
                suctionPressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal suctionTemperature;
    //TODO remove after LTNG migration
    public String suctionTemperatureClassic {
        get {
            return String.valueOf(suctionTemperature);
        }
        set {
            suctionTemperatureClassic = value;
            if(String.isNotBlank(value)) {
                suctionTemperature = Decimal.valueOf(value);
            }
        }
    }

    /**
     * Maps local variables into Compressor Operating Condition Stage record.
     *
     * @return SObject (EPD_Compressor_Operating_Condition_Stage__c)
     */
    public override SObject getRecord() {
        System.debug(CLASS_NAME + ' -> getRecord. COC Stage Structure: ' + JSON.serialize(this));

        return new EPD_Compressor_Operating_Condition_Stage__c(
                Id = recordId,
                Engine_Performance_Data__c = enginePerformanceDataId,
                Stage_Number__c = stageNumber,
                Discharge_Pressure__c = dischargePressure,
                Discharge_Temperature__c = dischargeTemperature,
                Suction_Pressure__c = suctionPressure,
                Suction_Temperature__c = suctionTemperature
        );
    }

    /**
     * Maps Compressor Oeprating Conditiong Stage record into local variables.
     *
     * @param obj
     *
     * @return EPD_FormStructureBase
     */
    public override EPD_FormStructureBase setRecord(SObject obj) {
        System.debug(CLASS_NAME + ' -> setRecord. START. Object param: ' + JSON.serialize(obj));

        EPD_Compressor_Operating_Condition_Stage__c stage = (EPD_Compressor_Operating_Condition_Stage__c) obj;
        recordId = stage.Id;
        enginePerformanceDataId = stage.Engine_Performance_Data__c;
        stageNumber = stage.Stage_Number__c;
        dischargePressure = stage.Discharge_Pressure__c;
        dischargeTemperature = stage.Discharge_Temperature__c;
        suctionPressure = stage.Suction_Pressure__c;
        suctionTemperature = stage.Suction_Temperature__c;

        System.debug(CLASS_NAME + ' -> setRecord. END. COC Stage Structure: ' + JSON.serialize(this));
        return this;
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuildingString = '';
        contentBuildingString += '"recordId":' + (String.isNotBlank(recordId) ? ('"' + recordId + '"') : 'null') + ',';
        contentBuildingString += '"enginePerformanceDataId":' + (String.isNotBlank(enginePerformanceDataId) ? ('"' + enginePerformanceDataId + '"') : 'null') + ',';
        contentBuildingString += '"stageNumber":' + stageNumber + ',';
        contentBuildingString += '"dischargePressure":' + dischargePressure + ',';
        contentBuildingString += '"dischargeTemperature":' + dischargeTemperature + ',';
        contentBuildingString += '"suctionPressure":' + suctionPressure + ',';
        contentBuildingString += '"suctionTemperature":' + suctionTemperature;
        return '{' + contentBuildingString + '}';
    }

}