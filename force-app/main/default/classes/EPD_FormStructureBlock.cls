/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Form structure for Block Information SObject
Test Class:     EPD_FormStructureBlockTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructureBlock extends EPD_FormStructureBase {

    private static final String CLASS_NAME = String.valueOf(EPD_FormStructureBlock.class);

    @AuraEnabled
    public Id recordId;

    @AuraEnabled
    public Id epdId;

    @AuraEnabled
    public String blockType {get;set;}

    @AuraEnabled
    public Decimal exhaustO2;
    //TODO remove after LTNG migration
    public String exhaustO2Classic {
        get {
            return String.valueOf(exhaustO2);
        }
        set{
            exhaustO2Classic = value;
            if(String.isNotBlank(value)) {
                exhaustO2 = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public Decimal intakeManifoldPressure;
    //TODO remove after LTNG migration
    public String intakeManifoldPressureClassic {
        get {
            return String.valueOf(intakeManifoldPressure);
        }
        set {
            intakeManifoldPressureClassic = value;
            if(String.isNotBlank(value)) {
                intakeManifoldPressure = Decimal.valueOf(value);
            }
        }
    }

    @AuraEnabled
    public String inputLabelIMP {get;set;}

    @AuraEnabled
    public String inputLabelEO2 {get;set;}

    @AuraEnabled
    public String blockName {get;set;}

    @AuraEnabled
    public String measurementType {get;set;}

    @AuraEnabled
    public List<EPD_FormStructureCylinder> cylinders {get;set;}

    /**
     * Maps local variables into Block Information record
     *
     * @return SObject (EPD_Block_Information__c)
     */
    public override SObject getRecord() {
        System.debug(CLASS_NAME + ' -> getRecord. Block Structure: ' + JSON.serialize(this));

        return new EPD_Block_Information__c(
                Id = recordId,
                Engine_Performance_Data__c = epdId,
                Block_Type__c = blockType,
                Exhaust_O2__c = exhaustO2,
                Intake_Manifold_Pressure__c = intakeManifoldPressure
        );
    }

    /**
     * Maps Block Information record into local variables
     *
     * @param obj
     *
     * @return EPD_FormStructureBase
     */
    public override EPD_FormStructureBase setRecord(SObject obj) {
        System.debug(CLASS_NAME + ' -> setRecord. START. Object param: ' + JSON.serialize(obj));

        EPD_Block_Information__c blockInformation = (EPD_Block_Information__c) obj;
        recordId = blockInformation.Id;
        epdId = blockInformation.Engine_Performance_Data__c;
        blockType = blockInformation.Block_Type__c;
        exhaustO2 = blockInformation.Exhaust_O2__c;
        intakeManifoldPressure = blockInformation.Intake_Manifold_Pressure__c;

        System.debug(CLASS_NAME + ' -> setRecord. END. Block Structure: ' + JSON.serialize(this));
        return this;
    }

    /**
     * Sets input labels, block name and measurement type based on provided Engine Configuration.
     * Using mapping in Constants class.
     *
     * @param config
     *
     * @return EPD_FormStructureBlock
     */
    public EPD_FormStructureBlock setConfig(EPD_Engine__mdt config){
        System.debug(CLASS_NAME + ' -> setConfig. START. Block Type: ' + blockType);

        inputLabelIMP = EPD_Constants.BLOCK_TYPE_TO_IMP_INPUT_LABEL_MAP.get(blockType);
        inputLabelEO2 = EPD_Constants.BLOCK_TYPE_TO_EO2_INPUT_LABEL_MAP.get(blockType);
        blockName = EPD_Constants.BLOCK_TYPE_TO_BLOCK_NAME_MAP.get(blockType);
        measurementType = config.Measurement_Type__c;

        System.debug(CLASS_NAME + ' -> setConfig. END. Block Structure: ' + JSON.serialize(this));
        return this;
    }

    /**
     * Checks required values against thresholds and if thresholds are reached it will build string with warning.
     * Then it calls similar logic on all of its Cylinders.
     * Currently used only for Threshold Alert email.
     *
     * @param thresholds
     *
     * @return String
     */
    public String getThresholdWarnings(EPD_FormStructureThresholds thresholds){
        String blockThresholdWarnings = '';

        if(EPD_Utility.isOutsideOfRange(intakeManifoldPressure, thresholds.minIntakeManifoldPressure, thresholds.maxIntakeManifoldPressure)) {
            blockThresholdWarnings += getIMPThresholdWarning();
        }

        String cylinderThresholdWarnings = '';
        for(EPD_FormStructureCylinder cylinder : cylinders) {
            cylinderThresholdWarnings += cylinder.getThresholdWarnings(thresholds);
        }

        blockThresholdWarnings += cylinderThresholdWarnings;
        if(String.isNotBlank(blockThresholdWarnings)) {
            blockThresholdWarnings = '</br>Warning(s) for bank: ' + blockName + '</br>' + blockThresholdWarnings;
        }

        return blockThresholdWarnings;
    }

    /**
     * Builds Threshold warning string for Intake Manifold Pressure.
     *
     * @return String
     */
    private String getIMPThresholdWarning() {
        return String.format(
                EPD_Constants.THRESHOLD_EMAIL_WARNING_BASE_SIMPLE,
                new List<String> {
                        'Intake Manifold Pressure',
                        String.valueOf(intakeManifoldPressure) + '"Hg'
                }
        );
    }

    /**
     * Checks if Block measurements reached engine thresholds.
     *
     * @param thresholds
     *
     * @return Boolean
     */
    public Boolean hasReachedThreshold(EPD_FormStructureThresholds thresholds) {
        Boolean cylinderHasReachedThresholds = false;
        for(EPD_FormStructureCylinder cylinder : cylinders) {
            cylinderHasReachedThresholds |= cylinder.hasReachedThreshold(thresholds);
        }
        return EPD_Utility.isOutsideOfRange(intakeManifoldPressure, thresholds.minIntakeManifoldPressure, thresholds.maxIntakeManifoldPressure)
                || cylinderHasReachedThresholds;
    }

    /**
     * Checks if Block measurements reached engine thresholds.
     *
     * @param previousEngineBlock
     *
     * @return Boolean
     */
    public Boolean hasReachedRetrospectiveThreshold(EPD_FormStructureBlock previousEngineBlock) {
        Boolean cylinderHasReachedThresholds = false;

        for(EPD_FormStructureCylinder previousCylinder : previousEngineBlock.cylinders) {
            for (EPD_FormStructureCylinder cylinder : cylinders) {
                if(previousCylinder.cylinderNumber == cylinder.cylinderNumber) {
                    cylinderHasReachedThresholds |= cylinder.hasReachedRetrospectiveThreshold(previousCylinder);
                }
            }
        }

        return cylinderHasReachedThresholds;
    }

    /**
     * Builds retrospective warning messages for block.
     *
     * @param previousEngineBlock
     *
     * @return
     */
    public String getRetrospectiveThresholdWarnings(EPD_FormStructureBlock previousEngineBlock) {
        String thresholdWarnings = '';

        for(EPD_FormStructureCylinder previousCylinder : previousEngineBlock.cylinders) {
            for (EPD_FormStructureCylinder cylinder : cylinders) {
                if (previousCylinder.cylinderNumber == cylinder.cylinderNumber) {
                    thresholdWarnings += cylinder.getRetrospectiveThresholdWarnings(previousCylinder);
                }
            }
        }

        if(String.isNotBlank(thresholdWarnings)) {
            thresholdWarnings = '</br>Retrospective warning(s) for bank: ' + blockName + '</br>' + thresholdWarnings;
        }

        return thresholdWarnings;
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuildingString = '';
        contentBuildingString += '"recordId":' + (String.isNotBlank(recordId) ? ('"' + recordId + '"') : 'null') + ',';
        contentBuildingString += '"epdId":' + (String.isNotBlank(epdId) ? ('"' + epdId + '"') : 'null') + ',';
        contentBuildingString += '"blockType":' + (String.isNotBlank(blockType) ? ('"' + blockType + '"') : 'null') + ',';
        contentBuildingString += '"exhaustO2":' + exhaustO2 + ',';
        contentBuildingString += '"intakeManifoldPressure":' + intakeManifoldPressure + ',';
        contentBuildingString += '"inputLabelEO2":' + (String.isNotBlank(inputLabelEO2) ? ('"' + inputLabelEO2 + '"') : 'null') + ',';
        contentBuildingString += '"blockName":' + (String.isNotBlank(blockName) ? ('"' + blockName + '"') : 'null') + ',';
        contentBuildingString += '"measurementType":' + (String.isNotBlank(measurementType) ? ('"' + measurementType + '"') : 'null');
        if(cylinders != null) {
            String cylinderContentBuilderString = '';
            for(EPD_FormStructureCylinder cylinderStructure: cylinders) {
                if(String.isNotBlank(cylinderContentBuilderString)) {cylinderContentBuilderString += ',';}
                cylinderContentBuilderString += cylinderStructure.toString();
            }
            cylinderContentBuilderString += ']';
            contentBuildingString += ',"cylinders":[' + cylinderContentBuilderString;
        }
        return '{' + contentBuildingString + '}';
    }

}