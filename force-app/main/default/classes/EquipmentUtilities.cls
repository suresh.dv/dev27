public class EquipmentUtilities {

	public class UserPermissions {

		public Boolean isSystemAdmin { get; private set; }
		public Boolean isHogAdmin { get; private set; }
		public Boolean isSapSupportLead { get; private set; }


		public UserPermissions() {
			this.isSystemAdmin = isUserProfile('System Administrator');
			this.isHogAdmin = isUserPermissionSet('HOG_Administrator');
			this.isSapSupportLead = isUserRole('SAP_Support_Lead');

		}
	}

	public static final String ROLE_TRADEMAN_LEAD = 'Tradesman Lead';
	public static final String ROLE_PLANNER = 'Planner';
	public static final String REQUEST_STATUS_OPEN = 'Open';
	public static final String REQUEST_STATUS_REJECTED = 'Rejected';
	public static final String REQUEST_STATUS_PROCESSED = 'Processed';
	public static final String REQUEST_STATUS_REJECTED_CLOSED = 'Rejected Closed';
	public static final String REQUEST_STATUS_AUTO_CLOSED = 'Auto Closed';
	public static final String REQUEST_STATUS_MISSING = 'Missing';
	public static final List<String> equipmentDataUpdateFields = new List<String>{
			'Equipment_Status__c', 'Description_new__c', 'Manufacturer_new__c', 'Model_Number_new__c', 'Manufacturer_Serial_No_new__c',
			'Tag_Number_new__c', 'Comments__c', 'Expiration_Date__c', 'Reason__c', 'Regulatory_Equipment__c', 'Safety_Critical_Equipment__c'
	} ;

	public static final String EQUIPMENT_HIERARCHY_FIELDS = 'Id, ' +
			'Name, ' +
			'Description_of_Equipment__c, ' +
			'Status__c, ' +
			'Superior_Equipment__c, ' +
			'Manufacturer__c, ' +
			'Manufacturer_Serial_No__c, ' +
			'Model_Number__c, ' +
			'Tag_Number__c, ' +
			'CreatedDate, ' +
			'CreatedBy.Name';

	public static final String EQUIPMENT_FIELDS = 'Id, ' +
			'Name, ' +
			'Description_of_Equipment__c, ' +
			'Manufacturer__c, ' +
			'Manufacturer_Serial_No__c, ' +
			'Model_Number__c, ' +
			'Tag_Number__c,' +
			'Superior_Equipment__c';

	public static final String FLOC_FIELDS = 'Id, ' +
			'Name, ' +
			'Route__c, ' +
			'Route__r.Id, ' +
			'Route__r.Name, ' +
			'Planner_Group__c';

	public static final String FACILITY_FIELDS = 'Id, ' +
			'Name, ' +
			'Plant_Section__c, ' +
			'Plant_Section__r.Name, ' +
			'Plant_Section__r.Id, ' +
			'Planner_Group__c';

	public static final String ADD_FORM_FIELDS = 'Id, ' +
			'Name, ' +
			'Status__c, ' +
			'Equipment_Status__c, ' +
			'Manufacturer__c, ' +
			'Manufacturer_Serial_No__c, ' +
			'Model_Number__c, ' +
			'Tag_Number__c, ' +
			'CreatedDate, ' +
			'CreatedBy.Name';

	public static final String TRANSFER_FORM_FIELDS = 'Id,' +
			'Name, ' +
			'Status__c, ' +
			'FROM_Source__c, ' +
			'TO_Destination__c, ' +
			'Reason_for_Transfer_Additional_Details__c, ' +
			'Date_of_Physical_Transfer__c, ' +
			'Authorized_By__r.Name, ' +
			'Shipped_Via__c, ' +
			'Waybill_No__c, ' +
			'CreatedBy.Name, ' +
			'CreatedDate, ' +
			'To_Location__c, ' +
			'To_Facility__c, ' +
			'To_Functional_Equipment_Level__c, ' +
			'To_Yard__c, ' +
			'To_System__c, ' +
			'To_Sub_System__c, ' +
			'To_Well_Event__c, ' +
			'From_Location__c, ' +
			'From_Facility__c, ' +
			'From_Functional_Equipment_Level__c, ' +
			'From_Yard__c, ' +
			'From_System__c, ' +
			'From_Sub_System__c, ' +
			'From_Well_Event__c ';

	//warning and error messages
	public static final String ERROR_ON_LOAD_MESSAGE = 'An error occurred during loading the record. Please, try to refresh the page. If the error persist, please contact HOG Administrator.';
	public static final String FORM_IS_CLOSED_MESSAGE = 'The form is already closed and cannot be updated.';

	//profile check
	private static Boolean isUserProfile(String profileName) {
		return (currentUserProfile == null) ? false : currentUserProfile.Name == profileName;
	}

	//role check
	private static Boolean isUserRole(String roleName) {
		return (currentUserRole == null) ? false : currentUserRole.DeveloperName == roleName;
	}

	//permission set check
	private static Boolean isUserPermissionSet(String permissionSet) {
		for (PermissionSetAssignment psa : currentUserPermissionSetAssignments) {
			if (psa.PermissionSet != null && psa.PermissionSet.Name.equals(permissionSet)) {
				return true;
			}
		}

		return false;
	}

	//get current user Profile
	private static Profile currentUserProfile {
		get {
			if (currentUserProfile == null) {
				currentUserProfile = [
						SELECT Id, Name
						FROM Profile
						WHERE Id = :UserInfo.getProfileId()
				];
			}
			return currentUserProfile;
		}
	}

	//get current user Role
	private static UserRole currentUserRole {
		get {
			if (currentUserRole == null) {
				currentUserRole = [
						SELECT Id, DeveloperName
						FROM UserRole
						WHERE Id = :UserInfo.getUserRoleId()
				];
			}
			return currentUserRole;
		}
	}

	//get current user Permission sets
	private static List<PermissionSetAssignment> currentUserPermissionSetAssignments {
		get {
			if (currentUserPermissionSetAssignments == null || currentUserPermissionSetAssignments.isEmpty()) {
				currentUserPermissionSetAssignments = [
						SELECT Id, PermissionSet.Name, AssigneeId
						FROM PermissionSetAssignment
						WHERE AssigneeId = :UserInfo.getUserId()
				];
			}
			return currentUserPermissionSetAssignments;
		}
	}
}