/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for EPD_CylinderInformationTrigger
History:        jschn 2019-08-07 - Created.
*************************************************************************************************/
@IsTest
private class EPD_CylinderInformationTriggerTest {

    @IsTest
    static void delete_allowed() {
        EPD_Cylinder_Information__c cylinderInformation = EPD_TestData.createCylinderInformation();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = false;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete cylinderInformation;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

    @IsTest
    static void delete_restricted() {
        EPD_Cylinder_Information__c cylinderInformation = EPD_TestData.createCylinderInformation();
        EPD_Constants.SETTINGS.Restrict_Record_Deletion__c = true;
        Boolean expectedFailFlag = EPD_Constants.SETTINGS.Restrict_Record_Deletion__c;
        Boolean failFlag = false;

        Test.startTest();
        try {
            delete cylinderInformation;
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(expectedFailFlag, failFlag);
    }

}