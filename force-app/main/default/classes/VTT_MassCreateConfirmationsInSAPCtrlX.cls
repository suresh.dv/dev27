//Test Class: VTT_MassCreateConfInSAPCtrlXTest
public with sharing class VTT_MassCreateConfirmationsInSAPCtrlX {
    private ApexPages.StandardSetController stdController;
    private List<Work_Order_Activity_Confirmation__c> filteredConfirmationList;
    private List<Work_Order_Activity_confirmation__c> sentConfirmationList;

    public List<Work_Order_Activity_Confirmation__c> selectedConfirmationList {get; private set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public VTT_MassCreateConfirmationsInSAPCtrlX(ApexPages.StandardSetController stdController) {
        this.stdController = stdController;
        //this.selectedConfirmationList = stdController.getSelected();
        //getFilteredConfirmationLists();
        refreshResults();
    }

    public void sendDataToSAP() {

        //Filter confirmation list to ones that are in error
        List<HOG_SAPConfirmationsService.DT_SFDC_Confirmation> sapConfirmationList = VTT_ConfirmationsUtilities.createSAPConfirmations(filteredConfirmationList);


        //Callout and aftermath
        try {
            HOG_SAPConfirmationsService.HTTPS_Port confirmationService = new HOG_SAPConfirmationsService.HTTPS_Port();
            HOG_Settings__c settingsHOG = HOG_Settings__c.getInstance();
            confirmationService.inputHttpHeaders_x = new Map<String, String>();
            String authString = EncodingUtil.base64Encode(Blob.valueOf(settingsHOG.SAP_DPVR_Username__c + ':' + settingsHOG.SAP_DPVR_Password__c));
            confirmationService.inputHttpHeaders_x.put('Authorization', 'Basic ' + authString);

            HOG_SAPConfirmationsService.Confirmation_Resp_element[] response = confirmationService.SI_SFDC_Confirmation_Sync_OB(sapConfirmationList);

            //Assuming response is in the same order
            for(Integer i=0; i < response.size(); i++) {
                System.debug('response: ' + response[i]);
                filteredConfirmationList[i].Confirmation_Number__c = response[i].Conf_No;
                filteredConfirmationList[i].Status__c = (response[i].Status.startsWith(VTT_ConfirmationsUtilities.SAP_RESPONSE_ERROR_PREFIX)) ? 
                    VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR : VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_SENT;
                if(String.isBlank(filteredConfirmationList[i].Confirmation_Log__c))
                    filteredConfirmationList[i].Confirmation_Log__c = String.format('{0} SAP Returned: ' + response[i].Status, new List<String>{String.valueof(Datetime.now())});
                else
                    VTT_ConfirmationsUtilities.addEntryToConfirmationLog(filteredConfirmationList[i], 
                            String.format('{0} SAP Returned: ' + response[i].Status, new List<String>{String.valueof(Datetime.now())}));
            }
        } catch (CalloutException ex) {
            for(Work_Order_Activity_Confirmation__c confirmation : filteredConfirmationList) {
                confirmation.Status__c = VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR;
                if(String.isBlank(confirmation.Confirmation_Log__c))
                    confirmation.Confirmation_Log__c = String.format('{0} SAP Callout Attempt Failed: ' + ex.getMessage(), new List<String>{String.valueof(Datetime.now())});
                else
                    VTT_ConfirmationsUtilities.addEntryToConfirmationLog(confirmation, 
                            String.format('\n{0} SAP Callout Attempt Failed: ' + ex.getMessage(), new List<String>{String.valueof(Datetime.now())}));
                
            }
        }

        //Update already sent ones
        for(Work_Order_Activity_Confirmation__c confirmation : sentConfirmationList) {
            if(String.isBlank(confirmation.Confirmation_Log__c))
                confirmation.Confirmation_Log__c = String.format('{0} Confirmation already sent', new List<String>{String.valueof(Datetime.now())});
            else
                VTT_ConfirmationsUtilities.addEntryToConfirmationLog(confirmation, 
                        String.format('\n{0} Confirmation already sent', new List<String>{String.valueof(Datetime.now())}));
        }

        //Update confirmations in SF (using Data.update to not do action all or nothing)
        Database.SaveResult[] updateResults = Database.update(selectedConfirmationList, false);
        refreshResults();
    }

    private void getFilteredConfirmationLists() {
        this.filteredConfirmationList = new List<Work_Order_Activity_Confirmation__c>();
        this.sentConfirmationList = new List<Work_Order_Activity_Confirmation__c>();
        for(Work_Order_Activity_Confirmation__c confirmation : this.selectedConfirmationList) {
            if(confirmation.Status__c == VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_NEW ||
                confirmation.Status__c == VTT_ConfirmationsUtilities.CONFIRMATION_STATUS_ERROR) {
                this.filteredConfirmationList.add(confirmation);
            } else {
                this.sentConfirmationList.add(confirmation);
            }
        }
    }

    private void refreshResults() {
        //Set<Id> selectedConfirmationIds = new Set<Id>(); 
        //for(Work_Order_Activity_Confirmation__c confirmation : this.stdController.getSelected()) {
        //    selectedConfirmationIds.add(confirmation.Id);
        //}
        this.selectedConfirmationList = [Select Id, Name, Work_Order_Activity__r.Maintenance_Work_Order__r.Work_Order_Number__c,
                                                 Work_Order_Activity__r.Operation_Number__c, Work_Order_Activity__r.Description__c,
                                                 Work_Order_Activity__r.System_Status__c, Work_Order_Activity__r.Work_Center__c,
                                                 Actual_Work__c, Final_Confirmation__c, Work_Start__c, Work_Finish__c, 
                                                 Confirmation_Text__c, Confirmation_Long_Text__c,
                                                 Work_Order_Activity__r.Operating_Field_AMU__r.Maintenance_Plant__c, 
                                                 Work_Order_Activity__r.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Maintenance_Plant__c,
                                                 Confirmation_Log__c, Status__c, Work_Order_Activity__c, Tradesman__c, Status_Icon__c
                                          From Work_Order_Activity_Confirmation__c
                                          Where Id In :this.stdController.getSelected()];
        getFilteredConfirmationLists();
    }
}