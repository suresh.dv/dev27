global with sharing class MyHuskyCommunityLoginController {
    global String username {get; set;}
    global String password {get; set;}
    public String orgId {get; set;}
    public String portalId {get; set;}
    public String siteURL  {get; set;}
    public String startURL {get; set;}
    public List<AuthProvider> authProviders {get; set;}
    public String retailLogo {get; set;}

    public MyHuskyCommunityLoginController() {
        orgId = UserInfo.getOrganizationId();
        siteURL  = Site.getBaseUrl();
        portalId = '060G00000005Fqa';
        startURL = System.currentPageReference().getParameters().get('startURL');

        if (startURL == null) 
            startURL = '/apex/LOM_Dealer_Dashboard?sfdc.tabName=01rG0000000iiBD';

        authProviders = [SELECT Id,DeveloperName,FriendlyName,ProviderType FROM AuthProvider];
    }

    global PageReference login() {
        startUrl = '/apex/LOM_Dealer_Dashboard?sfdc.tabName=01rG0000000iiBD';
        return Site.login(username, password, startUrl);      
    }
    
    global String getEncodedSiteUrl() {
        return EncodingUtil.urlEncode(siteURL, 'UTF-8');
    }
    
    global String getEncodedStartUrl() {
        return EncodingUtil.urlEncode(startURL, 'UTF-8');
    }    
}