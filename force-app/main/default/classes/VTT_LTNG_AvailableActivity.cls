/**
 * Created by MarcelBrimus on 28/10/2019.
 */

public with sharing class VTT_LTNG_AvailableActivity {
    @AuraEnabled
    public Boolean markToComplete;

    @AuraEnabled
    public Work_Order_Activity__c activityRecord;

    @AuraEnabled
    public Integer userDurationInput;

//    @AuraEnabled
//    public Boolean IsLimitMet;

    public VTT_LTNG_AvailableActivity(Work_Order_Activity__c pActivity, Boolean pSelected) {
        this.userDurationInput = 0;
        this.activityRecord = pActivity;
        this.markToComplete = pSelected;
    }

    public VTT_LTNG_AvailableActivity(Work_Order_Activity__c pActivity) {
        this(pActivity, false);
    }
}