/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Main part of the form structure. Holds all form structures that are needed to properly
                render EPD Form and move values from and to database.
Test Class:     EPD_FormStructureTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_FormStructure {

    @AuraEnabled
    public EPD_FormStructureEPD epd {get;set;}

    @AuraEnabled
    public List<EPD_FormStructureBlock> engineBlocks {get;set;}

    @AuraEnabled
    public List<EPD_FormStructurePart> parts {get;set;}

    @AuraEnabled
    public List<EPD_FormStructureCOCStage> cocStages {get;set;}

    @AuraEnabled
    public EPD_FormStructureThresholds engineThresholds {get;set;}

    @AuraEnabled
    public List<EPD_FormStructureEOC> EOCs {get;set;}

    @AuraEnabled
    public String cylinderMeasurements {get;set;}

    public EPD_FormStructure setEPDStructure(EPD_FormStructureEPD epd) {
        this.epd = epd;
        return this;
    }

    public EPD_FormStructure setEngineBlocks(List<EPD_FormStructureBlock> engineBlocks) {
        this.engineBlocks = engineBlocks;
        return this;
    }

    public EPD_FormStructure setParts(List<EPD_FormStructurePart> parts) {
        this.parts = parts;
        return this;
    }

    public EPD_FormStructure setCOCStages(List<EPD_FormStructureCOCStage> cocStages) {
        this.cocStages = cocStages;
        return this;
    }

    public EPD_FormStructure setEngineThresholds(EPD_FormStructureThresholds engineThresholds) {
        this.engineThresholds = engineThresholds;
        return this;
    }

    public EPD_FormStructure setEOCs(List<EPD_FormStructureEOC> EOCs) {
        this.EOCs = EOCs;
        return this;
    }

    public EPD_FormStructure setFlags(EPD_Engine__mdt engineConfig) {
        cylinderMeasurements = engineConfig.Measurement_Type__c;
        return this;
    }

    /**
     * Checks if EPD reached engine thresholds.
     *
     * @return Boolean
     */
    public Boolean hasReachedThreshold() {
        Boolean engineBlocksHasReachedThresholds = false;
        for(EPD_FormStructureBlock engineBlock : engineBlocks) {
            engineBlocksHasReachedThresholds |= engineBlock.hasReachedThreshold(engineThresholds);
        }
        return epd.hasReachedThreshold(engineThresholds) || engineBlocksHasReachedThresholds;
    }

    /**
     * Checks if current form reached Retrospective thresholds.
     *
     * @param previousEPDForm
     *
     * @return Boolean
     */
    public Boolean hasReachedRetrospectiveThreshold(EPD_FormStructure previousEPDForm) {
        Boolean engineBlocksHasReachedThresholds = false;

        for(EPD_FormStructureBlock engineBlock : engineBlocks) {
            for(EPD_FormStructureBlock previousEngineBlock : previousEPDForm.engineBlocks) {
                if(previousEngineBlock.blockName == engineBlock.blockName) {
                    engineBlocksHasReachedThresholds |= engineBlock.hasReachedRetrospectiveThreshold(previousEngineBlock);
                }
            }
        }

        return engineBlocksHasReachedThresholds;
    }

    //TODO remove after LTNG migration
    public override String toString() {
        String contentBuilderString = '';
        if(epd != null) {
            contentBuilderString += epd.toString();
        }
        if(engineBlocks != null) {
            if(String.isNotBlank(contentBuilderString)) {contentBuilderString += ',';}
            String blockContentBuilderString = '';
            for(EPD_FormStructureBlock blockStructure: engineBlocks) {
                if(String.isNotBlank(blockContentBuilderString)) {blockContentBuilderString += ',';}
                blockContentBuilderString += blockStructure.toString();
            }
            blockContentBuilderString += ']';
            contentBuilderString += '"engineBlocks":[' + blockContentBuilderString;

        }
        if(parts != null) {
            if(String.isNotBlank(contentBuilderString)) {contentBuilderString += ',';}
            String partContentBuilderString = '';
            for(EPD_FormStructurePart partStructure: parts) {
                if(String.isNotBlank(partContentBuilderString)) {partContentBuilderString += ',';}
                partContentBuilderString += partStructure.toString();
            }
            partContentBuilderString += ']';
            contentBuilderString += '"parts":[' + partContentBuilderString;
        }
        if(cocStages != null) {
            if(String.isNotBlank(contentBuilderString)) {contentBuilderString += ',';}
            String cocStagesContentBuilderString = '';
            for(EPD_FormStructureCOCStage cocStagesStructure: cocStages) {
                if(String.isNotBlank(cocStagesContentBuilderString)) {cocStagesContentBuilderString += ',';}
                cocStagesContentBuilderString += cocStagesStructure.toString();
            }
            cocStagesContentBuilderString += ']';
            contentBuilderString += '"cocStages":[' + cocStagesContentBuilderString;
        }
        if(engineThresholds != null) {
            if(String.isNotBlank(contentBuilderString)) {contentBuilderString += ',';}
            contentBuilderString += engineThresholds.toString();
        }
        if(EOCs != null) {
            if(String.isNotBlank(contentBuilderString)) {contentBuilderString += ',';}
            String EOCContentBuilderString = '';
            for(EPD_FormStructureEOC EOCStructure: EOCs) {
                if(String.isNotBlank(EOCContentBuilderString)) {EOCContentBuilderString += ',';}
                EOCContentBuilderString += EOCStructure.toString();
            }
            EOCContentBuilderString += ']';
            contentBuilderString += '"EOCs":[' + EOCContentBuilderString;
        }
        if(String.isNotBlank(contentBuilderString)) {contentBuilderString += ',';}
        contentBuilderString += '"cylinderMeasurements":' + (String.isNotBlank(cylinderMeasurements) ? '"' + cylinderMeasurements + '"' : 'null');
        return '{' + contentBuilderString + '}';
    }

}