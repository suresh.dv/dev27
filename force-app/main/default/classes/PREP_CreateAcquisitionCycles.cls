public class  PREP_CreateAcquisitionCycles
{
  /* public static void insertAcquisition( List<PREP_Initiative__c > initiative){
   ID prj=  Schema.getGlobalDescribe().get('PREP_Initiative__c').getDescribe().getRecordTypeInfosByName().get('Projects Initiative').getRecordTypeId();
    //RecordType prj= [SELECT Id From RecordType WHERE Name = 'Projects Initiative'];
    //RecordType matACId = [SELECT Id From RecordType WHERE Name = 'Mat Acquisition Cycle'];
    //RecordType servACId = [SELECT Id From RecordType WHERE Name = 'Serv Acquisition Cycle'];
    ID matACId =Schema.getGlobalDescribe().get('PREP_Acquisition_Cycle__c').getDescribe().getRecordTypeInfosByName().get('Mat Acquisition Cycle').getRecordTypeId();
    ID servACId =Schema.getGlobalDescribe().get('PREP_Acquisition_Cycle__c').getDescribe().getRecordTypeInfosByName().get('Serv Acquisition Cycle').getRecordTypeId();
    List<PREP_Acquisition_Cycle__c> acqlist = new list<PREP_Acquisition_Cycle__c>();
   //list<PREP_Initiative__c> initiatives = [ select id,RecordTypeId from PREP_Initiative__c where RecordTypeId =:prj];
  // System.debug('@@@'+initiatives );
   //try{
    for(PREP_Initiative__c init: initiative )
    {

        If( init.RecordTypeId == prj)
         {

            PREP_Acquisition_Cycle__c MatAcqCycle = new PREP_Acquisition_Cycle__c();
            MatAcqCycle.Initiative__c= init.Id;
            MatAcqCycle.RFQ_Prep__c = 3;
            MatAcqCycle.Bid_Period__c =  28;
            MatAcqCycle.Bid_Evaluation_1__c = 21;
            MatAcqCycle.Bid_Evaluation_2__c = 21;
            MatAcqCycle.Bid_Evaluation_3__c = 2;
            MatAcqCycle.Bid_Evaluation_4__c = 7;
            MatAcqCycle.Husky_Approval__c = 7;
            MatAcqCycle.MRP_Prep__c = 7;
            MatAcqCycle.PO_Prep_Issue__c = 9; 
            MatAcqCycle.Shipping_Duration__c = 7;
            MatAcqCycle.Type__c = 'Standard 12';
            MatAcqCycle.RecordTypeId = matACId;
            
            acqlist .add(MatAcqCycle);
            //insert MatAcqCycle;
        
            PREP_Acquisition_Cycle__c MatAcqCycle2 = new PREP_Acquisition_Cycle__c();
            MatAcqCycle2.Initiative__c= init.Id;
            MatAcqCycle2.RFQ_Prep__c = 3;
            MatAcqCycle2.Bid_Period__c =  14;
            MatAcqCycle2.Bid_Evaluation_1__c = 7;
            MatAcqCycle2.Bid_Evaluation_2__c = 7;
            MatAcqCycle2.Bid_Evaluation_3__c = 1;
            MatAcqCycle2.Bid_Evaluation_4__c = 1;
            MatAcqCycle2.Husky_Approval__c = 7;
            MatAcqCycle2.MRP_Prep__c = 7;
            MatAcqCycle2.PO_Prep_Issue__c = 2; 
            MatAcqCycle2.Shipping_Duration__c = 7;
            MatAcqCycle2.Type__c = 'Standard 6';
            MatAcqCycle2.RecordTypeId = matACId;
            acqlist .add(MatAcqCycle2);
            //insert MatAcqCycle2;

        
            PREP_Acquisition_Cycle__c MatAcqCycle3 = new PREP_Acquisition_Cycle__c();
            MatAcqCycle3.Initiative__c= init.Id;
            MatAcqCycle3.RFQ_Prep__c = 1;
            MatAcqCycle3.Bid_Period__c =  5;
            MatAcqCycle3.Bid_Evaluation_1__c = 2;
            MatAcqCycle3.Bid_Evaluation_2__c = 2;
            MatAcqCycle3.Bid_Evaluation_3__c = 2;
            MatAcqCycle3.Bid_Evaluation_4__c = 1;
            MatAcqCycle3.Husky_Approval__c = 1;
            MatAcqCycle3.MRP_Prep__c = 0;
            MatAcqCycle3.PO_Prep_Issue__c = 2; 
            MatAcqCycle3.Shipping_Duration__c = 7;
            MatAcqCycle3.Type__c = 'Standard 2';
            MatAcqCycle3.RecordTypeId = matACId;
            acqlist .add(MatAcqCycle3);
            //insert MatAcqCycle3;

         
                
            PREP_Acquisition_Cycle__c ServAcqCycle = new PREP_Acquisition_Cycle__c();
            ServAcqCycle.Initiative__c= init.Id;
            ServAcqCycle.RFQ_Prep__c = 3;
            ServAcqCycle.Bid_Period__c =  28;
            ServAcqCycle.Bid_Evaluation_1__c = 21;
            ServAcqCycle.Bid_Evaluation_2__c = 21;
            ServAcqCycle.Bid_Evaluation_3__c = 2;
            ServAcqCycle.Bid_Evaluation_4__c = 7;
            ServAcqCycle.Husky_Approval__c = 7;
            ServAcqCycle.CWP_Prep__c = 7;
            ServAcqCycle.CT_Prep_Issue__c = 9;
            ServAcqCycle.Kick_off_Mobilization__c = 5;
            ServAcqCycle.Demob__c = 5;
            ServAcqCycle.Type__c = 'Standard 12';
            ServAcqCycle.RecordTypeId = servACId;
             acqlist .add(ServAcqCycle);
            //insert ServAcqCycle;
      
        
            PREP_Acquisition_Cycle__c ServAcqCycle2 = new PREP_Acquisition_Cycle__c();
            ServAcqCycle2.Initiative__c= init.Id;
            ServAcqCycle2.RFQ_Prep__c = 3;
            ServAcqCycle2.Bid_Period__c =  14;
            ServAcqCycle2.Bid_Evaluation_1__c = 7;
            ServAcqCycle2.Bid_Evaluation_2__c = 7;
            ServAcqCycle2.Bid_Evaluation_3__c = 1;
            ServAcqCycle2.Bid_Evaluation_4__c = 1;
            ServAcqCycle2.Husky_Approval__c = 7;
            ServAcqCycle2.CWP_Prep__c = 7;
            ServAcqCycle2.CT_Prep_Issue__c = 2;
            ServAcqCycle2.Kick_off_Mobilization__c = 3;
            ServAcqCycle2.Demob__c = 3;            
            ServAcqCycle2.Type__c = 'Standard 6';
            ServAcqCycle2.RecordTypeId = servACId;
            acqlist .add(ServAcqCycle2);
           //insert ServAcqCycle2;
            
            PREP_Acquisition_Cycle__c ServAcqCycle3 = new PREP_Acquisition_Cycle__c();
            ServAcqCycle3.Initiative__c= init.Id;
            ServAcqCycle3.RFQ_Prep__c = 1;
            ServAcqCycle3.Bid_Period__c =  5;
            ServAcqCycle3.Bid_Evaluation_1__c = 2;
            ServAcqCycle3.Bid_Evaluation_2__c = 2;
            ServAcqCycle3.Bid_Evaluation_3__c = 2;
            ServAcqCycle3.Bid_Evaluation_4__c = 1;
            ServAcqCycle3.Husky_Approval__c = 1;
            ServAcqCycle3.CWP_Prep__c = 0;
            ServAcqCycle3.CT_Prep_Issue__c = 2;
            ServAcqCycle3.Kick_off_Mobilization__c = 2;
            ServAcqCycle3.Demob__c = 2;            
            ServAcqCycle3.Type__c = 'Standard 2';
            ServAcqCycle3.RecordTypeId = servACId;
            acqlist .add(ServAcqCycle3);
            //insert ServAcqCycle3;
            }
      }
      
      if(acqlist.size()>0)
       {
            insert acqlist ;
            //return null;
        }
        
 }    */  
}