/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class containing all queries for EPD_Block_Information__c SObject
Test Class:     EPD_BlockInformationSelectorTest
History:        jschn 2019-05-24 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class EPD_BlockInformationSelector implements EPD_BlockInformationDAO {

    /**
     * Method for querying list of Block Information records based on set of EPD_Engine_Performance_Data__c
     * record IDs.
     *
     * @param epdIds
     *
     * @return List<EPD_Block_Information__c>
     */
    public List<EPD_Block_Information__c> getBlockInformationByEPDs(Set<Id> epdIds) {
        return [
                SELECT Name, Block_Type__c, Intake_Manifold_Pressure__c, Engine_Performance_Data__c,
                        Exhaust_O2__c,
                (
                        SELECT Name, Cylinder_Number__c, RecordTypeId, Compression__c, Wear_Intake__c,
                                Wear_Exhaust__c, Exhaust_Temperature__c, Head_Replacement__c, Block_Information__c
                        FROM Cylinder_Information__r
                        ORDER BY Cylinder_Number__c
                )
                FROM EPD_Block_Information__c
                WHERE Engine_Performance_Data__c IN : epdIds
        ];
    }

}