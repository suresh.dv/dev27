/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_Utility_EndpointHandler
History:        jschn 25/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_Utility_EndpointHandlerTest {

    @IsTest
    static void getTradesmanInfo_isAdmin() {
        Boolean isAdmin = true;
        Boolean isVendorTradesman = false;
        User runningUser = VTT_TestData.createVTTAdminUser();
        VTT_TradesmanFlags result;

        System.runAs(runningUser) {
            Test.startTest();
            result = new VTT_Utility_EndpointHandler().getTradesmanInfo();
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Response should be returned.');
        System.assertEquals(isAdmin, result.isAdmin, 'As user has proper PS/Profile, isAdmin flag should be true');
        System.assertEquals(isVendorTradesman, result.isVendorSupervisor, 'As user has proper PS/Profile, isVendorSupervisor flag should be false');
    }

    @IsTest
    static void getTradesmanInfo_isVendorSupervisor() {
        Boolean isAdmin = false;
        Boolean isVendorTradesman = true;
        User runningUser = VTT_TestData.createVendorSupervisorUser();
        VTT_TradesmanFlags result;

        System.runAs(runningUser) {
            Test.startTest();
            result = new VTT_Utility_EndpointHandler().getTradesmanInfo();
            Test.stopTest();
        }

        System.assertNotEquals(null, result, 'Response should be returned.');
        System.assertEquals(isAdmin, result.isAdmin, 'As user has proper PS/Profile, isAdmin flag should be false');
        System.assertEquals(isVendorTradesman, result.isVendorSupervisor, 'As user has proper PS/Profile, isVendorSupervisor flag should be true');
    }

    @IsTest
    static void getTradesmanInfo_withContactWActivity() {
        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            tradesman.Current_Work_Order_Activity__c = activity.Id;
            update tradesman;
            VTT_TradesmanFlags result;

            Test.startTest();
            result = new VTT_Utility_EndpointHandler().getTradesmanInfo();
            Test.stopTest();

            System.assertNotEquals(null, result, 'Response should be returned.');
            System.assertEquals(tradesman.Id, result.tradesman.Id, 'Check if running user has already contact. Contact should be populated with one created in the test');
            System.assertEquals(activity.Id, result.currentWorkOrderActivityId, 'Current Work Order ID should be one created in test.');
        }
    }

    @IsTest
    static void getTradesmanInfo_withContactWoActivity_mockEmpty() {
        VTT_Utility_EndpointHandler.logEntryDAO = new EmptyMock();
        VTT_TradesmanFlags result;

        Test.startTest();
        result = new VTT_Utility_EndpointHandler().getTradesmanInfo();
        Test.stopTest();

        System.assertNotEquals(null, result, 'Response should be returned.');
        System.assertEquals(null, result.currentWorkOrderActivityId, 'As no Log Entry returned, current WOA Id should be blank');
    }

    @IsTest
    static void getTradesmanInfo_withContactWoActivity_mockWrong() {
        VTT_Utility_EndpointHandler.logEntryDAO = new WrongMock();
        VTT_TradesmanFlags result;

        Test.startTest();
        result = new VTT_Utility_EndpointHandler().getTradesmanInfo();
        Test.stopTest();

        System.assertNotEquals(null, result, 'Response should be returned.');
        System.assertEquals(null, result.currentWorkOrderActivityId, 'As Log Entry with wrong Status was returned, WOA Id should be blank');
    }

    @IsTest
    static void getTradesmanInfo_withContactWoActivity_mockGood() {
        VTT_Utility_EndpointHandler.logEntryDAO = new GoodMock();
        VTT_TradesmanFlags result;

        Test.startTest();
        result = new VTT_Utility_EndpointHandler().getTradesmanInfo();
        Test.stopTest();

        System.assertNotEquals(null, result, 'Response should be returned.');
        System.assertEquals(UserInfo.getUserId(), result.currentWorkOrderActivityId, 'As Log Entry returned with correct Status, Activity ID should be returned');
    }

    //MOCKS
    private class EmptyMock implements VTT_Utility_WOA_LogEntryDAO {
        public List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesman(Id tradesmanId) {
            return new List<Work_Order_Activity_Log_Entry__c>();
        }
    }
    private class WrongMock implements VTT_Utility_WOA_LogEntryDAO {
        public List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesman(Id tradesmanId) {
            return new List<Work_Order_Activity_Log_Entry__c>{
                    new Work_Order_Activity_Log_Entry__c(
                            Status__c = VTT_Utilities.LOGENTRY_FINISHEDFORTHEDAY,
                            Work_Order_Activity__c = UserInfo.getUserId()
                    )
            };
        }
    }
    private class GoodMock implements VTT_Utility_WOA_LogEntryDAO {
        public List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesman(Id tradesmanId) {
            return new List<Work_Order_Activity_Log_Entry__c>{
                    new Work_Order_Activity_Log_Entry__c(
                            Status__c = VTT_Utilities.LOGENTRY_STARTJOB,
                            Work_Order_Activity__c = UserInfo.getUserId()
                    )
            };
        }
    }

}