/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Selector class containing all queries for HOG_Planner_Group_Notification_Type__c SObject
Test Class:     HOG_PlannerGroupNotificationTypeSlcrTest
History:        jschn 2019-07-03 - Created. - EPD R1
*************************************************************************************************/
public inherited sharing class HOG_PlannerGroupNotificationTypeSelector implements HOG_PlannerGroupNotificationTypeDAO {

    /**
     * Queries HOG Planner Group Notification Type records with certain Role Types (provided as arguments)
     *
     * @param roleTypes
     *
     * @return List<HOG_Planner_Group_Notification_Type__c>
     */
    public List<HOG_Planner_Group_Notification_Type__c> getNotificationTypesForRoleTypes(List<String> roleTypes) {
        return [
                SELECT Id, HOG_Planner_Group__r.Name, Contact__c, Work_Center__c, FLOC__c, FLOC_Excluded__c
                FROM HOG_Planner_Group_Notification_Type__c
                WHERE Role_Type__c IN : roleTypes
        ];
    }

}