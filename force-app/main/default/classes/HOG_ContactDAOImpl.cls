/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Contact DAO Layer implementation
Test Class:     HOG_ContactDAOImplTest
History:        jschn 11/10/2019 - Created.
*************************************************************************************************/
public with sharing class HOG_ContactDAOImpl implements HOG_ContactDAO, VTT_SCContactDAO, VTT_ContactUtilityDAO {

    /**
     * Returns result of the query that selects Contact records based on provided account Id.
     * Only contacts that has User assigned and that user is still active are selected
     * Results are ordered by Name (ASC)
     * and number of results is limited by 5000
     *
     * @param accountId
     *
     * @return List<Contact>
     */
    public List<Contact> getActiveContactsByAccId(Id accountId) {
        return [
                SELECT Name
                FROM Contact
                WHERE AccountId = :accountId
                AND User__c <> NULL
                AND User__r.IsActive = TRUE
                ORDER BY Name
                LIMIT 5000
        ];
    }

    /**
     * Returns result of the query that selects Contact records based on provided account Id.
     * Only contacts that has User assigned are selected.
     * Results are ordered by Name (ASC)
     * and number of results is limited by 5000
     *
     * @param accId
     *
     * @return List<Contact>
     */
    public List<Contact> getContactsByAccount(Id accountId) {
        return [
                SELECT Name
                FROM Contact
                WHERE AccountId = : accountId
                AND User__c <> NULL
                ORDER BY Name
                LIMIT 5000
        ];
    }

    /**
     * Returns result of the query that selects Contact Records related for VTT WorkFlows based on User Id that runs
     * the session.
     *
     * @return List<Contact>
     */
    public List<Contact> getTradesmanInfo() {
        return [
                SELECT Id, Name, Tradesman_Status__c, AccountId, Account.Name, Tradesman_Status_Date__c,
                        Current_Work_Order_Activity__c, Last_Work_Order_Activity__c, Tradesman_Still_Working__c,
                        Tradesman_Start_Work_Enabled__c
                FROM Contact
                WHERE User__c = :UserInfo.getUserId()
        ];
    }

}