/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_TransferForm_ItemForInsert {
	public Equipment__c equipment;
	public String tagColour;
	public HOG_File itemFile;
}