@isTest
private class VendorPortalManagementControllerTest {
	
	@isTest static void testEnableDisableUser() {
		User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        VendorPortalUtility.portalExecuteTriggerCode = false;
        
       	
		Test.startTest();
		System.runAs(runningUser){
			Account portalAccount = AccountTestData.createAccount('PortalAccount', Null);
	        insert portalAccount;
	        portalAccount.IsPartner = true;
	        update portalAccount;
	        
	        //Create Contact under this account and assign it to sysadmin
			Contact user = VTT_TestData.createTradesmanContact('Test', 'Vendor',  portalAccount.id, runningUser.id);
			user.Email = 'test@portal.com';
			//Test tradesmen
			Contact testTradesmen = VTT_TestData.createTradesmanContact('Test', 'Vendor',  portalAccount.id);

			VendorPortalManagementController controller = new VendorPortalManagementController();

 			//Test enable user
 			controller.contactIdToEnablePortal = testTradesmen.id;
 			controller.userType = 'HOG_Vendor_Community_VTT_User';
 			controller.enablePortalUser();
            User u1 = [SELECT Id, ContactId FROM User WHERE ContactId =: testTradesmen.id];
            System.assertNotEquals(null, u1);
            
            //Test disable user
            controller.contactIdToDisablePortal = testTradesmen.id;
            controller.disablePortalUser();
            
            //Test reenable user
            controller.contactIdToDisablePortal = testTradesmen.id;
            controller.userType = 'HOG_Vendor_Community_VTT_User';
            controller.enablePortalUser();
			u1 = [SELECT Id, ContactId, IsActive FROM User WHERE ContactId =: testTradesmen.id];
            System.assertEquals(true, u1.IsActive);
            
            // test pagination methods
            controller.getPortalDisabledVendorSet();
			controller.refreshPageSizePortalDisabled();
            controller.getPortalEnabledVendorSet();
            controller.refreshPageSizePortalEnabled();
                
        }
		Test.stopTest();  
        VendorPortalUtility.portalExecuteTriggerCode = true;
	}
	
	@isTest static void testController() {
		User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        VendorPortalUtility.portalExecuteTriggerCode = false;
        
        System.runAs(runningUser){
        	//Prepare Data
            Account portalAccount = AccountTestData.createAccount('PortalAccount', Null);
	        insert portalAccount;
	        portalAccount.IsPartner = true;
	        update portalAccount;
            
            Account portalAccount2 = AccountTestData.createAccount('PortalAccount2', Null);
	        insert portalAccount2;
	        portalAccount2.IsPartner = true;
	        update portalAccount2;
            
            //Set current page to VPM
            PageReference pageRef = Page.VendorPortalManagement;
            Test.setCurrentPage(pageRef);
            
            //Test when runningUser has no contact assigned to portal enabled account
            VendorPortalManagementController controller1 = new VendorPortalManagementController();
            //Test if message was displayed
            List<Apexpages.Message> msgs = ApexPages.getMessages();
                boolean b = false;
                for(Apexpages.Message msg:msgs){
                    if (msg.getDetail().contains('Error: User has no contact assigned')) b = true;
                }
			system.assert(b);
            
            //Create two contacts under each portal enabled account - message should appear
            Contact user1 = VTT_TestData.createTradesmanContact('Test', 'Vendor',  portalAccount.id, runningUser.id);
            Contact user2 = VTT_TestData.createTradesmanContact('Test2', 'Vendor2',  portalAccount2.id, runningUser.id);
            VendorPortalManagementController controller2 = new VendorPortalManagementController();
            
            List<Apexpages.Message> msgs2 = ApexPages.getMessages();
                boolean c = false;
                for(Apexpages.Message msg:msgs2){
                    if (msg.getDetail().contains('Error: Your user is referenced in multiple contacts that are under portal enabled account')) c = true;
                }
			system.assertEquals(true, c);
        }

        VendorPortalUtility.portalExecuteTriggerCode = true;
        
	}

	
}