/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 3/18/2020   
 */

@IsTest
public class ELG_ShiftSummary_EndpointTest {

	@IsTest
	static void getShiftSummariesTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		System.assertEquals(0, [SELECT count() FROM ELG_Shift_Summary__c]);
		ELG_Shift_Summary__c summary = ELG_ShiftSummary_Endpoint.getShiftSummaries(shift.Id);
		System.assertEquals(1, [SELECT count() FROM ELG_Shift_Summary__c]);
		System.assertEquals(shift.Id, summary.Shift_Assignement__c);
		summary = ELG_ShiftSummary_Endpoint.getShiftSummaries(shift.Id);
		System.assertEquals(1, [SELECT count() FROM ELG_Shift_Summary__c]);
		System.assertEquals(shift.Id, summary.Shift_Assignement__c);
		Test.stopTest();
	}

	@IsTest
	static void getShiftSummariesNegativeTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);

		Test.startTest();
		try {
			ELG_ShiftSummary_Endpoint.getShiftSummaries(shift.Id);
		} catch (Exception ex) {
			System.debug('Exception message: ' + ex.getMessage());
			System.assertNotEquals(null, ex.getMessage());
		}
		Test.stopTest();
	}

	@IsTest
	static void saveShiftSummaryTest() {
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id);
		insert summary;

		Test.startTest();
		summary.HSE_Incidents__c = 'test';
		ELG_ShiftSummary_Endpoint.saveShiftSummary(summary);
		System.assertEquals(summary.HSE_Incidents__c, [SELECT HSE_Incidents__c FROM ELG_Shift_Summary__c LIMIT 1].HSE_Incidents__c);
		Test.stopTest();
	}

	@IsTest
	static void saveShiftSummaryNegativeTest() {
		Test.startTest();
		try {
			ELG_ShiftSummary_Endpoint.saveShiftSummary(new ELG_Shift_Summary__c());
		} catch (Exception ex) {
			System.debug('Exception: ' + ex.getMessage());
			System.assertNotEquals(null, ex.getMessage());
		}
		Test.stopTest();
	}

	@IsTest
	static void loadShiftHandoversTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		List<String> shiftIds = new List<String>();
		shiftIds.add(shift.Id);

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.loadShiftHandovers(shiftIds);
		System.debug('result: ' + result);
		System.assertEquals(1, result.resultObjects.size());
		Test.stopTest();
	}

	@IsTest
	static void loadSummaryBasedOnShiftTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		insert new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id);
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.loadSummaryBasedOnShift(shift.Id);
		System.assertEquals(2, result.resultObjects.size());
		Test.stopTest();
	}

	@IsTest
	static void loadSummaryBasedOnShiftNegativeTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.loadSummaryBasedOnShift('Wrong Id');
		System.assertEquals(null, result.resultObjects);
		System.assertEquals(1, result.errors.size());
		Test.stopTest();
	}

	@IsTest
	static void loadSummaryBasedOnRecordIdTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id);
		insert summary;
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.loadSummaryBasedOnRecordId(summary.Id);
		System.assertEquals(2, result.resultObjects.size());
		Test.stopTest();
	}

	@IsTest
	static void loadSummaryBasedOnRecordIdNegativeTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.loadSummaryBasedOnRecordId('Wrong Id');
		System.assertEquals(null, result.resultObjects);
		System.assertEquals(1, result.errors.size());
		Test.stopTest();
	}

	@IsTest
	static void signOffFromSummaryAndUpdateHandoverTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id);
		insert summary;
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.signOffFromSummaryAndUpdateHandover(shift.Id);
		System.assertEquals(2, result.resultObjects.size());
		System.assertEquals(ELG_Constants.SHIFT_SUMMARY_AWAITING_ACCEPT,
				[SELECT Status__c FROM ELG_Shift_Summary__c WHERE Shift_Assignement__c = :shift.Id].Status__c);
		System.assertEquals(ELG_Constants.HANDOVER_IN_PROGRESS,
				[SELECT Status__c FROM ELG_Shift_Handover__c WHERE Shift_Assignement__c = :shift.Id].Status__c);
		Test.stopTest();
	}

	@IsTest
	static void signOffFromSummaryAndUpdateHandoverUserNotPrimaryTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		List<User> users = [SELECT Id FROM User WHERE Alias = 'theOne' OR Alias = 'BabaJaga'];
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		shift.Primary__c = users[0].Id;
		shift.Shift_Takeover_Reason__c = 'Test';
		update shift;
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id);
		insert summary;
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		System.runAs(users[1]) {
			result = ELG_ShiftSummary_Endpoint.signOffFromSummaryAndUpdateHandover(shift.Id);
			System.assertEquals(null, result.resultObjects);
			System.assertEquals(ELG_Constants.ONLY_SHIFT_LEAD_PRIMARY_CAN_SIGN_OFF, result.errors[0]);
		}
		Test.stopTest();
	}

	@IsTest
	static void signOffFromSummaryAndUpdateHandoverAlreadySignedOffTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id,
				Status__c = ELG_Constants.SHIFT_SUMMARY_AWAITING_ACCEPT);
		insert summary;
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.signOffFromSummaryAndUpdateHandover(shift.Id);
		System.assertEquals(null, result.resultObjects);
		System.assertEquals(ELG_Constants.SHIFT_SUMMARY_CANNOT_BE_UPDATED, result.errors[0]);
		Test.stopTest();
	}

	@IsTest
	static void takeNewShiftAndUpdateSummaryTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id,
		Status__c = ELG_Constants.SHIFT_SUMMARY_AWAITING_ACCEPT);
		insert summary;
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.takeNewShiftAndUpdateSummary(shift.Id);
		System.assertEquals(2, result.resultObjects.size());
		System.assertEquals(ELG_Constants.SHIFT_SUMMARY_ACCEPTED,
				[SELECT Status__c FROM ELG_Shift_Summary__c WHERE Shift_Assignement__c = :shift.Id].Status__c);
		System.assertEquals(ELG_Constants.HANDOVER_ACCEPTED,
				[SELECT Status__c FROM ELG_Shift_Handover__c WHERE Shift_Assignement__c = :shift.Id].Status__c);
		Test.stopTest();
	}

	@IsTest
	static void takeNewShiftAndUpdateSummaryAlreadyAcceptedTest() {
		HOG_CustomResponseImpl result = new HOG_CustomResponseImpl();
		ELG_Post__c post = ELG_TestDataFactory.createPost('Well', true);
		ELG_Shift_Assignement__c shift = ELG_TestDataFactory.createShiftAssignement(post, true);
		ELG_Shift_Summary__c summary = new ELG_Shift_Summary__c(Shift_Assignement__c = shift.Id,
		Status__c = ELG_Constants.SHIFT_SUMMARY_ACCEPTED);
		insert summary;
		post.Shift_Lead_Post__c = true;
		update post;

		Test.startTest();
		result = ELG_ShiftSummary_Endpoint.takeNewShiftAndUpdateSummary(shift.Id);
		System.debug('result: ' +result);
		System.assertEquals(ELG_Constants.SHIFT_SUMMARY_CANNOT_BE_UPDATED, result.errors[0]);
		System.assertEquals(null, result.resultObjects);
		Test.stopTest();
	}

	@TestSetup
	static void createTestUsers() {
		ELG_TestDataFactory.createReviewerAndLogEntryUsers();
	}

}