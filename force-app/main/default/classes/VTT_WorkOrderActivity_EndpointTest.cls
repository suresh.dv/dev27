/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit test for VTT_WorkOrderActivity_Endpoint class
History:        jschn 20/11/2019 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class VTT_WorkOrderActivity_EndpointTest {

    @IsTest
    static void getRecords_mocked_emptyList() {
        Integer expectedRecordCount = 0;
        VTT_WorkOrderActivity_Endpoint.selector = new EmptyListMock();

        Test.startTest();
        List<Work_Order_Activity__c> records = VTT_WorkOrderActivity_Endpoint.getRecords(null);
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Even with bad parameter, SOQL query should return empty list.');
        System.assertEquals(expectedRecordCount, records.size(),
                'There shoulnd\'t be any record with ID of null. Returned records: ' + records.size());
    }

    @IsTest
    static void getRecords_mocked_singleItemList() {
        Integer expectedRecordCount = 1;
        VTT_WorkOrderActivity_Endpoint.selector = new SingleItemListMock();

        Test.startTest();
        List<Work_Order_Activity__c> records = VTT_WorkOrderActivity_Endpoint.getRecords(null);
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Expected List with single record. Result: null.');
        System.assertEquals(expectedRecordCount, records.size(),
                'We were expecting 1 record. Returned: ' + records.size());
    }

    class EmptyListMock implements VTT_WOA_MA_ActivityDAO {
        public List<Work_Order_Activity__c> getRecordsForAssignments(List<Id> woaIds) {
            return new List<Work_Order_Activity__c>();
        }
    }

    class SingleItemListMock implements VTT_WOA_MA_ActivityDAO {
        public List<Work_Order_Activity__c> getRecordsForAssignments(List<Id> woaIds) {
            return new List<Work_Order_Activity__c>{new Work_Order_Activity__c()};
        }
    }

}