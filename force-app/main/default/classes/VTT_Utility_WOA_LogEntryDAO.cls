/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    DAO Interface for VTT Utility Endpoint purposes
History:        jschn 25/10/2019 - Created.
*************************************************************************************************/
public interface VTT_Utility_WOA_LogEntryDAO {

    List<Work_Order_Activity_Log_Entry__c> getLastLogEntryForTradesman(Id tradesmanId);

}