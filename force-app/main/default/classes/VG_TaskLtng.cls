/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Test Class: 	VG_TaskLtngTest
Description:    Vent Gas Alert Task endpoint service class.
History:        jschn 07.10.2018 - Created.
**************************************************************************************************/
public with sharing class VG_TaskLtng {
    
/**********
*CONSTANTS*
**********/
    private static final String CLASS_NAME = 'VG_TaskLtng';
    private static final Boolean DEBUG_ON = HOG_GeneralUtilities.DEBUG_ON;

/**********
*ENDPOINTS*
**********/
    @AuraEnabled public static HOG_RequestResult getTask_v1(String taskId) {
    	if(DEBUG_ON) System.debug(CLASS_NAME + ' -> TaskID: ' + taskId);
    	HOG_RequestResult result;
    	try {
			HOG_Vent_Gas_Alert_Task__c task = getTask(taskId);
    		result = new HOG_SelectResponseImpl(task);
    	} catch(Exception ex) {
    		result = new HOG_SelectResponseImpl(ex);
    		System.debug(CLASS_NAME + ' -> getTask_v1 exception: ' + ex.getMessage());
    	}
    	return result;
    }

    @AuraEnabled public static HOG_RequestResult getNonCompletedTasks_v1(String alertId) {
    	HOG_RequestResult result;
    	try {
    		result = new HOG_SelectResponseImpl(
    			getTasks(alertId, 
	    			new Set<String>{
	    				HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED,
	    				HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS
	    			}
    			)
			);
    	} catch(Exception ex) {
    		result = new HOG_SelectResponseImpl(ex);
    	}
    	return result;
    }

    @AuraEnabled public static HOG_RequestResult getCompletedTasks_v1(String alertId) {
    	HOG_RequestResult result;
    	try {
    		result = new HOG_SelectResponseImpl(
    			getTasks(alertId, 
	    			new Set<String>{
	    				HOG_VentGas_Utilities.TASK_STATUS_COMPLETED,
	    				HOG_VentGas_Utilities.TASK_STATUS_CANCELLED
	    			}
    			)
			);
    	} catch(Exception ex) {
    		result = new HOG_SelectResponseImpl(ex);
    	}
    	return result;
    }

	@AuraEnabled public static HOG_RequestResult getRouteOperator_v1(Id locationId) {
		HOG_RequestResult result;
		try {
			Location__c location = getLocation(locationId);
			if(location != null) {
				Operator_On_Call__c operator = getOperator(location.Route__c);
				if (operator != null) {
					result = new HOG_SelectResponseImpl(getRouteOperatorByUserId(operator.Operator__c));
				}
			}
    	} catch(Exception ex) {
    		result = new HOG_SelectResponseImpl(ex);
    	}
    	return result;
	}

	@AuraEnabled public static HOG_RequestResult upsertTask_v1(String vgTask) {
		HOG_RequestResult result;
		Savepoint sp = Database.setSavepoint();
        try {
        	HOG_Vent_Gas_Alert_Task__c ventGasTask = deserializeAndCleanTask(vgTask);
        	upsert ventGasTask;
			updateAlertStatus(ventGasTask);
			result = new HOG_DMLResponseImpl(ventGasTask);
        } catch(Exception ex) {
        	Database.rollback(sp);
			System.debug(CLASS_NAME + ' -> '+ ex.getMessage());
        	result = new HOG_DMLResponseImpl(ex);
        }
        return result;
	}

/********
*HELPERS*
********/
	private static void updateAlertStatus(HOG_Vent_Gas_Alert_Task__c task) {
		HOG_Vent_Gas_Alert__c alert = (HOG_Vent_Gas_Alert__c) ((HOG_SelectResponseImpl) VG_AlertLtng.getAlert_v1(task.Vent_Gas_Alert__c)).resultObjects.get(0);
		if(alert.Status__c == HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED) {
			alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS;
			update alert;
		}
	}

	private static Location__c getLocation(Id locationId) {
		return [SELECT Route__c 
				FROM Location__c 
				WHERE Id =: locationId];
	}

	private static Operator_On_Call__c getOperator(Id routeId) {
		return [SELECT Operator__c
				FROM Operator_On_Call__c
				WHERE Operator_Route__c =: routeId
				ORDER BY CreatedDate DESC 
				LIMIT 1];
	}

	private static User getRouteOperatorByUserId(Id operatorId) {
		return [SELECT Id, Name 
				FROM User
				WHERE Id =: operatorId];
	}

	private static HOG_Vent_Gas_Alert_Task__c deserializeAndCleanTask(String vgTask) {		
		if(DEBUG_ON) System.debug(CLASS_NAME + ' -> JSON task String: ' + vgTask);
		HOG_Vent_Gas_Alert_Task__c ventGasTask = (HOG_Vent_Gas_Alert_Task__c) JSON.deserialize(vgTask, HOG_Vent_Gas_Alert_Task__c.class);
		ventGasTask = cleanVentGasTask(ventGasTask);
		return ventGasTask;
	}

	private static HOG_Vent_Gas_Alert_Task__c cleanVentGasTask(HOG_Vent_Gas_Alert_Task__c ventGasTask) {
		ventGasTask.Vent_Gas_Alert__r = null;
		if (String.isBlank(ventGasTask.Id)) {
			ventGasTask.Name = ventGasTask.Name + String.valueOf(Date.today());
		}
		if(DEBUG_ON) System.debug(CLASS_NAME + ' -> Deserialized record: ' + ventGasTask);
		return ventGasTask;
	}

	private static List<HOG_Vent_Gas_Alert_Task__c> getTasks(String alertId, Set<String> statuses) {
		return [SELECT Id
					, Name
					, Assignee1__c
					, Assignee1__r.Name
					, Assignee2__c
					, Assignee2__r.Name
					, Assignee_Type__c
					, LastModifiedDate
					, Priority__c
					, Status__c
					, Remind__c
				FROM HOG_Vent_Gas_Alert_Task__c
				WHERE Vent_Gas_Alert__c =: alertId
					AND Status__c IN : statuses
				ORDER BY CreatedDate ASC];
	}

	private static HOG_Vent_Gas_Alert_Task__c getTask(String taskId) {
		return [SELECT Id
               		, Name
					, Assignee1__c
					, Assignee1__r.Name
					, Assignee2__c
					, Assignee2__r.Name
               		, Assignee_Type__c
                	, Comments__c
                	, Is_Closed__c
                	, My_Task__c
					, Priority__c
                	, Production_EngineerID__c
					, Remind__c
					, Status__c
                	, Subject__c
                	, Vent_Gas_Alert__c
                	, Vent_Gas_Alert__r.Name
					, Vent_Gas_Alert__r.Well_Location__c
               FROM HOG_Vent_Gas_Alert_Task__c
               WHERE Id =: taskId];
	}

}