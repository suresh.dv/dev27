/*************************************************************************************************\
Author:         Shreyas Dhond
Company:        Husky Energy
Description:    Unit test for controller Extension for Canceling Truck Trip page.
Tested Class:   FM_CancelTruckTripControllerX
History:        jschn 06.11.2018    - Added coverage for ability to create stuck truck for external dispatchers 
                                    - US W-001151
**************************************************************************************************/
@isTest
private class FM_CancelTruckTripControllerXTest {

	public static final String CLASS_NAME = 'FM_CancelTruckTripControllerXTest';
	
	@isTest 
	static void dispatcherVendorCancelTestWithoutStuckTruck_Success() {
		User runningUser = FM_TestData.createUser();
        runningUser = FM_TestData.assignPermissionSet(runningUser, 'HOG_FM_Dispatcher_Vendor');
        runningUser = FM_TestData.assignProfile(runningUser, 'Standard HOG – Fluid Management Vendor Portal User');
        List<SelectOption> cancelOptions = FM_LoadRequest_Utilities.getCancelReasons();

        //Query Trucktrip
        FM_Truck_Trip__c truckTrip = [Select Id, Name, Load_Request_Cancel_Reason__c,
        									 Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
        									, Carrier__c
											, Unit__c
											, Load_Request__r.Source_Location__c
											, Load_Request__r.Source_Facility__c
        							  From FM_Truck_Trip__c];

        System.runAs(runningUser) {
        	Test.startTest();
			Test.setCurrentPage(Page.FM_CancelTruckTrip);

			ApexPages.StandardController con = new ApexPages.StandardController(truckTrip);
			FM_CancelTruckTripControllerX ext = new FM_CancelTruckTripControllerX(con);
			System.assertEquals(ext.getLoadRequestCancelReasons(), cancelOptions);

			ext.trucktripRecord.Load_Request_Cancel_Reason__c = cancelOptions[0].getValue();
			ext.handleCancelReasonChange();
			System.assertEquals(ext.trucktripRecord.Load_Request_Cancel_Comments__c, '');

			ext.trucktripRecord.Load_Request_Cancel_Reason__c = cancelOptions[cancelOptions.size() - 1].getValue();
			ext.handleCancelReasonChange();
			ext.trucktripRecord.Load_Request_Cancel_Comments__c = 'Other comments reason';
			ext.submit();

			truckTrip = [Select Id, Name, Load_Request_Cancel_Reason__c,
								Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
						 From FM_Truck_Trip__c];	
			System.assertEquals(ext.trucktripRecord.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_CANCELLED);
			System.assertEquals(ext.trucktripRecord.Load_Request_Cancel_Reason__c, cancelOptions[cancelOptions.size() - 1].getValue());
			System.assertEquals(ext.trucktripRecord.Load_Request_Cancel_Comments__c, 'Other comments reason');		

			Test.stopTest();
        }
	}

	@isTest
	static void dispatcherVendorCancelTestWithoutStuckTruckLightning_Success() {
		User runningUser = FM_TestData.createUser();
        runningUser = FM_TestData.assignPermissionSet(runningUser, 'HOG_FM_Dispatcher_Vendor');
        runningUser = FM_TestData.assignProfile(runningUser, 'Standard HOG – Fluid Management Vendor Portal User');
        List<SelectOption> cancelOptions = FM_LoadRequest_Utilities.getCancelReasons();

        //Query Trucktrip
        FM_Truck_Trip__c truckTrip = [Select Id, Name, Load_Request_Cancel_Reason__c,
        									 Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
        									, Carrier__c
											, Unit__c
											, Load_Request__r.Source_Location__c
											, Load_Request__r.Source_Facility__c
        							  From FM_Truck_Trip__c];

        System.runAs(runningUser) {
        	Test.startTest();
			Test.setCurrentPage(Page.FM_CancelTruckTrip);

			ApexPages.StandardController con = new ApexPages.StandardController(truckTrip);
			FM_CancelTruckTripControllerX ext = new FM_CancelTruckTripControllerX(con);
			System.assertEquals(ext.getLoadRequestCancelReasons(), cancelOptions);

			ext.trucktripRecord.Load_Request_Cancel_Reason__c = cancelOptions[0].getValue();
			ext.handleCancelReasonChange();
			System.assertEquals(ext.trucktripRecord.Load_Request_Cancel_Comments__c, '');

			ext.trucktripRecord.Load_Request_Cancel_Reason__c = cancelOptions[cancelOptions.size() - 1].getValue();
			ext.handleCancelReasonChange();
			ext.trucktripRecord.Load_Request_Cancel_Comments__c = 'Other comments reason';
			ext.submitLtng();

			truckTrip = [Select Id, Name, Load_Request_Cancel_Reason__c,
								Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
						 From FM_Truck_Trip__c];

			Test.stopTest();

			System.assertEquals(ext.trucktripRecord.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_CANCELLED);
			System.assertEquals(ext.trucktripRecord.Load_Request_Cancel_Reason__c, cancelOptions[cancelOptions.size() - 1].getValue());
			System.assertEquals(ext.trucktripRecord.Load_Request_Cancel_Comments__c, 'Other comments reason');
			System.assert(ext.successSave);
			System.assert(!ext.isLightningUser);
        }
	}

	@isTest
	static void dispatcherVendorCancelTestWithoutStuckTruckLightning_Fail() {
		User dispatcher = HOG_TestDataFactory.createUser('Forest', 'Gump', 'Fgmp');
		dispatcher = HOG_TestDataFactory.assignProfile(dispatcher, 'Standard HOG – Fluid Management Vendor Portal User');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher_Vendor');

		List<SelectOption> cancelOptions = FM_LoadRequest_Utilities.getCancelReasons();

		FM_Truck_Trip__c truckTrip = [SELECT Id, Name, Load_Request_Cancel_Reason__c,
				Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
				, Carrier__c
				, Unit__c
				, Load_Request__r.Source_Location__c
				, Load_Request__r.Source_Facility__c
		FROM FM_Truck_Trip__c];

		Carrier_Unit__c unit = [SELECT Id
		FROM Carrier_Unit__c
		WHERE Carrier__c =: truckTrip.Carrier__c
		LIMIT 1];

		System.runAs(dispatcher) {
			Test.startTest();
			Test.setCurrentPage(Page.FM_CancelTruckTrip);

			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(truckTrip);
			FM_CancelTruckTripControllerX controller = new FM_CancelTruckTripControllerX(stdCtrl);

			System.assertEquals(controller.getLoadRequestCancelReasons(), cancelOptions);
			controller.trucktripRecord.Load_Request_Cancel_Reason__c = cancelOptions[0].getValue();
			controller.isStuckTruck = true;

			System.assert(controller.isExternalDispatcher);
			System.assert(!ApexPages.hasMessages());

			Id carrierId = controller.stuckTruck.Carrier__c;
			controller.stuckTruck.Carrier__c = null;
			controller.stuckTruck.Carrier_Unit__c = unit.Id;
			controller.submit();
			System.assert(ApexPages.hasMessages());
			System.assertEquals(1, ApexPages.getMessages().size());

			controller.stuckTruck.Carrier__c = carrierId;
			controller.stuckTruck.Carrier_Unit__c = null;
			controller.submitLtng();
			System.assert(ApexPages.hasMessages());

			truckTrip = [
					Select Id, Name, Load_Request_Cancel_Reason__c,
							Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
					From FM_Truck_Trip__c
			];

			Test.stopTest();

			System.assertNotEquals(controller.trucktripRecord.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_CANCELLED);
			System.assert(!controller.successSave);
			System.assert(!controller.isLightningUser);
        }
	}

	@isTest static void dispatcherVendorCancelTestWithStuckTruck_Success() {
		User dispatcher = HOG_TestDataFactory.createUser('Forest', 'Gump', 'Fgmp');
		dispatcher = HOG_TestDataFactory.assignProfile(dispatcher, 'Standard HOG – Fluid Management Vendor Portal User');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher_Vendor');

		List<SelectOption> cancelOptions = FM_LoadRequest_Utilities.getCancelReasons();

        FM_Truck_Trip__c truckTrip = [SELECT Id, Name, Load_Request_Cancel_Reason__c,
        									 Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
        									, Carrier__c
											, Unit__c
											, Load_Request__r.Source_Location__c
											, Load_Request__r.Source_Facility__c
        							  FROM FM_Truck_Trip__c];
	  	Integer stuckTruckCountBefore = [SELECT Count() 
	  									FROM FM_Stuck_Truck__c];

		Carrier_Unit__c unit = [SELECT Id 
								FROM Carrier_Unit__c
								WHERE Carrier__c =: truckTrip.Carrier__c
								LIMIT 1];

		System.runAs(dispatcher) {
			Test.startTest();
			Test.setCurrentPage(Page.FM_CancelTruckTrip);

			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(truckTrip);
			FM_CancelTruckTripControllerX controller = new FM_CancelTruckTripControllerX(stdCtrl);

			System.assertEquals(controller.getLoadRequestCancelReasons(), cancelOptions);
			controller.trucktripRecord.Load_Request_Cancel_Reason__c = cancelOptions[0].getValue();
			controller.isStuckTruck = true;
			System.assert(controller.isExternalDispatcher);
			controller.stuckTruck.Carrier_Unit__c = unit.Id;
			controller.submit();

			Test.stopTest();

			Integer stuckTruckCountAfter = [SELECT Count() 
	  										FROM FM_Stuck_Truck__c];
			System.assertNotEquals(stuckTruckCountBefore, stuckTruckCountAfter);
			System.assertEquals(stuckTruckCountBefore+1, stuckTruckCountAfter);

			truckTrip = [Select Id, Name, Load_Request_Cancel_Reason__c,
								Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
						 From FM_Truck_Trip__c];	
			System.assertEquals(controller.trucktripRecord.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_CANCELLED);
			System.assertEquals(controller.trucktripRecord.Load_Request_Cancel_Reason__c, cancelOptions[0].getValue());
		}
	}

	@isTest static void dispatcherVendorCancelTestWithStuckTruck_Fail() {
		User dispatcher = HOG_TestDataFactory.createUser('Forest', 'Gump', 'Fgmp');
		dispatcher = HOG_TestDataFactory.assignProfile(dispatcher, 'Standard HOG – Fluid Management Vendor Portal User');
		dispatcher = HOG_TestDataFactory.assignPermissionSet(dispatcher, 'HOG_FM_Dispatcher_Vendor');

		List<SelectOption> cancelOptions = FM_LoadRequest_Utilities.getCancelReasons();

        FM_Truck_Trip__c truckTrip = [SELECT Id, Name, Load_Request_Cancel_Reason__c,
        									 Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
        									, Carrier__c
											, Unit__c
											, Load_Request__r.Source_Location__c
											, Load_Request__r.Source_Facility__c
        							  FROM FM_Truck_Trip__c];

		Carrier_Unit__c unit = [SELECT Id 
								FROM Carrier_Unit__c
								WHERE Carrier__c =: truckTrip.Carrier__c
								LIMIT 1];

		System.runAs(dispatcher) {
			Test.startTest();
			Test.setCurrentPage(Page.FM_CancelTruckTrip);

			ApexPages.StandardController stdCtrl = new ApexPages.StandardController(truckTrip);
			FM_CancelTruckTripControllerX controller = new FM_CancelTruckTripControllerX(stdCtrl);

			System.assertEquals(controller.getLoadRequestCancelReasons(), cancelOptions);
			controller.trucktripRecord.Load_Request_Cancel_Reason__c = cancelOptions[0].getValue();
			controller.isStuckTruck = true;
			
			System.assert(controller.isExternalDispatcher);
			System.assert(!ApexPages.hasMessages());

			Id carrierId = controller.stuckTruck.Carrier__c;
			controller.stuckTruck.Carrier__c = null;
			controller.stuckTruck.Carrier_Unit__c = unit.Id;
			controller.submit();
			System.assert(ApexPages.hasMessages());
			System.assertEquals(1, ApexPages.getMessages().size());
			
			controller.stuckTruck.Carrier__c = carrierId;
			controller.stuckTruck.Carrier_Unit__c = null;
			controller.submit();
			System.assert(ApexPages.hasMessages());
			System.assertEquals(2, ApexPages.getMessages().size());

			Boolean hasMissingCarrier = false;
			Boolean hasMissingUnit = false;
			for(ApexPages.Message msg : ApexPages.getMessages()) {
				hasMissingUnit = hasMissingUnit || msg.getSummary().contains(FM_CancelTruckTripControllerX.ERROR_MSG_MISSING_UNIT);
				hasMissingCarrier = hasMissingCarrier || msg.getSummary().contains(FM_CancelTruckTripControllerX.ERROR_MSG_MISSING_CARRIER);
			}
			System.assert(hasMissingUnit && hasMissingCarrier);

			System.assertEquals(0, [SELECT Count() FROM FM_Stuck_Truck__c]);
			truckTrip = [Select Id, Name, Load_Request_Cancel_Reason__c,
								Load_Request_Cancel_Comments__c, Truck_Trip_Status__c
						 From FM_Truck_Trip__c];	
			System.assertNotEquals(controller.trucktripRecord.Truck_Trip_Status__c, FM_Utilities.TRUCKTRIP_STATUS_CANCELLED);

			Test.stopTest();
		}
	}

	@testSetup
	static void createTestData() {
        Account acc = (Account) HOG_SObjectFactory.createSObject(new Account(), true);
        Field__c field = HOG_TestDataFactory.createAMU(true);

  		Route__c route = HOG_TestDataFactory.createRoute(true);
		Carrier__c carrier = FM_TestDataFactory.createCarrier(acc.Id, true);
        Carrier_Unit__c carrierUnit = FM_TestDataFactory.createCarrierUnit(carrier.Id, true);
        Location__c loc = HOG_TestDataFactory.createLocation(route.Id, field.Id, true);

        Equipment_Tank__c eq = HOG_TestDataFactory.createEquipmentTank(loc, true);

		FM_Run_Sheet__c rs = FM_TestDataFactory.createRunSheetOnLocation(loc.Id, true);
		FM_TestDataFactory.createLoadRequestOnLocation(carrier.Id, null, null,
                                                        'O', 
                                                        rs.Id,
                                                        'Night',
                                                        loc.Id,
                                                        FM_LoadRequest_Utilities.LOADREQUEST_STATUS_SUBMITTED,
                                                        true); 
	}
	
}