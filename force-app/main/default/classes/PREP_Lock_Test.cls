/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PREP_Lock_Test {

    static testMethod void myUnitTest() 
    {       
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)']; 
        
        User u = new User(Alias = 'standt', Email='PREP@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP@testorg.com');
            
        insert u;
        
        PREP_Discipline__c disp = new PREP_Discipline__c();
        disp.Name = 'Disp 1';
        disp.Active__c = true;
        disp.SCM_Manager__c = u.Id;
        
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c();
        cat.Name = 'Category';
        cat.Active__c = true;
        cat.Discipline__c = disp.Id;
        
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c();
        sub.Name = 'SubCat';
        sub.Category__c = cat.Id;
        sub.Category_Manager__c = u.Id;
        sub.Category_Specialist__c = u.Id;
        sub.Active__c = true;
        
        insert sub;
        
        PREP_Initiative__c ini = new PREP_Initiative__c();
        ini.Initiative_Name__c = 'Test ini';
        ini.Discipline__c = disp.Id;
        ini.Category__c = cat.Id;
        ini.Sub_Category__c = sub.Id;
        ini.Status__c = 'Active';
        ini.Type__c = 'Category';
        ini.Risk_Level__c = 'High';
        ini.HSSM_Package__c = 'Full';
        ini.Workbook__c = 'Yes';
        ini.SRPM_Package__c = 'As Needed';
        ini.Aboriginal__c = 'No';
        ini.Global_Sourcing__c = 'Yes';
        ini.OEM__c = 'Yes';
        ini.Rate_Validation__c = 'Yes';
        ini.Reverse_Auction__c = 'No';
        ini.Rebate__c = 'Multiple Area Award';
        ini.Rebate_Frequency__c = 'Annually';
        ini.SCM_Manager__c = u.Id;
        ini.Category_Manager__c = u.Id;
        ini.Category_Specialist__c = u.Id;
        
        insert ini;
        
        Id baseRecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Locked Global Baseline').getRecordTypeId();
        
        PREP_Baseline__c baseline = new PREP_Baseline__c();
        baseline.Baseline_Spend_Dollar_Target__c = 10000;
        baseline.Local_Baseline_Spend_Percent__c = 0.10;
        baseline.Local_Baseline_Savings_Percent__c = 0.10;
        baseline.Global_Baseline_Savings_Percent__c = 0.10;
        baseline.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 10);
        baseline.RecordTypeId = baseRecordTypeId;
        baseline.Status__c = 'Approved';
        baseline.Initiative_Id__c = ini.Id;
        insert baseline;
        
        
        Id BUARecordTypeId = Schema.SObjectType.PREP_Business_Unit_Baseline_Allocation__c.getRecordTypeInfosByName().get('Business Unit Baseline Allocation').getRecordTypeId();
        
        PREP_Business_Unit_Baseline_Allocation__c BUA = new PREP_Business_Unit_Baseline_Allocation__c();
        BUA.Baseline_Id__c = baseline.Id;
        BUA.Business_Unit__c = 'Downstream';
        BUA.Allocation_Level__c = 'Downstream Commercial';
        BUA.Baseline_Spend_Allocation_Percent__c = 100;
        BUA.RecordTypeId = BUARecordTypeId;
        
        insert BUA;
        
        PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c();
        MS.Name = 'MS Name';
        MS.Material_Service_Group_Name__c = 'MS';
        MS.Sub_Category__c = sub.Id;
        MS.Category_Manager__c = u.Id;
        MS.SAP_Short_Text_Name__c = 'MS';
        MS.Active__c = true;
        MS.Type__c = 'Material';
        MS.Category_Specialist__c = u.Id;
        MS.Includes__c = 'MS';
        MS.Does_Not_Include__c = 'MS';
        MS.Category_Link__c = cat.Id;
        
        insert MS;
        
        PREP_Material_Service_Group__c MS2 = new PREP_Material_Service_Group__c();
        MS2.Name = 'MS Name';
        MS2.Material_Service_Group_Name__c = 'MS';
        MS2.Sub_Category__c = sub.Id;
        MS2.Category_Manager__c = u.Id;
        MS2.SAP_Short_Text_Name__c = 'MS';
        MS2.Active__c = true;
        MS2.Type__c = 'Material';
        MS2.Category_Specialist__c = u.Id;
        MS2.Includes__c = 'MS';
        MS2.Does_Not_Include__c = 'MS';
        MS2.Category_Link__c = cat.Id;
        
        insert MS2;
        
        Id MSARecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Category Material / Service Group Baseline Allocation').getRecordTypeId();
        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSA = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSA.Baseline_Id__c = baseline.Id;
        MSA.Material_Service_Group__c = MS.Id;
        MSA.RecordTypeId = MSARecordTypeId;
        MSA.Baseline_Spend_Allocation_Percent__c = 100;
        MSA.Sub_Category__c = sub.Id;
        insert MSA;
        
        PREP_Business_Unit_Baseline_Allocation__c BUA2 = new PREP_Business_Unit_Baseline_Allocation__c();
        BUA2.Baseline_Id__c = baseline.Id;
        BUA2.Business_Unit__c = 'Atlantic Region';
        BUA2.Allocation_Level__c = 'Atlantic Region – Drilling & Completions';
        BUA2.RecordTypeId = BUARecordTypeId;
        BUA2.Baseline_Spend_Allocation_Percent__c = 100;

        
        insert BUA2;
        
        PREP_Mat_Serv_Group_Baseline_Allocation__c MSA2 = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
        MSA2.Baseline_Id__c = baseline.Id;
        MSA2.Material_Service_Group__c = MS2.Id;
        MSA2.RecordTypeId = MSARecordTypeId;
        MSA2.Baseline_Spend_Allocation_Percent__c = 100;
        MSA2.Sub_Category__c = sub.Id;
        
        insert MSA2;
        
        
    }
}