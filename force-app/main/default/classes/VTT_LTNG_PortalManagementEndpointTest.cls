/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Unit Test for VTT_LTNG_PortalManagementEndpoint & VTT_LTNG_PortalManagementService class
History:        mbrimus 05/12/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_LTNG_PortalManagementEndpointTest {

    @IsTest
    static void loadUserData_noData() {
        HOG_CustomResponseImpl result;

        Test.startTest();
        result = VTT_LTNG_PortalManagementEndpoint.loadUserData();
        Test.stopTest();

        System.assertNotEquals(null, result.resultObjects);

    }

    @IsTest
    static void loadUserData_communityUser() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;

        Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
        insert portalAccount;
        portalAccount.IsPartner = true;
        update portalAccount;
        Contact supervisorContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
        System.assertNotEquals(null, supervisorContact);

        // Enable in lightning community
        VTT_LTNG_PortalManagementEndpoint.enableLightningUser(supervisorContact, VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING);
        User supervisorUser = [SELECT Id, ContactId FROM User WHERE ContactId = :supervisorContact.Id];
        supervisorContact.User__c = supervisorUser.Id;
        update supervisorContact;
        System.assertNotEquals(null, supervisorUser);

        Test.startTest();

        System.runAs(supervisorUser) {
            result = VTT_LTNG_PortalManagementEndpoint.loadUserData();
        }
        Test.stopTest();

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(expectedSize, result.resultObjects.size());

        VTT_LTNG_PortalManagementResponse model = (VTT_LTNG_PortalManagementResponse) result.resultObjects.get(0);
        Boolean isCommunityUser = model.isCommunityUser;
        Account userAccount = model.vendorAccount;
        List<Contact> disabledContacts = model.disabledContacts;
        List<Contact> enabledContacts = model.enabledContacts;
        List<Account> vendorAccounts = model.vendorAccounts;
        Id recType = model.hogContactRecordType;

        System.assertEquals(true, isCommunityUser);
        System.assertNotEquals(null, userAccount);
        System.assertEquals(0, disabledContacts.size());
        System.assertEquals(1, enabledContacts.size());
        System.assertEquals(null, vendorAccounts);
        System.assertEquals(null, recType);

    }

    @IsTest
    static void modelCoverage() {
        VTT_LTNG_PortalManagementResponse model = new VTT_LTNG_PortalManagementResponse();
        model.isCommunityUser = false;
        model.vendorAccount = new Account();
        model.disabledContacts = new List<Contact>();
        model.enabledContacts = new List<Contact>();
        model.vendorAccounts = new List<Account>();
        model.hogContactRecordType = Id.valueOf('001xa000003DIlo');
    }

    @IsTest
    static void loadUserData_internalUser() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;

        Test.startTest();

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;

            result = VTT_LTNG_PortalManagementEndpoint.loadUserData();
        }
        Test.stopTest();

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(expectedSize, result.resultObjects.size());
    }

    @IsTest
    static void loadAccountContacts_noData() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;

        Test.startTest();

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            result = VTT_LTNG_PortalManagementEndpoint.loadAccountContacts(null);
        }
        Test.stopTest();

        System.assertNotEquals(null, result.resultObjects);
    }

    @IsTest
    static void loadAccountContacts_withata() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;

        Test.startTest();

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;

            result = VTT_LTNG_PortalManagementEndpoint.loadAccountContacts(portalAccount.Id);
        }
        Test.stopTest();

        System.assertNotEquals(null, result.resultObjects);
        System.assertEquals(expectedSize, result.resultObjects.size());
    }

    @IsTest
    static void enableInLightning_withData() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;
        Contact supervisorContact;

        Test.startTest();
        Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
        insert portalAccount;
        portalAccount.IsPartner = true;
        update portalAccount;
        supervisorContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
        System.assertNotEquals(null, supervisorContact);

        result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(supervisorContact, VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING);
        Test.stopTest();

        List<User> resultUser = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(expectedSize, resultUser.size());
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING, resultUser.get(0).Vendor_Portal_User_Type__c);

    }

    @IsTest
    static void enableInLightning_noData() {

        HOG_CustomResponseImpl result;

        Test.startTest();

        System.runAs(VTT_TestData.createVTTAdminUser()) {
            result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(null, null);
        }
        Test.stopTest();

        System.assertEquals(null, result.resultObjects);
    }

    @IsTest
    static void migrateToLightning_supervisor() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;
        Contact supervisorContact;
        User runningUser = VTT_TestData.createVTTAdminUser();
        runningUser = HOG_TestDataFactory.assignRole(runningUser, 'Dispatcher');
        System.runAs(runningUser) {
            Test.startTest();
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;
            supervisorContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
            System.assertNotEquals(null, supervisorContact);
            result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(supervisorContact, VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR);
        }
        List<User> classicSupervisor = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(expectedSize, classicSupervisor.size());
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR, classicSupervisor.get(0).Vendor_Portal_User_Type__c);
        supervisorContact.User__c = classicSupervisor.get(0).Id;
        result = VTT_LTNG_PortalManagementEndpoint.migrateToCommunity(supervisorContact);
        Test.stopTest();

        List<User> lightningUser = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING, lightningUser.get(0).Vendor_Portal_User_Type__c);
        System.assertEquals(expectedSize, lightningUser.size());


    }

    @IsTest
    static void migrateToLightning_tradesman() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;
        Contact supervisorContact;
        User runningUser = VTT_TestData.createVTTAdminUser();
        runningUser = HOG_TestDataFactory.assignRole(runningUser, 'Dispatcher');

        System.runAs(runningUser) {
            Test.startTest();
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;
            supervisorContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
            System.assertNotEquals(null, supervisorContact);
            result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(supervisorContact, VendorPortalUtility.VENDOR_PORTAL_TRADESMAN);
        }
        List<User> classicSupervisor = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(expectedSize, classicSupervisor.size());
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_TRADESMAN, classicSupervisor.get(0).Vendor_Portal_User_Type__c);
        supervisorContact.User__c = classicSupervisor.get(0).Id;
        result = VTT_LTNG_PortalManagementEndpoint.migrateToCommunity(supervisorContact);
        Test.stopTest();

        List<User> lightningUser = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_TRADESMAN_LIGHTNING, lightningUser.get(0).Vendor_Portal_User_Type__c);
        System.assertEquals(expectedSize, lightningUser.size());
    }

    @IsTest
    static void revertFromLightning_toClassicCommunity() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;
        Contact tradesmanContact;
        User runningUser = VTT_TestData.createVTTAdminUser();
        runningUser = HOG_TestDataFactory.assignRole(runningUser, 'Dispatcher');

        System.runAs(runningUser) {
            Test.startTest();
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;
            tradesmanContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
            System.assertNotEquals(null, tradesmanContact);
            result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(tradesmanContact, VendorPortalUtility.VENDOR_PORTAL_TRADESMAN);
        }
        List<User> classicSupervisor = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :tradesmanContact.Id];
        System.assertEquals(expectedSize, classicSupervisor.size());
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_TRADESMAN, classicSupervisor.get(0).Vendor_Portal_User_Type__c);
        tradesmanContact.User__c = classicSupervisor.get(0).Id;
        result = VTT_LTNG_PortalManagementEndpoint.migrateToCommunity(tradesmanContact);

        List<User> lightningUser = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :tradesmanContact.Id];
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_TRADESMAN_LIGHTNING, lightningUser.get(0).Vendor_Portal_User_Type__c);
        System.assertEquals(expectedSize, lightningUser.size());

        // Revert back to classic
        result = VTT_LTNG_PortalManagementEndpoint.migrateToCommunity(tradesmanContact);

        Test.stopTest();

        lightningUser = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :tradesmanContact.Id];
        System.assertEquals(VendorPortalUtility.VENDOR_PORTAL_TRADESMAN, lightningUser.get(0).Vendor_Portal_User_Type__c);
        System.assertEquals(expectedSize, lightningUser.size());
    }

    @IsTest
    static void enableInLightning_invalidValue() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;
        Contact supervisorContact;
        User runningUser = VTT_TestData.createVTTAdminUser();
        runningUser = HOG_TestDataFactory.assignRole(runningUser, 'Dispatcher');
        System.runAs(runningUser) {
            Test.startTest();
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;
            supervisorContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
            System.assertNotEquals(null, supervisorContact);
            result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(supervisorContact, 'undefined');
        }
        Test.stopTest();

        List<User> lightningUser = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(0, lightningUser.size());
    }


    @IsTest
    static void disableUser_internal() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;
        Contact supervisorContact;

        User runningUser = VTT_TestData.createVTTAdminUser();
        runningUser = HOG_TestDataFactory.assignRole(runningUser, 'Dispatcher');
        System.runAs(runningUser) {
            Test.startTest();
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;
            supervisorContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
            System.assertNotEquals(null, supervisorContact);
            result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(supervisorContact, VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING);
            List<User> users = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
            System.assertEquals(expectedSize, users.size());
            supervisorContact.User__c = users.get(0).Id;
        }

        result = VTT_LTNG_PortalManagementEndpoint.disableUser(supervisorContact, false);

        List<User> lightningUser = [SELECT Id, IsActive, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(expectedSize, lightningUser.size());
        System.assertEquals(false, lightningUser.get(0).IsActive);
    }

    @IsTest
    static void disableUser_community() {
        VendorPortalUtility.portalExecuteTriggerCode = false;

        HOG_CustomResponseImpl result;
        Integer expectedSize = 1;
        Contact supervisorContact;

        User runningUser = VTT_TestData.createVTTAdminUser();
        runningUser = HOG_TestDataFactory.assignRole(runningUser, 'Dispatcher');
        System.runAs(runningUser) {
            Test.startTest();
            Account portalAccount = AccountTestData.createAccount('PortalAccount', null);
            insert portalAccount;
            portalAccount.IsPartner = true;
            update portalAccount;
            supervisorContact = VTT_TestData.createTradesmanContact('Test', 'Vendor', portalAccount.Id, null);
            System.assertNotEquals(null, supervisorContact);
            result = VTT_LTNG_PortalManagementEndpoint.enableLightningUser(supervisorContact, VendorPortalUtility.VENDOR_PORTAL_SUPERVISOR_LIGHTNING);
            List<User> users = [SELECT Id, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
            System.assertEquals(expectedSize, users.size());
            supervisorContact.User__c = users.get(0).Id;
        }
        Contact refreshedContact = [SELECT Id, Name, LastName, Account.Name FROM Contact WHERE Id = :supervisorContact.Id];
        result = VTT_LTNG_PortalManagementEndpoint.disableUser(refreshedContact, true);

        List<User> lightningUser = [SELECT Id, IsActive, ContactId, Vendor_Portal_User_Type__c FROM User WHERE ContactId = :supervisorContact.Id];
        System.assertEquals(expectedSize, lightningUser.size());
        System.assertEquals(true, lightningUser.get(0).IsActive);
    }
}