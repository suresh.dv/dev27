/*************************************************************************************************\
Author:         Marcel Brimus
Company:        Husky Energy
Description:    Class that carries information for form related actions.
Test Class:     SNR_EndpointTest, SNR_ServiceTest
History:        mbrimus 2020-01-10 - Created. - SNR Migration
*************************************************************************************************/
public inherited sharing class SNR_Response extends HOG_RequestResult {
    @AuraEnabled public List<AggregateResult> serviceCategories = new List<AggregateResult>();
    @AuraEnabled public List<AggregateResult> serviceTypes = new List<AggregateResult>();
    @AuraEnabled public List<AggregateResult> serviceActivities = new List<AggregateResult>();
    @AuraEnabled public List<HOG_Notification_Type_Priority__c> servicePriorities = new List<HOG_Notification_Type_Priority__c>();
    @AuraEnabled public List<AggregateResult> serviceRequired = new List<AggregateResult>();
    @AuraEnabled public List<AggregateResult> serviceSpecifics = new List<AggregateResult>();
    @AuraEnabled public SNR_FLOCModel FLOCData;
    public SNR_Response() {
        success = true;
    }

    public SNR_Response(Exception ex) {
        setErrorMessage(HOG_ErrorPrettifyService.prettifyErrorMessage(ex.getMessage()));
    }

    public SNR_Response(String errorMessage) {
        setErrorMessage(errorMessage);
    }

    private void setErrorMessage(String errorMessage) {
        if (errors == null) {
            errors = new List<String>();
        }
        errors.add(errorMessage);
        this.success = false;
    }

    public void addServiceCategories(List<AggregateResult> result) {
        if (result != null)serviceCategories.addAll(result);
    }

    public void addServiceType(List<AggregateResult> result) {
        if (result != null)serviceTypes.addAll(result);
    }

    public void addServiceActivities(List<AggregateResult> result) {
        if (result != null)serviceActivities.addAll(result);
    }

    public void addServicePriorities(List<HOG_Notification_Type_Priority__c> result) {
        if (result != null)servicePriorities.addAll(result);
    }

    public void addServiceRequired(List<AggregateResult> result) {
        if (result != null)serviceRequired.addAll(result);
    }

    public void addServiceSpecifics(List<AggregateResult> result) {
        if (result != null)serviceSpecifics.addAll(result);
    }

    public void addFLOCData(SNR_FLOCModel data) {
        this.FLOCData = data;
    }

    public List<String> addError(HOG_Exception ex) {
        setErrorMessage(ex.getMessage());
        return errors;
    }

    public List<String> addError(Exception ex) {
        setErrorMessage(HOG_ErrorPrettifyService.prettifyErrorMessage(ex.getMessage()));
        return errors;
    }

    public List<String> addError(String errorMsg) {
        setErrorMessage(errorMsg);
        return errors;
    }

}