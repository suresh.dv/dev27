/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 *
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class HCVC_MapControllerTest {

   public static Id crudeRecordTypeId {get {if(crudeRecordTypeId==null)crudeRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Crude Pipeline').getRecordTypeId(); return crudeRecordTypeId;}set;}
   public static Id overviewRecordTypeId {get {if(overviewRecordTypeId==null)overviewRecordTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Overview').getRecordTypeId(); return overviewRecordTypeId;}set;}
   public static Id refineryTypeId {get {if(refineryTypeId==null)refineryTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery').getRecordTypeId(); return refineryTypeId;}set;}
   public static Id refineryUnitTypeId {get {if(refineryUnitTypeId==null)refineryUnitTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Unit').getRecordTypeId(); return refineryUnitTypeId;}set;}
   public static Id refineryRacksTypeId {get {if(refineryRacksTypeId==null)refineryRacksTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Refinery Racks').getRecordTypeId(); return refineryRacksTypeId;}set;}
   public static Id tankTypeId {get {if(tankTypeId==null)tankTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Tank').getRecordTypeId(); return tankTypeId;}set;}  
   public static Id geojsonTypeId {get {if(geojsonTypeId==null)geojsonTypeId = Schema.SObjectType.Asset__c.getRecordTypeInfosByName().get('Geojson').getRecordTypeId(); return geojsonTypeId;}set;}  


      static testMethod void constructorTest1() {

		  System.assertNotEquals(HCVC_MapController.crudeRecordTypeId, null);
		  System.assertNotEquals(HCVC_MapController.overviewRecordTypeId, null);
		  System.assertNotEquals(HCVC_MapController.refineryTypeId, null);
		  System.assertNotEquals(HCVC_MapController.refineryUnitTypeId, null);
		  System.assertNotEquals(HCVC_MapController.refineryRacksTypeId, null);
		  System.assertNotEquals(HCVC_MapController.tankTypeId, null);
		  System.assertNotEquals(HCVC_MapController.geojsonTypeId, null);
		  
          PageReference pageRef = Page.HCVC_MapPage;
          Test.setCurrentPage(pageRef);

          Asset__c parentrefinery=new Asset__c(Name='Prince George Refinery',Lat__c=53.928333,Long__c=-122.696390,
                                               RecordTypeId=refineryTypeId,Details__c='Prince George Refinery',Refinery_History__c='history',
                                               Refinery_Overview__c='overview',Retail_Overview__c='retail overview');
          insert parentrefinery;
		   Id parentId = parentrefinery.Id;
		    
		    // Make sure it was inserted properly
		    parentrefinery = [SELECT Name, Details__c FROM Asset__c WHERE Id=: parentId];
		    System.assertEquals(parentrefinery.Name, 'Prince George Refinery');
		    
		   // Set up tank coordinates map custom setting
		   TankCoordinatesMap__c tcm = new TankCoordinatesMap__c();
		   tcm.name = parentId;
		   tcm.csname__c = 'PGtankCoordinates';
		   insert tcm;
		   
		   // Set up some dummy coordinate values for tank
		   PGtankCoordinates__c tankCoords = new PGtankCoordinates__c();
		   tankCoords.name = 'co1';
		   tankCoords.long__c = 53.930400;
		   tankCoords.lat__c = -122.700800;
		   insert tankCoords;
		    
		  Asset__c geoJsonObj2 = new Asset__c();
          geoJsonObj2.GeoJson_Name__c = 'www.testurl.com';
          geoJsonObj2.Name = 'Test Obj2';
          geoJsonObj2.RecordTypeId = HCVC_Utility.geojsonTypeId;
          insert geoJsonObj2;
          
          // Create a test pipeline object
          Asset__c pipelineObj = new Asset__c();
          pipelineObj.GeoJson_Name__c = 'www.testurl.com';
          pipelineObj.Name = 'Test Pipeline Obj';
          pipelineObj.RecordTypeId = HCVC_Utility.crudeRecordTypeId;
          insert pipelineObj;

          ApexPages.currentPage().getParameters().put('id',parentrefinery.Id);
          ApexPages.currentPage().getParameters().put('isRefinery','true');
          ApexPages.currentPage().getParameters().put('isPipeline','true');
          ApexPages.currentPage().getParameters().put('isRetail','true');


          Asset__c assetunit=new Asset__c(Name='Crude Unit (CRUD - UNIT #:10)',Parent_Refinery_Asset__c=parentrefinery.Id,Substance__c='',
                                         Tank_Number__c='1',order__c=1,Lat__c=53.927528,Long__c=-122.698284,RecordTypeId=refineryUnitTypeId,
                                         Details__c='Crude Unit (CRUD – UNIT #: 10)',Address__c='');

              insert assetunit;
              // Make sure it was inserted properly
                assetunit = [SELECT Name, Details__c FROM Asset__c WHERE Details__c='Crude Unit (CRUD – UNIT #: 10)'];
                System.assertEquals(assetunit.Name, 'Crude Unit (CRUD - UNIT #:10)');

          Account acc=new Account(Name='Mldesk');
          insert acc;

          Retail_Location__c  rl=new Retail_Location__c(Location_Name__c='', Latitude__c=52.102064, Longitude__c=-121.925776,
                                                         Address__c='', Account__c=acc.Id);
                      insert rl;

        List<Asset__c> assetunits=new List<Asset__c>();

		HCVC_Customsettings__c customSettings = new HCVC_Customsettings__c();
		customSettings.HCVC_tabId__c = '01rJ00000000cXK';
		insert customSettings;
		
        HCVC_Customsettings__c hcvccs=[SELECT Id,HCVC_tabId__c FROM HCVC_Customsettings__c LIMIT 1];

        HCVC_MapController mc=new HCVC_MapController();


        HCVC_MapController.loadPGTankCoordinates();
        mc.loadChildAssets(assetunits);

        HCVC_MapController.loadAssets(parentrefinery.Id);
        mc.getCoordinatesForProcessFlow();
        HCVC_MapController.loadRefineryAssets();
        HCVC_MapController.loadRetailSites();
        mc.loadDefaultMap();
        mc.assetidHistory=parentrefinery.Id;
        mc.setRefineryHistoryOverview();

        Asset__c tophoAsset=new Asset__c(Name='Tloverview test',Overview__c='Top level overview test',Tank_Number__c='1',order__c=1,isOverview__c=true);
          insert tophoAsset;

          mc.assetthoid=tophoAsset.Id;
          mc.setTopHistoryOverview();
          mc.setTopHistoryOverviewFlag();
          mc.assettype=refineryTypeId;
          mc.loadPlacemarker();

   }




        static testMethod void constructorTest2() {

            PageReference pageRef = Page.HCVC_MapPage;
            Test.setCurrentPage(pageRef);

            Asset__c parentrefinery=new Asset__c(Name='Prince George Refinery',Lat__c=53.928333,Long__c=-122.696390,
                                                 RecordTypeId=refineryTypeId,Details__c='Prince George Refinery',
                                                Refinery_Overview__c='',Refinery_History__c='');
            insert parentrefinery;
		   Id parentId = parentrefinery.Id;
		   

		   
		             
          // Create a test geojson object
          Asset__c geoJsonObj = new Asset__c();
          geoJsonObj.GeoJson_Name__c = 'www.testurl.com';
          geoJsonObj.Name = 'Test Obj';
          geoJsonObj.AssetType__c = 'Railway';
          geoJsonObj.RecordTypeId = HCVC_Utility.geojsonTypeId;
          insert geoJsonObj;
          
		    
		    // Make sure it was inserted properly
		    parentrefinery = [SELECT Name, Details__c FROM Asset__c WHERE Id=: parentId];
		    System.assertEquals(parentrefinery.Name, 'Prince George Refinery');

			ApexPages.currentPage().getParameters().put('id',null);
            ApexPages.currentPage().getParameters().put('isRefinery','true');
            ApexPages.currentPage().getParameters().put('isPipeline','true');
            ApexPages.currentPage().getParameters().put('isRetail','true');

            Asset__c assetunit=new Asset__c(Name='Crude Unit (CRUD - UNIT #:10)',Parent_Refinery_Asset__c=parentrefinery.Id,
                                            Substance__c='',Tank_Number__c='1',order__c=1,Lat__c=53.927528,Long__c=-122.698284,
                                            RecordTypeId=refineryUnitTypeId,Details__c='Crude Unit (CRUD – UNIT #: 10)',Address__c='');

                insert assetunit;
                 // Make sure it was inserted properly
                assetunit = [SELECT Name, Details__c FROM Asset__c WHERE Details__c='Crude Unit (CRUD – UNIT #: 10)'];
                System.assertEquals(assetunit.Name, 'Crude Unit (CRUD - UNIT #:10)');

          Account acc=new Account(Name='Mldesk');
          insert acc;

          Retail_Location__c  rl=new Retail_Location__c(Location_Name__c='', Latitude__c=52.102064, Longitude__c=-121.925776,
                      Address__c='', Account__c=acc.Id);
                      insert rl;

                  HCVC_MapController mc;
                  User thisUser = [ Select Id from User where Id = :UserInfo.getUserId() ];

         System.runAs( thisUser ){
          HCVC_Customsettings__c hcvccs=HCVC_Customsettings__c.getInstance();

          if(hcvccs==null){

          hcvccs=new HCVC_Customsettings__c(HCVC_tabId__c='');
            insert hcvccs;

         }

        mc=new HCVC_MapController();

     }
        mc.assettype=refineryUnitTypeId;
        mc.assetid=assetunit.Id;

        mc.loadPlacemarker();
        HCVC_MapController.latlongwrapper ml=new HCVC_MapController.latlongwrapper();


      }


}