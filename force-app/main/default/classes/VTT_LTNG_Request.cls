/**
 * Created by MarcelBrimus on 02/10/2019.
 */

public class VTT_LTNG_Request {
    @AuraEnabled
    public String actionName;

    @AuraEnabled
    public Decimal currentLatitude;

    @AuraEnabled
    public Decimal currentLongitude;

    @AuraEnabled
    public String actionComment;

    @AuraEnabled
    public String onHoldReason;

    // JOBCOMPLETE STUFF START
    @AuraEnabled
    public List<VTT_LTNG_AvailableActivity> activitiesToComplete;

    @AuraEnabled
    public String partItem;

    @AuraEnabled
    public String damageItem;

    @AuraEnabled
    public String damageDescription;

    @AuraEnabled
    public String causeItem;

    @AuraEnabled
    public String causeDescription;

    @AuraEnabled
    public Boolean isOtherTradesmanStillWorking;

    @AuraEnabled
    public Boolean requestReschedule;

    @AuraEnabled
    public Integer runningTally;
    // JOBCOMPLETE STUFF END

}