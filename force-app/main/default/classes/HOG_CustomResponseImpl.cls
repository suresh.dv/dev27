public with sharing class HOG_CustomResponseImpl extends HOG_CustomResponse{
	
	public HOG_CustomResponseImpl(Object result){
		success = true;
		if(resultObjects == null) resultObjects = new List<Object>();
		resultObjects.add(result);
	}

	public HOG_CustomResponseImpl(List<Object> results){
		success = true;
		if(resultObjects == null) resultObjects = new List<Object>();
		resultObjects.addAll(results);
	}

	public HOG_CustomResponseImpl() {
		success = true;
	}

	public HOG_CustomResponseImpl(String errorMsg) {
		success = false;
		addError(errorMsg);
	}

	public HOG_CustomResponseImpl(Exception ex) {
		success = false;
		addError(ex);
	}

	public void addResult(Object result) {
		if(resultObjects == null) resultObjects = new List<Object>();
		resultObjects.add(result);
	}

	public List<String> addError(Exception ex) {
		addError(ex.getMessage());
		return errors;
	}

	public List<String> addError(String errorMsg) {
		if(errors == null) errors = new List<String>();
		errors.add(errorMsg);
		success = false;
		return errors;
	}
}