/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WorkOrderActivitySelector
History:        jschn 2019-07-09 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WorkOrderActivitySelectorTest {

    @IsTest
    static void getRecord_withoutParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Work_Order_Activity__c> records = new VTT_WorkOrderActivitySelector().getRecord(null);
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Even with bad parameter, SOQL query should return empty list.');
        System.assertEquals(expectedRecordCount, records.size(),
                'There shoulnd\'t be any record with ID of null. Returned records: ' + records.size());
    }

    @IsTest
    static void getRecord_withWrongParam() {
        Integer expectedRecordCount = 0;
        EPD_TestData.createEPD();

        Test.startTest();
        List<Work_Order_Activity__c> records = new VTT_WorkOrderActivitySelector().getRecord(UserInfo.getUserId());
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Even with bad parameter, SOQL query should return empty list.');
        System.assertEquals(expectedRecordCount, records.size(),
                'There shoulnd\'t be any record with ID of UserInfo ID. Returned records: ' + records.size());
    }

    @IsTest
    static void getRecord_withProperParam() {
        Integer expectedRecordCount = 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        List<Work_Order_Activity__c> records = new VTT_WorkOrderActivitySelector().getRecord(epd.Work_Order_Activity__c);
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Expected List with single record. Result: null.');
        System.assertEquals(expectedRecordCount, records.size(),
                'We were expecting 1 record. Returned: ' + records.size());
    }

    @IsTest
    static void getRecordsForAssignments_withoutParam() {
        List<Work_Order_Activity__c> records;
        Boolean expectedFailFlag = true;
        Boolean failFlag = false;

        Test.startTest();
        try {
            records = new VTT_WorkOrderActivitySelector().getRecordsForAssignments(null);
        } catch (Exception ex) {
            failFlag = true;
        }
        Test.stopTest();

        System.assertEquals(null, records,
                'With bad parameter, SOQL query should fail.');
        System.assertEquals(expectedFailFlag, failFlag,
                'As query failed, it should fall into catch clause.');
    }

    @IsTest
    static void getRecordsForAssignments_withEmptyParam() {
        Integer expectedRecordCount = 0;

        Test.startTest();
        List<Work_Order_Activity__c> records = new VTT_WorkOrderActivitySelector().getRecordsForAssignments(new List<Id>());
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Even with bad parameter, SOQL query should return empty list.');
        System.assertEquals(expectedRecordCount, records.size(),
                'There shoulnd\'t be any record with ID of null. Returned records: ' + records.size());
    }

    @IsTest
    static void getRecordsForAssignments_withWrongParam() {
        Integer expectedRecordCount = 0;
        EPD_TestData.createEPD();

        Test.startTest();
        List<Work_Order_Activity__c> records = new VTT_WorkOrderActivitySelector().getRecordsForAssignments(new List<Id> {UserInfo.getUserId()});
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Even with bad parameter, SOQL query should return empty list.');
        System.assertEquals(expectedRecordCount, records.size(),
                'There shoulnd\'t be any record with ID of UserInfo ID. Returned records: ' + records.size());
    }

    @IsTest
    static void getRecordsForAssignments_withProperParam() {
        Integer expectedRecordCount = 1;
        EPD_Engine_Performance_Data__c epd = EPD_TestData.createEPD();

        Test.startTest();
        List<Work_Order_Activity__c> records = new VTT_WorkOrderActivitySelector().getRecordsForAssignments(new List<Id> {epd.Work_Order_Activity__c});
        Test.stopTest();

        System.assertNotEquals(null, records,
                'Expected List with single record. Result: null.');
        System.assertEquals(expectedRecordCount, records.size(),
                'We were expecting 1 record. Returned: ' + records.size());
    }

}