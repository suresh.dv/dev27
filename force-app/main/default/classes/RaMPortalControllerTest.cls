@isTest
public with sharing class RaMPortalControllerTest {

  @testSetUp 
  static void setup() {
      String accountRecordType = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
      String contactRecordType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RaM Vendor').getRecordTypeId();
      String caseRecordType = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RaM Case').getRecordTypeId();

      Profile headOfficeProfile = [SELECT Id FROM Profile WHERE Name = 'Category Manager User'];
      Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'RaM Lightning Vendor Community'];
      UserRole role = [SELECT Id, Name FROM UserRole WHERE Name = 'Retail Fuels & Convenience - Staff'];

      User headOffice = TestUtilities.createTestUser(headOfficeProfile);
      headOffice.Email = 'head_office_unit_test@huskyenergy.huskyram.com';
      headOffice.UserName = 'head_office_unit_test@huskyenergy.huskyram.com';
      headOffice.UserRoleId = role.Id;

      insert headOffice;       

      System.runAs(headOffice) {
          Account vendor1 = new Account(Name = 'RaM Vendor Account 1');
          vendor1.RecordTypeId = accountRecordType;
  
          Account vendor2 = new Account(Name = 'RaM Vendor Account 2');
          vendor2.RecordTypeId = accountRecordType;
  
          insert vendor1;
          insert vendor2;

          Contact vendor1Contact1 = new Contact(FirstName = 'V1', LastName = 'Vendor 1 Dispatcher');
          vendor1Contact1.AccountId = vendor1.id;
          vendor1Contact1.Email = 'vendor1_dispatcher@vendor1.com';
          vendor1Contact1.RecordTypeId = contactRecordType;   
          
          Contact vendor1Contact2 = new Contact(FirstName = 'V1', LastName = 'Vendor 1 Technicain');
          vendor1Contact2.AccountId = vendor1.id;
          vendor1Contact2.Email = 'vendor1_technicain@vendor1.com';
          vendor1Contact2.RecordTypeId = contactRecordType;              
  
          Contact vendor2Contact1 = new Contact(FirstName = 'V2', LastName = 'Vendor 2 Dispatcher');
          vendor2Contact1.AccountId = vendor2.id;
          vendor2Contact1.Email = 'vendor2_dispatcher@vendor2.com';
          vendor2Contact1.RecordTypeId = contactRecordType;  
          
          Contact vendor2Contact2 = new Contact(FirstName = 'V2', LastName = 'Vendor 2 Technicain');
          vendor2Contact2.AccountId = vendor2.id;
          vendor2Contact2.Email = 'vendor2_technicain@vendor2.com';
          vendor2Contact2.RecordTypeId = contactRecordType;             
  
          insert vendor1Contact1;
          insert vendor1Contact2;
          insert vendor2Contact1;
          insert vendor2Contact2;
          
          RaMPortalUtility.createPortalUser(vendor1Contact1, 'RaM Dispatcher');
          RaMPortalUtility.createPortalUser(vendor1Contact2, 'RaM Technician');
          RaMPortalUtility.createPortalUser(vendor2Contact1, 'RaM Dispatcher');
          RaMPortalUtility.createPortalUser(vendor2Contact2, 'RaM Technician');

          RaM_Location__c locationRecord = new RaM_Location__c(Name = '1234', Service_Area__c = 'Island', SAPFuncLOC__c = 'C1-04-002-00042', 
                      Location_Classification__c = 'Urban', R_M_Location__c = true, LocationName__c = 'Unit Test Location', Retailer__c = 'Unit Test Retailer',
                      Physical_Address_1__c = 'Unit Test Address 1', Physical_Address_2__c = 'Unit Test Address 2', City__c = 'Calgary', Province__c = 'AB', 
                      Postal_Code__c = 'T2W8T6', Phone__c = '4031236655', District_Manager__c = 'Test Manager', Maintenance_Tech_User__c = null);

          insert locationRecord; 
          
          RaM_Equipment__c ramEquipment = new RaM_Equipment__c(Name = 'RP0042SSDVR0001-16 Channel DVR--');
          ramEquipment.FunctionalLocation__c = 'C1-04-002-00042';
          ramEquipment.Equipment_Type__c = 'ELECTRICAL';
          ramEquipment.Equipment_Class__c = 'ELECTRICAL PANEL';
          ramEquipment.Status__c = 'Active';
          
          insert ramEquipment;

          RaM_VendorAssignment__c vendorAssignment = new RaM_VendorAssignment__c();
          vendorAssignment.Account__c = vendor1.id;
          vendorAssignment.Equipment_Type_VA__c = 'ELECTRICAL';
          vendorAssignment.Equipment_Class_VA__c = 'ELECTRICAL PANEL';
          vendorAssignment.Priority__c = 1;
          vendorAssignment.ServiceArea__c = 'Island';
          vendorAssignment.Location_Classification_VA__c = 'Urban';
          
          insert vendorAssignment;

          RaM_Setting__c cusSetting = new RaM_Setting__c();
          cusSetting.name = 'Case Next Counter';
          cusSetting.R_M_Ticket_Number_Next_Sequence__c = 123456;
          insert cusSetting;             

          Case kase1 = new Case(RecordTypeId = caseRecordType,
                                  Status = 'New',
                                  Priority = 'P2-Emergency',
                                  Origin = 'Maintenance Tech',
                                  LocationNumber__c = locationRecord.id,
                                  EquipmentName__c = ramEquipment.id,                                 
                                  Equipment_Type__c='ELECTRICAL',
                                  Equipment_Class__c='ELECTRICAL PANEL',
                                  Location_Contact__c = 'Retailer Contact', 
                                  Location_Classification__c = 'Urban',
                                  Service_Area__c = 'Island' , 
                                  subject = 'Unit Test RaM Case', 
                                  description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat purus odio. Donec lacus mi, condimentum a diam eget, posuere mollis purus. Donec ac velit.' ); 

          insert kase1;   

          kase1.Status = 'Assigned';
          kase1.Vendor_Assignment__c = vendorAssignment.id;
          update kase1;

          List<User> users = [SELECT Id FROM User WHERE ContactId = :vendor1Contact2.Id];
          List<Case> kases = [SELECT Id, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordTypeId = :caseRecordType];
          
          Test.startTest();
          RaMPortalUtility.createAndAssignCase(kases[0], users);
          Test.stopTest();
      }
  }
  
  @isTest
  private static void test_getRaMOpenCases() {
    Test.startTest();
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];

    System.runAs(users[0]) {
      List<Case> cases = RaMPortalController.getRaMOpenCases();
      System.assertEquals(1, cases.size());
    }
    Test.stopTest();
  }

  @isTest
  private static void test_getRaMCaseByCaseNumber() {
    Test.startTest();  
    Case kase1 = [SELECT Id, CaseNumber FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];

    Case resultCase = RaMPortalController.getRaMCaseByCaseNumber(kase1.CaseNumber);
    System.assertEquals(kase1.CaseNumber, resultCase.CaseNumber);
    Test.stopTest();    
  }

  @isTest
  private static void test_getPortalUsers() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];

    System.runAs(users[0]) {
      List<RaMPortalManagementService.PortalUserWrapper> portalUsers = RaMPortalController.getPortalUsers();
      System.assertEquals(1, portalUsers.size());
    }
    Test.stopTest();    
  }

  @isTest
  private static void test_getAssignmentUsersByCase() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];

    Case kase1 = [SELECT Id, CaseNumber FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];

    System.runAs(users[0]) {
      List<RaMPortalManagementService.CurrentAssignment> assignments = RaMPortalController.getAssignmentUsersByCase(kase1.Id);
      System.assertEquals(2, assignments.size());
    }
    Test.stopTest();    
  }

  @isTest
  private static void test_updateContact() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<Contact> contacts = [SELECT Id, FirstName, LastName, Email FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher'];
    RaMPortalController.updateContact(contacts[0].Id, contacts[0].FirstName, contacts[0].LastName, contacts[0].Email, '4033214455', '', '', '', '', '', '', '', '', '', '');

    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];
    System.runAs(users[0]) {
      List<Contact> updatedContacts = [SELECT Id, FirstName, LastName, Email, MobilePhone FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher'];
      System.assertEquals('4033214455', updatedContacts[0].MobilePhone);
    }
    Test.stopTest();    
  }

  @isTest
  private static void test_createContact() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    RaMPortalController.createContact(acct.Id, 'Unit test 1', 'Unit test 2', 'Unit_test@unittest.com', '',  '5874445566', '', '', '', '', '', '', '', '', '');

    List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :acct.Id];
    System.assertEquals(3, contacts.size());
    Test.stopTest();
  }

  @isTest
  private static void test_deactivateUser() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];
    
    System.runAs(users[0]) {
      RaMPortalController.deactivateUser(users[0].Id);
    }

    users = [SELECT Id, IsActive FROM User WHERE Id = :users[0].Id];
    // System.assertEquals(false, users[0].IsActive);
    Test.stopTest();    
  }

  @isTest
  private static void test_activateOrCreateUser() {
    Test.startTest();
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];

    Contact cont = new Contact(AccountId = acct.Id , FirstName = 'FirstName', LastName = 'LastName', Email = 'unit_test_contact@unittest.huskyram.com');
    insert cont;

    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];
    System.runAs(users[0]) {      
      RaMPortalController.activateOrCreateUser(cont.Id, 'false', '', false, 'RaM Technician');        
    }
     
    User usr = [SELECT Id, FirstName FROM User WHERE ContactId = :cont.Id];
    System.assertEquals(usr.FirstName, cont.FirstName);
    Test.stopTest();
  }

  @isTest
  private static void test_assignCaseToTechnician() {
    Test.startTest();
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    Case kase1 = [SELECT Id, CaseNumber, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];
    List<User> technicianUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Technicain')];
    List<User> dispatcherUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];

    List<String> technicianIds = new List<String>();
    for(User technicianUser : technicianUsers) {
      technicianIds.add(technicianUser.Id);
    }

    System.runAs(dispatcherUsers[0]) {
      RaMPortalController.assignCaseToTechnician(kase1.Id, technicianIds, kase1.Subject, kase1.Ticket_Number_R_M__c);
    }

    //There is already an assignment record for  this user and it should not create a new one
    List<RaM_Case_Assignment__c> assignments = [SELECT Id FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase1.Id];
    System.assertEquals(1, assignments.size());
    Test.stopTest();
  }

  @isTest
  private static void test_getAssignmentsByCase() {
    Test.startTest();  
    Case kase1 = [SELECT Id, CaseNumber, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];
    List<RaMPortalManagementService.CaseAssignment> assignments = RaMPortalController.getAssignmentsByCase(kase1.CaseNumber);

    System.assertEquals(1, assignments.size());
    Test.stopTest();    
  }

  @isTest
  private static void test_getAssignmentsByContact() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Technicain'];

    List<RaMPortalManagementService.CaseAssignment> assignments = RaMPortalController.getAssignmentsByContact(contacts[0].Id);
    System.assertEquals(1, assignments.size());
    Test.stopTest();    
  }

  @isTest
  private static void test_getOpenActivityListByTechnician() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> technicianUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Technicain')];

    List<RaMPortalManagementService.OpenActivity> activities = RaMPortalController.getOpenActivityListByTechnician(technicianUsers[0].Id);
    System.assertEquals(1, activities.size());
    Test.stopTest();    
  }

  @isTest
  private static void test_getActivityDetails() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> technicianUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Technicain')];
    
    Case kase1 = [SELECT Id, CaseNumber, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];
    List<RaM_Case_Assignment__c> assignments = [SELECT Id, Subject__c, Ticket_Number__c  FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase1.Id];

    RaMPortalManagementService.OpenActivity activity = RaMPortalController.getActivityDetails(assignments[0].Id, technicianUsers[0].Id);
    System.assertEquals(activity.tkt.ticketNumber, kase1.Ticket_Number_R_M__c);
    System.assertEquals(activity.tkt.subject, kase1.Subject);
    Test.stopTest();    
  }

  @isTest
  private static void test_updateActivityStatus() {
    Test.startTest();  
    Case kase1 = [SELECT Id, CaseNumber, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];
    List<RaM_Case_Assignment__c> assignments = [SELECT Id, Subject__c, Ticket_Number__c  FROM RaM_Case_Assignment__c WHERE Parent_Case__c = :kase1.Id];
    
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> technicianUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Technicain')];
    
    System.runAs(technicianUsers[0]) {
      RaMPortalController.updateActivityStatus(assignments[0].Id, '51.048615', '-114.070847', 'Work start comments', 'Assigned', 'Pending Parts', '', kase1.Id);
    }

    List<RaM_Case_Assignment_Activity__c> activities = [SELECT Id FROM RaM_Case_Assignment_Activity__c WHERE RaM_Case_Assignment__c = :assignments[0].Id];
    System.assertEquals(1, activities.size());
    Test.stopTest();    
  }

  @isTest
  private static void test_updatePrimaryTechnician() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    Case kase1 = [SELECT Id, CaseNumber, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];

    Contact cont = new Contact(AccountId = acct.Id , FirstName = 'FirstName', LastName = 'LastName', Email = 'unit_test_contact@unittest.huskyram.com');
    insert cont;

    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];
    System.runAs(users[0]) {
      RaMPortalController.activateOrCreateUser(cont.Id, 'false', '', false, 'RaM Technician');   
    }

    List<User> newUsers = [SELECT Id FROM User WHERE ContactId = :cont.Id];
    List<String> technicianIds = new List<String>();
    for(User newUser : newUsers) {
      technicianIds.add(newUser.Id);
    } 
    
    System.runAs(users[0]) {
      RaMPortalController.assignCaseToTechnician(kase1.Id, technicianIds, kase1.Subject, kase1.Ticket_Number_R_M__c);
      RaMPortalController.updatePrimaryTechnician(kase1.Id, newUsers[0].Id);
    }

    List<RaM_Case_Assignment__c> assignments = [SELECT Id, Is_Primary_Assignment__c 
                                                  FROM RaM_Case_Assignment__c 
                                                  WHERE Parent_Case__c = :kase1.Id
                                                    AND Assign_To__c = :newUsers[0].Id];
    // System.assertEquals(true, assignments[0].Is_Primary_Assignment__c);
    Test.stopTest();    
  }

  @isTest
  private static void test_removeTechnicianAssignment() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];
    List<User> technicianUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Technicain')];
    List<User> dispatcherUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];
    Case kase1 = [SELECT Id, CaseNumber, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];

    List<RaM_Case_Assignment__c> assignments = [SELECT Id, Is_Primary_Assignment__c 
                                                  FROM RaM_Case_Assignment__c 
                                                  WHERE Parent_Case__c = :kase1.Id
                                                    AND Assign_To__c = :technicianUsers[0].Id];

      System.runAs(dispatcherUsers[0]) {
        RaMPortalController.removeTechnicianAssignment(assignments[0].Id);
      }

      List<RaM_Case_Assignment__c> newAssignments = new List<RaM_Case_Assignment__c>();
      newAssignments = [SELECT Id, Is_Primary_Assignment__c 
                    FROM RaM_Case_Assignment__c 
                    WHERE Parent_Case__c = :kase1.Id
                      AND Assign_To__c = :dispatcherUsers[0].Id];   
                      
      System.assertEquals(0, newAssignments.size());
    Test.stopTest();      
  }

  @isTest
  private static void test_reassignTechnician() {
    Test.startTest();  
    Account acct = [SELECT Id, Name FROM Account WHERE Name = 'RaM Vendor Account 1'];

    Contact cont = new Contact(AccountId = acct.Id , FirstName = 'FirstName', LastName = 'LastName', Email = 'unit_test_contact@unittest.huskyram.com');
    insert cont;

    List<User> users = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND LastName = 'Vendor 1 Dispatcher')];
    System.runAs(users[0]) {
      RaMPortalController.activateOrCreateUser(cont.Id, 'false', '', false, 'RaM Technician');   
    }

    List<User> technicianUsers = [SELECT Id FROM User WHERE ContactId IN (SELECT Id FROM Contact WHERE AccountId = :acct.Id AND Email = 'unit_test_contact@unittest.huskyram.com')];
    Case kase1 = [SELECT Id, CaseNumber, Subject, Ticket_Number_R_M__c FROM Case WHERE RecordType.Name IN ('RaM Case', 'RaM Child Case')];

    List<RaM_Case_Assignment__c> assignments = [SELECT Id, Is_Primary_Assignment__c 
                                                  FROM RaM_Case_Assignment__c 
                                                  WHERE Parent_Case__c = :kase1.Id];

      System.runAs(users[0]) {
        RaMPortalController.reassignTechnician(assignments[0].Id, technicianUsers[0].Id);
      }
      
      assignments = [SELECT Id, Is_Primary_Assignment__c, Assign_To__c 
                    FROM RaM_Case_Assignment__c 
                    WHERE Parent_Case__c = :kase1.Id];
                    
      System.assertEquals(technicianUsers[0].Id, assignments[0].Assign_To__c);
    Test.stopTest();      
  }


}