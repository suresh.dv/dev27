/**
 *  Author:         MarosGrajcar
 *  Company:        Husky Energy
 *  Description:    
 *  History:        Created on 2/11/2020   
 */

public class Equipment_TransferForm_FLOC_Info {
	@AuraEnabled
	public SObject toFloc;
	@AuraEnabled
	public String toFlocId;
	@AuraEnabled
	public String toFlocName;
	@AuraEnabled
	public String toRouteId;
	@AuraEnabled
	public String toRouteNumber;
	@AuraEnabled
	public String toPlannerGroup;
	@AuraEnabled
	public String toAPIName;

	@AuraEnabled
	public Boolean isFromForm = false;

	@AuraEnabled
	public SObject fromFloc;
	@AuraEnabled
	public String fromFlocId;
	@AuraEnabled
	public String fromFlocName;
	@AuraEnabled
	public String fromRouteId;
	@AuraEnabled
	public String fromRouteNumber;
	@AuraEnabled
	public String fromPlannerGroup;
	@AuraEnabled
	public String fromAPIName;
}