/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for HOG_PicklistItem_Simple
History:        jschn 25/10/2019 - Created.
*************************************************************************************************/
@IsTest(IsParallel = true)
private class HOG_PicklistItem_SimpleTest {

    @IsTest
    static void testBehavior() {
        String label = 'label';
        String value = 'value';

        HOG_PicklistItem_Simple picklistItem = new HOG_PicklistItem_Simple(value, label);

        System.assertEquals(label, picklistItem.label, 'Labels should be the same');
        System.assertEquals(value, picklistItem.value, 'Values should be the same');
    }

}