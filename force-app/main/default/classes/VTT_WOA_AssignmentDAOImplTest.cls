/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Unit Test for VTT_WOA_AssignmentDAOImpl class
History:        jschn 31/10/2019 - Created.
*************************************************************************************************/
@IsTest
private class VTT_WOA_AssignmentDAOImplTest {

    @IsTest
    static void getAssignments_null() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments_empty() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param = new Set<Id>();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments_wrong() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param = new Set<Id>{UserInfo.getUserId()};
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments_oneRecord() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param = new Set<Id>();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            param.add(activity.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments2Param_null() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        List<Work_Order_Activity__c> param1;
        List<Id> param2;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param1, param2);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getAssignments2Param_empty() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        List<Work_Order_Activity__c> param1 = new List<Work_Order_Activity__c>();
        List<Id> param2 = new List<Id>();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param1, param2);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments2Param_oneRecord() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        List<Work_Order_Activity__c> param1 = new List<Work_Order_Activity__c>();
        List<Id> param2 = new List<Id>();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            param1.add(activity);
            param2.add(tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param1, param2);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments3Param_null() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param1;
        List<Work_Order_Activity__c> param2;
        List<Id> param3;
        Boolean failFlag = false;
        Boolean expectedFailFlag = true;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param1, param2, param3);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertEquals(null, result);
    }

    @IsTest
    static void getAssignments3Param_empty() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param1 = new Set<Id>();
        List<Work_Order_Activity__c> param2 = new List<Work_Order_Activity__c>();
        List<Id> param3 = new List<Id>();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param1, param2, param3);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments3Param_oneRecord() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param1 = new Set<Id>();
        List<Work_Order_Activity__c> param2 = new List<Work_Order_Activity__c>();
        List<Id> param3 = new List<Id>();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            param2.add(activity);
            param3.add(tradesman.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param1, param2, param3);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getAssignments3Param_oneRecord2() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Set<Id> param1 = new Set<Id>();
        List<Work_Order_Activity__c> param2 = new List<Work_Order_Activity__c>();
        List<Id> param3 = new List<Id>();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            param1.add(activity.Id);

            Test.startTest();
            try {
                result = selector.getAssignments(param1, param2, param3);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }



    @IsTest
    static void getNonRejectedAssignmentsByTradesman_null() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        String param;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByTradesman(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByTradesman_empty() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        String param = '';
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByTradesman(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByTradesman_wrong() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        String param = 'wrong';
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByTradesman(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByTradesman_rejected() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        String param;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            Work_Order_Activity_Assignment__c assignment = VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            assignment.Rejected__c = true;
            assignment.Reject_Reason__c = 'something';
            update assignment;
            param = tradesman.Id;

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByTradesman(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByTradesman_oneRecord() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        String param;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            param = tradesman.Id;

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByTradesman(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByActivity_null() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Id param;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByActivity(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByActivity_wrong() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Id param = UserInfo.getUserId();
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByActivity(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByActivity_rejected() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Id param;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 0;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            Work_Order_Activity_Assignment__c assignment = VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            assignment.Rejected__c = true;
            assignment.Reject_Reason__c = 'something';
            update assignment;
            param = activity.Id;

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByActivity(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

    @IsTest
    static void getNonRejectedAssignmentsByActivity_oneRecord() {
        VTT_WOA_AssignmentDAO selector = new VTT_WOA_AssignmentDAOImpl();
        List<Work_Order_Activity_Assignment__c> result;
        Id param;
        Boolean failFlag = false;
        Boolean expectedFailFlag = false;
        Integer expectedCount = 1;

        System.runAs(VTT_TestData.createVTTUser()) { //needs to be run as this user. Otherwise mixed DML exception.
            MaintenanceServicingUtilities.executeTriggerCode = false;
            VTT_TestData.SetupRelatedTestData(true);
            Account vendor = VTT_TestData.createVendorAccount('Avengers');
            Contact tradesman = VTT_TestData.createTradesmanContact('Tony', 'Stark', vendor.Id, UserInfo.getUserId());
            HOG_Service_Request_Notification_Form__c serviceRequest = VTT_TestData.createServiceRequest(VTT_TestData.workOrderType);
            HOG_Maintenance_Servicing_Form__c workOrder = VTT_TestData.createWorkOrder(serviceRequest.Id, VTT_TestData.notificationType.Id);
            Work_Order_Activity__c activity = VTT_TestData.createWorkOrderActivity(workOrder.Id);
            VTT_TestData.createWorkOrderActivityAssignment(activity.Id, tradesman.Id);
            param = activity.Id;

            Test.startTest();
            try {
                result = selector.getNonRejectedAssignmentsByActivity(param);
            } catch (Exception ex) {
                failFlag = true;
            }
            Test.stopTest();
        }

        System.assertEquals(expectedFailFlag, failFlag);
        System.assertNotEquals(null, result);
        System.assertEquals(expectedCount, result.size());
    }

}