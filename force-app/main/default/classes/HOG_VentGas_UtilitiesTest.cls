/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VentGas_Utilities.cls
				Some part of coverage is done also with HOG_VentGasAlertTriggerTest.cls
History:        jschn 04.26.2018 - Created.
**************************************************************************************************/
@isTest
private class HOG_VentGas_UtilitiesTest {
	
	@isTest static void testTaskStuff() {
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_RETURN_URL, 'URL_PARAM_RETURN_URL');
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_SAVE_URL, 'URL_PARAM_SAVE_URL');
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_TASK, 'URL_PARAM_TASK');
		ApexPages.currentPage().getParameters().put(HOG_VentGas_Utilities.URL_PARAM_ALERT, 'URL_PARAM_ALERT');
		List<HOG_Vent_Gas_Alert_Task__c> tasks = [SELECT Id, Status__c, Vent_Gas_Alert__c FROM HOG_Vent_Gas_Alert_Task__c];
		Test.startTest();
		//Test getters
		System.assertEquals('URL_PARAM_RETURN_URL', HOG_VentGas_Utilities.getReturnUrlParameter());
		System.assertEquals('URL_PARAM_SAVE_URL', HOG_VentGas_Utilities.getSaveUrlParameter());
		System.assertEquals('URL_PARAM_TASK', HOG_VentGas_Utilities.getVentGasTaskParameter());
		System.assertEquals('URL_PARAM_ALERT', HOG_VentGas_Utilities.getVentGasAlertParameter());
		//Test constants
		System.assertEquals('Not Started', HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED);
		System.assertEquals('In Progress', HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS);
		System.assertEquals('Completed', HOG_VentGas_Utilities.TASK_STATUS_COMPLETED);
		System.assertEquals('Cancelled', HOG_VentGas_Utilities.TASK_STATUS_CANCELLED);
		System.assertEquals('Route Operator', HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
		System.assertEquals('Operations Engineer', HOG_VentGas_Utilities.ASSIGNEE_TYPE_OPERATIONS_ENGINEER);
		//Test retrieve methods
		System.assertEquals(1, HOG_VentGas_Utilities.retrieveTask(tasks.get(0).Id).size());
		System.assertEquals(null, HOG_VentGas_Utilities.retrieveTask(null));
		Set<Id> ids = new Set<Id>();
		for(HOG_Vent_Gas_Alert_Task__c t : tasks) ids.add(t.Vent_Gas_Alert__c);
		System.assertEquals(tasks.size(), HOG_VentGas_Utilities.retrieveTasksByAlertId(ids).size());
		//Test helper methods
		//NOTE no public methods to test.
		Test.stopTest();
	}
	
	@isTest static void testVentGasAlertStuff() {
		List<HOG_Vent_Gas_Alert__c> alerts = [SELECT Id,Status__c FROM HOG_Vent_Gas_Alert__c];
		Test.startTest();
		//Test constants
		System.assertEquals('Not Started', HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED);
		System.assertEquals('In Progress', HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS);		
		System.assertEquals('Completed', HOG_VentGas_Utilities.ALERT_STATUS_COMPLETED);
		System.assertEquals('Acknowledge', HOG_VentGas_Utilities.ALERT_STATUS_ACKNOWLEDGE);
		System.assertEquals('Expired GOR Test', HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);
		System.assertEquals('Predicted High Vent Rate', HOG_VentGas_Utilities.ALERT_TYPE_HIGH_VENT);
		System.assertEquals('Large Low Variance in Predicted Vent Rate vs.Test Vent Rate', HOG_VentGas_Utilities.ALERT_TYPE_LOW_VARIANCE);
		System.assertEquals('Large High Variance in Predicted Vent Rate vs.Test Vent Rate', HOG_VentGas_Utilities.ALERT_TYPE_HIGH_VARIANCE);
		System.assertEquals('Urgent', HOG_VentGas_Utilities.ALERT_PRIORITY_URGENT);
		System.assertEquals('High', HOG_VentGas_Utilities.ALERT_PRIORITY_HIGH);
		System.assertEquals('Medium', HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM);
		System.assertEquals('Low', HOG_VentGas_Utilities.ALERT_PRIORITY_LOW);
		//Test Validations
		HOG_Vent_Gas_Alert__c alert = new HOG_Vent_Gas_Alert__c(Status__c = HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED);
		System.assert(HOG_VentGas_Utilities.isAlertNotStarted(alert));
		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS;
		System.assert(!HOG_VentGas_Utilities.isAlertNotStarted(alert));
		alert.Status__c = null;
		System.assert(!HOG_VentGas_Utilities.isAlertNotStarted(alert));
		//Test Retrieve methods
		System.assertEquals(1, HOG_VentGas_Utilities.retrieveAlert(alerts.get(0).Id).size());
		System.assertEquals(alerts.size(), HOG_VentGas_Utilities.retrieveAlertsMap((new Map<Id, HOG_Vent_Gas_Alert__c>(alerts)).keySet()).size());
		//Test trigger methods
		//NOTE done in separate test (HOG_VentGasAlertTriggerTest)
		//Test helper methods
		//NOTE no public methods to test.
		Test.stopTest();
	}

	@isTest static void testLocationStuff() {
		Location__c loc = [SELECT Id, Name, Route__c FROM Location__c].get(0);
		List<Operator_On_Call__c> oocs = [SELECT Id, Operator__c FROM Operator_On_Call__c WHERE Operator_Route__c =: loc.Route__c];
		Test.startTest();
		//Test retrieve methods
		System.assertEquals(oocs.size(), HOG_VentGas_Utilities.getRouteOperator(loc.Route__c).size());
		//Test Helpers method
		System.assertEquals(oocs.get(0).Operator__c, HOG_VentGas_Utilities.getRouteOperatorUserId(loc.Route__c));
		System.assertEquals(null, HOG_VentGas_Utilities.getRouteOperatorUserId(''));
		Test.stopTest();
	}

	@isTest static void testGeneralMethods() {
		User usr = [SELECT Id FROM User WHERE Alias = 'IronMan'].get(0);
		User admin = [SELECT Id FROM User WHERE Alias = 'eyepatch'].get(0);
		admin = HOG_VentGas_TestData.assignPermissionSet(admin, HOG_VentGas_Utilities.HOG_ADMIN_PS_NAME);

		Test.startTest();
		System.runAs(usr){
			//Test constants
			system.assertEquals('System Administrator', HOG_VentGas_Utilities.ADMIN_PROFILE_NAME);
			system.assertEquals('HOG_Administrator', HOG_VentGas_Utilities.HOG_ADMIN_PS_NAME);
			//Test Helper methods
			System.assert(!ApexPages.hasMessages());
			HOG_VentGas_Utilities.addErrorMsg('error msg');
			System.assert(ApexPages.hasMessages());

			System.assertEquals(null, HOG_VentGas_Utilities.getFirstOrNull(null));
			System.assertEquals(null, HOG_VentGas_Utilities.getFirstOrNull(new List<SObject>()));
			List<HOG_Vent_Gas_Alert__c> l = new List<HOG_Vent_Gas_Alert__c>();
			l.add(new HOG_Vent_Gas_Alert__c(Name='1'));
			l.add(new HOG_Vent_Gas_Alert__c(Name='2'));
			System.assertEquals(l.get(0).Name, ((HOG_Vent_Gas_Alert__c) HOG_VentGas_Utilities.getFirstOrNull(l)).Name);
			//Test Validations
			System.assert(!HOG_VentGas_Utilities.getIsAdmin());
			System.assert(!HOG_VentGas_Utilities.getIsHOGAdmin());
			
		}
		System.runAs(admin) {
			System.assert(HOG_VentGas_Utilities.getIsAdmin());
			System.assert(HOG_VentGas_Utilities.getIsHOGAdmin());
		}
		Test.stopTest();
	}
	
	@testSetup static void createTestData() {
		User admin = HOG_VentGas_TestData.createAdmin();
		Location__c loc = HOG_VentGas_TestData.createLocation();
		User usr = HOG_VentGas_TestData.createUser();
		Operator_On_Call__c operatorOnCall = HOG_VentGas_TestData.createOperatorOnCall(loc.Route__c, usr.Id);
		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
																		'TestDescription', 
																		loc.Id,
																		usr.Id, 
																		HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
																		HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
																		HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		HOG_Vent_Gas_Alert_Task__c t = HOG_VentGas_TestData.createTask(alert.Id, 
																	usr.Id, 
																	null, 
																	'TestTask', 
																	alert.Priority__c, 
																	true, 
																	HOG_VentGas_Utilities.TASK_STATUS_NOT_STARTED, 
																	HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR);
	}

}