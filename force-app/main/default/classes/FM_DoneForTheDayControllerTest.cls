/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FM_DoneForTheDayControllerTest
{
	/////////////////
	//* CONSTANTS *//
	/////////////////

	private static final String FM_PERMISSION_FIELD_OPERATOR = 'HOG_Field_Operator';
	private static final String FM_PERMISSION_DISPATCHER_INTERNAL = 'HOG_FM_Dispatcher';

    // *************************** //
    // ** TEST DONE FOR THE DAY ** //
    // *************************** //

	static testMethod void TestDoneForTheDayEntry()
	{
		CreateTestData();

		/////////////////////////////////
		//* RETRIEVE TEST DATA VALUES *//
		/////////////////////////////////

		User oUser = GetTestUser();

		/////////////////////////////////////
		//* MISC VARIABLES FOR ASSIGNMENT *//
		/////////////////////////////////////

		List<FM_DoneForTheDayController.DoneForTheDayRoute> lRouteData;
		List<FM_DoneForTheDayController.DoneForTheDayRouteEntry> lRouteEntryData;
		List<SelectOption> lSelectData;

		FM_Done_For_The_Day__c oDone;
		FM_DoneForTheDayController.DoneForTheDayRoute oOneRoute;
		FM_DoneForTheDayController.DoneForTheDayRouteEntry oOneRouteEntry;
		PageReference oPage1;
		PageReference oPage2;
		DateTime dTest;
		String sTest;
		Integer iTest;
		Boolean bTest;

		//////////////////
		//* START TEST *//
		//////////////////

		Test.startTest();

		System.runAs(oUser)
		{
			// Create Controllers
			FM_DoneForTheDayController oController = new FM_DoneForTheDayController();

			// Current Date SHOULD Match Today's Date
			oDone = oController.getDoneForTheDay();
			oController.setDoneForTheDay(oDone);
			System.assert(oDone.Done_Date__c == System.today());

			// Get Current User -> SHOULD Match Test User
			System.assert(oUser.Name == oController.getCurrentUserName());

			// Get Redirect PageReference
			oPage1 = oController.RedirectListView();

			// Attempt to Submit Done For The Day -> SHOULD Suceed AND re-direct to list view
			oPage2 = oController.SubmitDoneForTheDay();
			System.assert(oPage1.getURL() == oPage2.getURL());

			// Re-Attempt Submission -> SHOULD fail AND re-direct should be same page
			oPage2 = oController.SubmitDoneForTheDay();
			System.assert(oPage1.getURL() != oPage2.getURL());

			// Rebuild Route Info
			oPage1 = oController.CreateRouteInfo();

			// Get Desk Names (Selection Data) -> SHOULD be 1 (only 1 test Dispatch Desk created in CreateTestData)
			lSelectData = oController.getDeskNames();
			System.assert(lSelectData.size() == 1);

			// Get Route Count -> SHOULD be 1 (only 1 test Runsheet created for 1 route)
			iTest = oController.getRouteCount();
			System.assert(iTest == 1);

			// Get Timezone Formatted String
			sTest = oController.getTimeZone();

			// Verify Complete Permission -> SHOULD be true (running user has FIELD OPERATOR permission set)
			bTest = oController.getMarkCompletePermission();
			System.assert(bTest == true);

			// Verify View Permission -> SHOULD be true (running user has DISPATCHER INTERNAL permission set)
			bTest = oController.getViewPermission();
			System.assert(bTest == true);

			// Verify Dispatch Type -> SHOULD be true (running user has DISPATCHER INTERNAL permission set NOT VENDOR DISPATCH)
			bTest = oController.getIsDispatcherStandard();
			System.assert(bTest == true);

			// Verify Already Submitted -> SHOULD be true (running user has already marked Routes as COMPLETE)
			bTest = oController.getAlreadySubmittedForTheDay();
			System.assert(bTest == true);

			// Verify Fluid Submission -> SHOULD be true (running user has created AT LEAST 1 Runsheet for the day)
			bTest = oController.getFluidSubmittedForTheDay();
			System.assert(bTest == true);

			// Get RouteData (Custom Class) -> Full Route Data SHOULD have 1 entry (1 Route COMPLETED)
			lRouteData = oController.getUserRouteStatus();
			lRouteData = oController.getFullRouteStatus();
			System.assert(lRouteData.size() == 1);

			// Get Route Record -> gets its values -> Entry count SHOULD be 1 (one Runsheet COMPLETED) AND SHOULD be COMPLETED
			oOneRoute = lRouteData[0];
			lRouteEntryData = oOneRoute.getEntries();
			oOneRouteEntry = lRouteEntryData[0];
			iTest = oOneRoute.getEntriesSize();
			bTest = oOneRoute.getRouteComplete();
			System.assert(iTest == 1);
			System.assert(bTest == true);

			// Get Route Entry Record -> get its values -> Runsheet Count SHOULD be 1 (one Runsheet COMPLETED)
			sTest = oOneRouteEntry.getDoneTimeFormatted();
			dTest = oOneRouteEntry.getDoneTime();
			iTest = oOneRouteEntry.getRunSheetCount();
			System.assert(iTest == 1);

			// Check Entry Values -> Sort Order SHOULD be 1 (the first) -> SHOULD be done (already COMPLETED) -> SHOULD NOT be created after COMPLETED
			iTest = oOneRouteEntry.getSortOrder();
			System.assert(iTest == 1);
			bTest = oOneRouteEntry.getIsDone();
			System.assert(bTest == true);
			bTest = oOneRouteEntry.getAfterDone();
			System.assert(bTest == false);
		}

		/////////////////
		//* STOP TEST *//
		/////////////////

		Test.stopTest();
    }

	//////////////////////////////////////////////
	//* DATA RETRIEVAL FUNCTIONS --> TEST DATA *//
	//////////////////////////////////////////////

	private static User GetTestUser()
	{
		return [SELECT Id, Name FROM User WHERE Name = 'Guy Incognito' AND Alias = 'guyincog' AND Email = 'guy.incognito@testingdonefortheday.com' LIMIT 1];
	}

    // ******************************************* //
    // ** CREATE ALL TEST DATA FOR TEST METHODS ** //
    // ******************************************* //

	private static void CreateTestData()
	{
		// Get Profile
		List<Profile> lProfiles = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User' LIMIT 1];

		// Test USER
		User oUser = new User();
		oUser.FirstName = 'Guy';
		oUser.LastName = 'Incognito';
		oUser.Email = oUser.FirstName.toLowerCase() + '.' + oUser.LastName.toLowerCase() + '@testingdonefortheday.com';
		oUser.UserName = oUser.Email;
		oUser.Alias = 'guyincog';
		oUser.TimeZoneSidKey = 'America/New_York';
		oUser.LocaleSidKey = 'en_US';
		oUser.EmailEncodingKey = 'ISO-8859-1'; 
		oUser.LanguageLocaleKey='en_US';
		oUser.ProfileId = lProfiles[0].Id;
		oUser.IsActive = true;
		insert oUser;

		PermissionSet oPermission;
		PermissionSetAssignment oPermissionAssign;

		System.runAs(oUser)
		{
			// Permission Set Assignment - Field Operator
			oPermission = [SELECT Id, Name FROM PermissionSet WHERE Name = :FM_PERMISSION_FIELD_OPERATOR LIMIT 1];
			oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
			insert oPermissionAssign;

			// Permission Set Assignment - Internal Dispatcher
			oPermission = [SELECT Id, Name FROM PermissionSet WHERE Name = :FM_PERMISSION_DISPATCHER_INTERNAL LIMIT 1];
			oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
			insert oPermissionAssign;
		}

		// Test Account
		Account oAccount = new Account();
		oAccount.Name = 'Husky Account';
		insert oAccount;

		// Test Contact
		Contact oContact = new Contact();
		oContact.Account = oAccount;
		oContact.FirstName = 'Senator';
		oContact.LastName = 'McTest';
		oContact.X2Z_Employee_ID__c = 'Octothorpe';
		insert oContact;

		// Test Business Department
		Business_Department__c oBusiness = new Business_Department__c();
		oBusiness.Name = 'Husky Business';
		insert oBusiness;

		// Test Operating District
		Operating_District__c oDistrict = new Operating_District__c();
		oDistrict.Name = 'District 1234';
		oDistrict.Business_Department__c = oBusiness.Id;
		insert oDistrict;

		// Test AMU Field
		Field__c oField = new Field__c();
		oField.Name = 'AMU Field';
		oField.Operating_District__c = oDistrict.Id;
		insert oField;

		// Test Route 1
		Route__c oRoute1 = new Route__c();
		oRoute1.Fluid_Management__c = true;
		oRoute1.Name = '777';
		oRoute1.Route_Number__c = '777';
		insert oRoute1;

		// Test Route 2
		Route__c oRoute2 = new Route__c();
		oRoute2.Fluid_Management__c = true;
		oRoute2.Name = '999';
		oRoute2.Route_Number__c = '999';
		insert oRoute2;

		// Test Location 1
		Location__c oLocation1 = new Location__c();
		oLocation1.Name = 'Husky Well';
		oLocation1.Fluid_Location_Ind__c = true;
		oLocation1.Location_Name_for_Fluid__c = 'Husky Well Fluid';
		oLocation1.Operating_Field_AMU__c = oField.Id;
		oLocation1.Hazardous_H2s__c = 'Y';
		oLocation1.Functional_Location_Category__c = 4;
		oLocation1.Well_Type__c = 'OIL';
		oLocation1.Route__c = oRoute2.Id;
		insert oLocation1;

		// Test Location 2
		Location__c oLocation2 = new Location__c();
		oLocation2.Name = 'Husky WIG Well';
		oLocation2.Fluid_Location_Ind__c = true;
		oLocation2.Location_Name_for_Fluid__c = 'Husky Well Fluid Stuff';
		oLocation2.Operating_Field_AMU__c = oField.Id;
		oLocation2.Hazardous_H2s__c = 'N';
		oLocation2.Functional_Location_Category__c = 4;
		oLocation2.Well_Type__c = 'OIL';
		oLocation2.Route__c = oRoute2.Id;
		insert oLocation2;

		// Well Event Type
		List<RecordType> lRecordTypes = [SELECT Id FROM RecordType WHERE SobjectType = 'Location__c' AND DeveloperName = 'Well_Event' LIMIT 1];
		RecordType oRecordType = lRecordTypes[0];

		// Test Well Event 1
		Location__c oEvent1 = new Location__c();
		oEvent1.Name = 'Husky Well Event';
		oEvent1.RecordTypeId = oRecordType.Id;
		oEvent1.Operating_Field_AMU__c = oField.Id;
		oEvent1.Route__c = oRoute2.Id;
		oEvent1.Well_ID__c = oLocation1.Id;
		oEvent1.PVR_AVGVOL_30D_OIL__c = 111;
		oEvent1.PVR_AVGVOL_30D_WATER__c = 222;
		insert oEvent1;

		// Test Well Event 2
		Location__c oEvent2 = new Location__c();
		oEvent2.Name = 'Another Husky Well Event';
		oEvent2.RecordTypeId = oRecordType.Id;
		oEvent2.Operating_Field_AMU__c = oField.Id;
		oEvent2.Route__c = oRoute2.Id;
		oEvent2.Well_ID__c = oLocation2.Id;
		oEvent2.PVR_AVGVOL_30D_OIL__c = 123;
		oEvent2.PVR_AVGVOL_30D_WATER__c = 321;
		insert oEvent2;

		System.runAs(oUser)
		{
			// Test FLUID Run Sheet
			FM_Run_Sheet__c oFluidRunSheet = new FM_Run_Sheet__c();
			oFluidRunSheet.Date__c = Date.today();
			oFluidRunSheet.Well__c = oLocation2.Id;
			oFluidRunSheet.Act_Flow_Rate__c = 5;
			oFluidRunSheet.Act_Tank_Level__c = 3;
			oFluidRunSheet.Axle__c = '6';
			oFluidRunSheet.Flowline_volume__c = 4;
			oFluidRunSheet.Sour__c = true;
			oFluidRunSheet.Tank__c = 'Tank 2';
			oFluidRunSheet.Tomorrow_Oil__c = 1;
			insert oFluidRunSheet;
		}

		// Test Dispatch Desk
		Dispatch_Desk__c oDispatchDesk = new Dispatch_Desk__c();
		oDispatchDesk.Name = 'Table Flipping';
		oDispatchDesk.Phone__c = '403-555-1234'; 
		insert oDispatchDesk;

		// Test Done For The Day
/*
		FM_Done_For_The_Day__c oDone = new FM_Done_For_The_Day__c();
		oDone.Done_Date__c = System.today();
		oDone.Operator__c = oUser.Id;
		oDone.Operator_Route__c = oRoute1.Id;
		oDone.Runsheet_Count__c = 5;
*/
	}
}