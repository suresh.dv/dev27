@isTest
global class VrmSNowIntegrationMock implements HttpCalloutMock
{
    global HTTPResponse respond(HttpRequest req) 
    {
        req.setEndpoint('https://huskydev.service-now.com/api/now/table/cmdb_ci_appl?sysparm_query=sys_class_name%3Dcmdb_ci_appl%5Einstall_status!%3D7&sysparm_display_value=true&sysparm_fields=name%2Csys_id%2Cu_service_delivery_specialist');
        req.setMethod('GET');
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"result":[{"sys_id": "0021295fdd2916446b6dbf97591750cc","u_service_delivery_specialist": "Test User (UserT)","name": "Active Risk Manager (Atlantic Region)", "id":"12345"}]}');
        res.setStatusCode(200);
        
        return res; 
    }
}