/**
 * Created by MarcelBrimus on 27/05/2019.
 */
({
    // Loads Settings for logged in user and sets state variables
    loadUserSettingsAndAvailablePosts: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getBaseShiftHandoverData",
            {},
            this.handleLoad,
            this.handleError
        );
    },

    handleLoad: function(component, response, helper){
        var responseNew = response.resultObjects[0];

        var settingsKey = 'USERSETTINGS';
        // Will be instantiated in backend
        var availablePostsKey = 'AVAILABLEPOSTS';
        var isShiftEngineer = 'ISSHIFTENGINEER';
        var currentShiftKey = 'PRIMARYONSHIFT';

        if(responseNew[settingsKey]){
            var userSettings = responseNew[settingsKey];
            component.set('v.settingId', userSettings.Id);
            component.set('v.userName', userSettings.User__r.Name);
            component.set('v.userFacility', userSettings.Operating_Field_AMU__r.Name);
            component.set('v.userFacilityId', userSettings.Operating_Field_AMU__c);
            component.set('v.availablePosts', responseNew[availablePostsKey]);
            component.set('v.canBecomeShiftEngineer', responseNew[isShiftEngineer]);
            component.set('v.currentShift', responseNew[currentShiftKey]);
        }

        helper.initializeButtonsBasedOnState(component, helper);
        // This used in ELG_ShiftHandover_AddLogForm Component
        helper.filterOutPostsWithNoShift(component);
        helper.addCssToShifts(component);
        helper.hideSpinner(component);
    },

    handleError: function(component, response, helper){
        var responseString = response.resultObjects[0];
        console.log('ERROR loading user settings ' + JSON.stringify(response));
        helper.displayToast(component, 'error', responseString);
        helper.hideSpinner(component);
    },

    // Renders buttons on header based on state of data
    initializeButtonsBasedOnState: function(component, helper) {
        var availablePosts = component.get('v.availablePosts');

        if (availablePosts.length == 0) {
            component.set('v.selectedPostId', null);
        }

        console.log('initializeButtonsBasedOnState');
        var buttonsToRender = helper.getButtonsToRender(component, helper);

        // First destroy all buttons
        component.set("v.body", []);

        $A.createComponents(buttonsToRender,
        function(components, status, errorMessage){
            if (status === "SUCCESS") {
                var body = component.get("v.body");
                components.forEach(function(item){
                    body.push(item);
                });
                component.set("v.body", body);
            } else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.")
            } else if (status === "ERROR") {
                console.log("Error: " + errorMessage);
            }
        }
       );
    },

    // Returns array of buttons to be rendered based on state of data
    getButtonsToRender: function(component, helper){
        console.log('setting buttons');
        var buttonsToRender = [];
        var shiftLeadPostId;

        console.log('component.get: ' + component.get('v.settingId'));

        // Only users with settings can use the app
        if(helper.getHasUserSetSetting(component)){

            // Also if user selected facility with no posts then he has to choose another one
            // Or setup posts on facility
            if(component.get('v.availablePosts').length > 0){

                // Filter only Posts with new shifts
                var postsWithShift = [];

                // Shift where Shift Engineer is not yet assigned and should be
                var shiftsThatAreMissingShiftEngineer = [];

                // Loop to populated arrays above
                component.get('v.availablePosts').forEach((item) => {
                    if(item.Shift_Assignements__r) {

                        if(!item.Shift_Assignements__r[0].Shift_Engineer__c &&
                        item.Shift_Assignements__r[0].Post__r.Shift_Engineer_Review_Required__c){
                            shiftsThatAreMissingShiftEngineer.push(item);
                        }
                        postsWithShift.push(item);
                    }

                    if (item.Shift_Lead_Post__c) {
                        shiftLeadPostId = item.Id;
                    }
                });

                // Only allow starting shift if Post has not started any shift (can happen when app is deployed)
                // Or New post is added to facility
                if(postsWithShift.length != component.get('v.availablePosts').length){
                    buttonsToRender.push(helper.createButton('slds-theme_brand slds-m-right_xxx-small',
                                        'Success', 'Start Shift', component, "c.showFirstShift"));
                }

                // If there is at least one shift for this facility, render these buttons
                if(postsWithShift.length > 0){

                    // Render this button if there active shift on post that need engineer and does not have it
                    if(shiftsThatAreMissingShiftEngineer.length > 0
                        && component.get('v.canBecomeShiftEngineer')){
                        buttonsToRender.push(helper.createButton('slds-theme_brand slds-m-right_xxx-small',
                                                   'destructive', 'Become Shift Engineer', component, "c.showAssignShiftEngineer"));
                    }

                    buttonsToRender.push(helper.createButton('slds-theme_brand slds-m-right_xxx-small',
                                       'Brand', 'Take Post', component, "c.showTakePost"));

                   if (component.get('v.selectedPostId') == shiftLeadPostId) {
                    buttonsToRender.push(helper.createButton('slds-theme_brand slds-m-right_xxx-small',
                                        'Brand', 'Shift Summary', component, "c.navigateToShiftSummary"));
                   } else {
                    buttonsToRender.push(helper.createButton('slds-theme_brand slds-m-right_xxx-small',
                                        'Brand', 'Handover', component, "c.navigateToHandover"));
                   }

                } else {
                    this.displayToast(
                        component,
                        'success',
                        'Currently there is no shift on ony of the posts, click on Start First Shift to start one.'
                    );
                }
            } else {
                this.displayToast(
                    component,
                    'warning',
                    'There is no Post setup for this Facility, please contact HOGLloydSF@huskyenergy.com '
                );
            }
        } else {
            this.displayToast(
                component,
                'warning',
                'Please click on My Settings to set Facility you are working on'
            );
        }

        buttonsToRender.push(helper.createButton('slds-theme_brand slds-m-right_xxx-small',
                               'Brand', 'My Settings', component, "c.showSettings"));


        return buttonsToRender;
    },

    // Post status dropdown
    showHide : function(component, event, helper){
        var myMenu = component.find('postStatus');
        $A.util.toggleClass(myMenu, 'slds-is-open');
    },

    // Returns single button
    createButton: function(styleClass, buttonVariant, label, component, onClickComponentReference){
        var buttonSettings = {};
        buttonSettings.class = styleClass;
        buttonSettings.variant = buttonVariant;
        buttonSettings.label = label;
        if(onClickComponentReference){
            buttonSettings.onclick = component.getReference(onClickComponentReference);
        }
        var button = ['lightning:button',buttonSettings];
        return button;
    },

    // Returns true if userSettingId is set (this means user has some settings selected)
    // If not then returns false
    getHasUserSetSetting: function(component){
        return component.get('v.settingId') ?  true:false;
    },

    // Add styling to Post Statuses items
    addCssToShifts: function(component){
        if(component.get('v.availablePosts').length > 0){
            component.get('v.availablePosts').forEach((item) => {
                if(item.Shift_Assignements__r){
                    if(item.Shift_Assignements__r[0].Shift_Status__c == 'New'){
                        item.cssClass = 'slds-has-success';
                    } else {
                        item.cssClass = 'slds-has-warning';
                    }
                }
            });
        }
    },

    // Only allow adding entries to posts where there is at least one active shift
    // Filter out posts with no shift and shifts with wrong status so that users cannot add log entries into them
    // These are used for add log entries component
    filterOutPostsWithNoShift: function (component){
        var availablePosts = component.get('v.availablePosts');
        var filteredPosts = [];
        availablePosts.forEach((item) => {
            if(item.Shift_Assignements__r){
                if(item.Shift_Assignements__r[0].Shift_Status__c === 'New'
                || item.Shift_Assignements__r[0].Shift_Status__c === 'Handover in progress'){
                     filteredPosts.push(item);
                }

            }
        });
        component.set('v.postsWithActiveShift', filteredPosts);
    },

    // HELPERS
    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 10000
        });
        toastEvent.fire();
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },
})