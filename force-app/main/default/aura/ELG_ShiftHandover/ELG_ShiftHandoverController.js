/**
 * Created by MarcelBrimus on 27/05/2019.
 */
({
    init: function (component, evt, helper) {
        helper.loadUserSettingsAndAvailablePosts(component);
    },

    // Displays settings with form
    showSettings : function(component, evt, helper) {
        var modalBody;
        $A.createComponent("c:ELG_SettingsModal", {
            userSettingId:component.get('v.settingId'),
            settingsSaved:component.getReference('c.onFormSubmitted')
        }, function(content, status, errorMessage) {
            console.log('Creating ELG_SettingsModal ' + status + ' - ' + errorMessage );
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: "My Settings",
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },

    // Handle when handover ID is sent from ELG_GenericEvent
    onHandoverDocumentIdChanged: function(component, evt, helper) {
        component.set('v.handoverDocumentId', evt.getParam("handoverId"));
    },

    // Displays First Shift form
    showFirstShift : function(component, evt, helper) {
       var modalBody;
       $A.createComponent("c:ELG_StartFirstShiftModal", {
           startedFirstShift:component.getReference('c.onFormSubmitted')
       },
          function(content, status, errorMessage) {
              console.log('Creating ELG_StartFirstShiftModal ' + status + ' - ' + errorMessage );
              if (status === "SUCCESS") {
                  modalBody = content;
                  component.find('overlayLib').showCustomModal({
                      header: "Starting Shift",
                      body: modalBody,
                      showCloseButton: true,
                      closeCallback: function() {}
                  })
              }
          });
    },

    // Show data correction
    showShiftDataCorrection: function(component, evt, helper) {
        var modalBody;
        $A.createComponent("c:ELG_DataCorrection", {
            shiftDataCorrection:component.getReference('c.onFormSubmitted')
        },
        function(content, status, errorMessage) {
            console.log('Creating ELG_DataCorrection ' + status + ' - ' + errorMessage );
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: "Shift Management",
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },

    // Displays take post
    showTakePost : function(component, evt, helper) {
       var modalBody;
       $A.createComponent("c:ELG_AcceptHandover", {
           acceptedHandover:component.getReference('c.onFullPageRefresh')
       },
          function(content, status, errorMessage) {
              console.log('Creating ELG_AcceptHandover ' + status + ' - ' + errorMessage );
              if (status === "SUCCESS") {
                  modalBody = content;
                  component.find('overlayLib').showCustomModal({
                      header: "Take Post",
                      body: modalBody,
                      showCloseButton: true,
                      closeCallback: function() {}
                  })
              }
          });
    },

    // Display Assign Shift Engineer Modal
    showAssignShiftEngineer : function(component, evt, helper) {
       var modalBody;
       $A.createComponent("c:ELG_AssignShiftEngineer", {
           userFacilityId: component.get('v.userFacilityId'),
           assignEngineer:component.getReference('c.onFullPageRefresh')
       },
          function(content, status, errorMessage) {
              console.log('Creating ELG_AssignShiftEngineer ' + status + ' - ' + errorMessage );
              if (status === "SUCCESS") {
                  modalBody = content;
                  component.find('overlayLib').showCustomModal({
                      header: "Become Shift Engineer",
                      body: modalBody,
                      showCloseButton: true,
                      closeCallback: function() {}
                  })
              }
          });
    },

    // Navigation to AMU record when user clicks on link in header
    onAMUClick: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.userFacilityId")
        });
        navEvt.fire();
    },

    // Callback to refresh main component, mostly called from modal components after successful submit
    onFormSubmitted : function(component, evt, helper) {
        helper.loadUserSettingsAndAvailablePosts(component);
    },

    // Callback used from modal that need full page refresh
    onFullPageRefresh : function(component, evt, helper){
        $A.get("e.force:refreshView").fire();
    },

    // When Handover button is clicked, navigate to handover
    navigateToHandover : function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": component.get('v.handoverDocumentId'),
          "slideDevName": "detail"
        });
        navEvt.fire();
    },

    navigateToShiftSummary : function (component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:ELG_ShiftSummary_Record",
            isredirect : "true",
            componentAttributes: {
             shiftId : component.get("v.selectedShiftId")
            }
        });
        evt.fire();
    },

    showHide : function(component, event, helper){
        helper.showHide(component, event, helper);
    },

    hideOnBlur : function(component, event, helper){
        var myMenu = component.find('postStatus');
        $A.util.removeClass(myMenu, 'slds-is-open');
    },

     // Handle when handover ID is sent from ELG_GenericEvent
    handlePostIdForTask : function(component, evt, helper) {
        component.set("v.selectedPostId", evt.getParam("postId"));
        component.set("v.selectedPostName", evt.getParam("postName"));
        var finder = component.find("taskComponent");
        if (finder) {
            finder.getPostId();
        }
        helper.initializeButtonsBasedOnState(component, helper);
    },

     //createTask
    createTask : function(component, evt, helper) {
        var modalBody;
        var currentShift = component.get('v.currentShift');
        var selectedAmuId = component.get('v.postsWithActiveShift')[0].Operating_Field_AMU__c;

        if (currentShift) {
            selectedAmuId = currentShift.Post__r.Operating_Field_AMU__c;
        } else {
            selectedAmuId = component.get('v.postsWithActiveShift')[0].Operating_Field_AMU__c;
        }

        $A.createComponent("c:ELG_Task_Create",
            {selectedPostId : component.get('v.selectedPostId'),
            amuId : selectedAmuId},
            function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: 'Create Task',
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },

    setSelectedShiftId : function(component, event, helper) {
        component.set('v.selectedShiftId', event.getParam("shiftId"));
        console.log('EVENT PARAM 2 : ' +component.get('v.selectedShiftId'));
    },

    openShiftSummary : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:ELG_ShiftSummary_Record",
            isredirect : "true",
            componentAttributes: {shiftId : component.get('v.selectedShiftId')}
        });
        evt.fire();
    },
})