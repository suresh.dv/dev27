/**
 * Created by MarcelBrimus on 03/06/2019.
 */
({
    init: function (component, evt, helper) {
        // Set current user to prepopulate User lookup on settings
        component.set("v.currentUserId", $A.get("$SObjectType.CurrentUser.Id"));
        helper.loadAvailablePosts(component);
    },

    // Set Post before submitting
    onRecordSubmit: function(component, event, helper) {
        component.set("v.submitDisabled", 'true');
      event.preventDefault();
      var eventFields = event.getParam("fields");
      eventFields["Post__c"] = component.get('v.selectedPost');
      component.find('startFirstShiftForm').submit(eventFields);
    },

    handleSaveSuccess : function(component, event, helper) {
        var startedFirstShift = component.getEvent('startedFirstShift');
        startedFirstShift.fire();
        component.find("overlayLib").notifyClose();
    },

    handleFormLoad : function(component, event, helper) {
        component.set("v.submitDisabled", 'false');
    }
})