/**
 * Created by MarcelBrimus on 03/06/2019.
 */
({
    // Loads Settings for logged in user and sets state variables
    loadAvailablePosts: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getBaseShiftHandoverData",
            {},
            this.handleSuccess,
            this.handleError
        );
    },

    handleError: function(component, response, helper){
        helper.hideSpinner(component);
    },

    // Populates available posts and sets default value on postPicklist
    handleSuccess: function(component, response, helper){

        var responseNew = response.resultObjects[0];
        var availableKey = 'AVAILABLEPOSTS';
        if(responseNew[availableKey] && responseNew[availableKey].length > 0){

            var postsWithNoShift = [];
            responseNew[availableKey].forEach((item) => {

                if(!item.Shift_Assignements__r) {
                    postsWithNoShift.push(item);
                }
            });

            // If there is at least one post which does not have shift started
            // Set first available post as default, because if there is exactly one available post
            // then this value is not set
            if(postsWithNoShift.length > 0){
                component.find("postPicklist").set("v.value", postsWithNoShift[0].Id);
                component.set('v.availablePostsToStartShift', postsWithNoShift);
            }
        }
        helper.hideSpinner(component);
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },

    // HELPERS
    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 10000
        });
        toastEvent.fire();
    },
})