/**
 * Created by MarosGrajcar on 10/22/2019.
 */

({
    saveTask : function(component, event, helper) {
        if (helper.validateCancelReason(component)) {
            component.set('v.disableButton', true);
            component.set('v.buttonName', 'Processing');
            var userId = $A.get("$SObjectType.CurrentUser.Id");
            var apexService = component.find("apexService");
            apexService.callApexWithErrorCallbackWithHelper(
                component,
                this,
                "c.taskUpdateCancelled",
                {taskId : component.get('v.recordId'),
                userId : userId,
                cancellationReason : component.get('v.cancelReason')},
                this.handleSaveSuccess,
                this.handleError
            );
        }
    },

    handleSaveSuccess : function(component, event, helper) {
        console.log('SUCESS');
        component.set('v.disableButton', false);
        component.set('v.buttonName', 'Cancel Task');
         // Close modal
        component.find("overlayLib").notifyClose();
        $A.get('e.force:refreshView').fire();
    },

    handleError : function(component, requestResult, helper) {
        console.log('ERROR ' +requestResult.errors[0]);
        component.set('v.disableButton', false);
        component.set('v.buttonName', 'Cancel Task');
    },

    validateCancelReason : function(component, event, helper) {
        var inputCmp = component.find("cancelReason");
        var isFieldFilled = component.get('v.cancelReason');
        var isInputValid = true;
        var cancelReasonMaxCharacters = 30000;

        if(!isFieldFilled) {
           inputCmp.setCustomValidity("Please, fill the Cancellation Reason.");
           inputCmp.reportValidity();
           isInputValid = false;
        } else if(isFieldFilled.length > cancelReasonMaxCharacters) {
           cancelReasonMaxCharacters = isFieldFilled.length - cancelReasonMaxCharacters;
           inputCmp.setCustomValidity("Cancellation Reason cannot exceed more than 30000 characters. You are above " +cancelReasonMaxCharacters+ " characters limit to proceed.");
           inputCmp.reportValidity();
           isInputValid = false;
        }

        return isInputValid;
    },
});