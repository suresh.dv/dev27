/**
 * Created by MarosGrajcar on 10/21/2019.
 */

({
    init : function(component, event, helper) {
        helper.loadTask(component, event, helper);
    },

    showCompleteSection : function(component, event, helper) {
        component.set('v.openCancelSection', false);
        component.set('v.openCompleteSection', !component.get('v.openCompleteSection'));
    },

    showCancelSection : function(component, event, helper) {
        component.set('v.openCompleteSection', false);
        component.set('v.openCancelSection', !component.get('v.openCancelSection'));
    },

    handleClose : function(component, event, helper) {
        // Close modal
        component.find("overlayLib").notifyClose();
    },

    handleComplete : function(component, event, helper) {
        helper.saveTaskComplete(component, event, helper);
    },

    handleCancel : function(component, event, helper) {
        helper.saveTaskCancel(component, event, helper);
    },
});