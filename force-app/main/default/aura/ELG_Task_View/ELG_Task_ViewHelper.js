/**
 * Created by MarosGrajcar on 10/21/2019.
 */

({
    loadTask : function(component, event, helper) {
        component.set('v.loading', true);
        component.set('v.disableButton', true);
        var apexService = component.find("apexService");
        //this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getTask",
            {taskId : component.get('v.recordId')},
            this.handleLoad,
            this.handleError
        );
    },

    handleLoad : function(component, requestResult, helper) {
        console.log('SUCCESS');
        component.set("v.task", requestResult.resultObjects[0]);
        var obj = requestResult.resultObjects[0];
        if (component.get('v.task').Status__c == 'Active') {
            component.set('v.isTaskClosed', false);
        } else {
            component.set('v.isTaskClosed', true);
        }
        component.set('v.loading', false);
        component.set('v.disableButton', false);
    },

    handleError : function(component, requestResult, helper) {
        console.log('ERROR');
        component.set('v.loading', false);
        component.set('v.disableButton', false);
    },


     saveTaskComplete : function(component, event, helper) {
        component.set('v.disableButton', true);
        component.set('v.buttonNameComplete', 'Processing');
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.taskUpdateComplete",
            {taskId : component.get('v.recordId'),
            userId : userId,
            userComment : component.get('v.userComment')},
            this.handleSaveSuccess,
            this.handleError
        );
    },

    saveTaskCancel : function(component, event, helper) {
        if (helper.validateCancelReason(component)) {
            component.set('v.disableButton', true);
            component.set('v.buttonNameCancel', 'Processing');
            var userId = $A.get("$SObjectType.CurrentUser.Id");
            var apexService = component.find("apexService");
            apexService.callApexWithErrorCallbackWithHelper(
                component,
                this,
                "c.taskUpdateCancelled",
                {taskId : component.get('v.recordId'),
                userId : userId,
                cancellationReason : component.get('v.cancelReason')},
                this.handleSaveSuccess,
                this.handleError
            );
        }
    },

    handleSaveSuccess : function(component, event, helper) {
        console.log('SUCCESS');
        component.set('v.disableButton', false);
         // Close modal
        if (component.get('v.isModal')) {
            component.find("overlayLib").notifyClose();
        }
        if (component.get('v.inWrapper')) {
            component.getEvent('reloadTaskWrapper').fire();
        } else {
            $A.get('e.force:refreshView').fire();
        }
    },

    handleError : function(component, requestResult, helper) {
        console.log('ERROR ' +requestResult.errors[0]);
        component.set('v.disableButton', false);
        component.set('v.buttonNameComplete', 'Complete Task');
        component.set('v.buttonNameCancel', 'Cancel Task');
    },

    validateCancelReason : function(component, event, helper) {
        var inputCmp = component.find("cancelReason");
        var isFieldFilled = component.get('v.cancelReason');
        var isInputValid = true;
        var cancelReasonMaxCharacters = 30000;

        if(!isFieldFilled) {
           inputCmp.setCustomValidity("Please, fill the Cancellation Reason.");
           inputCmp.reportValidity();
           isInputValid = false;
        } else if(isFieldFilled.length > cancelReasonMaxCharacters) {
           cancelReasonMaxCharacters = isFieldFilled.length - cancelReasonMaxCharacters;
           inputCmp.setCustomValidity("Cancellation Reason cannot exceed more than 30000 characters. You are above " +cancelReasonMaxCharacters+ " characters limit to proceed.");
           inputCmp.reportValidity();
           isInputValid = false;
        }

        return isInputValid;
    },
});