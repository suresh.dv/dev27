/**
 * Created by MarcelBrimus on 14/06/2019.
 */
({
    init: function (component, event, helper) {
        helper.loadHandoverData(component);
    },

    navigateToHandover: function (component, event, helper) {
        var selectedId = component.get("v.selectedDocument");
        console.log('Value is: ' +selectedId);

        if (selectedId == component.get('v.shiftLeadPostHandoverId')) {
            console.log('to component');
            var evt = $A.get("e.force:navigateToComponent");
                evt.setParams({
                componentDef : "c:ELG_ShiftSummary_Record",
                isredirect : "true",
                 componentAttributes: {shiftId : component.get('v.shiftLeadPostShiftId')}
            });
            evt.fire();
        } else {
            console.log('to record');
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": selectedId
            });
            navEvt.fire();
        }
    },
})