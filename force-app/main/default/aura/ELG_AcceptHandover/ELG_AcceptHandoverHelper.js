/**
 * Created by MarcelBrimus on 17/06/2019.
 */
({
    // Load Handover data
    // Also checks if there is some data
    loadHandoverData: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.findDataForTakePost",
            {},
            this.handleLoadSuccess,
            this.handleLoadError
        );
    },

    handleLoadSuccess: function(component, response, helper){
        if (response.resultObjects[0]) {
            var handoversForAcceptance = response.resultObjects[0];

            if (response.resultObjects[0].length > 0) {
                component.set('v.selectedDocument', response.resultObjects[0][0].Id);
            }

            component.set('v.handoverDocuments', handoversForAcceptance);
            console.log('WTF: ' +JSON.stringify(component.get('v.handoverDocuments')));
            handoversForAcceptance.forEach(function(handover) {
                console.log('handoversForAcceptance: ' +handover.Shift_Assignement__r.Post__r.Shift_Lead_Post__c);
                if (handover.Shift_Assignement__r.Post__r.Shift_Lead_Post__c) {
                    component.set('v.shiftLeadPostHandoverId', handover.Id);
                    component.set('v.shiftLeadPostShiftId', handover.Shift_Assignement__c);
                }
            });

            console.log('PICUS: ' +component.get('v.shiftLeadPostHandoverId'));
        }

        helper.hideSpinner(component);
    },

    handleLoadError: function(component, response, helper){
        helper.displayToast(component, 'error', 'Error getting Data for taking posts');
        helper.hideSpinner(component);
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },

    // HELPERS
    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 10000
        });
        toastEvent.fire();
    },
})