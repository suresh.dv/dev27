({

	/**
	 * Fire event when this record is selected
	 */
	selectRecord : function(component, event, helper) {
		helper.fireSelectRecordEvt(component);
	},

})