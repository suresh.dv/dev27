({

	/**
	 * Fire selectRecord event with selected record as param.
	 */
	fireSelectRecordEvt : function(component) {
		var selectedRecord = component.get("v.objRecord");
		var evt = component.getEvent("selectedRecord");
		evt.setParams({"recordByEvent" : selectedRecord});
		evt.fire();
	},

})