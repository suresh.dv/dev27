/**
 * Created by MarosGrajcar on 10/17/2019.
 */

({
    loadPostTasks : function (component, event, helper) {
        component.set('v.loading', true);
        var apexService = component.find("apexService");
        //this.showSpinner(component);

        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getPostTasks",
            {"postId" : component.get("v.selectedPostId")},
            this.handleLoad,
            this.handleError
            );
    },

    handleLoad : function(component, requestResult, helper) {
        console.log('SUCCESS on load Tasks');
        component.set("v.postTasks", requestResult.resultObjects[0]);
        if (requestResult.resultObjects[0] && requestResult.resultObjects[0].length == 0) {
            component.set('v.postHasTask', false);
        } else {
            component.set('v.postHasTask', true);
        }
        component.set('v.loading', false);
    },

    handleError : function(component, requestResult, helper) {
        console.log('ERROR');
        component.set('v.loading', false);
    },

});