/**
 * Created by MarosGrajcar on 10/17/2019.
 */

({
    handlePostId : function(component, event, helper) {
        helper.loadPostTasks(component, event, helper);
    },

    handleViewSelection: function (component, event, helper) {
        component.set('v.truncate', !component.get('v.truncate'));
    },

    openTaskModal : function(component, event, helper) {
        var taskId = event.target.dataset.taskid;
        var modalBody;
        $A.createComponent("c:ELG_Task_View",
            {recordId : taskId,
            isModal : true},
            function(content, status) {
            if (status === "SUCCESS") {
              modalBody = content;
              component.find('overlayLib').showCustomModal({
                  header: 'Task',
                  body: modalBody,
                  showCloseButton: true,
                  closeCallback: function() {}
              })
            }
        });
    },

    completeTask : function(component, event, helper) {
        var taskId = event.target.dataset.idtask;
        var taskName = event.target.dataset.taskname;
        var postName = event.target.dataset.postname;
        var shortDescription = event.target.dataset.shortdescription;
        var taskDescription = event.target.dataset.taskdescription;
        var modalBody;
        $A.createComponent("c:ELG_Task_Complete",
             {recordId : taskId,
            taskName : taskName,
            postName : postName,
            shortDescription : shortDescription,
            taskDescription : taskDescription},
            function(content, status) {
            if (status === "SUCCESS") {
              modalBody = content;
              component.find('overlayLib').showCustomModal({
                  header: 'Complete Task',
                  body: modalBody,
                  showCloseButton: true,
                  closeCallback: function() {}
              })
            }
        });
    },

    cancelTask : function(component, event, helper) {
        var taskId = event.target.dataset.idtask;
        var taskName = event.target.dataset.taskname;
        var postName = event.target.dataset.postname;
        var shortDescription = event.target.dataset.shortdescription;
        var taskDescription = event.target.dataset.taskdescription;
        var modalBody;
        $A.createComponent("c:ELG_Task_Cancel",
            {recordId : taskId,
            taskName : taskName,
            postName : postName,
            shortDescription : shortDescription,
            taskDescription : taskDescription},
            function(content, status) {
            if (status === "SUCCESS") {
              modalBody = content;
              component.find('overlayLib').showCustomModal({
                  header: 'Cancel Task',
                  body: modalBody,
                  showCloseButton: true,
                  closeCallback: function() {}
              })
            }
        });
    },

    refreshData : function(component, event, helper) {
        helper.loadPostTasks(component, event, helper);
    },

});