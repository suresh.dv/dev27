/**
 * Created by MarosGrajcar on 10/30/2019.
 */

({
    // Loads Settings for logged in user and sets state variables
    loadUserSettings : function(component, event, helper) {
        var apexService = component.find("apexService");
        //this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getUserSettings",
            {},
            this.handleLoad,
            this.handleError
        );
    },

    handleLoad : function(component, requestResult, helper) {
        console.log('SUCCESS');
        component.set('v.userSettings', requestResult.resultObjects[0]);
        component.set('v.amuId', component.get('v.userSettings').Operating_Field_AMU__c);
        component.set('v.settingId', component.get('v.userSettings').Id);
        var headerTitle = 'Create Task for ';
        headerTitle += component.get('v.userSettings').Operating_Field_AMU__r.Name;
        component.set('v.headerTitle', headerTitle);
        component.set('v.buttonLabel', headerTitle);
    },

    handleError : function(component, requestResult, helper) {
        console.log('Error on holder ' +requestResult.errors[0]);
        component.set('v.buttonLabel', 'Click My Setting to proceed with Creating Task');
        component.set('v.buttonVariant', 'destructive');
    },
});