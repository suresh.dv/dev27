/**
 * Created by MarosGrajcar on 10/29/2019.
 */

({

    init : function(component, event, helper) {
        helper.loadUserSettings(component, event, helper);
    },

    // Displays settings with form
    openSettings : function(component, event, helper) {
        var modalBody;
        $A.createComponent("c:ELG_SettingsModal", {
            userSettingId:component.get('v.settingId'),
            settingsSaved:component.getReference('c.onFormSubmitted')
        }, function(content, status, errorMessage) {
            console.log('Creating ELG_SettingsModal ' + status + ' - ' + errorMessage );
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: "Set Default AMU",
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },

    //createTask
    createTask : function(component, event, helper) {
        var modalBody;
        var headerTitle = component.get('v.headerTitle');
        $A.createComponent("c:ELG_Task_Create",
            {amuId : component.get('v.amuId')},
            function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: headerTitle,
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },

    onFormSubmitted : function(component, evt, helper) {
        $A.get('e.force:refreshView').fire();
    },

    handlePassedTasks : function(component, event, helper) {
        var taskIds = [];
        var selectedTasks = event.getParam('selectedDatatableTasks');

        if (selectedTasks.length > 1) {
            selectedTasks.forEach(function(taskId) {
                taskIds.push(taskId.taskId);
             });
        } else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "message": "Please, select at least two Handovers to proceed."
                });
            toastEvent.fire();
        }

         component.set('v.taskIds', taskIds);

        var evt = $A.get("e.force:navigateToComponent");
         evt.setParams({
             componentDef : "c:ELG_Task_Search_ListWrapper",
             isredirect : "true",
             componentAttributes: {
                 taskIds : component.get("v.taskIds")
             }
         });
         evt.fire();
    },
});