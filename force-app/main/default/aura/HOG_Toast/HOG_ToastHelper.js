({
    
	fireNotification : function(toastTitle, toastMessage, toastType, toastDuration) {
		if(toastDuration == null)  toastDuration = 3000;
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
		    title: toastTitle,
		    message: toastMessage,
		    type: toastType,
		    duration: toastDuration
		});
		toastEvent.fire();
	},
    
})