/**
 * Created by MarosGrajcar on 10/22/2019.
 */

({
    handleSubmit : function(component, event, helper) {
      helper.saveTask(component, event, helper);
    },

    handleClose : function(component, event, helper) {
       // Close modal
      component.find("overlayLib").notifyClose();
    },
});