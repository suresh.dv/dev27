/**
 * Created by MarosGrajcar on 10/22/2019.
 */

({
    saveTask : function(component, event, helper) {
        component.set('v.disableButton', true);
        component.set('v.buttonName', 'Processing');
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.taskUpdateComplete",
            {taskId : component.get('v.recordId'),
            userId : userId,
            userComment : component.get('v.userComment')},
            this.handleSaveSuccess,
            this.handleError
        );
    },

    handleSaveSuccess : function(component, event, helper) {
        console.log('SUCESS');
        component.set('v.disableButton', false);
        component.set('v.buttonName', 'Complete Task');
         // Close modal
        component.find("overlayLib").notifyClose();
        $A.get('e.force:refreshView').fire();
    },

    handleError : function(component, requestResult, helper) {
        console.log('ERROR ' +requestResult.errors[0]);
        component.set('v.disableButton', false);
        component.set('v.buttonName', 'Complete Task');
    },

});