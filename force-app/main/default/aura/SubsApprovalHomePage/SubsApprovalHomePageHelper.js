({
	sortHelper : function(component, event, sortFieldName){
    	var currentDir = component.get("v.arrowDirection");
 		var currentDir = component.get("v.arrowDirection");
 		console.log('arrowDirection --- ' + currentDir);
        console.log('sortFieldName --- ' + sortFieldName);
        if (currentDir == 'arrowdown') 
        {
         // set the arrowDirection attribute for conditionally rendred arrow sign  
         	component.set("v.arrowDirection", 'arrowup');
         // set the isAsc flag to true for sort in Assending order.  
         	component.set("v.isAsc", true);
      	} 
        else 
        {
         	component.set("v.arrowDirection", 'arrowdown');
         	component.set("v.isAsc", false);
      	}
		var action= component.get("c.getUserPendingProcess");
        
        action.setParams({
            		'sortField':sortFieldName,
                    'isAsc':component.get("v.isAsc")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS')
            {
                if(response.getReturnValue() != null)
                {
                    component.set("v.PendingProcessList",response.getReturnValue());
                    console.log('response.getReturnvalue() - ' + JSON.stringify(response.getReturnValue()));
                }
            }
        });
        $A.enqueueAction(action);
	}
})