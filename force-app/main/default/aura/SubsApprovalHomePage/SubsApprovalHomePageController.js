({
	doInit : function(component, event, helper) {
		var action = component.get("c.getUserPendingProcess");
        action.setParams({
            		'sortField':'',
                    'isAsc': false
        });
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state === "SUCCESS")
            {
                if(response.getReturnValue() != null)
                {
                    component.set("v.PendingProcessList",response.getReturnValue());
                    console.log('response.getReturnvalue() - ' + JSON.stringify(response.getReturnValue()));
                }
            }
        });
        $A.enqueueAction(action);
	},
    sortRelatedTo: function(component, event, helper) {
       component.set("v.selectedTabsoft", 'RelatedTo');
       helper.sortHelper(component, event, 'RelatedTo');
    },
    sortName: function(component, event, helper) {
       component.set("v.selectedTabsoft", 'Name');
       helper.sortHelper(component, event, 'Name');
    },
    sortAmount : function(component, event, helper) {
       component.set("v.selectedTabsoft", 'Amount');
       helper.sortHelper(component, event, 'Amount');
    },
    sortCurrency : function(component, event, helper) {
       component.set("v.selectedTabsoft", 'Currency');
       helper.sortHelper(component, event, 'Currency');
    },
    sortStartDate : function(component, event, helper) {
       component.set("v.selectedTabsoft", 'StartDate');
       helper.sortHelper(component, event, 'StartDate');
    },
    sortEndDate : function(component, event, helper) {
       component.set("v.selectedTabsoft", 'EndDate');
       helper.sortHelper(component, event, 'EndDate');
    },
})