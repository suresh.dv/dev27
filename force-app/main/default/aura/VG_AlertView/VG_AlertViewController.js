({
	doInit : function(component, event, helper) {
    	/*helper.loadAlert(component);*/
    	helper.loadUserPermissions(component);
	},
	
    handleAlertUpdate : function(component, event, helper) {
		helper.loadAlert(component);
    	helper.loadUserPermissions(component);
	},
})