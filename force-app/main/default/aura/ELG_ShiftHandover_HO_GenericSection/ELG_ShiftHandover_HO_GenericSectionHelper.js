/**
 * Created by MarosGrajcar on 6/27/2019.
 */

({
    /*Load section*/

    loadLogEntries : function(component) {
        var helper = this;
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.loadCategoryAndQuestions",
            {categoryName : component.get('v.sectionName')},
            this.loadSuccess,
            this.errorLoad);
    },

    loadSuccess : function(component, requestResult, helper) {
        var sections = requestResult.resultObjects[0];
        component.set("v.logEntries", sections);
        component.set('v.buttonLabel', 'Save');
        component.set("v.disableSaveButton", true);
    },

    errorLoad : function(component, requestResult, helper) {
        console.log('Error on log load');
        helper.toastMessage('',requestResult.errors[0],'error');
        // Close modal
        component.find("overlayLib").notifyClose();
        $A.get("e.force:refreshView").fire();
    },

    /*Validation section*/
    /*Check to call Save or Not and leave component open*/
    validateBeforeSave : function(component, event, helper) {
        component.set('v.buttonLabel', 'Processing');
        component.set("v.disableSaveButton", true);
        component.set("v.disableCancelButton", true);
        var isSaveValid = helper.validateAllSectionsBeforeSave(component, event, helper);
        if(isSaveValid) {
           helper.insertLogEntries(component);
        } else {
        component.set('v.buttonLabel', 'Save');
        component.set("v.disableSaveButton", false);
        component.set("v.disableCancelButton", false);
        }
    },

    /*Validate the field values: If empty and checked wont save and give error message from ELG_ShiftHandover_HO_SectionFiller.cmp*/
    validateAllSectionsBeforeSave : function(component, event, helper) {
        var sectionWithQuestions= component.find('questionSection');
        var checked = true;

        if(sectionWithQuestions.length) {
            sectionWithQuestions.forEach(function(section) {
            var fieldValidation = section.validateField();
            if(!fieldValidation) {
               checked = false;
            }
            });

        } else {
            var singleSection = component.find('questionSection');
            var fieldValidation = singleSection.validateField();
            if(!fieldValidation) {
                checked = false;
            }
        }

        return checked;
    },

    /*Save section*/
    insertLogEntries : function(component) {
        var helper = this;
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.logEntriesFromComponent",
            {listOfEntries : component.get('v.logEntries'),
            shiftAssignment : component.get('v.shiftAssignement')},
            this.saveSuccessful,
            this.errorSave);
    },

    /*On successful save refresh page and close modal*/
    saveSuccessful : function(component, requestResult, helper) {
        // Close modal
        component.find("overlayLib").notifyClose();
        var event = $A.get("e.c:ELG_GenericApplicationEvent");
        event.fire();
    },

    errorSave : function(component, requestResult, helper) {
        console.log('Error on section save');
        helper.toastMessage('',requestResult.errors[0],'error');
        // Close modal
        component.find("overlayLib").notifyClose();
        $A.get("e.force:refreshView").fire();
    },


    /*Generic Toast message section*/
    /*fire an notification on successful save*/
    toastMessage : function(toastTitle, toastMessage, toastType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: toastTitle,
            message: toastMessage,
            type: toastType
        });
        toastEvent.fire();
    },

     /*Button validation section*/
     /*Will set Save button as disabled until user action is taken, function is triggered on event named checkboxOnChange*/
     enableOrDisableSaveButton : function(component, event, helper) {
          var state = event.getParam('setButtonState');
          component.set('v.disableSaveButton', state);
    },
});