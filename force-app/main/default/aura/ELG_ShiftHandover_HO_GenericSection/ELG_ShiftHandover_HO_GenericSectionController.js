/**
 * Created by MarosGrajcar on 6/27/2019.
 */

({
   doInit : function(component, event, helper) {
        helper.loadLogEntries(component);
    },

    saveSection : function(component, event, helper) {
        helper.validateBeforeSave(component, event, helper);
    },

    enableSaveButton : function(component, event, helper) {
        helper.enableOrDisableSaveButton(component, event, helper);
    },

    cancel : function(component, event, helper) {
        // Close modal
        component.find("overlayLib").notifyClose();
    },
});