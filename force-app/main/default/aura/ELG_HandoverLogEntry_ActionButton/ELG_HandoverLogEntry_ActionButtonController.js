/**
 * Created by MarcelBrimus on 28/06/2019.
 */
({
    // Handover section buttons
    showGeneric : function(component, evt, helper) {
        var modalBody;
        $A.createComponent("c:ELG_ShiftHandover_HO_GenericSection",
            {sectionName:component.get('v.question.Name'),
            shiftAssignement:component.get('v.shiftAssignement')},
            function(content, status) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: component.get('v.question.Name'),
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },
})