({

	/**
	 * Load picklist data based on provided ObjectAPIName and FieldAPIName.
	 */
	getPicklistValues : function(component, event, helper) {
		var params = event.getParam('arguments');
        helper.loadData(component, event, params.objectAPIName, params.fieldAPIName);
	},

})