({

	debugOn : function() {
		return false;
	},

	/**
	 * Retrieve picklist data from server based on provided parameters
	 * @param  {[type]} objectName [SObject API Name where picklist field is]
	 * @param  {[type]} fieldName  [Field API Name for which we want to get picklsit values]
	 */
	loadData : function(component, event, objectName, fieldName) {
		var action = component.get("c.getPicklist");
		action.setParams({"objectName" : objectName,
						"fieldName" : fieldName});
		action.setCallback(this, function(response) {
			var state = response.getState();
        	if(state === "SUCCESS") {
        		var picklist = response.getReturnValue();
    			if(this.debugOn()) console.log(objectName + "." + fieldName + ": Loaded from server.");
        		this.returnData(event, picklist, null);
        	} else if(state === "ERROR") {
        		this.returnData(event, null, response.getError());
        	}
		});
		$A.enqueueAction(action);
	},
    
    /**
     * Return picklist data or error to callback function for parent component
     * to retrieve values.
     */
	returnData : function(event, data, error) {
		var params = event.getParam("arguments");
		if(error) console.log("Error" + error);
		params.callback(error, data);
	},
    
})