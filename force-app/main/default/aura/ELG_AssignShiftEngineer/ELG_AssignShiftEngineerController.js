/**
 * Created by MarcelBrimus on 09/07/2019.
 */

({
    init: function (component, evt, helper) {
        component.set("v.currentUserId", $A.get("$SObjectType.CurrentUser.Id"));
        helper.loadShiftForShiftEngineer(component, evt, helper);
    },

    handleSaveSuccess : function(component, event, helper) {
        var assignEngineer = component.getEvent('assignEngineer');
        assignEngineer.fire();
        component.find("overlayLib").notifyClose();
    },

    onSaveError : function(component, event, helper) {
        helper.displayToast(component, 'error', 'Error when saving Steam Shift Engineer');
    },

    handleFormLoad : function(component, event, helper) {
        component.set("v.disableButton", false);
    },

    onNo : function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },

    submitSelection : function(component, event, helper) {
      helper.saveSelection(component, event, helper);
    },

});