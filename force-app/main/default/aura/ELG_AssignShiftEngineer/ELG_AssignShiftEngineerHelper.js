/**
 * Created by MarcelBrimus on 09/07/2019.
 */

({
    // Load data
    loadShiftForShiftEngineer: function(component, evt, helper) {
        console.log('Data ' +  component.get('v.userFacilityId'));
        component.set("v.loading", true);
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getSteamShiftWithMissingShiftEngineer",
            {"amuId": component.get('v.userFacilityId')},
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess : function(component, response, helper){
        if(response.resultObjects){
            if(response.resultObjects[1] == 1) {
                var responseNew = response.resultObjects[0];
                component.set("v.shiftId", responseNew[0].Id);
                component.set("v.shiftEngineerShifts", responseNew);
                component.set("v.disableButton", false);
                component.set("v.loading", false);
            } else {
                var responseNew = response.resultObjects[0];
                component.set("v.shiftEngineerShifts", responseNew);
                component.set("v.moreShiftsForShiftEngineer", true);
                component.set("v.disableButton", false);
                component.set("v.loading", false);
            }
        } else {
            component.set("v.noShiftForEngineer", true);
            component.set("v.loading", false);
        }
    },

    handleError : function(component, response, helper){
        helper.displayToast(component, 'error', 'Error Loading data for Missing Steam Shift Engineer')
        component.set("v.loading", false);
        console.log('err ' + JSON.stringify(response));
    },

    //Save Section
    saveSelection : function(component, event, helper) {
        var sectionWithQuestions= component.find('checkboxSection');
        var checked = true;
        var listOfIds = [];

        if (sectionWithQuestions.length) {
            sectionWithQuestions.forEach(function(section) {
                if (section.get("v.checked")) {
                    listOfIds.push(section.get("v.value"));
                }
            });
        } else {
            var singleSection = component.find('checkboxSection');
            listOfIds.push(singleSection.get("v.value"));
        }

        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.saveShiftEngineer",
            {"shiftIds": listOfIds,
            "currentUser": component.get("v.currentUserId")},
            this.handleSaveSuccess,
            this.handleError
        );
    },

    handleSaveSuccess : function(component, event, helper) {
        var assignEngineer = component.getEvent('assignEngineer');
        assignEngineer.fire();
        component.find("overlayLib").notifyClose();
    },

    // HELPERS
    displayToast : function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 10000
        });
        toastEvent.fire();
    },

});