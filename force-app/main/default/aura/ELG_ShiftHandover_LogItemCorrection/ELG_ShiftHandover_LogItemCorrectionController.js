/**
 * Created by MarcelBrimus on 30/07/2019.
 */

({
    init: function (component, evt, helper) {
       // Set current user to prepopulate User lookup on settings
       component.set("v.currentUserId", $A.get("$SObjectType.CurrentUser.Id"));
    },

    handleSaveSuccess : function(component, event, helper) {
        var onLogCorrected = component.getEvent('onLogCorrected');
        onLogCorrected.fire();
        component.find("overlayLib").notifyClose();
    },

    handleFormLoad : function(component, event, helper) {
        component.set("v.disableButton", false);
    },

    onCancel: function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
});