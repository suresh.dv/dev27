/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Stores original Display Mode to handle specific case after save from Edit
    */
    saveOriginalMode : function(component) {
        component.set("v.originalDisplayMode", component.get('v.displayMode'));
    },

    /**
    loads EPD Form for current Work Order Activity.
    */
    loadForm : function(component) {
        var helper = this;
        var callParams = helper.getCallParams(component);
        if(!component.get("v.loading")) {
            component.set("v.actionInProgress", true);
        }
        component.find("apexService")
                .callApexWithAllCallbacks(
                        component,
                        helper,
                        callParams.methodToCall,
                        callParams.payload,
                        this.handleLoadFormSuccess,
                        this.handleResponseError,
                        this.handleServerError
                );
    },

    /**
    get params for Load Form action
    */
    getCallParams : function(component) {
        var helper = this;
        var params;
        if(component.get("v.displayMode") === "WOA") {
            params = helper.getParamsForWOAMode(component);
        } else {
            params = helper.getParamsForOtherModes(component);
        }
        return params;
    },

    /**
    get params for WOA Form mode
    */
    getParamsForWOAMode : function(component) {
        return {
            methodToCall : "c.loadForm",
            payload : {"workOrderActivityId" : component.get("v.recordId")}
        };
    },

    /**
    get params for other Form modes
    */
    getParamsForOtherModes : function(component) {
        return {
            methodToCall : "c.getEPDRecordWithSharing",
            payload : {"epdId" : component.get("v.recordId")}
        };
    },

    /**
    Load success handler. Sets result of load into formStructure attribute and stops loading.
    */
    handleLoadFormSuccess : function(component, requestResult, helper) {
        component.set("v.formStructure", requestResult.formStructure);
        helper.stopLoading(component, false);
        if((component.get("v.displayMode") === "VIEW" || component.get("v.displayMode") === "EDIT") && !requestResult.formStructure.epd.submitted) {
            if(component.get("v.displayMode") === "EDIT") {
                component.set("v.displayMode", "VIEW");
            }
            helper.showToast('', 'warning', 'This form has not yet been submitted. Edit option is disabled.');
        }
    },

    /**
    Load error handler. Sets error message to errorMessage attribute and stops loading.
    */
    handleResponseError : function(component, requestResult, helper) {
        component.set("v.errorMessage", helper.getError(requestResult));
        helper.stopLoading(component, true);
    },


    /**
    Load server error handler. Sets error message to errorMessage attribute and stops loading.
    */
    handleServerError : function(component, requestResult, helper) {
        component.set("v.errorMessage", helper.getErrorMessage(requestResult));
        helper.stopLoading(component, true);
    },

    /**
    Sets required attributes to stop loading.
    */
    stopLoading : function(component, showError) {
        component.set("v.showError", showError);
        component.set("v.loading", false);
        component.set("v.actionInProgress", false);
    },

    /**
    Handle save event action. Checks submit param of event based on that it will call specific APEX method.
    */
    handleSave : function(component, event) {
        var helper = this;
        component.set("v.actionInProgress", true);
        var doSubmit = event.getParam("submit");
        component.set("v.doSubmit", doSubmit);
        var formStructure = component.get("v.formStructure");
        var payload = {
            formStructure : JSON.stringify(formStructure)
        };

        //TODO fire event on success submit for parent to catch
        //TODO show "Are you sure?" popup.

        console.log(JSON.stringify(component.get("v.formStructure")));
        var methodToCall = doSubmit ? "c.submitForm" : "c.saveForm";
        methodToCall += (component.get("v.displayMode") === "EDIT") ? "WithSharing" : ""; //enabling sharing
        this.saveRecords(component, methodToCall, payload);
        if(component.get("v.displayMode") === "EDIT") {
            if(component.get('v.originalDisplayMode') === "EDIT") {
                helper.redirectToView(component);
            } else {
                component.set("v.displayMode", 'VIEW');
            }
        }
    },

    /**
    Redirect to record page
    */
    redirectToView : function(component) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          "recordId": component.get("v.recordId"),
          "slideDevName": "detail",
          "isredirect" : true
        });
        navEvt.fire();
    },

    /**
    This function does an actual call to APEX method based on provided params.
    Also starts spinner as indication of loading.
    */
    saveRecords : function(component, methodToCall, payload) {
        var helper = this;
        component.set("v.actionInProgress", true);
        component.find("apexService")
                .callApexWithAllCallbacks(
                        component,
                        helper,
                        methodToCall,
                        payload,
                        this.handleSaveSuccess,
                        this.handleSaveError,
                        this.handleSaveServerError
                );
    },

    /**
    Save success handler. Disables spinner and show toast message.
    */
    handleSaveSuccess : function(component, requestResult, helper) {
        component.set("v.actionInProgress", false);
        helper.showToast("Success!", "success", "The form has been saved successfully.");
        if(component.get("v.doSubmit")){
            var refreshWorkflowEngine = component.getEvent("workflowRefreshEvent");
            refreshWorkflowEngine.fire();
        }
    },

    /**
    Save error handler. Disables spinner and show toast message.
    */
    handleSaveError : function(component, requestResult, helper) {
        component.set("v.actionInProgress", false);
        helper.showToast("Error!", "error", helper.getError(requestResult));
    },

    /**
    Save server error handler. Disables spinner and show toast message.
    */
    handleSaveServerError : function(component, requestResult, helper) {
        component.set("v.actionInProgress", false);
        helper.showToast("Error!", "error", helper.getErrorMessage(requestResult));
    },

    /**
    Extract response error message for display.
    */
    getError : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0];
        }
        return 'Unknown error';
    },

    /**
    Extract server error message for display.
    */
    getErrorMessage : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0].message;
        }
        return 'Unknown error';
    },

    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },

});