/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.saveOriginalMode(component);
        helper.loadForm(component);
    },

    handleSave : function(component, event, helper) {
        helper.handleSave(component, event);
        event.stopPropagation();
    },

});