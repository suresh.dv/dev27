/**
 * Created by MarosGrajcar on 11/7/2019.
 */

({
    doInit : function(component, event, helper) {
        var listOfIds = component.get('v.handoverIds');
        component.set('v.currentHandover', listOfIds[0]);
        component.set('v.selectedPage', 1);
        component.set('v.totalPages', listOfIds.length);
    },

    selectFirstHandover : function(component, event, helper) {
        var listOfIds = component.get('v.handoverIds');
        component.set('v.currentHandover', listOfIds[0]);
        component.set('v.currentPosition', 0);
        component.set('v.selectedPage', 1);
        helper.checkButtons(component, event, helper);
    },

    selectPreviousHandover : function(component, event, helper) {
        var listOfIds = component.get('v.handoverIds');
        var position = component.get('v.currentPosition');
        if (position != 0) {
            position--;

            component.set('v.selectedPage', position+1);
            component.set('v.currentPosition', position);
            component.set('v.currentHandover', listOfIds[position]);
        }
        helper.checkButtons(component, event, helper);
    },

    selectNextHandover : function(component, event, helper) {
        var listOfIds = component.get('v.handoverIds');
        var position = component.get('v.currentPosition');
        if ((position+1) != listOfIds.length) {
            position++;

            component.set('v.selectedPage', position+1);
            component.set('v.currentPosition', position);
            component.set('v.currentHandover', listOfIds[position]);
        }
        helper.checkButtons(component, event, helper);
    },

    selectLastHandover : function(component, event, helper) {
        var listOfIds = component.get('v.handoverIds');
        var handoverQuantity = listOfIds.length;
        component.set('v.currentHandover', listOfIds[handoverQuantity-1]);
        component.set('v.currentPosition', handoverQuantity-1);
        component.set('v.selectedPage', handoverQuantity);
        helper.checkButtons(component, event, helper);
    },

    /* Open Modal For Save and Select trainee */
    openMassUpdate: function(component, evt, helper) {
        var modalBody;
        $A.createComponent("c:ELG_HandoverForm_MassUpdate", {
            handoverIds:component.get('v.handoverIds')
        },
        function(content, status, errorMessage) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: "Mass Handover Review Update",
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },

    componentReload : function(component, event, helper) {
        component.set('v.reloadComponent', false);
        component.set('v.reloadComponent', true);
    },

    noReviewerSet : function(component, event, helper) {
        var cannotReview = event.getParam('userCannotReview');
        component.set('v.userCannotReview', cannotReview);
    },

});