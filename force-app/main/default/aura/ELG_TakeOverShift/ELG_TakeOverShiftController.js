/**
 * Created by MarcelBrimus on 19/06/2019.
 */
({
    init: function (component, evt, helper) {
        component.set("v.currentUserId", $A.get("$SObjectType.CurrentUser.Id"));
        helper.loadAvailableShifts(component);
    },

    onShiftsForTakeOverChange: function (component, evt, helper) {
        helper.setFirstItemAsDefault(component);
    },

    handleSaveSuccess : function(component, event, helper) {
        var takenOverShift = component.getEvent('takenOverShift');
        takenOverShift.fire();
        component.find("overlayLib").notifyClose();
    },

    handleFormLoad : function(component, event, helper) {
        component.set("v.disableButton", false);
    },

    onCancel: function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
})