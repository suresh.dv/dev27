/**
 * Created by MarcelBrimus on 19/06/2019.
 */
({
    // Load data
    loadAvailableShifts: function(component) {
        var apexService = component.find("apexService");
//        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getBaseShiftHandoverData",
            {},
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess: function(component, response, helper){
        var responseNew = response.resultObjects[0];
        var availablePostsKey = 'AVAILABLEPOSTS';
        var incomingHandoverKey = 'INCOMINGHANDOVER';

        if(responseNew[incomingHandoverKey]) {
            component.set('v.incomingHandover', true);
        }

        if(responseNew[availablePostsKey]){
           helper.filterOutPostsWithNoShift(component, responseNew[availablePostsKey]);
        }
        component.set("v.disableButton", false);
    },

    handleError: function(component, response, helper){
//        helper.hideSpinner(component);
    },

    // Only allow take over posts where there is at least one active shift
    // Filter out posts with no shift, shifts with wrong status or shifts where
    // user is primary so that users cannot take them over
    filterOutPostsWithNoShift: function (component, availablePosts){
        var shiftsAvailableForTakeOver = [];

        availablePosts.forEach((item) => {
            if(item.Shift_Assignements__r){
                if( item.Shift_Assignements__r[0].Shift_Status__c === 'New'
                && item.Shift_Assignements__r[0].Primary__c !== component.get('v.currentUserId')){
                     shiftsAvailableForTakeOver.push(item.Shift_Assignements__r[0]);
                }
            }
        });
        component.set('v.shiftsForTakeOver', shiftsAvailableForTakeOver);
    },

    setFirstItemAsDefault: function (component){
        if(component.get('v.shiftsForTakeOver').length > 0){
             component.set('v.selectedShift', component.get('v.shiftsForTakeOver')[0].Id);
        }
    },

    // HELPERS
    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 10000
        });
        toastEvent.fire();
    },

//    showSpinner : function(component) {
//        component.set("v.toggleSpinner", true);
//    },
//
//    hideSpinner : function(component) {
//        component.set("v.toggleSpinner", false);
//    },
})