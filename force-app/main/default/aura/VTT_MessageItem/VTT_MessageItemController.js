({
    init: function (component, evt, helper) {
        component.set('v.style', 'slds-notify slds-notify_alert slds-theme_alert-texture '
         + component.get('v.item.severity'));
    },

    // Navigate to record
    onLinkClick: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.item.entityId")
        });
        navEvt.fire();
    },
});