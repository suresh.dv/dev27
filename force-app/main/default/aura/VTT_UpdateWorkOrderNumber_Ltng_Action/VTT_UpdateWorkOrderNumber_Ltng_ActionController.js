/**
 * Created by Maros Grajcar on 2/22/2019.
 */
({

    prefillWONumber : function(component, event, helper) {
        helper.autoPrefillWONumber(component);
    },

    submit : function(component, event, helper) {
        helper.verifyAndSubmit(component, event, helper);
    },

})