/**
 * Created by Maros Grajcar on 2/21/2019.
 */
({
    autoPrefillWONumber : function(component) {
        component.set("v.workOrderNumber", component.get("v.workOrder.Work_Order_Number__c"));
    },

    showSpinner : function(component) {
            component.set("v.loading", true);
    },

    hideSpinner : function(component) {
            component.set("v.loading", false);
    },

    verifyAndSubmit : function(component, helper) {
        var regex=/^[0-9]+$/;
        var workOrderNumber = component.get("v.workOrderNumber");
        var apexService = component.find("apexService");

        this.showSpinner(component);
        if(!regex.test(workOrderNumber)) {
            component.set("v.returnMessage", "Invalid Entry!");
            this.hideSpinner(component);
        } else {
            apexService.callApexWithHelper(component,
                                this,
                                "c.updateWorkOrder_Ltng",
                                { "wellServicingFormId" : component.get("v.recordId"),
                                "notificationServicingFormId" : component.get("v.workOrder").HOG_Service_Request_Notification_Form__c,
                                "workOrderNumber" : component.get("v.workOrderNumber") },
                                this.handleSuccess);

            }
    },

    handleSuccess: function(component, response, helper){
            var responseMessage = response.resultObjects[0];
            var workOrderNumber = component.get("v.workOrderNumber");
            component.set('v.returnMessage', responseMessage);
            if(responseMessage === workOrderNumber) {
                $A.get("e.force:closeQuickAction").fire();
                $A.get("e.force:refreshView").fire();
            }
            helper.hideSpinner(component);
        },


})