({

	/**
	 * Load alert record
	 */
	doInit : function(component, event, helper) {
    	helper.loadAlert(component);
	},

	/**
	 * Reload Alert record after it was changed.
	 */
	handleAlertChange : function(component, event, helper) {
		helper.loadAlert(component);
	},
    
})