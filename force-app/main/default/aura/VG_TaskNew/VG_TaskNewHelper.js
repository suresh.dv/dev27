({
    
    /**
     * Load VG Alert record to use as template for creating Task from VG_Task.cmp.
     * Notify VG_Task cmp after it is successfully loaded.
     */
	loadAlert : function(component) {
        component.find("apexService").callApex(component, "c.getAlert_v1", { alertId : component.get("v.recordId") }, this.alertLoadSuccess);
	},

    alertLoadSuccess : function(component, requestResult) {
        component.set("v.alert", requestResult.resultObjects[0]);
        var childCmp = component.find("TaskComponent");
        childCmp.alertLoaded();
    },

    /**
     * Show toast message.
     */
    fireNotification : function(toastTitle, toastMessage, toastType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: toastTitle,
            message: toastMessage,
            type: toastType
        });
        toastEvent.fire();
    },
    
})