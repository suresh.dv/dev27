/**
 * Created by MarcelBrimus on 27/05/2019.
 */
({
    init: function (component) {
        // Set current user to prepopulate User lookup on settings
        component.set("v.currentUserId", $A.get("$SObjectType.CurrentUser.Id"));
    },

    handleSuccess : function(component, event, helper) {
        // Save Id so that when user opens modal after first save it will get populated
        var savedRecord = event.getParams().response;

        var id = savedRecord.id;
        var saveEvent = component.getEvent('settingsSaved');
        saveEvent.setParams({'settingsId': id });
        saveEvent.fire();

        // Close modal
        component.find("overlayLib").notifyClose();
    },

    handleLoad : function(component, event, helper) {
        component.set("v.toggleSpinner", false);
    }
})