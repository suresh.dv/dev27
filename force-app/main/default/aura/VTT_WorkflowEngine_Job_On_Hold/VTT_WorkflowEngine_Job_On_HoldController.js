/**
 * Created by MarcelBrimus on 15/10/2019.
 */

({
    viewMode: function(component, evt, helper) {
        helper.goToView(component);
    },

    onReasonChange: function(component, event, helper){
        var activity = component.get('v.workOrderActivity');
        // As per logic in original workflow engine we are nullyfing Estimated_Rescheduled_Date__c
        activity.Estimated_Rescheduled_Date__c = null;
        component.set('v.workOrderActivity', activity);
    },

    activityLoaded : function(component, event, helper){
        var activity = component.get('v.workOrderActivity');

        // As per logic in original workflow engine we are nullyfing Estimated_Rescheduled_Date__c
        activity.Estimated_Rescheduled_Date__c = null;
        component.set('v.workOrderActivity', activity)

        if(activity){
            if(activity.Operating_Field_AMU__c){
                component.set('v.isThermal', activity.Operating_Field_AMU__r.Is_Thermal__c);
            } else if(activity.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c) {
                component.set('v.isThermal', activity.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c);
            }

            if(component.get('v.isThermal')){
                // Call to check if there is already a closeout checklist and if user is permit holder
                helper.getPermitHolderData(component, event, helper);
                console.log('its thermal call checklist')
            }
        }
    },

    handleSave : function(component, event, helper) {
        helper.handleSave(component, event, helper);
        event.stopPropagation();
    }
});