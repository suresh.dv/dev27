/**
 * Created by MarcelBrimus on 15/02/2019.
 */
({
    doCancelProgram: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);

        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.cancelScheduledRecurringService_ltng",
            { "serviceRequestId" : component.get("v.recordId") },
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess: function(component, response, helper){
        var responseString = response.resultObjects[0];
        if(responseString == '0'){
            component.set('v.messageFromWebservice', 'There are no Active programs');
        } else {
            $A.get("e.force:refreshView").fire();
            $A.get("e.force:closeQuickAction").fire();
        }
        helper.hideSpinner(component);
    },

    handleError: function(component, response, helper){
        var responseString = response.resultObjects[0];
        component.set('v.messageFromWebservice', responseString);
        helper.hideSpinner(component);
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },
})