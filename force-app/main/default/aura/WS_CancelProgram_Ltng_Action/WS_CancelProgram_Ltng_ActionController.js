/**
 * Created by MarcelBrimus on 15/02/2019.
 */
({
    onYes : function(component, event, helper) {
        helper.doCancelProgram(component);
    },

    onCancel : function(){
        $A.get("e.force:closeQuickAction").fire();
    }
})