/**
 * Created by MarosGrajcar on 7/31/2019.
 */

({
    changeReasonIsEmpty : function(component) {
        var inputCmp = component.find("userInput");
        var isFieldFilled = component.get('v.fieldValue');
        var isInputValid = true;

        if (!isFieldFilled) {
            inputCmp.setCustomValidity("Please, write a reason why are you changing operator.");
            inputCmp.reportValidity();
            isInputValid = false;
        }

        return isInputValid;
    },

    selectedOperatorIsEmpty : function(component) {
        var incomingOperator;
        component.find("incomingOperatorValue").isValid(function(unusedValue, isValid) {
            incomingOperator = isValid;
        });
        var isOperatorEmpty = component.get('v.incomingOperator');
        var isInputValid = true;

        if (!incomingOperator) {
           isInputValid = false;
        }

        return isInputValid;
    },




});