/**
 * Created by MarosGrajcar on 11/6/2019.
 */

({
    handlePassedHandovers : function(component, event, helper) {
        console.log('Open handovers');
        var handoverIds = [];
        var selectedHandovers = event.getParam('selectedDatatableHandovers');

        if (selectedHandovers.length > 1) {
            selectedHandovers.forEach(function(handover) {
                handoverIds.push(handover.handId);
             });
        } else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "message": "Please, select at least two Handovers to proceed."
                });
            toastEvent.fire();
        }

        component.set('v.handoverIds', handoverIds);
        var evt = $A.get("e.force:navigateToComponent");
         evt.setParams({
             componentDef : "c:ELG_HandoverForm_ListWrapper",
             isredirect : "true",
             componentAttributes: {
                 handoverIds : component.get("v.handoverIds")
             }
         });
         evt.fire();

    },

    handlePassedSummaries : function(component, event, helper) {
        console.log('Open summaries');
        var summaryIds = [];
        var selectedSummaries = event.getParam('selectedDatatableSummaries');

        if (selectedSummaries.length > 1) {
            selectedSummaries.forEach(function(summary) {
                console.log('Summary: ' +JSON.stringify(summary));
                summaryIds.push(summary.summaryId);
             });
        } else {
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "",
                    "message": "Please, select at least two Shift Summaries to proceed."
                });
            toastEvent.fire();
        }

        component.set('v.summaryIds', summaryIds);
        console.log('IDS: ' +summaryIds);
        var evt = $A.get("e.force:navigateToComponent");
         evt.setParams({
             componentDef : "c:ELG_ShiftSummary_ListWrapper",
             isredirect : "true",
             componentAttributes: {
                 summaryIds : component.get("v.summaryIds")
             }
         });
         evt.fire();

    },

});