/**
 * Created by MarosGrajcar on 10/18/2019.
 */

({
    init : function(component, event, helper) {
        helper.loadAMUPosts(component, event, helper);
    },

    handleSubmit : function(component, event, helper) {
        helper.createTask(component, event, helper);
    },

    handleClose : function(component, event, helper) {
        // Close modal
        component.find("overlayLib").notifyClose();
    },

    handlePostChange : function(component, event, helper) {
        var selectedPost = event.getParam("value");
        component.set('v.selectedPostId', selectedPost);
    },

    handleAMUChange : function(component, event, helper) {
        helper.loadAMUPosts(component, event, helper);
    }

});