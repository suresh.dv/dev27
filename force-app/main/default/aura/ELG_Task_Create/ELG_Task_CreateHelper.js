/**
 * Created by MarosGrajcar on 10/18/2019.
 */

({
    loadAMUPosts : function(component, event, helper) {
        component.set('v.disableButton', true);
        var apexService = component.find("apexService");
        //this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getAMUPosts",
            {amuId : component.get('v.amuId')},
            this.handleLoad,
            this.handleError
        );
    },

    handleLoad : function(component, requestResult, helper) {
        console.log('SUCCESS');
        component.set("v.amuPosts", requestResult.resultObjects[0]);
        component.set('v.disableButton', false);
    },

    handleError : function(component, requestResult, helper) {
        console.log('ERROR ' +requestResult.errors[0]);
        component.set('v.hasError', true);
        component.set("v.errorMsg", requestResult.errors[0]);
        component.set('v.disableButton', false);
        component.set('v.buttonName', 'Submit Task');
    },

    createTask : function(component, event, helper) {
        var selectedPost = component.get('v.selectedPostId');
        if (selectedPost && helper.validateShortDescription(component) && helper.validateTaskDescription(component)) {
            component.set('v.buttonName', 'Processing');
            component.set('v.disableButton', true);
            var userId = $A.get("$SObjectType.CurrentUser.Id");
            var apexService = component.find("apexService");
            apexService.callApexWithErrorCallbackWithHelper(
                component,
                this,
                "c.createPostTask",
                {postId : component.get('v.selectedPostId'),
                shortDescription : component.get('v.shortDescription'),
                taskDescription : component.get('v.taskDescription')},
                this.handleSaveSuccess,
                this.handleError
            );
        }
    },

    handleSaveSuccess : function(component, event, helper) {
        console.log('SUCESS');
        component.set('v.disableButton', false);
        component.set('v.buttonName', 'Submit Task');
         // Close modal
        component.find("overlayLib").notifyClose();
        $A.get('e.force:refreshView').fire();
    },

    validateShortDescription : function(component, event, helper) {
        var inputCmp = component.find("validateShort");
        var isFieldFilled = component.get('v.shortDescription');
        var isInputValid = true;
        var shortDescriptionMaxCharacters = 80;

        if(!isFieldFilled) {
           inputCmp.setCustomValidity("Please, fill the Short Description.");
           inputCmp.reportValidity();
           isInputValid = false;
        } else if(isFieldFilled.length > shortDescriptionMaxCharacters) {
           shortDescriptionMaxCharacters = isFieldFilled.length - shortDescriptionMaxCharacters;
           inputCmp.setCustomValidity("Short Description cannot exceed more than 80 characters. You are above " +shortDescriptionMaxCharacters+ " characters limit to proceed.");
           inputCmp.reportValidity();
           isInputValid = false;
        }

        return isInputValid;
    },

    validateTaskDescription : function(component, event, helper) {
        var inputCmp = component.find("validateTask");
        var isFieldFilled = component.get('v.taskDescription');
        var isInputValid = true;
        var taskDescriptionMaxCharacters = 30000;

        if(!isFieldFilled) {
           inputCmp.setCustomValidity("Please, fill the Task Description.");
           inputCmp.reportValidity();
           isInputValid = false;
        } else if(isFieldFilled.length > taskDescriptionMaxCharacters) {
           taskDescriptionMaxCharacters = isFieldFilled.length - taskDescriptionMaxCharacters;
           inputCmp.setCustomValidity("Task Description cannot exceed more than 30000 characters. You are above " +taskDescriptionMaxCharacters+ " characters limit to proceed.");
           inputCmp.reportValidity();
           isInputValid = false;
        }

        return isInputValid;
    },
});