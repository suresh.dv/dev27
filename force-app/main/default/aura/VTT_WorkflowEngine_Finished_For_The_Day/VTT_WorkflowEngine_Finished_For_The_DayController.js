/**
 * Created by MarcelBrimus on 11/10/2019.
 */

({
    viewMode: function(component, evt, helper) {
        helper.goToView(component);
    },

    handleSave : function(component, event, helper) {
        helper.handleSave(component, event, helper);
        event.stopPropagation();
    }
});