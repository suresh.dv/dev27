/**
 * Created by MarcelBrimus on 28/10/2019.
 */

({
    viewMode: function(component, evt, helper) {
        helper.goToView(component);
    },

    activityLoaded : function(component, event, helper){
        helper.getDataForJobComplete(component, event, helper);
        var activity = component.get('v.workOrderActivity');

        if(activity){
            if(activity.Maintenance_Work_Order__r.Equipment__c && activity.Maintenance_Work_Order__r.Equipment__r.Catalogue_Code__c){
                component.set('v.isEquipmentWorkOrder', true);
            }
            if(activity.Operating_Field_AMU__c){
                component.set('v.isThermal', activity.Operating_Field_AMU__r.Is_Thermal__c);
            } else if(activity.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__c) {
                component.set('v.isThermal', activity.Maintenance_Work_Order__r.Operating_Field_AMU_Lookup__r.Is_Thermal__c);
            }
        }
    },

    onCloseOutFormSuccess: function(component, event, helper){
        component.set('v.actionInProgress', false);
        helper.showToast("Success!", "Success", 'Close-Out Checklist save successful');
        component.set('v.jobCompleteStep', 'JOBCOMPLETE')
    },

    handleSave: function(component, event, helper){
        helper.handleSave(component, event, helper, false);
    },

    handleCloseOutChecklistLoad: function(component, event, helper){
        component.set('v.actionInProgress', false);
    },

    handleSaveWithReschedule: function(component, event, helper){
        helper.handleSave(component, event, helper, true);
    },

    distributeTime: function(component,event, helper){
        helper.doDistributeTime(component, event, helper);
    },

    checkAll: function(component, event, helper){
        helper.doCheckAll(component, event, helper);
    },

    onMassCompleteDataChange: function(component, event, helper){

        if(!event.getParam("isChecked")){
            component.set('v.runningTally',
            (Number(component.get('v.runningTally')) + Number(event.getParam("durationValue"))))

            component.set('v.timeForDistribution',
            (Number(component.get('v.timeForDistribution')) + Number(event.getParam("durationValue"))))

            var numberOfSelectedActivities = 0;
            component.get('v.availableActivitiesToComplete').forEach(function(item) {
                if(item.markToComplete){
                    numberOfSelectedActivities += 1;
                }
            });
            component.set('v.numberOfSelectedActivities', numberOfSelectedActivities);
        } else {
            var availableDuration = Number(component.get('v.runningTallySnapShot'));
            var totalDurationSoFar = 0;
            var numberOfSelectedActivities = 0;
            component.get('v.availableActivitiesToComplete').forEach(function(item) {
                if(item.markToComplete){
                    totalDurationSoFar = Number(totalDurationSoFar) + Number(item.userDurationInput);
                    numberOfSelectedActivities += 1;
                }
            });

            var newTally = availableDuration - totalDurationSoFar;
            component.set('v.runningTally', newTally);
            component.set('v.timeForDistribution', newTally - 1);
            component.set('v.numberOfSelectedActivities', numberOfSelectedActivities);

            if((newTally - 1) < 0){
                component.set('v.massCompleteErrorMessage', 'You have spent more time that on selected activities then you have available, please subtract minutes from one of the selected activities')
            } else {
                component.set('v.massCompleteErrorMessage', null);
            }
        }
    }
});