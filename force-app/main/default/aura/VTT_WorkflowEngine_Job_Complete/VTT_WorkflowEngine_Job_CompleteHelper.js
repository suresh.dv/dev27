/**
 * Created by MarcelBrimus on 28/10/2019.
 */

({
    handleSave : function(component, helper, event, isReschedule) {
        var helper = this;

        // Validation
        var isPDCValid = helper.isPDCValid(component);
        var isMassCompleteValid = helper.isMassCompleteValid(component);

        if(isMassCompleteValid && isPDCValid && helper.validateForm(component, helper, event)){
            // This caused issues when serializing data on Save, but its not needed so we can null this
            var activitiesToComplete = component.get('v.availableActivitiesToComplete');
            if(activitiesToComplete && activitiesToComplete.length > 0 ){
                for(var i = 0; i < activitiesToComplete.length; i++){
                    activitiesToComplete[i].activityRecord.Work_Order_Activity_Assignments__r = null;
                }
            }

            var isOtherTradesmanStillWorking = false;
            if(component.get('v.otherWorkingTradesman') && component.get('v.otherWorkingTradesman').length > 0){
                isOtherTradesmanStillWorking = true;
            }

            var payload = {
                actionName : $A.get("{!$Label.c.VTT_LTNG_Job_Complete_Action}"),
                actionComment : component.get('v.actionComment'),
                currentLatitude : component.get('v.latitude'),
                currentLongitude : component.get('v.longitude'),
                activitiesToComplete : activitiesToComplete,
                partItem: component.get('v.partItem'),
                damageItem: component.get('v.damageItem'),
                damageDescription : component.get('v.damageDescription'),
                causeItem: component.get('v.causeItem'),
                causeDescription: component.get('v.causeDescription'),
                isOtherTradesmanStillWorking: isOtherTradesmanStillWorking,
                runningTally: component.get('v.runningTally'),
                requestReschedule : isReschedule
            }

            var requestBody = {
                activity : component.get('v.workOrderActivity'),
                tradesman : component.get('v.tradesman'),
                request : JSON.stringify(payload)
            }

            component.set("v.actionInProgress", true);
            component.find("apexService").callApexWithAllCallbacks(
                component,
                helper,
                "c.saveAction",
                requestBody,
                helper.handleSaveSuccess,
                helper.handleResponseError,
                helper.handleServerError
            );
        }
    },

    handleSaveSuccess : function(component, requestResult, helper) {
        helper.refreshWorkflow(component);
        helper.showToast("Success!", "success", "Job Complete Successful.");
    },

    handleResponseError : function(component, requestResult, helper) {
        console.log('RESPONSE SAVE ERR' + JSON.stringify(requestResult))
        if(requestResult.resultObjects && !requestResult.resultObjects[0]){
           helper.needRefresh(component);
        } else {
           helper.showToast("Error!", "error", helper.getError(requestResult));
           helper.goToView(component);
        }
    },

    handleServerError : function(component, requestResult, helper) {
        helper.showToast("Error!", "error", helper.getErrorMessage(requestResult));
        helper.goToView(component);
    },

    getDataForJobComplete : function(component, event, helper) {
        component.set("v.actionInProgress", true);
        var apexService = component.find("apexService");

        apexService.callApexWithErrorCallbackWithHelper(
            component,
            helper,
            "c.getDataForJobComplete",
            { "activity" : component.get('v.workOrderActivity'),
            "tradesman" : component.get('v.tradesman')},
            this.handleDataForJobCompleteSuccess,
            this.handleDataForJobCompleteError
        );
    },

    handleDataForJobCompleteSuccess : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        if(response.resultObjects[0]){
            component.set('v.otherWorkingTradesman',response.resultObjects[0]['otherWorkingTradesman']);
            component.set('v.showPartsDamageCause',response.resultObjects[0]['showPartsDamageCause']);
            component.set('v.availableActivitiesToComplete',response.resultObjects[0]['availableActivitiesToComplete']);
            component.set('v.isPermitHolder',response.resultObjects[0]['isPermitHolder']);
            component.set('v.checkListID',response.resultObjects[0]['closeOutChecklistId']);
            component.set('v.runningTally' , response.resultObjects[0]['runningTally']);
            component.set('v.runningTallySnapShot', response.resultObjects[0]['runningTally'])
            component.set('v.availableCause',response.resultObjects[0]['causesSelection']);
            component.set('v.availableParts',response.resultObjects[0]['partsSelection']);

            if(response.resultObjects[0]['runningTally'] > 0){
                component.set('v.timeForDistribution',(response.resultObjects[0]['runningTally']-1));
            }
            helper.showJobCompleteSection(component);
        }
    },

    showJobCompleteSection: function(component){
        if(component.get('v.isPermitHolder') && component.get('v.isThermal')){
            component.set('v.jobCompleteStep', 'CLOSEOUT');
        } else {
            component.set('v.jobCompleteStep', 'JOBCOMPLETE');
        }
    },

    handleDataForJobCompleteError : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        helper.showToast("Error!", "error", helper.getError(response));
    },

    // If we need to change time to Hh:mm:ss from seconds uncomment this
    doDistributeTime : function(component, event, helper) {
        var availableTime = component.get('v.runningTally');
        var numberOfActivitiesToDistributeTime = 0;
        var totalUserSelectedDuration = 0;
        var timeForEachActivity = 0;
        var newData = [];
        component.get('v.availableActivitiesToComplete').forEach(function(item) {
            // Split time between these
            // If activity is marked and user did not already added time to it
            if(item.markToComplete && item.userDurationInput == 0){
                numberOfActivitiesToDistributeTime += 1;
            }
            totalUserSelectedDuration =  Number(totalUserSelectedDuration) + Number(item.userDurationInput);
        });

        if(numberOfActivitiesToDistributeTime > 0 && availableTime >= numberOfActivitiesToDistributeTime )  {
            // Since we need to leave something for original activity we will add one into counter
            timeForEachActivity = (component.get('v.timeForDistribution') - totalUserSelectedDuration) / (numberOfActivitiesToDistributeTime + 1);
            if(timeForEachActivity >= 1){
                component.get('v.availableActivitiesToComplete').forEach(function(item) {
                    if(item.markToComplete && item.userDurationInput == 0){
                        item.userDurationInput = Math.floor(timeForEachActivity);
                    }
                    newData.push(item);
                });

                component.set('v.runningTally', Math.floor((availableTime - (timeForEachActivity * numberOfActivitiesToDistributeTime))));
                if(availableTime > 0){
                    component.set('v.timeForDistribution', Math.floor((availableTime - (timeForEachActivity * numberOfActivitiesToDistributeTime)) -1));
                }
                component.set('v.availableActivitiesToComplete', newData);
            }
        }
    },

    doCheckAll : function(component, event, helper) {
        var currentData = [];
        var counter = 0;
        component.get('v.availableActivitiesToComplete').forEach(function(item) {
            if(component.get('v.checkAllActivities') && counter >= 200){
                item.markToComplete = false;
            } else {
                item.markToComplete = component.get('v.checkAllActivities');
            }

            if(component.get('v.checkAllActivities') && item.markToComplete){
                counter += 1;
            }

            // Un-checking
            if(!component.get('v.checkAllActivities')){
                item.userDurationInput = 0;
                counter = 0;
                component.set('v.runningTally', component.get('v.runningTallySnapShot'))
                component.set('v.timeForDistribution',(Number(component.get('v.runningTallySnapShot'))-1));
            }
            currentData.push(item);
        });
        component.set('v.numberOfSelectedActivities', counter)
        component.set('v.availableActivitiesToComplete', currentData)
    },

    /**
    VALIDATIONS For items that have toCheck Id..
    */
    validateForm : function(component, helper, event){
        var toCheck = component.find('toCheck');
        if(toCheck){
            var allValid = [].concat(component.find('toCheck')).reduce(function (validFields,
                inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validFields && inputCmp.get('v.validity').valid;
            }, true);
            return allValid;
        } else {
            return true;
        }
    },

    isPDCValid: function(component){
        var PDCComponent = component.find('pdc');
        var isPDCValid = true;
        if(PDCComponent){
            isPDCValid = PDCComponent.validatePDC();
        }
        return isPDCValid;
    },

    isMassCompleteValid: function(component){
        var isMassCompleteValid = true;
        var numberOfChecked = 0;
        var massCompleteItemComponentList = [].concat(component.find('massCompleteItem')) ;
        console.log('enter isMassCompleteValid')
        console.log(component.get('v.availableActivitiesToComplete').length)
        console.log(massCompleteItemComponentList.length)
        if(component.get('v.availableActivitiesToComplete').length > 0
        && massCompleteItemComponentList.length > 0){
            for(var i = 0; i < massCompleteItemComponentList.length; i++){
                var item = massCompleteItemComponentList[i];
                if(item && isMassCompleteValid){
                    isMassCompleteValid = item.validateMassCompleteItem();
                    var isChecked = item.isItemChecked()
                    if (isChecked){
                        numberOfChecked += 1;
                    }
                }
            }

            if(numberOfChecked > 0 && Number(component.get('v.runningTally')) < 1){
                isMassCompleteValid = false;
            }
        }
        console.log('isVALID ' + isMassCompleteValid)
        return isMassCompleteValid;
    },

    goToView : function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'VIEW'});
        workflowAction.fire();
    },

    /**
    Extract server error message for display.
    */
    getErrorMessage : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0].message;
        }
        return 'Unknown error';
    },

    /**
    Extract response error message for display.
    */
    getError : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0];
        }
        return 'Unknown error';
    },

    goToView : function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'VIEW'});
        workflowAction.fire();
    },

    needRefresh: function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'Need Refresh'});
        workflowAction.fire();
    },

    refreshWorkflow : function(component){
        var refreshWorkflowEngine = component.getEvent("workflowRefreshEvent");
        refreshWorkflowEngine.fire();
    },

    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
});