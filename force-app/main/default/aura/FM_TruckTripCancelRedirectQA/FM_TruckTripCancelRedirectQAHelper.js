/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-01-25 - Created.
*************************************************************************************************/
({

    validateRedirect : function(component, helper) {
        this.showSpinner(component);
        var apexService = component.find("apexService");
        var recId = component.get("v.recordId");
        console.log("Record ID: " + recId);
        apexService.callApexWithErrorCallbackWithHelper(
                component,
                helper,
                "c.checkCancelRedirect",
                { "recordId" : recId },
                this.handleValidCase,
                this.handleInvalidCase
        );
    },

    handleValidCase : function(component, requestResult, helper) {
        console.log("valid case");
        var targetUrl = requestResult.resultObjects[0];
        component.set("v.targetUrl", targetUrl);
        component.set("v.showError", false);
        component.set("v.errorMessage", "");
        helper.hideSpinner(component);
        component.set("v.loaded", true);
    },

    handleInvalidCase : function(component, requestResponse, helper) {
        console.log("invalid case");
        var errorMsg = "";
        requestResponse.errors.forEach(function(element) {
            errorMsg += element + "\n";
        });
        console.log(errorMsg);
        component.set("v.targetUrl", "");
        component.set("v.showError", true);
        component.set("v.errorMessage", errorMsg);
        helper.hideSpinner(component);
        component.set("v.loaded", true);
    },

    showSpinner : function(component) {
        component.set("v.loading", true);
    },

    hideSpinner : function(component) {
        component.set("v.loading", false);
    },

})