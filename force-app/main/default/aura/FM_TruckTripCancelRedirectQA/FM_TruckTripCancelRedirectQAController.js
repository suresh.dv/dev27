/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-01-25 - Created.
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.validateRedirect(component, helper);
    },

})