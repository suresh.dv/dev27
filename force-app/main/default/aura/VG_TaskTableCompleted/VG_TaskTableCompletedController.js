({

	/**
	 * Load tasks for table component
	 */
	doInit : function(component, event, helper) {
		helper.loadData(component);
	},
    
    /**
     * Reload tasks for table component
     */
    handleTaskUpdate : function(component, event, helper) {
        helper.loadData(component);
    },
    
})