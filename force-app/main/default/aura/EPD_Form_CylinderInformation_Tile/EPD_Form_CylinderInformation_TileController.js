/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-10 - Created. - EPD R1
*************************************************************************************************/
({

    checkCompressionThresholds : function(component, event, helper) {
        helper.checkCompressionThresholds(component, event);
    },

    checkIntakeThresholds : function(component, event, helper) {
        helper.checkIntakeThresholds(component, event);
    },

    checkExhaustThresholds : function(component, event, helper) {
        helper.checkExhaustThresholds(component, event);
    },

});