/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-10 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Validates Compression input value against thresholds
    */
    checkCompressionThresholds : function(component, event) {
        var helper = this;
        var inputField = component.find('compressionInput');
        var engineThresholds = component.get("v.thresholds");
        var cylinderInfo = component.get("v.cylinder");
        console.log("running validation");
        var message = "";

        if(cylinderInfo.compression != ""
                && (cylinderInfo.compression < engineThresholds.minCylinderCompression
                || cylinderInfo.compression > engineThresholds.maxCylinderCompression)) {

            message = "Warning: Value is outside of thresholds: Min Cylinder Compression: "
                    + engineThresholds.minCylinderCompression
                    + ", Max Cylinder Compression: "
                    + engineThresholds.maxCylinderCompression
                    + ".";

        }
        helper.inputDisplayErrorIfInvalid(inputField, message);
    },

    /**
    put validation error message on field. (Put blank message if error is not required)
    */
    inputDisplayErrorIfInvalid : function(inputField, message) {
        inputField.setCustomValidity(message);
        inputField.reportValidity();
    },

    /**
    Validates Compression input value against thresholds
    */
    checkIntakeThresholds : function(component, event) {
        var helper = this;
        var inputField = component.find('intakeInput');
        var engineThresholds = component.get("v.thresholds");
        var cylinderInfo = component.get("v.cylinder");
        console.log("running validation");
        var message = "";

        if(cylinderInfo.wearIntake != ""
                && cylinderInfo.wearIntake > engineThresholds.maxValveSeatRecession) {

            message = "Warning: Value is outside of thresholds: Max Valve Seat Recession: "
                    + engineThresholds.maxValveSeatRecession
                    + ".";

        }
        helper.inputDisplayErrorIfInvalid(inputField, message);
    },

    /**
    Validates Compression input value against thresholds
    */
    checkExhaustThresholds : function(component, event) {
        var helper = this;
        var inputField = component.find('exhaustInput');
        var engineThresholds = component.get("v.thresholds");
        var cylinderInfo = component.get("v.cylinder");
        console.log("running validation");
        var message = "";

        if(cylinderInfo.wearExhaust != ""
                && cylinderInfo.wearExhaust > engineThresholds.maxValveSeatRecession) {

            message = "Warning: Value is outside of thresholds: Max Valve Seat Recession: "
                    + engineThresholds.maxValveSeatRecession
                    + ".";

        }
        helper.inputDisplayErrorIfInvalid(inputField, message);
    },

});