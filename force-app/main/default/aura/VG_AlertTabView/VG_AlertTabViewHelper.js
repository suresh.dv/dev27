({
	loadAlert : function(component) {
        component.set("v.loading", true);
        component.find("apexService").callApex(component, "c.getAlert_v1", { alertId : component.get("v.recordId") }, this.loadAlertSuccess);
    },

    loadAlertSuccess : function(component, requestResult) {
        console.log("Request Result: " + JSON.stringify(requestResult));
        console.log("Request Result -> result Object: " + JSON.stringify(requestResult.resultObjects[0]));
        component.set("v.alert", requestResult.resultObjects[0]);
        component.set("v.loading", false);
    },

    loadUserPermissions : function(component) {
        component.set("v.loading", true);
        console.log("alertId: " + component.get("v.recordId"));
        component.find("apexService").callApex(component, "c.getUserPermissionsForAlert", { alertId : component.get("v.recordId") }, this.loadUserPermissionsSuccess);
    },

    loadUserPermissionsSuccess : function(component, requestResult) {
        console.log("Request Result: " + JSON.stringify(requestResult));
        console.log("Request Result -> result Object: " + JSON.stringify(requestResult.resultObjects[0]));
        component.set("v.canEditAlert", requestResult.resultObjects[0].canEditAlert);
        component.set("v.loading", false);
    }
})