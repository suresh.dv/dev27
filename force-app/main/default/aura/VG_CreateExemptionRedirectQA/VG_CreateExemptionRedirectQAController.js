/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
History:        jschn 2019-02-01 - Created.
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.validateRedirect(component, helper);
    },

})