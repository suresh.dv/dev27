/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
History:        jschn 2019-02-01 - Created.
*************************************************************************************************/
({

    validateRedirect : function(component, helper) {
        this.showSpinner(component);
        var apexService = component.find("apexService");
        var recId = component.get("v.recordId");
        console.log("Record ID: " + recId);
        apexService.callApexWithErrorCallbackWithHelper(
                component,
                helper,
                "c.checkExemptionRedirect",
                { "recordId" : recId },
                this.handleValidCase,
                this.handleInvalidCase
        );
    },

    handleValidCase : function(component, requestResult, helper) {
        console.log("valid case");
        var alertRecord = requestResult.resultObjects[0];
        component.set("v.alertRecord", alertRecord);
        var defaultValues = {};
        defaultValues.Well_Location__c = alertRecord.Well_Location__c;
        defaultValues.Vent_Gas_Alert__c = alertRecord.Id;
        defaultValues.Expiration_Date__c = alertRecord.Exemption_Expiration_Date_Helper__c;
        component.set("v.defaultValues", defaultValues);
        component.set("v.showError", false);
        component.set("v.errorMessage", "");
        helper.hideSpinner(component);
        component.set("v.loaded", true);
        $A.get("e.force:closeQuickAction").fire();
    },

    handleInvalidCase : function(component, requestResponse, helper) {
        console.log("invalid case");
        var errorMsg = "";
        requestResponse.errors.forEach(function(element) {
            errorMsg += element + "\n";
        });
        console.log(errorMsg);
        component.set("v.alertRecord", null);
        component.set("v.defaultValues", null);
        component.set("v.showError", true);
        component.set("v.errorMessage", errorMsg);
        helper.hideSpinner(component);
        component.set("v.loaded", true);
    },

    showSpinner : function(component) {
        component.set("v.loading", true);
    },

    hideSpinner : function(component) {
        component.set("v.loading", false);
    },

})