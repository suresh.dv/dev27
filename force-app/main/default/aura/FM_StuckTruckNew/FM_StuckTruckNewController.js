/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
History:        jschn 2019-02-12 - Created.
*************************************************************************************************/
({

    handleTruckTripLoad : function(component, event, helper) {
        helper.validateTruckTrip(component);
    },

    handleEditFormLoad : function(component, event, helper) {
        helper.prepopulateStuckTruck(component);
    },

    handleSuccess : function(component, event, helper) {
        helper.redirectToView(component, event);
    },

})