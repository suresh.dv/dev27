/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
History:        jschn 2019-02-12 - Created.
*************************************************************************************************/
({

    validateTruckTrip : function(component) {

        var truckTrip = component.get("v.truckTrip");
        if(truckTrip != undefined) {

            var status = truckTrip.Truck_Trip_Status__c;
            var valid = true;
            var errorMsg = "";

            if(status !== "Dispatched"
                    && status !== 'Completed'
                    && status !== "Add On"
                    && status !== "Cancelled") {

                valid = false;
                errorMsg = "Only Dispatched, Cancelled, Completed and Add On truck trips can be set to stuck.";

            } else if (truckTrip.Stuck__c > 0) {

                valid = false;
                errorMsg = "This truck trip is already stuck!";

            }

            component.set("v.errorMessage", errorMsg);
            component.set("v.showError", !valid);
            component.set("v.showCreateRecord", valid);
        }

    },

    prepopulateStuckTruck : function(component) {
        var truckTrip = component.get("v.truckTrip");
        component.find("Location").set("v.value", truckTrip.Load_Request__r.Source_Location__c);
        component.find("Facility").set("v.value", truckTrip.Load_Request__r.Source_Facility__c);
        component.find("Carrier").set("v.value", truckTrip.Carrier__c);
        component.find("CarrierUnit").set("v.value", truckTrip.Unit__c);
        component.find("TruckTrip").set("v.value", truckTrip.Id);
        component.find("Status").set("v.value", "Stuck");
    },

    redirectToView : function(component, event) {
        $A.get("e.force:refreshView").fire();
        setTimeout(function() {
            var response = event.getParams().response;
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    actionName: 'view',
                    recordId: response.id
                }
            };
            navService.navigate(pageReference, true);
        }, 250);

    },

})