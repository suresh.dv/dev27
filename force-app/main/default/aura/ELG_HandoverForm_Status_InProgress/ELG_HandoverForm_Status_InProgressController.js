/**
 * Created by MarosGrajcar on 7/29/2019.
 */

({
    openModalForAccept : function(component,event,helper) {
        helper.openModalForAcceptAndTrainee(component,event,helper);
    },

    reviewedByShiftEngineer : function(component, event, helper) {
        helper.submitReviewedByShiftEngineer(component, event, helper);
    },

    disableOrEnableEngineerButton : function(component, event, helper) {
        var checkbox = component.find('shiftEngineer').get('v.checkboxState');
        component.set('v.disableShiftEngineerButton', !checkbox);
    },

    componentReload : function(component, event, helper) {
        var message = event.getParams().message;
        if (message == 'Handover form has been updated successfully') {
            component.getEvent('reloadHandoverWrapper').fire();
        }
    },

});