/**
 * Created by MarosGrajcar on 7/29/2019.
 */

({
    /*Save Shift Engineer review section*/
    submitReviewedByShiftEngineer : function(component, event, helper) {
        var user = $A.get("$SObjectType.CurrentUser.Id");
        component.set('v.disableButton', true);
        component.set('v.disableShiftEngineerButton', true);
        component.set('v.reviewButtonLabel', 'Processing');

        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.updateHandoverReviewByShiftEngineer",
            { "HOForm" : component.get("v.handoverForm"),
            "currentUser" : user },
            this.saveSuccessful,
            this.errorOnSave);
    },


     /*On successful save refresh page and close modal*/
    saveSuccessful : function(component, requestResult, helper) {
        component.set('v.disableButton', false);
        component.set('v.disableShiftEngineerButton', false);
        component.set('v.buttonLabel', 'Submit');
        component.set('v.acceptHandoverLabel', 'Accept Handover and Take Post');
        $A.get('e.force:refreshView').fire();
    },

    errorOnSave : function(component, requestResult, helper) {
        console.log('Error on Handover save.');
        component.set('v.disableButton', false);
        component.set('v.disableShiftEngineerButton', false);
        component.set('v.buttonLabel', 'Submit');
        component.set('v.acceptHandoverLabel', 'Accept Handover and Take Post');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();
         $A.get('e.force:refreshView').fire();
    },

    /* Open Modal For Save and Select trainee */
    openModalForAcceptAndTrainee: function(component, evt, helper) {
        var modalBody;
        $A.createComponent("c:ELG_HandoverForm_AcceptHandover_Modal", {
            handoverForm:component.get('v.handoverForm'),
            doRedirect:component.get('v.doRedirect')
        },
        function(content, status, errorMessage) {
            if (status === "SUCCESS") {
                modalBody = content;
                component.find('overlayLib').showCustomModal({
                    header: "Accept Handover",
                    body: modalBody,
                    showCloseButton: true,
                    closeCallback: function() {}
                })
            }
        });
    },
});