/**
 * Created by MarcelBrimus on 03/10/2019.
 */

({
    initAction : function(component, event, helper) {
        helper.saveOriginalLabel(component);
        helper.changeLabel(component, 'Setting location...');
        component.set('v.actionInProgress', true);

        // Try to set latlong
        if(navigator.geolocation){
            // setting for geolocations
            var options = {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 120000
            };
            // This might not be supported in all brosers,
            // In case user denies access it will go straight to other function
            navigator.geolocation.getCurrentPosition(function(position) {
                    var latit = position.coords.latitude;
                    var longit = position.coords.longitude;
                    component.set('v.latitude', latit);
                    component.set('v.longitude', longit);
                    helper.executeAction(component, event, helper)
                }, function(error) {
                    helper.executeAction(component, event, helper)
                }, options
             );
        } else {
            helper.executeAction(component, event, helper)
            console.log("No Capability to record position");
        }
    },

    executeAction : function(component, event, helper) {
        component.set("v.actionInProgress", true);
        var apexService = component.find("apexService");

        // Execute action based on context
        if(component.get('v.actionName') == $A.get("{!$Label.c.VTT_LTNG_Job_On_Hold_Action}")){
            helper.changeLabel(component, 'Checking if we can put activity on hold');
            apexService.callApexWithErrorCallbackWithHelper(
                component,
                helper,
                "c.canActivityBePutOnHold",
                { "activity" : component.get('v.workOrderActivity'),
                "tradesman" : component.get('v.tradesman')},
                this.handleActivityOnHoldSuccess,
                this.handleActivityOnHoldChangedError
            );
        } else {
            helper.changeLabel(component, 'Checking for data changes...');
            apexService.callApexWithErrorCallbackWithHelper(
                component,
                helper,
                "c.hasActivityChanged",
                { "activity" : component.get('v.workOrderActivity')},
                this.handleActivityChangedSuccess,
                this.handleActivityChangedError
            );
        }
    },

    handleActivityOnHoldSuccess : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        helper.changeLabel(component, component.get('v.originalButtonLabel'));

        if(!response.resultObjects[0]){
            var workflowAction = component.getEvent("workflowEventAction");
            workflowAction.setParams({actionName: 'Need Refresh'});
            workflowAction.fire();
        } else {
            var workflowAction = component.getEvent("workflowEventAction");
            workflowAction.setParams({
                actionName: component.get('v.actionName'),
                workOrderActivity: component.get('v.workOrderActivity'),
                tradesman: component.get('v.tradesman'),
                latitude: component.get('v.latitude'),
                longitude: component.get('v.longitude')
            })
            workflowAction.fire();
        }
    },

    handleActivityOnHoldChangedError : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        helper.changeLabel(component, component.get('v.originalButtonLabel'));
        helper.showToast("Error!", "error", helper.getError(response));
        component.set("v.actionInProgress", false);
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'VIEW'});
        workflowAction.fire();
    },

    handleActivityChangedSuccess : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        helper.changeLabel(component, component.get('v.originalButtonLabel'));

        if(!response.resultObjects[0]){
            var workflowAction = component.getEvent("workflowEventAction");
            workflowAction.setParams({actionName: 'Need Refresh'});
            workflowAction.fire();
        } else {
            var workflowAction = component.getEvent("workflowEventAction");
            workflowAction.setParams({
                actionName: component.get('v.actionName'),
                workOrderActivity: component.get('v.workOrderActivity'),
                tradesman: component.get('v.tradesman'),
                latitude: component.get('v.latitude'),
                longitude: component.get('v.longitude')
            })
            workflowAction.fire();
        }
    },

    handleActivityChangedError : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        helper.changeLabel(component, component.get('v.originalButtonLabel'));
        helper.showToast("Error!", "error", helper.getError(response));
        component.set("v.actionInProgress", false);
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'VIEW'});
        workflowAction.fire();
    },

    saveOriginalLabel: function(component){
        component.set('v.originalButtonLabel', component.get('v.buttonLabel'));
    },

    changeLabel: function(component, newLabelName){
        component.set('v.buttonLabel', newLabelName);
    },

    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
});