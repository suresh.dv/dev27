({

	/**
	 * Use picklist service to retrieve picklist options used on component.
	 */
	loadPicklistsOptions : function(component) {
		var service = component.find("picklistService");
		service.getPicklistValues($A.getCallback(function(error, data) {
			component.set("v.assigneeTypesList", data);
		}),"HOG_Vent_Gas_Alert_Task__c", "Assignee_Type__c");
		service.getPicklistValues($A.getCallback(function(error, data) {
			component.set("v.statusList", data);
		}),"HOG_Vent_Gas_Alert_Task__c", "Status__c");
	},

	/**
	 * Load task from server based on taskId attribute.
	 */
	loadTask : function(component) {
	    var helper = this;
        component.set("v.loading", true);
		component.find("apexService").callApexWithHelper(component, helper, "c.getTask_v1", { taskId : component.get("v.taskId") }, this.loadTaskSuccess);
	},

	loadTaskSuccess : function(component, requestResult, helper) {
		component.set("v.task", requestResult.resultObjects[0]);
		component.set("v.loading", false);

		var mode = component.get("v.originalMode");
	    if(mode === "edit") {
            helper.setAssigneeType(component, component.get("v.task").Assignee_Type__c);
            helper.setAssignees(component);
        }
	},

	/**
	 * Preset task values based on alert.
	 * Set defaul Assignee Type and clear selection for lookups.
	 */
	presetTask : function(component, event) {
		var alert = component.get("v.alert");
		var task = {};
		task.Vent_Gas_Alert__c = alert.Id;
		task.Vent_Gas_Alert__r = {};
		task.Vent_Gas_Alert__r.Name = alert.Name;
		task.Name = alert.Well_Location__r.Name + " - " + this.getAbbreviatedType(alert.Type__c) + " - ";
		task.Vent_Gas_Alert__r.Well_Location__c = alert.Well_Location__c;
		task.Priority__c = alert.Priority__c;
		task.Status__c = "Not Started";
		task.Comments__c = alert.Description__c;
		task.Type__c = alert.Type__c;
		task.Remind__c = true;
		component.set("v.task", task);
		this.setAssigneeType(component, "Operations Engineer");
		var lookup1 = component.find("Assignee1Lookup");
		lookup1.clearSelection();
		var lookup2 = component.find("Assignee2Lookup");
		lookup2.clearSelection();
	},

	/**
	* Returns abbreviated Alert type.
	* This is necessary to avoid error due to 80char on Name restriction.
	*
	* jschn - 4.12.2019 - W-001735
	*/
	getAbbreviatedType : function(type) {
	    var abbreviatedType;
	    switch(type) {
	        case 'Predicted High Vent Rate':
	            abbreviatedType = 'High Vent Rate';
	            break;
            case 'Large Low Variance in Predicted Vent Rate vs.Test Vent Rate':
                abbreviatedType = 'Low Variance Vent Rate';
                break;
            case 'Large High Variance in Predicted Vent Rate vs.Test Vent Rate':
                abbreviatedType = 'High Variance Vent Rate';
                break;
            default:
                abbreviatedType = type;
        }
        return abbreviatedType;
    },

	/**
	 * Get route operator from server based on Well Location and assign it to Assignee1 lookup
	 */
	getRouteOperator : function(component){
		component.set("v.loading", true);
		var mode = component.get("v.mode");
		var locId;
        if(mode == "edit"
            || mode == "view") {
            locId = component.get("v.task").Vent_Gas_Alert__r.Well_Location__c;
        } else {
            locId = component.get("v.alert").Well_Location__c;
        }
        console.log(locId);
		component.find("apexService").callApex(component, "c.getRouteOperator_v1", { locationId : locId }, this.loadRouteOperatorSuccess);
	},

	loadRouteOperatorSuccess : function(component, requestResult) {
		if(requestResult.resultObjects && requestResult.resultObjects.length > 0) {
			var operator = requestResult.resultObjects[0];
			if(operator && operator != "undefined") {
				var task = component.get("v.task");
				var lookup = component.find("Assignee1Lookup");
				lookup.selectRecord(operator);
	    		task.Assignee1__c = operator.Id;
	    		component.set("v.task", task);
	    	}
    	} else {
			var lookup = component.find("Assignee1Lookup");
			lookup.clearSelection();
    	}
		component.set("v.loading", false);
	},

	/**
	 * As there isn't kept Operator Engineer relation for route/location, we just blanks out 
	 * Assignee1 lookup.
	 */
	getOperatorEngineer : function(component){
		component.set("v.loading", true);
		var task = component.get("v.task");
		task.Assignee1__c = null;
		component.set("v.task", task);
		var lookup = component.find("Assignee1Lookup");
		lookup.clearSelection();
		component.set("v.loading", false);
	},

	/**
	 * If data are valid save task, fire notification(toast), reset task record displayed in component with values 
	 * from alert, clear lookups and fire success event. 
	 * Otherwise show errors/missing data.
	 */
	saveTask : function(component, helper) {
		var dataIsValid = this.dataIsValid(component);
		if(dataIsValid) {
			component.set("v.loading", true);
			component.find("apexService").callApexWithHelper(component, helper, "c.upsertTask_v1", { "vgTask" : JSON.stringify(component.get("v.task")) }, this.saveTaskSuccess);
		} else {
			this.fireNotification("", "Missing data.", "Error");
			console.log("Data are invalid.");
		}
	},

	saveTaskSuccess : function(component, requestResult, helper) {
		if(component.get("v.originalMode") === "edit") {
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    actionName: 'view',
                    recordId: component.get("v.taskId")
                }
            };
            navService.navigate(pageReference, true);
        } else {
            helper.fireNotification("", "Task saved.", "Success");
            helper.presetTask(component);
            helper.handleAssigneeTypeChange(component);
            helper.fireSuccessEvent(component);
            $A.get('e.force:refreshView').fire();
            component.set("v.loading", false);
        }
	},

	/**
	 * If data are valid save task, fire notification(toast), fire success event, fire task change event,
	 * reload task from server and toggle to view mode.
	 * Otherwise show errors/missing data.
	 */
	saveTaskEdit : function(component, helper) {
		var dataIsValid = this.dataIsValid(component);
		if(dataIsValid) {
			component.set("v.loading", true);
			component.find("apexService").callApexWithHelper(component, helper, "c.upsertTask_v1", { "vgTask" : JSON.stringify(component.get("v.task")) }, this.saveTaskEditSuccess);
		} else {
			this.fireNotification("", "Missing data.", "Error");
			console.log("Data are invalid.");
		}
	},

	saveTaskEditSuccess : function(component, requestResult, helper) {
		helper.fireNotification("", "Task updated.", "Success");
		helper.fireSuccessEvent(component);
		helper.fireTaskChangeEvent(component);
		helper.loadTask(component);
		helper.toggleEditMode(component);
		$A.get('e.force:refreshView').fire();
		component.set("v.loading", false);
	},

	/** 
	 * Run validation on required fields.
	 */
	dataIsValid : function(component) {
		var task = component.get("v.task");
		component.find("Assignee1Lookup").validate();
		if(!task.Assignee_Type__c) {
	        var assigneTypeInput = component.find("lookupInput");
	        assigneTypeInput.set("v.errors", [{ message : "Assignee Type is required field" }])
	    }
		return task.Assignee_Type__c
			&& task.Assignee1__c;
	},

	/**
	 * Show toast message.
	 */
	fireNotification : function(toastTitle, toastMessage, toastType) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
		    title: toastTitle,
		    message: toastMessage,
		    type: toastType
		});
		toastEvent.fire();
	},

	/**
	 * Handle assignee type change.
	 * Load Operator based on selected assignee type.
	 * Skip first run based on outcome from checkIfLoadOperators function.
	 */
	handleAssigneeTypeChange : function(component) {
		if(this.checkIfLoadOperators(component)) {
			var assigneeType = component.get("v.assigneeType");
			console.log(assigneeType);
			if(assigneeType == "Route Operator"){
				this.getRouteOperator(component);
			} else {
				this.getOperatorEngineer(component);
			}     
			var task = component.get("v.task");
			task.Assignee_Type__c = assigneeType;
			component.set("v.task", task);
		} else {
			component.set("v.load", false);
		}
	},

	/**
	 * Resolve if handle assignee type change should be skipped.
	 * Skip only happens when initial load is done(first run) and only for
	 * view and edit/view modes.
	 */
	checkIfLoadOperators : function(component) {
		var mode = component.get("v.mode");
		var firstLoad = component.get("v.load");
		return (!firstLoad 
				&& (mode === "edit" || mode === "view"))
			|| mode === "new";
	},

	/**
	 * Fire Applicatoin event that Task has been successfully changed.
	 * Reload needed.
	 */
	fireSuccessEvent : function(component) {
		 var event = $A.get("e.c:VG_TaskSaveSuccess"); 
		 event.fire();
	},

	/**
	 * Toggling edit/view mode.
	 */
	toggleEditMode : function(component) {
		var mode = component.get("v.mode");
		if(mode === "edit") {
			component.set("v.mode", "view");
		} else if(mode === "view") {
			component.set("v.mode", "edit");
		}
	},

	/**
	 * Set assignee type to temp attribute provided as parameter.
	 */
	setAssigneeType : function(component, assigneeType) {
		component.set("v.assigneeType", assigneeType);
		var task = component.get("v.task");
	},

	/**
	 * Set assignees into lookups.
	 */
	setAssignees : function(component) {
		var task = component.get("v.task");
		this.setAssignee(component, "Assignee1Lookup", task.Assignee1__r, task.Assignee1__c);
		this.setAssignee(component, "Assignee2Lookup", task.Assignee2__r, task.Assignee2__c);
	},

	/**
	 * Set assignee record to lookup
	 * @param {[String]} lookupId   [AuraId of lookup to set up]
	 * @param {[User]} operator   [User to set as selected inside lookup]
	 * @param {[String]} operatorId [User Id to set as selected inside lookup]
	 */
	setAssignee : function(component, lookupId, operator, operatorId) {
		if(operator) {
			operator.Id = operatorId;
			var lookup = component.find(lookupId);
			lookup.selectRecord(operator);
		}
	},

	/**
	 * Fire Application event that Task has been changed. Reload needed.
	 */
	fireTaskChangeEvent : function(component) {
		 var event = $A.get("e.c:VG_TaskChange"); 
		 event.fire();
	},

})