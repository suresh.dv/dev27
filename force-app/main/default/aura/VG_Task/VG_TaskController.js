({
	doInit : function(component, event, helper) {
		helper.loadPicklistsOptions(component);
		var mode = component.get("v.mode");
		if(mode === "view"
		|| mode === "edit") {
	    	helper.loadTask(component);
	    }
	},

	loadTask : function(component, event, helper) {
		if(component.get("v.mode") === "new") {
			helper.presetTask(component, event);
		}
	},

	save : function(component, event, helper) {
		helper.saveTask(component, helper);
	},

	saveEdit : function(component, event, helper) {
		helper.saveTaskEdit(component, helper);
	},

	handleAssigneeTypeChange : function(component, event, helper) {
		helper.handleAssigneeTypeChange(component);
	},

	clear : function(component, event, helper) {
		helper.presetTask(component, event);
		helper.fireNotification("", "Form clean completed.", "Informational");
	},

	cancel : function(component, event, helper) {
		if(component.get("v.originalMode") === "edit") {
//		    window.history.back();
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    actionName: 'view',
                    recordId: component.get("v.taskId")
                }
            };
            navService.navigate(pageReference, true);
        } else {
            helper.toggleEditMode(component);
            helper.fireTaskChangeEvent(component);
            helper.loadTask(component);
        }
	},

	edit : function(component, event, helper) {
		helper.toggleEditMode(component);
		helper.setAssigneeType(component, component.get("v.task").Assignee_Type__c);
		helper.setAssignees(component);
	},
	
})