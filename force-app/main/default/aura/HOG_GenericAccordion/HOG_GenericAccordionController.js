({
	doInit : function(component, event, helper) {	
		helper.ToggleExpandCollapseHandler(component, event);
	},

	ToggleExpandCollapse : function(component, event, helper) {	
		helper.ToggleExpandCollapseHandler(component, event);

	},
})