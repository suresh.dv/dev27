({
	ToggleExpandCollapseHandler : function(component, event) {
		var isAccordionOpen = component.get("v.loadOpenAccordion");
		var container = component.find("collapseContainer");

		if(isAccordionOpen === true) {
			component.set("v.loadOpenAccordion", false);
			$A.util.removeClass(container, "slds-hide");;
		} else {			
			component.set("v.loadOpenAccordion", true);
			$A.util.addClass(container, "slds-hide");
		}
	}
})