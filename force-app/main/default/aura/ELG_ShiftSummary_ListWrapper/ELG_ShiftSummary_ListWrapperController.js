/**
 * Created by MarosGrajcar on 3/12/2020.
 */

({
    doInit : function(component, event, helper) {
        console.log('SUMMARY WRAPPER: ' +component.get('v.summaryIds'));
        var listOfIds = component.get('v.summaryIds');
        component.set('v.currentSummary', listOfIds[0]);
        component.set('v.selectedPage', 1);
        component.set('v.totalPages', listOfIds.length);
    },

    selectFirstSummary : function(component, event, helper) {
        var listOfIds = component.get('v.summaryIds');
        component.set('v.currentSummary', listOfIds[0]);
        component.set('v.currentPosition', 0);
        component.set('v.selectedPage', 1);
        helper.checkButtons(component, event, helper);
    },

    selectPreviousSummary : function(component, event, helper) {
        var listOfIds = component.get('v.summaryIds');
        var position = component.get('v.currentPosition');
        if (position != 0) {
            position--;

            component.set('v.selectedPage', position+1);
            component.set('v.currentPosition', position);
            component.set('v.currentSummary', listOfIds[position]);
        }
        helper.checkButtons(component, event, helper);
    },

    selectNextSummary : function(component, event, helper) {
        var listOfIds = component.get('v.summaryIds');
        var position = component.get('v.currentPosition');
        if ((position+1) != listOfIds.length) {
            position++;

            component.set('v.selectedPage', position+1);
            component.set('v.currentPosition', position);
            component.set('v.currentSummary', listOfIds[position]);
            console.log('v.currentSummary: '+component.get('v.currentSummary'));
        }
        helper.checkButtons(component, event, helper);
    },

    selectLastSummary : function(component, event, helper) {
        var listOfIds = component.get('v.summaryIds');
        var summaryQuantity = listOfIds.length;
        component.set('v.currentSummary', listOfIds[summaryQuantity-1]);
        component.set('v.currentPosition', summaryQuantity-1);
        component.set('v.selectedPage', summaryQuantity);
        helper.checkButtons(component, event, helper);
    },

    componentReload : function(component, event, helper) {
        component.set('v.reloadComponent', false);
        component.set('v.reloadComponent', true);
    },

    noReviewerSet : function(component, event, helper) {
        var cannotReview = event.getParam('userCannotReview');
        component.set('v.userCannotReview', cannotReview);
    },
});