/**
 * Created by MarosGrajcar on 3/12/2020.
 */

({
    checkButtons : function(component, event, helper) {
        var listOfIds = component.get('v.summaryIds');
        var summaryQuantity = listOfIds.length;
        var position = component.get('v.currentPosition');

        if (position == (summaryQuantity -1)) {
            component.set('v.disableForward', true);
        } else {
            component.set('v.disableForward', false)
        }

        if (position == 0) {
            component.set('v.disableBack', true);
        } else {
            component.set('v.disableBack', false);
        }
    },
});