/**
 * Created by MarosGrajcar on 7/29/2019.
 */

({
    /*Save of reviewers*/
    submitReviewedSections : function(component, event, helper) {
        var user = $A.get("$SObjectType.CurrentUser.Id");

        component.set('v.disableButton', true);
        component.set('v.buttonLabel', 'Processing');

        var sectionsForReview = component.get('v.reviewersSection');
        var reviewersSectionForUpdate = [];

        sectionsForReview.forEach(function(section) {
            var existingSection = component.find(section);
            if (existingSection) {
                var checkboxState = existingSection.get('v.checkboxState')
                var user = existingSection.get('v.reviewer');
                if (checkboxState && !user) {
                        reviewersSectionForUpdate.push(existingSection.getLocalId());
                }
            }
        });

        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.updateHandoverReview",
            { "HOForm" : component.get("v.handoverForm"),
            "sectionsForUpdate" : reviewersSectionForUpdate,
            "currentUser" : user },
            this.reviewedSuccess, this.reviewedError);
    },

    /*On successful save refresh page*/
    reviewedSuccess : function(component, requestResult, helper) {
        component.set('v.buttonLabel', 'Submit');
        component.set('v.disableButton', false);
        if (component.get('v.doRedirect')) {
            helper.gotoList(component, event, helper);
        } else {
            component.getEvent('reloadHandoverWrapper').fire();
        }
    },

    reviewedError : function(component, requestResult, helper) {
        console.log('Error on Review save.');
        component.set('v.disableButton', false);
        component.set('v.buttonLabel', 'Submit');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();
        $A.get("e.force:refreshView").fire();
    },

    gotoList : function (component, event, helper) {
        var navEvent = $A.get("e.force:navigateToList");
        navEvent.setParams({
            "scope": "ELG_Shift_Handover__c"
            });
        navEvent.fire();
    },
});