/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Fire close event for modal parent.
    */
    fireCloseModalEvent : function(component) {
        var closeModalEvent = component.getEvent("closeEngineThreshold");
        closeModalEvent.fire();
    },

});