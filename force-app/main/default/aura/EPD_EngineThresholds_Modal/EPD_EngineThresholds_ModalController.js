/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    closeModal : function(component, event, helper) {
        helper.fireCloseModalEvent(component);
    },

});