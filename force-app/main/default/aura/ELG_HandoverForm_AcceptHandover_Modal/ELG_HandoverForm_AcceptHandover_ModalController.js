/**
 * Created by MarosGrajcar on 9/30/2019.
 */

({
    acceptSelectedHandover : function(component,event,helper) {
        helper.acceptPassedHandover(component,event,helper);
    },

    cancel : function(component) {
      // Close modal
      component.find("overlayLib").notifyClose();
    },
});