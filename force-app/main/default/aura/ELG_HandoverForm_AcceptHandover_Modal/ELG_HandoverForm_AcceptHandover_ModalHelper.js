/**
 * Created by MarosGrajcar on 9/30/2019.
 */

({

    /*Save section*/
    acceptPassedHandover : function(component, event, helper) {
        var user = $A.get("$SObjectType.CurrentUser.Id");
        var idOfTrainee = component.find('trainee').get('v.selectedId');
        var searchKeyWord = component.find('trainee').get('v.searchKeyWord');
        component.set('v.buttonLabel', 'Processing');
        component.set('v.disableButton', true);

        if (searchKeyWord && !idOfTrainee) {
            component.find('trainee').set('v.errorMsg', 'Please, click on a name from selection or clear the input field');
            component.find('trainee').showError();
            component.set('v.buttonLabel', 'Submit');
            component.set('v.disableButton', false);
        } else  {
            component.find('trainee').clearError();
            component.find("apexService").callApexWithErrorCallbackWithHelper(component,
                        helper,
                        "c.acceptHandover",
                        { "HOForm" : component.get("v.handoverForm"), "currentUserId" : user, "traineeId" : idOfTrainee },
                        this.saveSuccessfulOnAccept,
                        this.errorOnSave);
        }
    },

     /*On successful save redirect to Shift component*/
    saveSuccessfulOnAccept : function(component, requestResult, helper) {
        if (component.get('v.doRedirect')) {
            helper.navigateToShift(component, event, helper);
        } else {
            const toastEvent = $A.get('e.force:showToast');
            toastEvent.setParams({
              type: 'success',
              message: 'Handover form has been updated successfully'
            });
            toastEvent.fire();
            component.find("overlayLib").notifyClose();
        }
    },

    errorOnSave : function(component, requestResult, helper) {
        console.log('Error on Handover save.');
        component.set('v.disableButton', false);
        component.set('v.buttonLabel', 'Submit');
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();
    },

    navigateToShift : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
        componentDef : "c:ELG_ShiftHandover",
        isredirect : "true",
        componentAttributes: {}
        });
        evt.fire();
    },
});