/**
 * Created by MarcelBrimus on 22/02/2019.
 */
({

    doExecuteTECO: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.updateTECO_ltng",
            { "workOrderId" : component.get("v.recordId"),
              "validateStatus" : true},
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess: function(component, response, helper){
        var responseString = response.resultObjects[0];
        helper.hideSpinner(component);
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:closeQuickAction").fire();
        helper.displayToast(component, 'warning', responseString);
    },

    handleError: function(component, response, helper){
        var responseString = response.resultObjects[0];
        component.set('v.messageFromWebservice', responseString);
        helper.hideSpinner(component);
    },

    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          type: type,
          message: message
        });
        toastEvent.fire();
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },
})