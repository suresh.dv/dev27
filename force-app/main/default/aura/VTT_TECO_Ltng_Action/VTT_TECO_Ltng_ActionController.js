/**
 * Created by MarcelBrimus on 22/02/2019.
 */
({
    /*
    * Called when record data in component loads, then decides is refresh or create is needed
    */
    onRecordInit : function(component, event, helper) {
        helper.doExecuteTECO(component);
    },
})