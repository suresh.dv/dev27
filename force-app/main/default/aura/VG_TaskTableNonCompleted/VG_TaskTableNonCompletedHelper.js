({

    /**
     * Load Tasks related to Alert with noncompleted lifecycle.
     */
	loadTasks : function(component) {
        component.set("v.loading", true);
        component.find("apexService").callApex(component, "c.getNonCompletedTasks_v1", { alertId : component.get("v.recordId") }, this.loadTasksSuccess);
    },

    loadTasksSuccess : function(component, requestResult) {
        var tasks = requestResult.resultObjects;
        component.set("v.tasks", tasks);
        component.set("v.tasksPresent", tasks != null && tasks.length > 0);
        component.set("v.loading", false);
    },

})