({

	/**
	 * Load tasks for table component
	 */
	doInit : function(component, event, helper) {
		helper.loadTasks(component);
	},

    /**
     * Reload tasks for table component
     */
    handleNewTaskCreation : function(component, event, helper) {
        helper.loadTasks(component);
    },
    
})