/**
 * Created by MarosGrajcar on 11/14/2019.
 */

({
    massReviewUpdate : function(component, event, helper) {
        var user = $A.get("$SObjectType.CurrentUser.Id");

        component.set('v.disableButton', true);
        component.set('v.buttonLabel', 'Processing');

        var selectedHandovers = component.get('v.handoverIds');
        var supervisorReview = component.find('supervisorReview').get('v.checkboxState');
        var steamChiefReview = component.find('steamChiefReview').get('v.checkboxState');
        var shiftEngineerReview = component.find('shiftEngineerReview').get('v.checkboxState');
        var shiftLeadReview = component.find('shiftLeadReview').get('v.checkboxState');

        if (supervisorReview || steamChiefReview || shiftEngineerReview || shiftLeadReview) {
            component.find("apexService").callApexWithErrorCallbackWithHelper(component,
                helper,
                "c.massHandoverReviewersUpdate",
                { "listOfIds" : selectedHandovers,
                "currentUser" : user,
                "supervisorReviews" : supervisorReview,
                "steamChiefReviews" : steamChiefReview,
                "shiftEngineerReviews" : shiftEngineerReview,
                "shiftLeadReviews" : shiftLeadReview },
                this.reviewedSuccess,
                this.reviewedError);
        } else {
           helper.toastMessage('', 'Please, select at least one option.', 'warning');
           component.set('v.disableButton', false);
           component.set('v.buttonLabel', 'Submit');
        }
    },

    reviewedSuccess : function(component, requestResult, helper) {
        component.set('v.disableButton', false);
        component.set('v.buttonLabel', 'Submit');
        var returnedMessage = requestResult.resultObjects[0];
        helper.toastMessage('', returnedMessage, 'success');
        $A.get('e.force:refreshView').fire();
        // Close modal
        component.find("overlayLib").notifyClose();
    },

    reviewedError : function(component, requestResult, helper) {
        component.set('v.disableButton', false);
        component.set('v.buttonLabel', 'Submit');
        helper.toastMessage('', requestResult.errors[0], 'error');
    },

    toastMessage : function(toastTitle, toastMessage, toastType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: toastTitle,
            message: toastMessage,
            type: toastType
        });
        toastEvent.fire();
    },



});