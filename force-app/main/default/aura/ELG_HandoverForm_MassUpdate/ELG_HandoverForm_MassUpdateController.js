/**
 * Created by MarosGrajcar on 11/14/2019.
 */

({
    submitReviews : function(component, event, helper) {
      helper.massReviewUpdate(component, event, helper);
    },

    closeModal : function(component) {
       // Close modal
        component.find("overlayLib").notifyClose();
    },
});