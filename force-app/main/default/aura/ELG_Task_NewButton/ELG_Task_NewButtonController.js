/**
 * Created by MarosGrajcar on 10/24/2019.
 */

({
    navigateToShiftLogs : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
        componentDef : "c:ELG_ShiftHandover",
        isredirect : "true",
        componentAttributes: {}
        });
        evt.fire();
    },

    navigateToSearchTasks : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
        componentDef : "c:ELG_Task_Search_cmpHolder",
        isredirect : "true",
        componentAttributes: {}
        });
        evt.fire();
    },
});