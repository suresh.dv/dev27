/**
 * Created by MarosGrajcar on 10/24/2019.
 */

({
    getAllAMUActiveTasks : function(component, event, helper) {
      var apexService = component.find("apexService");
          //this.showSpinner(component);
          apexService.callApexWithErrorCallbackWithHelper(
              component,
              this,
              "c.getAMUPosts",
              {},
              this.handleLoad,
              this.handleError
          );
      },

      handleLoad : function(component, requestResult, helper) {
          console.log('SUCCESS');
          component.set("v.amuPosts", requestResult.resultObjects);
      },

      handleError : function(component, requestResult, helper) {
          console.log('ERROR ' +requestResult.errors[0]);
      },
});