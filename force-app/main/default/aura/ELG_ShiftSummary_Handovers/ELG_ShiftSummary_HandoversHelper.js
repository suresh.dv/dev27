/**
 * Created by MarosGrajcar on 3/6/2020.
 */

({
    loadHandovers : function(component) {
        var helper = this;
        //var user = $A.get("$SObjectType.CurrentUser.Id");
        //component.set('v.loading', true);
        console.log('initializing');
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.loadShiftHandovers",
            { shiftIds : component.get("v.shiftIds")},
            this.loadSuccess,
            this.errorCallback);

    },

    /*Load on HandoverForm Component*/
    loadSuccess : function(component, requestResult, helper) {
        console.log('result: ' +JSON.stringify(requestResult.resultObjects[0]));
        component.set("v.shiftHandovers", requestResult.resultObjects[0]);
    },

    errorCallback : function(component, requestResult, helper) {
        console.log('Error load');
        /*component.set('v.loading', false);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();*/
    },
});