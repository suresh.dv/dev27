/**
 * Created by MarosGrajcar on 3/6/2020.
 */

({
    doInit : function(component, event, helper) {
        helper.loadHandovers(component);
    },

    refreshData : function(component, event, helper) {
        helper.loadHandovers(component);
    },
});