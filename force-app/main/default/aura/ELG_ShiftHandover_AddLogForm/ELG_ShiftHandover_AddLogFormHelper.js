/**
 * Created by MarcelBrimus on 04/06/2019.
 */
({
    // CALLOUT to save log entry
    saveLogEntry: function(component, helper){
        if (component.get('v.newLogEntry').User_Log_Entry__c) {
            helper.showSpinner(component);

            var newLogEntry = component.get("v.newLogEntry");
            newLogEntry.Post__c = component.get('v.selectedPost');
            if(component.get('v.currentShift')){
                newLogEntry.Shift_Assignement__c = component.get('v.currentShift').Id;
            }
            newLogEntry.Operating_Field_AMU__c = component.get('v.facilityId');

            component.find("apexService").callApexWithErrorCallbackWithHelper(
                component,
                helper,
                "c.saveLogEntry",
                {"log" : newLogEntry},
                this.handleSaveEntrySuccess,
                this.handleSaveEntryError
            );
        } else {
            var inputCmp = component.find("userInput");
            inputCmp.reportValidity();
        }
    },

    handleSaveEntrySuccess: function(component, response, helper) {
        var message = 'Successfully added new log entry..';
        helper.clearForm(component);
        helper.hideSpinner(component);
        helper.displayToast(component, 'success', message);
        component.find('logList').refreshData();
    },

    handleSaveEntryError: function(component, response, helper) {
        console.log('Error adding entry ' + JSON.stringify(response));
        helper.hideSpinner(component);
        helper.displayToast(component, 'error', 'Error saving log entry.. ' + response.errors);
    },

    // CALLOUT to get all question categories to display buttons on add log entry form
    loadQuestionCategories: function(component, helper){
        component.find("apexService").callApexWithErrorCallbackWithHelper(
                component,
                helper,
                "c.getAllQuestionCategories",
                {},
                this.handleQuestionLoadSuccess,
                this.handleQuestionLoadError
            );
    },

    handleQuestionLoadSuccess: function(component, response, helper) {
        component.set('v.handoverQuestionCategories', response.resultObjects[0]);
    },

    handleQuestionLoadError: function(component, response, helper) {
        helper.displayToast(component, 'error', message);
    },

    // Finds handover document that is tied to this shift
    // Also sets selectedPostCurrentShift that is used to pass to other data
    // And text on buttons
    findHandoverDocumentAndSelectedPostCurrentShift: function (component, event, helper){
        var selectedPost = component.get('v.selectedPost');
        var postsWithActiveShift = component.get('v.postsWithActiveShift');
        var arrayOfIds = [];
        postsWithActiveShift.forEach((item) => {
            if(item.Id === selectedPost){
                component.set('v.selectedPostCurrentShift', item.Shift_Assignements__r[0]);
                component.set('v.buttonText', 'Create Log Entry for ' + item.Shift_Assignements__r[0].Post__r.Post_Name__c);
                component.set('v.inputText','Text of a log entry for ' + $A.localizationService.formatDate(item.Shift_Assignements__r[0].CreatedDate, "MMM. dd, yyyy") + ' - ' + item.Shift_Assignements__r[0].Shift_Cycle__c + ' Shift.');

                component.find("apexService").callApexWithErrorCallbackWithHelper(
                    component,
                    helper,
                    "c.findHandoverDocumentByShift",
                    {"shiftId" : item.Shift_Assignements__r[0].Id},
                    this.handleHandoverDocumentSuccess,
                    this.handleHandoverDocumentError
                );

                //console.log('ITEM: ' +JSON.stringify(item));
                if (item.Shift_Lead_Post__c) {
                    var user = $A.get("$SObjectType.CurrentUser.Id");
                    if (item.Shift_Assignements__r[0].Primary__c == user) {
                        component.set('v.shiftLeadPostButton', true);
                    } else {
                        component.set('v.shiftLeadPostButton', false);
                    }
                    console.log('in true: ' +item.Shift_Assignements__r[0].Id);
                    component.set('v.shiftLeadPost', true);
                    component.set('v.shiftId', item.Shift_Assignements__r[0].Id);
                    console.log('after set before dispatch: ' +component.get('v.shiftId'));
                    var passShiftIdEvent = component.getEvent('selectedShiftId');
                    console.log('child event: ' +passShiftIdEvent);
                    passShiftIdEvent.setParams({shiftId: component.get('v.shiftId')});
                    passShiftIdEvent.fire();
                    console.log('after dispatch');
                } else {
                    console.log('in false');
                    component.set('v.shiftLeadPost', false);
                }

            }
            //console.log('ITEM 2: ' +JSON.stringify(item));
            arrayOfIds.push(item.Shift_Assignements__r[0].Id);
        });

        if (component.get('v.selectedPost')) {
            helper.eventToLoadSelectedPostTasks(component,event, helper);
            console.log('selectedPost: ' +JSON.stringify(component.get('v.selectedPost')));
        }

        console.log('array: ' +arrayOfIds);
        component.set('v.shiftIds', arrayOfIds);
        console.log('cmp array: ' +component.get('v.shiftIds'));

    },

    eventToLoadSelectedPostTasks : function(component, event, helper) {
        var passPostIdForTaskCmp = component.getEvent("postIdForTask");
        passPostIdForTaskCmp.setParams({postId: component.get('v.selectedPost'),
        postName: component.get('v.selectedPostCurrentShift').Post__r.Post_Name__c});
        passPostIdForTaskCmp.fire();
    },

    // This locks adding entries if handover is in progress and sends id to parent component
    handleHandoverDocumentSuccess: function(component, response, helper) {
        // Check if handover is in progress for selected post
        if(response.resultObjects[0].Shift_Assignement__r.Shift_Status__c === 'Handover in progress') {
            component.set('v.handoverInProgress', true);
        } else {
            component.set('v.handoverInProgress', false);
        }
        component.set('v.handoverDocumentIdCurrentShift', response.resultObjects[0].Id);
    },

    handleHandoverDocumentError: function(component, response, helper) {
        console.log('Error getting handover document id ' + JSON.stringify(response));
        helper.hideSpinner(component);
        helper.displayToast(component, 'error', 'Error getting handover data ' + response.errors);
    },

    // This clears form after successful insert of entry
    clearForm: function(component){
        component.set("v.newLogEntry", {
                    'User_Log_Entry__c': null,
                    'Post__c': null,
                    'User__c': null,
                    'Shift_Assignement__c': null,
                    'Operating_Field_AMU__c': null});
    },

    // This sets default post for shift primary
    // So that if user is primary on shift it will default to his post
    // and also sets current Shift for this selected post
    setDefaultPostAndSelectedPostCurrentShift: function (component){

        var postsWithActiveShift = component.get('v.postsWithActiveShift');
        var currentShift = component.get('v.currentShift');
        console.log('Current Shift: ' +JSON.stringify(currentShift));

        if (currentShift) {
            postsWithActiveShift.forEach((item) => {
                    if(item.Id === currentShift.Post__c){
                        item.selected = true;
                        component.set('v.selectedPost', item.Id);
                        component.set('v.selectedPostCurrentShift', item.Shift_Assignements__r[0]);
                    }
            });

            if (currentShift.Post__r.Shift_Lead_Post__c) {
                component.set('v.shiftLeadPost', true);
            }

        } else if (postsWithActiveShift.length > 0) {
            component.set('v.selectedPost', postsWithActiveShift[0].Id);
            component.set('v.selectedPostCurrentShift', postsWithActiveShift[0].Shift_Assignements__r[0]);
        }
    },

    // HELPERS
    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },

    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 10000
        });
        toastEvent.fire();
    },
})