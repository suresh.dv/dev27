/**
 * Created by MarcelBrimus on 27/05/2019.
 */
({
    init: function (component, evt, helper) {
        // Set default value on post based on users current shift
        helper.setDefaultPostAndSelectedPostCurrentShift(component);
        helper.loadQuestionCategories(component, helper);
    },

    // On init, this will preselect post for shift primary
    // and also sets current shift for this selected post
    onPostsAndCurrentShiftChange: function(component, evt, helper) {
        helper.setDefaultPostAndSelectedPostCurrentShift(component);
    },

    // Change handover document and set shift id when user changes post
    onPostChange: function(component, evt, helper) {
        component.set('v.shiftLeadPostButton', true);
        helper.findHandoverDocumentAndSelectedPostCurrentShift(component, evt, helper);
    },

    // This fires event and parent component capture it. It will then set handoverDocID used by button
    // that is link to this doc...
    fireHandoverIdEvent: function(component, evt, helper) {
        var handoverDocumentEvent = component.getEvent("handoverDocumentIdChanged");
        handoverDocumentEvent.setParams({handoverId: component.get('v.handoverDocumentIdCurrentShift')});
        handoverDocumentEvent.fire();
    },

    saveNewLogEntry:function(component, event, helper) {
        helper.saveLogEntry(component, helper);
    },

    handleObjectProcess : function(component, event, helper) {
        console.log('handler');
        var parameter = event.getParam('summary');
        console.log('Param: ' +JSON.stringify(parameter));
        component.find('categoryButtons').passedSummary(parameter);
    },

    handleSave : function(component, event, helper) {
        console.log('handling save');
        component.find('loadSummary').getSummaries();
    },

    editShiftSummary : function(component, event, helper) {
        component.find("shiftSummaryObject").editMode();
        component.set('v.shiftLeadPostButton', false);
    },

    enableSummaryEditButton : function(component, event, helper) {
        component.set('v.shiftLeadPostButton', true);
        console.log('starting init');
        component.find('shiftSummaryHandovers').refreshData();
        //component.find('shiftSummaryHandovers').refreshData();
        //todo refresh aj tasky takto
        //$A.get('e.force:refreshView').fire();
    }


})