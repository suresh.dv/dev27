/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-03 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Fire event with submit param set to true for parent component (EPD_Form) to handle submit action.
    */
    submit : function(component) {
        var saveRecordEvt = component.getEvent("saveRecords");
        saveRecordEvt.setParams({
            submit : true
        });
        saveRecordEvt.fire();
    },

});