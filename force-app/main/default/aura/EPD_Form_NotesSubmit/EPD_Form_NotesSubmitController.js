/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-03 - Created. - EPD R1
*************************************************************************************************/
({

    submit : function(component, event, helper) {
        helper.submit(component);
    },

});