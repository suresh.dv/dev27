/**
 * Created by MarcelBrimus on 09/10/2019.
 */

({
    handleSave : function(component, helper, event, checklistValues) {
        var helper = this;
        var checkListPayload = checklistValues.fields;
        checkListPayload.Id = component.get('v.closeOutChecklistId');

        var payload = {
            actionName : component.get('v.actionMode'),
            actionComment : component.get('v.comment'),
            currentLatitude : component.get('v.latitude'),
            currentLongitude : component.get('v.longitude'),
            onHoldReason : component.get('v.onHoldReason')
        }

        var requestBody = {
            activity : component.get('v.workOrderActivity'),
            tradesman : component.get('v.tradesman'),
            checklist : JSON.stringify(checklistValues.fields),
            request : JSON.stringify(payload)
        }

        component.set("v.actionInProgress", true);
        component.find("apexService").callApexWithAllCallbacks(
            component,
            helper,
            "c.saveAction",
            requestBody,
            helper.handleSaveSuccess,
            helper.handleResponseError,
            helper.handleServerError
        );

    },

    handleSaveSuccess : function(component, requestResult, helper) {
        helper.refreshWorkflow(component);
        helper.showToast("Success!", "success", component.get('v.actionName') + " has been successful.");
    },

    handleResponseError : function(component, requestResult, helper) {
        if(requestResult.resultObjects && !requestResult.resultObjects[0]){
            helper.needRefresh(component);
        } else if(requestResult.errors){
            component.set("v.actionInProgress", false);
            component.set('v.errors', requestResult.errors)
        } else {
            helper.showToast("Error!", "error", helper.getError(requestResult));
            helper.goToView(component);
        }
    },

    handleServerError : function(component, requestResult, helper) {
        console.log('handleServerError ' + JSON.stringify(requestResult));
        helper.showToast("Error!", "error", helper.getErrorMessage(requestResult));
        helper.goToView(component);
    },

    goToView : function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'VIEW'});
        workflowAction.fire();
    },

    /**
    Extract server error message for display.
    */
    getErrorMessage : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0].message;
        }
        return 'Unknown error';
    },

    /**
    Extract response error message for display.
    */
    getError : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0];
        }
        return 'Unknown error';
    },

    needRefresh: function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'Need Refresh'});
        workflowAction.fire();
    },

    refreshWorkflow : function(component){
        var refreshWorkflowEngine = component.getEvent("workflowRefreshEvent");
        refreshWorkflowEngine.fire();
    },
    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
});