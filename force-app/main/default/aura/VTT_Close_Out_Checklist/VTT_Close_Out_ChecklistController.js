/**
 * Created by MarcelBrimus on 09/10/2019.
 */

({
    init: function (component, event, helper) {
        var today = $A.localizationService.formatDate(new Date(), "MM/dd/yy");
        component.set('v.today', today);
    },

    viewMode: function(component, evt, helper) {
        helper.goToView(component);
    },

    handleFormLoad: function(component, event, helper){
        component.set('v.actionInProgress', false);
    },

    onCloseOutFormSubmit: function(component, event, helper){
        console.log('onCloseOutFormSubmit called ' + component.get('v.comment'));
        event.preventDefault();       // stop the form from submitting
        var payload = event.getParams();
        helper.handleSave(component, event, helper, payload);
    },

    navigateTo: function (component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.tradesman.Id")
        });
        navEvt.fire();
    },
});