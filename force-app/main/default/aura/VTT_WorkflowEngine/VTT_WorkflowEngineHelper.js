/**
 * Created by MarcelBrimus on 25/09/2019.
 */

({
    suppressEPD: function(component, event, helper) {
        helper.showSpinner(component);
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.suppressEPD",
            { "workOrderActivityId" : component.get("v.recordId"),
            "flag" : component.get("v.suppressEPD")},
            this.handleSuppressEPDSuccess,
            this.handleResponseError
        );
    },

    handleSuppressEPDSuccess: function(component, response, helper){
        helper.hideSpinner(component);
        var message = '';
        if(component.get("v.suppressEPD")){
            message = 'Successfully disabled EPD'
        } else {
            message = 'Successfully enabled EPD'
        }
        helper.showToast("Success!", "success", message);
        helper.goToView(component);
        helper.loadState(component, event, helper);
    },

    loadState: function(component, event, helper) {
        helper.showSpinner(component);

        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.loadEngineState",
            { "workOrderActivityId" : component.get("v.recordId")},
            this.handleLoadSuccess,
            this.handleLoadError
        );
    },

    handleLoadSuccess: function(component, response, helper){
        helper.hideSpinner(component);
        if(response.resultObjects[0]){
           component.set('v.messages', response.resultObjects[0].messages);
           component.set('v.canManageAssignments', response.resultObjects[0].canManageAssignments);

           // EPD
           if(response.resultObjects[0].isEPDFormRequired &&
            response.resultObjects[0].isEPDFormAvailable &&
            !response.resultObjects[0].hasEPDSubmitted){
                component.set('v.showDisableEPD', true);
           } else {
               component.set('v.showDisableEPD', false);
           }

           if(response.resultObjects[0].activity.Maintenance_Work_Order__r.EPD_Suppressed__c &&
           response.resultObjects[0].isEPDFormAvailable){
               component.set('v.showEnableEPD', true);
           } else {
               component.set('v.showEnableEPD', false);
           }

           var activities = [];
           activities.push(response.resultObjects[0].activity);
           component.set('v.activity', activities);
           component.set('v.tradesmanInfo', response.resultObjects[0].tradesmanInfo);
           component.set('v.actions', response.resultObjects[0].availableActions);
           helper.initializeActionButtons(component,
                                    helper,
                                    response.resultObjects[0].availableActions,
                                    response.resultObjects[0].activity,
                                    response.resultObjects[0].tradesman);

           // Set Work Order Id
           var workflowAction = component.getEvent("workOrderDataAction");
           workflowAction.setParams({workOrderId: response.resultObjects[0].activity.Maintenance_Work_Order__c});
           workflowAction.fire();
        }
    },

    handleLoadError: function(component, response, helper){
        helper.hideSpinner(component);
        if(response.resultObjects[0]){
            var messages = [];
            messages.push(response.resultObjects[0]);
            component.set('v.messages', messages);
        }
    },

    initializeActionButtons: function(component, helper, data, activity, tradesman){
        var buttonsToRender = helper.getButtonsToRender(component, helper, data, activity, tradesman);

        // First destroy all buttons
        component.set("v.body", []);
        $A.createComponents(buttonsToRender
        ,function(components, status, errorMessage){
            if (status === "SUCCESS") {
               var body = component.get("v.body");
               components.forEach(function(item){
                   body.push(item);
               });
               component.set("v.body", body);
            } else if (status === "INCOMPLETE") {
                console.log("No response from server or client is offline.")
                // Show offline error
            } else if (status === "ERROR") {
                console.log("Error: " + errorMessage);
                // Show error message
            }
        });
    },

    // Returns array of buttons to be rendered based on state of data
    getButtonsToRender: function(component, helper, data, activity, tradesman){
        var buttonsToRender = [];
        if(data){
            data.forEach((item) => {
                var button = ["c:VTT_WorkflowActionButton",{
                    "buttonLabel" : item.buttonLabel,
                    "actionName" : item.actionName,
                    "workOrderActivity" : activity,
                    "tradesman" : tradesman,
                }];
                buttonsToRender.push(button);
            });
        }
        return buttonsToRender;
    },

    goToView : function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'VIEW'});
        workflowAction.fire();
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },

    handleResponseError : function(component, requestResult, helper) {
        helper.hideSpinner(component);
        helper.showToast("Error!", "error", helper.getError(requestResult));
    },

    /**
    Extract server error message for display.
    */
    getErrorMessage : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0].message;
        }
        return 'Unknown error';
    },

    /**
    Extract response error message for display.
    */
    getError : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0];
        }
        return 'Unknown error';
    },

    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
});