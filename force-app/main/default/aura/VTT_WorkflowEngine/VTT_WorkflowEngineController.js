/**
 * Created by MarcelBrimus on 25/09/2019.
 */

({
    init: function (component, evt, helper) {
        console.log("Initialize Workflow Engine")
        helper.loadState(component, evt, helper);
    },

    showManageAssignments: function(component, event, helper){
        component.set('v.showManageAssignment', true);
    },

    hideManageAssignments: function(component, event, helper){
        component.set('v.showManageAssignment', false);
    },

    handleManageAssignmentsSuccess: function(component, event, helper){
        component.set('v.showManageAssignment', false);
        helper.loadState(component, event, helper);
        helper.goToView(component);
    },

    enableEPD: function (component, evt, helper) {
        component.set("v.suppressEPD", false)
        helper.suppressEPD(component, evt, helper);
    },

    disableEPD: function (component, evt, helper) {
        component.set("v.suppressEPD", true)
        helper.suppressEPD(component, evt, helper);
    },
});