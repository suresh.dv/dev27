/**
 * Created by MarosGrajcar on 7/27/2019.
 */

({
    doInit : function(component, event, helper) {
        helper.loadHandover(component);
    },

    saveHandover : function(component,event,helper) {
        helper.saveHandoverRecord(component,event,helper);
    },

    acceptSelectedHandover : function(component,event,helper) {
        helper.acceptPassedHandover(component,event,helper);
    },

    reviewed : function(component, event, helper) {
        helper.submitReviewedHandoverAndLock(component, event, helper);
    },

    reviewedByShiftEngineer : function(component, event, helper) {
        helper.submitReviewedByShiftEngineer(component, event, helper);
    },

    disableOrEnableEngineerButton : function(component, event, helper) {
        var checkbox = component.find('shiftEngineer').get('v.checkboxState');
        component.set('v.disableShiftEngineerButton', !checkbox);
    },

    changeOperator : function(component, event, helper) {
        component.set('v.changeOperator', true);
        component.set('v.isViewState', false);
    },

    changeState : function(component) {
        component.set('v.changeOperator', false);
        component.set('v.isViewState', true);
    },

    componentReload : function(component, event, helper) {
        component.getEvent('eventToRefreshWrapper').fire();
    },

});