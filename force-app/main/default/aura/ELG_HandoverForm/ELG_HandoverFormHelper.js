/**
 * Created by MarosGrajcar on 5/28/2019.
 */

({
    /*Load Section*/
    loadHandover : function(component) {
        var helper = this;
        var user = $A.get("$SObjectType.CurrentUser.Id");
        component.set('v.loading', true);
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.loadHandoverFromDatabase",
            { handoverForm : component.get("v.recordId"),  currentUserId : user},
            this.loadSuccess,
            this.errorCallback);

    },

    /*Load on HandoverForm Component*/
    loadSuccess : function(component, requestResult, helper) {
        var isReadOnly = requestResult.resultObjects[0][1];
        component.set("v.handoverForm", requestResult.resultObjects[0][0]);
        component.set("v.sectionEntries", requestResult.resultObjects[1]);
        component.set("v.userCannotReview", requestResult.resultObjects[2]);

        helper.setDefaultAttributes(component, event, helper);
        helper.loadLogEntries(component, event, helper);
        helper.prepareHandoverCmpState(component, isReadOnly);
        helper.getUserForValidation(component);
        component.set('v.loading', false);
        console.log('Success');

        var eventToSetParent = component.getEvent('setUserReviewPermissionSetOnParent');
        eventToSetParent.setParams({'userCannotReview': component.get('v.userCannotReview')});
        eventToSetParent.fire();
    },

    errorCallback : function(component, requestResult, helper) {
        console.log('Error on Handover load');
        component.set('v.loading', false);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();
    },

    /*Load Section load Log entries of Additional Items*/
    loadLogEntries : function(component, event, helper) {
        component.find("apexService").callApexWithHelper(component,
            helper,
            "c.loadGenericLogEntries",
            { shiftAssignmentId : component.get("v.handoverForm.Shift_Assignement__c") },
            this.entriesLoaded);
    },

    /*Set loaded log entries to array*/
    entriesLoaded : function(component, requestResult, helper) {
        var listOfLogEntries = requestResult.resultObjects[0];
        component.set('v.logEntries', listOfLogEntries);
    },

    setDefaultAttributes : function(component, event, helper) {
        component.set('v.isViewState', false);
        component.set('v.isReviewable', false);
        component.set('v.isInProgress', false);
        component.set('v.isReadOnly', false);
        component.set('v.isShiftEngineerRequired', false);
        component.set('v.isUserShiftEngineer', false);
        component.set('v.isEngineerShiftPrimary', false);
        component.set('v.isUserPrimary', false);
    },

    /*Prepare component state of Handover Component*/
    prepareHandoverCmpState : function(component, isReadOnly) {
        //get correct state
        var handoverStatus = component.get('v.handoverForm').Status__c;

        if (handoverStatus == 'Accepted') {
            component.set('v.isInProgress', false);
            component.set('v.isReviewable', true);
            component.set('v.isViewState', true);
        } else if (handoverStatus == 'In Progress') {
            component.set('v.isInProgress', true);
            component.set('v.isViewState', true);
        }

        if (isReadOnly) {
            component.set('v.isReadOnly', true)
        }

        if (component.get('v.shiftLeadPost')) {
            component.set('v.isReadOnly', true)
        }

        console.log('Status: '+component.get('v.shiftLeadPost'))
        console.log('Status 2 : '+component.get('v.isReadOnly'))
    },

    getUserForValidation : function(component) {
        var user = $A.get("$SObjectType.CurrentUser.Id");
        var shiftEngineer = component.get('v.handoverForm').Shift_Assignement__r.Shift_Engineer__c;
        var shiftPrimary = component.get('v.handoverForm').Shift_Assignement__r.Primary__c;
        var incomingOperator = component.get('v.handoverForm').Incoming_Operator__c;
        var isShiftEngineerRequired = component.get('v.handoverForm').Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c;

        if(user == incomingOperator) {
            component.set('v.isUserIncomingOperator', true);
        }

        if (user == shiftEngineer) {
            component.set('v.isUserShiftEngineer', true);
        }

        if(user == shiftPrimary) {
            component.set('v.isUserPrimary', true);
        }

        if(isShiftEngineerRequired) {
            component.set('v.isShiftEngineerRequired', isShiftEngineerRequired);
            component.set('v.disableShiftEngineerButton', true);
        }

        if(shiftPrimary == shiftEngineer) {
            component.set('v.isEngineerShiftPrimary', true);
        }
    },

});