/**
 * Created by MarcelBrimus on 10/01/2020.
 */

({
    load: function(component, event, helper) {
        helper.showSpinner(component);
        component.set('v.spinnerText', 'Loading Data...');

        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.loadSNRData",
            {   "recordId" : component.get("v.recordId"),
                "objectName" : component.get("v.sObjectType")},
            this.handleLoadSuccess,
            this.handleLoadError
        );
    },

    handleLoadSuccess: function(component, response, helper){
        helper.hideSpinner(component);

        component.set('v.FLOCData', response.FLOCData);

        component.set('v.serviceCategories', response.serviceCategories);
        if(response.serviceCategories && response.serviceCategories.length > 0) {
            component.set('v.serviceCategoryId', response.serviceCategories[0].Id);
        }

        component.set('v.serviceTypes', response.serviceTypes);
        if(response.serviceTypes && response.serviceTypes.length > 0) {
            component.set('v.serviceTypeId', response.serviceTypes[0].Id);
        }

        component.set('v.serviceActivities', response.serviceActivities);
        if(response.serviceActivities && response.serviceActivities.length > 0) {
            component.set('v.serviceActivityId', response.serviceActivities[0].Id);
        }

        component.set('v.servicePriorities', response.servicePriorities);
        component.set('v.serviceRequired', response.serviceRequired);
    },

    handleLoadError: function(component, response, helper){
        console.log('Error loading data ' + response);
        helper.hideSpinner(component);
    },

    handleServiceRequiredChange: function(component, event, helper){
        helper.showSpinner(component);
        component.set('v.spinnerText', 'Loading Data...');
        // Reset picklists
        component.set('v.serviceSpecifics', null);
        component.set('v.serviceSpecificId', null);
        if(!component.get('v.serviceSpecificId')){
            component.set('v.selectedVendorId', null);
        }

        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.loadServiceSpecifics",
            {
                "serviceCategoryId" : component.get("v.serviceCategoryId"),
                "serviceTypeId" : component.get("v.serviceTypeId"),
                "serviceActivityId" : component.get("v.serviceActivityId"),
                "serviceRequiredId" : component.get("v.serviceRequiredId"),
                "recordId" : component.get("v.recordId"),
                "objectName" : component.get("v.sObjectType")
            },
            this.handleServiceRequiredChangeSuccess,
            this.handleServiceRequiredChangeError
        );
    },

    handleServiceRequiredChangeSuccess: function(component, response, helper){
        helper.hideSpinner(component);
        component.set('v.serviceSpecifics', response.serviceSpecifics);
        component.set('v.FLOCData', response.FLOCData);
    },

    handleServiceRequiredChangeError: function(component, response, helper){
        console.log('Error loading data handleServiceRequiredChangeSuccess' + response);
        helper.showToast("Error!", "error", response);
        helper.hideSpinner(component);
    },

    handleSave : function(component, helper, event) {
        var helper = this;
        if(helper.validateForm(component, helper, event)){
            var snrRequest = {
                workDetails : component.get('v.workDetails'),
                title : component.get('v.title'),
                malfunctionStartDate : component.get('v.malfunctionStartDate'),
                serviceCategoryId : component.get('v.serviceCategoryId'),
                serviceTypeId : component.get('v.serviceTypeId') ,
                serviceActivityId : component.get('v.serviceActivityId') ,
                servicePriorityId : component.get('v.servicePriorityId') ,
                serviceRequiredId : component.get('v.serviceRequiredId') ,
                serviceSpecificsId : component.get('v.serviceSpecificId') ,
                vendorCompanyId: component.get('v.selectedVendorId'),
                equipmentDown: component.get('v.equipmentDown'),
                productionImpacted: component.get('v.productionImpacted')
            }

           var requestBody = {
               objectName : component.get('v.sObjectType'),
               flocModel : JSON.stringify(component.get('v.FLOCData')),
               snrRequest : JSON.stringify(snrRequest)
           }

           helper.showSpinner(component);
           component.set('v.spinnerText', 'Saving Service Notification Request, do not leave the page...');

           component.find("apexService").callApexWithAllCallbacks(
               component,
               helper,
               "c.save",
               requestBody,
               helper.handleSaveSuccess,
               helper.handleResponseError,
               helper.handleServerError
           );
        }
    },

    handleSaveSuccess : function(component, requestResult, helper) {
        helper.hideSpinner(component);
        helper.showToast("Success!", "success", "Service Notification Request Saved successfully.");
        helper.closeModalAndRefresh();
    },

    closeModalAndRefresh: function(){
        $A.get("e.force:closeQuickAction").fire();
        $A.get("e.force:refreshView").fire();
    },

    handleResponseError : function(component, requestResult, helper) {
        console.log('error ' + JSON.stringify(requestResult));
        helper.hideSpinner(component);
        helper.showToast("Error!", "error", helper.getError(requestResult));
    },

    handleServerError : function(component, requestResult, helper) {
        helper.hideSpinner(component);
        helper.showToast("Error!", "error", helper.getErrorMessage(requestResult));
    },

    validateForm : function(component, helper, event){
        var allValid = [].concat(component.find('validate')).reduce(function (validFields,
            inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        return allValid;
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },

    handleResponseError : function(component, requestResult, helper) {
        helper.hideSpinner(component);
        helper.showToast("Error!", "error", helper.getError(requestResult));
    },

    /**
    Extract server error message for display.
    */
    getErrorMessage : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0].message;
        }
        return 'Unknown error';
    },

    /**
    Extract response error message for display.
    */
    getError : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0];
        }
        return 'Unknown error';
    },

    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
});