/**
 * Created by MarcelBrimus on 10/01/2020.
 */

({
    init: function (component, evt, helper) {
        helper.load(component, evt, helper);
    },

    onServiceRequiredChange: function(component, evt, helper){
        console.log('service required changed');
        helper.handleServiceRequiredChange(component,evt,helper);
    },

    saveSNR: function(component, evt, helper){
        helper.handleSave(component, evt, helper);
    },

    cancel: function(component, evt, helper){
        $A.get("e.force:closeQuickAction").fire();
    },

    resetValues: function(component, evt, helper){
        if(!component.get('v.equipmentDown')){
            component.set('v.malfunctionStartDate', null);
        }
    }
});