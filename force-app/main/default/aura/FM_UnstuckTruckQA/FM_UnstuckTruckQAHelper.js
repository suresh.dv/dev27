/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
History:        jschn 2019-02-12 - Created.
*************************************************************************************************/
({
    loadStuckTruck : function(component, helper) {
        component.find("apexService").callApexWithErrorCallbackWithHelper(
                component,
                helper,
                "c.getActiveStuckTruckId",
                { "truckTripId" : component.get("v.recordId") },
                this.getStuckTruckSuccessHandler,
                this.getStuckTruckFailureHandler
        );
    },

    getStuckTruckSuccessHandler : function(component, response, helper) {
        component.set("v.stuckTruckId", response.resultObjects[0]);
        component.set("v.showError", false);
        component.set("v.showEditRecord", true);
        helper.toggleSpinner(component);
    },

    getStuckTruckFailureHandler : function(component, response, helper) {
        component.set("v.errorMessage", response.errors[0]);
        component.set("v.showError", true);
        component.set("v.showEditRecord", false);
        helper.toggleSpinner(component);
    },

    toggleSpinner : function(component) {
        var spinner = component.get("v.loading");
        component.set("v.loading", !spinner);
    },

    prepopulateStuckTruck : function(component) {
        var truckTrip = component.get("v.truckTrip");
        component.find("Status").set("v.value", "Unstuck");
        component.find("StuckTo").set("v.value", new Date().toISOString());
    },

    redirectToView : function(component, event) {
        var recordId = component.get("v.stuckTruckId");
        $A.get("e.force:refreshView").fire();
        setTimeout(function() {
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    actionName: 'view',
                    recordId: recordId
                }
            };
            navService.navigate(pageReference, true);
        }, 250);

    },

})