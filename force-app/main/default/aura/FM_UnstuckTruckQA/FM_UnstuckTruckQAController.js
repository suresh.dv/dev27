/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    XXX
History:        jschn 2019-02-12 - Created.
*************************************************************************************************/
({
    doInit : function(component, event, helper) {
        helper.loadStuckTruck(component, helper);
    },

    handleEditFormLoad : function(component, event, helper) {
        helper.prepopulateStuckTruck(component);
    },

    handleSuccess : function(component, event, helper) {
        helper.redirectToView(component, event);
    },

})