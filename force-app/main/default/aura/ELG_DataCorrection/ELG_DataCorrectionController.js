/**
 * Created by MarcelBrimus on 19/06/2019.
 */
({
    init: function (component, evt, helper) {
        component.set("v.currentUserId", $A.get("$SObjectType.CurrentUser.Id"));
        helper.loadAvailableShifts(component);
    },

    onShiftForCorrection: function (component, evt, helper) {
        helper.setFirstItemAsDefault(component);
    },

    // Check if Primary is filled before submit
    onRecordSubmit: function(component, event, helper) {
        event.preventDefault();
        var fields = event.getParam("fields");
        if(!fields["Primary__c"]){
           component.set('v.isPrimaryFilled', false);
        } else {
           component.set('v.isPrimaryFilled', true);
           component.find('shiftDataCorrection').submit(fields);
        }
    },

    onSelectedShiftChange: function (component, evt, helper) {
        component.set("v.disableButton", true);
        helper.setSelectedShiftObject(component);
    },

    handleSaveSuccess : function(component, event, helper) {
        var shiftDataCorrection = component.getEvent('shiftDataCorrection');
        shiftDataCorrection.fire();
        var refreshLogEntriesEvent = $A.get("e.c:ELG_ShiftDataCorrectionAppEvent");
        refreshLogEntriesEvent.fire();
        component.find("overlayLib").notifyClose();
        $A.get('e.force:refreshView').fire();
    },

    handleFormLoad : function(component, event, helper) {
        component.set("v.disableButton", false);
    },

    onCancel: function(component, event, helper) {
        component.find("overlayLib").notifyClose();
    },
})