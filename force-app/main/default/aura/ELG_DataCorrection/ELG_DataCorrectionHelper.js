/**
 * Created by MarcelBrimus on 19/06/2019.
 */
({
    // Load data
    loadAvailableShifts: function(component) {
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getBaseShiftHandoverData",
            {},
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess: function(component, response, helper){
        var responseNew = response.resultObjects[0];
        var availablePostsKey = 'AVAILABLEPOSTS';

        if(responseNew[availablePostsKey]){
           helper.filterOutPostsWithNoShift(component, responseNew[availablePostsKey]);
        }
        component.set("v.disableButton", false);
    },

    handleError: function(component, response, helper){
        console.log(JSON.stringify(response));
    },

    // Only allow edit post where there is at least one active shift
    // Filter out posts with no shift, shifts with wrong status or shifts where
    filterOutPostsWithNoShift: function (component, availablePosts){
        var shiftsAvailableForTakeOver = [];
        var shiftForCorrectionMap = new Map();

        availablePosts.forEach((item) => {
            if(item.Shift_Assignements__r){
                if( item.Shift_Assignements__r[0].Shift_Status__c === 'New'
                    && item.Shift_Assignements__r[0].Primary__c){
                     shiftForCorrectionMap.set(item.Shift_Assignements__r[0].Id, item.Shift_Assignements__r[0]);
                     shiftsAvailableForTakeOver.push(item.Shift_Assignements__r[0]);
                }
            }
        });
        component.set('v.shiftForCorrectionMap', shiftForCorrectionMap);
        component.set('v.shiftForCorrection', shiftsAvailableForTakeOver);
    },

    setSelectedShiftObject: function (component){
        var selectedShiftID = component.get('v.selectedShift');
        var selectedShift = component.get('v.shiftForCorrectionMap').get(selectedShiftID);
        component.set('v.selectedShiftObject', selectedShift);
    },

    setFirstItemAsDefault: function (component){
        if(component.get('v.shiftForCorrection').length > 0){
             component.set('v.selectedShift', component.get('v.shiftForCorrection')[0].Id);
        }
    },

    // HELPERS
    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 10000
        });
        toastEvent.fire();
    },
})