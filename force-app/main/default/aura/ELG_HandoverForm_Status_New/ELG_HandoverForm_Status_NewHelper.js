/**
 * Created by MarosGrajcar on 7/29/2019.
 */

({
    /*Save section*/
    /*Save pass handover and update status*/
    saveHandoverRecord : function(component, event, helper) {
        component.set('v.buttonLabel', 'Processing');
        component.set('v.disableButton', true);
        //var operator = component.find("incomingOperatorId").get('v.incomingOperator');
        //component.set('v.handoverForm.Incoming_Operator__c', operator);
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.updateHandoverAndStatus",
            { "HOFormId" : component.get("v.recordId")},
            this.saveSuccessful,
            this.errorOnSave);

    },

    /*On successful save refresh page and close modal*/
    saveSuccessful : function(component, requestResult, helper) {
        component.set('v.disableButton', false);
        if (component.get('v.doRedirect')) {
            helper.navigateToShift(component, event, helper);
        } else {
            component.getEvent('reloadHandoverWrapper').fire();
        }
    },

    errorOnSave : function(component, requestResult, helper) {
        console.log('Error on Handover save.');
        component.set('v.disableButton', false);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();
        component.set('v.buttonLabel', 'Sign Off and Pass Handover');
        $A.get('e.force:refreshView').fire();
    },

     navigateToShift : function(component, event, helper) {
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
            componentDef : "c:ELG_ShiftHandover",
            isredirect : "true",
            componentAttributes: {}
            });
            evt.fire();
        },
});