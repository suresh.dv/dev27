({

	debugOn : function() {
		return false;
	},

	callApex : function(component, componentHelper, controllerMethod, actionParameters, successCallback, errorCallback, serverErrorCallback) {
		if(this.debugOn()) console.log("HOG_ApexService -> Controller method/Action Parameters: " + controllerMethod + "/" + JSON.stringify(actionParameters));
		var action = component.get(controllerMethod);

		if(actionParameters) {
			action.setParams(actionParameters);
		}
		
		action.setCallback(this, function(response) {
			this.handleResponse(component, componentHelper, response, successCallback, errorCallback, serverErrorCallback);
		});

		$A.enqueueAction(action);
	},

	handleResponse : function(component, componentHelper, response, successCallback, errorCallback, serverErrorCallback) {
		var state = response.getState();
		if(this.debugOn()) console.log("HOG_ApexService -> response state: " + state);

    	if(state === "SUCCESS") {
    		this.handleSuccessState(component, componentHelper, response, successCallback, errorCallback);
		} else if(state === "INCOMPLETE"){
			console.log("HOG_ApexService -> Load was incomplete.");
    	} else if(state === "ERROR") {
    		this.handleErrorState(component, componentHelper, response, serverErrorCallback);
    	}
	},

	handleSuccessState : function(component, componentHelper, response, successCallback, errorCallback) {
		var requestResponse = response.getReturnValue();
		// Check if correct object returned
		this.checkRequestResponse(requestResponse);
		if(requestResponse.success) {
			// Handle request with defined success callback.
			successCallback(component, requestResponse, componentHelper);
		} else {
			this.handleNonSuccess(component, requestResponse, componentHelper, errorCallback);
    	}
	},

	checkRequestResponse : function(requestResponse) {
		if(!requestResponse.success && requestResponse.success != false) {
			if(this.debugOn()) console.log("HOG_ApexService -> !!!-ERROR-!!! Success sate status is undefined, you've got wrong return type.");
			this.fireNotification("", "Wrong return type.", "Error");
		}
	},

	handleNonSuccess : function(component, requestResponse, componentHelper, errorCallback) {
		// if errorCallback is defined, handle error with that.
		// Otherwise log error and fire notification
		if(errorCallback) {
			if(this.debugOn()) console.log("HOG_ApexService -> Success state error calback handler.");
			errorCallback(component, requestResponse, componentHelper);
		} else {	
			this.defaultErrorCallback(requestResponse);
		}
	},

	defaultErrorCallback : function(requestResponse) {
		var errors = this.buildRequestResponseErrors(requestResponse);
		if(this.debugOn()) console.log("HOG_ApexService -> Erors: " + errors);
		this.fireNotification("", errors, "Error");
	},

	buildRequestResponseErrors : function(requestResponse) {
		var errors = "";
		if(requestResponse.errors) {
			requestResponse.errors.forEach(function(element) {
				errors += element + '\n';
			});
		} else {
			console.log("HOG_ApexService -> Type of returned object is wrong. HOG_RequestResponse instance is expected.")
		}
		return errors;
	},

	handleErrorState : function(component, componentHelper, response, serverErrorCallback) {
		var errors = response.getError();
		if(this.debugOn()) console.log("HOG_ApexService -> Error state error: " + errors);
		// Handle server error with serverErrorCallback if provided. otherwise log error and fire notification. 
		if(serverErrorCallback) {
			serverErrorCallback(component, errors, componentHelper);
		} else {
			this.defaultServerErrorCallback(errors);
	    }
    },

    defaultServerErrorCallback : function(errors) {
    	if(errors) {
            if(errors[0] && errors[0].message) {
                if(this.debugOn()) console.log("HOG_ApexService -> Error message: " + errors[0].message);
            } 
			this.fireNotification("", errors, "Error");
        } else {
            if(this.debugOn()) console.log("HOG_ApexService -> Unknown error");
			this.fireNotification("Unknown error", "HOG_ApexService has experienced Unknown error.", "Error");
        }
    },

})