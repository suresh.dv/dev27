({

	callApex : function(component, event, helper) {
		var params = event.getParams().arguments;
		var callerComponent = params.component;
		var controllerMethod = params.controllerMethod;
		var actionParameters = params.actionParameters;
		var successCallback = params.successCallback;
		helper.callApex(callerComponent, null, controllerMethod, actionParameters, successCallback, null, null);
	},

	callApexWithHelper : function(component, event, helper) {
		var params = event.getParams().arguments;
		var callerComponent = params.component;
		var componentHelper = params.componentHelper;
		var controllerMethod = params.controllerMethod;
		var actionParameters = params.actionParameters;
		var successCallback = params.successCallback;
		var errorCallback = params.errorCallback;
		helper.callApex(callerComponent, componentHelper, controllerMethod, actionParameters, successCallback, null, null);
	},

	callApexWithErrorCallback : function(component, event, helper) {
		var params = event.getParams().arguments;
		var callerComponent = params.component;
		var controllerMethod = params.controllerMethod;
		var actionParameters = params.actionParameters;
		var successCallback = params.successCallback;
		var errorCallback = params.errorCallback;
		helper.callApex(callerComponent, null, controllerMethod, actionParameters, successCallback, errorCallback, null);
	},

	callApexWithErrorCallbackWithHelper : function(component, event, helper) {
		var params = event.getParams().arguments;
		var callerComponent = params.component;
		var componentHelper = params.componentHelper;
		var controllerMethod = params.controllerMethod;
		var actionParameters = params.actionParameters;
		var successCallback = params.successCallback;
		var errorCallback = params.errorCallback;
		helper.callApex(callerComponent, componentHelper, controllerMethod, actionParameters, successCallback, errorCallback, null);
	},

	callApexWithAllCallbacks : function(component, event, helper) {
		var params = event.getParams().arguments;
		var callerComponent = params.component;
		var componentHelper = params.componentHelper;
		var controllerMethod = params.controllerMethod;
		var actionParameters = params.actionParameters;
		var successCallback = params.successCallback;
		var errorCallback = params.errorCallback;
		var serverErrorCallback = params.serverErrorCallback;
		helper.callApex(callerComponent, componentHelper, controllerMethod, actionParameters, successCallback, errorCallback, serverErrorCallback);
	},

})