/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-03 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    toggles render for COC Stages modal form (component)
    */
    toggleCOCStagesModal : function(component, showFlag) {
        component.set("v.showCOCStages", showFlag);
    },

});