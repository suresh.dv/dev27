/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-03 - Created. - EPD R1
*************************************************************************************************/
({

    showCOCStages : function(component, event, helper) {
        helper.toggleCOCStagesModal(component, true);
    },

    handleCloseModal : function(component, event, helper) {
        helper.toggleCOCStagesModal(component, false);
    },

});