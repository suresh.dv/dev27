/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-07-01 - Created. - EPD R1
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.runRefreshAction(component);
    },

});