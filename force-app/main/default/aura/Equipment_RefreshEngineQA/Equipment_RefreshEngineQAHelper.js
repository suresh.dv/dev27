/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-07-01 - Created. - EPD R1
*************************************************************************************************/
({

    //creates callout to apex endpoint that runs Refresh Action for child Engine record
    runRefreshAction : function(component) {
        this.showSpinner(component);
        var apexService = component.find("apexService");
        var recId = component.get("v.recordId");
        apexService.callApexWithErrorCallbackWithHelper(
                component,
                this,
                "c.refreshEngineData",
                { "equipmentId" : recId },
                this.handleValidCase,
                this.handleInvalidCase
        );
    },

    //Handles success case.
    //stops spinner, displays toast with success message and refresh view.
    handleValidCase : function(component, requestResult, helper) {
        component.set("v.showError", false);
        component.set("v.errorMessage", "");
        helper.sendToast(requestResult.successMessage);
        helper.hideSpinner(component);
        $A.get('e.force:refreshView').fire();
        $A.get("e.force:closeQuickAction").fire();
    },

    //Handles invalid case.
    //Stops spinner and shows the error message.
    handleInvalidCase : function(component, requestResponse, helper) {
        var errorMsg = "";
        requestResponse.errors.forEach(function(element) {
            errorMsg += element + "\n";
        });
        component.set("v.showError", true);
        component.set("v.errorMessage", errorMsg);
        helper.hideSpinner(component);
    },

    showSpinner : function(component) {
        component.set("v.loading", true);
    },

    hideSpinner : function(component) {
        component.set("v.loading", false);
    },

    sendToast : function(message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title" : "Success!",
            "type" : "success",
            "message" : message
        });
        toastEvent.fire();
    },

});