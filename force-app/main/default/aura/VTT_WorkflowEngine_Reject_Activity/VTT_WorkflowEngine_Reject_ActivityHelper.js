/**
 * Created by MarcelBrimus on 21/10/2019.
 */

({
    handleSave : function(component, helper, event) {
        var helper = this;

        if(helper.validateForm(component, helper, event)){
            var payload = {
                actionName : $A.get("{!$Label.c.VTT_LTNG_Reject_Activity_Action}"),
                actionComment : component.get('v.actionComment'),
                currentLatitude : component.get('v.latitude'),
                currentLongitude : component.get('v.longitude')
            }

            var requestBody = {
                activity : component.get('v.workOrderActivity'),
                tradesman : component.get('v.tradesman'),
                request : JSON.stringify(payload)
            }

            component.set("v.actionInProgress", true);
            component.find("apexService").callApexWithAllCallbacks(
                component,
                helper,
                "c.saveAction",
                requestBody,
                helper.handleSaveSuccess,
                helper.handleResponseError,
                helper.handleServerError
            );
        }
    },

    validateForm : function(component, helper, event){
        var allValid = [].concat(component.find('toCheck')).reduce(function (validFields,
            inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        return allValid;
    },

    handleSaveSuccess : function(component, requestResult, helper) {
        helper.refreshWorkflow(component);
        helper.showToast("Success!", "success", "Reject of Actvity has been successfully.");
    },

    handleResponseError : function(component, requestResult, helper) {
        console.log('error ' + JSON.stringify(requestResult));
        if(requestResult.resultObjects && !requestResult.resultObjects[0]){
           helper.needRefresh(component);
        } else {
           helper.showToast("Error!", "error", helper.getError(requestResult));
           helper.goToView(component);
        }

    },

    handleServerError : function(component, requestResult, helper) {
        helper.showToast("Error!", "error", helper.getErrorMessage(requestResult));
        helper.goToView(component);
    },

    /**
    Extract server error message for display.
    */
    getErrorMessage : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0].message;
        }
        return 'Unknown error';
    },

    /**
    Extract response error message for display.
    */
    getError : function(requestResult) {
        if(requestResult && requestResult.errors && requestResult.errors.length > 0) {
            return requestResult.errors[0];
        }
        return 'Unknown error';
    },

    goToView : function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'VIEW'});
        workflowAction.fire();
    },

    needRefresh: function(component){
        var workflowAction = component.getEvent("workflowEventAction");
        workflowAction.setParams({actionName: 'Need Refresh'});
        workflowAction.fire();
    },

    refreshWorkflow : function(component){
        var refreshWorkflowEngine = component.getEvent("workflowRefreshEvent");
        refreshWorkflowEngine.fire();
    },
    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
});