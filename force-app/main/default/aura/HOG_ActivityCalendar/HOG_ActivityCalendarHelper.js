/**
 * Created by MarcelBrimus on 22/08/2019.
 */

({
    // Initialize Calendar
    initCalendar: function(component, evt, helper) {

        var calEvents = component.get('v.events');

        var calendar = new FullCalendar.Calendar(component.find("calendar").getElement(), {
           plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
           themeSystem: 'bootstrap',
           events: calEvents,
           header: {
               left: 'prev,next today',
               center: 'title',
               right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
           },
           editable: false,
           eventLimit: true,
       });
       calendar.render();
    },

    // Load Events
    loadEvents: function(component, evt, helper) {
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.loadEvents",
            {},
            this.handleSuccess,
            this.handleError
        );
    },

    handleError: function(component, response, helper){
        console.log('Error' + JSON.stringify(response));
    },

    handleSuccess: function(component, response, helper){
        var activityEvents = response.resultObjects[0];
        var calendarEvents = [];

        activityEvents.forEach((item) => {
            console.log('Success ' + JSON.stringify(item));
            calendarEvents.push({
                title: item.title,
                start: item.startString,
                end: item.endString,
                className: item.className
            });
        });

        component.set('v.events', calendarEvents);
        helper.initCalendar(component, null, helper);
    },
});