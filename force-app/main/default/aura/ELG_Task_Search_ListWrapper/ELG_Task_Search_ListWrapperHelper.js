/**
 * Created by MarosGrajcar on 11/13/2019.
 */

({
    checkButtons : function(component, event, helper) {
        var listOfIds = component.get('v.taskIds');
        var taskQuantity = listOfIds.length;
        var position = component.get('v.currentPosition');

        if (position == (taskQuantity -1)) {
            component.set('v.disableForward', true);
        } else {
            component.set('v.disableForward', false)
        }

        if (position == 0) {
            component.set('v.disableBack', true);
        } else {
            component.set('v.disableBack', false);
        }
    },
});