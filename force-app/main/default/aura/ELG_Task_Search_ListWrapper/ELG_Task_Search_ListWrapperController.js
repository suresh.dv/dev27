/**
 * Created by MarosGrajcar on 11/13/2019.
 */

({
    doInit : function(component, event, helper) {
        var listOfIds = component.get('v.taskIds');
        component.set('v.currentTask', listOfIds[0]);
        component.set('v.selectedPage', 1);
        component.set('v.totalPages', listOfIds.length);
    },

    selectFirstTask : function(component, event, helper) {
        var listOfIds = component.get('v.taskIds');
        component.set('v.currentTask', listOfIds[0]);
        component.set('v.currentPosition', 0);
        component.set('v.selectedPage', 1);
        helper.checkButtons(component, event, helper);
    },

    selectPreviousTask : function(component, event, helper) {
        var listOfIds = component.get('v.taskIds');
        var position = component.get('v.currentPosition');
        if (position != 0) {
            position--;

            component.set('v.selectedPage', position+1);
            component.set('v.currentPosition', position);
            component.set('v.currentTask', listOfIds[position]);
        }
        helper.checkButtons(component, event, helper);
    },

    selectNextTask : function(component, event, helper) {
        var listOfIds = component.get('v.taskIds');
        var position = component.get('v.currentPosition');
        if ((position+1) != listOfIds.length) {
            position++;

            component.set('v.selectedPage', position+1);
            component.set('v.currentPosition', position);
            component.set('v.currentTask', listOfIds[position]);
        }
        helper.checkButtons(component, event, helper);
    },

    selectLastTask : function(component, event, helper) {
        var listOfIds = component.get('v.taskIds');
        var taskQuantity = listOfIds.length;
        component.set('v.currentTask', listOfIds[taskQuantity-1]);
        component.set('v.currentPosition', taskQuantity-1);
        component.set('v.selectedPage', taskQuantity);
        helper.checkButtons(component, event, helper);
    },

     componentReload : function(component, event, helper) {
        component.set('v.reloadComponent', false);
        component.set('v.reloadComponent', true);
    },
});