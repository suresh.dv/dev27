/**
 * Created by MarcelBrimus on 13/11/2019.
 */

({
    validate : function(component, helper, event){
        var allValid = [].concat(component.find('toCheck')).reduce(function (validFields,
            inputCmp) {
            if(!inputCmp){
                return true;
            }
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);
        return allValid;
    },
});