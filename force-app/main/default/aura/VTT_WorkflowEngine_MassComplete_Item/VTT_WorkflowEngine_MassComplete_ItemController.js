/**
 * Created by MarcelBrimus on 04/11/2019.
 */

({
    handleTimeChange: function(component, event, helper){
        var massCompleteDataChange = component.getEvent("massCompleteDataChange");
        massCompleteDataChange.setParams(
            {   isChecked: component.get('v.item.markToComplete'),
                durationValue: component.get('v.item.userDurationInput')
        });
        massCompleteDataChange.fire();
    },

    handleCheckChange: function(component, event, helper){
        var massCompleteDataChange = component.getEvent("massCompleteDataChange");
        massCompleteDataChange.setParams(
            {   isChecked: component.get('v.item.markToComplete'),
                durationValue: component.get('v.item.userDurationInput')
        });

        massCompleteDataChange.fire();

        // Not sure if this is needed here as input is in aura:if but just in case..
        if(!component.get('v.item.markToComplete')){
            component.set('v.item.userDurationInput', 0);
        }

    },

    isChecked: function(component){
        return component.get('v.item.markToComplete')
    },

    navigateTo: function(component, event, helper){
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": component.get("v.item.activityRecord.Location__c")
        });
        navEvt.fire();
    },

    validate: function(component, event, helper){
       return helper.validate(component, event, helper);
    }
});