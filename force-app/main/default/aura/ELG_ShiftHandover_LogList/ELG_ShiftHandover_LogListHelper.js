/**
 * Created by MarcelBrimus on 03/06/2019.
 */
({
    loadLogEntries: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.getAllLogEntriesForPostInFacility",
            { "post" : component.get("v.postId"), "facilityId" : component.get("v.facilityId")},
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess: function(component, response, helper){
        component.set("v.logs", response.resultObjects[0]);
        helper.hideSpinner(component);
    },

    handleError: function(component, response, helper){
        helper.hideSpinner(component);
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    }
})