/**
 * Created by MarcelBrimus on 03/06/2019.
 */
({
    init: function (component, evt, helper) {
        component.set("v.currentUserId", $A.get("$SObjectType.CurrentUser.Id"));
        helper.loadLogEntries(component);
    },

    postChanged: function (component, evt, helper) {
        helper.loadLogEntries(component);
    },

    refreshData: function (component, evt, helper){
         helper.loadLogEntries(component);
    }
})