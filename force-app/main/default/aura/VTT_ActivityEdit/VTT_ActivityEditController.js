/**
 * Created by MarcelBrimus on 30/05/2019.
 */
({
    handleSuccess : function(component, event, helper) {

        var contactRec = event.getParams().response;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
           "recordId": component.get('v.recordId'),
           "slideDevName": "related"
        });

       navEvt.fire();
    }
})