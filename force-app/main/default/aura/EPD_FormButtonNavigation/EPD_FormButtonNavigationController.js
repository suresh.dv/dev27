/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.initButtonMaps(component);
    },

    goPrev : function(component, event, helper) {
        helper.goPrev(component);
    },

    goNext : function(component, event, helper) {
        helper.goNext(component);
    },

    handleOuterChange : function(component, event, helper) {
        helper.handleOuterChange(component);
    },

});