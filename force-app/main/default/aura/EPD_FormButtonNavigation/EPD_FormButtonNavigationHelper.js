/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Initialize button mappings. To handle changing button labels during navigation.
    */
    initButtonMaps : function(component) {
        var prevButtonMapping = new Map();
        var nextButtonMapping = new Map();

        //TODO instead of manually adding item into these maps, do forloop which will go thru v.navigationItems and build these maps based on that.
        nextButtonMapping.set('Main Information', 'Engine Information');
        nextButtonMapping.set('Engine Information', 'Cylinder Information');
        nextButtonMapping.set('Cylinder Information', 'Parts Inspection');
        nextButtonMapping.set('Parts Inspection', 'Operating Conditions');
        nextButtonMapping.set('Operating Conditions', 'Pyrometers');
        nextButtonMapping.set('Pyrometers', 'Submit');
        nextButtonMapping.set('Submit', 'Submit');

        prevButtonMapping.set('Main Information', 'Main Information');
        prevButtonMapping.set('Engine Information', 'Main Information');
        prevButtonMapping.set('Cylinder Information', 'Engine Information');
        prevButtonMapping.set('Parts Inspection', 'Cylinder Information');
        prevButtonMapping.set('Operating Conditions', 'Parts Inspection');
        prevButtonMapping.set('Pyrometers', 'Operating Conditions');
        prevButtonMapping.set('Submit', 'Pyrometers');

        component.set("v.prevButtonMapping", prevButtonMapping);
        component.set("v.nextButtonMapping", nextButtonMapping);
    },

    /**
    Handle when navigation happens outside of this component.
    */
    handleOuterChange : function(component) {
        var helper = this;
        helper.setPrevButton(component);
        helper.setNextButton(component);
    },

    /**
    Handle go previous button action.
    Sets new selected tab and sets new button labels.
    */
    goPrev : function(component) {
        var helper = this;
        var prevButtonMapping = component.get("v.prevButtonMapping");

        helper.setSelectedTab(component, prevButtonMapping);

        helper.setPrevButton(component);
        helper.setNextButton(component);
    },

    /**
    Handle go next button action.
    Sets new selected tab and sets new button labels.
    */
    goNext : function(component) {
        var helper = this;
        var prevButtonMapping = component.get("v.nextButtonMapping");

        helper.setSelectedTab(component, prevButtonMapping);

        helper.setPrevButton(component);
        helper.setNextButton(component);
    },

    /**
    Sets new selected tab based on provided button mapping.
    If pyrometers tab is not required, this item is skipped.
    */
    setSelectedTab : function(component, buttonMapping) {
        var pyrometersRequired = component.get("v.pyrometersRequired");
        var selectedTab = component.get("v.selectedTab");

        selectedTab = buttonMapping.get(selectedTab);

        if(selectedTab === "Pyrometers" && !pyrometersRequired) {
            selectedTab = buttonMapping.get(selectedTab);
        }

        component.set("v.selectedTab", selectedTab);
    },

    /**
    set new label for Previous button.
    */
    setPrevButton : function(component) {
        var helper = this;
        var buttonMapping = component.get("v.prevButtonMapping");
        helper.setButton(component, "v.prevButtonLabel", buttonMapping, "v.prevButtonDisabled", "Main Information");
    },

    /**
    set new label for Next button.
    */
    setNextButton : function(component) {
        var helper = this;
        var buttonMapping = component.get("v.nextButtonMapping");
        helper.setButton(component, "v.nextButtonLabel", buttonMapping, "v.nextButtonDisabled", "Submit");
    },

    /**
    Sets button based on all params provided.
    Disables button if edge tab is selected.
    */
    setButton : function(component, buttonName, buttonMapping, buttonDisabledFlagName, edgeTabName) {
        var pyrometersRequired = component.get("v.pyrometersRequired");
        var selectedTab = component.get("v.selectedTab");

        var buttonLabel = buttonMapping.get(selectedTab);

        if(buttonLabel === "Pyrometers" && !pyrometersRequired) {
            buttonLabel = buttonMapping.get(buttonLabel);
        }

        component.set(buttonName, buttonLabel);
        component.set(buttonDisabledFlagName, selectedTab === edgeTabName);
    },

});