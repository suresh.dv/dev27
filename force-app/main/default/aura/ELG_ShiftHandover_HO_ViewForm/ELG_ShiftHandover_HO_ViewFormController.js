/**
 * Created by MarosGrajcar on 7/29/2019.
 */

({
    doInit : function(component, event, helper) {
        helper.loadHandover(component);
    },

    reloadHandover : function(component, event, helper) {
        helper.loadHandover(component);
    },
});