/**
 * Created by MarosGrajcar on 7/29/2019.
 */

({
    /*Load Section*/
    loadHandover : function(component) {
        var helper = this;
        component.set("v.toggleSpinner", true);
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper, "c.loadHandoverFromDatabase",
            { handoverForm : component.get("v.recordId") },
            this.loadSuccessOnShift,
            this.errorCallbackOnShift);
    },

    /*Load Success on Shift Assignement Component*/
    loadSuccessOnShift : function(component, requestResult, helper) {
        component.set("v.handoverForm", requestResult.resultObjects[0][0]);
        component.set("v.sectionEntries", requestResult.resultObjects[1]);

        helper.prepareShiftAssignemnetCmpState(component);
        component.set("v.toggleSpinner", false);
    },

    /*Prepare component state onShift Assignement Component*/
    prepareShiftAssignemnetCmpState : function (component) {
        if(component.get('v.sectionEntries').length === 0) {
            component.set('v.isSectionEmpty', true);
        } else {
            component.set('v.isSectionEmpty', false);
        }

        var getShiftEngineer = component.get('v.handoverForm').Shift_Assignement__r.Post__r.Shift_Engineer_Review_Required__c;

        component.set('v.isShiftEngineerRequired', getShiftEngineer);
        var status = component.get('v.handoverForm').Shift_Status__c;
        if(status == 'Handover in progress') {
            component.set('v.isInProgress', true);
        } else {
            component.set('v.isInProgress', false);
        }
    },

    /*Error on Load on Shift Assignement Component*/
    errorCallbackOnShift : function(component, requestResult, helper) {
        console.log('Error on Handover load on Shift Handover tab');
        component.set("v.toggleSpinner", false);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();
    }
});