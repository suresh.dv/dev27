/**
 * Created by MarcelBrimus on 30/07/2019.
 */

({
    init : function (component, evt, helper) {
        if(component.get('v.log').Correction_Entries__r && component.get('v.log').Correction_Entries__r.length > 0){
            var log = component.get('v.log');
            log.class = 'strike';
            component.set('v.log', log);
        }
    },

    onLogCorrection : function (component, evt, helper) {
        var p = component.get("v.parentComponent");
        p.refreshData();
    },

    // Show log entry correction
    showLogCorrection : function(component, evt, helper) {
       var modalBody;
       $A.createComponent("c:ELG_ShiftHandover_LogItemCorrection", {
           parentLogEntry:component.get('v.log'),
           onLogCorrected:component.getReference('c.onLogCorrection')
       },
          function(content, status) {
              if (status === "SUCCESS") {
                  modalBody = content;
                  component.find('overlayLib').showCustomModal({
                      header: "Correct Log Entry",
                      body: modalBody,
                      showCloseButton: true,
                      closeCallback: function() {}
                  })
              }
          });
    },
});