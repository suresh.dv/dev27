/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-01-28 - Created.
*************************************************************************************************/
({

    doRedirectIfNoError : function(component) {
        var showError = component.get("v.showError");
        var errorMsg = component.get("v.errorMessage");
        if(!showError &&
                (errorMsg === ""
                || errorMsg == undefined
                || errorMsg == null)) {
            var targetUrl = component.get("v.targetUrl");
            this.doRedirect(targetUrl);
        }
    },

    doRedirect : function(targetUrl) {
        console.log("Redirect component -> redirecting to: " + targetUrl);
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url" : targetUrl
        });
        urlEvent.fire();
    },

})