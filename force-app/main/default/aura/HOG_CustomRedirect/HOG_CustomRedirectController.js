/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-01-28 - Created.
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.doRedirectIfNoError(component);
    },

})