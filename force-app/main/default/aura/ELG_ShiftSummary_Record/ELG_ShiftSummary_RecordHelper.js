/**
 * Created by MarosGrajcar on 3/9/2020.
 */

({
     /*Load Section*/
    loadSummary : function(component) {
        var helper = this;
        component.set('v.loading', true);
        /*var user = $A.get("$SObjectType.CurrentUser.Id");
        component.set('v.loading', true);*/
        if (component.get('v.recordId') != null) {
            component.find("apexService").callApexWithErrorCallbackWithHelper(component,
                helper,
                "c.loadSummaryBasedOnRecordId",
                { recordId : component.get("v.recordId")},
                this.loadSuccess,
                this.errorCallback);
        } else {
            component.find("apexService").callApexWithErrorCallbackWithHelper(component,
                helper,
                "c.loadSummaryBasedOnShift",
                { shiftId : component.get("v.shiftId")},
                this.loadSuccess,
                this.errorCallback);
        }

    },

    /*Load on HandoverForm Component*/
    loadSuccess : function(component, requestResult, helper) {
        console.log('SUCCESS SHIFT SUMMARY');
        component.set("v.shiftSummary", requestResult.resultObjects[0]);
        component.set("v.handoverForm", requestResult.resultObjects[1]);
        component.set('v.shiftId', component.get('v.shiftSummary').Shift_Assignement__c);

        console.log('JSON SHIFT SUMMARY: '+JSON.stringify(component.get('v.shiftSummary')));
        component.set('v.loading', false);
        var user = $A.get("$SObjectType.CurrentUser.Id");
        console.log('Priamry: '+component.get('v.shiftSummary').Shift_Assignement__r.Primary__c + ' user: ' +user);
        if (component.get('v.shiftSummary').Shift_Assignement__r.Primary__c == user) {
            component.set('v.isPrimary', true);
        } else {
            component.set('v.isPrimary', false);
        }
        helper.prepareComponentState(component, event, helper);
    },

    errorCallback : function(component, requestResult, helper) {
        console.log('Error on SHIFT SUMMARY');
        component.set('v.loading', false);
    },

    /*Save Section*/
    saveShiftSummary : function(component, event, helper) {
        component.set('v.buttonLabel', 'Processing');
        component.set('v.disableButton', true);
        component.set('v.loading', true);
        //var operator = component.find("incomingOperatorId").get('v.incomingOperator');
        //component.set('v.handoverForm.Incoming_Operator__c', operator);
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.signOffFromSummaryAndUpdateHandover",
            { "shiftId" : component.get("v.shiftId")},
            this.saveSuccessful,
            this.errorOnSave);

    },

    takeOverShift : function(component, event, helper) {
        component.set('v.buttonLabel', 'Processing');
        component.set('v.disableButton', true);
        component.set('v.loading', true);
        //var operator = component.find("incomingOperatorId").get('v.incomingOperator');
        //component.set('v.handoverForm.Incoming_Operator__c', operator);
        component.find("apexService").callApexWithErrorCallbackWithHelper(component,
            helper,
            "c.takeNewShiftAndUpdateSummary",
            { "shiftId" : component.get("v.shiftId")},
            this.saveSuccessful,
            this.errorOnSave);

    },

    /*On successful save refresh page and close modal*/
    saveSuccessful : function(component, requestResult, helper) {
        component.set('v.disableButton', false);
        component.set('v.loading', false);
        //helper.navigateToShift(component, event, helper);
        if (component.get('v.doRedirect')) {
            var evt = $A.get("e.force:navigateToComponent");
            evt.setParams({
                componentDef : "c:ELG_ShiftHandover",
                isredirect : "true",
                componentAttributes: {}
            });
            evt.fire();
        } else {
            component.getEvent('eventToRefreshWrapper').fire();
        }
    },

    errorOnSave : function(component, requestResult, helper) {
        console.log('Error on Shift Summary save.' +requestResult.errors[0]);
        component.set('v.disableButton', false);
        component.set('v.loading', false);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title: '',
            message: requestResult.errors[0],
            type: 'error'
        });
        toastEvent.fire();
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:ELG_ShiftHandover",
            isredirect : "true",
            componentAttributes: {}
        });
        evt.fire();
        $A.get('e.force:refreshView').fire();
    },

    prepareComponentState : function(component) {
        var shiftSummaryStatus = component.get('v.shiftSummary').Status__c;

        console.log('Status: ' +shiftSummaryStatus);

        //Setting default values due to cache
        component.set('v.inProgress', false);
        component.set('v.awaitingAccept', false);

        if (shiftSummaryStatus == 'In Progress') {
            component.set('v.inProgress', true);
            component.set('v.buttonLabel', 'Sign Off');
        } else  if (shiftSummaryStatus == 'Awaiting Accept') {
           component.set('v.awaitingAccept', true);
           component.set('v.buttonLabel', 'Take Shift');
        }
        console.log('HM?: ' +component.get('v.isPrimary') + ' : ' +component.get('v.inProgress'));
    },

    navigateToShift : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
        componentDef : "c:ELG_ShiftHandover",
        isredirect : "true",
        componentAttributes: {}
        });
        evt.fire();
    },
});