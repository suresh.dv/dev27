/**
 * Created by MarosGrajcar on 3/9/2020.
 */

({
    doInit : function(component, event, helper) {
        console.log('initializing summary')
        helper.loadSummary(component);
    },

    signOffFromShift : function(component, event, helper) {
        helper.saveShiftSummary(component);
    },

    takeShift : function(component, event, helper) {
      helper.takeOverShift(component);
    },

    componentReload : function(component, event, helper) {
        component.getEvent('eventToRefreshWrapper').fire();
    },
});