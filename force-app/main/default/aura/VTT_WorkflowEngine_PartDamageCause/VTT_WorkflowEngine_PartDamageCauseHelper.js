/**
 * Created by MarcelBrimus on 30/10/2019.
 */
({
    onPartsChange: function(component, event, helper){
        component.set('v.availableDamage', []);
        if(component.get('v.availableParts')){
            component.get('v.availableParts').forEach((item) => {
                if(item.Id === component.get('v.partItem')){
                   helper.getDamage(component, event, helper, item.Part_Code__c)
                }
           });
        }
    },

    getDamage : function(component, event, helper, value) {
        component.set("v.actionInProgress", true);
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            helper,
            "c.getDamage",
            { "partCode" : value},
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        if(response.resultObjects[0]){
            component.set('v.availableDamage',response.resultObjects[0]);
        }
    },

    handleError : function(component, response, helper) {
        component.set('v.actionInProgress', false);
        helper.showToast("Error!", "error", helper.getError(response));
    },

    validate : function(component, helper, event){
        var allValid = [].concat(component.find('toCheck')).reduce(function (validFields,
            inputCmp) {
            if(!inputCmp){
                return true;
            }
            inputCmp.showHelpMessageIfInvalid();
            return validFields && inputCmp.get('v.validity').valid;
        }, true);

        return allValid;
    },

    /**
    Show toast based on provided params
    */
    showToast : function(title, type, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "type": type,
            "message": message
        });
        toastEvent.fire();
    },
});