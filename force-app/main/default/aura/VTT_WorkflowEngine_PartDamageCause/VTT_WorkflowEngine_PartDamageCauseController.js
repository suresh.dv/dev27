/**
 * Created by MarcelBrimus on 30/10/2019.
 */

({
    onPartsChange: function(component, event, helper){
        helper.onPartsChange(component, event, helper);
    },

    validatePDC: function(component, event, helper){
       return helper.validate(component, event, helper);
    }
});