/**
 * Created by MarosGrajcar on 6/11/2019.
 */

({
    validationOfFields : function(component, event, helper) {
        var isFieldDisabled = component.get('v.isDisabled');
        var isFieldFilled = component.get('v.fieldValue');
        var inputCmp = component.find("userInput");
        var isInputValid = true;
        var logEntryCharacterLimit = 5000;

        if(!isFieldDisabled) {
            if(!isFieldFilled) {
                inputCmp.setCustomValidity("Please answer or uncheck the question");
                inputCmp.reportValidity();
                isInputValid = false;
            } else if(isFieldFilled.length > logEntryCharacterLimit) {
                logEntryCharacterLimit = isFieldFilled.length - logEntryCharacterLimit;
                inputCmp.setCustomValidity("Log Entry cannot exceed limit more than 5000 characters. You are above " +logEntryCharacterLimit+ " characters limit to proceed.");
                inputCmp.reportValidity();
                isInputValid = false;
            }
        }

        return isInputValid;
    },

    errorReset : function(component) {
        var inputCmp = component.find("userInput");
        inputCmp.setCustomValidity("");
        inputCmp.reportValidity();
    },
});