/**
 * Created by MarosGrajcar on 6/3/2019.
 */

({
    changeState : function(component, event, helper){
        var stateOfSaveButton = component.getEvent("checkboxOnChange");
        if(component.get('v.isDisabled')) {
            stateOfSaveButton.setParams({
                        setButtonState : false
                    });
            stateOfSaveButton.fire();
            helper.errorReset(component);
            component.set('v.isDisabled', false);
        } else {
            helper.errorReset(component);
            component.set('v.isDisabled', true);
            component.set("v.fieldValue", null);
        }
    },

    validateField : function(component, event, helper) {
        return helper.validationOfFields(component, event, helper);
    },
});