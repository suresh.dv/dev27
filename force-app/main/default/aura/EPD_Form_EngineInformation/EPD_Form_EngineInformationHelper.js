/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-10 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Checks value of input and validate it against thresholds.
    */
    checkRPMThresholds : function(component) {
        var helper = this;
        var rpmInput = component.find('rpm');
        var formStructure = component.get("v.formStructure");

        var message = "";

        if(formStructure.epd.EIRPM != ""
                && (formStructure.epd.EIRPM < formStructure.engineThresholds.minRPM
                || formStructure.epd.EIRPM > formStructure.engineThresholds.maxRPM)) {

            message = "Warning: Value is outside of thresholds: Min RPM: "
                    + formStructure.engineThresholds.minRPM
                    + ", Max RPM: "
                    + formStructure.engineThresholds.maxRPM
                    + ".";

        }
        helper.inputDisplayErrorIfInvalid(rpmInput, message);
    },

    /**
    Checks value of input and validate it against thresholds.
    */
    checkManifoldPressure : function(component, event) {
        var helper = this;
        var manifoldInput = event.getSource();
        var newValue = manifoldInput.get("v.value");
        var formStructure = component.get("v.formStructure");

        var message = "";

        if(newValue != ""
                && (newValue < formStructure.engineThresholds.minIntakeManifoldPressure
                || newValue > formStructure.engineThresholds.maxIntakeManifoldPressure)) {

           message = "Warning: Value is outside of thresholds: Min Intake Manifold Pressure: "
                               + formStructure.engineThresholds.minIntakeManifoldPressure
                               + ", Max Intake Manifold Pressure: "
                               + formStructure.engineThresholds.maxIntakeManifoldPressure
                               + ".";
        }

        helper.inputDisplayErrorIfInvalid(manifoldInput, message);
    },

    /**
    put validation error message on field. (Put blank message if error is not required)
    */
    inputDisplayErrorIfInvalid : function(inputField, message) {
        inputField.setCustomValidity(message);
        inputField.reportValidity();
    },

});