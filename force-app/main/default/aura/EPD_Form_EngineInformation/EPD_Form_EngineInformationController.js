/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-10 - Created. - EPD R1
*************************************************************************************************/
({

    checkRPMThresholds : function(component, event, helper) {
        helper.checkRPMThresholds(component);
    },

    checkManifoldPressure : function(component, event, helper) {
        helper.checkManifoldPressure(component, event);
    },

});