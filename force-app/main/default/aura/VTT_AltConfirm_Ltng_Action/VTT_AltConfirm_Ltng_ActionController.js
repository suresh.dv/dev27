/**
 * Created by MarcelBrimus on 08/01/2019.
 */
({
    onWorkOrderInit : function(component, event, helper) {
        if(component.get('v.workOrder.VTT_Activities_Count__c') > component.get('v.workOrder.VTT_Activities_New_Count__c')){
            component.set('v.needVerification', true);
            helper.hideSpinner(component);
        } else {
            helper.doAltConfirm(component);
        }
    },

    onOK : function(){
        $A.get("e.force:closeQuickAction").fire();
    },

    onYes : function(component, event, helper){
        helper.doAltConfirm(component);
        component.set('v.needVerification', false);
    },

    onNo : function(){
        $A.get("e.force:closeQuickAction").fire();
    }
})