/**
 * Created by MarcelBrimus on 08/01/2019.
 */
({
    doAltConfirm: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        component.set('v.needVerification', false);

        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.updateALTConfirmed_ltng",
            { "maintenanceServicingFormId" : component.get("v.recordId") },
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess: function(component, response, helper){
        var responseString = response.resultObjects[0];
        component.set('v.messageFromWebservice', responseString);
        helper.hideSpinner(component);
        // Close modal if alt confirm was successful..
        if(responseString.indexOf('successfully')>=0){
            helper.displayToast(component, 'success', responseString);
            $A.get("e.force:refreshView").fire();
            $A.get("e.force:closeQuickAction").fire();
        }
    },

    handleError: function(component, response, helper){
        var responseString = response.resultObjects[0];
        component.set('v.messageFromWebservice', responseString);
        helper.hideSpinner(component);
    },

    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          mode: 'dismissible',
          type: type,
          message: message,
          duration: 5000
        });
        toastEvent.fire();
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },
})