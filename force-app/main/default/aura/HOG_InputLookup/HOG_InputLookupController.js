({

    /**
     * Key press handler for handling key presses from lookup input
     */
	keyPressController : function(component, event, helper) {
		helper.handleKeyPress(component, event);
	},
  

    /**
     * Clear selection and all neccessary attributes
     */
    clear : function(component, event, helper){
		helper.clearLookup(component);
    },
      
    /**
     * Handle record selection from child component which displays list of options
     */
    handleRecordSelection : function(component, event, helper) {
        helper.selectRecord(component, event);     
	},

    /**
     * Initialize lookup with record passed as attribute
     */
	handleInitialization : function(component, event, helper) {
        helper.initLookup(component, event);
	},

    /**
     * Run validation on component to show styling error if there is error on it.
     */
    validate : function(component, event, helper) {
        helper.validateInputField(component, event);
    },

    /**
     * Run validatoin on compomnent to show styling error if there is error on it 
     * and return result to callback attribute to be passed to parent.
     */
    isValid : function(component, event, helper) {
        helper.isValid(component, event, true);
    },

    /**
     * Display error styling on component
     */
    showError : function(component, event, helper) {
        helper.showError(component);
    },

    /**
     * Hide error styling on component
     */
    clearError : function(component, event, helper) {
        helper.clearError(component);
    },

    /**
     * Hide loading spinner
     */
    hideSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.addClass(spinner, "slds-hide");
    },

    /**
     * Show loading spinner 
     */
    showSpinner : function (component, event, helper) {
        var spinner = component.find('spinner');
        $A.util.removeClass(spinner, "slds-hide");
    },
    
})