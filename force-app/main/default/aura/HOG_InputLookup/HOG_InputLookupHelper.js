({

    /**
     * [searchHelper description]
     * @param  {[String]} getInputkeyWord [Key word used for record search]
     * @param  {[String]} sObjectName     [API Name of SObject for which we are searching records]
     */
	searchHelper : function(component, event, getInputkeyWord, sObjectName) {
     	var action = component.get("c.prefetchLookupData");
        action.setParams({
            "withSharing": component.get('v.withSharing'),
            "objectName" : sObjectName,
			"stringToSearch": getInputkeyWord
		});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                if(storeResponse && storeResponse != 'undefined' && storeResponse.length == 0) {
                    component.set("v.message", 'No Result Found...');
                } else {
                    component.set("v.message", 'Search Result...');
                }
                component.set("v.listOfSearchRecords", storeResponse);
            } else {
            	component.set("v.listOfSearchRecords", []);
            }
        }); 
        $A.enqueueAction(action);
	},
    
    /**
     * Handler for key press on input field.
     * When fired it will look for record and dispay list of options (if inputkeyword is bigger then 1)
     * or it will hide list of options.
     */
    handleKeyPress : function(component, event) {
        var getInputkeyWord = component.get("v.searchKeyWord");
		var sObjectName = component.get("v.sObjectName");
        if( getInputkeyWord.length > 1 ) {
			var forOpen = component.find("searchRes");
			$A.util.addClass(forOpen, 'slds-is-open');
			$A.util.removeClass(forOpen, 'slds-is-close');
			this.searchHelper(component, event, getInputkeyWord, sObjectName);
        } else {  
			component.set("v.listOfSearchRecords", null); 
			var forclose = component.find("searchRes");
			$A.util.addClass(forclose, 'slds-is-close');
			$A.util.removeClass(forclose, 'slds-is-open');
		}
    },
    
    /**
     * Clear selection and all attributes
     */
    clearLookup : function(component) {
        var pillTarget = component.find("lookup-pill");
		var lookUpTarget = component.find("lookupField"); 

		$A.util.addClass(pillTarget, 'slds-hide');
		$A.util.removeClass(pillTarget, 'slds-show');
        
		$A.util.addClass(lookUpTarget, 'slds-show');
		$A.util.removeClass(lookUpTarget, 'slds-hide');

		component.set("v.searchKeyWord", null);
		component.set("v.listOfSearchRecords", null);
        component.set("v.selectedId", null);
        component.set("v.selectedRecord", null);
    },
    
    /**
     * Select record. 
     * Set's attributes and displays pill with selected record.
     */
    selectRecord : function(component, event) {
        var selectedAccountGetFromEvent = event.getParam("recordByEvent");
        
        component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
        component.set("v.selectedId", selectedAccountGetFromEvent.Id);

        this.displayRecordPill(component);
    },
    
    /**
     * Preselect lookup with record.
     */
    initLookup : function(component, event) {
        var selectedAccountGetFromEvent = event.getParam('arguments');

        component.set("v.selectedRecord" , selectedAccountGetFromEvent.recordByEvent); 
        component.set("v.selectedId", selectedAccountGetFromEvent.recordByEvent.Id);

        this.displayRecordPill(component);  
    },

    /**
     * Display pill
     */
    displayRecordPill : function(component) {
        var forclose = component.find("lookup-pill");
        $A.util.addClass(forclose, 'slds-show');
        $A.util.removeClass(forclose, 'slds-hide');

        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');

        var lookUpTarget = component.find("lookupField");
        $A.util.addClass(lookUpTarget, 'slds-hide');
        $A.util.removeClass(lookUpTarget, 'slds-show');
    },

    /**
     * Validate input field based on attributes and display error styling
     * with message if there is missing value.
     */
    validateInputField : function(component, event) {
        this.isValid(component, event, false);
    },

    /**
     * Validate input field based on attributes and display error styling
     * with message if there is missing value.
     * If required (returnCallback is set to true) return result of validity
     */
    isValid : function(component, event, returnCallback) {
        var inputField = component.find("lookupInput");
        $A.util.removeClass(inputField, "has-error");
        if(component.get("v.required")
        && !component.get("v.selectedId")) {
            this.showError(component);
        }
        if(returnCallback) {
            var params = event.getParam("arguments");
            params.callback(null, !component.get("v.required") || component.get("v.selectedId"));
        }
    },

    /**
     * Display error styling with message.
     */
    showError : function(component) {
        var inputField = component.find("lookupInput");
        inputField.set("v.errors", [{ message : component.get("v.errorMsg") }]);
        $A.util.addClass(inputField, "has-error");
    },

    /**
     * Hide error styling.
     */
    clearError : function(component) {
        $A.util.removeClass(inputField, "has-error");
        var inputField = component.find("lookupInput");
        inputField.set("v.errors", null );
    },
    
})