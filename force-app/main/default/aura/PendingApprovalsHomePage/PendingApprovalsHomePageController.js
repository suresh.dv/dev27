({
	doInit : function(component, event, helper) {
		var action = component.get("c.getUserPendingProcess");
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state === "SUCCESS")
            {
                if(response.getReturnValue() != null)
                {
                    var PendingProcessList = [];
                    for(var i in response.getReturnValue())
                    {
                        PendingProcessList.push({'RelatedTo' : response.getReturnValue()[i].RelatedTo,'Name' : response.getReturnValue()[i].Name,
                                                'Type' : response.getReturnValue()[i].Type,'AssignedTo' : response.getReturnValue()[i].AssignedTo,
                                                'ActualApprover' : response.getReturnValue()[i].ActualApprover,'DateSubmitted' : response.getReturnValue()[i].DateSubmitted,
                                                'RelatedToId' : response.getReturnValue()[i].RelatedToId,'NameId' : response.getReturnValue()[i].NameId,
                                                'AssignedToId' : response.getReturnValue()[i].AssignedToId,'ActualApproverId' : response.getReturnValue()[i].ActualApproverId});
                    }
                    component.set("v.PendingProcessList",PendingProcessList);
                    component.set("v.OriginalPendingProcessList",PendingProcessList);
                    
                    console.log('response.getReturnvalue() - ' + JSON.stringify(PendingProcessList));
                }
            }
        });
        $A.enqueueAction(action);
	},
    
    sortRelatedTo : function(component, event, helper) {
        
        //component.set('v.selectedTabsoft',event.currentTarget.name);
        var currentDir = component.get("v.arrowDirection");
        var sortList = component.get("v.PendingProcessList");
        var newSortList = [];
        var sortBy = event.currentTarget.name;
        component.set('v.selectedTabsoft',sortBy);
         sortList.sort(function(a, b) {
                
                if(sortBy != 'DateSubmitted'){
                    
                    var textA=a[sortBy];
                    textA=textA.replace(/\s/g,'').toLowerCase();
                    var textB=b[sortBy];
                        textB=textB.replace(/\s/g,'').toLowerCase();
                     return (textA <textB) ? -1 : (textA > textB) ? 1 : 0;
                }
            		
                else
                    return Date.parse(a[sortBy]) - Date.parse(b[sortBy]);
			});
        if(currentDir == 'arrowdown')
        {
            component.set("v.arrowDirection",'arrowup');          
        }
        else
        {
            component.set("v.arrowDirection",'arrowdown');
            sortList.reverse();
        }
        component.set("v.PendingProcessList",sortList);
    },
})