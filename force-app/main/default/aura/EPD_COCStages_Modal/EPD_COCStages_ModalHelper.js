/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-06-03 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Fire event for parent to close COC Stages modal window.
    */
    closeModal : function(component) {
        var closeModalEvent = component.getEvent("closeCOCStages");
        closeModalEvent.fire();
    }

});