/**
 * Created by MarcelBrimus on 01/10/2019.
 */

({
    onWorkflowAction: function(component, event, helper){
        component.set('v.actionMode', event.getParam("actionName"));
        component.set('v.tradesman', event.getParam("tradesman"));
        component.set('v.latitude', event.getParam("latitude"));
        component.set('v.longitude', event.getParam("longitude"));
        component.set('v.workOrderActivity', event.getParam("workOrderActivity"));
    },

    refreshEngineAndSetView: function(component, event, helper){
        //var workflowEngineComponent = component.find("workflowEngineComponent");
        //workflowEngineComponent.refreshWorkflow();
        helper.nullifyData(component);
        //component.set('v.actionMode', 'VIEW');
        $A.get("e.force:refreshView").fire();
    },

    setWorkOrderId: function(component, event, helper){
        component.set('v.workOrderId', event.getParam("workOrderId"));
    }
});