<!--
 - Created by MarcelBrimus on 30/09/2019.
 -->

<aura:component description="VTT_ActivityDetail"
                implements="flexipage:availableForAllPageTypes,force:hasRecordId,force:appHostable,forceCommunity:availableForAllPageTypes">
    <!--ATTRIBUTES-->
    <aura:attribute name="recordId" type="String" description="Work Order Activity Record Id"/>
    <aura:attribute name="workOrderId" type="String" description="Work Order"/>
    <aura:attribute name="workOrderActivity" type="Work_Order_Activity__c"
                    description="WOA, value flows from VTT_LTNG_WorkflowActionEvent"/>
    <aura:attribute name="tradesman" type="Contact"
                    description="Tradesman, value flows from VTT_LTNG_WorkflowActionEvent"/>
    <aura:attribute name="actionMode"
                    type="String"
                    description="Defines which component to render, value flows from VTT_LTNG_WorkflowActionEvent"
                    default="VIEW"/>
    <aura:attribute name="workOrderDetailFields" type="String[]"
                    default="[
                    'Service_Status__c', 'Operator_Route__c',
                    'Location__c','Operating_Field_AMU_Lookup__c',
                    'Facility__c','Requested_Stop_Time__c',
                    'Equipment__c','Requested_Start_Time__c',
                    'Order_Description__c', 'Required_End_Date__c']"/>

    <aura:attribute name="latitude" type="Decimal"/>
    <aura:attribute name="longitude" type="Decimal"/>

    <!--HANDLERS-->
    <aura:handler event="c:VTT_LTNG_WorkflowActionEvent" name="workflowEventAction"
                  action="{!c.onWorkflowAction}"/>
    <aura:handler event="c:VTT_LTNG_WorkflowActionEvent" name="workOrderDataAction"
                  action="{!c.setWorkOrderId}"/>
    <aura:handler event="c:VTT_LTNG_WorkflowRefreshEvent" name="workflowRefreshEvent"
                  action="{!c.refreshEngineAndSetView}"/>

    <div class="slds-grid slds-wrap">
        <div class="slds-col slds-small-size--1-of-1 slds-medium-size--4-of-12 slds-large-size--3-of-12 slds-p-right--small slds-p-bottom--small">
            <c:VTT_WorkflowEngine aura:id="workflowEngineComponent" recordId="{!v.recordId}"/>
        </div>
        <div class="slds-col slds-small-size--1-of-1 slds-medium-size--8-of-12 slds-large-size--9-of-12">
            <aura:if isTrue="{!v.actionMode == 'VIEW'}">
                <article class="slds-card">
                    {!v.workOrderActivity.Maintenance_Work_Order__c}
                    <div class="slds-card__header">
                        <header class="slds-media slds-media_center slds-has-flexi-truncate">
                            <h2 class="slds-card__header-title">
                                Work Order Header Information
                            </h2>
                        </header>
                    </div>
                    <div class="slds-card__body slds-card__body_inner">
                        <aura:if isTrue="{!v.workOrderId}">
                            <lightning:recordForm
                                recordId="{!v.workOrderId}"
                                objectApiName="HOG_Maintenance_Servicing_Form__c"
                                mode="readonly"
                                columns="2"
                                fields="{!v.workOrderDetailFields}"/>
                        </aura:if>
                    </div>
                </article>
                <article class="slds-card">
                    <div class="slds-card__header">
                        <header class="slds-media slds-media_center slds-has-flexi-truncate">
                            <h2 class="slds-card__header-title">
                                Work Order Activity Detail
                            </h2>
                        </header>
                    </div>
                    <div class="slds-card__body slds-card__body_inner">
                        <lightning:recordForm
                            recordId="{!v.recordId}"
                            objectApiName="Work_Order_Activity__c"
                            layoutType="Full"
                            columns="2"
                            mode="readonly"/>
                    </div>
                </article>
            </aura:if>
            <aura:if isTrue="{!v.actionMode == 'Need Refresh'}">
                <article class="slds-card">
                    <div class="slds-card__body slds-card__body_inner">
                        <button class="slds-button slds-button_destructive slds-button_stretch"
                                onclick="{!c.refreshEngineAndSetView}"> Activity Record has changed - Click Here for
                            refresh
                        </button>
                    </div>
                </article>
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_Start_Job_Action, v.workOrderActivity)}">
                <c:VTT_WorkflowEngine_Start_Job
                        workOrderActivity="{!v.workOrderActivity}"
                        tradesman="{!v.tradesman}"
                        latitude="{!v.latitude}"
                        longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_Start_Job_At_Equipment_Action, v.workOrderActivity)}">
                <c:VTT_WorkflowEngine_Start_Job_At_Eq
                        workOrderActivity="{!v.workOrderActivity}"
                        tradesman="{!v.tradesman}"
                        latitude="{!v.latitude}"
                        longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_Start_At_Equipment_Action, v.workOrderActivity)}">
                <c:VTT_WorkflowEngine_Start_At_Equipment
                        workOrderActivity="{!v.workOrderActivity}"
                        tradesman="{!v.tradesman}"
                        latitude="{!v.latitude}"
                        longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_Finished_Job_At_Equipment_Action, v.workOrderActivity)}">
                <c:VTT_WorkflowEngine_Finished_At_Equipment
                        workOrderActivity="{!v.workOrderActivity}"
                        tradesman="{!v.tradesman}"
                        latitude="{!v.latitude}"
                        longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!v.actionMode == $Label.c.VTT_LTNG_Finished_for_the_Day_Action}">
                <c:VTT_WorkflowEngine_Finished_For_The_Day workOrderActivity="{!v.workOrderActivity}"
                                                           tradesman="{!v.tradesman}"
                                                           latitude="{!v.latitude}"
                                                           longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_Job_On_Hold_Action, v.workOrderActivity)}">
                <c:VTT_WorkflowEngine_Job_On_Hold workOrderActivity="{!v.workOrderActivity}"
                                                  tradesman="{!v.tradesman}"
                                                  latitude="{!v.latitude}"
                                                  longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_Job_Complete_Action,v.workOrderActivity)}">
                <c:VTT_WorkflowEngine_Job_Complete workOrderActivity="{!v.workOrderActivity}"
                                                   tradesman="{!v.tradesman}"
                                                   latitude="{!v.latitude}"
                                                   longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_Reject_Activity_Action,v.workOrderActivity)}">
                <c:VTT_WorkflowEngine_Reject_Activity workOrderActivity="{!v.workOrderActivity}"
                                                      tradesman="{!v.tradesman}"
                                                      latitude="{!v.latitude}"
                                                      longitude="{!v.longitude}"
                />
            </aura:if>
            <aura:if isTrue="{!and(v.actionMode == $Label.c.VTT_LTNG_EPD_Action,v.workOrderActivity)}">
                <c:EPD_Form recordId="{!v.workOrderActivity.Id}" displayMode="WOA"/>
            </aura:if>
        </div>
    </div>
</aura:component>