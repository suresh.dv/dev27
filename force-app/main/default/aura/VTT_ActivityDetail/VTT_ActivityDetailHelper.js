/**
 * Created by MarcelBrimus on 03/10/2019.
 */

({
    nullifyData : function(component) {
        component.set('v.workOrderActivity', null);
        component.set('v.tradesman', null);
        component.set('v.latitude', null);
        component.set('v.longitude', null);
    },
});