/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-02-25 - Created.
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.handleInit(component);
    },

    handleProceedButton : function(component, event, helper) {
        helper.createWOInSAP(component);
    },

})