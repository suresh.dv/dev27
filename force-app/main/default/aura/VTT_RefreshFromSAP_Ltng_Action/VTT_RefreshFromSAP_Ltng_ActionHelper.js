/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-02-25 - Created.
*************************************************************************************************/
({

    handleInit : function(component) {
        var helper = this;
        helper.showSpinner(component);
        helper.updateWOOrShowDialog(component);
    },

    showSpinner : function(component) {
        component.set("v.loading", true);
    },

    hideSpinner : function(component) {
        component.set("v.loading", false);
    },

    updateWOOrShowDialog : function(component) {
        var helper = this;
        var workOrder = component.get("v.workOrder");

        try{
            if(!workOrder.Work_Order_Number__c) {
                if(workOrder.SAP_Request_Returned_Critical_Error__c) {
                    helper.showError(component, "This work order cannot be refreshed from SAP.", helper);
                } else {
                    helper.showDialog(component);
                }
            } else {
                helper.updateWOFromSAP(component);
            }
        } catch (e) {
            component.set("v.errorMessage", 'An Error has Occurred. Error: ' + e);
            component.set("v.showError", true);
            helper.hideSpinner(component);
        }
    },

    showDialog : function(component) {
        var helper = this;
        component.set("v.showDialog", true);
        helper.hideSpinner(component);
    },

    updateWOFromSAP : function(component) {
        var helper = this;
        var workOrder = component.get("v.workOrder");
        var params = {
                wellServicingFormId: workOrder.Id,
                notificationServicingFormId: workOrder.HOG_Service_Request_Notification_Form__c,
                workOrderNumber: workOrder.Work_Order_Number__c
        };
        helper.callApex(
                component,
                helper,
                params,
                "c.updateServiceWorkOrderFromSAPLtng"
        );
    },

    createWOInSAP : function(component) {
        var helper = this;
        var workOrder = component.get("v.workOrder");
        var params = {
                wellServicingFormId: workOrder.Id,
                notificationServicingFormId: workOrder.HOG_Service_Request_Notification_Form__c
        };
        helper.showSpinner(component);
        helper.callApex(
                component,
                helper,
                params,
                "c.createServiceWorkOrderInSAPLtng"
        );
    },

    callApex : function(component, helper, params, method) {
        var apexService = component.find("apexService");
        apexService.callApexWithErrorCallbackWithHelper(
                component,
                helper,
                method,
                params,
                this.successCallback,
                this.errorCallback
        );
    },

    successCallback : function(component, response, helper) {
        var workOrderNumberFromServer = response.resultObjects[0];
        var workOrderNumberFromPage = component.get("v.workOrder").Work_Order_Number__c;

        if(workOrderNumberFromServer.toString().trim() == workOrderNumberFromPage) {
            helper.hideSpinner(component);
            $A.get("e.force:closeQuickAction").fire();
            $A.get("e.force:refreshView").fire();
        } else {
            helper.showError(component, response.resultObjects[0].toString(), helper);
        }
    },

    errorCallback : function(component, response, helper) {
        helper.showError(component, response.errors[0], helper);
    },

    showError : function(component, errorMessage, helper) {
        component.set("v.errorMessage", errorMessage);
        component.set("v.showError", true);
        helper.hideSpinner(component);
    },

})