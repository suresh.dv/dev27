/**
 * Created by MarosGrajcar on 1/13/2020.
 */

({
    closeModal : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        $A.get("e.force:refreshView").fire();
    },

    closeForm : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    },
});