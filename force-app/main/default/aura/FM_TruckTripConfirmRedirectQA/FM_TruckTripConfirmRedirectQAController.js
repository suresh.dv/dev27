/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-01-30 - Created.
*************************************************************************************************/
({

    doInit : function(component, event, helper) {
        helper.validateRedirect(component, helper);
    },

})