/**
 * Created by MarcelBrimus on 01/02/2019.
 */
({
    onRedirect : function(component, event, helper) {
       helper.doRedirect(component, event);
    }
})