/**
 * Created by MarcelBrimus on 01/02/2019.
 */
({
      doRedirect: function(component) {
            var redirectId = component.get("v.redirectId");
            var action = component.get("c.redirectToThisPage");
            action.setParams({ redirectId : redirectId });

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {

                } else if (state === "INCOMPLETE") {

                } else if (state === "ERROR") {

                }
            });
            $A.enqueueAction(action);
      }
})