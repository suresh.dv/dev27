/**
 * Created by MarcelBrimus on 18/02/2019.
 */
({
    doUpdateNotificationOrderFromSAP: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.updateNotificationOrderFromSAP_ltng",
            { "notificationServicingFormId" : component.get("v.recordId"),
              "notificationOrderNumber" : component.get("v.snr.Notification_Number__c")},
            this.handleSuccess,
            this.handleError
        );
    },

    doCreateNotificationOrderInSAP: function(component) {
        var apexService = component.find("apexService");
        this.showSpinner(component);
        apexService.callApexWithErrorCallbackWithHelper(
            component,
            this,
            "c.createNotificationOrderInSAP_ltng",
            { "notificationServicingFormId" : component.get("v.recordId")},
            this.handleSuccess,
            this.handleError
        );
    },

    handleSuccess: function(component, response, helper){
        var responseString = response.resultObjects[0];
        helper.hideSpinner(component);
        console.log('handleSuccess ' + responseString);
        $A.get("e.force:refreshView").fire();
        $A.get("e.force:closeQuickAction").fire();
        helper.displayToast(component, 'warning', responseString);
    },

    handleError: function(component, response, helper){
        var responseString = response.resultObjects[0];
        console.log('Error ' + responseString);
        component.set('v.messageFromWebservice', responseString);
        helper.hideSpinner(component);
    },

    displayToast: function (component, type, message) {
        const toastEvent = $A.get('e.force:showToast');
        toastEvent.setParams({
          type: type,
          message: message
        });
        toastEvent.fire();
    },

    showSpinner : function(component) {
        component.set("v.toggleSpinner", true);
    },

    hideSpinner : function(component) {
        component.set("v.toggleSpinner", false);
    },
})