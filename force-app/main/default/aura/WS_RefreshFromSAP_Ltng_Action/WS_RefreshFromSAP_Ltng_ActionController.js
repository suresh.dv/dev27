/**
 * Created by MarcelBrimus on 18/02/2019.
 */
({
    /*
    * Called when record data in component loads, then decides is refresh or create is needed
    */
    onRecordInit : function(component, event, helper) {
        var notificationNumber = component.get("v.snr.Notification_Number__c");
        if(notificationNumber){
            helper.doUpdateNotificationOrderFromSAP(component);
        }
    },

    onCreateNotificationOrderInSAP : function(component, event, helper) {
        helper.doCreateNotificationOrderInSAP(component);
    },

    onCancel : function(){
        $A.get("e.force:closeQuickAction").fire();
    }
})