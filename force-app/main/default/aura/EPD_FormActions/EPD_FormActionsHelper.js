/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    /**
    Fire component level event to initiate save action.
    */
    save : function (component, event) {
        var saveRecordEvt = component.getEvent("saveRecords");
        saveRecordEvt.setParams({
            submit : false
        });
        saveRecordEvt.fire();
    },

    /**
    Loads Engine Thresholds modal
    */
    displayThresholds : function(component) {
        component.set("v.showThresholds", true);
    },


    /**
    Hides Engine Thresholds modal
    */
    handleCloseModal : function(component) {
        component.set("v.showThresholds", false);
    },

    /**
    Switch display mode back to view
    */
    toggleEdit : function(component) {
        if(component.get("v.displayMode") === "EDIT") {
            component.set("v.displayMode", "VIEW");
            component.getEvent("cancelEdit").fire();
        } else {
            component.set("v.displayMode", "EDIT");
        }
    },

    /**
    Displays PDF for EPD form.
    */
    showPDF : function(component) {
        window.open('/apex/EPD_PDFPage?Id=' + component.get("v.recordId"), '_blank');
    },

});