/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
History:        jschn 2019-05-30 - Created. - EPD R1
*************************************************************************************************/
({

    save : function (component, event, helper) {
        helper.save(component, event);
    },

    displayThresholds : function(component, event, helper) {
        helper.displayThresholds(component);
    },

    handleCloseModal : function(component, event, helper) {
        helper.handleCloseModal(component);
    },

    toggleEdit : function(component, event, helper) {
        helper.toggleEdit(component);
    },

    showPDF : function(component, event, helper) {
        helper.showPDF(component);
    },

});