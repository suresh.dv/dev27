({
    doInit : function(component, event, helper) {
        console.log(event.getSource());
    	helper.loadAlert(component);
        helper.loadPicklistsOptions(component);
    },

    loadAlert : function(component, event, helper) {
    	helper.loadAlert(component);
    },

	saveEdit : function(component, event, helper) {
		helper.saveAlert(component, helper);
	},

	cancel : function(component, event, helper) {
		if(component.get("v.originalMode") === "edit") {
//		    window.history.back();
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    actionName: 'view',
                    recordId: component.get("v.alertId")
                }
            };
            navService.navigate(pageReference, true);
        } else {
            helper.toggleEditMode(component);
            helper.fireAlertChangeEvent(component);
            helper.loadAlert(component);

        }
	},

	edit : function(component, event, helper) {
		helper.toggleEditMode(component);
		helper.setEngineer(component);
	},
    
})