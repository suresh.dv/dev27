({
   	/*Data Saving*/
	saveAlert : function(component, helper) {
		var dataIsValid = this.dataIsValid(component);
		if(dataIsValid) {
			component.set("v.loading", true);
			component.find("apexService").callApexWithHelper(component, helper, "c.upsertAlert_v1", { "vgAlert" : JSON.stringify(component.get("v.alert")) }, this.saveAlertSuccess);
		} else {
			this.fireNotification("", "Missing data.", "Error");
			console.log("Data are invalid.");
		}
	},

	saveAlertSuccess : function(component, requestResult, helper) {
        if(component.get("v.originalMode") === "edit") {
            var navService = component.find("navService");
            var pageReference = {
                type: 'standard__recordPage',
                attributes: {
                    actionName: 'view',
                    recordId: component.get("v.alertId")
                }
            };
            navService.navigate(pageReference, true);
        } else {
            helper.fireNotification("", "Alert updated.", "Success");
            helper.fireSuccessEvent(component);
            helper.fireAlertChangeEvent(component);
            helper.loadAlert(component);
            helper.toggleEditMode(component);
            $A.get('e.force:refreshView').fire();
            component.set("v.loading", false);
        }
	},


	fireNotification : function(toastTitle, toastMessage, toastType) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
		    title: toastTitle,
		    message: toastMessage,
		    type: toastType
		});
		toastEvent.fire();
	},

	fireSuccessEvent : function(component) {
		 var event = $A.get("e.c:VG_AlertSaveSuccess"); 
		 event.fire();
	},

	fireAlertChangeEvent : function(component) {
		 var event = $A.get("e.c:VG_AlertChange"); 
		 event.fire();
	},

	/*Data load*/
	loadAlert : function(component) {
	    var helper = this;
        component.set("v.loading", true);
        component.find("apexService").callApexWithHelper(component, helper, "c.getAlert_v1", { alertId : component.get("v.alertId") }, this.loadAlertSuccess);
    },

    loadAlertSuccess : function(component, requestResult, helper) {
        component.set("v.alert", requestResult.resultObjects[0]);
        component.set("v.loading", false);
        if(component.get("v.originalMode") === "edit") {
            helper.setEngineer(component);
        }
    },	

	toggleEditMode : function(component) {
		var mode = component.get("v.mode");
		if(mode === "edit") {
			component.set("v.mode", "view");
		} else if(mode === "view") {
			component.set("v.mode", "edit");
		}
	},

	/*Show engineer in Edit mode field*/
	setEngineer : function(component) {
		var alert = component.get("v.alert");
		this.setEngineerForLookup(component, "EngineerLookup", alert.Production_Engineer__r, alert.Production_Engineer__c);
	},

	setEngineerForLookup : function(component, lookupId, operator, operatorId) {
		if(operator) {
			operator.Id = operatorId;
			var lookup = component.find(lookupId);
			lookup.selectRecord(operator);
		}
	},

	/*Dont know*/
	dataIsValid : function(component) {
		var alert = component.get("v.alert");
		var lookupCmp = component.find("EngineerLookup");
		lookupCmp.validate();
		return alert.Production_Engineer__c
	},

	 /*Pick List Values*/
	loadPicklistsOptions : function(component) {
		var service = component.find("picklistService");
		service.getPicklistValues($A.getCallback(function(error, data) {
			component.set("v.status", data);
		}),"HOG_Vent_Gas_Alert__c", "Status__c");
	},
})